<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Donor_m extends MY_Model {

	protected $_table_name = 'donors';
	protected $_primary_key = 'donorID';
	protected $_primary_filter = 'intval';
	protected $_order_by = "donorID asc";

	function __construct() {
		parent::__construct();
	}

	function get_donor($array=NULL, $signal=FALSE) {
		$query = parent::get($array, $signal);
		return $query;
	}

	function get_order_by_donor($array=NULL) {
		$query = parent::get_order_by($array);
		return $query;
	}

	function get_single_donor($array=NULL) {
		$query = parent::get_single($array);
		return $query;
	}

	function insert_donor($array) {
		$error = parent::insert($array);
		return $error;
	}

	function update_donor($data, $id = NULL) {
		parent::update($data, $id);
		return $id;
	}

	public function delete_donor($id){
		parent::delete($id);
	}

}

/* End of file book_m.php */
/* Location: .//D/xampp/htdocs/school/mvc/models/book_m.php */