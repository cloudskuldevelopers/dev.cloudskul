<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Payment_settings_m extends MY_Model {

	function update_key($array) {
		$this->db->update_batch('ini_config', $array, 'config_key'); 
	}

	function get_order_by_config() {
		$query = $this->db->get_where('ini_config');
		return $query->result();
	}

	function get_account_cloudskul() {
		$query = $this->db->get_where('account_cloudskul');
		return $query->row();
	}

	public function get_account_schools($id){
		//$username = $this->session->userdata("username");
		$schoolID = $this->session->userdata('schoolID');
		// $query = $this->db->get_where("account_schools", array('schoolID' => $schoolID));
		$query = $this->db->get_where("account_schools", array('schoolID' => $schoolID,"accountID"=>$id));
		return $query->row();
	}
}