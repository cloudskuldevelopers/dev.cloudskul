<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//Cloudskul Bank Account Model
class cloudaccount_m extends MY_Model {

	protected $_table_name = 'account_cloudskul';
	protected $_primary_key = 'accountID';
	protected $_primary_filter = 'intval';
	protected $_order_by = "accountID desc";

	function __construct() {
		parent::__construct();
	}

	function get_school($array=NULL, $signal=FALSE) {
		$query = parent::get($array, $signal);
		return $query;
	}

	function get_order_by_school($array=NULL) {
		$query = parent::get_order_by($array);
		return $query;
	}

	function insert_school($array) {
		$error = parent::insert($array);
		return $error;
	}

	function get_account($array=NULL, $signal=FALSE) {
		$query = parent::get($array, $signal);
		return $query;
	}

	function update_school($data, $id = NULL) {
		parent::update($data, $id);
		return $id;
	}

	public function delete_school($id){
		parent::delete($id);
	}
}

/* End of file exam_m.php */
/* Location: .//D/xampp/htdocs/school/mvc/models/exam_m.php */