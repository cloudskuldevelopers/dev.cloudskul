<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class applied_school_m extends MY_Model {

	protected $_table_name = 'tmp_school';
	protected $_primary_key = 'tmp_schoolID';
	protected $_primary_filter = 'intval';
	protected $_order_by = "tmp_schoolID desc";

	function __construct() {
		parent::__construct();
	}

	function get_tmp_school($array=NULL, $signal=FALSE) {
		$query = parent::get($array, $signal);
		return $query;
	}

	function get_order_by_tmp_school($array=NULL) {
		$query = parent::get_order_by($array);
		return $query;
	}

	function get_single_tmp_school($array) {
		$query = parent::get_single($array);
		return $query;
	}

	public function delete_tmp_school($id){
		parent::delete($id);
	}
}

/* End of file tmp_school_m.php */
/* Location: .//D/xampp/htdocs/school/mvc/models/tmp_school_m.php */