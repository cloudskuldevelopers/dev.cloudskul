<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Assignment_m extends MY_Model {

	protected $_table_name = 'assignment';
	protected $_primary_key = 'assignmentID';
	protected $_primary_filter = 'intval';
	protected $_order_by = "assignmentID desc";

	function __construct() {
		parent::__construct();
	}
	function get_classes() {
		$schoolID = $this->session->userdata('schoolID');
		$this->db->select('*')->from('classes')->where('schoolID', $schoolID)->order_by('classes_numeric asc');
		$query = $this->db->get();
		return $query->result();
	}

	function get_subject($id) {
		$schoolID = $this->session->userdata('schoolID');
		$query = $this->db->get_where('subject', array('schoolID' => $schoolID, 'classesID' => $id));
		return $query->result();
	}

	function get_join_assignment($id, $sectionID) {
		$schoolID = $this->session->userdata('schoolID');
		$this->db->select('*');
		$this->db->from('assignment');
		$this->db->where('assignment.schoolID', $schoolID);
		$this->db->where('assignment.classesID', $id);
		$this->db->where('assignment.sectionID', $sectionID);
		$query = $this->db->get();
		return $query->result();
	}
	
	function get_join_assignments($id) {
		$schoolID = $this->session->userdata('schoolID');
		$this->db->select('*');
		$this->db->from('assignment');
		$this->db->where('assignment.schoolID', $schoolID);
		$this->db->where('assignment.classesID', $id);
		//$this->db->where('assignment.sectionID', $sectionID);
		$query = $this->db->get();
		return $query->result();
	}
	function get_join_assignment_teacher($id) {
		$schoolID = $this->session->userdata('schoolID');
		$username = $this->session->userdata('username');
		$usertype = $this->session->userdata('usertype');
		$userID = $this->session->userdata('loginuserID');
		$this->db->select('*');
		$this->db->from('assignment');
		$this->db->where('assignment.schoolID', $schoolID);
		$this->db->where('assignment.classesID', $id);
		$this->db->where('assignment.create_username', $username);
		$this->db->where('assignment.create_usertype', $usertype);
		$this->db->where('assignment.create_userID', $userID);
		$query = $this->db->get();
		return $query->result();
	}

	function get_assignment($array=NULL, $signal=FALSE) {
		$query = parent::get($array, $signal);
		return $query;
	}

	function get_order_by_assignment($array=NULL) {
		$query = parent::get_order_by($array);
		return $query;
	}

	function get_single_assignment($array) {
		$query = parent::get_single($array);
		return $query;
	}

	function insert_assignment($array) {
		$error = parent::insert($array);
		return TRUE;
	}

	function update_assignment($data, $id = NULL) {
		parent::update($data, $id);
		return $id;
	}

	public function delete_assignment($id){
		parent::delete($id);
	}
}

/* End of file assignment_m.php */
/* Location: .//D/xampp/htdocs/school/mvc/models/assignment_m.php */