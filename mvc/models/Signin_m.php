<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class signin_m extends MY_Model {

	function __construct() {
		parent::__construct();
	}

	public function signin() {
		$tables = array('student' => 'student', 'parent' => 'parent', 'teacher' => 'teacher', 'user' => 'user', 'systemadmin' => 'systemadmin');
		$array = array();
		$i = 0;
		$username = $this->input->post('username');
		$password = $this->hash($this->input->post('password'));
		$userdata = '';
		foreach ($tables as $table) {
			$user = $this->db->get_where($table, array("username" => $username, "password" => $password));
			$alluserdata = $user->row();
			

			if(count($alluserdata)) {
				$userdata = $alluserdata;
				$array['permition'][$i] = 'yes';
			} else {
				$array['permition'][$i] = 'no';
			}
			$i++;
		}

		if(in_array('yes', $array['permition'])) {
			if($userdata->usertype == "Student") {
				if($userdata->studentactive == 1) {
					$school = $this->db->get_where('school', array("schoolID" => $userdata->schoolID));
					$school = $school->row();

					if(count($school)) {
						if($school->schoolactive == 1) {
							$data = array(
								"loginuserID" => $userdata->studentID,
								"schoolID" => $school->schoolID,
								"school" => $school->school,
								"schoollogo" => $school->photo,
								"name" => $userdata->name,
								"email" => $userdata->email,
								"usertype" => $userdata->usertype,
								"username" => $userdata->username,
								"photo" => $userdata->photo,
								"lang" => "english",
								"alias" => $school->alias,
								"loggedin" => TRUE
							);
							$this->session->set_userdata($data);
							return TRUE;
						} else {
							return FALSE;
						}
					} else {
						return FALSE;
					}

				} else {
					return FALSE;
				}
			} elseif($userdata->usertype == "Teacher") {
				if($userdata->teacheractive == 1) {
					$school = $this->db->get_where('school', array("schoolID" => $userdata->schoolID));
					$school = $school->row();
					if(count($school)) {
						if($school->schoolactive == 1) {
							$data = array(
								"loginuserID" => $userdata->teacherID,
								"schoolID" => $school->schoolID,
								"school" => $school->school,
								"schoollogo" => $school->photo,
								"name" => $userdata->name,
								"email" => $userdata->email,
								"phone_status" => $userdata->phone_status,
								"usertype" => $userdata->usertype,
								"username" => $userdata->username,
								"photo" => $userdata->photo,
								"lang" => "english",
								"loggedin" => TRUE
							);
							$this->session->set_userdata($data);
							return TRUE;
						} else {
							return FALSE;
						}
					} else {
						return FALSE;
					}

				} else {
					return FALSE;
				}
			} elseif($userdata->usertype == "Parent") {
				if($userdata->parentactive == 1) {
					$school = $this->db->get_where('school', array("schoolID" => $userdata->schoolID));
					$school = $school->row();
					if(count($school)) {
						if($school->schoolactive == 1) {
							$data = array(
								"loginuserID" => $userdata->parentID,
								"schoolID" => $school->schoolID,
								"school" => $school->school,
								"schoollogo" => $school->photo,
								"name" => $userdata->name,
								"email" => $userdata->email,
								"phone_status" => $userdata->phone_status,
								"usertype" => $userdata->usertype,
								"username" => $userdata->username,
								"photo" => $userdata->photo,
								"lang" => "english",
								"loggedin" => TRUE
							);
							$this->session->set_userdata($data);
							return TRUE;
						} else {
							return FALSE;
						}
					} else {
						return FALSE;
					}
				} else {
					return FALSE;
				}
			} elseif($userdata->usertype == 'Super Admin') {
				if($userdata->systemadminactive == 1) {
					if($userdata->systemadminID != 1 && $userdata->systemadminID != 4){
						if(!is_null($userdata->priviledges)){
							$access = explode(',', $userdata->priviledges);
							$school = $this->db->get_where('school', array("schoolID" => $access[0]))->row();
						}else{

						}

					}else{
						$this->db->select_max('schoolID');
						$school = $this->db->get('school');
						$school = $school->row();
					}
					if(count($school)) {
						$get_school = $this->db->get_where('school', array("schoolID" => $school->schoolID));
						$get_school = $get_school->row();
						$data = array(
							"loginuserID" => $userdata->systemadminID,
							"schoolID" => $school->schoolID,
							"school" => $get_school->school,
							"schoollogo" => $get_school->photo,
							"name" => $userdata->name,
							"email" => $userdata->email,
							"phone_status" => $userdata->phone_status,
							"usertype" => $userdata->usertype,
							"username" => $userdata->username,
							"photo" => $userdata->photo,
							"lang" => "english",
							"alias" => $school->alias,
							"loggedin" => TRUE
						);
						$this->session->set_userdata($data);
						return TRUE;
					} else {
						return FALSE;
					}
				} else {
					return FALSE;
				}
			} elseif($userdata->usertype == 'Admin' || $userdata->usertype == "Accountant" || $userdata->usertype == "Librarian") {
				if($userdata->useractive == 1) {
					$school = $this->db->get_where('school', array("schoolID" => $userdata->schoolID));
					$school = $school->row();

					if(count($school)) {
						if($school->schoolactive == 1) {
							$data = array(
								"loginuserID" => $userdata->userID,
								"schoolID" => $school->schoolID,
								"school" => $school->school,
								"schoollogo" => $school->photo,
								"name" => $userdata->name,
								"email" => $userdata->email,
								"phone_status" => $userdata->phone_status,
								"usertype" => $userdata->usertype,
								"username" => $userdata->username,
								"photo" => $userdata->photo,
								"lang" => "english",
								"alias" => $school->alias,
								"loggedin" => TRUE
							);
							$this->session->set_userdata($data);
							return TRUE;
						} else {
							return FALSE;
						}
					} else {
						return FALSE;
					}
				} else {
					return FALSE;
				}
			} else {
				return FALSE;
			}
		} else {
			return FALSE;
		}			
	}

	function change_password() {
		$table = strtolower($this->session->userdata("usertype"));
		if($table == "admin") {
			$table = "setting";
		} elseif($table == "accountant") {
			$table = "user";
		} elseif($table == "librarian") {
			$table = "user";
		} elseif($table == "super admin") {
			$table = "systemadmin";
		}
		

		$username = $this->session->userdata("username");
		$old_password = $this->hash($this->input->post('old_password'));
		$new_password = $this->hash($this->input->post('new_password'));

		$user = $this->db->get_where($table, array("username" => $username, "password" => $old_password));

		$alluserdata = $user->row();

		if(count($alluserdata)) {
			if($alluserdata->password == $old_password){
				$array = array(
					"password" => $new_password
				);
				$this->db->where(array("username" => $username, "password" => $old_password));
				$this->db->update($table, $array);
				$this->session->set_flashdata('success', $this->lang->line('menu_success'));
				return TRUE; 
			}
		} else {
			return FALSE;
		}
	}

	public function signout() {
		$this->session->sess_destroy();
	}

	public function loggedin() {
		return (bool) $this->session->userdata("loggedin");
	}
}

/* End of file signin_m.php */
/* Location: .//D/xampp/htdocs/school/mvc/models/signin_m.php */