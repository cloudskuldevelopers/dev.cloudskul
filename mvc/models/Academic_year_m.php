<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Academic_year_m extends MY_Model {

	protected $_table_name = 'academic_year';
	protected $_primary_key = 'yearID';
	protected $_primary_filter = 'intval';
	protected $_order_by = "year_title asc";

	function __construct() {
		parent::__construct();
	}

	function get_year($array=NULL, $signal=FALSE) {
		$query = parent::get($array, $signal);
		return $query;
	}

	function get_order_by_year($array=NULL) {
		$query = parent::get_order_by($array);
		return $query;
	}

	function get_single_year($array=NULL) {
		$query = parent::get_single($array);
		return $query;
	}

	function insert_year($array) {
		$error = parent::insert($array);
		return TRUE;
	}

	function update_year($data, $id = NULL) {
		parent::update($data, $id);
		return $id;
	}

	public function delete_year($id){
		parent::delete($id);
	}

	function allyears($title) {
		$query = $this->db->query("SELECT * FROM academic_year WHERE year_title LIKE '$title%'");
		return $query->result();
	}
}

/* End of file book_m.php */
/* Location: .//D/xampp/htdocs/school/mvc/models/book_m.php */