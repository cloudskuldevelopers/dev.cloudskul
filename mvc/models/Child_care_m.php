<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Child_care_m extends MY_Model {

	protected $_table_name = 'pickup_policy';
	protected $_primary_key = 'policyID';
	protected $_primary_filter = 'intval';
	protected $_order_by = "policyID asc";

	function __construct() {
		parent::__construct();
	}

	function get_username($table, $data=NULL) {
		$query = $this->db->get_where($table, $data);
		return $query->result();
	}

	function get_join_all($id) {
		$schoolID = $this->session->userdata('schoolID');
		//$this->db->select('name');
		$this->db->select(" Distinct( student.studentID ),student.name,parent.father_name,parent.mother_name,parent.phone,parent.pickup, GROUP_CONCAT( CONCAT_WS('&' ,`pickup_policy`.`policyID`,`pickup_policy`.`last_name`, pickup_policy.first_name ) SEPARATOR '".$this->db->escape_like_str(';')."') as adult");
		//$this->db->select('student.studentID,student.name,parent.father_name,parent.mother_name,parent.phone,pickup_policy.first_name,pickup_policy.last_name');
		$this->db->from('student');
		$this->db->where(array('student.schoolID' => $schoolID, 'student.classesID' => $id));
		$this->db->join('parent', 'parent.parentID = student.parentID','LEFT');
		$this->db->join('pickup_policy', 'pickup_policy.parentID = parent.parentID','LEFT');
		//$this->db->where('parent.pickup', 1);
		$this->db->group_by("student.studentID"); 
		$query = $this->db->get();
		return $query->result();
	}
	
	function get_classes() {
		$this->db->select('*')->from('classes')->order_by('classes_numeric asc');
		$query = $this->db->get();
		return $query->result();
	}

	function get_order_by_roll($array=NULL) {
		$query = parent::get_order_by($array);
		return $query;
	}

	function get_roll($id) {
		$query = $this->db->get_where('student', array('classesID' => $id, "parent !=" => 1));
		return $query->result();
	}

	function get_guardian($array=NULL, $signal=FALSE) {
		$query = parent::get($array, $signal);
		return $query;
	}

	function get_single_guardian($array) {
		$query = parent::get_single($array);
		return $query;
	}

	function get_order_by_guardian($array=NULL) {
		$query = parent::get_order_by($array);
		return $query;
	}

	function insert_guardian($array) {
		$error = parent::insert($array);
		return TRUE;
	}

	function update_guardian($data, $id = NULL) {
		parent::update($data, $id);
		return $id;
	}

	function update_student($data, $id = NULL) {
		$this->db->where('studentID', $id);
		$this->db->update('student', $data);
		return TRUE;
	}

	function delete_guardian($id){
		parent::delete($id);
		return TRUE;
	}

	function hash($string) {
		return parent::hash($string);
	}

	/* ini code starts here	*/
}

/* End of file parent_m.php */
/* Location: .//D/xampp/htdocs/school/mvc/models/parent_m.php */