<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class feetype_m extends MY_Model {

	protected $_table_name = 'feetype';
	protected $_primary_key = 'feetypeID';
	protected $_primary_filter = 'intval';
	protected $_order_by = "feetypeID asc";

	function __construct() {
		parent::__construct();
	}

	function get_feetype($array=NULL, $signal=FALSE) {
		$query = parent::get($array, $signal);
		return $query;
	}

	function get_order_by_feetype($array=NULL) {
		$query = parent::get_order_by($array);
		return $query;
	}

	function get_single_feetype($array=NULL) {
		$query = parent::get_single($array);
		return $query;
	}

	function insert_feetype($array) {
		$error = parent::insert($array);
		return TRUE;
	}

	function update_feetype($data, $id = NULL) {
		parent::update($data, $id);
		return $id;
	}

	public function delete_feetype($id){
		parent::delete($id);
	}

	function allfeetype($feetype) {
		$schoolID = $this->session->userdata('schoolID');
		$query = $this->db->query("SELECT * FROM feetype WHERE schoolID = '$schoolID' && feetype LIKE '$feetype%'");
		return $query->result();
	}
	
	// THE FEE CATEGORY FUNCTIONS
	function feeCategory(){
		return $this->db->get("cloudskul_fee_category")->result_array();
	}
	function getFeeTypeByCode($code){
		return $this->db->get_where('cloudskul_fee_category', array("code" => $code))->row()->category;
	}
}

/* End of file feetype_m.php */
/* Location: .//D/xampp/htdocs/school/mvc/models/feetype_m.php */