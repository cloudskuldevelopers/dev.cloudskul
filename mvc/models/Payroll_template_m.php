<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Payroll_template_m extends MY_Model {
	
	public $_table_name;
    public $_order_by;
    public $_primary_key;

	function __construct() {
		parent::__construct();
	}
	
	public function get_all_employee_template($school_id){
		$this->db->select('*');
		$this->db->from('employee_payroll');
		$this->db->where('schoolID', $school_id);
		$query=$this->db->get();
		$data= $query->result();
		return $data;
	}
	
	public function get_single_employee_template($payroll_id, $school_id){
		$this->db->select('*');
		$this->db->from('employee_payroll');
		$this->db->where('payroll_id', $payroll_id);
		$this->db->where('schoolID', $school_id);
		$query=$this->db->get();
		$data= $query->row();
		return $data;
	}
	
	public function get_unique_salary_template_name($schoolID, $template_name){
		$this->db->select('template_name');
		$this->db->from('employee_payroll');
		$this->db->where('template_name', $template_name);
		$this->db->where('schoolID', $schoolID);
		$query=$this->db->get();
		$data= $query->row();
		return $data;
	}
	
	public function insert_employee_payroll($data){
		$this->db->insert('employee_payroll', $data);
		return true;
	}
	
	public function delete_employee_template($payroll_id, $school_id){
		$this->db->delete('employee_payroll', array('payroll_id' => $payroll_id, 'schoolID' => $school_id));
		return true;
	}
	
	public function update_employee_payroll($payroll_id, $school_id, $data){
		$this->db->where('payroll_id', $payroll_id);
		$this->db->where('schoolID', $school_id);
		$this->db->update('employee_payroll', $data);
		return true;
	}
}

/* End of file book_m.php */
/* Location: .//D/xampp/htdocs/school/mvc/models/book_m.php */
?>