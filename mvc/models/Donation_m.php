<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Donation_m extends MY_Model {

	protected $_table_name = 'donation';
	protected $_primary_key = 'donateID';
	protected $_primary_filter = 'intval';
	protected $_order_by = "create_date asc";

	function __construct() {
		parent::__construct();
	}

	function get_donation($array=NULL, $signal=FALSE) {
		$query = parent::get($array, $signal);
		return $query;
	}

	function get_order_by_donation($array=NULL) {
		$query = parent::get_order_by($array);
		return $query;
	}

	function get_single_donation($array=NULL) {
		$query = parent::get_single($array);
		return $query;
	}

	function insert_donation($array) {
		$error = parent::insert($array);
		return $error;
	}

	function update_donation($data, $id = NULL) {
		parent::update($data, $id);
		return $id;
	}

	public function delete_donation($id){
		parent::delete($id);
	}

}

/* End of file book_m.php */
/* Location: .//D/xampp/htdocs/school/mvc/models/book_m.php */