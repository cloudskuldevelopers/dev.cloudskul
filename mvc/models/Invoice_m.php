<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Invoice_m extends MY_Model {

	protected $_table_name = 'invoice';
	protected $_primary_key = 'invoiceID';
	protected $_primary_filter = 'intval';
	protected $_order_by = "invoiceID desc";
	

	function __construct() {
		parent::__construct();
	}

	function get_join_where_classes($studentID, $classesID) {
		$this->db->select('*');
		$this->db->from('invoice');
		$this->db->join('classes', 'classes.classesID = invoice.classesID', 'LEFT');
		$this->db->where("invoice.classesID", $classesID);
		$this->db->where("invoice.studentID", $studentID);
		$query = $this->db->get();
		return $query->result();
	}

	function get_invoice($array=NULL, $signal=FALSE) {
		$query = parent::get($array, $signal);
		return $query;
	}

	function get_order_by_invoice($array=NULL) {
		$query = parent::get_order_by($array);
		return $query;
	}

	function get_single_invoice($array=NULL) {
		$query = parent::get_single($array);
		return $query;
	}

	function insert_invoice($array) {
		$error = parent::insert($array);
		return $error;
	}

	function update_invoice($data, $id = NULL) {
		parent::update($data, $id);
		return $id;
	}

	public function delete_invoice($id){
		parent::delete($id);
	}

	function getFeeTypesByCategory($category){
		return $this->db->get_where('feetype', array("feecategoryID" => $category))->result_array();
	}
	
	function getInstallmentByInstallmentRef($ref){
		return $this->db->get_where('installments', array("installment_ref" => $ref))->row();
	}
	
	function getStudentInvoiceDetails($installmentRef, $studentID){
		return $this->db->get_where('invoice', array("installment_ref" => $installmentRef, "studentID" => $studentID))->row();
	}
	
	function getInstallmentTableData($id){
		return $this->db->get_where('installment_status', array("installment_statusID" => $id))->row();
	}
	
	function getSelectedInstallmentsByID($orderID){
		return $this->db->get_where('installment_status', array("installment_statusID" => $orderID))->row();
	}
	
	function getApprovedInstallment($ref, $schoolID, $studentID){
		return $this->db->get_where('approved_payments', array("installment_ref" => $ref, "schoolID" => $schoolID, "studentID" => $studentID))->result_array();
	}
	function getApprovedSelectedInstallment($ref, $schoolID, $studentID){
		$array = array("installment_ref" => $ref, "schoolID" => $schoolID, "studentID" => $studentID);
		$this->db->select('selected_installments');
		$this->db->where($array);
		return $this->db->get('approved_payments')->result_array();
		//return $this->db->get_where('approved_payments', array("installment_ref" => $ref))->result_array();
	}
	
	function getInstallmentRefRecord($ref, $schoolID, $studentID){
		return $this->db->get_where('installment_status', array("installment_ref" => $ref, "schoolID" => $schoolID, "studentID" => $studentID))->row();
	}
}

/* End of file invoice_m.php */
/* Location: .//D/xampp/htdocs/school/mvc/models/invoice_m.php */