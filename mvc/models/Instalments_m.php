<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Instalments_m extends MY_Model {

	protected $_table_name = 'payment_instalments';
	protected $_primary_key = 'instID';
	protected $_primary_filter = 'intval';
	protected $_order_by = "instID desc";
	

	function __construct() {
		parent::__construct();
	}

	function get_join_where_classes($studentID, $classesID) {
		$this->db->select('*');
		$this->db->from('invoice');
		$this->db->join('classes', 'classes.classesID = invoice.classesID', 'LEFT');
		$this->db->where("invoice.classesID", $classesID);
		$this->db->where("invoice.studentID", $studentID);
		$query = $this->db->get();
		return $query->result();
	}

	function get_instalments($array=NULL, $signal=FALSE) {
		$query = parent::get($array, $signal);
		return $query;
	}

	function get_order_by_instalment($array=NULL) {
		$query = parent::get_order_by($array);
		return $query;
	}

	function get_single_instalment($array=NULL) {
		$query = parent::get_single($array);
		return $query;
	}

	function insert_instalment($array) {
		$error = parent::insert($array);
		return $error;
	}
	function insert_multiple($array){
		$error = parent::insert_batch($array);
		return $error;
	}
	function update_instalment($data, $id = NULL) {
		parent::update($data, $id);
		return $id;
	}

	function update_invoice_instalment($data, $id = NULL) {
		$this->db->set($data);
		$this->db->where("invoiceID", $id);
		$this->db->update($this->_table_name);
		return $id;
	}

	public function delete_instalments($id){
		parent::delete($id);
	}	
}

/* End of file invoice_m.php */
/* Location: .//D/xampp/htdocs/school/mvc/models/invoice_m.php */
