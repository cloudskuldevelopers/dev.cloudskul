<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Department_m extends MY_Model {
	
	public $_table_name;
    public $_order_by;
    public $_primary_key;

	function __construct() {
		parent::__construct();
	}
	
	public function get_add_department_by_id($school_id, $department_id) {
		$this->db->select('departments.*', FALSE);
        $this->db->from('departments');
        $this->db->where('departments.schoolID', $school_id);
        $this->db->select('designations.*', FALSE);
		$this->db->where('departments.department_id', $department_id);
        $this->db->join('designations', 'departments.department_id = designations.department_id', 'left');
        $query_result = $this->db->get();
        $result = $query_result->result();
        return $result;
	}
	
	public function get_add_department($school_id) {
		$this->db->select('departments.department_name', FALSE);
        $this->db->from('departments');
        $this->db->where('departments.schoolID', $school_id);
        $this->db->select('designations.*', FALSE);
        $this->db->join('designations', 'departments.department_id = designations.department_id', 'left');
        $query_result = $this->db->get();
        $result = $query_result->result();
        return $result;
	}
	
	public function get_unique_id($school_id, $department_name){
		$this->db->select('*');
		$this->db->from('departments');
		$this->db->where('department_name', $department_name);
		$this->db->where('schoolID', $school_id);
		$query=$this->db->get();
		$data= $query->row();
		return $data;
	}
	
	public function get_unique_department($school_id){
		$this->db->select('*');
		$this->db->from('departments');
		$this->db->where('schoolID', $school_id);
		$query_result=$this->db->get();
		$result = $query_result->result();
        return $result;
	}
	
	public function get_unique_designations($department_id){
		$this->db->select('*');
		$this->db->from('designations');
		$this->db->where('department_id', $department_id);
		$query_result=$this->db->get();
		$result = $query_result->result();
        return $result;
	}
	
	public function insert_departments($data){
		$this->db->insert('departments', $data);
	}
	
	public function insert_designations($data){
		$this->db->insert('designations', $data);
	}
	
	public function delete_department_id($department_id, $school_id){
		$this->db->where('department_id', $department_id);
		$this->db->where('schoolID', $school_id);
		$this->db->delete('departments');
		return true;
	}
	
	public function delete_designation_m($designation_id){
		$this->db->where('designation_id', $designation_id);
		$this->db->delete('designations');
		return true;
	}
	
	public function edit_designation_m($data, $designation_id){
		$this->db->where('designation_id', $designation_id);
		$this->db->update('designations', $data);
		return true;
	}
	
	public function get_single_designation($designation_id){
		$this->db->select('*');
		$this->db->from('designations');
		$this->db->where('designation_id', $designation_id);
		$query=$this->db->get();
		$data= $query->row();
		return $data;
	}
	
	public function update_department_details($data, $designation_id, $department_id){
		$this->db->where('designation_id', $designation_id);
		$this->db->where('department_id', $department_id);
		$this->db->update('designations', $data);
	}
	
	public function delete_designation_id($department_id){
		$this->db->where('department_id', $department_id);
		$this->db->delete('designations');
		return true;
	}
	
	public function update_departments($data, $department_id, $schoolID){
		$this->db->where('department_id', $department_id);
		$this->db->where('schoolID', $schoolID);
		$this->db->update('departments', $data);
	}
	
	public function update_designaitons($data, $department_id){
		$this->db->where('department_id', $department_id);
		//$this->db->where('designation_id', $designationID);
		$this->db->update('designations', $data);
	}
	
	function getUsersWhere($school_id,$department_name){
		$this->db->where($school_id,$department_name);
		$query=$this->db->get('departments');
		return $query->result_array();

	}
}

/* End of file book_m.php */
/* Location: .//D/xampp/htdocs/school/mvc/models/book_m.php */
?>