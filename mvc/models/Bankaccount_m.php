<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//School Bank Account Model
/* 
   Author: Etti Emmanuel
   Contributing Author: Sunday Okoi
   Supervisor: Ugi Augustine
   Website: www.cloudskul.com
   Email: teamCloudSkul@cloudskul.com
*/
class bankaccount_m extends MY_Model {

	protected $_table_name = 'account_schools';
	protected $_primary_key = 'accountID';
	protected $_primary_filter = 'intval';
	protected $_order_by = "accountID desc";

	function __construct() {
		parent::__construct();
	}

	function get_school($array=NULL, $signal=FALSE) {
		$query = parent::get($array, $signal);
		return $query;
	}

	function allaccount() {
		$query = $this->db->query("SELECT * FROM account_schools as a,school as s WHERE a.schoolID = s.schoolID ");
		return $query->result();
	}

	function get_order_by_school($array=NULL) {
		$query = parent::get_order_by($array);
		return $query;
	}

	function insert_school($array) {
		$error = parent::insert($array);
		return $error;
	}

	function update_school($data, $id = NULL) {
		parent::update($data, $id);
		return $id;
	}

	public function delete_account($id){
		parent::delete($id);
	}

	function allbanks($bank) {
		//$schoolID = $this->session->userdata('schoolID');
		$query = $this->db->query("SELECT * FROM bank_info WHERE bank_name LIKE '$bank%'");
		return $query->result();
	}
}

/* End of file exam_m.php */
/* Location: .//D/xampp/htdocs/school/mvc/models/exam_m.php */