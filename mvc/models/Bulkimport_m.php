<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class bulkimport_m extends MY_Model {

	protected $_table_name = 'bulkimport';
	protected $_primary_key = 'bulkimportID';
	protected $_primary_filter = 'intval';
	protected $_order_by = "bulkimportID desc";

	function __construct() {
		parent::__construct();
	}

	function get_bulkimport($array=NULL, $signal=FALSE) {
		$query = parent::get($array, $signal);
		return $query;
	}

	function get_order_by_bulkimport($array=NULL) {
		$query = parent::get_order_by($array);
		return $query;
	}

	function insert_bulkimport($array) {
		$error = parent::insert($array);
		return TRUE;
	}

	function update_bulkimport($data, $id = NULL) {
		parent::update($data, $id);
		return $id;
	}

	public function delete_bulkimport($id){
		parent::delete($id);
	}
}

/* End of file bulkimport_m.php */
/* Location: .//D/xampp/htdocs/school/mvc/models/bulkimport_m.php */