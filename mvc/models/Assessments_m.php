<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Assessments_m extends MY_Model {

	protected $_table_name = 'assessments';
	protected $_primary_key = 'testID';
	protected $_primary_filter = 'intval';
	protected $_order_by = "assessments asc";

	function __construct() {
		parent::__construct();
	}

	function get_assessments($array=NULL, $signal=FALSE) {
		$query = parent::get($array, $signal);
		return $query;
	}

	function get_order_by_assessments($array=NULL) {
		$query = parent::get_order_by($array);
		return $query;
	}

	function get_single_assessments($array) {
		$query = parent::get_single($array);
		return $query;
	}

	function insert_assessment($array) {
		$error = parent::insert($array);
		return TRUE;
	}

	function update_assessments($data, $id = NULL) {
		parent::update($data, $id);
		return $id;
	}
	
	function update_assessment($data, $id, $schoolID) {
		$array = array("schoolID" => $schoolID, "examID" => $id);
		$this->db->where($array);
		$this->db->update('assessments', $data);
	}

	public function delete_assessments($id){
		parent::delete($id);
	}
}

/* End of file assessments_m.php */
/* Location: .//D/xampp/htdocs/school/mvc/models/assessments_m.php */