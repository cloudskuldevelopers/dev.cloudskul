<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//this model was edited by @eetti
class smssettings_m extends MY_Model {

	function update_data($schoolID, $field_names, $field_values) {
		$array = array(
			'field_values' => $field_values
		);
		$this->db->update('smssettings', $array, array('schoolID' => $schoolID, 'field_names' => $field_names));
		return TRUE;
	}

	function get_order_by_clickatell() {
		//$schoolID = $this->session->userdata('schoolID');
		$schoolID = '5';
		$query = $this->db->get_where('smssettings', array('schoolID' => $schoolID, 'types' => 'clickatell'));
		return $query->result();
	}

	function update_clickatell($array) {
		$this->db->update_batch('smssettings', $array, 'field_names'); 
	}

	function get_order_by_twilio() {
		//$schoolID = $this->session->userdata('schoolID');
		$schoolID = '5';
		$query = $this->db->get_where('smssettings', array('schoolID' => $schoolID, 'types' => 'twilio'));
		return $query->result();
	}

	function update_twilio($array) {
		$this->db->update_batch('smssettings', $array, 'field_names'); 
	}

	function get_order_by_bulk() {
		//$schoolID = $this->session->userdata('schoolID');
		$schoolID = '5';
		$query = $this->db->get_where('smssettings', array('schoolID' => $schoolID, 'types' => 'bulk'));
		return $query->result();
	}

	function update_bulk($array) {
		$this->db->update_batch('smssettings', $array, 'field_names'); 
	}


	function get_order_by_ozioma() {
		//$schoolID = $this->session->userdata('schoolID');
		$schoolID = '5';
		$query = $this->db->get_where('smssettings', array('schoolID' => $schoolID, 'types' => 'ozioma'));
		return $query->result();
	}

	function update_ozioma($array) {
		$this->db->update_batch('smssettings', $array, 'field_names'); 
	}



}