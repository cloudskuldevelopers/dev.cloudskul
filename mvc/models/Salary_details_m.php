<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Salary_details_m extends MY_Model {
	
	public $_table_name;
    public $_order_by;
    public $_primary_key;

	function __construct() {
		parent::__construct();
	}
	
	public function get_departments($school_id){
		$this->db->select('*');
		$this->db->from('departments');
		$this->db->where('schoolID', $school_id);
		$query=$this->db->get();
		$data= $query->result();
		return $data;
	}
	
	public function get_employees_in_departments($designation_id, $school_id){
		$this->db->select('*');
		$this->db->from('employee');
		$this->db->where('designation_id', $designation_id);
		$this->db->where('schoolID', $school_id);
		$this->db->join('designations', 'designations.designation_id  = employee.designation_id', 'left');
		$query=$this->db->get();
		$data= $query->result();
		return $data;
	}
	
	public function get_salary_template($school_id){
		$this->db->select('*');
		$this->db->from('employee_payroll');
		$this->db->where('schoolID', $school_id);
		$query=$this->db->get();
		$data= $query->result();
		return $data;
	}
	
	public function get_emp_info_by_id($designation_id, $school_id) {
        $this->db->select('employee.*');
		$this->db->where('employee.schoolID', $school_id);
        $this->db->from('employee');
        $this->db->select('designations.*');
        $this->db->where('designations.designation_id', $designation_id);
        $this->db->join('designations', 'designations.designation_id  = employee.designation_id', 'left');
        $query_result = $this->db->get();
        $result = $query_result->result();
        return $result;
    }
	
	public function get_emp_information($department_id, $school_id, $designation_id){
        $this->db->select('employee.*'); 
		$this->db->where('employee.department_id', $department_id);
		$this->db->from('employee');
		$this->db->select('designations.designation_name');
		$this->db->join('designations', 'designations.designation_id  = designations.designation_id');
		$this->db->where('designations.designation_id', $designation_id);
        $query = $this->db->get();
        $result = $query->result();
        return $result;
	}
	
	public function update_salary_details($data, $payroll_id, $school_id){
		$this->db->where('payroll_id', $payroll_id);
		$this->db->where('schoolID', $school_id);
		$this->db->update('employee_payroll', $data);
	}
	
	public function get_employee_salary_list($school_id){
		$this->db->select('employee_payroll.*');
		$this->db->select('employee.*'); 
		$this->db->where('employee.schoolID', $school_id);
		$this->db->select('designations.*', FALSE);
        $this->db->select('departments.department_name', FALSE);
		$this->db->from('employee_payroll');
		$this->db->join('employee', 'employee_payroll.employee_id = employee.employee_id', 'left');
        $this->db->join('designations', 'designations.designation_id  = employee.designation_id', 'left');
        $this->db->join('departments', 'departments.department_id  = designations.department_id', 'left');
        $query = $this->db->get();
        $result = $query->result();
        return $result;
	}
}
?>