<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Employee_m extends MY_Model {
	
	public $_table_name;
    public $_order_by;
    public $_primary_key;

	function __construct() {
		parent::__construct();
	}
	
	
    public function all_emplyee_info($schoolID) {
        $this->db->select('employee.*', FALSE);
        $this->db->select('designations.*', FALSE);
        $this->db->select('departments.department_name', FALSE);
        $this->db->from('employee');
		$this->db->where('employee.schoolID', $schoolID);
        $this->db->join('designations', 'designations.designation_id  = employee.designation_id', 'left');
        $this->db->join('departments', 'departments.department_id  = designations.department_id', 'left');
		$query_result = $this->db->get();
        $result = $query_result->result();
        return $result;
    }
	
	public function single_emplyee_info($schoolID, $employee_id) {
        $this->db->select('employee.*', FALSE);
        $this->db->select('employee_bank.*', FALSE);
        $this->db->select('employee_document.*', FALSE);
        $this->db->select('designations.*', FALSE);
        $this->db->select('departments.department_name', FALSE);
        $this->db->from('employee');
		$this->db->where('employee.employee_id', $employee_id);
		$this->db->where('employee.schoolID', $schoolID);
        $this->db->join('employee_bank', 'employee_bank.employee_id = employee.employee_id', 'left');
        $this->db->join('employee_document', 'employee_document.employee_id  = employee.employee_id', 'left');
        $this->db->join('designations', 'designations.designation_id  = employee.designation_id', 'left');
        $this->db->join('departments', 'departments.department_id  = designations.department_id', 'left');
		$query_result = $this->db->get();
        $result = $query_result->row();
        return $result;
    }
	
	public function delete_single_employee($schoolID, $employee_id){
		$this->db->delete('employee', array('schoolID' => $schoolID, 'employee_id' => $employee_id));
		$this->db->delete('employee_bank', array('employee_id' => $employee_id));
		$this->db->delete('employee_document', array('employee_id' => $employee_id));
		return true;
	}
	
	public function get_unique_id($school_id, $employee_id){
		$this->db->select('*');
		$this->db->from('employee');
		$this->db->where('employment_id', $employee_id);
		$this->db->where('schoolID', $school_id);
		$query=$this->db->get();
		$data= $query->row();
		return $data;
	}
	
	public function insert_employee($data){
		$this->db->insert('employee', $data);
	}
	
	public function insert_employee_bank($data){
		$this->db->insert('employee_bank', $data);
	}
	
	public function insert_employee_document($data){
		$this->db->insert('employee_document', $data);
	}
}

/* End of file book_m.php */
/* Location: .//D/xampp/htdocs/school/mvc/models/book_m.php */
?>