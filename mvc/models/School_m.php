<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class school_m extends MY_Model {

	protected $_table_name = 'school';
	protected $_primary_key = 'schoolID';
	protected $_primary_filter = 'intval';
	protected $_order_by = "schoolID desc";

	function __construct() {
		parent::__construct();
	}

	function get_school($array=NULL, $signal=FALSE) {
		$query = parent::get($array, $signal);
		return $query;
	}

	function get_order_by_school($array=NULL) {
		$query = parent::get_order_by($array);
		return $query;
	}
	
	function get_order_by_school_alias($post){
		return $this->db->get_where($this->_table_name, array("alias" => $post))->row();
	}

	function insert_school($array) {
		$error = parent::insert($array);
		return $error;
	}

	function update_school($data, $id = NULL) {
		parent::update($data, $id);
		return $id;
	}

	public function delete_school($id){
		parent::delete($id);
	}
}

/* End of file exam_m.php */
/* Location: .//D/xampp/htdocs/school/mvc/models/exam_m.php */