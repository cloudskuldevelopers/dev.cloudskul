<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Academic_year extends MY_Controller {
/*
| -----------------------------------------------------
| PRODUCT NAME: 	CloudSkul
| -----------------------------------------------------
| AUTHOR:			CLOUDSKUL TEAM
| -----------------------------------------------------
| EMAIL:			info@cloudskul.com
| -----------------------------------------------------
| COPYRIGHT:		RESERVED BY Cloudskul
| -----------------------------------------------------
| WEBSITE:			http://cloudskul.com
| -----------------------------------------------------
*/
	function __construct() {
		parent::__construct();
		//$this->load->model("bankaccount_m");
		//$this->load->model("cloudaccount_m");
		$this->load->model("academic_year_m");
		$this->load->model("school_m");
		$this->load->model("systemadmin_m");
		$language = $this->session->userdata('lang');
		$this->lang->load('academic_year', $language);	
	}

	public function index() {
		$usertype = $this->session->userdata("usertype");
		$schoolID = $this->session->userdata('schoolID');
		if($usertype == "Super Admin" || $usertype == "Admin") {
			$this->data['years'] = $this->academic_year_m->get_order_by_year(array("schoolID" => $schoolID));
			$this->data["subview"] = "academic_year/index";
			$this->load->view('_layout_main', $this->data);
		} else {
			$this->data["subview"] = "error";
			$this->load->view('_layout_main', $this->data);
		}
	}

	protected function rules() {
		$rules = array(
				array(
					'field' => 'year_title', 
					'label' => $this->lang->line("year_title"), 
					'rules' => 'trim|required|xss_clean|max_length[60]|callback_unique_year'
				)
			);
		return $rules;
	}

	public function add() {
		$usertype = $this->session->userdata("usertype");
		if($usertype == "Admin" || $usertype == "Super Admin") {
			if($_POST) {
				$rules = $this->rules();
				$this->form_validation->set_rules($rules);
				if ($this->form_validation->run() == FALSE) {
					$this->data["subview"] = "academic_year/add";
					$this->load->view('_layout_main', $this->data);			
				} else {
					$array = array(
						"schoolID" => $this->session->userdata('schoolID'),
						"year_title" => $this->input->post("year_title"),
						"year_status" => 1,
						"create_date" => date("Y-m-d h:i:s"),
						"modify_date" => date("Y-m-d h:i:s"),
						"create_userID" => $this->session->userdata('loginuserID'),
						"create_username" => $this->session->userdata('username'),
						"create_usertype" => $this->session->userdata('usertype'),
					);
					$this->academic_year_m->insert_year($array);
					$this->session->set_flashdata('success', $this->lang->line('menu_success'));
					redirect(base_url("academic_year/index"));
				}
			} else {
				$this->data["subview"] = "academic_year/add";
				$this->load->view('_layout_main', $this->data);
			}
		} else {
			$this->data["subview"] = "error";
			$this->load->view('_layout_main', $this->data);
		}
	}

	public function edit() {
		$usertype = $this->session->userdata("usertype");
		$schoolID = $this->session->userdata('schoolID');
		if($usertype == "Admin" || $usertype == "Super Admin") {
			$id = htmlentities($this->db->escape_str($this->uri->segment(3)));
			if((int)$id) {
				$this->data['year'] = $this->academic_year_m->get_single_year(array('schoolID' => $schoolID, 'yearID' => $id));
				if($this->data['year']) {
					if($_POST) {
						$rules = $this->rules();
						$this->form_validation->set_rules($rules);
						if ($this->form_validation->run() == FALSE) {
							$this->data['form_validation'] = validation_errors(); 
							$this->data["subview"] = "academic_year/add";
							$this->load->view('_layout_main', $this->data);			
						} else {
							$array = array(
								"year_title" => $this->input->post("year_title"),
								"year_status" => 1,
								"modify_date" => date("Y-m-d h:i:s")
							);
							$this->academic_year_m->update_year($array, $id);
							$this->session->set_flashdata('success', $this->lang->line('menu_success'));
							redirect(base_url("academic_year/index"));
						}
					} else {
						$this->data["subview"] = "academic_year/edit";
						$this->load->view('_layout_main', $this->data);
					}
				} else {
					$this->data["subview"] = "error";
					$this->load->view('_layout_main', $this->data);
				}
			} else {
				$this->data["subview"] = "error";
				$this->load->view('_layout_main', $this->data);
			}
		} else {
			$this->data["subview"] = "error";
			$this->load->view('_layout_main', $this->data);
		}
	}

	public function delete() {
		$usertype = $this->session->userdata("usertype");
		$schoolID = $this->session->userdata('schoolID');
		if($usertype == "Admin" || $usertype == "Super Admin") {
			$id = htmlentities($this->db->escape_str($this->uri->segment(3)));
			if((int)$id) {
				$this->data['year'] = $this->academic_year_m->get_single_year(array('schoolID' => $schoolID, 'yearID' => $id));
				if($this->data['year']) {
					$this->academic_year_m->delete_year($id);
					$this->session->set_flashdata('success', $this->lang->line('menu_success'));
					redirect(base_url("academic_year/index"));
				} else {
					redirect(base_url("academic_year/index"));
				}
			} else {
				redirect(base_url("academic_year/index"));
			}	
		} else {
			$this->data["subview"] = "error";
			$this->load->view('_layout_main', $this->data);
		}
	}

	public function unique_year() {
		$id = htmlentities($this->db->escape_str($this->uri->segment(3)));
		$schoolID = $this->session->userdata('schoolID');
		if((int)$id) {
			$student = $this->academic_year_m->get_order_by_year(array('schoolID' => $schoolID, "year_title" => $this->input->post("year_title")));
			if(count($student)) {
				$this->form_validation->set_message("unique_year", "%s already exists");
				return FALSE;
			}
			return TRUE;
		} else {
			$student = $this->academic_year_m->get_order_by_year(array('schoolID' => $schoolID, "year_title" => $this->input->post("year_title")));

			if(count($student)) {
				$this->form_validation->set_message("unique_year", "%s already exists");
				return FALSE;
			}
			return TRUE;
		}	
	}
}