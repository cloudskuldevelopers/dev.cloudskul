<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Profile extends MY_Controller {
/*
| -----------------------------------------------------
| PRODUCT NAME: 	CloudSkul
| -----------------------------------------------------
| AUTHOR:			CLOUDSKUL TEAM
| -----------------------------------------------------
| EMAIL:			info@cloudskul.com
| -----------------------------------------------------
| COPYRIGHT:		RESERVED BY Cloudskul
| -----------------------------------------------------
| WEBSITE:			http://cloudskul.com
| -----------------------------------------------------
*/
	function __construct() {
		parent::__construct();
		$this->load->model("student_m");
		$this->load->model("parentes_m");
		$this->load->model("teacher_m");
		$this->load->model("user_m");
		$this->load->model("systemadmin_m");
		$language = $this->session->userdata('lang');
		$this->lang->load('profile', $language);
		$usertype = $this->session->userdata("usertype");
		if($usertype != "Parent") {	
		$this->lang->load('user', $language);	
		}else{
		$this->lang->load('parentes', $language);	
		}
	}

	protected function rules() {
		$rules = array(
			array(
				'field' => 'name', 
				'label' => $this->lang->line("user_name"), 
				'rules' => 'trim|required|xss_clean|max_length[60]'
			), 
			array(
				'field' => 'dob', 
				'label' => $this->lang->line("user_dob"),
				'rules' => 'trim|required|max_length[10]|callback_date_valid|xss_clean'
			),
			array(
				'field' => 'sex', 
				'label' => $this->lang->line("user_sex"), 
				'rules' => 'trim|max_length[10]|xss_clean'
			),
			array(
				'field' => 'religion', 
				'label' => $this->lang->line("user_religion"), 
				'rules' => 'trim|max_length[25]|xss_clean'
			),
			array(
				'field' => 'email', 
				'label' => $this->lang->line("user_email"), 
				'rules' => 'trim|required|max_length[40]|valid_email|xss_clean|callback_unique_email'
			),
			array(
				'field' => 'phone', 
				'label' => $this->lang->line("user_phone"), 
				'rules' => 'trim|min_length[11]|max_length[11]|xss_clean'
			),
			array(
				'field' => 'address', 
				'label' => $this->lang->line("user_address"), 
				'rules' => 'trim|max_length[200]|xss_clean'
			),
			array(
				'field' => 'jod', 
				'label' => $this->lang->line("user_jod"), 
				'rules' => 'trim|required|max_length[10]|callback_date_valid|xss_clean'
			),
			array(
				'field' => 'usertype', 
				'label' => $this->lang->line("user_type"), 
				'rules' => 'trim|required|max_length[20]|xss_clean'
			),
			array(
				'field' => 'photo', 
				'label' => $this->lang->line("user_photo"), 
				'rules' => 'trim|max_length[200]|xss_clean'
			),
			array(
				'field' => 'username', 
				'label' => $this->lang->line("user_username"), 
				'rules' => 'trim|required|min_length[4]|max_length[40]|xss_clean|callback_lol_username'
			),
			array(
				'field' => 'password',
				'label' => $this->lang->line("user_password"), 
				'rules' => 'trim|required|min_length[4]|max_length[40]|min_length[4]|xss_clean'
			)
		);
		return $rules;
	}

	public function verify_phone(){
		$phone = $this->input->post("phone");
		$code  = 'EE';
		$code .= substr(number_format(time() * rand(),0,'',''),0,4);
		$this->session->set_userdata("verify",$code);
		if($phone){
			echo $code;
		}
	}

	public function verify_phone_code(){
		
		$code = $this->session->userdata("verify");
		if($code){
			echo $code;
		}
	}

	public function index() {
		$usertype = $this->session->userdata("usertype");
		$username = $this->session->userdata('username');
		if($usertype == "Admin") {
			$user = $this->user_m->get_single_user(array("username" => $username));
			if($user) {
				$this->data['siteinfos'] = $user;
				$this->data['user'] = $user;
				$this->data["subview"] = "profile/index";
				$this->load->view('_layout_main', $this->data);
			} else {
				$this->data["subview"] = "error";
				$this->load->view('_layout_main', $this->data);
			}
		} elseif($usertype == "Librarian" || $usertype == "Accountant") {
			$user = $this->user_m->get_single_user(array("username" => $username));
			if($user) {
				$this->data['user'] = $user;
				$this->data["subview"] = "profile/index";
				$this->load->view('_layout_main', $this->data);
			} else {
				$this->data["subview"] = "error";
				$this->load->view('_layout_main', $this->data);
			}
		} elseif($usertype == "Teacher") {
			$teacher = $this->teacher_m->get_single_teacher(array('username' => $username));
			if($teacher) {
				$this->data['teacher'] = $teacher;
				$this->data["subview"] = "profile/index";
				$this->load->view('_layout_main', $this->data);
			} else {
				$this->data["subview"] = "error";
				$this->load->view('_layout_main', $this->data);
			}
		} elseif($usertype == "Student") {
			$student = $this->student_m->get_single_student(array('username' => $username));
			$this->data["student"] = $this->student_m->get_student($student->studentID);
			$this->data["class"] = $this->student_m->get_class($student->classesID);
			if($this->data["student"] && $this->data["class"]) {
				$this->data["parent"] = $this->parentes_m->get_parentes($student->parentID);
				$this->data["subview"] = "profile/index";
				$this->load->view('_layout_main', $this->data);
			} else {
				$this->data["subview"] = "error";
				$this->load->view('_layout_main', $this->data);
			}
		} elseif($usertype == "Parent") {
			$parentes = $this->parentes_m->get_single_parentes(array("username" => $username));
			if($parentes) {
				$this->data['parentes'] = $parentes;
				$this->data['user'] = $parentes;
				$this->data["subview"] = "profile/index";
				$this->load->view('_layout_main', $this->data);
			} else {
				$this->data["subview"] = "error";
				$this->load->view('_layout_main', $this->data);
			}
		} elseif($usertype == "Super Admin") {
			$systemadmin = $this->systemadmin_m->get_single_systemadmin(array("username" => $username));
			if($systemadmin) {
				$this->data['systemadmin'] = $systemadmin;
				$this->data["subview"] = "profile/index";
				$this->load->view('_layout_main', $this->data);
			} else {
				$this->data["subview"] = "error";
				$this->load->view('_layout_main', $this->data);
			}
		} else {
			$this->data["subview"] = "error";
			$this->load->view('_layout_main', $this->data);
		}
	}

	function insert_with_image($username) {
	    $random = rand(1, 10000000000000000);
	    $makeRandom = hash('sha512', $random. $username . config_item("encryption_key"));
	    return $makeRandom;
	}
	public function edit() {
		$usertype = $this->session->userdata("usertype");
		if($usertype == "Admin" || $usertype == "Super Admin") {
			$id = htmlentities($this->db->escape_str($this->uri->segment(3)));
			
			if ((int)$id) {
				$schoolID = $this->session->userdata('schoolID');
				$this->data['user'] = $this->user_m->get_single_user(array("schoolID" => $schoolID, "userID" => $id));
				if($this->data['user']) {
					/*if($this->data['user']->usertype == "Admin" && $this->session->userdata('usertype') == "Admin") {
						redirect(base_url("profile/index"));
					}*/

					if($_POST) {
						$rules = $this->rules();
						unset($rules[7],$rules[8],$rules[9], $rules[10], $rules[11]);
						$this->form_validation->set_rules($rules);
						if ($this->form_validation->run() == FALSE) { 
							$this->data["subview"] = "profile/edit";
							$this->load->view('_layout_main', $this->data);
						} else {
							$array = array();
							$array["name"] = $this->input->post("name");
							$array["dob"] = date("Y-m-d", strtotime($this->input->post("dob")));
							$array["sex"] = $this->input->post("sex");
							$array["religion"] = $this->input->post("religion");
							$array["email"] = $this->input->post("email");
							$array["phone"] = $this->input->post("phone");
							$array["address"] = $this->input->post("address");
							//$array["jod"] = date("Y-m-d", strtotime($this->input->post("jod")));
							//$array["usertype"] = $usertype;
							$array["modify_date"] = date("Y-m-d h:i:s");
							
							if($_FILES["image"]['name'] !="") {
								if($this->data['user']->photo != 'defualt.png') {
									unlink(FCPATH.'uploads/images/'.$this->data['user']->photo);
								}
								$file_name = $_FILES["image"]['name'];
								$file_name_rename = $this->insert_with_image($this->data['user']->username);
					            $explode = explode('.', $file_name);
					            if(count($explode) >= 2) {
						            $new_file = $file_name_rename.'.'.$explode[1];
									$config['upload_path'] = "./uploads/images";
									$config['allowed_types'] = "gif|jpg|png";
									$config['file_name'] = $new_file;
									$config['max_size'] = '1024';
									$config['max_width'] = '3000';
									$config['max_height'] = '3000';
									$array['photo'] = $new_file;
									$this->load->library('upload', $config);
									if(!$this->upload->do_upload("image")) {
										$this->data["image"] = $this->upload->display_errors();
										$this->data["subview"] = "user/edit";
										$this->load->view('_layout_main', $this->data);
									} else {
										$data = array("upload_data" => $this->upload->data());
										$this->user_m->update_user($array, $id);
										$this->session->set_flashdata('success', $this->lang->line('menu_success'));
										redirect(base_url("user/index"));
									}
								} else {
									$this->data["image"] = "Invalid file";
									$this->data["subview"] = "user/edit";
									$this->load->view('_layout_main', $this->data);
								}
							} else {
								$this->user_m->update_user($array, $id);
								$this->session->set_flashdata('success', $this->lang->line('menu_success'));
								redirect(base_url("profile/index"));
							}
						}
					} else {
						$this->data["subview"] = "profile/edit";
						$this->load->view('_layout_main', $this->data);
					}
				} else {
					$this->data["subview"] = "error";
					$this->load->view('_layout_main', $this->data);
				}
			} else {
				$this->data["subview"] = "error";
				$this->load->view('_layout_main', $this->data);
			}
		} elseif($usertype == "Parent" || $usertype == "Student" || $usertype == "Librarian" || $usertype == "Accountant"){
			$id = htmlentities($this->db->escape_str($this->uri->segment(3)));
			
			if ((int)$id) {
				$schoolID = $this->session->userdata('schoolID');
				$this->data['user'] = $this->parentes_m->get_single_parentes(array("schoolID" => $schoolID, "parentID" => $id));
				$this->data['parentes'] = $this->data['user'];
				if($this->data['user']) {
					if($_POST) {
						$rules = $this->rules();
						unset($rules[7],$rules[8],$rules[9], $rules[10], $rules[11]);
						$this->form_validation->set_rules($rules);
						if ($this->form_validation->run() == FALSE) { 
							$this->data["subview"] = "profile/edit";
							$this->load->view('_layout_main', $this->data);
						} else {
							$array = array();
							$array["name"] = $this->input->post("name");
							$array["dob"] = date("Y-m-d", strtotime($this->input->post("dob")));
							$array["sex"] = $this->input->post("sex");
							$array["religion"] = $this->input->post("religion");
							$array["email"] = $this->input->post("email");
							$array["phone"] = $this->input->post("phone");
							$array["address"] = $this->input->post("address");
							//$array["jod"] = date("Y-m-d", strtotime($this->input->post("jod")));
							//$array["usertype"] = $usertype;
							$array["modify_date"] = date("Y-m-d h:i:s");
							
							if($_FILES["image"]['name'] !="") {
								if($this->data['user']->photo != 'defualt.png') {
									unlink(FCPATH.'uploads/images/'.$this->data['user']->photo);
								}
								$file_name = $_FILES["image"]['name'];
								$file_name_rename = $this->insert_with_image($this->data['user']->username);
					            $explode = explode('.', $file_name);
					            if(count($explode) >= 2) {
						            $new_file = $file_name_rename.'.'.$explode[1];
									$config['upload_path'] = "./uploads/images";
									$config['allowed_types'] = "gif|jpg|png";
									$config['file_name'] = $new_file;
									$config['max_size'] = '1024';
									$config['max_width'] = '3000';
									$config['max_height'] = '3000';
									$array['photo'] = $new_file;
									$this->load->library('upload', $config);
									if(!$this->upload->do_upload("image")) {
										$this->data["image"] = $this->upload->display_errors();
										$this->data["subview"] = "parentes/edit";
										$this->load->view('_layout_main', $this->data);
									} else {
										$data = array("upload_data" => $this->upload->data());
										$this->user_m->update_user($array, $id);
										$this->session->set_flashdata('success', $this->lang->line('menu_success'));
										redirect(base_url("parentes/index"));
									}
								} else {
									$this->data["image"] = "Invalid file";
									$this->data["subview"] = "parentes/edit";
									$this->load->view('_layout_main', $this->data);
								}
							} else {
								$this->user_m->update_user($array, $id);
								$this->session->set_flashdata('success', $this->lang->line('menu_success'));
								redirect(base_url("profile/index"));
							}
						}
					} else {
						$this->data["subview"] = "parentes/edit";
						$this->load->view('_layout_main', $this->data);
					}
				} else {
					$this->data["subview"] = "error";
					$this->load->view('_layout_main', $this->data);
				}
			} else {
				$this->data["subview"] = "error";
				$this->load->view('_layout_main', $this->data);
			}
		}

		else {
			$this->data["subview"] = "error";
			$this->load->view('_layout_main', $this->data);
		}
	}
	public function print_preview() {
		$id = htmlentities($this->db->escape_str($this->uri->segment(3)));
		if ((int)$id) {
			$usertype = $this->session->userdata('usertype');
			if ($usertype == "Admin" || $usertype == "Super Admin") {
				$this->load->library('html2pdf');
			    $this->html2pdf->folder('./assets/pdfs/');
			    $this->html2pdf->filename('Report.pdf');
			    $this->html2pdf->paper('a4', 'portrait');

			    $schoolID = $this->session->userdata('schoolID');
				$this->data['user'] = $this->user_m->get_single_user(array("schoolID" => $schoolID, "userID" => $id));
				if($this->data['user']) {
					/*if($this->data['user']->usertype == "Admin" && $this->session->userdata('usertype') == "Admin") {
						redirect("profile/index");
					}*/
					$this->data['panel_title'] = $this->lang->line('panel_title');
					$html = $this->load->view('profile/print_preview', $this->data, true);
					$this->html2pdf->html($html);
					$this->html2pdf->create();
				} else {
					$this->data["subview"] = "error";
					$this->load->view('_layout_main', $this->data);
				}
			} else {
				$this->data["subview"] = "error";
				$this->load->view('_layout_main', $this->data);
			}	
		} else {
			$this->data["subview"] = "error";
			$this->load->view('_layout_main', $this->data);
		}
	}
	public function send_mail() {
		$usertype = $this->session->userdata("usertype");
		$schoolID = $this->session->userdata('schoolID');
		if($usertype == "Admin" || $usertype == "Super Admin") {
			$id = $this->input->post('id');
			if ((int)$id) {
				$this->load->library('html2pdf');
			    $this->html2pdf->folder('uploads/report');
			    $this->html2pdf->filename('Report.pdf');
			    $this->html2pdf->paper('a4', 'portrait');

				$this->data['user'] = $this->user_m->get_single_user(array('schoolID' => $schoolID, 'userID' => $id));
				if($this->data["user"]) {
					$this->data['panel_title'] = $this->lang->line('panel_title');
					$html = $this->load->view('profile/print_preview', $this->data, true);
					$this->html2pdf->html($html);
					$this->html2pdf->create('save');
					
					if($path = $this->html2pdf->create('save')) {
					$this->load->library('email');
					$this->email->set_mailtype("html");
					$this->email->from($this->data["siteinfos"]->email, $this->session->userdata('school'));
					$this->email->to($this->input->post('to'));
					$this->email->subject($this->input->post('subject'));
					$this->email->message($this->input->post('message'));	
					$this->email->attach($path);
						if($this->email->send()) {
							$this->session->set_flashdata('success', $this->lang->line('mail_success'));
						} else {
							$this->session->set_flashdata('error', $this->lang->line('mail_error'));
						}
					}
				} else {
					$this->data["subview"] = "error";
					$this->load->view('_layout_main', $this->data);
				}
			} else {
				$this->data["subview"] = "error";
				$this->load->view('_layout_main', $this->data);
			}
		} else {
			$this->data["subview"] = "error";
			$this->load->view('_layout_main', $this->data);
		}
	}

	public function active(){
		$id = htmlentities($this->db->escape_str($this->uri->segment(3)));
		if((int)$id) {
			$user_info = $this->user_m->get_single_user(array('userID' => $id));
			$tables = array('student' => 'student', 'parent' => 'parent', 'teacher' => 'teacher', 'user' => 'user', 'systemadmin' => 'systemadmin');
			$array = array();
			//$i = 0;
			foreach ($tables as $table) {
				$user = $this->user_m->get_username($table, array("phone" => $this->input->post('phone'), "phone_status !=" => 1 ));
				if(count($user)) {
					$this->form_validation->set_message("lol_username", "%s is not verified");
					//$array['permition'][$i] = 'no';
				} else {
					//$array['permition'][$i] = 'yes';
				}
				//$i++;
			}
		} else {
			$tables = array('student' => 'student', 'parent' => 'parent', 'teacher' => 'teacher', 'user' => 'user', 'systemadmin' => 'systemadmin');
			$array = array();
			//$i = 0;
			foreach ($tables as $table) {
				$user = $this->user_m->get_username($table, array("phone" => $this->input->post('phone'), "phone_status !=" => 1 ));
				if(count($user)) {
					$this->form_validation->set_message("lol_username", "%s is not verified");
					//$array['permition'][$i] = 'no';
				} else {
					//$array['permition'][$i] = 'yes';
				}
				//$i++;
			}

			if(in_array('no', $array['permition'])) {
				return FALSE;
			} else {
				return TRUE;
			}
		}	
	}

	public function lol_username() {
		$id = htmlentities($this->db->escape_str($this->uri->segment(3)));
		if((int)$id) {
			$user_info = $this->user_m->get_single_user(array('userID' => $id));
			$tables = array('student' => 'student', 'parent' => 'parent', 'teacher' => 'teacher', 'user' => 'user', 'systemadmin' => 'systemadmin');
			$array = array();
			$i = 0;
			foreach ($tables as $table) {
				$user = $this->user_m->get_username($table, array("username" => $this->input->post('username'), "email !=" => $user_info->email ));
				if(count($user)) {
					$this->form_validation->set_message("lol_username", "%s already exists");
					$array['permition'][$i] = 'no';
				} else {
					$array['permition'][$i] = 'yes';
				}
				$i++;
			}
			if(in_array('no', $array['permition'])) {
				return FALSE;
			} else {
				return TRUE;
			}
		} else {
			$tables = array('student' => 'student', 'parent' => 'parent', 'teacher' => 'teacher', 'user' => 'user', 'systemadmin' => 'systemadmin');
			$array = array();
			$i = 0;
			foreach ($tables as $table) {
				$user = $this->user_m->get_username($table, array("username" => $this->input->post('username')));
				if(count($user)) {
					$this->form_validation->set_message("lol_username", "%s already exists");
					$array['permition'][$i] = 'no';
				} else {
					$array['permition'][$i] = 'yes';
				}
				$i++;
			}

			if(in_array('no', $array['permition'])) {
				return FALSE;
			} else {
				return TRUE;
			}
		}			
	}

	public function date_valid($date) {
		if(strlen($date) <10) {
			$this->form_validation->set_message("date_valid", "%s is not valid dd-mm-yyyy");
	     	return FALSE;
		} else {
	   		$arr = explode("-", $date);   
	        $dd = $arr[0];            
	        $mm = $arr[1];              
	        $yyyy = $arr[2];
	      	if(checkdate($mm, $dd, $yyyy)) {
	      		return TRUE;
	      	} else {
	      		$this->form_validation->set_message("date_valid", "%s is not valid dd-mm-yyyy");
	     		return FALSE;
	      	}
	    } 
	} 

	public function unique_email() {
		$id = htmlentities($this->db->escape_str($this->uri->segment(3)));
		if((int)$id) {
			$user_info = $this->user_m->get_single_user(array('userID' => $id));
			$tables = array('student' => 'student', 'parent' => 'parent', 'teacher' => 'teacher', 'user' => 'user', 'systemadmin' => 'systemadmin');
			$array = array();
			$i = 0;
			foreach ($tables as $table) {
				$user = $this->user_m->get_username($table, array("email" => $this->input->post('email'), 'username !=' => $user_info->username ));
				if(count($user)) {
					$this->form_validation->set_message("unique_email", "%s already exists");
					$array['permition'][$i] = 'no';
				} else {
					$array['permition'][$i] = 'yes';
				}
				$i++;
			}
			if(in_array('no', $array['permition'])) {
				return FALSE;
			} else {
				return TRUE;
			}
		} else {
			$tables = array('student' => 'student', 'parent' => 'parent', 'teacher' => 'teacher', 'user' => 'user', 'systemadmin' => 'systemadmin');
			$array = array();
			$i = 0;
			foreach ($tables as $table) {
				$user = $this->user_m->get_username($table, array("email" => $this->input->post('email')));
				if(count($user)) {
					$this->form_validation->set_message("unique_email", "%s already exists");
					$array['permition'][$i] = 'no';
				} else {
					$array['permition'][$i] = 'yes';
				}
				$i++;
			}

			if(in_array('no', $array['permition'])) {
				return FALSE;
			} else {
				return TRUE;
			}
		}	
	}
}

/* End of file book.php */
/* Location: .//D/xampp/htdocs/school/mvc/controllers/book.php */