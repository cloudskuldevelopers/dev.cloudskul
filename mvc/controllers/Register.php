<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Register extends CI_Controller {
/*
| -----------------------------------------------------
| PRODUCT NAME: 	CLOUDSKUL
| -----------------------------------------------------
| AUTHOR:			CLOUDSKUL TEAM
| -----------------------------------------------------
| EMAIL:			
| -----------------------------------------------------
| COPYRIGHT:		RESERVED BY CLOUDSKUL
| -----------------------------------------------------
| WEBSITE:			http://cloudskul.com
| -----------------------------------------------------
*/
	function __construct() {
		parent::__construct();
		$this->load->model('register_m');
		$this->load->model('school_m');
		$this->load->model('user_m');
		$this->load->helper("form");
		$this->load->library("email");
		$this->load->library('ozioma');
		$this->load->library("form_validation");
		$this->load->helper("url");	
		$this->load->helper('security');
		$language = $this->session->userdata('lang');
		$this->lang->load('topbar_menu', $language);
	}
	protected function rules() {
		$rules = array(
			array(
				'field' => 'school',
				'label' => 'School Name', 
				'rules' => 'trim|required|xss_clean|max_length[200]|callback_unique_school'
			),
			array(
				'field' => 'alias', 
				'label' => 'School Alias', 
				'rules' => 'trim|required|xss_clean|max_length[4]|callback_unique_alias'
			),
			array(
				'field' => 'email', 
				'label' => 'Email', 
				'rules' => 'trim|required|xss_clean|valid_email|max_length[40]'
			),
			array(
				'field' => 'phone', 
				'label' => 'Phone', 
				'rules' => 'trim|required|xss_clean|min_length[11]|max_length[11]'
			),
			array(
				'field' => 'address', 
				'label' => 'Address', 
				'rules' => 'trim|required|max_length[200]|xss_clean'
			),
			array(
				'field' => 'photo', 
				'label' => 'Photo', 
				'rules' => 'trim|max_length[200]|xss_clean'
			),
			array(
				'field' => 'name', 
				'label' => 'Admin Name', 
				'rules' => 'trim|required|xss_clean|max_length[60]'
			), 
			array(
				'field' => 'referrerID', 
				'label' => 'Referrer ID', 
				'rules' => 'trim|xss_clean|max_length[60]'
			),
			array(
				'field' => 'dob', 
				'label' => 'Date Of Birth',
				'rules' => 'trim|required|max_length[10]|callback_date_valid|xss_clean'
			),
			array(
				'field' => 'sex', 
				'label' => 'Sex', 
				'rules' => 'trim|max_length[10]|xss_clean'
			),
			array(
				'field' => 'username', 
				'label' => 'Username', 
				'rules' => 'trim|required|min_length[4]|max_length[40]|xss_clean|callback_lol_username'
			),
			array(
				'field' => 'password',
				'label' => 'Password', 
				'rules' => 'trim|required|min_length[4]|max_length[40]|min_length[4]|xss_clean'
			)
		);
		return $rules;
	}
	public function index() {
		$this->load->database();
		$this->load->library('session');
		$this->data['form_validation'] = "No";
		$this->data['siteinfos'] = $this->get_admin();
		if($_POST) {
			$rules = $this->rules();
			$this->form_validation->set_rules($rules);
			if ($this->form_validation->run() == FALSE) {
				$this->data['form_validation'] = validation_errors(); 
				$this->data["subview"] = "register/index";
				$this->load->view('_layout_reset', $this->data);			
			} else {
				$array = array();
				$array["school"] = $this->input->post("school");
				$school_sms = $this->input->post("school"); //school name for sms
				$array["email"] = $this->input->post("email");
				$array["phone"] = $this->input->post("phone");
				$array["address"] = $this->input->post("address");
				$array["name"] = $this->input->post("name");
				$array["dob"] = date("Y-m-d", strtotime($this->input->post("dob")));
				$array["referrerID"] = $this->input->post('referrerID');
				$array["alias"] = strtoupper($this->input->post('alias'));
				$array["sex"] = $this->input->post("sex");
				$array["username"] = $this->input->post("username");
				$array["password"] = $this->register_m->hash($this->input->post("password"));
				$array["create_date"] = date("Y-m-d h:i:s");			
				$new_file = "site.png";
				if($_FILES["image"]['name'] !="") {
					$file_name = $_FILES["image"]['name'];
					$file_name_rename = $this->insert_with_image($this->input->post("school"));
		            $explode = explode('.', $file_name);
		            if(count($explode) >= 2) {
			            $new_file = $file_name_rename.'.'.$explode[1];
						$config['upload_path'] = "./uploads/images";
						$config['allowed_types'] = "gif|jpg|png";
						$config['file_name'] = $new_file;
						$config['max_size'] = '1024';
						$config['max_width'] = '3000';
						$config['max_height'] = '3000';
						$array['photo'] = $new_file;
						$this->load->library('upload', $config);
						if(!$this->upload->do_upload("image")) {
							$this->data["image"] = $this->upload->display_errors();
							$this->data["subview"] = "register/index";
							$this->load->view('_layout_reset', $this->data);
						} else {
							$data = array("upload_data" => $this->upload->data());
							$returnID = $this->register_m->insert_school($array);
							$this->send_sms($school_sms); //send sms to cloudskul super admin
							$this->session->set_userdata('success', 'School registration successful!');
							redirect(base_url("register/index"));
						}
					} else {
						$this->data["image"] = "Invalid file";
						$this->data["subview"] = "register/index";
						$this->load->view('_layout_reset', $this->data);
					}
				} else {
					$array["photo"] = $new_file;
					$returnID = $this->register_m->insert_school($array);
					$this->send_sms($school_sms); //send sms to cloudskul super admin
					$this->session->set_flashdata('success', 'School registration successful!');
					redirect(base_url("register/success"));
				}
			}
		} else {
			$this->data["subview"] = "register/index";
			$this->load->view('_layout_signup', $this->data);
		}
	}

	function success(){
		if($this->session->userdata('success') != 'School registration successful!'){
			redirect(base_url('register/index'));
		}
		$this->load->view('register/success');
	}

	function send_sms($school){
		
		$receiver = "Ugi";
		$recipient = "08063986286";
		$message = "Hello ".$receiver.",the following school: ".$school.", just registered on CloudSkul.";
		$message .="Your attention is needed.\n"; 
		$message .="Thank you.";
		$this->ozioma->set_message($message);//message 
        $this->ozioma->set_recipient($recipient);//separate numbers with commas 
        $this->ozioma->set_sender("CloudSkul");//sender 
        $this->ozioma->send();

        $this->email->set_mailtype("html");
		$this->email->from($this->data["siteinfos"]->email, $this->data['siteinfos']->sname);
		//$this->email->to($tmp_school->email);
		$this->email->to('etti.emman@gmail.com,docaustyne@yahoo.co.uk,docaustyn@artty.net');
		$this->email->subject('Welcome');
		$this->email->message($message);	
		//$this->email->attach($path);
		$this->email->send();
	}
	function get_admin() {
		$query = $this->db->get_where('setting', array('SettingID' => 1));
		return $query->row();
	}
	function insert_with_image($username) {
	    $random = rand(1, 10000000000000000);
	    $makeRandom = hash('sha512', $random. $username . config_item("encryption_key"));
	    return $makeRandom;
	}
	public function unique_school() {
		$id = htmlentities($this->db->escape_str($this->uri->segment(3)));
		if((int)$id) {
			$school = $this->school_m->get_order_by_school(array("school" => $this->input->post("school"), "schoolID !=" => $id));
			if(count($school)) {
				$this->form_validation->set_message("unique_school", "%s already exists");
				return FALSE;
			}
			return TRUE;
		} else {
			$school = $this->school_m->get_order_by_school(array("school" => $this->input->post("school")));

			if(count($school)) {
				$this->form_validation->set_message("unique_school", "%s already exists");
				return FALSE;
			}
			return TRUE;
		}	
	}
	
	public function unique_alias() {
		$id = htmlentities($this->db->escape_str($this->uri->segment(3)));
		if((int)$id) {
			$alias = $this->school_m->get_order_by_school_alias($this->input->post("alias"));
			if(count($alias)) {
				$this->form_validation->set_message("unique_alias", "%s already exists");
				return FALSE;
			}
			return TRUE;
		} else {
			$alias = $this->school_m->get_order_by_school_alias($this->input->post("alias"));
			if(count($alias)) {
				$this->form_validation->set_message("unique_alias", "%s already exists");
				return FALSE;
			}
			return TRUE;
		}	
	}
	
	public function lol_username() {
		$id = htmlentities($this->db->escape_str($this->uri->segment(3)));
		if((int)$id) {
			$user_info = $this->user_m->get_single_user(array('userID' => $id));
			$tables = array('student' => 'student', 'parent' => 'parent', 'teacher' => 'teacher', 'user' => 'user', 'systemadmin' => 'systemadmin');
			$array = array();
			$i = 0;
			foreach ($tables as $table) {
				$user = $this->user_m->get_username($table, array("username" => $this->input->post('username'), "email !=" => $user_info->email ));
				if(count($user)) {
					$this->form_validation->set_message("lol_username", "%s already exists");
					$array['permition'][$i] = 'no';
				} else {
					$array['permition'][$i] = 'yes';
				}
				$i++;
			}
			if(in_array('no', $array['permition'])) {
				return FALSE;
			} else {
				return TRUE;
			}
		} else {
			$tables = array('student' => 'student', 'parent' => 'parent', 'teacher' => 'teacher', 'user' => 'user', 'systemadmin' => 'systemadmin');
			$array = array();
			$i = 0;
			foreach ($tables as $table) {
				$user = $this->user_m->get_username($table, array("username" => $this->input->post('username')));
				if(count($user)) {
					$this->form_validation->set_message("lol_username", "%s already exists");
					$array['permition'][$i] = 'no';
				} else {
					$array['permition'][$i] = 'yes';
				}
				$i++;
			}

			if(in_array('no', $array['permition'])) {
				return FALSE;
			} else {
				return TRUE;
			}
		}			
	}
	public function date_valid($date) {
		if(strlen($date) <10) {
			$this->form_validation->set_message("date_valid", "%s is not valid dd-mm-yyyy");
	     	return FALSE;
		} else {
	   		$arr = explode("/", $date);   
	        $dd = $arr[1];            
	        $mm = $arr[0];              
	        $yyyy = $arr[2];
	      	if(checkdate($mm, $dd, $yyyy)) {
	      		return TRUE;
	      	} else {
	      		$this->form_validation->set_message("date_valid", "%s is not valid dd-mm-yyyy");
	     		return FALSE;
	      	}
	    } 
	}

}



/* End of file class.php */
/* Location: .//D/xampp/htdocs/school/mvc/controllers/class.php */