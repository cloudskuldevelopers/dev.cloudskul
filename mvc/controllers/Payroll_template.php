<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Payroll_template extends MY_Controller {
/*
| -----------------------------------------------------
| PRODUCT NAME: 	CloudSkul
| -----------------------------------------------------
| AUTHOR:			CLOUDSKUL TEAM
| -----------------------------------------------------
| EMAIL:			info@cloudskul.com
| -----------------------------------------------------
| COPYRIGHT:		RESERVED BY Cloudskul
| -----------------------------------------------------
| WEBSITE:			http://cloudskul.com
| -----------------------------------------------------
*/

	public function __construct(){
		parent::__construct();
		$this->load->model('payroll_template_m');
		$this->load->library('upload');
	}
	
	public function index(){
		//get user session
		$usertype = $this->session->userdata("usertype");
		$school_id = $this->session->userdata('schoolID');
		
		if($usertype == "Super Admin" || $usertype == "Admin") {
			$this->data["employee_salary_template"] = $this->payroll_template_m->get_all_employee_template($school_id);
			$this->data['subview'] = 'payroll_template/index';
			$this->load->view('_layout_main', $this->data);
		} else {
			//load error view
			$this->data["subview"] = "error";
			$this->load->view('_layout_main', $this->data);
		}
	}
	
	protected function rules(){
		$rules = array(
			array(
				'field' => 'template_name', 
				'rules' => 'trim|required|xss_clean|max_length[60]'
			), 
			array(
				'field' => 'monthly_rate',
				'rules' => 'trim|required|max_length[60]|xss_clean'
			)
		);
		return $rules;
	}
		
	public function add(){
		//get user session
		$usertype = $this->session->userdata("usertype");
		$school_id = $this->session->userdata('schoolID');
		
		if($usertype == "Super Admin" || $usertype == "Admin") {	
			if($_POST) {
				$rules = $this->rules();
				$this->form_validation->set_rules($rules);
				if ($this->form_validation->run() == FALSE) {
					$this->data["subview"] = "payroll_template/add";
					$this->load->view('_layout_main', $this->data);			
				} else {
					//do something
					$template_name = $this->payroll_template_m->get_unique_salary_template_name($school_id, $this->input->post('template_name'));
					if($template_name){
						$this->session->set_flashdata('error', 'template name already exists, please try again');
						redirect(base_url("payroll_template/add"));
					}else{
						//do something
						$employee_payroll_array = array(
							"schoolID" => $school_id,
							"template_name" => $this->input->post("template_name"),
							"overtime_rate" => $this->input->post('overtime_rate'),
							"monthly_rate" => $this->input->post("monthly_rate")
						);
						
						$check_employe_insert_success = $this->payroll_template_m->insert_employee_payroll($employee_payroll_array);
						if($check_employe_insert_success == true){
							$this->session->set_flashdata('success', $this->lang->line('menu_success'));
							redirect(base_url("payroll_template/index"));
						}
					}
				}
			}
			else{
				$this->data["subview"] = "payroll_template/add";
				$this->load->view('_layout_main', $this->data);
			}
		} else {
			//load error view
			$this->data["subview"] = "error";
			$this->load->view('_layout_main', $this->data);
		}
	}
	
	public function delete_employee_payroll($payroll_id){
		$usertype = $this->session->userdata("usertype");
		$school_id = $this->session->userdata('schoolID');
		
		if($usertype == "Super Admin" || $usertype == "Admin") {
			if($payroll_id){
				$delete_payroll_template = $this->payroll_template_m->delete_employee_template($payroll_id, $school_id);
				
				if($delete_payroll_template == true){
					$this->session->set_flashdata('success', $this->lang->line('menu_success'));
					redirect(base_url("payroll_template/index"));
				}else{
					$this->session->set_flashdata('error', 'error was encountered please try again.');
					redirect(base_url("payroll_template/index"));
				}
			}else{
				$this->data["subview"] = "payroll_template/index";
				$this->load->view('_layout_main', $this->data);
			}
		} else {
			//load error view
			$this->data["subview"] = "error";
			$this->load->view('_layout_main', $this->data, TRUE);
		}
	}
	
	public function edit_employee_payroll($payroll_id){
		//get user session
		$usertype = $this->session->userdata("usertype");
		$school_id = $this->session->userdata("schoolID");
		$this->data['employee_payroll_details'] = $this->payroll_template_m->get_single_employee_template($payroll_id, $school_id);
		
		if($usertype == "Super Admin" || $usertype == "Admin") {
			//retrieve all employee_payroll data
			if($_POST){
				$rules = $this->rules();
				$this->form_validation->set_rules($rules);
				if ($this->form_validation->run() == FALSE) {
					$this->data["subview"] = "payroll_template/edit";
					$this->load->view('_layout_main', $this->data);			
				} else {
					$employee_payroll_array = array(
						"schoolID" => $school_id,
						"template_name" => $this->input->post("template_name"),
						"overtime_rate" => $this->input->post('overtime_rate'),
						"monthly_rate" => $this->input->post("monthly_rate")
					);
					
					$check_employee_payroll_update = $this->payroll_template_m->update_employee_payroll($payroll_id, $school_id, $employee_payroll_array);
					if($check_employee_payroll_update == true){
						$this->session->set_flashdata('success', $this->lang->line('menu_success'));
						redirect(base_url("payroll_template/index"));
					}
				}
			}else{
				//load the edit view if session exists
				$this->data["subview"] = "payroll_template/edit";
				$this->load->view('_layout_main', $this->data);
			}
		}else{
			//load error view
			$this->data["subview"] = "error";
			$this->load->view('_layout_main', $this->data, TRUE);
		}
	}
}
?>