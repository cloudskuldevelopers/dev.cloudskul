<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Assignment extends MY_Controller {
/*
| -----------------------------------------------------
| PRODUCT NAME: 	INILABS SCHOOL MANAGEMENT SYSTEM
| -----------------------------------------------------
| AUTHOR:			INILABS TEAM
| -----------------------------------------------------
| EMAIL:			info@inilabs.net
| -----------------------------------------------------
| COPYRIGHT:		RESERVED BY INILABS IT
| -----------------------------------------------------
| WEBSITE:			http://inilabs.net
| -----------------------------------------------------
*/
	function __construct() {
		parent::__construct();
		$this->load->model("assignment_m");
		$this->load->model("assignment_attachment_m");
		$this->load->model('classes_m');
		$this->load->model('section_m');
		$this->load->model('student_m');
		$this->load->model('student_info_m');
		$this->load->model('subject_m');
		$this->load->model('user_m');
		$language = $this->session->userdata('lang');
		$this->lang->load('assignment', $language);	
	}
	public function index() {
		$usertype = $this->session->userdata("usertype");
		$username = $this->session->userdata("username");
		$userID = $this->session->userdata("loginuserID");
		$schoolID = $this->session->userdata('schoolID');
		if($usertype == "Admin" || $usertype == "Super Admin") {
			$id = htmlentities($this->db->escape_str($this->uri->segment(3)));
			if((int)$id) {
				$this->data['set'] = $id;
				$this->data['classes'] = $this->student_m->get_order_by_classes(array("schoolID" => $schoolID));
				//var_dump($this->data['classes']);
				//break;
				$this->data['assignments'] = $this->assignment_m->get_join_assignments($id);
				$this->data["subview"] = "assignment/index";
				$this->load->view('_layout_main', $this->data);
			} else {
				$this->data['classes'] = $this->student_m->get_order_by_classes(array("schoolID" => $schoolID));
				$this->data["subview"] = "assignment/search";
				$this->load->view('_layout_main', $this->data);
			}
		} elseif($usertype == "Teacher") {
			$id = htmlentities($this->db->escape_str($this->uri->segment(3)));
			if((int)$id) {
				$this->data['set'] = $id;
				$this->data['classes'] = $this->student_m->get_order_by_classes(array("schoolID" => $schoolID));
				$this->data['assignments'] = $this->assignment_m->get_join_assignment_teacher($id);
				$this->data["subview"] = "assignment/index";
				$this->load->view('_layout_main', $this->data);
			} else {
				$this->data['classes'] = $this->student_m->get_order_by_classes(array("schoolID" => $schoolID));
				$this->data["subview"] = "assignment/search";
				$this->load->view('_layout_main', $this->data);
			}
		} elseif($usertype == "Student") {
			$student = $this->student_info_m->get_student_info();
			$this->data['set'] = $student->classesID;
			$this->data['assignments'] = $this->student_info_m->get_join_where_assignment($student->classesID, $student->sectionID);
			$this->data["subview"] = "assignment/index";
			$this->load->view('_layout_main', $this->data);
		} else {
			$this->data["subview"] = "error";
			$this->load->view('_layout_main', $this->data);
		}
	}
	public function assignment_list() {
		$classID = $this->input->post('id');
		if((int)$classID) {
			$string = base_url("assignment/index/$classID");
			echo $string;
		} else {
			redirect(base_url("assignment/index"));
		}
	}

	protected function rules() {
		$rules = array(
					array(
						'field' => 'title', 
						'label' => $this->lang->line("assignment_title"), 
						'rules' => 'trim|required|xss_clean|max_length[200]'
					), 
					array(
						'field' => 'description', 
						'label' => $this->lang->line("description"),
						'rules' => 'trim|required'
					),
					array(
						'field' => 'classesID', 
						'label' => $this->lang->line("select_class"),
						'rules' => 'trim|required|xss_clean'
					),
					array(
						'field' => 'sectionID', 
						'label' => $this->lang->line("student_select_section"),
						'rules' => 'trim|xss_clean'
					),
					array(
						'field' => 'subjectID', 
						'label' => $this->lang->line("subject"),
						'rules' => 'trim|required|xss_clean'
					),
					array(
						'field' => 'end_date', 
						'label' => $this->lang->line("assignment_end_date"),
						'rules' => 'trim|required|xss_clean|callback_date_valid|callback_pastdate_check'
					),
				);
		return $rules;
	}

	public function add() {
		$usertype = $this->session->userdata("usertype");
		$year = date("Y");
		if($usertype == "Admin" || $usertype == "Super Admin" || $usertype=="Teacher") {
			$schoolID = $this->session->userdata('schoolID');
			$this->data['classes'] = $this->classes_m->get_order_by_classes(array("schoolID" => $schoolID));
			$this->data['sections'] = $this->section_m->get_order_by_section(array("schoolID" => $schoolID));
			$classesID = $this->input->post("classesID");
			$this->data['set_subject'] = 0;
			if($classesID != 0) {
				$this->data['sections'] = $this->section_m->get_order_by_section(array("schoolID" => $schoolID, "classesID" =>$classesID));
			} else {
				$this->data['sections'] = "empty";
			}
			$this->data['sectionID'] = $this->input->post("sectionID");
			if($classesID != 0) {
				if($usertype == "Admin" || $usertype == "Super Admin") {
					$this->data['subjects'] = $this->subject_m->get_subject_call($classesID);
				} elseif($usertype == "Teacher") {
					$username = $this->session->userdata("username");
					$teacher = $this->user_m->get_username_row("teacher", array('schoolID' => $schoolID, "username" => $username));
					$this->data['subjects'] = $this->subject_m->get_order_by_subject(array('schoolID' => $schoolID, "classesID" => $classesID, "teacherID" => $teacher->teacherID));
				}
			} else {
				$this->data['subjects'] = 0;
			}
			if($_POST) {
				$rules = $this->rules();
				$this->form_validation->set_rules($rules);
				if ($this->form_validation->run() == FALSE) {
					$this->data['form_validation'] = validation_errors(); 
					$this->data["subview"] = "assignment/add";
					$this->load->view('_layout_main', $this->data);			
				} else {
					$array = array(
						"schoolID" => $this->session->userdata('schoolID'),
						"title" => $this->input->post("title"),
						"description" => $this->input->post("description"),
						"classesID" => $this->input->post("classesID"),
						"sectionID" => $this->input->post("sectionID"),
						"subjectID" => $this->input->post("subjectID"),
						"end_date" => date("Y-m-d", strtotime($this->input->post("end_date"))),
						"create_date" => date("Y-m-d h:i:s"),
						"modify_date" => date("Y-m-d h:i:s"),
						"create_userID" => $this->session->userdata('loginuserID'),
						"create_username" => $this->session->userdata('username'),
						"create_usertype" => $this->session->userdata('usertype'),
					);
					$this->assignment_m->insert_assignment($array);
					$this->session->set_flashdata('success', $this->lang->line('successfull_msg'));
					redirect(base_url("assignment/index"));
				}
			} else {
				$this->data["subview"] = "assignment/add";
				$this->load->view('_layout_main', $this->data);
			}
		} else {
			$this->data["subview"] = "error";
			$this->load->view('_layout_main', $this->data);
		}
	}

	public function edit() {
		$usertype = $this->session->userdata("usertype");
		$schoolID = $this->session->userdata('schoolID');
		if($usertype == "Admin" || $usertype == "Super Admin" || "Teacher") {
			$id = htmlentities($this->db->escape_str($this->uri->segment(3)));
			$url = htmlentities($this->db->escape_str($this->uri->segment(4)));
			if((int)$id && (int)$url) {
				$this->data['assignment'] = $this->assignment_m->get_single_assignment(array('schoolID' => $schoolID, 'assignmentID' =>$id));
				if($this->data['assignment']) {
					$classID = $this->data['assignment']->classesID;
					$this->data['subjects'] = $this->assignment_m->get_subject($classID);
					$this->data['classes'] = $this->assignment_m->get_classes();
					$this->data['sections'] = $this->section_m->get_order_by_section(array('schoolID' => $schoolID, "classesID" => $classID));
					$this->data['set'] = $url;
					if($_POST) {
						$rules = $this->rules();
						$this->form_validation->set_rules($rules);
						if ($this->form_validation->run() == FALSE) {
							$this->data["subview"] = "assignment/edit";
							$this->load->view('_layout_main', $this->data);			
						} else {
							$array = array(
								"schoolID" => $this->session->userdata('schoolID'),
								"title" => $this->input->post("title"),
								"description" => $this->input->post("description"),
								"classesID" => $this->input->post("classesID"),
								"sectionID" => $this->input->post("sectionID"),
								"subjectID" => $this->input->post("subjectID"),
								"end_date" => date("Y-m-d", strtotime($this->input->post("end_date"))),
								"modify_date" => date("Y-m-d h:i:s")
							);
							$this->assignment_m->update_assignment($array, $id);
							$this->session->set_flashdata('success', $this->lang->line('menu_success'));
							redirect(base_url("assignment/index/$url"));
						}
					} else {
						$this->data["subview"] = "assignment/edit";
						$this->load->view('_layout_main', $this->data);
					}
				} else {
					$this->data["subview"] = "error";
					$this->load->view('_layout_main', $this->data);
				}
			} else {
				$this->data["subview"] = "error";
				$this->load->view('_layout_main', $this->data);
			}
		} else {
			$this->data["subview"] = "error";
			$this->load->view('_layout_main', $this->data);
		}
	}

	public function view()
	{
		$usertype = $this->session->userdata("usertype");
		$schoolID = $this->session->userdata('schoolID');
		if($usertype == "Admin" || $usertype == "Super Admin") {
			$id = htmlentities($this->db->escape_str($this->uri->segment(3)));
			$classesID = htmlentities($this->db->escape_str($this->uri->segment(4)));
			if((int)$id && (int)$classesID) {
				$this->data['assignment'] = $this->assignment_m->get_single_assignment(array('schoolID' => $schoolID, 'classesID' => $classesID, 'assignmentID' => $id));
				$this->data['assignment_attachment'] = $this->assignment_attachment_m->get_order_by_assignment_attachment(array('schoolID' => $schoolID, 'assignmentID' => $id));
				$this->data['class'] = $this->classes_m->get_single_classes(array("schoolID" => $schoolID, 'classesID' => $classesID));
				$this->data['subject'] = $this->subject_m->get_single_subject(array("schoolID" => $schoolID, 'subjectID' => $this->data['assignment']->subjectID));
				if($this->data['assignment'] && $this->data['class']) {
					$this->data["section"] = $this->section_m->get_section($this->data['assignment']->sectionID);
					$this->data['set'] = $classesID;
					$this->data["subview"] = "assignment/view";
					$this->load->view('_layout_main', $this->data);
				} else {
					redirect(base_url("assignment/index"));
				}
			} else {
				redirect(base_url("assignment/index"));
			}
		} elseif($usertype=="Teacher") {
			$userID = $this->session->userdata('loginuserID');
			$id = htmlentities($this->db->escape_str($this->uri->segment(3)));
			$classesID = htmlentities($this->db->escape_str($this->uri->segment(4)));
			if((int)$id && (int)$classesID) {
				$this->data['assignment'] = $this->assignment_m->get_single_assignment(array('schoolID' => $schoolID, 'classesID' => $classesID, 'assignmentID' => $id, 'create_userID' => $userID));
				$this->data['assignment_attachment'] = $this->assignment_attachment_m->get_order_by_assignment_attachment(array('schoolID' => $schoolID, 'assignmentID' => $id));
				$this->data['class'] = $this->classes_m->get_single_classes(array("schoolID" => $schoolID, 'classesID' => $classesID));
				$this->data['subject'] = $this->subject_m->get_single_subject(array("schoolID" => $schoolID, 'subjectID' => $this->data['assignment']->subjectID));
				if($this->data['assignment'] && $this->data['class']) {
					$this->data["section"] = $this->section_m->get_section($this->data['assignment']->sectionID);
					$this->data['set'] = $classesID;
					$this->data["subview"] = "assignment/view";
					$this->load->view('_layout_main', $this->data);
				} else {
					redirect(base_url("assignment/index"));
				}
			} else {
				redirect(base_url("assignment/index"));
			}
		} elseif($usertype=="Student") {
			$userID = $this->session->userdata('loginuserID');
			$id = htmlentities($this->db->escape_str($this->uri->segment(3)));
			$classesID = htmlentities($this->db->escape_str($this->uri->segment(4)));
			$student = $this->student_info_m->get_student_info();
			if((int)$id && (int)$classesID && ($student->classesID == $classesID)) {
				$this->data['assignment'] = $this->assignment_m->get_single_assignment(array('schoolID' => $schoolID, 'classesID' => $classesID, 'assignmentID' => $id));
				$this->data['class'] = $this->classes_m->get_single_classes(array("schoolID" => $schoolID, 'classesID' => $classesID));
				$this->data['subject'] = $this->subject_m->get_single_subject(array("schoolID" => $schoolID, 'subjectID' => $this->data['assignment']->subjectID));
				$this->data['assignment_attachment'] = $this->assignment_attachment_m->get_order_by_assignment_attachment(array('schoolID' => $schoolID, 'assignmentID' => $id, 'create_userID'=> $userID, 'create_usertype' => $usertype));
				if($this->data['assignment'] && $this->data['class']) {
					$this->data["section"] = $this->section_m->get_section($this->data['assignment']->sectionID);
					$this->data['set'] = $classesID;
					if($_POST) {
						$array = array();
						$array['schoolID'] = $this->session->userdata('schoolID');
						$array['assignmentID'] = $id;
						$array["note"] = $this->input->post("note");
						$array["create_date"] = date("Y-m-d h:i:s");
						$array["modify_date"] = date("Y-m-d h:i:s");
						$array["create_userID"] = $this->session->userdata('loginuserID');
						$array["create_username"] = $this->session->userdata('username');
						$array["create_usertype"] = $this->session->userdata('usertype');
						$array["recevied_status"] = 0;
						// $new_file = "defualt.png";
						if($_FILES["attachment"]['name'] !="") {
							$file_name = $_FILES["attachment"]['name'];
							$file_name_rename = $this->insert_with_image($this->input->post("username"));
				            $explode = explode('.', $file_name);
				            if(count($explode) >= 2) {
					            $new_file = $file_name_rename.'.'.$explode[1];
								$config['upload_path'] = "./uploads/assignment";
								$config['allowed_types'] = "gif|jpg|png|pdf|doc|txt|ppt|docx|csv";
								$config['file_name'] = $new_file;
								$config['max_size'] = '5000';
								$config['max_width'] = '3000';
								$config['max_height'] = '3000';
								$array['attachment'] = $new_file;
								$this->load->library('upload', $config);
								if(!$this->upload->do_upload("attachment")) {
									$this->data["attachment_error"] = $this->upload->display_errors();
									$this->data["subview"] = "assignment/view";
									$this->load->view('_layout_main', $this->data);
								} else {
									$data = array("upload_data" => $this->upload->data());
									$this->assignment_attachment_m->insert_assignment_attachment($array);
									$this->session->set_flashdata('success', $this->lang->line('menu_success'));
									redirect(base_url("assignment/view/$id/$classesID"));
								}
							} else {
								$this->data["attachment_error"] = "Invalid file";
								$this->data["subview"] = "assignment/view";
								$this->load->view('_layout_main', $this->data);
							}
						} else {
							$this->session->set_flashdata('error', $this->lang->line('menu_error'));
							redirect(base_url("assignment/view/$id/$classesID"));
						}
					}
					$this->data["subview"] = "assignment/view";
					$this->load->view('_layout_main', $this->data);
				} else {
					redirect(base_url("assignment/index"));
				}
			} else {
				redirect(base_url("assignment/index"));
			}
		} else {
			$this->data["subview"] = "error";
			$this->load->view('_layout_main', $this->data);
		}
	}
	function insert_with_image($username) {
	    $random = rand(1, 10000000000000000);
	    $makeRandom = hash('sha512', $random. $username . config_item("encryption_key"));
	    return $makeRandom;
	}

	public function delete() {
		$usertype = $this->session->userdata("usertype");
		$schoolID = $this->session->userdata('schoolID');
		if($usertype == "Admin" || $usertype == "Super Admin") {
			$id = htmlentities($this->db->escape_str($this->uri->segment(3)));
			$classesID = htmlentities($this->db->escape_str($this->uri->segment(4)));
			if((int)$id && (int)$classesID) {
				$this->data['assignment'] = $this->assignment_m->get_single_assignment(array('schoolID' => $schoolID, 'classesID' => $classesID, 'assignmentID' => $id));
				if($this->data['assignment']) {
					$this->assignment_m->delete_assignment($id);
					$this->session->set_flashdata('success', $this->lang->line('menu_success'));
					redirect(base_url("assignment/index/$classesID"));
				} else {
					redirect(base_url("assignment/index"));
				}
			} else {
				redirect(base_url("assignment/index"));
			}
		} elseif($usertype == "Teacher") {
			$userID = $this->session->userdata('loginuserID');
			$id = htmlentities($this->db->escape_str($this->uri->segment(3)));
			$classesID = htmlentities($this->db->escape_str($this->uri->segment(4)));
			if((int)$id && (int)$classesID) {
				$this->data['assignment'] = $this->assignment_m->get_single_assignment(array('schoolID' => $schoolID, 'classesID' => $classesID, 'assignmentID' => $id, 'create_userID' => $userID));
				if($this->data['assignment']) {
					$this->assignment_m->delete_assignment($id);
					$this->session->set_flashdata('success', $this->lang->line('menu_success'));
					redirect(base_url("assignment/index/$classesID"));
				} else {
					redirect(base_url("assignment/index"));
				}
			} else {
				redirect(base_url("assignment/index"));
			}
		} else {
			$this->data["subview"] = "error";
			$this->load->view('_layout_main', $this->data);
		}
	}
	public function delete_attachment() {
		$usertype = $this->session->userdata("usertype");
		$schoolID = $this->session->userdata('schoolID');
		if($usertype == "Admin" || $usertype == "Super Admin") {
			$id = htmlentities($this->db->escape_str($this->uri->segment(3)));
			$classesID = htmlentities($this->db->escape_str($this->uri->segment(4)));
			if((int)$id && (int)$classesID) {
				$this->data['attachment'] = $this->assignment_attachment_m->get_single_assignment_attachment(array('schoolID' => $schoolID, 'assignment_attachmentID' => $id));
				if($this->data['attachment']) {
					$assignmentID = $this->data['attachment']->assignmentID;
					$this->assignment_attachment_m->delete_assignment_attachment($id);
					$this->session->set_flashdata('success', $this->lang->line('menu_success'));
					redirect(base_url("assignment/view/$assignmentID/$classesID"));
				} else {
					redirect(base_url("assignment/index"));
				}
			} else {
				redirect(base_url("assignment/index"));
			}
		} elseif($usertype="Student") {
			$userID = $this->session->userdata('loginuserID');
			$id = htmlentities($this->db->escape_str($this->uri->segment(3)));
			$classesID = htmlentities($this->db->escape_str($this->uri->segment(4)));
			if((int)$id && (int)$classesID) {
				$this->data['attachment'] = $this->assignment_attachment_m->get_single_assignment_attachment(array('schoolID' => $schoolID, 'assignment_attachmentID' => $id, 'create_userID' => $userID, 'create_usertype' => $usertype));
				if($this->data['attachment']) {
					$assignmentID = $this->data['attachment']->assignmentID;
					$this->assignment_attachment_m->delete_assignment_attachment($id);
					$this->session->set_flashdata('success', $this->lang->line('menu_success'));
					redirect(base_url("assignment/view/$assignmentID/$classesID"));
				} else {
					$this->session->set_flashdata('error', $this->lang->line('menu_error'));
					redirect(base_url("assignment/view/$assignmentID/$classesID"));
				}
			} else {
				redirect(base_url("assignment/view/$assignmentID/$classesID"));
			}
		} else {
			$this->data["subview"] = "error";
			$this->load->view('_layout_main', $this->data);
		}
	}

	function date_valid($date) {
		if(strlen($date) <10) {
			$this->form_validation->set_message("date_valid", "%s is not valid dd-mm-yyyy");
	     	return FALSE;
		} else {
	   		$arr = explode("/", $date);   
	        $dd = $arr[0];            
	        $mm = $arr[1];              
	        $yyyy = $arr[2];
	      	if(checkdate($mm, $dd, $yyyy)) {
	      		return TRUE;
	      	} else {
	      		$this->form_validation->set_message("date_valid", "%s is not valid dd-mm-yyyy");
	     		return FALSE;
	      	}
	    } 
	} 
	function pastdate_check() {
		$date = strtotime($this->input->post("end_date"));
		$now_date = strtotime(date("Y-m-d"));
		if($date < $now_date) {
			$this->form_validation->set_message("pastdate_check", "The %s field is past");
	     	return FALSE;
		}
		return TRUE;
	}
	// public function print_preview() {
	// 	$usertype = $this->session->userdata("usertype");
	// 	$schoolID = $this->session->userdata('schoolID');
	// 	if($usertype == "Admin" || $usertype == "Super Admin" || $usertype == "Teacher") {
	// 		$id = htmlentities($this->db->escape_str($this->uri->segment(3)));
	// 		if((int)$id) {
	// 			$this->data['notice'] = $this->notice_m->get_single_notice(array('schoolID' => $schoolID, 'noticeID' => $id));
	// 			if($this->data['notice']) {
	// 				$this->load->library('html2pdf');
	// 			    $this->html2pdf->folder('./assets/pdfs/');
	// 			    $this->html2pdf->filename('Report.pdf');
	// 			    $this->html2pdf->paper('a4', 'portrait');
	// 			    $this->data['panel_title'] = $this->lang->line('panel_title');
	// 				$html = $this->load->view('notice/print_preview', $this->data, true);
	// 				$this->html2pdf->html($html);
	// 				$this->html2pdf->create();
	// 			} else {
	// 				$this->data["subview"] = "error";
	// 				$this->load->view('_layout_main', $this->data);
	// 			}
	// 		} else {
	// 			$this->data["subview"] = "error";
	// 			$this->load->view('_layout_main', $this->data);
	// 		}	
	// 	} else {
	// 		$this->data["subview"] = "error";
	// 		$this->load->view('_layout_main', $this->data);
	// 	}
	// }

	// public function send_mail() {
	// 	$usertype = $this->session->userdata("usertype");
	// 	$schoolID = $this->session->userdata('schoolID');
	// 	if($usertype == "Admin" || $usertype == "Super Admin") {
	// 		$id = $this->input->post('id');
	// 		if ((int)$id) {
	// 			$this->data['notice'] = $this->notice_m->get_single_notice(array('schoolID' => $schoolID, 'noticeID' => $id));
	// 			if($this->data['notice']) {

	// 				$this->load->library('html2pdf');
	// 			    $this->html2pdf->folder('uploads/report');
	// 			    $this->html2pdf->filename('Report.pdf');
	// 			    $this->html2pdf->paper('a4', 'portrait');
	// 			    $this->data['panel_title'] = $this->lang->line('panel_title');
	// 				$html = $this->load->view('notice/print_preview', $this->data, true);
	// 				$this->html2pdf->html($html);
	// 				$this->html2pdf->create('save');
					
	// 				if($path = $this->html2pdf->create('save')) {
	// 				$this->load->library('email');
	// 				$this->email->set_mailtype("html");
	// 				$this->email->from($this->data["siteinfos"]->email, $this->session->userdata('school'));
	// 				$this->email->to($this->input->post('to'));
	// 				$this->email->subject($this->input->post('subject'));
	// 				$this->email->message($this->input->post('message'));	
	// 				$this->email->attach($path);
	// 					if($this->email->send()) {
	// 						$this->session->set_flashdata('success', $this->lang->line('mail_success'));
	// 					} else {
	// 						$this->session->set_flashdata('error', $this->lang->line('mail_error'));
	// 					}
	// 				}
	// 			} else {
	// 				$this->data["subview"] = "error";
	// 				$this->load->view('_layout_main', $this->data);
	// 			}
	// 		} else {
	// 			$this->data["subview"] = "error";
	// 			$this->load->view('_layout_main', $this->data);
	// 		}
	// 	} else {
	// 		$this->data["subview"] = "error";
	// 		$this->load->view('_layout_main', $this->data);
	// 	}
	// }

}

/* End of file assignment.php */
/* Location: .//D/xampp/htdocs/school/mvc/controllers/assignment.php */