<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class New_invoice extends MY_Controller {
/*
| -----------------------------------------------------
| PRODUCT NAME: 	CloudSkul
| -----------------------------------------------------
| AUTHOR:			CLOUDSKUL TEAM
| -----------------------------------------------------
| EMAIL:			info@cloudskul.com
| -----------------------------------------------------
| COPYRIGHT:		RESERVED BY Cloudskul
| -----------------------------------------------------
| WEBSITE:			http://cloudskul.com
| -----------------------------------------------------
*/
	function __construct() {
		parent::__construct();
		$this->load->model("invoice_m");
		$this->load->model("new_invoice_m");
		$this->load->model("bankaccount_m"); //bank acount model
		$this->load->model("instalments_m"); //instalments model
		$this->load->model("feetype_m");
		$this->load->model('payment_m');
		$this->load->model("classes_m");
		$this->load->model("student_m");
		$this->load->model("parentes_m");
		$this->load->model("section_m");
		$this->load->model("school_m");
		$this->load->model('user_m');
		$this->load->model("payment_settings_m");
		$this->load->library("remita");
		$this->load->library("ozioma");
		$language = $this->session->userdata('lang');
		$this->lang->load('invoice', $language);
	}

	public function index() {
		$usertype = $this->session->userdata("usertype");
		$schoolID = $this->session->userdata('schoolID');
		if($usertype == "Admin" || $usertype == "Super Admin" || $usertype == "Accountant") {
			$this->data['invoices'] = $this->new_invoice_m->get_order_by_invoice(array('schoolID' => $schoolID));
			$this->data["subview"] = "new_invoice/index"; //made change ~ etti
			$this->load->view('_layout_main', $this->data);
		} elseif($usertype == "Student") {
			$username = $this->session->userdata("username");
			$student = $this->student_m->get_single_student(array('schoolID' => $schoolID, "username" => $username));
			$this->data['invoices'] = $this->new_invoice_m->get_order_by_invoice(array('schoolID' => $schoolID, 'studentID' => $student->studentID));
			$this->data["subview"] = "new_invoice/index";
			$this->load->view('_layout_main', $this->data);
		} elseif($usertype == "Parent") {
			
			$username = $this->session->userdata("username");
			$parent = $this->parentes_m->get_single_parentes(array('schoolID' => $schoolID, 'username' => $username));
			$this->data['students'] = $this->student_m->get_order_by_student(array('schoolID' => $schoolID, 'parentID' => $parent->parentID));
			$id = htmlentities($this->db->escape_str($this->uri->segment(3)));
			if((int)$id) {
				$checkstudent = $this->student_m->get_single_student(array('schoolID' => $schoolID, 'studentID' => $id));
				if(count($checkstudent)) {
					if($checkstudent->parentID == $parent->parentID) {
						$classesID = $checkstudent->classesID;
						$this->data['set'] = $id;
						$this->data['invoices'] = $this->new_invoice_m->get_order_by_invoice(array('schoolID' => $schoolID, 'studentID' => $id));
						$this->data["subview"] = "new_invoice/index_parent";
						$this->load->view('_layout_main', $this->data); 
					} else {
						$this->data["subview"] = "error";
						$this->load->view('_layout_main', $this->data);
					}
				} else {
					$this->data["subview"] = "error";
					$this->load->view('_layout_main', $this->data);
				}
			} else {
				$this->data["subview"] = "new_invoice/search_parent";
				$this->load->view('_layout_main', $this->data);
			}
		} else {
			$this->data["subview"] = "error";
			$this->load->view('_layout_main', $this->data);
		}
	}
	
	private function calculateInstallmentAmount($amount1, $amount2, $amount3, $noOfInstallments, $installmentRef, $studentID){
		$usertype = $this->session->userdata("usertype");
		if($usertype == "Parent" || $usertype == "Admin" || $usertype == "Super Admin"){
			
			//$returnArray = array();
			//check to see the selected checkboxes
			
			if(isset($_POST['installment1']) && $noOfInstallments == 1){
				$amountPaid = $amount1;
				return array("installment_desc" => 'Complete Payment', "amount_paid" => $amountPaid, "selected_installments" => '1');
			}
			
			if(isset($_POST['installment1']) && isset($_POST['installment2']) && isset($_POST['installment3'])){
				
				$amountPaid = $amount1 + $amount2 + $amount3;
				return array("installment_desc" => 'Complete Payment', "amount_paid" => $amountPaid, "selected_installments" => '123');
				
			}
				
			if(isset($_POST['installment1']) && isset($_POST['installment2']) && $noOfInstallments == 2){
				
				$amountPaid = $amount1 + $amount2;
				return array("installment_desc" => 'Complete Payment', "amount_paid" => $amountPaid, "selected_installments" => '12');
				
			}elseif(isset($_POST['installment1']) && isset($_POST['installment2']) && $noOfInstallments == 3){
				
				$amountPaid = $amount1 + $amount2;
				return array("installment_desc" => '1st and 2nd Installment Payment', "amount_paid" => $amountPaid, "selected_installments" => '12');
				
			}
				
			if(isset($_POST['installment2']) && isset($_POST['installment3'])){
				
				$amountPaid = $amount2 + $amount3;
				return array("installment_desc" => '2nd and 3rd Installment Payment', "amount_paid" => $amountPaid, "selected_installments" => '23');
				
			}
				
			if(isset($_POST['installment1']) && isset($_POST['installment3'])){
				
				$amountPaid = $amount1 + $amount3;
				return array("installment_desc" => '1st and 3rd Installment Payment', "amount_paid" => $amountPaid, "selected_installments" => '13');
				
			}
				
			if(isset($_POST['installment1']) && $noOfInstallments == 2){
				
				$amountPaid = $amount1;
				return array("installment_desc" => '1st Installment Payment', "amount_paid" => $amountPaid, "selected_installments" => '1');
				
			}elseif(isset($_POST['installment1']) && $noOfInstallments == 3){
				
				$amountPaid = $amount1;
				return array("installment_desc" => '1st Installment Payment', "amount_paid" => $amountPaid, "selected_installments" => '1');
				
			}elseif(isset($_POST['installment1']) && $noOfInstallments == 1){
				
				$amountPaid = $amount1;
				return array("installment_desc" => 'Complete Payment', "amount_paid" => $amountPaid, "selected_installments" => '1');
				
			}
				
			if(isset($_POST['installment2'])){
				
				$amountPaid = $amount2;
				return array("installment_desc" => '2nd Installment Payment', "amount_paid" => $amountPaid, "selected_installments" => '2');
				
			}
				
			if(isset($_POST['installment3'])){
				
				$amountPaid = $amount3;
				return array("installment_desc" => '3rd Installment Payment', "amount_paid" => $amountPaid, "selected_installments" => '3');
				
			}
			
			if(!isset($_POST['installment1']) && !isset($_POST['installment2']) && !isset($_POST['installment3'])){
				
				$this->session->set_flashdata('error', "No Installmental Plan Selected");
				redirect(base_url("invoice/installments/1/" . $installmentRef . '/' . $studentID));
				
			}
			
		}else {
			$this->data["subview"] = "error";
			$this->load->view('_layout_main', $this->data);
		}
	}
	
	private function getPaidInstallments($combinedSelected, $noOfInstallments){
		if($combinedSelected == '123' && $noOfInstallments == 3){
			return array("first_installment" => "paid", "second_installment" => "paid", "third_installment" => "paid");
		}
		
		if($combinedSelected == '12' && $noOfInstallments == 3){
			return array("first_installment" => "paid", "second_installment" => "paid");
		}
		
		if($combinedSelected == '23' && $noOfInstallments == 3){
			return array("second_installment" => "paid", "third_installment" => "paid");
		}
		
		if($combinedSelected == '1' && $noOfInstallments == 3){
			return array("first_installment" => "paid");
		}
		
		if($combinedSelected == '2' && $noOfInstallments == 3){
			return array("second_installment" => "paid");
		}
		
		if($combinedSelected == '3' && $noOfInstallments == 3){
			return array("third_installment" => "paid");
		}
		
		if($combinedSelected == '12' && $noOfInstallments == 2){
			return array("first_installment" => "paid", "second_installment" => "paid");
		}
		
		if($combinedSelected == '1' && $noOfInstallments == 2){
			return array("first_installment" => "paid");
		}
		
		if($combinedSelected == '2' && $noOfInstallments == 2){
			return array("second_installment" => "paid");
		}
		
		if($combinedSelected == '1' && $noOfInstallments == 1){
			return array("first_installment" => "paid");
		}
	}
	
	private function checkInstallmentPayment($ref, $noOfInstallments, $schoolID, $studentID){
		
		//use the installment ref to get the paid installments
		//get the selected installments for payment
		//
		
		
		//if the response from approved table has no value,
		//then installment 1 must be selected
		
		//if the response is null n num of installments is 3, or 2 or 1
		// then check is installment 1 is selected
		
		//also check if 3 was selected n 2 not selected
		
		//if response is null and num of installment is 3 and 1 and 3 is set, return num 2 error
		//if response is null n 
		
		//if response is 1 and num of installment is 3 and 2 is not set, then return 2 error
		
		
		//use the installment ref to get the selected installments 
		$getAllSelectedInstallment = $this->invoice_m->getApprovedSelectedInstallment($ref, $schoolID, $studentID);
		
		if(!empty($getAllSelectedInstallment)){
			
			foreach($getAllSelectedInstallment as $theInstalls => $theVal){
				$getSelectedInstallment = $theVal['selected_installments'];
			}
			
			if($getSelectedInstallment == '1' && $noOfInstallments == 3 && !isset($_POST['installment2'])){
				return array("status" => "fail", "error" => "You haven't paid for second installment yet o.");
			}
			
		}else{
			
			if((empty($getSelectedInstallment) && $noOfInstallments == 3 && !isset($_POST['installment1'])) || (empty($getSelectedInstallment) && $noOfInstallments == 2 && !isset($_POST['installment1'])) || (empty($getSelectedInstallment) && $noOfInstallments == 1 && !isset($_POST['installment1'])) ){
				return array("status" => "fail", "error" => "You haven't paid for first installment yet.");
			}
			
			if(empty($getSelectedInstallment) && $noOfInstallments == 3 && isset($_POST['installment1']) && isset($_POST['installment3']) && !isset($_POST['installment2']) ){
				return array("status" => "fail", "error" => "You haven't paid for second installment yet.");
			}
			
		}
		
	}
	
	/*
	* Description: Instalments View
	* Dependencies: new_invoice_m {Model}
	* 15.03.2015
	* E.Etti
	*/
	public function instalments(){
		$usertype = $this->session->userdata("usertype");
		$schoolID = $this->session->userdata('schoolID');
		
		if($usertype == "Parent" || $usertype == "Admin" || $usertype == "Super Admin" || $usertype == "Student"){
			$id = htmlentities($this->db->escape_str($this->uri->segment(3)));
			$inst_id = htmlentities($this->db->escape_str($this->uri->segment(4)));
			$this->data['invoice'] = $this->new_invoice_m->get_single_invoice(array('schoolID' => $schoolID, 'invoiceID' => $id));
			$this->data['invoice_details'] = $this->new_invoice_m->get_join_instalments($schoolID,$id,$inst_id);
			$this->data["subview"] = "new_invoice/instalments";
			$this->load->view('_layout_main', $this->data);
		}else {
			$this->data["subview"] = "error";
			$this->load->view('_layout_main', $this->data);
		}
	}
	
	protected function rules() {
		$rules = array(
				array(
					'field' => 'classesID', 
					'label' => $this->lang->line("invoice_classesID"), 
					'rules' => 'trim|required|xss_clean|max_length[11]|numeric|callback_unique_classID'
				),
				array(
					'field' => 'studentID', 
					'label' => $this->lang->line("invoice_studentID"), 
					'rules' => 'trim|required|xss_clean|max_length[11]|numeric'
				),
				array(
					'field' => 'feetype', 
					'label' => $this->lang->line("invoice_feetype"), 
					'rules' => 'trim|required|xss_clean|max_length[128]'
				),
				array(
					'field' => 'amount',
					'label' => $this->lang->line("invoice_amount"), 
					'rules' => 'trim|required|xss_clean|max_length[10]|numeric|callback_valid_number|callback_correctInstalment'
				),
				array(
					'field' => 'account',
					'label' => $this->lang->line("invoice_account"), 
					'rules' => 'trim|required|xss_clean|max_length[10]|numeric'
				),
				array(
					'field' => 'date', 
					'label' => $this->lang->line("invoice_date"), 
					'rules' => 'trim|required|xss_clean|max_length[10]|callback_date_valid'
				),

			);
		return $rules;
	}

	protected function payment_rules() {
		$rules = array(
				array(
					'field' => 'amount', 
					'label' => $this->lang->line("invoice_amount"), 
					'rules' => 'trim|required|xss_clean|max_length[11]|numeric|callback_valid_number'
				),
				array(
					'field' => 'payment_method', 
					'label' => $this->lang->line("invoice_paymentmethod"), 
					'rules' => 'trim|required|xss_clean|max_length[11]|callback_unique_paymentmethod'
				)
			);
		return $rules;
	}

	public function payment_type(){

		$type = $this->input->post('type');
		$status = $this->input->post('status');

		if($status != 3){

			if($type !== ''){
				if($type == "UPL" || $type == "Interswitch"){
					echo "1500.00";

				}elseif($type == 'BANK_BRANCH'){
					echo "1500.00";
				}
			}else
			echo "Payment type not selected";
		}else{
			echo "1500.00";
		}
	}

	/*
	*Added my sweet version of instalments
	*The Amount gets split into various parts
	*and get inserted as individual records
	*/
	public function add() {
		$usertype = $this->session->userdata("usertype");
		$schoolID = $this->session->userdata('schoolID');
		$id = htmlentities($this->db->escape_str($this->uri->segment(3)));
		if($usertype == "Admin" || $usertype == "Super Admin" || $usertype == "Accountant") {
			$this->data['classes'] = $this->classes_m->get_order_by_classes(array('schoolID' => $schoolID));  //get all available classes
			$this->data['feetypes'] = $this->feetype_m->get_order_by_feetype(array('schoolID' => $schoolID)); //fee types
			$this->data['feecat'] = $this->feetype_m->feeCategory(); //fee category
			// $this->data['accounts'] = $this->bankaccount_m->get_order_by_school(array("schoolID"=>$schoolID,"accountID"=>$id)); //get bank accounts
			$this->data['accounts'] = $this->bankaccount_m->get_order_by_school(array("schoolID"=>$schoolID)); //get bank accounts
			$classesID = $this->input->post("classesID"); //get selected class from form input
			if($classesID != 0) {
				$this->data['students'] = $this->student_m->get_order_by_student(array('schoolID' => $schoolID, "classesID" => $classesID)); 
				//get all students in a particular class
			} else {
				$this->data['students'] = "empty";
			}
			$this->data['studentID'] = 0;
			if($_POST) {
				$this->data['studentID'] = $this->input->post('studentID'); 
				$instalment = (int)$id;
				$rules = $this->rules();
				$this->form_validation->set_rules($rules);
				if ($this->form_validation->run() == FALSE) {
					$this->data['form_validation'] = validation_errors(); 
					$this->data["subview"] = "new_invoice/add";
					$this->load->view('_layout_main', $this->data);			
				} else {
					//check instalments and process the input fields
					switch ($instalment) {
						case '1':
							$totalamount = $this->input->post("amount");
							$instalment1 = $this->input->post("amount");
							$ins_array = [$instalment1];
							break;

						case '2':
							$totalamount = $this->input->post("amount");
							$instalment1 = $this->input->post("amount1");
							$instalment2 = $this->input->post("amount2");
							$ins_array = [$instalment1,$instalment2];
							break;

						case '3':
							$totalamount = $this->input->post("amount");
							$instalment1 = $this->input->post("amount1");
							$instalment2 = $this->input->post("amount2");
							$instalment3 = $this->input->post("amount3");
							$ins_array = [$instalment1,$instalment2,$instalment3];
							break;
						
						default:
							redirect(base_url("new_invoice/index"));
							break;
					}

					if($this->input->post('studentID')) {
						//if a certain student was selected

						$classesID = $this->input->post('classesID');
						$getclasses = $this->classes_m->get_single_classes(array('schoolID' => $schoolID, 'classesID' => $classesID)); //get class details
						$studentID = $this->input->post('studentID'); 
						$getstudent = $this->student_m->get_single_student(array('schoolID' => $schoolID, 'studentID' => $studentID)); //get selected student details
						$getparent = $this->parentes_m->get_single_parentes(array('schoolID' => $schoolID, 'parentID' => $getstudent->parentID)); //get parent details for selected student
						$getschool = $this->school_m->get_school($schoolID); //get school details
						$amount = $this->input->post("amount"); //get amount
						//$timesammp=DATE("dmyHis");
						//$code  = 'CS';
						//$code .= substr(number_format(time() * rand(),0,'',''),0,6);
						
						$array = array(
							'schoolID' => $this->session->userdata('schoolID'),
							'classesID' => $classesID,
							'classes' => $getclasses->classes,
							'invoice_ref' => $this->InvCode($schoolID,$classesID),
							'instalment_plan' => $instalment,
							'accountID' => $this->input->post("account"),
							'feecategory' => $this->input->post('feecategory'),
							'studentID' => $studentID,
							'service_charge_info' => $getschool->service_charge,
							'student' => $getstudent->name,
							'roll' => $getstudent->roll,
							'feetype' => $this->input->post("feetype"),
							'amount' => $amount,
							'status' => 0,
							'date' => date("Y-m-d", strtotime($this->input->post("date"))),
							'year' => date('Y'),
							'create_date' => date("Y-m-d h:i:s"),
							'modify_date' => date("Y-m-d h:i:s"),
							'create_userID' => $this->session->userdata('loginuserID'),
							'create_username' => $this->session->userdata('username'),
							'create_usertype' => $this->session->userdata('usertype')

						);

						/*
						//sms details 
						$amount = number_format(floatval($amount),2);
						$message = "An invoice for this term fees has been generated ";
						$message .="for your WARD ".$getstudent->name." with code ".$code.","; 
						$message .=" Amount:N".$amount.". Kindly Pay at http://www.cloudskul.com/payments. Thank You";

						//ozioma library implementation
						$this->ozioma->set_message($message);//message from textfield
			            $this->ozioma->set_recipient($getparent->phone);//separate numbers with commas and include zip code in every number
			            $this->ozioma->set_sender($getschool->school);//sender from database
			            $this->ozioma->send();
			            */
						$oldamount = $getstudent->totalamount;
						$nowamount = $oldamount+$amount;

						$this->student_m->update_student(array('totalamount' => $nowamount), $getstudent->studentID); //update total amount student has paid
						$returnID = $this->new_invoice_m->insert_invoice($array); //return ID for current inserted row

						//calculate instalments
						foreach ($ins_array as $i => $value) {
							$instalArr[$i]["amount"] = $value;//$ins_array[$i];
							$instalArr[$i]["pay_seq"] = $i+1; //order of payment
							$instalArr[$i]["invoiceID"] = $returnID;
							$instalArr[$i]["schoolID"] = $schoolID;
							$instalArr[$i]["inst_ref"] = $this->randomRef();
						}
						$this->instalments_m->insert_multiple($instalArr); //insert multi array of instalments
						// echo "<pre>";
						// var_dump($instalArr);
						// echo "</pre>";
						// die;

						$this->session->set_flashdata('success', $this->lang->line('menu_success'));
					 	redirect(base_url("new_invoice/view/$returnID"));
					} else {
						$classesID = $this->input->post('classesID');
						$getclasses = $this->classes_m->get_single_classes(array('schoolID' => $schoolID, 'classesID' => $classesID));
						$getstudents = $this->student_m->get_order_by_student(array('schoolID' => $schoolID, "classesID" => $classesID));
						$amount = $this->input->post("amount");
						$code  = 'CS';
						
						$getschool = $this->school_m->get_school($schoolID);
						foreach ($getstudents as $key => $getstudent) {
							$code .= substr(number_format(time() * rand(),0,'',''),0,6); //autogenerated code
							$getparent = $this->parentes_m->get_single_parentes(array('schoolID' => $schoolID, 'parentID' => $getstudent->parentID));
							$array = array(
								'schoolID' => $this->session->userdata('schoolID'),
								'classesID' => $classesID,
								'classes' => $getclasses->classes,
								'invoice_ref' => $this->InvCode($schoolID,$classesID),
								'instalment_plan' => $instalment,
								'accountID' => $this->input->post("account"),
								'studentID' => $getstudent->studentID,
								'student' => $getstudent->name,
								'roll' => $getstudent->roll,
								'feetype' => $this->input->post("feetype"),
								'feecategory' => $this->input->post('feecategory'),
								'amount' => $amount,
								'status' => 0,
								'date' => date("Y-m-d", strtotime($this->input->post("date"))),
								'year' => date('Y'),
								'create_date' => date("Y-m-d h:i:s"),
								'modify_date' => date("Y-m-d h:i:s"),
								'create_userID' => $this->session->userdata('loginuserID'),
								'create_username' => $this->session->userdata('username'),
								'create_usertype' => $this->session->userdata('usertype')
							);


							$amount = number_format(floatval($amount),2);
							$message = "An invoice for this term fees has been generated ";
							$message .="for your WARD ".$getstudent->name." with code ".$code.","; 
							$message .=" Amount:N".$amount.". Kindly Pay at http://www.cloudskul.com/payments. Thank You";
							$this->ozioma->set_message($message);//message from textfield
				            $this->ozioma->set_recipient($getparent->phone);//separate numbers with commas and include zip code in every number
				            $this->ozioma->set_sender($getschool->school);//sender from database
				            $this->ozioma->send();
				            $amount = $this->input->post("amount");//re initialize variable
				            $code = 'CS';//re initialize variable

							$oldamount = $getstudent->totalamount; //1
							$nowamount = $oldamount+$amount;       //1

							//var_dump($message);
							// $this->student_m->update_student(array('totalamount' => $nowamount), $getstudent->studentID); //1
							// $this->invoice_m->insert_invoice($array); //1


							$this->student_m->update_student(array('totalamount' => $nowamount), $getstudent->studentID); //update total amount student has paid 2
							$returnID = $this->new_invoice_m->insert_invoice($array); //return ID for current inserted row 2

							//calculate instalments
							foreach ($ins_array as $i => $value) {
								$instalArr[$i]["amount"] = $value;//$ins_array[$i];
								$instalArr[$i]["pay_seq"] = $i+1; //order of payment
								$instalArr[$i]["invoiceID"] = $returnID;
								$instalArr[$i]["schoolID"] = $schoolID;
								$instalArr[$i]["inst_ref"] = $this->randomRef();
							}
							$this->instalments_m->insert_multiple($instalArr); //insert multi array of instalments

							
						}
						$this->session->set_flashdata('success', $this->lang->line('menu_success'));
					 	redirect(base_url("new_invoice/index"));
					}
				}
			} else {
				$this->data["subview"] = "new_invoice/add";
				$this->load->view('_layout_main', $this->data);
			}
		} else {
			$this->data["subview"] = "error";
			$this->load->view('_layout_main', $this->data);
		}
	}

	public function getFeeType(){
		$usertype = $this->session->userdata("usertype");
		
		if($usertype == "Admin" || $usertype == "Super Admin" || $usertype == "Accountant") {

			$category =  $_POST['category'];

			$this->data['feetypes'] = $this->invoice_m->getFeeTypesByCategory($category);
			$this->load->view('invoice/feetypes', $this->data);
		}
	}
	
	public function edit() {
		$usertype = $this->session->userdata("usertype");
		$schoolID = $this->session->userdata('schoolID');
		if($usertype == "Admin" || $usertype == "Super Admin" || $usertype == "Accountant") {
			$id = htmlentities($this->db->escape_str($this->uri->segment(3)));
			if((int)$id) {
				$this->data['invoice'] = $this->new_invoice_m->get_single_invoice(array('schoolID' => $schoolID, 'invoiceID' => $id));
				$this->data['classes'] = $this->classes_m->get_order_by_classes(array('schoolID' => $schoolID));
				$this->data['feetypes'] = $this->feetype_m->get_order_by_feetype(array('schoolID' => $schoolID)); //fee types
				$this->data['feecat'] = $this->feetype_m->feeCategory(); //fee category
				$this->data['accounts'] = $this->bankaccount_m->get_order_by_school(array("schoolID"=>$schoolID)); //get bank accounts
				
				if($this->data['invoice']) {

					if($this->data['invoice']->classesID != 0) {
						$this->data['students'] = $this->student_m->get_order_by_student(array('schoolID' => $schoolID, "classesID" => $this->data['invoice']->classesID));
					} else {
						$this->data['students'] = "empty";
					}
					$this->data['studentID'] = $this->data['invoice']->studentID;

					if($_POST) {
						$this->data['studentID'] = $this->input->post('studentID');
						$rules = $this->rules();
						$this->form_validation->set_rules($rules);
						if ($this->form_validation->run() == FALSE) {
							$this->data["subview"] = "new_invoice/edit";
							$this->load->view('_layout_main', $this->data);			
						} else {
							$status = 0;
							$oldstudent = $this->student_m->get_single_student(array('schoolID' => $schoolID, 'studentID' => $this->data['invoice']->studentID));
							$osoldamount = $oldstudent->totalamount;
							$oldnowamount = ($osoldamount)-($this->data['invoice']->amount);
							$this->student_m->update_student(array('totalamount' => $oldnowamount), $oldstudent->studentID);
							$classesID = $this->input->post('classesID');
							$getclasses = $this->classes_m->get_single_classes(array('schoolID' => $schoolID, 'classesID' => $classesID));
							$studentID = $this->input->post('studentID');
							$getstudent = $this->student_m->get_single_student(array('schoolID' => $schoolID, 'studentID' => $studentID));
							$amount = $this->input->post("amount");

							if(empty($this->data['invoice']->paidamount)) {
								$status = 0;
							} elseif($this->data['invoice']->paidamount == $amount) { 
								$status = 2;
							} else {
								$status = 1;
							}

							$array = array(
								'classesID' => $classesID,
								'classes' => $getclasses->classes,
								'studentID' => $studentID,
								'student' => $getstudent->name,
								'roll' => $getstudent->roll,
								'feetype' => $this->input->post("feetype"),
								'amount' => $amount,
								'status' => $status,
								'modify_date' => date("Y-m-d h:i:s")
							);
							$oldamount = $getstudent->totalamount;
							$nowamount = $oldamount+$amount;

							$this->student_m->update_student(array('totalamount' => $nowamount), $getstudent->studentID);
							$this->invoice_m->update_invoice($array, $id);
							$this->session->set_flashdata('success', $this->lang->line('menu_success'));
						 	redirect(base_url("new_invoice/index"));

						}
					} else {
						$this->data["subview"] = "new_invoice/edit";
						$this->load->view('_layout_main', $this->data);
					}				
				} else {
					$this->data["subview"] = "error";
					$this->load->view('_layout_main', $this->data);
				}
			} else {
				$this->data["subview"] = "error";
				$this->load->view('_layout_main', $this->data);
			}
		} else {
			$this->data["subview"] = "error";
			$this->load->view('_layout_main', $this->data);
		}
	}

	public function delete() {
		$usertype = $this->session->userdata("usertype");
		$schoolID = $this->session->userdata('schoolID');
		if($usertype == "Admin" || $usertype == "Super Admin" || $usertype == "Accountant") {
			$id = htmlentities($this->db->escape_str($this->uri->segment(3)));
			if((int)$id) {
				$this->data['invoice'] = $this->new_invoice_m->get_single_invoice(array('schoolID' => $schoolID, 'invoiceID' => $id));
				if($this->data['invoice']) {
					$oldstudent = $this->student_m->get_single_student(array('schoolID' => $schoolID, 'studentID' => $this->data['invoice']->studentID));
					$osoldamount = $oldstudent->totalamount;
					$oldnowamount = ($osoldamount)-($this->data['invoice']->amount);
					$this->student_m->update_student(array('totalamount' => $oldnowamount), $oldstudent->studentID);
					$this->new_invoice_m->delete_invoice($id);
					//We would also need to delete the instalments that were created
					//This was accomplished with Foreign key Check
					//On delete Cascade
					$this->session->set_flashdata('success', $this->lang->line('menu_success'));
					redirect(base_url('new_invoice/index'));
				} else {
					redirect(base_url('new_invoice/index'));
				}
			} else {
				$this->data["subview"] = "error";
				$this->load->view('_layout_main', $this->data);
			}
		} else {
			$this->data["subview"] = "error";
			$this->load->view('_layout_main', $this->data);
		}
	}

	public function view() {
		$usertype = $this->session->userdata("usertype");
		$schoolID = $this->session->userdata('schoolID');
		if($usertype == "Admin" || $usertype == "Super Admin" || $usertype == "Accountant") {
			$id = htmlentities($this->db->escape_str($this->uri->segment(3)));
			//$installmentStatusID = htmlentities($this->db->escape_str($this->uri->segment(4)));
			if((int)$id) {
				$this->data["invoice"] = $this->new_invoice_m->get_single_invoice(array('schoolID' => $schoolID, 'invoiceID' => $id));
				$this->data["invoiceData"] = $this->instalments_m->get_order_by_instalment(array('invoiceID'=>$this->data["invoice"]->invoiceID));
				//var_dump($this->data["invoiceData"]);
				//die;
				if($this->data["invoice"]) {
					$this->data["student"] = $this->student_m->get_single_student(array('schoolID' => $schoolID, 'studentID' =>  $this->data["invoice"]->studentID));
					$this->data["subview"] = "new_invoice/view";
					$this->load->view('_layout_main', $this->data);
				} else {
					$this->data["subview"] = "error";
					$this->load->view('_layout_main', $this->data);
				}
			} else {
				$this->data["subview"] = "error";
				$this->load->view('_layout_main', $this->data);
			}
		} elseif($usertype == "Student") {
			$id = htmlentities($this->db->escape_str($this->uri->segment(3)));
			if((int)$id) {
				$username = $this->session->userdata("username");
				$getstudent = $this->student_m->get_single_student(array('schoolID' => $schoolID, "username" => $username));
				$this->data["invoice"] = $this->new_invoice_m->get_single_invoice(array('schoolID' => $schoolID, 'invoiceID' => $id));
				if($this->data['invoice'] && ($this->data['invoice']->studentID == $getstudent->studentID)) {
					$this->data["invoiceData"] = $this->instalments_m->get_order_by_instalment(array('invoiceID'=>$this->data["invoice"]->invoiceID));
					$this->data["student"] = $this->student_m->get_single_student(array('schoolID' => $schoolID, 'studentID' => $this->data["invoice"]->studentID));
					$this->data["subview"] = "new_invoice/view";
					$this->load->view('_layout_main', $this->data);
				} else {
					$this->data["subview"] = "error";
					$this->load->view('_layout_main', $this->data);
				}
			} else {
				$this->data["subview"] = "error";
				$this->load->view('_layout_main', $this->data);
			}
		} elseif($usertype == "Parent") {
			$id = htmlentities($this->db->escape_str($this->uri->segment(3)));
			$installmentStatusID = htmlentities($this->db->escape_str($this->uri->segment(4)));
			$inst_id = $installmentStatusID;
			// check invoice id was passed in the url
			if((int)$id) {
				$username = $this->session->userdata("username");
				$parent = $this->student_m->get_parent_info($username);
				$this->data["invoice"] = $this->new_invoice_m->get_single_invoice(array('schoolID' => $schoolID, 'invoiceID' => $id)); //retrieve invoice data
				$this->data["invoiceData"] = $this->instalments_m->get_order_by_instalment(array('invoiceID'=>$this->data["invoice"]->invoiceID)); //retrieve invoice data
				$this->data['invoice_details'] = $this->new_invoice_m->get_join_instalments($schoolID,$id,$inst_id); //retrieve instalment data
				// $this->data["invoiceData"] = $this->invoice_m->getInstallmentTableData($installmentStatusID);

				// check an instalment value was passed in the url
				if(!(int)$inst_id) {
					if($this->data['invoice']) {
						$getstudent = $this->student_m->get_single_student(array('schoolID' => $schoolID, "studentID" => $this->data['invoice']->studentID));
						if($this->data['invoice'] && ($parent->parentID == $getstudent->parentID)) {	
							$this->data["student"] = $this->student_m->get_single_student(array('schoolID' => $schoolID, 'studentID' => $this->data["invoice"]->studentID));
							$this->data["subview"] = "new_invoice/view";
							$this->load->view('_layout_main', $this->data);
						} else {
							$this->data["subview"] = "error";
							$this->load->view('_layout_main', $this->data);
						}
					} else {
						$this->data["subview"] = "error";
						$this->load->view('_layout_main', $this->data);
					}
				} else { 
					if($this->data['invoice']) {
						$getstudent = $this->student_m->get_single_student(array('schoolID' => $schoolID, "studentID" => $this->data['invoice']->studentID));
						if($this->data['invoice'] && ($parent->parentID == $getstudent->parentID)) {	
							$this->data["student"] = $this->student_m->get_single_student(array('schoolID' => $schoolID, 'studentID' => $this->data["invoice"]->studentID));
							$this->data["subview"] = "new_invoice/view_instalments";
							$this->load->view('_layout_main', $this->data);
						} else {
							$this->data["subview"] = "error";
							$this->load->view('_layout_main', $this->data);
						}
					} else {
						$this->data["subview"] = "error";
						$this->load->view('_layout_main', $this->data);
					}
				}

			} else {
				$this->data["subview"] = "error";
				$this->load->view('_layout_main', $this->data);
			}
		} else {
			$this->data["subview"] = "error";
			$this->load->view('_layout_main', $this->data);
		}
	}

	public function print_preview() {
		$usertype = $this->session->userdata("usertype");
		$schoolID = $this->session->userdata('schoolID');
		if($usertype == "Admin" || $usertype == "Super Admin" || $usertype == "Accountant") {
			$id = htmlentities($this->db->escape_str($this->uri->segment(3)));
			if((int)$id) {
				$this->data["invoice"] = $this->new_invoice_m->get_single_invoice(array('schoolID' => $schoolID, 'invoiceID' => $id));
				if($this->data["invoice"]) {
					$this->data["student"] = $this->student_m->get_single_student(array('schoolID' => $schoolID, 'studentID' => $this->data["invoice"]->studentID));
					
					$this->load->library('html2pdf');
				    $this->html2pdf->folder('./assets/pdfs/');
				    $this->html2pdf->filename('Report.pdf');
				    $this->html2pdf->paper('a4', 'portrait');
				    $this->data['panel_title'] = $this->lang->line('panel_title');

				    $html = $this->load->view('invoice/print_preview', $this->data, true);
					$this->html2pdf->html($html);
					$this->html2pdf->create();

				} else {
					$this->data["subview"] = "error";
					$this->load->view('_layout_main', $this->data);
				}
			} else {
				$this->data["subview"] = "error";
				$this->load->view('_layout_main', $this->data);
			}
		} 
	}
	
	public function remita_invoice() {
		$usertype = $this->session->userdata("usertype");
		$schoolID = $this->session->userdata('schoolID');
		if($usertype == "Admin" || $usertype == "Super Admin" || $usertype == "Accountant" || $usertype == "Student" || $usertype == "Parent" ) {
			$id = htmlentities($this->db->escape_str($this->uri->segment(3)));
			if((int)$id) {
				$this->data["invoice"] = $this->new_invoice_m->get_single_invoice(array('schoolID' => $schoolID, 'invoiceID' => $id));
				if($this->data["invoice"]) {
					$this->data["student"] = $this->student_m->get_single_student(array('schoolID' => $schoolID, 'studentID' => $this->data["invoice"]->studentID));
					
					$this->load->library('html2pdf');
				    $this->html2pdf->folder('./assets/pdfs/');
				    $this->html2pdf->filename('Report.pdf');
				    $this->html2pdf->paper('a4', 'portrait');
				    $this->data['panel_title'] = $this->lang->line('panel_title');

				    $html = $this->load->view('new_invoice/remita_invoices', $this->data, true);
					$this->html2pdf->html($html);
					$this->html2pdf->create();

				} else {
					$this->data["subview"] = "error";
					$this->load->view('_layout_main', $this->data);
				}
			} else {
				$this->data["subview"] = "error";
				$this->load->view('_layout_main', $this->data);
			}
		} 
	}

	public function send_mail() {
		$usertype = $this->session->userdata("usertype");
		$schoolID = $this->session->userdata('schoolID');
		if($usertype == "Admin" || $usertype == "Accountant" || $usertype == "Super Admin") {
			$id = $this->input->post('id');
			
			if ((int)$id) {
				$this->data["invoice"] = $this->invoice_m->get_single_invoice(array('schoolID' => $schoolID, 'invoiceID' => $id));
				if($this->data["invoice"]) {
					$this->data["student"] = $this->student_m->get_single_student(array('schoolID' => $schoolID, 'studentID' => $this->data["invoice"]->studentID));
					
					$this->load->library('html2pdf');
				    $this->html2pdf->folder('./assets/pdfs/');
				    $this->html2pdf->filename('Report.pdf');
				    $this->html2pdf->paper('a4', 'portrait');
				    $this->data['panel_title'] = $this->lang->line('panel_title');
				    $html = $this->load->view('invoice/print_preview', $this->data, true);
					$this->html2pdf->html($html);
					$this->html2pdf->create('save');
					
					if($path = $this->html2pdf->create('save')) {
					$this->load->library('email');
					$this->email->set_mailtype("html");
					$this->email->from($this->data["siteinfos"]->email, $this->session->userdata('school'));
					$this->email->to($this->input->post('to'));
					$this->email->subject($this->input->post('subject'));
					$this->email->message($this->input->post('message'));	
					$this->email->attach($path);
						if($this->email->send()) {
							$this->session->set_flashdata('success', $this->lang->line('mail_success'));
						} else {
							$this->session->set_flashdata('error', $this->lang->line('mail_error'));
						}
					}

				} else {
					$this->data["subview"] = "error";
					$this->load->view('_layout_main', $this->data);
				}
			} else {
				$this->data["subview"] = "error";
				$this->load->view('_layout_main', $this->data);
			}
		} else {
			$this->data["subview"] = "error";
			$this->load->view('_layout_main', $this->data);
		}
	}

	public function payment() {
		$usertype = $this->session->userdata("usertype");
		$schoolID = $this->session->userdata('schoolID');
		if($usertype == "Admin" || $usertype == "Super Admin" || $usertype == "Accountant") {
			$id = htmlentities($this->db->escape_str($this->uri->segment(3)));
			if((int)$id) {
				$this->data['invoice'] = $this->new_invoice_m->get_single_invoice(array('schoolID' => $schoolID, 'invoiceID' => $id));
				$this->data['invoice_details'] = $this->new_invoice_m->get_join_instalments_total($schoolID,$id);

				if($this->data['invoice']) {
					if(($this->data['invoice']->paidamount != $this->data['invoice']->amount) && ($this->data['invoice']->status == 0 || $this->data['invoice']->status == 1 || $this->data['invoice']->status == 3)) {
						if($_POST) {

							// var_dump($_POST); die("test");

							$rules = $this->payment_rules();
							$this->form_validation->set_rules($rules);
							if ($this->form_validation->run() == FALSE) {
								$this->data["subview"] = "new_invoice/view";
								$this->load->view('_layout_main', $this->data);			
							} else {

								$payable_amount = $this->input->post('amount')+$this->data['invoice']->paidamount; //calculate payable ammount
								if ($payable_amount > $this->data['invoice']->amount+$this->input->post('service_charge')) {
									$this->session->set_flashdata('error', 'Payment amount is more than invoice amount');
									redirect(base_url("new_invoice/view/$id"));
								} else {							
									$this->post_data = $this->input->post();	
									if ($this->input->post('payment_method') == 'Paypal') {
										$this->post_data['id'] = $this->uri->segment(3);
										$this->invoice_data = $this->invoice_m->get_single_invoice(array('schoolID' => $schoolID, 'invoiceID' => $this->post_data['id']));
										$this->Paypal();
									} elseif ($this->input->post('payment_method') == 'Remita') {
										
										
										$this->post_data = $this->input->post();
										$this->post_data['id'] = $this->uri->segment(3);
										$this->invoice_data = $this->new_invoice_m->get_single_invoice(array('schoolID' => $schoolID, 'invoiceID' => $this->post_data['id']));
										//var_dump($this->invoice_data);
										$this->Remita("full");
									} elseif($this->input->post('payment_method') == 'Cash') {
										$status = 0;
										if($payable_amount == $this->data['invoice']->amount) {
											$status = 2;
										} else {
											$status = 1;
										}

										$username = $this->session->userdata('username');
										$dbuserID = $this->session->userdata('loginuserID');
										$dbusertype = $this->session->userdata('usertype');
										$dbuname = $this->session->userdata('name');

										$nowpaymenttype = '';
										if(empty($this->data['invoice']->paymenttype)) {
											$nowpaymenttype = 'Cash';
										} else {
											if($this->data['invoice']->paymenttype == 'Cash') {
												$nowpaymenttype = 'Cash';
											} else {
												$exp = explode(',', $this->data['invoice']->paymenttype);
												if(!in_array('Cash', $exp)) {
													$nowpaymenttype =  $this->data['invoice']->paymenttype.','.'Cash';
												} else {
													$nowpaymenttype =  $this->data['invoice']->paymenttype;
												}
											}
										}

										$array = array(
											"paidamount" => $payable_amount,
											"status" => $status,
											"paymenttype" => $nowpaymenttype,
											"paiddate" => date('Y-m-d'),
											"userID" => $dbuserID,
											"usertype" => $dbusertype,
											'uname' => $dbuname
										);

										$payment_array = array(
											"schoolID" => $this->session->userdata('schoolID'),
											"invoiceID" => $id,
											"studentID"	=> $this->data['invoice']->studentID,
											"paymentamount" => $this->input->post('amount'),
											"paymenttype" => $this->input->post('payment_method'),
											"paymentdate" => date('Y-m-d'),
											"paymentmonth" => date('M'),
											"paymentyear" => date('Y'),
											"create_date" => date("Y-m-d h:i:s"),
											"modify_date" => date("Y-m-d h:i:s"),
											"create_userID" => $this->session->userdata('loginuserID'),
											"create_username" => $this->session->userdata('username'),
											"create_usertype" => $this->session->userdata('usertype'),
										);

										$this->payment_m->insert_payment($payment_array);

										$studentID = $this->data['invoice']->studentID;
										$getstudent = $this->student_m->get_single_student(array('schoolID' => $schoolID, 'studentID' => $studentID));
										$nowamount = ($getstudent->paidamount)+($this->input->post('amount'));
										$this->student_m->update_student(array('paidamount' => $nowamount), $studentID);

										$this->invoice_m->update_invoice($array, $id);
										$this->session->set_flashdata('success', $this->lang->line('menu_success'));
										redirect(base_url("invoice/view/$id"));
									} elseif($this->input->post('payment_method') == 'Cheque') {
										$status = 0;
										if($payable_amount == $this->data['invoice']->amount) {
											$status = 2;
										} else {
											$status = 1;
										}

										$username = $this->session->userdata('username');
										$dbuserID = $this->session->userdata('loginuserID');
										$dbusertype = $this->session->userdata('usertype');
										$dbuname = $this->session->userdata('name');

										$nowpaymenttype = '';
										if(empty($this->data['invoice']->paymenttype)) {
											$nowpaymenttype = 'Cheque';
										} else {
											if($this->data['invoice']->paymenttype == 'Cheque') {
												$nowpaymenttype = 'Cheque';
											} else {
												$exp = explode(',', $this->data['invoice']->paymenttype);
												if(!in_array('Cheque', $exp)) {
													$nowpaymenttype =  $this->data['invoice']->paymenttype.','.'Cheque';
												} else {
													$nowpaymenttype =  $this->data['invoice']->paymenttype;
												}
											}
										}

										$array = array(
											"paidamount" => $payable_amount,
											"status" => $status,
											"paymenttype" => $nowpaymenttype,
											"paiddate" => date('Y-m-d'),
											"userID" => $dbuserID,
											"usertype" => $dbusertype,
											'uname' => $dbuname
										);

										$payment_array = array(
											"schoolID" => $this->session->userdata('schoolID'),
											"invoiceID" => $id,
											"studentID"	=> $this->data['invoice']->studentID,
											"paymentamount" => $this->input->post('amount'),
											"paymenttype" => $this->input->post('payment_method'),
											"paymentdate" => date('Y-m-d'),
											"paymentmonth" => date('M'),
											"paymentyear" => date('Y'),
											"create_date" => date("Y-m-d h:i:s"),
											"modify_date" => date("Y-m-d h:i:s"),
											"create_userID" => $this->session->userdata('loginuserID'),
											"create_username" => $this->session->userdata('username'),
											"create_usertype" => $this->session->userdata('usertype'),
										);

										$this->payment_m->insert_payment($payment_array);

										$studentID = $this->data['invoice']->studentID;
										$getstudent = $this->student_m->get_single_student(array('schoolID' => $schoolID, 'studentID' => $studentID));
										$nowamount = ($getstudent->paidamount)+($this->input->post('amount'));
										$this->student_m->update_student(array('paidamount' => $nowamount), $studentID);

										$this->invoice_m->update_invoice($array, $id);
										$this->session->set_flashdata('success', $this->lang->line('menu_success'));
										redirect(base_url("invoice/view/$id"));
									} else {
										$this->data["subview"] = "new_invoice/payment";
										$this->load->view('_layout_main', $this->data);
									}	
								}	
							}				
						} else {
							// $this->data["subview"] = "new_invoice/payment";
							// $this->load->view('_layout_main', $this->data);
							// var_dump($_SERVER); die ('');
							// redirect($_SERVER['HTTP_REFERER']);
							$this->data["subview"] = "error";
							$this->load->view('_layout_main', $this->data);
						}
					} else {
						$this->data["subview"] = "error";
						$this->load->view('_layout_main', $this->data);
					}
				} else {
					$this->data["subview"] = "error";
					$this->load->view('_layout_main', $this->data);
				}
			} else {
				$this->data["subview"] = "error";
				$this->load->view('_layout_main', $this->data);
			}
		} elseif($usertype == "Student") {
			$id = htmlentities($this->db->escape_str($this->uri->segment(3)));
			if((int)$id) {
				$this->data["invoice"] = $this->invoice_m->get_single_invoice(array('schoolID' => $schoolID, 'invoiceID' => $id));
				$username = $this->session->userdata("username");
				$getstudent = $this->student_m->get_single_student(array('schoolID' => $schoolID, "username" => $username));

				if($this->data['invoice'] && ($this->data['invoice']->studentID == $getstudent->studentID)) {
					if(($this->data['invoice']->paidamount != $this->data['invoice']->amount) && ($this->data['invoice']->status == 0 || $this->data['invoice']->status == 1 || $this->data['invoice']->status == 3)) {
						if($_POST) {
							$rules = $this->payment_rules();
							unset($rules[1]);
							$this->form_validation->set_rules($rules);
							if ($this->form_validation->run() == FALSE) {
								$this->data["subview"] = "invoice/payment";
								$this->load->view('_layout_main', $this->data);			
							} else {
								$payable_amount = $this->input->post('amount')+$this->data['invoice']->paidamount;//calculate payable ammount
								if ($payable_amount > $this->data['invoice']->amount+$this->input->post('service_charge')) {
									$this->session->set_flashdata('error', 'Payment amount is much than invoice amount');
									redirect(base_url("invoice/view/$id"));
								} else {
									$this->post_data = $this->input->post();
									$this->post_data['id'] = $id;
									$this->invoice_data = $this->invoice_m->get_single_invoice(array('schoolID' => $schoolID, 'invoiceID' => $id));
									$this->Remita("full"); //remita function
								}
							}
						} else {
							// $this->data["subview"] = "invoice/view";
							// $this->load->view('_layout_main', $this->data);
							$this->data["subview"] = "error";
							$this->load->view('_layout_main', $this->data);
						}
					} else {
						$this->data["subview"] = "error";
						$this->load->view('_layout_main', $this->data);
					}
				} else {
					$this->data["subview"] = "error";
					$this->load->view('_layout_main', $this->data);
				}
			} else {
				$this->data["subview"] = "error";
				$this->load->view('_layout_main', $this->data);
			}
		} elseif($usertype == "Parent") {
			$id = htmlentities($this->db->escape_str($this->uri->segment(3)));
			$installmentStatusID = htmlentities($this->db->escape_str($this->uri->segment(4)));
			if((int)$id) {
				$this->data['invoice'] = $this->new_invoice_m->get_single_invoice(array('schoolID' => $schoolID, 'invoiceID' => $id));
				$username = $this->session->userdata("username");

				if($this->data["invoice"]) {
					$getstudent = $this->student_m->get_single_student(array('schoolID' => $schoolID, "studentID" => $this->data['invoice']->studentID));
					if($this->data['invoice'] && ($this->data['invoice']->studentID == $getstudent->studentID)) {
						if(($this->data['invoice']->paidamount != $this->data['invoice']->amount) && ($this->data['invoice']->status == 0 || $this->data['invoice']->status == 1 || $this->data['invoice']->status == 3)) {
							if($_POST) {
								$rules = $this->payment_rules();
								unset($rules[1]);
								$this->form_validation->set_rules($rules);
								if ($this->form_validation->run() == FALSE) {
									$this->data["subview"] = "new_invoice/payment";
									$this->load->view('_layout_main', $this->data);			
								} else {
									$payable_amount = $this->input->post('amount')+$this->data['invoice']->paidamount;//calculate payable ammount
									if ($payable_amount > $this->data['invoice']->amount+$this->input->post('service_charge')) {
									//$payable_amount = $this->input->post('amount')+$this->data['invoice']->paidamount;
									//if ($payable_amount > $this->data['invoice']->amount) {
										$this->session->set_flashdata('error', 'Payment amount is much than invoice amount');
										redirect(base_url("new_invoice/view/$id"));
									} else {
										$this->post_data = $this->input->post();
										$this->post_data['id'] = $id;
										$this->invoice_data = $this->new_invoice_m->get_single_invoice(array('schoolID' => $schoolID, 'invoiceID' => $id));
										$this->Remita("full"); //remita function 
									}
								}
							} else {
								// $this->data["subview"] = "invoice/view";
								// $this->load->view('_layout_main', $this->data);
								$this->data["subview"] = "error";
								$this->load->view('_layout_main', $this->data);
							}
						} else {
							$this->data["subview"] = "error";
							$this->load->view('_layout_main', $this->data);
						}
					} else {
						$this->data["subview"] = "error";
						$this->load->view('_layout_main', $this->data);
					}

				} else {
					$this->data["subview"] = "error";
					$this->load->view('_layout_main', $this->data);
				}
			} else {
				$this->data["subview"] = "error";
				$this->load->view('_layout_main', $this->data);
			}
		} else {
			$this->data["subview"] = "error";
			$this->load->view('_layout_main', $this->data);
		}
	}

	/*
	* Used mainly by the parent user at the moment
	* Created by eetti @ 21/03/2016	
	*/
	public function instalment_payment() {
		$usertype = $this->session->userdata("usertype");
		$schoolID = $this->session->userdata('schoolID');
		if($usertype == "Admin" || $usertype == "Super Admin" || $usertype == "Accountant") {
			$id = htmlentities($this->db->escape_str($this->uri->segment(3)));
			$inst_id = htmlentities($this->db->escape_str($this->uri->segment(4)));
			if((int)$id) {
				$this->data['invoice'] = $this->new_invoice_m->get_single_invoice(array('schoolID' => $schoolID, 'invoiceID' => $id));
				$this->data['invoice_details'] = $this->new_invoice_m->get_join_instalments($schoolID,$id,$inst_id);

				if($this->data['invoice']) {
					if(($this->data['invoice']->paidamount != $this->data['invoice']->amount) && ($this->data['invoice']->status == 0 || $this->data['invoice']->status == 1 || $this->data['invoice']->status == 3)) {
						if($_POST) {
							$rules = $this->payment_rules();
							$this->form_validation->set_rules($rules);
							if ($this->form_validation->run() == FALSE) {
								$this->data["subview"] = "new_invoice/view";
								$this->load->view('_layout_main', $this->data);			
							} else {

								$payable_amount = $this->input->post('amount')+$this->data['invoice']->paidamount;//calculate payable ammount
								if ($payable_amount > $this->data['invoice']->amount+$this->input->post('service_charge')) {
									$this->session->set_flashdata('error', 'Payment amount is much than invoice amount');
									redirect(base_url("new_invoice/view/$id"));
								} else {							
									$this->post_data = $this->input->post();
									if ($this->input->post('payment_method') == 'Remita') {
										
										$this->post_data['id'] = $this->uri->segment(3);
										$this->invoice_data = $this->new_invoice_m->get_single_invoice(array('schoolID' => $schoolID, 'invoiceID' => $this->post_data['id']));
										//var_dump($this->invoice_data);
										$this->Remita("part");
									}else {
										$this->data["subview"] = "new_invoice/payment";
										$this->load->view('_layout_main', $this->data);
									}	
								}	
							}				
						} else {
							$this->data["subview"] = "new_invoice/payment";
							$this->load->view('_layout_main', $this->data);
						}
					} else {
						$this->data["subview"] = "error";
						$this->load->view('_layout_main', $this->data);
					}
				} else {
					$this->data["subview"] = "error";
					$this->load->view('_layout_main', $this->data);
				}
			} else {
				$this->data["subview"] = "error";
				$this->load->view('_layout_main', $this->data);
			}
		} elseif($usertype == "Student") {
			$id = htmlentities($this->db->escape_str($this->uri->segment(3)));
			if((int)$id) {
				$this->data["invoice"] = $this->invoice_m->get_single_invoice(array('schoolID' => $schoolID, 'invoiceID' => $id));
				$username = $this->session->userdata("username");
				$getstudent = $this->student_m->get_single_student(array('schoolID' => $schoolID, "username" => $username));

				if($this->data['invoice'] && ($this->data['invoice']->studentID == $getstudent->studentID)) {
					if(($this->data['invoice']->paidamount != $this->data['invoice']->amount) && ($this->data['invoice']->status == 0 || $this->data['invoice']->status == 1 || $this->data['invoice']->status == 3)) {
						if($_POST) {
							$rules = $this->payment_rules();
							unset($rules[1]);
							$this->form_validation->set_rules($rules);
							if ($this->form_validation->run() == FALSE) {
								$this->data["subview"] = "invoice/payment";
								$this->load->view('_layout_main', $this->data);			
							} else {
								$payable_amount = $this->input->post('amount')+$this->data['invoice']->paidamount;//calculate payable ammount
								if ($payable_amount > $this->data['invoice']->amount+$this->input->post('service_charge')) {
									$this->session->set_flashdata('error', 'Payment amount is much than invoice amount');
									redirect(base_url("invoice/view/$id"));
								} else {
									$this->post_data = $this->input->post();
									$this->post_data['id'] = $id;
									$this->invoice_data = $this->invoice_m->get_single_invoice(array('schoolID' => $schoolID, 'invoiceID' => $id));
									$this->Remita("part"); //remita function
								}
							}
						} else {
							$this->data["subview"] = "invoice/view";
							$this->load->view('_layout_main', $this->data);
						}
					} else {
						$this->data["subview"] = "error";
						$this->load->view('_layout_main', $this->data);
					}
				} else {
					$this->data["subview"] = "error";
					$this->load->view('_layout_main', $this->data);
				}
			} else {
				$this->data["subview"] = "error";
				$this->load->view('_layout_main', $this->data);
			}
		} elseif($usertype == "Parent") {
			$id = htmlentities($this->db->escape_str($this->uri->segment(3)));
			$inst_id = htmlentities($this->db->escape_str($this->uri->segment(4)));
			if((int)$id) {
				$this->data['invoice'] = $this->new_invoice_m->get_single_invoice(array('schoolID' => $schoolID, 'invoiceID' => $id));
				$username = $this->session->userdata("username");

				$this->data['invoice_details'] = $this->new_invoice_m->get_join_instalments($schoolID,$id,$inst_id);

				if($this->data["invoice"] && $this->data['invoice_details']) {
					$getstudent = $this->student_m->get_single_student(array('schoolID' => $schoolID, "studentID" => $this->data['invoice']->studentID));
					if($this->data['invoice'] && ($this->data['invoice']->studentID == $getstudent->studentID)) {
						if($_POST) {
							$rules = $this->payment_rules();
								unset($rules[1]);
								$this->form_validation->set_rules($rules);
								if ($this->form_validation->run() == FALSE) {
									$this->data["subview"] = "new_invoice/view";
									$this->load->view('_layout_main', $this->data);			
								} else {
									$payable_amount = $this->input->post('amount');//calculate payable ammount
									if ($payable_amount != $this->data['invoice_details'][0]->amount+$this->input->post('service_charge')) {
									//$payable_amount = $this->input->post('amount')+$this->data['invoice']->paidamount;
									//if ($payable_amount > $this->data['invoice']->amount) {
										$this->session->set_flashdata('error', 'There seems to be an error with your payment!');
										redirect(base_url("new_invoice/view/$id/$inst_id"));
									} else {
										$this->post_data = $this->input->post();
										$this->post_data['id'] = $id;
										$this->post_data['inst_id'] = $inst_id;
										$this->invoice_data = $this->new_invoice_m->get_single_invoice(array('schoolID' => $schoolID, 'invoiceID' => $id));
										$this->inst_data = $this->new_invoice_m->get_join_instalments($schoolID,$id,$inst_id)[0]; //instalment data
										// adding type to remita
										$this->Remita("part"); //remita function 
									}
								}
						} else {
							redirect(base_url("new_invoice/instalments/$id"));
						}
					}else {
						$this->data["subview"] = "error";
						$this->load->view('_layout_main', $this->data);
					}
				}else{
					$this->data["subview"] = "error";
					$this->load->view('_layout_main', $this->data);
				}
			} else {
				$this->data["subview"] = "error";
				$this->load->view('_layout_main', $this->data);
			}
		} else {
			$this->data["subview"] = "error";
			$this->load->view('_layout_main', $this->data);
		}
	}

	/* Remita payment start*/
	public function Remita($type) {

		$schoolID = $this->session->userdata('schoolID');

		$api_config = array();
		$get_configs = $this->payment_settings_m->get_order_by_config();
		foreach ($get_configs as $key => $get_key) {
			$api_config[$get_key->config_key] = $get_key->value;
		}
		$this->data['set_key'] = $api_config;
		if($api_config['service_type_id'] =="" || $api_config['merchant_id'] =="" || $api_config['api_key']==""){
			$this->session->set_flashdata('error', 'Remita settings not available');
			redirect($_SERVER['HTTP_REFERER']);
		} else {
			$this->item_data = $this->post_data;
			$this->invoice_info = (array) $this->invoice_data; //Invoice details from previous function

			// Distinguish between part/full payment 
			if($type == "part"){
				$this->inst_info = (array) $this->inst_data; //Installment details from previous function
				//set the order ID for part payment
				$this->remita->set_orderID($this->inst_info['inst_ref']);

				$params = array(
			  		'cancelUrl' 	=> base_url('new_invoice/getRemitaSuccess'),
			  		'returnUrl' 	=> base_url('new_invoice/getRemitaSuccess'),
			  		'invoice_id'	=> $this->item_data['id'],
			  		'inst_id'	=> $this->item_data['inst_id'],
			    	'name'		=> $this->invoice_info['student'],
			    	'type'      => $type,
			    	'invoice_ref'		=> $this->inst_info['inst_ref'],
			    	'description' 	=> $this->invoice_info['feetype'], 
			    	'amount' 	=> intval($this->item_data['fees']),
			    	'currency' 	=> $this->data["setschool"]->currency_code,
					);
			}elseif($type == "full"){
				$params = array(
			  		'cancelUrl' 	=> base_url('new_invoice/getRemitaSuccess'),
			  		'returnUrl' 	=> base_url('new_invoice/getRemitaSuccess'),
			  		'invoice_id'	=> $this->item_data['id'],
			    	'name'		=> $this->invoice_info['student'],
			    	'type'      => $type,
			    	'invoice_ref'		=> $this->invoice_info['invoice_ref'],
			    	'description' 	=> $this->invoice_info['feetype'], 
			    	'amount' 	=> intval($this->item_data['fees']),
			    	'currency' 	=> $this->data["setschool"]->currency_code,
					);

				//set the order ID for full payment
				$this->remita->set_orderID($this->invoice_info['invoice_ref']);
			}

			$type = $this->post_data['payment_type'];

			$this->session->set_userdata("params", $params);

			//set the response url
	        $this->remita->set_responseurl($params["returnUrl"]);//set return url
			
			//set the payer name
	        $this->remita->set_payer_name($this->invoice_info['student']);
			
			//check student status || get student details
	        $checkstudent = $this->student_m->get_single_student(array('schoolID' => $schoolID, 'studentID' => $this->invoice_info['studentID']));
			
			//get parent details
			$parent = $this->parentes_m->get_single_parentes(array('schoolID' => $schoolID, 'parentID' => $checkstudent->parentID));
			
			//set all remita payment details
	        $this->remita->set_payer_phone($parent->phone);
	        $this->remita->set_payer_email($parent->email);

	        $this->remita->set_payment_descr($this->invoice_info['feetype']); //payment description
	        $this->remita->set_total_amount($this->post_data['amount']);//set total amount

	        //decider - how the money is deducted
	        if($this->invoice_info["service_charge_info"] == "1"){
	        	$this->remita->set_school_fee($this->post_data['amount'] - $this->post_data['service_charge']);
	        }else{
	        	$this->remita->set_school_fee($this->post_data['fees']);
	        }
	        //NOTE : Service charge must be int
	        $this->remita->set_cloudskul_fee((int)$this->post_data['service_charge']);//set service charge
			
			//get account details
	        $account_cloudskul = $this->payment_settings_m->get_account_cloudskul(); //retrieve account details from DB
	        $account_school = $this->payment_settings_m->get_account_schools($this->invoice_info['accountID']);   
	        //future update, if account is not set, retrieve school account number    
			
			
	        $this->remita->set_beneficiary_cloud($account_cloudskul); //set cloudskul account number
	        $this->remita->set_beneficiary($account_school); //set school account number
	        $this->remita->set_payment_type($this->post_data['payment_type']);//set payment type

	        $this->remita->getSettings(); //function to obtain settings from database

	        $execute = $this->remita->execute_remita(); //execute the remita process


	        if($execute && $type == 'BANK_BRANCH' && array_key_exists('RRR', $execute)){
	        	// var_dump($execute) and die();
	        	$rrr = trim($execute['RRR']); //return value is RRR;
				$orderID = trim($execute['orderID']); 
				$message = $execute['status'];
		        $params = $this->session->userdata('params');

				if($params['type'] == 'full'){

			    	$status = 3;
			    	$recent_paidamount = '';//$params['amount'];
			    	$nowpaymenttype = "Remita,Bank";

					$dbuserID = $this->session->userdata('loginuserID');
					$dbusertype = $this->session->userdata('usertype');
					$dbuname = $this->session->userdata('name');

					/*
					* Issue : I removed paid amount from the array below
					* Date: 14/04/2016
					* Edited By: Emmanuel Etti
					*/

			    	//array with contents 
			    	$array = array(
						"status" => $status,
						"paymenttype" => $nowpaymenttype,
						"rrr" => $rrr,
						"userID" => $dbuserID,
						"usertype" => $dbusertype, 
						'uname' => $dbuname
					);
					
					//update installment_status table with rrr

					// $this->db->where('installment_statusID', $orderID);
					// $this->db->update('installment_status', $remitaUpdate);
					
		        	$this->new_invoice_m->update_invoice($array, $params['invoice_id']);
				    $this->session->set_flashdata('success', 'RRR was generated. Click on the Print Bank Invoice button. ');
				    redirect(base_url("new_invoice/view/".$params['invoice_id']));

				}else{
					var_dump($execute); die("Bank Payment Test was successful");
					//will redirect to instalment page
					//set data to update for instalment_status table
					$remitaUpdate = array(
						"rrr" => $rrr,
						"remita_payment_status" => $message
					);
				}

				
				// Please note that the orderID = installment_statusID
				
				//get the order id record from installment status table
				//use that to get the selected installment
				//pass the selected installment to the control function 
				//what the control function returns, use to update the selected columns
				
				//use the order ID to get the selected installment

				/*Sunny code start
				$selectedInstallments = $this->invoice_m->getSelectedInstallmentsByID($orderID)->selected_installments;
				
				$selectedInstallmentsArray = $this->getSelectedInstallment($selectedInstallments, $message);
				**Sunny code end.
				*/
				
	        }else{
	        	$this->session->set_flashdata('error', 'Payment was unsuccessful');
			    redirect(base_url("new_invoice/view/".$params['invoice_id']));
	        }
	        
		}
		
	}
	
	private function getSelectedInstallment($selectedInstallments, $response){
		if($selectedInstallments == '123'){
			return array("first_installment_status" => $response, "second_installment_status" => $response, "third_installment_status" => $response);
		}
		
		if($selectedInstallments == '12'){
			return array("first_installment_status" => $response, "second_installment_status" => $response);
		}
		
		if($selectedInstallments == '13'){
			return array("first_installment_status" => $response, "third_installment_status" => $response);
		}
		
		if($selectedInstallments == '23'){
			return array("second_installment_status" => $response, "third_installment_status" => $response);
		}
		
		if($selectedInstallments == '1'){
			return array("first_installment_status" => $response);
		}
		
		if($selectedInstallments == '2'){
			return array("second_installment_status" => $response);
		}
		
		if($selectedInstallments == '3'){
			return array("third_installment_status" => $response);
		}
	}
	
	public function test(){
		$r = $this->remita->remita_transaction_details('CS210116183055');
		var_dump($r);
	}

	public function getRemitaSuccess(){
		$schoolID = $this->session->userdata('schoolID');
		$params = $this->session->userdata('params');
		$orderID  = $_GET['orderID'];
		$rrr  = $_GET['RRR'];
		$this->remita->getSettings(); //function to obtain settings from database
		$remitaResponse = $this->remita->remita_transaction_details($orderID);
		
		$message = $remitaResponse['message'];
		$transDate = $remitaResponse['transactiontime'];
		
		$this->data['invoice'] = $this->new_invoice_m->get_invoice($params['invoice_id']);
		/*
		var_dump($_GET); 
		var_dump($remitaResponse);
		die;
		*/
  		$recent_paidamount = $params['amount']+$this->data['invoice']->paidamount;
  		if(isset($orderID) && $remitaResponse['message'] === 'Approved' && $remitaResponse['status'] === '01' ) {
  			// Response
  			if (substr($orderID, 0, 3) == 'CSI') {

  				$nowpaymenttype = 'Remita';
  				$inst_array = array(
					"status" => 1,
					"payment_type" => $nowpaymenttype,
					"paid_date" => date('Y-m-d')
				);

				$instalment_info = $this->instalments_m->get_single_instalment(array("invoiceID"=>$params["invoice_id"],"pay_seq"=>$params["inst_id"]));

				// var_dump($instalment_info->instID);

				// echo "</br> An Update to the DB occurs here";

				$this->instalments_m->update_instalment($inst_array,$instalment_info->instID);

				// echo "<pre> Remita Payment: "; var_dump($remitaResponse["message"]); echo "</pre>"; 

				// die("</br> </br>Dev in serious progress");

				$status = 0;
				if($recent_paidamount == $this->data['invoice']->amount) {
					$status = 2;
				} else {
					$status = 1;
				}

				$usertype = $this->session->userdata("usertype");
				$username = $this->session->userdata('username');
				$dbuserID = $this->session->userdata('loginuserID');
				$dbusertype = $this->session->userdata('usertype');
				$dbuname = $this->session->userdata('name');

				$nowpaymenttype = ''; //payment type to insert inside invoice table
				if(empty($this->data['invoice']->paymenttype)) {
					$nowpaymenttype = 'Remita';
				} else {
					if($this->data['invoice']->paymenttype == 'Remita') {
						$nowpaymenttype = 'Remita';
					} else {
						$exp = explode(',', $this->data['invoice']->paymenttype);
						if(!in_array('Remita', $exp)) {
							$nowpaymenttype =  $this->data['invoice']->paymenttype.','.'Remita';
						} else {
							$nowpaymenttype =  $this->data['invoice']->paymenttype;
						}
					}
				}

				/*Organise data for invoice table*/
				$array = array(
					"paidamount" => $recent_paidamount,
					"status" => $status,
					"paymenttype" => $nowpaymenttype,
					"paiddate" => date('Y-m-d'),
					"userID" => $dbuserID,
					"usertype" => $dbusertype,
					'uname' => $dbuname
				);

				/*Organise data for payment table*/
				$payment_array = array(
					"schoolID" => $this->session->userdata('schoolID'),
					"invoiceID" => $params['invoice_id'],
					"studentID"	=> $this->data['invoice']->studentID,
					"paymentamount" => $params['amount'],
					"paymenttype" => 'Remita',					
					"order_id" => $orderID,
					"rrr" => $rrr,
					"paymentdate" => date('Y-m-d'),
					"paymentmonth" => date('M'),
					"paymentyear" => date('Y'),
					"create_date" => date("Y-m-d h:i:s"),
					"modify_date" => date("Y-m-d h:i:s"),
					"create_userID" => $this->session->userdata('loginuserID'),
					"create_username" => $this->session->userdata('username'),
					"create_usertype" => $this->session->userdata('usertype'),
				);

				$this->payment_m->insert_payment($payment_array);//insert data into payment table
				

				$studentID = $this->data['invoice']->studentID;
				$getstudent = $this->student_m->get_single_student(array('schoolID' => $schoolID, 'studentID' => $studentID));

				$nowamount = ($getstudent->paidamount)+($params['amount']);//add previous paid fees to new fees

				/*UPDATE STUDENT TABLE: increment total fees students have paid*/
				$this->student_m->update_student(array('paidamount' => $nowamount), $studentID);

				/*UPDATE INVOICE TABLE*/
				$this->new_invoice_m->update_invoice($array, $params['invoice_id']);
				
				$this->session->set_flashdata('success', $this->lang->line('menu_success'));

  			}elseif(substr($orderID, 0, 2) == 'CS' ){
  				// var_dump($remitaResponse); die("</br> </br> </br>Dev in serious progress");


  				/*
  				*status 0 : not paid
  				*status 1 : partially paid
  				*status 2 : fully paid
  				*/
				
				
				$status = 0;
				if($recent_paidamount == $this->data['invoice']->amount) {
					$status = 2;
				} else {
					$status = 1;
				}

				$usertype = $this->session->userdata("usertype");
				$username = $this->session->userdata('username');
				$dbuserID = $this->session->userdata('loginuserID');
				$dbusertype = $this->session->userdata('usertype');
				$dbuname = $this->session->userdata('name');

				$nowpaymenttype = ''; //payment type to insert inside invoice table
				if(empty($this->data['invoice']->paymenttype)) {
					$nowpaymenttype = 'Remita';
				} else {
					if($this->data['invoice']->paymenttype == 'Remita') {
						$nowpaymenttype = 'Remita';
					} else {
						$exp = explode(',', $this->data['invoice']->paymenttype);
						if(!in_array('Remita', $exp)) {
							$nowpaymenttype =  $this->data['invoice']->paymenttype.','.'Remita';
						} else {
							$nowpaymenttype =  $this->data['invoice']->paymenttype;
						}
					}
				}

				/*Organise data for invoice table*/
				$array = array(
					"paidamount" => $recent_paidamount,
					"status" => $status,
					"paymenttype" => $nowpaymenttype,
					"paiddate" => date('Y-m-d'),
					"userID" => $dbuserID,
					"usertype" => $dbusertype,
					'uname' => $dbuname
				);

				/*Organise data for payment table*/
				$payment_array = array(
					"schoolID" => $this->session->userdata('schoolID'),
					"invoiceID" => $params['invoice_id'],
					"studentID"	=> $this->data['invoice']->studentID,
					"paymentamount" => $params['amount'],
					"paymenttype" => 'Remita',					
					"order_id" => $orderID,
					"rrr" => $rrr,
					"paymentdate" => date('Y-m-d'),
					"paymentmonth" => date('M'),
					"paymentyear" => date('Y'),
					"create_date" => date("Y-m-d h:i:s"),
					"modify_date" => date("Y-m-d h:i:s"),
					"create_userID" => $this->session->userdata('loginuserID'),
					"create_username" => $this->session->userdata('username'),
					"create_usertype" => $this->session->userdata('usertype'),
				);

				$this->payment_m->insert_payment($payment_array);//insert data into payment table
				
				// insert into payment approval table

				$studentID = $this->data['invoice']->studentID;
				$getstudent = $this->student_m->get_single_student(array('schoolID' => $schoolID, 'studentID' => $studentID));

				$nowamount = ($getstudent->paidamount)+($params['amount']);//add previous paid fees to new fees

				/*UPDATE STUDENT TABLE: increment total fees students have paid*/
				$this->student_m->update_student(array('paidamount' => $nowamount), $studentID);

				/*UPDATE INVOICE TABLE*/
				$this->new_invoice_m->update_invoice($array, $params['invoice_id']);
				
				/*Write UPDATE INSTALMENT TABLE CODE HERE*/
				$inst_array = array(
					"status" => 1,
					"payment_type" => $nowpaymenttype,
					"paid_date" => date('Y-m-d')
				);
				
				$this->instalments_m->update_invoice_instalment($inst_array,$params['invoice_id']);

				$this->session->set_flashdata('success', $this->lang->line('menu_success'));
  			} else {
  				$this->session->set_flashdata('error', 'Error generating Payment reference!');
  			}
  			redirect(base_url("new_invoice/view/".$params['invoice_id']));
		}else {
      			
      		//Failed transaction
      		$this->session->set_flashdata('error', 'Sorry, your payment was not successful!');
  			redirect(base_url("new_invoice/view/".$params['invoice_id']));
      			
  		}

	}

	/*Batched Response start*/
	public function batched_response(){

		$raw_post_data = file_get_contents('php://input');
		$response_arr = json_decode(stripcslashes($raw_post_data), TRUE); //use json_decode when the post body is in json format

		if($response_arr !== NULL){
			
		     foreach($response_arr as $value)
		     {
				if(!empty($value['rrr'])){

					/*$str  = "insert into eduportal_remita_payment(payment_id, rrr, channel, amount, payer_name, payer_email, payer_phone, unique_id, response_code, trans_date,"; 
					$str .=	"debit_date, bank, branch, service_type, date_sent, date_requested, order_ref)"; 
					$str .=	"values ('".$value['id']."', '".$value['rrr']."', '".$value['channnel']."', '".$value['amount']."',";
					$str .=	" '".$value['payerName']."', '".$value['payerEmail']."', '".$value['payerPhoneNumber']."','".$value['uniqueIdentifier']."',";
					$str .= " '".$value['responseCode']."', '".$value['transactiondate']."', '".$value['debitdate']."', '".$value['bank']."',";
					$str .= " '".$value['branch']."', '".$value['serviceTypeId']."', '".$value['dateSent']."', '".$value['dateRequested']."', '".$value['orderRef']."') ";
					
					echo $str;
					return;*/
					
				//check if rrr already exists
					$invoice = $this->invoice_m->get_single_invoice(array('rrr'=>$value['rrr']));
					if(count($invoice) > 0){
						echo "Invoice found, proceeding to confirm  payment for RRR: ".$value['rrr']."! \n";
						$insertedRecord = $this->payment_m->get_single_payment(array('rrr'=>$value['rrr']));
						if(count($insertedRecord) > 0){
							echo "Payment already confirmed for RRR: ".$value['rrr']."! \n";
						}else{
							
							$bad_keys = array('invoiceextra1','invoiceextra2','create_date','modify_date','create_userID','create_username','create_usertype','date','paiddate','year','feetype','paymenttype');
							$out = array_diff_key(json_decode(json_encode($invoice),true),array_flip($bad_keys));

							$studentID = $out['studentID'];
							$schoolID = $out['schoolID'];
							$getstudent = $this->student_m->get_single_student(array('schoolID' => $schoolID, 'studentID' => $studentID));
							$getparent = $this->parentes_m->get_single_parentes(array('schoolID' => $schoolID, 'parentID' => $getstudent->parentID));

							//$values = json_encode($out); //$values = json_decode($values);
							$diff = $value['amount'] - $out['amount'];//taking out the commision
							//echo $diff."\n";
							$status = 0;
							if($value['amount'] > $out['amount']) {
								$status = 2;
							} else {
								$status = 1;
							}

							$array = array(
								"paidamount" => intval($value['amount']) - $diff,
								"status" => $status,
								"paymenttype" => 'Remita,Bank',
								"paiddate" => date('Y-m-d'),
								"userID" => $getparent->parentID,
								"usertype" => 'Parent',
								'uname' => $getparent->username
							);

							$payment_array = array(
								"schoolID" => $out['schoolID'],
								"invoiceID" => $out['invoiceID'],
								"studentID"	=> $out['studentID'],
								"paymentamount" => $out['amount'],
								"paymenttype" => 'Remita,Bank',					
								"order_id" => $value['order_ref'],
								"rrr" => $out['rrr'],
								"paymentdate" => date('Y-m-d'),
								"paymentmonth" => date('M'),
								"paymentyear" => date('Y'),
								"create_date" => date("Y-m-d h:i:s"),
								"modify_date" => date("Y-m-d h:i:s"),
								"create_userID" => 4,
								"create_username" => 'etti',
								"create_usertype" => 'Super Admin',
							);

							$this->payment_m->insert_payment($payment_array); //insert into payment table
							
							$nowamount = ($getstudent->paidamount)+($out['amount']);
							$this->student_m->update_student(array('paidamount' => $nowamount), $studentID);//update total amount of money student has paid

							$this->invoice_m->update_invoice($array, $out['invoiceID']);//update invoice

							echo "Payment was made successfully for RRR: ".$value['rrr']."! \n";
						}
					}else{
						echo "Invoice not found for RRR: ".$value['rrr']."! \n";
					}
					/*die;
					
					if(mysqli_num_rows($insertedRecord) > 0){
						echo "Payment already confirmed for RRR: ".$value['rrr']."! \n";
					}else{

						$updateRow = $mysqli->query("update eduportal_remita_temp_data set status = 'Payment Confirmed' where rrr = '".$value['rrr']."'");
						
						//if(mysqli_num_rows($updateRow) > 0){
							$str  = "insert into eduportal_remita_payment(payment_id, rrr, channel, amount, payer_name, payer_email, payer_phone, unique_id, response_code, trans_date,"; 
							$str .=	"debit_date, bank, branch, service_type, date_sent, date_requested, order_ref)"; 
							$str .=	"values ('".$value['id']."', '".$value['rrr']."', '".$value['channnel']."', '".$value['amount']."',";
							$str .=	" '".$value['payerName']."', '".$value['payerEmail']."', '".$value['payerPhoneNumber']."','".$value['uniqueIdentifier']."',";
							$str .= " '".$value['responseCode']."', '".$value['transactiondate']."', '".$value['debitdate']."', '".$value['bank']."',";
							$str .= " '".$value['branch']."', '".$value['serviceTypeId']."', '".$value['dateSent']."', '".$value['dateRequested']."', '".$value['orderRef']."') ";
					
							$insertRow = $mysqli->query($str);
							
							if($insertRow){
								echo "Payment Confirmed for RRR: ".$value['rrr']."\n";
							}else{
								die('Error: ' . $mysqli->error);
							}
						//}else{
						//	die('Error: ' . $mysqli->error);
						//}
					}*/
				}else{
					echo "Invalid Data: The Data that was sent could not be processed because the RRR value is NULL";
				}
			}
		}else
			echo "No data has been received";

	}

	function call_all_student() {
		$classesID = $this->input->post('id');
		$schoolID = $this->session->userdata('schoolID');
		if((int)$classesID) {
			echo "<option value='". 0 ."'>". $this->lang->line('invoice_select_student') ."</option>";
			$students = $this->student_m->get_order_by_student(array('schoolID' => $schoolID, 'classesID' => $classesID));
			foreach ($students as $key => $student) {
				echo "<option value='". $student->studentID ."'>". $student->name ."</option>";
			}
		} else {
			echo "<option value='". 0 ."'>". $this->lang->line('invoice_select_student') ."</option>";
		}
	}

	function feetypecall() {
		$feetype = $this->input->post('feetype');
		if($feetype) {
			$allfeetypes = $this->feetype_m->allfeetype($feetype);
			
			foreach ($allfeetypes as $allfeetype) {
				echo "<li id='". $allfeetype->feetypeID ."'>".$allfeetype->feetype."</li>";
			}
		}
	}

	function date_valid($date) {
		if(strlen($date) <10) {
			$this->form_validation->set_message("date_valid", "%s is not valid dd/mm/yyyy");
	     	return FALSE;
		} else {
	   		$arr = explode("/", $date);   
	        $dd = $arr[0];            
	        $mm = $arr[1];              
	        $yyyy = $arr[2];
	      	if(checkdate($dd, $mm, $yyyy)) {
	      		return TRUE;
	      	} else {
	      		$this->form_validation->set_message("date_valid", "%s is not valid dd/mm/yyyy");
	     		return FALSE;
	      	}
	    } 
	}

	function unique_classID() {
		if($this->input->post('classesID') == 0) {
			$this->form_validation->set_message("unique_classID", "The %s field is required");
	     	return FALSE;
		}
		return TRUE;
	}

	function valid_number() {
		if($this->input->post('amount') && $this->input->post('amount') < 0) {
			$this->form_validation->set_message("valid_number", "%s is invalid number");
			return FALSE;
		}
		return TRUE;
	}

	function unique_paymentmethod() {
		if($this->input->post('payment_method') === '0') {
			$this->form_validation->set_message("unique_paymentmethod", "The %s field is required");
	     	return FALSE;
		} elseif($this->input->post('payment_method') === 'Paypal') {
			$api_config = array();
			$get_configs = $this->payment_settings_m->get_order_by_config();
			foreach ($get_configs as $key => $get_key) {
				$api_config[$get_key->config_key] = $get_key->value;
			}
			if($api_config['paypal_api_username'] =="" || $api_config['paypal_api_password'] =="" || $api_config['paypal_api_signature']==""){
				$this->form_validation->set_message("unique_paymentmethod", "Paypal settings required");
				return FALSE;
			}
		}
		return TRUE;
	} 

	public function correctInstalment(){
			
		$len = $this->input->post('instVal');
		$sum =0;
		if($this->input->post('amount') && $this->input->post('amount') < 0){
			$this->form_validation->set_message("correctInstalment", "The %s field is required");
	     	return FALSE;
		}elseif($this->input->post("amount") > 0 && $len > 1){
			for ($i=1; $i <=$len ; $i++) { 
				$sum += $this->input->post("amount$i");
			}
			if($sum != $this->input->post('amount')){
				$this->form_validation->set_message("correctInstalment", "The Instalments Must Sum Up To the Total %s ");
				return FALSE;
			}
		}
		return TRUE;
	}

	public function student_list() {
		$studentID = $this->input->post('id');
		if((int)$studentID) {
			$string = base_url("new_invoice/index/$studentID");
			echo $string;
		} else {
			redirect(base_url("new_invoice/index"));
		}
	}

	public function Paypal() {
		$api_config = array();
		$get_configs = $this->payment_settings_m->get_order_by_config();
		foreach ($get_configs as $key => $get_key) {
			$api_config[$get_key->config_key] = $get_key->value;
		}
		$this->data['set_key'] = $api_config;
		if($api_config['paypal_api_username'] =="" || $api_config['paypal_api_password'] =="" || $api_config['paypal_api_signature']==""){
			$this->session->set_flashdata('error', 'Paypal settings not available');
			redirect($_SERVER['HTTP_REFERER']);
		} else {
			$this->item_data = $this->post_data;
			$this->invoice_info = (array) $this->invoice_data;

			$params = array(
	  		'cancelUrl' 	=> base_url('invoice/getSuccessPayment'),
	  		'returnUrl' 	=> base_url('invoice/getSuccessPayment'),
	  		'invoice_id'	=> $this->item_data['id'],
	    	'name'		=> $this->invoice_info['student'],
	    	'description' 	=> $this->invoice_info['feetype'], 
	    	'amount' 	=> number_format(floatval($this->item_data['amount']),2),
	    	'currency' 	=> $this->data["setschool"]->currency_code,
			);
			$this->session->set_userdata("params", $params);
			$gateway = Omnipay::create('PayPal_Express');
			$gateway->setUsername($api_config['paypal_api_username']);
			$gateway->setPassword($api_config['paypal_api_password']);
			$gateway->setSignature($api_config['paypal_api_signature']);

			$gateway->setTestMode($api_config['paypal_demo']);

			$response = $gateway->purchase($params)->send();

			if ($response->isSuccessful()) {
				// payment was successful: update database 
			} elseif ($response->isRedirect()) {
				$response->redirect();
			} else {
			  // payment failed: display message to customer
			  echo $response->getMessage();
			}
		}
		/*omnipay Paypal end*/
	}

	public function getSuccessPayment() {	
  		$schoolID = $this->session->userdata('schoolID');
  		$api_config = array();
		$get_configs = $this->payment_settings_m->get_order_by_config();
		foreach ($get_configs as $key => $get_key) {
			$api_config[$get_key->config_key] = $get_key->value;
		}
		$this->data['set_key'] = $api_config;

   		$gateway = Omnipay::create('PayPal_Express');
		$gateway->setUsername($api_config['paypal_api_username']);
		$gateway->setPassword($api_config['paypal_api_password']);
		$gateway->setSignature($api_config['paypal_api_signature']);

		$gateway->setTestMode($api_config['paypal_demo']);
      	
		$params = $this->session->userdata('params');
  		$response = $gateway->completePurchase($params)->send();
  		$paypalResponse = $response->getData(); // this is the raw response object
  		$purchaseId = $_GET['PayerID'];
  		$this->data['invoice'] = $this->invoice_m->get_invoice($params['invoice_id']);
  		$recent_paidamount = $params['amount']+$this->data['invoice']->paidamount;
  		if(isset($paypalResponse['PAYMENTINFO_0_ACK']) && $paypalResponse['PAYMENTINFO_0_ACK'] === 'Success') {
  			// Response
  			if ($purchaseId) {

				$status = 0;
				if($recent_paidamount == $this->data['invoice']->amount) {
					$status = 2;
				} else {
					$status = 1;
				}

				$usertype = $this->session->userdata("usertype");
				$username = $this->session->userdata('username');
				$dbuserID = $this->session->userdata('loginuserID');
				$dbusertype = $this->session->userdata('usertype');
				$dbuname = $this->session->userdata('name');

				$nowpaymenttype = '';
				if(empty($this->data['invoice']->paymenttype)) {
					$nowpaymenttype = 'Paypal';
				} else {
					if($this->data['invoice']->paymenttype == 'Paypal') {
						$nowpaymenttype = 'Paypal';
					} else {
						$exp = explode(',', $this->data['invoice']->paymenttype);
						if(!in_array('Paypal', $exp)) {
							$nowpaymenttype =  $this->data['invoice']->paymenttype.','.'Paypal';
						} else {
							$nowpaymenttype =  $this->data['invoice']->paymenttype;
						}
					}
				}

				$array = array(
					"paidamount" => $recent_paidamount,
					"status" => $status,
					"paymenttype" => $nowpaymenttype,
					"paiddate" => date('Y-m-d'),
					"userID" => $dbuserID,
					"usertype" => $dbusertype,
					'uname' => $dbuname
				);

				$payment_array = array(
					"schoolID" => $this->session->userdata('schoolID'),
					"invoiceID" => $params['invoice_id'],
					"studentID"	=> $this->data['invoice']->studentID,
					"paymentamount" => $params['amount'],
					"paymenttype" => 'Paypal',
					"paymentdate" => date('Y-m-d'),
					"paymentmonth" => date('M'),
					"paymentyear" => date('Y'),
					"create_date" => date("Y-m-d h:i:s"),
					"modify_date" => date("Y-m-d h:i:s"),
					"create_userID" => $this->session->userdata('loginuserID'),
					"create_username" => $this->session->userdata('username'),
					"create_usertype" => $this->session->userdata('usertype'),
				);

				$this->payment_m->insert_payment($payment_array);

				$studentID = $this->data['invoice']->studentID;
				$getstudent = $this->student_m->get_single_student(array('schoolID' => $schoolID, 'studentID' => $studentID));
				$nowamount = ($getstudent->paidamount)+($params['amount']);
				$this->student_m->update_student(array('paidamount' => $nowamount), $studentID);

				$this->invoice_m->update_invoice($array, $params['invoice_id']);
				$this->session->set_flashdata('success', $this->lang->line('menu_success'));
  			} else {
  				$this->session->set_flashdata('error', 'Payer id not found!');
  			}
  			redirect(base_url("invoice/view/".$params['invoice_id']));
  		} else {
      			
      		//Failed transaction
      		$this->session->set_flashdata('error', 'Payment not success!');
  			redirect(base_url("invoice/view/".$params['invoice_id']));
      			
  		}
  	}
	/* Paypal payment end*/
	

	/*
	*Descrition: Reference code for the installments
	*Purpose: Will be sent to Remita as orderID
	*@author: Emmanuel Etti
	*/
	private function randomRef(){
		$ref = 'CSI';
		$ref .= substr((time() * rand(123, 999)),0,7);
		return $ref;
	}
	
	/*
	*Descrition: Reference code for the Invoice
	*Purpose: It is just for reference purposes at the moment
	*@author: Emmanuel Etti
	*/
	private function InvCode($schoolID,$classesID){
		$ref = 'CS'.$schoolID.$classesID;
		$ref .= substr((time() * rand(123, 999)),0,7);
		return $ref;
	}

	// public function userID() {
	// 	$usertype = $this->session->userdata('usertype');
	// 	$username = $this->session->userdata('username');
	// 	if ($usertype=="Admin") {
	// 		$table = "setting";
	// 		$tableID = "settingID";
	// 	} elseif($usertype=="Accountant" || $usertype=="Librarian") {
	// 		$table = "user";
	// 		$tableID = "userID";
	// 	} else {
	// 		$table = strtolower($usertype);
	// 		$tableID = $table."ID";
	// 	}				
	// 	$query = $this->db->get_where($table, array('username' => $username));
	// 	$userID = $query->row()->$tableID;
	// 	return $userID;
	// }
}

/* End of file invoice.php */
/* Location: .//D/xampp/htdocs/school/mvc/controllers/invoice.php */
