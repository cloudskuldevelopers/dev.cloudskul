<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Assessmentsettings extends MY_Controller {
/*
| -----------------------------------------------------
| PRODUCT NAME: 	CloudSkul
| -----------------------------------------------------
| AUTHOR:			CLOUDSKUL TEAM
| -----------------------------------------------------
| EMAIL:			info@cloudskul.com
| -----------------------------------------------------
| COPYRIGHT:		RESERVED BY Cloudskul
| -----------------------------------------------------
| WEBSITE:			http://cloudskul.com
| -----------------------------------------------------
*/
	function __construct() {
		parent::__construct();
		$this->load->model("assessmentsettings_m");
		$this->load->model("school_m");
		$language = $this->session->userdata('lang');
		$this->lang->load('assessmentsettings', $language);	
	}

	protected function rules() {
		$rules = array(
			array(
				'field' => 'date', 
				'label' => $this->lang->line("classes_numeric"),
				'rules' => 'trim|required|max_length[10]|xss_clean|callback_date_valid|callback_valid_future_date'
			)
		);
		return $rules;
	}
		
	public function index() {
		$usertype = $this->session->userdata("usertype");
		$schoolID = $this->session->userdata('schoolID');
		if($usertype == "Admin" || $usertype == "Super Admin") {
			//$this->data['teachers'] = $this->teacher_m->get_order_by_teacher(array('schoolID' => $schoolID));
			$this->data["subview"] = "assessmentsettings/index";
			$this->load->view('_layout_main', $this->data);
		} else {
			$this->data["subview"] = "error";
			$this->load->view('_layout_main', $this->data);
		}
	}

	
}

/* End of file class.php */
/* Location: .//D/xampp/htdocs/school/mvc/controllers/class.php */