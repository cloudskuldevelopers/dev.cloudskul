<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Exam extends MY_Controller {
/*
| -----------------------------------------------------
| PRODUCT NAME: 	CloudSkul
| -----------------------------------------------------
| AUTHOR:			CLOUDSKUL TEAM
| -----------------------------------------------------
| EMAIL:			info@cloudskul.com
| -----------------------------------------------------
| COPYRIGHT:		RESERVED BY Cloudskul
| -----------------------------------------------------
| WEBSITE:			http://cloudskul.com
| -----------------------------------------------------
*/
	function __construct() {
		parent::__construct();
		$this->load->model("exam_m");
		$language = $this->session->userdata('lang');
		$this->lang->load('exam', $language);	
	}

	public function index() {
		$usertype = $this->session->userdata("usertype");
		$schoolID = $this->session->userdata('schoolID');
		if($usertype == "Admin" || $usertype == "Super Admin") {
			$this->data['exams'] = $this->exam_m->get_order_by_exam(array('schoolID' => $schoolID));
			$this->data["subview"] = "exam/index";
			$this->load->view('_layout_main', $this->data);
		} else {
			$this->data["subview"] = "error";
			$this->load->view('_layout_main', $this->data);
		}
	}

	protected function rules() {
		$rules = array(
			array(
				'field' => 'exam', 
				'label' => $this->lang->line("exam_name"), 
				'rules' => 'trim|required|xss_clean|max_length[60]|callback_unique_exam'
			), 
			array(
				'field' => 'date', 
				'label' => $this->lang->line("exam_date"),
				'rules' => 'trim|required|max_length[10]|xss_clean|callback_date_valid'
			), 
			array(
				'field' => 'note', 
				'label' => $this->lang->line("exam_note"), 
				'rules' => 'trim|max_length[200]|xss_clean'
			)
		);
		return $rules;
	}

	public function add() {
		$usertype = $this->session->userdata("usertype");
		if($usertype == "Admin" || $usertype == "Super Admin") {
			if($_POST) {
				$rules = $this->rules();
				$this->form_validation->set_rules($rules);
				if ($this->form_validation->run() == FALSE) {
					$this->data['form_validation'] = validation_errors(); 
					$this->data["subview"] = "exam/add";
					$this->load->view('_layout_main', $this->data);			
				} else {
					$array = array(
						"schoolID" => $this->session->userdata('schoolID'),
						"exam" => $this->input->post("exam"),
						"date" => date("Y-m-d", strtotime($this->input->post("date"))),
						"note" => $this->input->post("note"),
						"create_date" => date("Y-m-d h:i:s"),
						"modify_date" => date("Y-m-d h:i:s"),
						"create_userID" => $this->session->userdata('loginuserID'),
						"create_username" => $this->session->userdata('username'),
						"create_usertype" => $this->session->userdata('usertype'),
					);

					$this->exam_m->insert_exam($array);
					$this->session->set_flashdata('success', $this->lang->line('menu_success'));
					redirect(base_url("exam/index"));
				}
			} else {
				$this->data["subview"] = "exam/add";
				$this->load->view('_layout_main', $this->data);
			}
		} else {
			$this->data["subview"] = "error";
			$this->load->view('_layout_main', $this->data);
		}
	}

	public function edit() {
		$usertype = $this->session->userdata("usertype");
		$schoolID = $this->session->userdata('schoolID');
		if($usertype == "Admin" || $usertype == "Super Admin") {
			$id = htmlentities($this->db->escape_str($this->uri->segment(3)));
			if((int)$id) {
				$this->data['exam'] = $this->exam_m->get_single_exam(array("schoolID" => $schoolID, 'examID' => $id));
				if($this->data['exam']) {
					if($_POST) {
						$rules = $this->rules();
						$this->form_validation->set_rules($rules);
						if ($this->form_validation->run() == FALSE) {
							$this->data["subview"] = "exam/edit";
							$this->load->view('_layout_main', $this->data);			
						} else {
							$array = array(
								"exam" => $this->input->post("exam"),
								"date" => date("Y-m-d", strtotime($this->input->post("date"))),
								"note" => $this->input->post("note"),
								"modify_date" => date("Y-m-d h:i:s")
							);

							$this->exam_m->update_exam($array, $id);
							$this->session->set_flashdata('success', $this->lang->line('menu_success'));
							redirect(base_url("exam/index"));
						}
					} else {
						$this->data["subview"] = "exam/edit";
						$this->load->view('_layout_main', $this->data);
					}
				} else {
					$this->data["subview"] = "error";
					$this->load->view('_layout_main', $this->data);	
				}
			} else {
				$this->data["subview"] = "error";
				$this->load->view('_layout_main', $this->data);
			}	
		} else {
			$this->data["subview"] = "error";
			$this->load->view('_layout_main', $this->data);
		}
	}

	public function delete() {
		$usertype = $this->session->userdata("usertype");
		$schoolID = $this->session->userdata('schoolID');
		if($usertype == "Admin" || $usertype == "Super Admin") {
			$id = htmlentities($this->db->escape_str($this->uri->segment(3)));
			if((int)$id) {
				$this->data['exam'] = $this->exam_m->get_single_exam(array("schoolID" => $schoolID, 'examID' => $id));
				if($this->data['exam']) {
					$this->exam_m->delete_exam($id);
					$this->session->set_flashdata('success', $this->lang->line('menu_success'));
					redirect(base_url("exam/index"));
				} else {
					redirect(base_url("exam/index"));
				}
			} else {
				redirect(base_url("exam/index"));
			}
		} else {
			$this->data["subview"] = "error";
			$this->load->view('_layout_main', $this->data);
		}	
	}

	public function unique_exam() {
		$id = htmlentities($this->db->escape_str($this->uri->segment(3)));
		$schoolID = $this->session->userdata('schoolID');
		if((int)$id) {
			$exam = $this->exam_m->get_order_by_exam(array('schoolID' => $schoolID, "exam" => $this->input->post("exam"), "examID !=" => $id));
			if(count($exam)) {
				$this->form_validation->set_message("unique_exam", "%s already exists");
				return FALSE;
			}
			return TRUE;
		} else {
			$exam = $this->exam_m->get_order_by_exam(array('schoolID' => $schoolID, "exam" => $this->input->post("exam")));
			if(count($exam)) {
				$this->form_validation->set_message("unique_exam", "%s already exists");
				return FALSE;
			}
			return TRUE;
		}	
	}

	function date_valid($date) {
	  	if(strlen($date) <10) {
			$this->form_validation->set_message("date_valid", "%s is not valid dd-mm-yyyy");
	     	return FALSE;
		} else {
	   		$arr = explode("-", $date);   
	        $dd = $arr[0];            
	        $mm = $arr[1];              
	        $yyyy = $arr[2];
	      	if(checkdate($mm, $dd, $yyyy)) {
	      		return TRUE;
	      	} else {
	      		$this->form_validation->set_message("date_valid", "%s is not valid dd-mm-yyyy");
	     		return FALSE;
	      	}
	    } 

	} 
}

/* End of file exam.php */
/* Location: .//D/xampp/htdocs/school/mvc/controllers/exam.php */