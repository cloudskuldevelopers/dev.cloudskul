<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Bank_account extends MY_Controller {
/*
| -----------------------------------------------------
| PRODUCT NAME: 	CloudSkul
| -----------------------------------------------------
| AUTHOR:			CLOUDSKUL TEAM
| -----------------------------------------------------
| EMAIL:			info@cloudskul.com
| -----------------------------------------------------
| COPYRIGHT:		RESERVED BY Cloudskul
| -----------------------------------------------------
| WEBSITE:			http://cloudskul.com
| -----------------------------------------------------
*/
	function __construct() {
		parent::__construct();
		$this->load->model("bankaccount_m");
		$this->load->model("cloudaccount_m");
		$this->load->model("school_m");
		$this->load->model("systemadmin_m");
		$language = $this->session->userdata('lang');
		$this->lang->load('bankaccount', $language);	
	}

	public function index() {
		$usertype = $this->session->userdata("usertype");
		$schoolID = $this->session->userdata('schoolID');
		if($usertype == "Super Admin") {
			$this->data['accounts'] = $this->cloudaccount_m->get_order_by_school();
			$this->data['school_accounts'] = $this->bankaccount_m->allaccount();
			//var_dump($this->data['accounts']);
			$this->data["subview"] = "bankaccount/index";
			$this->load->view('_layout_main', $this->data);
		}elseif($usertype == "Admin") {
			$this->data['accounts'] = $this->bankaccount_m->get_order_by_school(array("schoolID" => $schoolID));
			$this->data["subview"] = "bankaccount/index";
			$this->load->view('_layout_main', $this->data);
		} else {
			$this->data["subview"] = "error";
			$this->load->view('_layout_main', $this->data);
		}
	}

	protected function rules() {
		$rules = array(
			array(
				'field' => 'account_name', 
				'label' => $this->lang->line("account_name"), 
				'rules' => 'trim|required|xss_clean|max_length[200]'
			),
			array(
				'field' => 'account_number', 
				'label' => $this->lang->line("account_number"), 
				'rules' => 'trim|required|xss_clean|min_length[5]|max_length[11]|callback_unique_account'
			),
			array(
				'field' => 'bank_code', 
				'label' => $this->lang->line("bank_code"), 
				'rules' => 'trim|required|xss_clean|min_length[2]|max_length[10]'
			),array(
				'field' => 'bank_name', 
				'label' => $this->lang->line("bank_name"), 
				'rules' => 'trim|required|xss_clean|max_length[100]'
			),
			array(
				'field' => 'account_type', 
				'label' => $this->lang->line("account_type"), 
				'rules' => 'trim|required|xss_clean|max_length[25]'
			)
		);
		return $rules;
	}

	function insert_with_image($username) {
	    $random = rand(1, 10000000000000000);
	    $makeRandom = hash('sha512', $random. $username . config_item("encryption_key"));
	    return $makeRandom;
	}

	function bankname() {
		$bankname = $this->input->post('bank_name');
		if($bankname) {
			$allbanks = $this->bankaccount_m->allbanks($bankname);
			
			foreach ($allbanks as $allbank) {
				echo "<li id='". $allbank->bank_code ."'>".$allbank->bank_name."</li>";
			}
		}
	}
	
	public function addaccount(){
		//get user session
		$usertype = $this->session->userdata("usertype");
		$schoolID = $this->session->userdata('schoolID');
		
		if($usertype == "Super Admin" || $usertype == "Admin") {
				if($_POST) {
				$rules = $this->rules();
				$this->form_validation->set_rules($rules);
				if ($this->form_validation->run() == FALSE) {
					$this->data['form_validation'] = validation_errors(); 
					$this->data["subview"] = "bankaccount/addaccount";
					$this->load->view('_layout_main', $this->data);			
				} else {
					$array = array();
					$array["account_name"] = $this->input->post("account_name");
					$array["bank_name"] = $this->input->post("bank_name");
					$array["account_number"] = $this->input->post("account_number");
					$array["account_type"] = $this->input->post("account_type");
					$array["bank_code"] = $this->input->post("bank_code");
					$array['schoolID'] = $schoolID;
					$array["create_date"] = date("Y-m-d h:i:s");
					$array["modify_date"] = date("Y-m-d h:i:s");
					$array["create_userID"] = $this->session->userdata('loginuserID');
					$array["create_username"] = $this->session->userdata('username');
					$array["create_usertype"] = $this->session->userdata('usertype');
					
					$this->bankaccount_m->insert_school($array);
					$this->session->set_flashdata('success', $this->lang->line('menu_success'));
					redirect(base_url("bank_account/index"));
				}

			} else {
				$this->data["subview"] = "bankaccount/addaccount";
				$this->load->view('_layout_main', $this->data);
			}
		} else {
			//load error view
			$this->data["subview"] = "error";
			$this->load->view('_layout_main', $this->data);
		}
	}

	//duplicate add function
	/*public function add() {
		$schoolID = $this->session->userdata('schoolID');
		$usertype = $this->session->userdata("usertype");
		if($usertype == "Super Admin" || $usertype == "Admin") {
			if($_POST) {
				$rules = $this->rules();
				$this->form_validation->set_rules($rules);
				if ($this->form_validation->run() == FALSE) {
					$this->data['form_validation'] = validation_errors(); 
					$this->data["subview"] = "bankaccount/addaccount";
					$this->load->view('_layout_main', $this->data);			
				} else {
					$array = array();
					$array["account_name"] = $this->input->post("account_name");
					$array["bank_name"] = $this->input->post("bank_name");
					$array["account_number"] = $this->input->post("account_number");
					$array["account_type"] = $this->input->post("account_type");
					$array["bank_code"] = $this->input->post("bank_code");
					$array['schoolID'] = $schoolID;
					$array["create_date"] = date("Y-m-d h:i:s");
					$array["modify_date"] = date("Y-m-d h:i:s");
					$array["create_userID"] = $this->session->userdata('loginuserID');
					$array["create_username"] = $this->session->userdata('username');
					$array["create_usertype"] = $this->session->userdata('usertype');

					$this->bankaccount_m->insert_school($array);
					$this->session->set_flashdata('success', $this->lang->line('menu_success'));
					redirect(base_url("bank_account/index"));
				}

			} else {
				$this->data["subview"] = "bankaccount/addaccount";
				$this->load->view('_layout_main', $this->data);
			}
		} else {
			$this->data["subview"] = "error";
			$this->load->view('_layout_main', $this->data);
		}
	}*/

	public function edit() {
		$usertype = $this->session->userdata("usertype");
		$schoolID = $this->session->userdata('schoolID');
		if($usertype == "Super Admin" || $usertype == "Admin") {
			$id = htmlentities($this->db->escape_str($this->uri->segment(3)));
			if((int)$id) {
				$this->data['account'] = $this->bankaccount_m->get_school(array("schoolID"=>$schoolID,"accountID"=>$id));
				var_dump($this->data['account']);
				exit;
				if($this->data['account']) {
					$sid = $this->data['account']->schoolID; //schoolID
					$this->data['school_name'] = $this->school_m->get_school($sid)->school;
					
					if($_POST) {
						$rules = $this->rules();
						$this->form_validation->set_rules($rules);
						if ($this->form_validation->run() == FALSE) {
							$this->data["subview"] = "bankaccount/edit";
							$this->load->view('_layout_main', $this->data);			
						} else {
							$array = array();
							$array["account_name"] = $this->input->post("account_name");
							$array["bank_name"] = $this->input->post("bank_name");
							$array["account_number"] = $this->input->post("account_number");
							$array["account_type"] = $this->input->post("account_type");
							$array["bank_code"] = $this->input->post("bank_code");
							$array["modify_date"] = date("Y-m-d h:i:s");

							$this->bankaccount_m->update_school($array, $id);
							$this->session->set_flashdata('success', $this->lang->line('menu_success'));
							redirect(base_url("bank_account/index"));
							
						}
					} else {
						
						$this->data["subview"] = "bankaccount/edit";
						$this->load->view('_layout_main', $this->data);
					}
				} else {
					$this->data["subview"] = "error";
					$this->load->view('_layout_main', $this->data);	
				}
			} else {
				$this->data["subview"] = "error";
				$this->load->view('_layout_main', $this->data);
			}	
		} else {
			$this->data["subview"] = "error";
			$this->load->view('_layout_main', $this->data);
		}
	}
	
	public function admin_edit() {
		$usertype = $this->session->userdata("usertype");
		if($usertype == "Super Admin") {
			$id = htmlentities($this->db->escape_str($this->uri->segment(3)));
			if((int)$id) {
				$this->data['account'] =  $this->cloudaccount_m->get_account($id);
				if($this->data['account']) {
					//var_dump($this->data['account']);
					$sid = $this->data['account']->accountID; //schoolID
					$this->data['school_name'] = 'Cloudskul';//$this->school_m->get_school($sid)->school; //needs revision
					
					if($_POST) {
						$rules = $this->rules();
						$this->form_validation->set_rules($rules);
						if ($this->form_validation->run() == FALSE) {
							$this->data["subview"] = "bankaccount/edit";
							$this->load->view('_layout_main', $this->data);			
						} else {
							$array = array();
							$array["account_name"] = $this->input->post("account_name");
							$array["bank_name"] = $this->input->post("bank_name");
							$array["account_number"] = $this->input->post("account_number");
							$array["account_type"] = $this->input->post("account_type");
							$array["bank_code"] = $this->input->post("bank_code");
							$array["modify_date"] = date("Y-m-d h:i:s");

							$this->cloudaccount_m->update_school($array, $id);
							$this->session->set_flashdata('success', $this->lang->line('menu_success'));
							redirect(base_url("bank_account/index"));
							
						}
					} else {
						
						$this->data["subview"] = "bankaccount/edit";
						$this->load->view('_layout_main', $this->data);
					}
				} else {
					$this->data["subview"] = "error";
					$this->load->view('_layout_main', $this->data);	
				}
			} else {
				$this->data["subview"] = "error";
				$this->load->view('_layout_main', $this->data);
			}	
		} else {
			$this->data["subview"] = "error";
			$this->load->view('_layout_main', $this->data);
		}
	}

	public function delete() {
		$usertype = $this->session->userdata("usertype");
		$schoolID = $this->session->userdata('schoolID');
		if($usertype == "Super Admin" || $usertype == "Admin") {
			$id = htmlentities($this->db->escape_str($this->uri->segment(3)));
			if((int)$id) {
				$this->data['account'] = $this->bankaccount_m->get_school(array("schoolID"=>$schoolID,"accountID"=>$id)); //get account details
				if($this->data['account']){
					$this->bankaccount_m->delete_account($id);
					$this->session->set_flashdata('success', $this->lang->line('menu_success'));
					redirect(base_url("bank_account/index"));
				}else{
					$this->session->set_flashdata('error', $this->lang->line('school_error'));
					redirect(base_url("bank_account/index"));
				}
			} else {
				redirect(base_url("bank_account/index"));
			}
		} else {
			$this->data["subview"] = "error";
			$this->load->view('_layout_main', $this->data);
		}	
	}

	public function view() {
		$usertype = $this->session->userdata("usertype");
		if($usertype == "Super Admin" || $usertype == "Admin" ) {
			$id = htmlentities($this->db->escape_str($this->uri->segment(3)));
			if ((int)$id) {
				$this->data["school"] = $this->school_m->get_school($id);
				if($this->data["school"]) {
					$this->data["user"] = $this->systemadmin_m->get_systemadmin($this->data["school"]->create_userID);
					$this->data["subview"] = "school/view";
					$this->load->view('_layout_main', $this->data);
				} else {
					$this->data["subview"] = "error";
					$this->load->view('_layout_main', $this->data);
				}
			} else {
				$this->data["subview"] = "error";
				$this->load->view('_layout_main', $this->data);
			}
		} else {
			$this->data["subview"] = "error";
			$this->load->view('_layout_main', $this->data);
		}
	}

	//needs revision, paused atm
	public function admin_view() {
		$usertype = $this->session->userdata("usertype");
		if($usertype == "Super Admin" || $usertype == "Admin" ) {
			$id = htmlentities($this->db->escape_str($this->uri->segment(3)));
			if ((int)$id) {
				$this->data["school"] = $this->cloudaccount_m->get_order_by_school();
				if($this->data["school"]) {
					$this->data["user"] = $this->systemadmin_m->get_systemadmin($this->data["school"]->create_userID);
					$this->data["subview"] = "school/view";
					$this->load->view('_layout_main', $this->data);
				} else {
					$this->data["subview"] = "error";
					$this->load->view('_layout_main', $this->data);
				}
			} else {
				$this->data["subview"] = "error";
				$this->load->view('_layout_main', $this->data);
			}
		} else {
			$this->data["subview"] = "error";
			$this->load->view('_layout_main', $this->data);
		}
	}

	public function print_preview() {
		$usertype = $this->session->userdata("usertype");
		if($usertype == "Super Admin" || $usertype == "Admin") {
			$id = htmlentities($this->db->escape_str($this->uri->segment(3)));
			$url = htmlentities($this->db->escape_str($this->uri->segment(4)));
			if((int)$id) {
				$this->data["school"] = $this->school_m->get_school($id);
				if($this->data["school"]) {
					$this->data["user"] = $this->systemadmin_m->get_systemadmin($this->data["school"]->create_userID);
					$this->load->library('html2pdf');
				    $this->html2pdf->folder('./assets/pdfs/');
				    $this->html2pdf->filename('Report.pdf');
				    $this->html2pdf->paper('a4', 'portrait');
				    $this->data['panel_title'] = $this->lang->line('panel_title');
				    $html = $this->load->view('school/print_preview', $this->data, true);
					$this->html2pdf->html($html);
					$this->html2pdf->create();
				} else {
					$this->data["subview"] = "error";
					$this->load->view('_layout_main', $this->data);
				}    
			} else {
				$this->data["subview"] = "error";
				$this->load->view('_layout_main', $this->data);
			}
		} else {
			$this->data["subview"] = "error";
			$this->load->view('_layout_main', $this->data);
		}
	}

	public function send_mail() {
		$usertype = $this->session->userdata("usertype");
		if($usertype == "Super Admin" || $usertype == "Admin") {
			$id = $this->input->post('id');
			if ((int)$id) {
				$this->data["school"] = $this->school_m->get_school($id);
				if($this->data["school"]) {
					$this->data["user"] = $this->systemadmin_m->get_systemadmin($this->data["school"]->create_userID);
				    $this->load->library('html2pdf');
				    $this->html2pdf->folder('uploads/report');
				    $this->html2pdf->filename('Report.pdf');
				    $this->html2pdf->paper('a4', 'portrait');
				    $this->data['panel_title'] = $this->lang->line('panel_title');
					$html = $this->load->view('school/print_preview', $this->data, true);
					$this->html2pdf->html($html);
					$this->html2pdf->create('save');
						
					if($path = $this->html2pdf->create('save')) {
					$this->load->library('email');
					$this->email->set_mailtype("html");
					$this->email->from($this->data["siteinfos"]->email, $this->data['siteinfos']->sname);
					$this->email->to($this->input->post('to'));
					$this->email->subject($this->input->post('subject'));
					$this->email->message($this->input->post('message'));	
					$this->email->attach($path);
						if($this->email->send()) {
							$this->session->set_flashdata('success', $this->lang->line('mail_success'));
						} else {
							$this->session->set_flashdata('error', $this->lang->line('mail_error'));
						}
					}
				} else {
					$this->data["subview"] = "error";
					$this->load->view('_layout_main', $this->data);
				}
			} else {
				$this->data["subview"] = "error";
				$this->load->view('_layout_main', $this->data);
			}
		} else {
			$this->data["subview"] = "error";
			$this->load->view('_layout_main', $this->data);
		}
	}

	public function unique_school() {
		$id = htmlentities($this->db->escape_str($this->uri->segment(3)));
		if((int)$id) {
			$school = $this->bankaccount_m->get_order_by_school(array("account_name" => $this->input->post("account_name"), "accountID !=" => $id));
			if(count($school)) {
				$this->form_validation->set_message("unique_school", "%s already exists");
				return FALSE;
			}
			return TRUE;
		} else {
			$school = $this->school_m->get_order_by_school(array("account_name" => $this->input->post("account_name")));

			if(count($school)) {
				$this->form_validation->set_message("unique_school", "%s already exists");
				return FALSE;
			}
			return TRUE;
		}	
	}

	public function unique_account() {
		$id = htmlentities($this->db->escape_str($this->uri->segment(3)));
		if((int)$id) {
			$school = $this->bankaccount_m->get_order_by_school(array("account_number" => $this->input->post("account_number"), "accountID != " => $id));
			//var_dump($school);
			if(count($school)) {
				$this->form_validation->set_message("unique_account", "%s already exists");
				return FALSE;
			}
			return TRUE;
		} else {
			$school = $this->bankaccount_m->get_order_by_school(array("account_number" => $this->input->post("account_number")));

			if(count($school)) {
				$this->form_validation->set_message("unique_account", "%s already exists");
				return FALSE;
			}
			return TRUE;
		}	
	}

	public function unique_day() {
		$day = $this->input->post('automation');
		if((int)$day) {
			if($day < 0 || $day > 28) {
				$this->form_validation->set_message("unique_day", "%s already exists");
				return FALSE;
			}
			return TRUE;
		} else {
			$this->form_validation->set_message("unique_day", "%s already exists");
			return FALSE;
		}	
	}

	function active() {
		$usertype = $this->session->userdata("usertype");
		if($usertype == "Super Admin") {
			$id = $this->input->post('id');
			$status = $this->input->post('status');
			if($id != '' && $status != '') {
				if((int)$id) {
					if($status == 'chacked') {
						$this->school_m->update_school(array('schoolactive' => 1), $id);
						echo 'Success';
					} elseif($status == 'unchacked') {
						$this->school_m->update_school(array('schoolactive' => 0), $id);
						echo 'Success';
					} else {
						echo "Error";
					}
				} else {
					echo "Error";
				}
			} else {
				echo "Error";
			}
		} else {
			echo "Error";
		}
	}

	function activeschoolnow($schoolID, $schoollogo) {
		$usertype = $this->session->userdata("usertype");
		if($usertype == "Super Admin") {
			$school = $this->school_m->get_school($schoolID);
			$data = array('schoolID' => $schoolID, 'school' => $school->school, 'schoollogo' => $schoollogo);
			$this->session->set_userdata($data);
			redirect('dashboard/index');
		} else {
			redirect($_SERVER['HTTP_REFERER']);
		}
	}

	function allschoolnow() {
		$usertype = $this->session->userdata("usertype");
		if($usertype == "Super Admin") {
			$this->data["subview"] = "school/allschoolnow";
			$this->load->view('_layout_main', $this->data);		
		} else {
			$this->data["subview"] = "error";
			$this->load->view('_layout_main', $this->data);
		}
	}

}

/* End of file exam.php */
/* Location: .//D/xampp/htdocs/school/mvc/controllers/exam.php */