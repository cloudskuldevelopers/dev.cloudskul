<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Donation extends MY_Controller {
/*
| -----------------------------------------------------
| PRODUCT NAME: 	CloudSkul
| -----------------------------------------------------
| AUTHOR:			CLOUDSKUL TEAM
| -----------------------------------------------------
| EMAIL:			info@cloudskul.com
| -----------------------------------------------------
| COPYRIGHT:		RESERVED BY Cloudskul
| -----------------------------------------------------
| WEBSITE:			http://cloudskul.com
| -----------------------------------------------------
*/
	
	function __construct(){
		parent::__construct();
		$this->load->model("donor_m");
		$this->load->model("donation_m");
		$this->load->model("donation_remita_m");
		$this->load->library("remita");
		$this->load->library("ozioma");
		$this->load->model("payment_settings_m");
	}
	
	public function index(){
		$this->load->view('donation/index');
	}

	public function payment_fail(){
		$this->load->view('donation/payment_fail');
	}

	public function success(){
		$id = htmlentities($this->db->escape_str($this->uri->segment(3)));
		if((int)$id) {
			$this->data["title"] = "Cloudskul ::: Donation";
			$donation = $this->donation_m->get_single_donation(array('donateID' => $id ));
			$this->data["donor"] = $this->donor_m->get_single_donor(array("donorID"=> $donation->donorID));
			$this->data["remita"] = $this->donation_remita_m->get_single_donation(array("donationID" => $id));
			$this->data["donation"] = $donation;
			$this->load->view('donation/payment_success',$this->data);
		}
	}
	
	function ValidateData()
	{
		$this->load->library('form_validation');
		$this->form_validation->set_rules();
	}

	public function payment(){

		if($_POST){
			$fname = $this->input->post("fname");
			$lname = $this->input->post("lname");
			$amount = $this->input->post('amount');
			$email = $this->input->post('email');
			$phone = $this->input->post('phone');
			$address = $this->input->post('address');
			$note = $this->input->post('note') == "Additional Note" ? NULL : $this->input->post('note');
			$currency = $this->input->post('currency');
			$donationType = $this->input->post('donationType');

			$array = array(
				"donor_fname" => $fname,
				"donor_lname" => $lname,
				"donor_email" => $email,
				"donor_phone" => $phone,
				"donor_address" => $address,
				"note" 		  => $note,
				"create_date" => Date("Y-m-d"),
				"status"      => "Not paid"
				);
			$returnID = $this->donor_m->insert_donor($array);
			//$this->input->post("id") = $returnID;
			$arr = $this->input->post();
			$arr["id"] = $returnID;
			$this->post_data = $arr;
			$this->session->set_userdata("post", $this->post_data);
			//var_dump($this->post_data);
			if(!is_null($returnID))
				//$this->remita();
				echo $returnID;
		}else{
			redirect($_SERVER['HTTP_REFERER']);
		}

	}

	public function make_payment(){
		$id = htmlentities($this->db->escape_str($this->uri->segment(3)));
		if((int)$id) {
			//var_dump($this->session->userdata("post"));
			$this->post_data = $this->session->userdata("post");
			$this->remita();
		}
	}

	private function remita(){
		$api_config = array();
		$get_configs = $this->payment_settings_m->get_order_by_config();
		foreach ($get_configs as $key => $get_key) {
			$api_config[$get_key->config_key] = $get_key->value;
		}
		$this->data['set_key'] = $api_config;
		if($api_config['service_type_id'] =="" || $api_config['merchant_id'] =="" || $api_config['api_key']==""){
			$this->session->set_flashdata('error', 'Remita settings not available');
			redirect($_SERVER['HTTP_REFERER']);
		}else{
			$params = array(
	  		'cancelUrl' 	=> base_url('donation/index'),
	  		'returnUrl' 	=> base_url('donation/payment_success'),
	  		"amount"        => $this->post_data['amount'],
	  		"id"        	=> $this->post_data['id']
	  		);
			//redirect($_SERVER['HTTP_REFERER']);
			
	  		$this->session->set_userdata("params", $params);
	  		$params1 = $this->session->userdata("params");
	  		session_start();
	  		$_SESSION["id"] = $params1["id"];
	  		//var_dump($params1); 
			//die;
	  		//$this->remita->set_responseurl(base_url('donation/payment_success'));
	  		$this->remita->set_responseurl($params["returnUrl"]);

	  		$this->remita->set_payer_phone($this->post_data['phone']);
	  		$this->remita->set_payer_name($this->post_data['lname'].", ".$this->post_data['fname']);
	        $this->remita->set_payer_email($this->post_data['email']);
	        $this->remita->set_payment_descr($this->post_data['donationType']);
	        $this->remita->set_total_amount($this->post_data['amount']);//set total amount
	        $account_cloudskul = $this->payment_settings_m->get_account_cloudskul();
	        $this->remita->set_orderID(DATE("dmyHis"));
	        $this->remita->set_beneficiary_cloud($account_cloudskul);
	        $this->remita->set_payment_type("Interswitch");//set payment type
	        $this->remita->getSettings(); //function to obtain settings from database

	        $execute = $this->remita->execute_donation();
		}

		
        //$this->remita->set_cloudskul_fee($this->post_data['service_charge']);
	}

	public function payment_success(){
		$params = $this->session->userdata("post");
		//var_dump($this->session->all_userdata());
		//var_dump(uri_string());
		session_start();
		$id = $_SESSION["id"];
		$orderID  = $_GET['orderID'];
		$rrr  = $_GET['RRR'];
		$remitaResponse = $this->remita->remita_transaction_details($orderID);
		$message = $remitaResponse['message'];
		//var_dump($remitaResponse) and die("dev in process");
		$this->session->unset_userdata('params');
		if(isset($orderID) && $remitaResponse['message'] === 'Approved' && $remitaResponse['status'] === '01' ) {
  			// Response
  			if ($orderID) {

  				$payArr = array(
  					"donorID" => $id,
  					"amount" => $remitaResponse["amount"],
  					"currency" => "NGR",
  					"payment_type" => "Remita",
  					"create_date" => Date("Y-m-d"),
  					);
  				$donation_ID = $this->donation_m->insert_donation($payArr);

  				$this->donor_m->update_donor(array('status' => "paid"), $id);

  				$remitaArr = array(
  					"donationID" => $donation_ID,
  					"rrr" => $rrr,
  					"orderID" => $orderID,
  					);

  				$this->donation_remita_m->insert_donation($remitaArr);
  				$this->session->set_flashdata('success', $this->lang->line('menu_success'));
				session_destroy();
  			}else {
  				$this->session->set_flashdata('error', 'Error generating Payment reference!');
  				redirect(base_url("donation/payment_fail"));
  			}
  			redirect(base_url("donation/success/".$donation_ID));
  		}else{
  			$this->session->set_flashdata('error', 'Sorry, your payment was not successful!');
  			redirect(base_url("donation/payment_fail"));
  		}
	}
}

/* End of file donation.php */
/* Location: .//D/xampp/htdocs/school/mvc/controllers/donation.php */