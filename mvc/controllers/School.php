<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class School extends MY_Controller {
/*
| -----------------------------------------------------
| PRODUCT NAME: 	CloudSkul
| -----------------------------------------------------
| AUTHOR:			CLOUDSKUL TEAM
| -----------------------------------------------------
| EMAIL:			info@cloudskul.com
| -----------------------------------------------------
| COPYRIGHT:		RESERVED BY Cloudskul
| -----------------------------------------------------
| WEBSITE:			http://cloudskul.com
| -----------------------------------------------------
*/
	function __construct() {
		parent::__construct();
		$this->load->model("school_m");
		$this->load->model("bankaccount_m");
		$this->load->model("systemadmin_m");
		$language = $this->session->userdata('lang');
		$this->lang->load('school', $language);	
	}

	public function index() {
		$usertype = $this->session->userdata("usertype");
		$schoolID = $this->session->userdata('schoolID');
		if($usertype == "Super Admin") {
			$this->data['schools'] = $this->school_m->get_order_by_school();
			$this->data["subview"] = "school/index";
			$this->load->view('_layout_main', $this->data);
		}elseif($usertype == "Admin") {
			$this->data['schools'] = $this->school_m->get_order_by_school(array("schoolID" => $schoolID));
			$this->data["subview"] = "school/index";
			$this->load->view('_layout_main', $this->data);
		} else {
			$this->data["subview"] = "error";
			$this->load->view('_layout_main', $this->data);
		}
	}

	protected function rules() {
		$rules = array(
			array(
				'field' => 'school', 
				'label' => $this->lang->line("school_school"), 
				'rules' => 'trim|required|xss_clean|max_length[200]|callback_unique_school'
			),
			array(
				'field' => 'alias', 
				'label' => $this->lang->line("school_alias"), 
				'rules' => 'trim|required|xss_clean|max_length[200]'
			),
			array(
				'field' => 'email', 
				'label' => $this->lang->line("school_email"), 
				'rules' => 'trim|required|xss_clean|valid_email|max_length[40]'
			),
			array(
				'field' => 'phone', 
				'label' => $this->lang->line("school_phone"), 
				'rules' => 'trim|required|xss_clean|min_length[5]|max_length[25]'
			),
			array(
				'field' => 'capacity', 
				'label' => $this->lang->line("school_capacity"), 
				'rules' => 'trim|required|xss_clean|numeric|max_length[11]'
			),
			array(
				'field' => 'automation', 
				'label' => $this->lang->line("school_automation"),
				'rules' => 'trim|required|max_length[2]|xss_clean|numeric|callback_unique_day'
			), 
			array(
				'field' => 'currency_code', 
				'label' => $this->lang->line("school_currency_code"), 
				'rules' => 'trim|required|max_length[11]|xss_clean'
			),
			array(
				'field' => 'currency_symbol', 
				'label' => $this->lang->line("school_currency_symbol"), 
				'rules' => 'trim|required|max_length[3]|xss_clean'
			),
			array(
				'field' => 'footer', 
				'label' => $this->lang->line("school_footer"), 
				'rules' => 'trim|required|max_length[200]|xss_clean'
			),
			array(
				'field' => 'address', 
				'label' => $this->lang->line("school_address"), 
				'rules' => 'trim|required|max_length[200]|xss_clean'
			),
			array(
				'field' => 'photo', 
				'label' => $this->lang->line("school_logo"), 
				'rules' => 'trim|max_length[200]|xss_clean'
			)
		);
		return $rules;
	}

	function insert_with_image($username) {
	    $random = rand(1, 10000000000000000);
	    $makeRandom = hash('sha512', $random. $username . config_item("encryption_key"));
	    return $makeRandom;
	}

	public function add() {
		$this->load->library('setsmssetting');


		// $smsgetways = $this->setsmssetting->set_sms_setting(5);
		// $data = array();
		// foreach ($smsgetways as $key => $smsgetway) {
		// 	$data = $smsgetway;
		// 	$this->db->insert_batch('smssettings', $data); 
		// }

		

		$usertype = $this->session->userdata("usertype");
		if($usertype == "Super Admin") {
			if($_POST) {
				$rules = $this->rules();
				$this->form_validation->set_rules($rules);
				if ($this->form_validation->run() == FALSE) {
					$this->data['form_validation'] = validation_errors(); 
					$this->data["subview"] = "school/add";
					$this->load->view('_layout_main', $this->data);			
				} else {
					$array = array();
					$array["school"] = $this->input->post("school");
					$array["alias"] = $this->input->post("alias");
					$array["email"] = $this->input->post("email");
					$array["phone"] = $this->input->post("phone");
					$array["capacity"] = $this->input->post("capacity");
					$array["automation"] = $this->input->post("automation");
					$array["currency_code"] = $this->input->post("currency_code");
					$array["currency_symbol"] = $this->input->post("currency_symbol");
					$array["footer"] = $this->input->post("footer");
					$array["address"] = $this->input->post("address");
					$array["create_date"] = date("Y-m-d h:i:s");
					$array["modify_date"] = date("Y-m-d h:i:s");
					$array["create_userID"] = $this->session->userdata('loginuserID');
					$array["create_username"] = $this->session->userdata('username');
					$array["create_usertype"] = $this->session->userdata('usertype');
					$array["schoolactive"] = 1;

					$new_file = "site.png";
					if($_FILES["image"]['name'] !="") {
						$file_name = $_FILES["image"]['name'];
						$file_name_rename = $this->insert_with_image($this->input->post("school"));
			            $explode = explode('.', $file_name);
			            if(count($explode) >= 2) {
				            $new_file = $file_name_rename.'.'.$explode[1];
							$config['upload_path'] = "./uploads/images";
							$config['allowed_types'] = "gif|jpg|png";
							$config['file_name'] = $new_file;
							$config['max_size'] = '1024';
							$config['max_width'] = '3000';
							$config['max_height'] = '3000';
							$array['photo'] = $new_file;
							$this->load->library('upload', $config);
							if(!$this->upload->do_upload("image")) {
								$this->data["image"] = $this->upload->display_errors();
								$this->data["subview"] = "school/add";
								$this->load->view('_layout_main', $this->data);
							} else {
								$data = array("upload_data" => $this->upload->data());
								$returnID = $this->school_m->insert_school($array);

								$arr['schoolID'] = $returnID;
								$arr["create_date"] = date("Y-m-d h:i:s");
								$arr["modify_date"] = date("Y-m-d h:i:s");
								$arr["create_userID"] = $this->session->userdata('loginuserID');
								$arr["create_username"] = $this->session->userdata('username');
								$arr["create_usertype"] = $this->session->userdata('usertype');
								$this->bankaccount_m->insert_school($arr);

								$smsgetways = $this->setsmssetting->set_sms_setting($returnID);
								$data = array();
								foreach ($smsgetways as $key => $smsgetway) {
									$data = $smsgetway;
									$this->db->insert_batch('smssettings', $data); 
								}
								$this->session->set_flashdata('success', $this->lang->line('menu_success'));
								redirect(base_url("school/index"));
							}
						} else {
							$this->data["image"] = "Invalid file";
							$this->data["subview"] = "school/add";
							$this->load->view('_layout_main', $this->data);
						}
					} else {
						$array["photo"] = $new_file;
						$returnID = $this->school_m->insert_school($array);
						//start insert school id into accounts table
						$arr['schoolID'] = $returnID;
						$arr["create_date"] = date("Y-m-d h:i:s");
						$arr["modify_date"] = date("Y-m-d h:i:s");
						$arr["create_userID"] = $this->session->userdata('loginuserID');
						$arr["create_username"] = $this->session->userdata('username');
						$arr["create_usertype"] = $this->session->userdata('usertype');
						$this->bankaccount_m->insert_school($arr);
						//end insert into accunts table
						$smsgetways = $this->setsmssetting->set_sms_setting($returnID);
						$data = array();
						foreach ($smsgetways as $key => $smsgetway) {
							$data = $smsgetway;
							$this->db->insert_batch('smssettings', $data); 
						}
						$this->session->set_flashdata('success', $this->lang->line('menu_success'));
						redirect(base_url("school/index"));
					}
				}
			} else {
				$this->data["subview"] = "school/add";
				$this->load->view('_layout_main', $this->data);
			}
		} else {
			$this->data["subview"] = "error";
			$this->load->view('_layout_main', $this->data);
		}
	}

	public function edit() {
		$usertype = $this->session->userdata("usertype");
		if($usertype == "Super Admin" || $usertype == "Admin") {
			$id = htmlentities($this->db->escape_str($this->uri->segment(3)));
			if((int)$id) {
				$this->data['school'] = $this->school_m->get_school($id);
				if($this->data['school']) {
					if($_POST) {
						$rules = $this->rules();
						$this->form_validation->set_rules($rules);
						if ($this->form_validation->run() == FALSE) {
							$this->data["subview"] = "school/edit";
							$this->load->view('_layout_main', $this->data);			
						} else {
							$array = array();
							$array["school"] = $this->input->post("school");
							$array["alias"] = $this->input->post("alias");
							$array["email"] = $this->input->post("email");
							$array["phone"] = $this->input->post("phone");
							$array["capacity"] = $this->input->post("capacity");
							$array["automation"] = $this->input->post("automation");
							$array["currency_code"] = $this->input->post("currency_code");
							$array["currency_symbol"] = $this->input->post("currency_symbol");
							$array["footer"] = $this->input->post("footer");
							$array["address"] = $this->input->post("address");
							$array["modify_date"] = date("Y-m-d h:i:s");

							if($_FILES["image"]['name'] !="") {
								if($this->data['school']->photo != 'site.png') {
									unlink(FCPATH.'uploads/images/'.$this->data['school']->photo);
								}
								$file_name = $_FILES["image"]['name'];
								$file_name_rename = $this->insert_with_image($this->input->post("school"));
					            $explode = explode('.', $file_name);
					            if(count($explode) >= 2) {
						            $new_file = $file_name_rename.'.'.$explode[1];
									$config['upload_path'] = "./uploads/images";
									$config['allowed_types'] = "gif|jpg|png";
									$config['file_name'] = $new_file;
									$config['max_size'] = '1024';
									$config['max_width'] = '3000';
									$config['max_height'] = '3000';
									$array['photo'] = $new_file;
									$this->load->library('upload', $config);
									if(!$this->upload->do_upload("image")) {
										$this->data["image"] = $this->upload->display_errors();
										$this->data["subview"] = "school/edit";
										$this->load->view('_layout_main', $this->data);
									} else {
										$data = array("upload_data" => $this->upload->data());
										$this->school_m->update_school($array, $id);
										$this->session->set_flashdata('success', $this->lang->line('menu_success'));
										redirect(base_url("school/index"));
									}
								} else {
									$this->data["image"] = "Invalid file";
									$this->data["subview"] = "school/edit";
									$this->load->view('_layout_main', $this->data);
								}
							} else {
								$this->school_m->update_school($array, $id);
								$this->session->set_flashdata('success', $this->lang->line('menu_success'));
								redirect(base_url("school/index"));
							}
						}
					} else {
						$this->data["subview"] = "school/edit";
						$this->load->view('_layout_main', $this->data);
					}
				} else {
					$this->data["subview"] = "error";
					$this->load->view('_layout_main', $this->data);	
				}
			} else {
				$this->data["subview"] = "error";
				$this->load->view('_layout_main', $this->data);
			}	
		} else {
			$this->data["subview"] = "error";
			$this->load->view('_layout_main', $this->data);
		}
	}

	public function delete() {
		$usertype = $this->session->userdata("usertype");
		if($usertype == "Super Admin") {
			$id = htmlentities($this->db->escape_str($this->uri->segment(3)));
			$school = $this->school_m->get_school($id);
			$schools = $this->school_m->get_school();
			if(count($schools) != 1) {
				if((int)$id) {
					if($school->photo != 'site.png') {
						unlink(FCPATH.'uploads/images/'.$school->photo);
					}
					$this->school_m->delete_school($id);
					$this->session->set_flashdata('success', $this->lang->line('menu_success'));
					redirect(base_url("school/index"));
				} else {
					redirect(base_url("school/index"));
				}
			} else {
				$this->session->set_flashdata('error', $this->lang->line('school_error'));
				redirect(base_url("school/index"));
			}
			
		} else {
			$this->data["subview"] = "error";
			$this->load->view('_layout_main', $this->data);
		}	
	}

	public function view() {
		$usertype = $this->session->userdata("usertype");
		if($usertype == "Super Admin" || $usertype == "Admin" ) {
			$id = htmlentities($this->db->escape_str($this->uri->segment(3)));
			if ((int)$id) {
				$this->data["school"] = $this->school_m->get_school($id);
				if($this->data["school"]) {
					$this->data["user"] = $this->systemadmin_m->get_systemadmin($this->data["school"]->create_userID);
					$this->data["subview"] = "school/view";
					$this->load->view('_layout_main', $this->data);
				} else {
					$this->data["subview"] = "error";
					$this->load->view('_layout_main', $this->data);
				}
			} else {
				$this->data["subview"] = "error";
				$this->load->view('_layout_main', $this->data);
			}
		} else {
			$this->data["subview"] = "error";
			$this->load->view('_layout_main', $this->data);
		}
	}

	public function print_preview() {
		$usertype = $this->session->userdata("usertype");
		if($usertype == "Super Admin" || $usertype == "Admin") {
			$id = htmlentities($this->db->escape_str($this->uri->segment(3)));
			$url = htmlentities($this->db->escape_str($this->uri->segment(4)));
			if((int)$id) {
				$this->data["school"] = $this->school_m->get_school($id);
				if($this->data["school"]) {
					$this->data["user"] = $this->systemadmin_m->get_systemadmin($this->data["school"]->create_userID);
					$this->load->library('html2pdf');
				    $this->html2pdf->folder('./assets/pdfs/');
				    $this->html2pdf->filename('Report.pdf');
				    $this->html2pdf->paper('a4', 'portrait');
				    $this->data['panel_title'] = $this->lang->line('panel_title');
				    $html = $this->load->view('school/print_preview', $this->data, true);
					$this->html2pdf->html($html);
					$this->html2pdf->create();
				} else {
					$this->data["subview"] = "error";
					$this->load->view('_layout_main', $this->data);
				}    
			} else {
				$this->data["subview"] = "error";
				$this->load->view('_layout_main', $this->data);
			}
		} else {
			$this->data["subview"] = "error";
			$this->load->view('_layout_main', $this->data);
		}
	}

	public function send_mail() {
		$usertype = $this->session->userdata("usertype");
		if($usertype == "Super Admin" || $usertype == "Admin") {
			$id = $this->input->post('id');
			if ((int)$id) {
				$this->data["school"] = $this->school_m->get_school($id);
				if($this->data["school"]) {
					$this->data["user"] = $this->systemadmin_m->get_systemadmin($this->data["school"]->create_userID);
				    $this->load->library('html2pdf');
				    $this->html2pdf->folder('uploads/report');
				    $this->html2pdf->filename('Report.pdf');
				    $this->html2pdf->paper('a4', 'portrait');
				    $this->data['panel_title'] = $this->lang->line('panel_title');
					$html = $this->load->view('school/print_preview', $this->data, true);
					$this->html2pdf->html($html);
					$this->html2pdf->create('save');
						
					if($path = $this->html2pdf->create('save')) {
					$this->load->library('email');
					$this->email->set_mailtype("html");
					$this->email->from($this->data["siteinfos"]->email, $this->data['siteinfos']->sname);
					$this->email->to($this->input->post('to'));
					$this->email->subject($this->input->post('subject'));
					$this->email->message($this->input->post('message'));	
					$this->email->attach($path);
						if($this->email->send()) {
							$this->session->set_flashdata('success', $this->lang->line('mail_success'));
						} else {
							$this->session->set_flashdata('error', $this->lang->line('mail_error'));
						}
					}
				} else {
					$this->data["subview"] = "error";
					$this->load->view('_layout_main', $this->data);
				}
			} else {
				$this->data["subview"] = "error";
				$this->load->view('_layout_main', $this->data);
			}
		} else {
			$this->data["subview"] = "error";
			$this->load->view('_layout_main', $this->data);
		}
	}

	public function unique_school() {
		$id = htmlentities($this->db->escape_str($this->uri->segment(3)));
		if((int)$id) {
			$school = $this->school_m->get_order_by_school(array("school" => $this->input->post("school"), "schoolID !=" => $id));
			if(count($school)) {
				$this->form_validation->set_message("unique_school", "%s already exists");
				return FALSE;
			}
			return TRUE;
		} else {
			$school = $this->school_m->get_order_by_school(array("school" => $this->input->post("school")));

			if(count($school)) {
				$this->form_validation->set_message("unique_school", "%s already exists");
				return FALSE;
			}
			return TRUE;
		}	
	}

	public function unique_day() {
		$day = $this->input->post('automation');
		if((int)$day) {
			if($day < 0 || $day > 30) {
				$this->form_validation->set_message("unique_day", "%s already exists");
				return FALSE;
			}
			return TRUE;
		} else {
			$this->form_validation->set_message("unique_day", "%s already exists");
			return FALSE;
		}	
	}

	function active() {
		$usertype = $this->session->userdata("usertype");
		if($usertype == "Super Admin") {
			$id = $this->input->post('id');
			$status = $this->input->post('status');
			if($id != '' && $status != '') {
				if((int)$id) {
					if($status == 'chacked') {
						$this->school_m->update_school(array('schoolactive' => 1), $id);
						echo 'Success';
					} elseif($status == 'unchacked') {
						$this->school_m->update_school(array('schoolactive' => 0), $id);
						echo 'Success';
					} else {
						echo "Error";
					}
				} else {
					echo "Error";
				}
			} else {
				echo "Error";
			}
		} else {
			echo "Error";
		}
	}

	function activeschoolnow($schoolID, $schoollogo) {
		$usertype = $this->session->userdata("usertype");
		if($usertype == "Super Admin") {
			$school = $this->school_m->get_school($schoolID);
			$data = array('schoolID' => $schoolID, 'school' => $school->school, 'schoollogo' => $schoollogo);
			$this->session->set_userdata($data);
			redirect($_SERVER['HTTP_REFERER']);
			redirect('dashboard/index');
		} else {
			redirect($_SERVER['HTTP_REFERER']);
		}
	}

	function allschoolnow() {
		$usertype = $this->session->userdata("usertype");
		if($usertype == "Super Admin") {
			$this->data["subview"] = "school/allschoolnow";
			$this->load->view('_layout_main', $this->data);		
		} else {
			$this->data["subview"] = "error";
			$this->load->view('_layout_main', $this->data);
		}
	}

}

/* End of file exam.php */
/* Location: .//D/xampp/htdocs/school/mvc/controllers/exam.php */