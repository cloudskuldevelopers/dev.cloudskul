<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Applied_school extends MY_Controller {
/*
| -----------------------------------------------------
| PRODUCT NAME: 	CloudSkul
| -----------------------------------------------------
| AUTHOR:			Cloudskul TEAM
| -----------------------------------------------------
| EMAIL:			
| -----------------------------------------------------
| COPYRIGHT:		RESERVED BY CLOUDSKUL
| -----------------------------------------------------
| WEBSITE:			http://cloudskul.com
| -----------------------------------------------------
*/
	function __construct() {
		parent::__construct();
		$this->load->model("applied_school_m");
		$this->load->model("bankaccount_m");
		$this->load->model("school_m");
		$this->load->model("user_m");
		$this->load->library("ozioma");
		$language = $this->session->userdata('lang');
		$this->lang->load('applied_school', $language);	
	}

	public function index() {
		$usertype = $this->session->userdata("usertype");
		$schoolID = $this->session->userdata('schoolID');
		if($usertype== "Super Admin") {	
			$this->data['applied_school'] = $this->applied_school_m->get_tmp_school();
			$this->data["subview"] = "applied_school/index";
			$this->load->view('_layout_main', $this->data);
		} else {
			$this->data["subview"] = "error";
			$this->load->view('_layout_main', $this->data);
		}
	}
	public function view() {
		$usertype = $this->session->userdata("usertype");
		if($usertype== "Super Admin") {	
			$id = $this->uri->segment(3);
			if ((int)$id) {
				$this->data['reg_school'] = $this->applied_school_m->get_tmp_school($id);
				$this->data["subview"] = "applied_school/view";
				$this->load->view('_layout_main', $this->data);				
			} else {
				$this->data["subview"] = "error";
				$this->load->view('_layout_main', $this->data);
			}			
		} else {
			$this->data["subview"] = "error";
			$this->load->view('_layout_main', $this->data);
		}
	}

	public function delete() {
		$usertype = $this->session->userdata("usertype");
		if($usertype == "Super Admin") {
			$id = htmlentities($this->db->escape_str($this->uri->segment(3)));
			if((int)$id) {
				$this->data['school'] = $this->applied_school_m->get_tmp_school($id);
				if($this->data['school']) {
					$this->applied_school_m->delete_tmp_school($id);
					$this->session->set_flashdata('success', $this->lang->line('delete_success'));
					redirect(base_url("applied_school/index"));
				} else {
					$this->session->set_flashdata('error', $this->lang->line('delete_error'));
					redirect(base_url("applied_school/index"));
				}
			} else {
				redirect(base_url("applied_school/index"));
			}	
		} else {
			$this->data["subview"] = "error";
			$this->load->view('_layout_main', $this->data);
		}
	}
	function active() {
		$usertype = $this->session->userdata("usertype");
		if($usertype == "Super Admin") {
			$id = $this->input->post('id');
			if($id != '') {
				if((int)$id) {
					$tmp_school = $this->applied_school_m->get_tmp_school($id);
					$array = array();
					$user = array();
					$array["school"] = $tmp_school->school;
					$array["email"] = $tmp_school->email;
					$array["phone"] = $tmp_school->phone;
					$array["address"] = $tmp_school->address;
					$array["photo"] = $tmp_school->photo;
					$array["create_date"] = date("Y-m-d h:i:s");
					$array["modify_date"] = date("Y-m-d h:i:s");
					$array["create_userID"] = $this->session->userdata('loginuserID');
					$array["create_username"] = $this->session->userdata('username');
					$array["create_usertype"] = $this->session->userdata('usertype');
					$array["schoolactive"] = 1;
					$returnID = $this->school_m->insert_school($array);

					//start insert school id into accounts table
					$arr['schoolID'] = $returnID;
					$arr["create_date"] = date("Y-m-d h:i:s");
					$arr["modify_date"] = date("Y-m-d h:i:s");
					$arr["create_userID"] = $this->session->userdata('loginuserID');
					$arr["create_username"] = $this->session->userdata('username');
					$arr["create_usertype"] = $this->session->userdata('usertype');
					$this->bankaccount_m->insert_school($arr);
					//end insert of school id into accounts table

					$user['schoolID'] = $returnID;
					$user['name'] = $tmp_school->name;
					$user['dob'] = $tmp_school->dob;
					$user['sex'] = $tmp_school->sex;
					$user['jod'] = date("Y-m-d h:i:s");
					$user['username'] = $tmp_school->username;
					$user['password'] = $tmp_school->password;
					$user['photo'] = "defualt.png";
					$user['usertype'] = "Admin";
					$user["create_date"] = date("Y-m-d h:i:s");
					$user["modify_date"] = date("Y-m-d h:i:s");
					$user["create_userID"] = $this->session->userdata('loginuserID');
					$user["create_username"] = $this->session->userdata('username');
					$user["create_usertype"] = $this->session->userdata('usertype');
					$user["useractive"] = 1;
					$a = '';
					//die;
					if($this->user_m->insert_user($user)){
					//if($a == ''){

						/*$this->load->library('email');
						$this->email->set_mailtype("html");
						$this->email->from($this->data["siteinfos"]->email, $this->data['siteinfos']->sname);
						//$this->email->to($tmp_school->email);
						$this->email->to('etti.emman@gmail.com');
						$this->email->subject('Welcome');
						$this->email->message($this->email_template());	
						//$this->email->attach($path);
						$this->email->send();
						if($this->email->send()) {
							$this->session->set_flashdata('success', $this->lang->line('mail_success'));
						} else {
							$this->session->set_flashdata('error', $this->lang->line('mail_error'));
						}*/
						//start ozioma
						$message = "Hello ".$tmp_school->school.", we are glad to inform you that your application to join CloudSkul has been approved.";
						$message .="You can now login with your username and password."; 
						$message .="Welcome to CloudSkul";
						$this->ozioma->set_message($message);//message from textfield
			            $this->ozioma->set_recipient($tmp_school->phone);//separate numbers with commas and include zip code in every number
			            $this->ozioma->set_sender("CloudSkul");//sender from database
			            $this->ozioma->send();
			            //end ozioma
						$this->applied_school_m->delete_tmp_school($id);
						$this->session->set_flashdata('success', $this->lang->line('add_success'));
						echo base_url("applied_school/index");
					} else {
						$this->session->set_flashdata('error', $this->lang->line('add_error'));
						echo base_url("applied_school/index");
					}

					
				} else {
					echo "Error";
				}
			} else {
				echo "Error";
			}
		} else {
			echo "Error";
		}
	}

	public function email_template2(){
		$message = "<html>";
        $message .="<head>";
        $message .="    <title>Packet</title>";
        $message .="</head>";
        $message .="<body>";
        $message .="<p>you have book this</p>";
        $message .="<table>";
        $message .="    <tr>";
        $message .="        <td>ID Booking</td>";
        $message .="        <td></td>";
        $message .="    </tr>";
        $message .="    <tr>";
        $message .="        <td>Price</td>";
        $message .="        <td></td>";
        $message .="    </tr>";
        $message .="    <tr>";
        $message .="        <td>Name</td>";
        $message .="        <td></td>";
        $message .="    </tr>";
		$message .="";
        $message .="</table>";
        $message .="</body>";
    	$message .="</html>";


    	$response = '
    	<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"> 
		<html xmlns="http://www.w3.org/1999/xhtml">
		<head>
			<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
			<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
			<title>Welcome:: Cloudskul</title>
			<style type="text/css">
				body{width:100% !important; -webkit-text-size-adjust:100%; -ms-text-size-adjust:100%; margin:0; padding:0;} 
				.ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div {line-height: 100%;}
				img {outline:none; text-decoration:none; -ms-interpolation-mode: bicubic;} 
				a img {border:none;} 
				.image_fix {display:block;}
				p {margin: 1em 0;}
				h1, h2, h3, h4, h5, h6 {color: black !important;}
				h1 a, h2 a, h3 a, h4 a, h5 a, h6 a {color: blue !important;}
				h1 a:active, h2 a:active,  h3 a:active, h4 a:active, h5 a:active, h6 a:active {
				color: red !important; 
				}
				h1 a:visited, h2 a:visited,  h3 a:visited, h4 a:visited, h5 a:visited, h6 a:visited {
				color: purple !important; 
				}
				
				table td {border-collapse: collapse;}
				
				table { border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt; }
				
				a {color: orange;}
				
				@media only screen and (max-device-width: 480px) {
					
					a[href^="tel"], a[href^="sms"] {
								text-decoration: none;
								color: blue; 
								pointer-events: none;
								cursor: default;
							}
					.mobile_link a[href^="tel"], .mobile_link a[href^="sms"] {
								text-decoration: default;
								color: orange !important;
								pointer-events: auto;
								cursor: default;
							}
				}
				
				@media only screen and (min-device-width: 768px) and (max-device-width: 1024px) {
				
					a[href^="tel"], a[href^="sms"] {
								text-decoration: none;
								color: blue; 
								pointer-events: none;
								cursor: default;
							}
					.mobile_link a[href^="tel"], .mobile_link a[href^="sms"] {
								text-decoration: default;
								color: orange !important;
								pointer-events: auto;
								cursor: default;
							}
				}
				@media only screen and (-webkit-min-device-pixel-ratio: 2) {
				/* Put your iPhone 4g styles in here */ 
				}
				/* Android targeting */
				@media only screen and (-webkit-device-pixel-ratio:.75){
				/* Put CSS for low density (ldpi) Android layouts in here */
				}
				@media only screen and (-webkit-device-pixel-ratio:1){
				/* Put CSS for medium density (mdpi) Android layouts in here */
				}
				@media only screen and (-webkit-device-pixel-ratio:1.5){
				/* Put CSS for high density (hdpi) Android layouts in here */
				}
				/* end Android targeting */
			</style>

			<!-- Targeting Windows Mobile -->
			<!--[if IEMobile 7]>
			<style type="text/css">
			
			</style>
			<![endif]-->   


			<!--[if gte mso 9]>
				<style>
				
				</style>
			<![endif]-->
		</head>
		<body>
    	<div marginwidth="0" marginheight="0" style="width:100%!important;min-width:100%;color:#222222;font-family:"Helvetica","Arial",sans-serif;font-weight:normal;text-align:left;line-height:19px;font-size:14px;margin:0;padding:0;background:#ffffff">
         <table cellpadding="0" cellspacing="0" style="border-spacing:0;border-collapse:collapse;vertical-align:top;text-align:left;height:100%;width:100%;color:#222222;font-family:"Helvetica","Arial",sans-serif;font-weight:normal;line-height:19px;font-size:14px;margin:0;padding:0">
            <tbody>
               <tr style="vertical-align:top;text-align:left;padding:0" align="left">
                  <td align="center" valign="top" style="word-break:break-word;border-collapse:collapse!important;vertical-align:top;text-align:center;color:#222222;font-family:"Helvetica","Arial",sans-serif;font-weight:normal;line-height:19px;font-size:14px;margin:0;padding:0">
                     <center style="width:100%;min-width:580px">
                        <table style="border-spacing:0;border-collapse:collapse;vertical-align:top;text-align:inherit;width:580px;margin:0 auto;padding:0;height:8px;background:rgb(29, 175, 237)">
                           <tbody>
                              <tr style="vertical-align:top;text-align:left;padding:0;height:0px" align="left">
                                 <td style="height:8px;word-break:break-word;border-collapse:collapse!important;vertical-align:top;text-align:left;color:#222222;font-family:"Helvetica","Arial",sans-serif;font-weight:normal;line-height:19px;font-size:14px;margin:0;padding:0" align="left" valign="top"></td>
                              </tr>
                           </tbody>
                        </table>
                        <table style="border-spacing:0;border-collapse:collapse;vertical-align:top;text-align:inherit;width:580px;margin:0 auto;padding:0">
                           <tbody>
                              <tr style="vertical-align:top;text-align:left;padding:0" align="left">
                                 <td style="word-break:break-word;border-collapse:collapse!important;vertical-align:top;text-align:left;color:#222222;font-family:"Helvetica","Arial",sans-serif;font-weight:normal;line-height:19px;font-size:14px;margin:0;padding:0" align="left" valign="top">
                                    <table style="border-spacing:0;border-collapse:collapse;vertical-align:top;text-align:left;width:580px;padding:0">
                                       <tbody>
                                          <tr>
                                             <td style="width:205px">
                                                <table style="border-spacing:0;border-collapse:collapse;vertical-align:top;text-align:left;width:205px;padding:0">
                                                   <tbody>
                                                      <tr style="vertical-align:top;text-align:left;padding:0" align="left">
                                                         <td style="word-break:break-word;border-collapse:collapse!important;vertical-align:top;text-align:left;color:#222222;font-family:"Helvetica","Arial",sans-serif;font-weight:normal;line-height:19px;font-size:14px;margin:0" align="left" valign="top">
                                                            <div><a href="#151a4e939f5fb7a2_" style="text-decoration:none"><img src="https://dev.cloudskul.com/uploads/images/logo.png" alt="logo" width="205px" height="72px" border="0" style="display:block" class="CToWUd"></a></div>
                                                         </td>
                                                      </tr>
                                                   </tbody>
                                                </table>
                                             </td>
                                             <td style="width:375px">
                                                <table style="border-spacing:0;border-collapse:collapse;vertical-align:top;text-align:left;width:205px;padding:0;margin-left:140px">
                                                   <tbody>
                                                      <tr style="vertical-align:top;text-align:left;padding:0" align="left">
                                                         <td style="word-break:break-word;border-collapse:collapse!important;vertical-align:top;text-align:left;color:#222222;font-family:"Helvetica","Arial",sans-serif;font-weight:normal;line-height:19px;font-size:14px;margin:0;width:55px;height:72px" align="left" valign="top">
                                                            <div><a href="#" style="text-decoration:none" target="_top"><img src="https://dev.cloudskul.com/uploads/images/twitter.jpg" alt="picto-twitter" border="0" style="display:block" class="CToWUd"></a></div>
                                                         </td>
                                                         <td style="word-break:break-word;border-collapse:collapse!important;vertical-align:top;text-align:left;color:#222222;font-family:"Helvetica","Arial",sans-serif;font-weight:normal;line-height:19px;font-size:14px;margin:0;width:55px;height:72px;border:0" align="left" valign="top">
                                                            <div><a href="#" style="text-decoration:none" target="_top"><img src="https://dev.cloudskul.com/uploads/images/googleplus.jpg" alt="picto-google" border="0" style="display:block" class="CToWUd"></a></div>
                                                         </td>
                                                         <td style="word-break:break-word;border-collapse:collapse!important;vertical-align:top;text-align:left;color:#222222;font-family:"Helvetica","Arial",sans-serif;font-weight:normal;line-height:19px;font-size:14px;margin:0;width:55px;height:72px;border:0" align="left" valign="top">
                                                            <div><a href="https://www.facebook.com/cloudskul/" style="text-decoration:none" target="_blank"><img src="https://dev.cloudskul.com/uploads/images/facebook.jpg" alt="picto-facebook" border="0" style="display:block" class="CToWUd"></a></div>
                                                         </td>
                                                         <td style="word-break:break-word;border-collapse:collapse!important;vertical-align:top;text-align:left;color:#222222;font-family:"Helvetica","Arial",sans-serif;font-weight:normal;line-height:19px;font-size:14px;margin:0;width:55px;height:72px;border:0" align="left" valign="top">
                                                            <div><a href="#" style="text-decoration:none" target="_top"><img src="https://dev.cloudskul.com/uploads/images/linkedin.jpg" alt="picto-linkedin" border="0" style="display:block" class="CToWUd"></a></div>
                                                         </td>
                                                      </tr>
                                                   </tbody>
                                                </table>
                                             </td>
                                          </tr>
                                       </tbody>
                                    </table>
                                    <table cellpadding="0" cellspacing="0" style="border-spacing:0;border-collapse:collapse;width:580px;height:140px;padding:0;border:0;vertical-align:top">
                                       <tbody>
                                          <tr style="margin:0;padding:0;border:0;vertical-align:top">
                                             <td style="width:580px;height:149px;padding:0;border:0;margin:0;border-spacing:0">
                                                <div style="padding:0;margin:0;color:#fff">
                                                   <div><img width="581" height="328" src="https://dev.cloudskul.com/uploads/images/cloud2.jpg" class="CToWUd"></div>
                                                </div>
                                             </td>
                                          </tr>
                                       </tbody>
                                    </table>
                                    <table cellpadding="0" cellspacing="0" style="border-spacing:0;border-collapse:collapse;vertical-align:top;text-align:left;width:580px;padding:0">
                                       <tbody>
                                          <tr>
                                             <td style="height:1px;width:580px;background:rgb(29, 175, 237);padding:0;border:0;margin:0"></td>
                                          </tr>
                                          <tr>
                                             <td>
                                                <table cellpadding="0" cellspacing="0" style="border-spacing:0;border-collapse:collapse;vertical-align:top;text-align:left;width:580px;padding:0">
                                                   <tbody>
                                                      <tr>
                                                         <td style="padding-right:48px;padding-top:35px;font-size:22px;color:rgb(29, 175, 237);align:left">
                                                            <div>
                                                               <div>Welcome to Cloudskul</div>
                                                            </div>
                                                         </td>
                                                      </tr>
                                                      <tr>
                                                         <td style="padding-left:48px;padding-top:15px;font-size:14px;color:#606062;font-weight:300;padding-bottom:20px">
                                                            <div>
                                                               <div><br>Thank you for signing up on <em>Cloudskul </em>. <em>Cloudskul</em> is happy to receive and approve your application to use cloudskul.com as a platform for managing the academic constituents of your institution. With your help, <em>cloudskul</em> is going to make life easy for countless number of schools and parents in the world. Cloudskul represents the future of education in a digital world.</div>
                                                               <!--ul>
                                                                  <li><strong>New WebPOS operations</strong> such as quotation rejection and cash up of several terminals&nbsp;that share cash drawer or credit card payment device as well as improved searching capabilities of products and customers.</li>
                                                                  <li>Several <strong>pricing and discounts improvements</strong> such as&nbsp;discount categories, multi price lists support and bill of materials taxes configuration.</li>
                                                                  <li><strong>Security enhancements</strong> with a new approval process when deleting ticket lines and additional validation when processing blind returns.</li>
                                                               </ul-->
                                                               <p>With these in mind, our overall objective is to connect fast growing schools, improve educational services and engage parents in the education of their children.</p> 
                                                               <p>As our gift to you, please enjoy complimentary internet and sms services for your school.</p>
                                                               <p>Thank you for your interest in Cloudskul.</p>
                                                               <p>Best regards,</p>
                                                               <p><em>Cloudskul Team</em></p>
                                                            </div>
                                                         </td>
                                                      </tr>
                                                      <!--tr>
                                                         <td style="height:36px;font-size:16px;color:#fff;font-weight:300;border:0;padding:0">
                                                            <table cellpadding="0" cellspacing="0" style="height:33px;padding:0;width:580px">
                                                               <tbody>
                                                                  <tr>
                                                                     <td style="height:0px;width:178px;font-size:16px;color:#fff;font-weight:300;padding:0;text-align:center"></td>
                                                                     <!--td style="height:33px;width:224px;font-size:16px;color:#fff;font-weight:300;padding:0;background:#eebb2c;text-align:center;vertical-align:middle">
                                                                        <div style="padding:0;margin:0;color:#fff">
                                                                           <div><a style="color:rgb(29, 175, 237);padding:0" href="http://go.openbravo.com/lQN5A000C3DE0A3jj010L0V" target="_blank">Request a FREE demo!</a></div>
                                                                        </div>
                                                                     </td-->
                                                                     <td style="height:0px;width:178px;font-size:16px;color:#fff;font-weight:300;padding:0;text-align:center"></td>
                                                                  </tr>
                                                               </tbody>
                                                            </table>
                                                         </td>
                                                      </tr-->
                                                   </tbody>
                                                </table>
                                             </td>
                                          </tr>
                                       </tbody>
                                    </table>
                                    <table cellspacing="0" cellpadding="0" style="border-spacing:0;border-collapse:collapse;vertical-align:top;text-align:left;width:580px;padding:0;margin-top:40px">
                                       <tbody>
                                          <tr>
                                             <td></td>
                                          </tr>
                                       </tbody>
                                    </table>
                                    <table cellspacing="0" cellpadding="0" style="height:44px;border-spacing:0;border-collapse:collapse;width:580px;padding:0">
                                       <tbody>
                                          <tr style="width:100%;height:10px;padding:0">
                                             <td style="font-size:0px;font-weight:300;width:100%;text-align:center;padding:0;border:0;margin:0"></td>
                                          </tr>
                                          <tr style="width:100%;height:34px;padding:0;border:0;margin:0">
                                             <td style="font-size:11px;font-weight:300;color:#fff;width:100%;text-align:center;background:rgb(29, 175, 237);padding:0;border:0;margin:0">
                                                <div style="padding:0;border:0;margin:0">© 2015 Cloudskul, All rights reserved</div>
                                             </td>
                                          </tr>
                                       </tbody>
                                    </table>
                                 </td>
                              </tr>
                           </tbody>
                        </table>
                     </center>
                  </td>
               </tr>
            </tbody>
         </table>
         <center>
            <!--p><font face="Verdana" size="1">If you no longer wish to receive these emails, click on the following link: <a href="http://go.openbravo.com/u/AQ0B0jVN301j3E500C0M0DA" style="color:blue" target="_blank">Unsubscribe</a><br> </font> </p-->
         </center>
         <div class="yj6qo"></div>
         <div class="adL"></div>
      </div>
      </body>
	  </html>';

    	echo $response;
	}

	public function emailer(){
		$msg = $this->email_template();

		$this->load->library('email');
		$this->email->set_mailtype("html");
		$this->email->from($this->data["siteinfos"]->welcome_email, $this->data['siteinfos']->sname);
		//$this->email->to($tmp_school->email);
		$this->email->to('docaustyn@artty.net');
		//$this->email->to('etti.emman@gmail.com');
		$this->email->subject('Welcome');
		$this->email->message($msg);	
		//$this->email->attach($path);
		$this->email->send();
		return $msg;
	}

	public function email_template(){

		$response = '
    	<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"> 
		<html xmlns="http://www.w3.org/1999/xhtml">
		<head>
			<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
			<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
			<title>Welcome:: Cloudskul</title>
			<style type="text/css">
				body{width:100% !important; -webkit-text-size-adjust:100%; -ms-text-size-adjust:100%; margin:0; padding:0;} 
				.ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div {line-height: 100%;}
				img {outline:none; text-decoration:none; -ms-interpolation-mode: bicubic;} 
				a img {border:none;} 
				.image_fix {display:block;}
				p {margin: 1em 0;}
				h1, h2, h3, h4, h5, h6 {color: black !important;}
				h1 a, h2 a, h3 a, h4 a, h5 a, h6 a {color: blue !important;}
				h1 a:active, h2 a:active,  h3 a:active, h4 a:active, h5 a:active, h6 a:active {
				color: red !important; 
				}
				h1 a:visited, h2 a:visited,  h3 a:visited, h4 a:visited, h5 a:visited, h6 a:visited {
				color: purple !important; 
				}
				
				table td {border-collapse: collapse;}
				
				table { border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt; }
				
				a {color: orange;}
				
				@media only screen and (max-device-width: 480px) {
					
					a[href^="tel"], a[href^="sms"] {
								text-decoration: none;
								color: blue; 
								pointer-events: none;
								cursor: default;
							}
					.mobile_link a[href^="tel"], .mobile_link a[href^="sms"] {
								text-decoration: default;
								color: orange !important;
								pointer-events: auto;
								cursor: default;
							}
				}
				
				@media only screen and (min-device-width: 768px) and (max-device-width: 1024px) {
				
					a[href^="tel"], a[href^="sms"] {
								text-decoration: none;
								color: blue; 
								pointer-events: none;
								cursor: default;
							}
					.mobile_link a[href^="tel"], .mobile_link a[href^="sms"] {
								text-decoration: default;
								color: orange !important;
								pointer-events: auto;
								cursor: default;
							}
				}
				@media only screen and (-webkit-min-device-pixel-ratio: 2) {
				/* Put your iPhone 4g styles in here */ 
				}
				/* Android targeting */
				@media only screen and (-webkit-device-pixel-ratio:.75){
				/* Put CSS for low density (ldpi) Android layouts in here */
				}
				@media only screen and (-webkit-device-pixel-ratio:1){
				/* Put CSS for medium density (mdpi) Android layouts in here */
				}
				@media only screen and (-webkit-device-pixel-ratio:1.5){
				/* Put CSS for high density (hdpi) Android layouts in here */
				}
				/* end Android targeting */
			</style>

			<!-- Targeting Windows Mobile -->
			<!--[if IEMobile 7]>
			<style type="text/css">
			
			</style>
			<![endif]-->   


			<!--[if gte mso 9]>
				<style>
				
				</style>
			<![endif]-->
		</head>
		<body>
    	<div marginwidth="0" marginheight="0" style="width:100%!important;min-width:100%;color:#222222;font-family:"Helvetica","Arial",sans-serif;font-weight:normal;text-align:left;line-height:19px;font-size:14px;margin:0;padding:0;background:#ffffff">
         <table cellpadding="0" cellspacing="0" style="border-spacing:0;border-collapse:collapse;vertical-align:top;text-align:left;height:100%;width:100%;color:#222222;font-family:"Helvetica","Arial",sans-serif;font-weight:normal;line-height:19px;font-size:14px;margin:0;padding:0">
            <tbody>
               <tr style="vertical-align:top;text-align:left;padding:0" align="left">
                  <td align="center" valign="top" style="word-break:break-word;border-collapse:collapse!important;vertical-align:top;text-align:center;color:#222222;font-family:"Helvetica","Arial",sans-serif;font-weight:normal;line-height:19px;font-size:14px;margin:0;padding:0">
                     <center style="width:100%;min-width:580px">
                        <table style="border-spacing:0;border-collapse:collapse;vertical-align:top;text-align:inherit;width:580px;margin:0 auto;padding:0;height:8px;background:rgb(29, 175, 237)">
                           <tbody>
                              <tr style="vertical-align:top;text-align:left;padding:0;height:0px" align="left">
                                 <td style="height:8px;word-break:break-word;border-collapse:collapse!important;vertical-align:top;text-align:left;color:#222222;font-family:"Helvetica","Arial",sans-serif;font-weight:normal;line-height:19px;font-size:14px;margin:0;padding:0" align="left" valign="top"></td>
                              </tr>
                           </tbody>
                        </table>
                        <table style="border-spacing:0;border-collapse:collapse;vertical-align:top;text-align:inherit;width:580px;margin:0 auto;padding:0">
                           <tbody>
                              <tr style="vertical-align:top;text-align:left;padding:0" align="left">
                                 <td style="word-break:break-word;border-collapse:collapse!important;vertical-align:top;text-align:left;color:#222222;font-family:"Helvetica","Arial",sans-serif;font-weight:normal;line-height:19px;font-size:14px;margin:0;padding:0" align="left" valign="top">
                                    <table style="border-spacing:0;border-collapse:collapse;vertical-align:top;text-align:left;width:580px;padding:0">
                                       <tbody>
                                          <tr>
                                             <td style="width:205px">
                                                <table style="border-spacing:0;border-collapse:collapse;vertical-align:top;text-align:left;width:205px;padding:0">
                                                   <tbody>
                                                      <tr style="vertical-align:top;text-align:left;padding:0" align="left">
                                                         <td style="word-break:break-word;border-collapse:collapse!important;vertical-align:top;text-align:left;color:#222222;font-family:"Helvetica","Arial",sans-serif;font-weight:normal;line-height:19px;font-size:14px;margin:0" align="left" valign="top">
                                                            <div><a href="#151a4e939f5fb7a2_" style="text-decoration:none"><img src="https://dev.cloudskul.com/uploads/images/logo.png" alt="logo" width="205px" height="72px" border="0" style="display:block" class="CToWUd"></a></div>
                                                         </td>
                                                      </tr>
                                                   </tbody>
                                                </table>
                                             </td>
                                             <td style="width:375px">
                                                <table style="border-spacing:0;border-collapse:collapse;vertical-align:top;text-align:left;width:205px;padding:0;margin-left:140px">
                                                   <tbody>
                                                      <tr style="vertical-align:top;text-align:left;padding:0" align="left">
                                                         <td style="word-break:break-word;border-collapse:collapse!important;vertical-align:top;text-align:left;color:#222222;font-family:"Helvetica","Arial",sans-serif;font-weight:normal;line-height:19px;font-size:14px;margin:0;width:55px;height:72px" align="left" valign="top">
                                                            <div><a href="#" style="text-decoration:none" target="_top"><img src="https://dev.cloudskul.com/uploads/images/twitter.jpg" alt="picto-twitter" border="0" style="display:block" class="CToWUd"></a></div>
                                                         </td>
                                                         <td style="word-break:break-word;border-collapse:collapse!important;vertical-align:top;text-align:left;color:#222222;font-family:"Helvetica","Arial",sans-serif;font-weight:normal;line-height:19px;font-size:14px;margin:0;width:55px;height:72px;border:0" align="left" valign="top">
                                                            <div><a href="#" style="text-decoration:none" target="_top"><img src="https://dev.cloudskul.com/uploads/images/googleplus.jpg" alt="picto-google" border="0" style="display:block" class="CToWUd"></a></div>
                                                         </td>
                                                         <td style="word-break:break-word;border-collapse:collapse!important;vertical-align:top;text-align:left;color:#222222;font-family:"Helvetica","Arial",sans-serif;font-weight:normal;line-height:19px;font-size:14px;margin:0;width:55px;height:72px;border:0" align="left" valign="top">
                                                            <div><a href="https://www.facebook.com/cloudskul/" style="text-decoration:none" target="_blank"><img src="https://dev.cloudskul.com/uploads/images/facebook.jpg" alt="picto-facebook" border="0" style="display:block" class="CToWUd"></a></div>
                                                         </td>
                                                         <td style="word-break:break-word;border-collapse:collapse!important;vertical-align:top;text-align:left;color:#222222;font-family:"Helvetica","Arial",sans-serif;font-weight:normal;line-height:19px;font-size:14px;margin:0;width:55px;height:72px;border:0" align="left" valign="top">
                                                            <div><a href="#" style="text-decoration:none" target="_top"><img src="https://dev.cloudskul.com/uploads/images/linkedin.jpg" alt="picto-linkedin" border="0" style="display:block" class="CToWUd"></a></div>
                                                         </td>
                                                      </tr>
                                                   </tbody>
                                                </table>
                                             </td>
                                          </tr>
                                       </tbody>
                                    </table>
                                    <table cellpadding="0" cellspacing="0" style="border-spacing:0;border-collapse:collapse;width:580px;height:140px;padding:0;border:0;vertical-align:top">
                                       <tbody>
                                          <tr style="margin:0;padding:0;border:0;vertical-align:top">
                                             <td style="width:580px;height:149px;padding:0;border:0;margin:0;border-spacing:0">
                                                <div style="padding:0;margin:0;color:#fff">
                                                   <div><img width="581" height="328" src="https://dev.cloudskul.com/uploads/images/cloud2.jpg" class="CToWUd"></div>
                                                </div>
                                             </td>
                                          </tr>
                                       </tbody>
                                    </table>
                                    <table cellpadding="0" cellspacing="0" style="border-spacing:0;border-collapse:collapse;vertical-align:top;text-align:left;width:580px;padding:0">
                                       <tbody>
                                          <tr>
                                             <td style="height:1px;width:580px;background:rgb(29, 175, 237);padding:0;border:0;margin:0"></td>
                                          </tr>
                                          <tr>
                                             <td>
                                                <table cellpadding="0" cellspacing="0" style="border-spacing:0;border-collapse:collapse;vertical-align:top;text-align:left;width:580px;padding:0">
                                                   <tbody>
                                                      <tr>
                                                         <td style="padding-right:48px;padding-top:35px;font-size:22px;color:rgb(29, 175, 237);align:left">
                                                            <div>
                                                               <div>Welcome to Cloudskul</div>
                                                            </div>
                                                         </td>
                                                      </tr>
                                                      <tr>
                                                         <td style="padding-left:48px;padding-top:15px;font-size:14px;color:#606062;font-weight:300;padding-bottom:20px">
                                                            <div>
                                                               <div><br>Thank you for signing up on <em>Cloudskul </em>. <em>Cloudskul</em> is happy to receive and approve your application to use cloudskul.com as a platform for managing the academic constituents of your institution. With your help, <em>cloudskul</em> is going to make life easy for countless number of schools and parents in the world. Cloudskul represents the future of education in a digital world.</div>
                                                               <!--ul>
                                                                  <li><strong>New WebPOS operations</strong> such as quotation rejection and cash up of several terminals&nbsp;that share cash drawer or credit card payment device as well as improved searching capabilities of products and customers.</li>
                                                                  <li>Several <strong>pricing and discounts improvements</strong> such as&nbsp;discount categories, multi price lists support and bill of materials taxes configuration.</li>
                                                                  <li><strong>Security enhancements</strong> with a new approval process when deleting ticket lines and additional validation when processing blind returns.</li>
                                                               </ul-->
                                                               <p>With these in mind, our overall objective is to connect fast growing schools, improve educational services and engage parents in the education of their children.</p> 
                                                               <p>As our gift to you, please enjoy complimentary internet and sms services for your school.</p>
                                                               <p>Thank you for your interest in Cloudskul.</p>
                                                               <p>Best regards,</p>
                                                               <p><em>Cloudskul Team</em></p>
                                                            </div>
                                                         </td>
                                                      </tr>
                                                      <!--tr>
                                                         <td style="height:36px;font-size:16px;color:#fff;font-weight:300;border:0;padding:0">
                                                            <table cellpadding="0" cellspacing="0" style="height:33px;padding:0;width:580px">
                                                               <tbody>
                                                                  <tr>
                                                                     <td style="height:0px;width:178px;font-size:16px;color:#fff;font-weight:300;padding:0;text-align:center"></td>
                                                                     <!--td style="height:33px;width:224px;font-size:16px;color:#fff;font-weight:300;padding:0;background:#eebb2c;text-align:center;vertical-align:middle">
                                                                        <div style="padding:0;margin:0;color:#fff">
                                                                           <div><a style="color:rgb(29, 175, 237);padding:0" href="http://go.openbravo.com/lQN5A000C3DE0A3jj010L0V" target="_blank">Request a FREE demo!</a></div>
                                                                        </div>
                                                                     </td-->
                                                                     <td style="height:0px;width:178px;font-size:16px;color:#fff;font-weight:300;padding:0;text-align:center"></td>
                                                                  </tr>
                                                               </tbody>
                                                            </table>
                                                         </td>
                                                      </tr-->
                                                   </tbody>
                                                </table>
                                             </td>
                                          </tr>
                                       </tbody>
                                    </table>
                                    <table cellspacing="0" cellpadding="0" style="border-spacing:0;border-collapse:collapse;vertical-align:top;text-align:left;width:580px;padding:0;margin-top:40px">
                                       <tbody>
                                          <tr>
                                             <td></td>
                                          </tr>
                                       </tbody>
                                    </table>
                                    <table cellspacing="0" cellpadding="0" style="height:44px;border-spacing:0;border-collapse:collapse;width:580px;padding:0">
                                       <tbody>
                                          <tr style="width:100%;height:10px;padding:0">
                                             <td style="font-size:0px;font-weight:300;width:100%;text-align:center;padding:0;border:0;margin:0"></td>
                                          </tr>
                                          <tr style="width:100%;height:34px;padding:0;border:0;margin:0">
                                             <td style="font-size:11px;font-weight:300;color:#fff;width:100%;text-align:center;background:rgb(29, 175, 237);padding:0;border:0;margin:0">
                                                <div style="padding:0;border:0;margin:0">© 2015 Cloudskul, All rights reserved</div>
                                             </td>
                                          </tr>
                                       </tbody>
                                    </table>
                                 </td>
                              </tr>
                           </tbody>
                        </table>
                     </center>
                  </td>
               </tr>
            </tbody>
         </table>
         <center>
            <!--p><font face="Verdana" size="1">If you no longer wish to receive these emails, click on the following link: <a href="http://go.openbravo.com/u/AQ0B0jVN301j3E500C0M0DA" style="color:blue" target="_blank">Unsubscribe</a><br> </font> </p-->
         </center>
         <div class="yj6qo"></div>
         <div class="adL"></div>
      </div>
      </body>
	  </html>';

    	return $response;
	}

}

/* End of file notice.php */
/* Location: .//D/xampp/htdocs/school/mvc/controllers/notice.php */