<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Feetype extends MY_Controller {
/*
| -----------------------------------------------------
| PRODUCT NAME: 	CloudSkul
| -----------------------------------------------------
| AUTHOR:			CLOUDSKUL TEAM
| -----------------------------------------------------
| EMAIL:			info@cloudskul.com
| -----------------------------------------------------
| COPYRIGHT:		RESERVED BY Cloudskul
| -----------------------------------------------------
| WEBSITE:			http://cloudskul.com
| -----------------------------------------------------
*/
	function __construct() {
		parent::__construct();
		$this->load->model("feetype_m");
		$language = $this->session->userdata('lang');
		$this->lang->load('feetype', $language);	
	}

	public function index() {
		$usertype = $this->session->userdata("usertype");
		$schoolID = $this->session->userdata('schoolID');
 		if($usertype == "Admin" || $usertype == "Super Admin" || $usertype == "Accountant") {
			$this->data['feetypes'] = $this->feetype_m->get_order_by_feetype(array('schoolID' => $schoolID));
			$this->data["subview"] = "feetype/index";
			$this->load->view('_layout_main', $this->data);
		} else {
			$this->data["subview"] = "error";
			$this->load->view('_layout_main', $this->data);
		}
	}

	protected function rules() {
		$rules = array(
				array(
					'field' => 'feetype', 
					'label' => $this->lang->line("feetype_name"), 
					'rules' => 'trim|required|xss_clean|max_length[60]|callback_unique_feetype'
				),
				array(
					'field' => 'note', 
					'label' => $this->lang->line("feetype_note"), 
					'rules' => 'trim|xss_clean|max_length[200]'
				),
			);
		return $rules;
	}

	public function add() {
		$usertype = $this->session->userdata("usertype");
		if($usertype == "Admin" || $usertype == "Super Admin" || $usertype == "Accountant") {
			if($_POST) {
				$rules = $this->rules();
				$this->form_validation->set_rules($rules);
				if ($this->form_validation->run() == FALSE) {
					
					$this->data['feecat'] = $this->feetype_m->feeCategory();
					$this->data["subview"] = "feetype/add";
					$this->load->view('_layout_main', $this->data);	
					
				} else {
					$array = array(
						"schoolID" => $this->session->userdata('schoolID'),
						"feecategory" => $this->feetype_m->getFeeTypeByCode($this->input->post("feecategory")),
						"feetype" => $this->input->post("feetype"),
						"feecategoryID" => $this->input->post("feecategory"),
						"note" => $this->input->post("note"),
						"create_date" => date("Y-m-d h:i:s"),
						"modify_date" => date("Y-m-d h:i:s"),
						"create_userID" => $this->session->userdata('loginuserID'),
						"create_username" => $this->session->userdata('username'),
						"create_usertype" => $this->session->userdata('usertype'),
					);

					$this->feetype_m->insert_feetype($array);
					$this->session->set_flashdata('success', $this->lang->line('menu_success'));
					redirect(base_url("feetype/index"));
				}
			} else {
				$this->data['feecat'] = $this->feetype_m->feeCategory();
				$this->data["subview"] = "feetype/add";
				$this->load->view('_layout_main', $this->data);
			}
		} else {
			$this->data["subview"] = "error";
			$this->load->view('_layout_main', $this->data);
		}
	}

	public function edit() {
		$usertype = $this->session->userdata("usertype");
		$schoolID = $this->session->userdata('schoolID');
		if($usertype == "Admin" || $usertype == "Super Admin" || $usertype == "Accountant") {
			$id = htmlentities($this->db->escape_str($this->uri->segment(3)));
			if((int)$id) {
				$this->data['feetype'] = $this->feetype_m->get_single_feetype(array('schoolID' => $schoolID, 'feetypeID' => $id));
				if($this->data['feetype']) {
					if($_POST) {
						$rules = $this->rules();
						$this->form_validation->set_rules($rules);
						if ($this->form_validation->run() == FALSE) {
							$this->data["subview"] = "feetype/edit";
							$this->load->view('_layout_main', $this->data);			
						} else {
							$array = array(
								"feetype" => $this->input->post("feetype"),
								"note" => $this->input->post("note"),
								"modify_date" => date("Y-m-d h:i:s")
							);

							$this->feetype_m->update_feetype($array, $id);
							$this->session->set_flashdata('success', $this->lang->line('menu_success'));
							redirect(base_url("feetype/index"));
						}
					} else {
						$this->data["subview"] = "feetype/edit";
						$this->load->view('_layout_main', $this->data);
					}
				} else {
					$this->data["subview"] = "error";
					$this->load->view('_layout_main', $this->data);
				}
			} else {
				$this->data["subview"] = "error";
				$this->load->view('_layout_main', $this->data);
			}	
		} else {
			$this->data["subview"] = "error";
			$this->load->view('_layout_main', $this->data);
		}
	}

	public function delete() {
		$usertype = $this->session->userdata("usertype");
		$schoolID = $this->session->userdata('schoolID');
		if($usertype == "Admin" || $usertype == "Super Admin" || $usertype == "Accountant") {
			$id = htmlentities($this->db->escape_str($this->uri->segment(3)));
			if((int)$id) {
				$this->data['feetype'] = $this->feetype_m->get_single_feetype(array('schoolID' => $schoolID, 'feetypeID' => $id));
				if($this->data['feetype']) {
					$this->feetype_m->delete_feetype($id);
					$this->session->set_flashdata('success', $this->lang->line('menu_success'));
					redirect(base_url("feetype/index"));
				} else {
					redirect(base_url("feetype/index"));
				}
			} else {
				redirect(base_url("feetype/index"));
			}
		} else {
			$this->data["subview"] = "error";
			$this->load->view('_layout_main', $this->data);
		}	
	}

	public function unique_feetype() {
		$id = htmlentities($this->db->escape_str($this->uri->segment(3)));
		$schoolID = $this->session->userdata('schoolID');
		if((int)$id) {
			$feetype = $this->feetype_m->get_order_by_feetype(array('schoolID' => $schoolID, "feetype" => $this->input->post("feetype"), "feetypeID !=" => $id));
			if(count($feetype)) {
				$this->form_validation->set_message("unique_feetype", "%s already exists");
				return FALSE;
			}
			return TRUE;
		} else {
			$feetype = $this->feetype_m->get_order_by_feetype(array('schoolID' => $schoolID, "feetype" => $this->input->post("feetype")));
			if(count($feetype)) {
				$this->form_validation->set_message("unique_feetype", "%s already exists");
				return FALSE;
			}
			return TRUE;
		}	
	}
}

/* End of file feetype.php */
/* Location: .//D/xampp/htdocs/school/mvc/controllers/feetype.php */