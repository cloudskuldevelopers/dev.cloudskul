<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Employee extends MY_Controller {
/*
| -----------------------------------------------------
| PRODUCT NAME: 	CloudSkul
| -----------------------------------------------------
| AUTHOR:			CLOUDSKUL TEAM
| -----------------------------------------------------
| EMAIL:			info@cloudskul.com
| -----------------------------------------------------
| COPYRIGHT:		RESERVED BY Cloudskul
| -----------------------------------------------------
| WEBSITE:			http://cloudskul.com
| -----------------------------------------------------
*/
	public function __construct(){
		parent::__construct();
		$this->load->model('employee_m');
		$this->load->model('department_m');
		$this->load->library('upload');
		$language = $this->session->userdata('lang');
		$this->lang->load('employee', $language);
	}
	
	public function index(){
		//get user session
		$usertype = $this->session->userdata("usertype");
		$school_id = $this->session->userdata('schoolID');
		
		if($usertype == "Super Admin" || $usertype == "Admin") {
			//get all employee d
			$this->data['all_employee_info'] = $this->employee_m->all_emplyee_info($school_id);
			//$this->data['all_employee_info'] = $this->db->get('employee')->result();
			
			$this->data['subview'] = 'employee/index';
			$this->load->view('_layout_main', $this->data);
		} else {
			//load error view
			$this->data["subview"] = "error";
			$this->load->view('_layout_main', $this->data);
		}
	}
	
	protected function rules(){
		$rules = array(
			array(
				'field' => 'first_name', 
				'rules' => 'trim|required|xss_clean|max_length[60]'
			), 
			array(
				'field' => 'last_name',
				'rules' => 'trim|required|max_length[60]|xss_clean'
			),
			array(
				'field' => 'father_name',
				'rules' => 'trim|required|max_length[60]|xss_clean'
			),
			array(
				'field' => 'nationality',
				'rules' => 'trim|required|max_length[50]|xss_clean'
			),
			array(
				'field' => 'present_address',
				'rules' => 'trim|required|max_length[300]|xss_clean'
			),
			array(
				'field' => 'city',
				'rules' => 'trim|required|max_length[60]|xss_clean'
			),
			array(
				'field' => 'phone',
				'rules' => 'trim|max_length[25]|min_length[5]|xss_clean|required'
			),
			array(
				'field' => 'email',
				'rules' => 'trim|required|max_length[40]|valid_email|xss_clean'
			),
			array(
				'field' => 'gender',
				'rules' => 'trim|required|max_length[10]|xss_clean|callback_select_gender'
			),
			array(
				'field' => 'marital_status',
				'rules' => 'trim|required|max_length[10]|xss_clean|callback_select_marital_status'
			),
			array(
				'field' => 'designation_id',
				'rules' => 'trim|required|max_length[10]|xss_clean|callback_select_designation'
			),
			array(
				'field' => 'date_of_birth',
				'rules' => 'trim|required|callback_date_of_birth'
			),
			array(
				'field' => 'joining_date',
				'rules' => 'trim|required|callback_joining_date'
			),
			array(
				'field' => 'employeeID',
				'rules' => 'trim|required'
			)
		);
		return $rules;
	}
	
	public function select_gender(){
		$gender = $_POST['gender'];
		if($gender=="none"){
			$this->form_validation->set_message('select_gender', 'Please select your gender.');
			return false;
		}else{
			return true;
		}
	}
	
	function date_of_birth() {
		$date = $_POST['date_of_birth'];
		if (preg_match("/[0-31]{2}\/[0-12]{2}\/[0-9]{4}/", $date)) {
			if(checkdate(substr($date, 3, 2), substr($date, 0, 2), substr($date, 6, 4))){
				return true;
			}else{
				$this->form_validation->set_message('date_of_birth', 'Please select your date of birth.');
				return false;
			}
		}	
	} 
	
	function joining_date() {
		$date = $_POST['joining_date'];
		if (preg_match("/[0-31]{2}\/[0-12]{2}\/[0-9]{4}/", $date)) {
			if(checkdate(substr($date, 3, 2), substr($date, 0, 2), substr($date, 6, 4))){
				return true;
			}else{
				$this->form_validation->set_message('joining_date', 'Please select your joining date.');
				return false;
			}
		}	
	} 
	
	public function select_marital_status(){
		$marital_status = $_POST['marital_status'];
		if($marital_status=="none"){
			$this->form_validation->set_message('select_marital_status', 'Please select your marital status.');
			return false;
		}else{
			return true;
		}
	}
	
	public function select_designation(){
		$designation_id = $_POST['designation_id'];
		if($designation_id=="none"){
			$this->form_validation->set_message('select_designation', 'Please select your designation.');
			return false;
		}else{
			return true;
		}
	}
	
	/*public function upload_image(){
		$image = $_FILES['image'];
		if (isset($image) && !empty($image)){
			return true;
		}else{
			$this->form_validation->set_message('upload_image', 'Photo is required.');
			return false;
		}
	}*/
	
	function insert_with_image($username) {
	    $random = rand(1, 10000000000000000);
	    $makeRandom = hash('sha512', $random. $username . config_item("encryption_key"));
	    return $makeRandom;
	}
	
	public function add(){
		//get user session
		$usertype = $this->session->userdata("usertype");
		$school_id = $this->session->userdata('schoolID');
		
		if($usertype == "Super Admin" || $usertype == "Admin") {
			//get departments and designations
			// get all department info and designation info using for loop to map them
			$this->department_m->_table_name = "departments"; //table name
			$this->department_m->_order_by = "department_id"; //table primary key
			$this->data['all_dept_info'] = $this->department_m->get(); //get all departments from departments table
			foreach ($this->data['all_dept_info'] as $v_dept_info) {
				$this->data['all_department_info'][$v_dept_info->department_name] = $this->department_m->get_add_department_by_id($school_id, $v_dept_info->department_id);
			}
			
			if($_POST) {
				$rules = $this->rules();
				$this->form_validation->set_rules($rules);
				if ($this->form_validation->run() == FALSE) {
					$this->data["subview"] = "employee/add";
					$this->load->view('_layout_main', $this->data);			
				} else {
					//do something
					//echo("validation tests passed successfully");
					//each employer must have a unique id.. so check if employee id exists in database first
					$employee_id = $this->employee_m->get_unique_id($school_id, $this->input->post("employeeID"));
					if($employee_id){
						$this->session->set_flashdata('error', 'employee id already exists, please try again');
						$this->data["subview"] = "employee/add";
						$this->load->view('_layout_main', $this->data);
					}else{
						$new_file = "defualt.png";
						$image = $_POST['image'];
						if (isset($image) && !empty($image)){
							$file_name = $image;
							$file_name_rename = $this->insert_with_image($this->input->post("employeeID"));
							$explode = explode('.', $file_name);
							if(count($explode) >= 2) {
								$new_file = $file_name_rename.'.'.$explode[1];
								$config['upload_path'] = "./uploads/images";
								$config['allowed_types'] = "gif|jpg|png";
								$config['file_name'] = $new_file;
								$config['max_size'] = '1024';
								$config['max_width'] = '3000';
								$config['max_height'] = '3000';
								//$array['photo'] = $new_file;
								$this->load->library('upload', $config);
								
								//do something
								$employee_array = array(
									"schoolID" => $school_id,
									"employment_id" => $this->input->post("employeeID"),
									"first_name" => $this->input->post("first_name"),
									"last_name" => $this->input->post("last_name"),
									"date_of_birth" => $this->input->post("date_of_birth"),
									"gender" => $this->input->post("gender"),
									"marital_status" => $this->input->post("marital_status"),
									"father_name" => $this->input->post("father_name"),
									"nationality" => $this->input->post("nationality"),
									"passport_number" => $this->input->post("passport_no"),
									"present_address" => $this->input->post("present_address"),
									"city" => $this->input->post("city"),
									"mobile" => $this->input->post("phone"),
									"email" => $this->input->post("email"),
									"joining_date" => $this->input->post("joining_date"),
									"status" => 1,
									"designation_id" => $this->input->post("designation_id"),
									"photo_path" => base_url()."uploads/images/".$new_file
								);
								
								$this->employee_m->insert_employee($employee_array);
								
								//each employer must have a unique id.. so check if employee id exists in database first
								$employee_id = $this->employee_m->get_unique_id($school_id, $this->input->post("employeeID"));
								
								$employee_bank_array = array(
									"employee_id" => $employee_id->employee_id,
									"bank_name" => $this->input->post("bank_name"),
									"branch_name" => $this->input->post("branch_name"),
									"account_name" => $this->input->post("account_name"),
									"account_number" => $this->input->post("account_number")
								);
								
								$this->employee_m->insert_employee_bank($employee_bank_array);
								
								$employee_document_array = array();
								if (!empty($_POST['resume'])) {
									$file_name = $_POST['resume'];
									$file_name_rename = $this->insert_with_image($this->input->post("employeeID"));
									$explode = explode('.', $file_name);
									if(count($explode) >= 2) {
										$resume_file = $file_name_rename.'.'.$explode[1];
										$config['upload_path'] = "./uploads/employee_document";
										$config['allowed_types'] = "gif|jpg|png";
										$config['file_name'] = $resume_file;
										$config['max_size'] = '1024';
										$config['max_width'] = '3000';
										$config['max_height'] = '3000';
										$this->load->library('upload', $config);
										$employee_document_array['resume'] =  $resume_file;
									}
								}
								
								$offer_letter_file = "default.pdf";
								if (!empty($_POST['offer_letter'])) {
									$file_name = $_POST['offer_letter'];
									$file_name_rename = $this->insert_with_image($this->input->post("employeeID"));
									$explode = explode('.', $file_name);
									if(count($explode) >= 2) {
										$offer_letter_file = $file_name_rename.'.'.$explode[1];
										$config['upload_path'] = "./uploads/employee_document";
										$config['allowed_types'] = "gif|jpg|png";
										$config['file_name'] = $offer_letter_file;
										$config['max_size'] = '1024';
										$config['max_width'] = '3000';
										$config['max_height'] = '3000';
										$this->load->library('upload', $config);
										$employee_document_array['offer_letter'] = $offer_letter_file;
									}
								}
								
								$joining_letter_file = "default.pdf";
								if (!empty($_POST['joining_letter'])) {
									$file_name = $_POST['joining_letter'];
									$file_name_rename = $this->insert_with_image($this->input->post("employeeID"));
									$explode = explode('.', $file_name);
									if(count($explode) >= 2) {
										$joining_letter_file = $file_name_rename.'.'.$explode[1];
										$config['upload_path'] = "./uploads/employee_document";
										$config['allowed_types'] = "gif|jpg|png";
										$config['file_name'] = $joining_letter_file;
										$config['max_size'] = '1024';
										$config['max_width'] = '3000';
										$config['max_height'] = '3000';
										$this->load->library('upload', $config);
										$employee_document_array['joining_letter'] = $joining_letter_file;
									}
								}
								
								$contract_paper_file = "default.pdf";
								if (!empty($_POST['contract_paper'])) {
									$file_name = $_POST['contract_paper'];
									$file_name_rename = $this->insert_with_image($this->input->post("employeeID"));
									$explode = explode('.', $file_name);
									if(count($explode) >= 2) {
										$contract_paper_file = $file_name_rename.'.'.$explode[1];
										$config['upload_path'] = "./uploads/employee_document";
										$config['allowed_types'] = "gif|jpg|png";
										$config['file_name'] = $contract_paper_file;
										$config['max_size'] = '1024';
										$config['max_width'] = '3000';
										$config['max_height'] = '3000';
										$this->load->library('upload', $config);
										$employee_document_array['contract_paper'] = $contract_paper_file;
									}
								}
								
								$id_proff_file = "default.pdf";
								if (!empty($_POST['id_proff'])) {
									$file_name = $_POST['id_proff'];
									$file_name_rename = $this->insert_with_image($this->input->post("employeeID"));
									$explode = explode('.', $file_name);
									if(count($explode) >= 2) {
										$id_proff_file = $file_name_rename.'.'.$explode[1];
										$config['upload_path'] = "./uploads/employee_document";
										$config['allowed_types'] = "gif|jpg|png";
										$config['file_name'] = $id_proff_file;
										$config['max_size'] = '1024';
										$config['max_width'] = '3000';
										$config['max_height'] = '3000';
										$this->load->library('upload', $config);
										$employee_document_array['id_proff'] = $id_proff_file;
									}
								}
								
								$other_documents_file = "default.pdf";
								if (!empty($_POST['other_documents'])) {
									$file_name = $_POST['other_documents'];
									$file_name_rename = $this->insert_with_image($this->input->post("employeeID"));
									$explode = explode('.', $file_name);
									if(count($explode) >= 2) {
										$other_documents_file = $file_name_rename.'.'.$explode[1];
										$config['upload_path'] = "./uploads/employee_document";
										$config['allowed_types'] = "gif|jpg|png";
										$config['file_name'] = $other_documents_file;
										$config['max_size'] = '1024';
										$config['max_width'] = '3000';
										$config['max_height'] = '3000';
										$this->load->library('upload', $config);
										$employee_document_array['other_document'] = $other_documents_file;
									}
								}
								
								$employee_document_array = array(
									"employee_id" => $employee_id->employee_id,
									"resume" => base_url()."uploads/employee_document/".$resume_file,
									"offer_letter" => base_url()."uploads/employee_document/".$offer_letter_file,
									"joining_letter" => base_url()."uploads/employee_document/".$joining_letter_file,
									"contract_paper" => base_url()."uploads/employee_document/".$contract_paper_file,
									"id_proff" => base_url()."uploads/employee_document/".$id_proff_file,
									"other_document" => base_url()."uploads/employee_document/".$other_documents_file
								);
								$this->employee_m->insert_employee_document($employee_document_array);
								
								//echo var_dump($employee_array, $employee_bank_array, $employee_document_array);
								$this->session->set_flashdata('success', $this->lang->line('menu_success'));
								redirect(base_url("employee/index"));
								
							} else {
								$this->data["image"] = "Invalid file";
								$this->data["subview"] = "employee/add";
								$this->load->view('_layout_main', $this->data);
							}
						}
					}
				}
			} else {
				$this->data["subview"] = "employee/add";
				$this->load->view('_layout_main', $this->data);
			}
		} else {
			//load error view
			$this->data["subview"] = "error";
			$this->load->view('_layout_main', $this->data, TRUE);
		}
	}
	
	public function profile($employeeID){
		//get user session
		$usertype = $this->session->userdata("usertype");
		$school_id = $this->session->userdata('schoolID');
		
		if($usertype == "Super Admin" || $usertype == "Admin") {
			if($employeeID){
				//get single employee details
				$this->data['employee_info'] = $this->employee_m->single_emplyee_info($school_id, $employeeID);
				
				$this->data["subview"] = "employee/profile";
				$this->load->view('_layout_main', $this->data);
			}else{
				$this->data["subview"] = "employee/index";
				$this->load->view('_layout_main', $this->data);
			}
		} else {
			//load error view
			$this->data["subview"] = "error";
			$this->load->view('_layout_main', $this->data, TRUE);
		}
	}
	
	public function delete($employeeID){
		//get user session
		$usertype = $this->session->userdata("usertype");
		$school_id = $this->session->userdata('schoolID');
		
		if($usertype == "Super Admin" || $usertype == "Admin") {
			if($employeeID){
				$deleteEmployee = $this->employee_m->delete_single_employee($school_id, $employeeID);
				
				if($deleteEmployee == true){
					$this->session->set_flashdata('success', $this->lang->line('menu_success'));
					redirect(base_url("employee/index"));
				}else{
					$this->session->set_flashdata('error', 'error was encountered please try again.');
					redirect(base_url("employee/index"));
				}
			}else{
				$this->data["subview"] = "employee/index";
				$this->load->view('_layout_main', $this->data);
			}
		} else {
			//load error view
			$this->data["subview"] = "error";
			$this->load->view('_layout_main', $this->data, TRUE);
		}
	}
}

/* End of file media.php */
/* Location: .//D/xampp/htdocs/school/mvc/controllers/media.php */
?>
