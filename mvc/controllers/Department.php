<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Department extends MY_Controller {
/*
| -----------------------------------------------------
| PRODUCT NAME: 	CloudSkul
| -----------------------------------------------------
| AUTHOR:			CLOUDSKUL TEAM
| -----------------------------------------------------
| EMAIL:			info@cloudskul.com
| -----------------------------------------------------
| COPYRIGHT:		RESERVED BY Cloudskul
| -----------------------------------------------------
| WEBSITE:			http://cloudskul.com
| -----------------------------------------------------
*/
	public function __construct(){
		parent::__construct();
		$this->load->model('department_m');	
		$this->load->model('employee_m');
		$language = $this->session->userdata('lang');
		$this->lang->load('department', $language);
	}
	
	public function index(){
		//get user session
		$usertype = $this->session->userdata("usertype");
		$schoolID = $this->session->userdata('schoolID');
		
		if($usertype == "Super Admin" || $usertype == "Admin") {
			//get department details for each school
			$this->data['unique_school_departments'] = $this->department_m->get_unique_department($schoolID);
			//$department_id = 26;
			//$this->data['unique_designations'] = $this->department_m->get_unique_designations($department_id);
			
			//department table initials
			/*$this->department_m->_table_name = "departments"; //table name
			$this->department_m->_order_by = "department_id"; //table primary key
			$this->data['all_dept_info'] = $this->department_m->get();*/ //get all departments from departments table
			
			
			// get all department info and designation info using for loop to map them
			/*foreach ($this->data['all_dept_info'] as $v_dept_info) {
				$this->data['all_department_info'][] = $this->department_m->get_add_department_by_id($schoolID, $v_dept_info->department_id);
			}*/
			
			//load view if data is gotten
			$this->data['subview'] = 'department/index';
			$this->load->view('_layout_main', $this->data);
		} else {
			//load error view
			$this->data["subview"] = "error";
			$this->load->view('_layout_main', $this->data);
		}
	}
	
	public function designations_list($department_id){
		$department_id = $department_id;
		$this->data['unique_designations'] = $this->department_m->get_unique_designations($department_id);
		$this->load->view('department/designations_list', $this->data); 
	}
	
	protected function rules(){
		$rules = array(
			array(
				'field' => 'department_name', 
				'rules' => 'trim|required|xss_clean|max_length[60]'
			), 
			array(
				'field' => 'designation_name[]',
				'rules' => 'trim|required|max_length[100]|xss_clean'
			)
		);
		return $rules;
	}
	
	public function add(){
		//get user session
		$usertype = $this->session->userdata("usertype");
		$school_id = $this->session->userdata('schoolID');
		
		if($usertype == "Super Admin" || $usertype == "Admin") {
			if($_POST) {
				$rules = $this->rules();
				$this->form_validation->set_rules($rules);
				if ($this->form_validation->run() == FALSE) {
					$this->data["subview"] = "department/add";
					$this->load->view('_layout_main', $this->data);			
				} else {
					//do something
					$department_ID = $this->department_m->get_unique_id($school_id, $this->input->post("department_name"));
					if($department_ID){
						$this->session->set_flashdata('error', 'Department already exists, please try again');
						redirect(base_url("department/add"));
					}else{
						$department_array = array(
							"schoolID" => $school_id,
							"department_name" => $this->input->post("department_name")
						);
						
						$this->department_m->insert_departments($department_array);
						
						$departmentName = $this->input->post("department_name");
	 
						$designationNames = $this->input->post('designation_name', TRUE);
						$department_ID = $this->department_m->get_unique_id($school_id, $departmentName);
						foreach($designationNames as $designationName) {

							$designation_array = array(              
								'department_id' => $department_ID->department_id,
								'designation_name' => $designationName
							);                  
							$this->department_m->insert_designations($designation_array);
						}
						/*$designation_array = array(
							"department_id" => $department_ID->department_id,
							"designation_name" => $designationName
						);
						$this->department_m->insert_designations($designation_array);*/
						
						$this->session->set_flashdata('success', $this->lang->line('menu_success'));
						redirect(base_url("department/index"));
					}
				}
			} else {
				$this->data["subview"] = "department/add";
				$this->load->view('_layout_main', $this->data);
			}
		} else {
			//load error view
			$this->data["subview"] = "error";
			$this->load->view('_layout_main', $this->data, TRUE);
		}
	
	}
	
	public function delete_department($departmentID){
		//get user session
		$usertype = $this->session->userdata("usertype");
		$school_id = $this->session->userdata('schoolID');
		
		$deleteDepartment = $this->department_m->delete_department_id($departmentID, $school_id);
		$deleteDesignation = $this->department_m->delete_designation_id($departmentID);
		
		if($deleteDepartment == true && $deleteDesignation == true){
			$this->session->set_flashdata('success', $this->lang->line('menu_success'));
			redirect(base_url("department/index"));
		}
	}
	
	public function delete_designation($designation_id){
		//get user session
		$usertype = $this->session->userdata("usertype");
		$school_id = $this->session->userdata('schoolID');
		
		$delete_designation = $this->department_m->delete_designation_m($designation_id);
		if($delete_designation == true){
			$this->session->set_flashdata('success', $this->lang->line('menu_success'));
			redirect(base_url("department/index"));
		}
	}
	
	protected function edit_designation_rules(){
		$edit_designation_rules = array(
			array(
				'field' => 'designation_name',
				'label' => 'designation name',
				'rules' => 'trim|required|xss_clean|max_length[60]'
			)
		);
		return $edit_designation_rules;
	}
	
	public function update($designation_id){
		//get user session
		$usertype = $this->session->userdata("usertype");
		$school_id = $this->session->userdata("schoolID");
		
		if($usertype == "Super Admin" || $usertype == "Admin"){
			$this->data['designation_details'] = $this->department_m->get_single_designation($designation_id);
			if($_POST){
				$edit_designation_rules = $this->edit_designation_rules();
				$this->form_validation->set_rules($edit_designation_rules);
				if ($this->form_validation->run() == FALSE) {
					$this->data["subview"] = "department/update";
					$this->load->view('_layout_main', $this->data);			
				} else {
					$designation_array = array(
						"designation_name" => $this->input->post("designation_name")
					);
					
					$edit_designation = $this->department_m->edit_designation_m($designation_array, $designation_id);
					if($edit_designation == true){
						$this->session->set_flashdata('success', $this->lang->line('menu_success'));
						redirect(base_url("department/index"));
					}
				}
			}else{
				$this->data["subview"] = "department/update";
				$this->load->view('_layout_main', $this->data);
			}
		}
	}
	
	public function edit($departmentName, $departmentID){
		//get user session
		$usertype = $this->session->userdata("usertype");
		$school_id = $this->session->userdata("schoolID");
		
		if($usertype == "Super Admin" || $usertype == "Admin") {
			//retrieve single department from database based on departmentID and schoolID
			$this->data['department_details'] = $this->department_m->get_unique_id($school_id, $departmentName);
			
			$this->data['all_department_info'] = $this->department_m->get_add_department_by_id($school_id, $departmentID);
			
			if($_POST) {
				$rules = $this->rules();
				$this->form_validation->set_rules($rules);
				if ($this->form_validation->run() == FALSE) {
					$this->data["subview"] = "department/edit";
					$this->load->view('_layout_main', $this->data);			
				} else {
					//do something
					$department_array = array(
						"schoolID" => $school_id,
						"department_name" => $this->input->post("department_name")
					);
					
					$this->department_m->update_departments($department_array, $departmentID, $school_id);
					
					//update designation table
					$designation_id = $this->input->post('designation_id');
					$designationNames = $this->input->post('designation_name');
					
					foreach($designationNames as $key => $department_name){
						$designation_array = array(
								"designation_name" => $department_name
							);
						$id = $designation_id[$key];
						$department_ID = $this->input->post('department_id');
						$this->department_m->update_department_details($designation_array, $id, $department_ID);
					}
					
					$this->session->set_flashdata('success', $this->lang->line('menu_success'));
					redirect(base_url("department/index"));
				}
			}else{
				//load the edit view if session exists
				$this->data["subview"] = "department/edit";
				$this->load->view('_layout_main', $this->data);
			}
		}else{
			//load error view
			$this->data["subview"] = "error";
			$this->load->view('_layout_main', $this->data, TRUE);
		}
	}
	
	public function check_designations($department_id, $v_designations) { // check_designations by id and designation
        $where = array('department_id' => $department_id, 'designations' => $v_designations);
        return $this->department_m->check_by($where, 'designations');
    }
}

/* End of file media.php */
/* Location: .//D/xampp/htdocs/school/mvc/controllers/media.php */
?>
