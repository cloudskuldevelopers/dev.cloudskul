<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mark extends MY_Controller {
/*
| -----------------------------------------------------
| PRODUCT NAME: 	CloudSkul
| -----------------------------------------------------
| AUTHOR:			CLOUDSKUL TEAM
| -----------------------------------------------------
| EMAIL:			info@cloudskul.com
| -----------------------------------------------------
| COPYRIGHT:		RESERVED BY Cloudskul
| -----------------------------------------------------
| WEBSITE:			http://cloudskul.com
| -----------------------------------------------------
*/
	function __construct() {
		parent::__construct();
		$this->load->model("student_m");
		$this->load->model("mark_m");
		$this->load->model("grade_m");
		$this->load->model("classes_m");
		$this->load->model("exam_m");
		$this->load->model("assessments_m");
		$this->load->model("subject_m");
		$this->load->model("user_m");
		$this->load->model("section_m");
		$this->load->model("parentes_m");
		$language = $this->session->userdata('lang');
		$this->lang->load('mark', $language);	
	}

	protected function rules() {
		$rules = array(
			array(
				'field' => 'examID', 
				'label' => $this->lang->line("mark_exam"), 
				'rules' => 'trim|required|xss_clean|max_length[11]|callback_check_exam'
			), 
			array(
				'field' => 'classesID', 
				'label' => $this->lang->line("mark_classes"), 
				'rules' => 'trim|required|xss_clean|max_length[11]|callback_check_classes'
			), 
			array(
				'field' => 'subjectID', 
				'label' => $this->lang->line("mark_subject"),
				'rules' => 'trim|required|xss_clean|max_length[11]|callback_check_subject'
			)
		);
		return $rules;
	}


	public function index() {
		$usertype = $this->session->userdata("usertype");
		$schoolID = $this->session->userdata('schoolID');
		if($usertype == "Admin" || $usertype == "Super Admin" || $usertype == "Teacher") {
			$id = htmlentities($this->db->escape_str($this->uri->segment(3)));
			if((int)$id) {
				$this->data['set'] = $id;
				$this->data['classes'] = $this->classes_m->get_order_by_classes(array('schoolID' => $schoolID));
				$this->data['student'] = $this->student_m->get_order_by_student(array('schoolID' => $schoolID, 'classesID' => $id));
				$this->data["subview"] = "mark/index";
				$this->load->view('_layout_main', $this->data);
			} else {
				$this->data['classes'] = $this->classes_m->get_order_by_classes(array('schoolID' => $schoolID));
				$this->data["subview"] = "mark/search";
				$this->load->view('_layout_main', $this->data);
			}
		} elseif($usertype == 'Parent') {
			$username = $this->session->userdata("username");
			$parent = $this->parentes_m->get_single_parentes(array('username' => $username));
			$this->data['allstudents'] = $this->student_m->get_order_by_student(array('parentID' => $parent->parentID));
			$id = htmlentities($this->db->escape_str($this->uri->segment(3)));

			if((int)$id) {
				$checkstudent = $this->student_m->get_single_student(array('studentID' => $id));
				if(count($checkstudent)) {
					$classesID = $checkstudent->classesID;
					$studentID = $checkstudent->studentID;
					$student = $checkstudent;
					$this->data['set'] = $id;
					$this->data["student"] = $this->student_m->get_student($studentID);
					$this->data["classes"] = $this->student_m->get_class($classesID);
					
					if($this->data["student"] && $this->data["classes"]) {
						$this->data["exams"] = $this->exam_m->get_exam();
						$this->data["grades"] = $this->grade_m->get_grade();
						$this->data["marks"] = $this->mark_m->get_order_by_mark(array("studentID" => $student->studentID, "classesID" => $student->classesID));
						$this->data["section"] = $this->section_m->get_section($this->data['student']->sectionID);
							
						$this->data["subview"] = "mark/index_parent";
						$this->load->view('_layout_main', $this->data);
					} else {
						$this->data["subview"] = "error";
						$this->load->view('_layout_main', $this->data);
					}
	
				} else {
					$this->data["subview"] = "error";
					$this->load->view('_layout_main', $this->data);
				}
			} else {
				$this->data["subview"] = "mark/search_parent";
				$this->load->view('_layout_main', $this->data);
			}

		} else {
			$this->data["subview"] = "error";
			$this->load->view('_layout_main', $this->data);
		}
	}

	public function add_exam() {
		$usertype = $this->session->userdata("usertype");
		$schoolID = $this->session->userdata('schoolID');
		if($usertype == "Admin" || $usertype == "Super Admin" || $usertype == "Teacher") {
			$this->data['students'] = array();
			$this->data['set_exam'] = 0;
			$this->data['set_classes'] = 0;
			$this->data['set_subject'] = 0;
			$classesID = $this->input->post("classesID");
			if($classesID != 0) {
				if($usertype == "Admin" || $usertype == "Super Admin") {
					$this->data['subjects'] = $this->subject_m->get_subject_call($classesID);
				} elseif($usertype == "Teacher") {
					$username = $this->session->userdata("username");
					$teacher = $this->user_m->get_username_row("teacher", array('schoolID' => $schoolID, "username" => $username));
					$this->data['subjects'] = $this->subject_m->get_order_by_subject(array('schoolID' => $schoolID, "classesID" => $classesID, "teacherID" => $teacher->teacherID));
				}
			} else {
				$this->data['subjects'] = 0;
			}
			$this->data['exams'] = $this->exam_m->get_order_by_exam(array('schoolID' => $schoolID));
			$this->data['classes'] = $this->classes_m->get_order_by_classes(array("schoolID" => $schoolID));
			if($_POST) {
				$rules = $this->rules();
				$this->form_validation->set_rules($rules);
				if ($this->form_validation->run() == FALSE) { 
					$this->data["subview"] = "mark/add_exam";
					$this->load->view('_layout_main', $this->data);			
				} else {
					$examID = $this->input->post('examID');
					$classesID = $this->input->post('classesID');
					$subjectID = $this->input->post('subjectID');
					$this->data['set_exam'] = $examID;
					$this->data['set_classes'] = $classesID;
					$this->data['set_subject'] = $subjectID;

					$exam = $this->exam_m->get_single_exam(array('schoolID' => $schoolID, 'examID' => $examID));
					$subject = $this->subject_m->get_single_subject(array('schoolID' => $schoolID, 'subjectID' => $subjectID));
					$year = date("Y");
					$students = $this->student_m->get_order_by_student(array('schoolID' => $schoolID, "classesID" => $classesID));
					
						if(count($students)) {
							foreach ($students as $student) {
								$studentID = $student->studentID;
								$in_student = $this->mark_m->get_order_by_mark(array('schoolID' => $schoolID, "examID" => $examID, "classesID" => $classesID, "subjectID" => $subjectID, "studentID" => $studentID));
								// var_dump($in_student); die("Trying out someting E.E");
								if(!count($in_student)) {
									$array = array(
										"schoolID" => $this->session->userdata('schoolID'),
										"examID" => $examID,
										"exam" => $exam->exam,
										"studentID" => $studentID,
										"classesID" => $classesID,
										"subjectID" => $subjectID,
										"subject" => $subject->subject,
										"year" => $year,
										"create_date" => date("Y-m-d h:i:s"),
										"modify_date" => date("Y-m-d h:i:s"),
										"create_userID" => $this->session->userdata('loginuserID'),
										"create_username" => $this->session->userdata('username'),
										"create_usertype" => $this->session->userdata('usertype'),

									);
									$this->mark_m->insert_mark($array);
								}
							}
							$this->data['students'] = $students;
							$all_student = $this->mark_m->get_order_by_mark(array('schoolID' => $schoolID, "examID" => $examID, "classesID" => $classesID, "subjectID" => $subjectID));
							$this->data['marks'] = $all_student;
						}
						
					$this->data["subview"] = "mark/add_exam";
					$this->load->view('_layout_main', $this->data);
				}
			} else {
				$this->data["subview"] = "mark/add_exam";
				$this->load->view('_layout_main', $this->data);
			}
		} else {
			$this->data["subview"] = "error";
			$this->load->view('_layout_main', $this->data);
		}
	}
	
	public function add_assessment() {
		$usertype = $this->session->userdata("usertype");
		$schoolID = $this->session->userdata('schoolID');
		if($usertype == "Admin" || $usertype == "Super Admin" || $usertype == "Teacher") {
			$this->data['students'] = array();
			$this->data['set_exam'] = 0;
			$this->data['set_classes'] = 0;
			$this->data['set_subject'] = 0;
			$classesID = $this->input->post("classesID");
			if($classesID != 0) {
				if($usertype == "Admin" || $usertype == "Super Admin") {
					$this->data['subjects'] = $this->subject_m->get_subject_call($classesID);
				} elseif($usertype == "Teacher") {
					$username = $this->session->userdata("username");
					$teacher = $this->user_m->get_username_row("teacher", array('schoolID' => $schoolID, "username" => $username));
					$this->data['subjects'] = $this->subject_m->get_order_by_subject(array('schoolID' => $schoolID, "classesID" => $classesID, "teacherID" => $teacher->teacherID));
				}
			} else {
				$this->data['subjects'] = 0;
			}
			$this->data['assessments'] = $this->assessments_m->get_order_by_assessments(array('schoolID' => $schoolID));
			$this->data['classes'] = $this->classes_m->get_order_by_classes(array("schoolID" => $schoolID));
			if($_POST) {
				$rules = $this->rules();
				$this->form_validation->set_rules($rules);
				if ($this->form_validation->run() == FALSE) { 
					$this->data["subview"] = "mark/add_assessments";
					$this->load->view('_layout_main', $this->data);			
				} else {
					$examID = $this->input->post('examID');
					$classesID = $this->input->post('classesID');
					$subjectID = $this->input->post('subjectID');
					$this->data['set_exam'] = $examID;
					$this->data['set_classes'] = $classesID;
					$this->data['set_subject'] = $subjectID;

					$assessment = $this->assessments_m->get_single_assessments(array('schoolID' => $schoolID, 'testID' => $examID));
					$subject = $this->subject_m->get_single_subject(array('schoolID' => $schoolID, 'subjectID' => $subjectID));
					$year = date("Y");
					$students = $this->student_m->get_order_by_student(array('schoolID' => $schoolID, "classesID" => $classesID));
					
						if(count($students)) {
							foreach ($students as $student) {
								$studentID = $student->studentID;
								$in_student = $this->mark_m->get_order_by_mark_assessment(array('schoolID' => $schoolID, "testID" => $examID, "classesID" => $classesID, "subjectID" => $subjectID, "studentID" => $studentID));
								// var_dump($in_student); die("Trying out someting E.E");
								if(!count($in_student)) {
									$array = array(
										"schoolID" => $this->session->userdata('schoolID'),
										"testID" => $examID,
										"assessment" => $assessment->assessments,
										"studentID" => $studentID,
										"classesID" => $classesID,
										"subjectID" => $subjectID,
										"subject" => $subject->subject,
										"year" => $year,
										"create_date" => date("Y-m-d h:i:s"),
										"modify_date" => date("Y-m-d h:i:s"),
										"create_userID" => $this->session->userdata('loginuserID'),
										"create_username" => $this->session->userdata('username'),
										"create_usertype" => $this->session->userdata('usertype'),

									);
									$this->mark_m->insert_mark_assessment($array);
								}
							}
							$this->data['students'] = $students;
							$all_student = $this->mark_m->get_order_by_mark_assessment(array('schoolID' => $schoolID, "testID" => $examID, "classesID" => $classesID, "subjectID" => $subjectID));
							$this->data['marks'] = $all_student;
						}
						
					$this->data["subview"] = "mark/add_assessments";
					$this->load->view('_layout_main', $this->data);
				}
			} else {
				$this->data["subview"] = "mark/add_assessments";
				$this->load->view('_layout_main', $this->data);
			}
		} else {
			$this->data["subview"] = "error";
			$this->load->view('_layout_main', $this->data);
		}
	}

	function mark_send() {
		$examID = $this->input->post("examID");
		$classesID = $this->input->post("classesID");
		$subjectID = $this->input->post("subjectID");
		$inputs = $this->input->post("inputs");
		$schoolID = $this->session->userdata('schoolID');
		$expload = explode("$" , $inputs);
		$ex_array = array();
		foreach ($expload as $key => $value) {
			if($value == "") {
				break;
			} else {
				$ar_exp = explode(":", $value);
				$ex_array[$ar_exp[0]] = $ar_exp[1];
			}
		}

		$students = $this->student_m->get_order_by_student(array('schoolID' => $schoolID, "classesID" => $classesID));
		foreach ($students as $student) {
			foreach ($ex_array as $key => $mark) {
				if($key == $student->studentID) {
					$array = array("mark" => $mark);
					$this->mark_m->update_mark_classes($array, array("examID" => $examID, "classesID" => $classesID, "subjectID" => $subjectID, "studentID" => $student->studentID));
					break;
				}
			}
		}
		echo $this->lang->line('mark_success');
	}
	
	function mark_assessment_send() {
		$examID = $this->input->post("examID");
		$classesID = $this->input->post("classesID");
		$subjectID = $this->input->post("subjectID");
		$inputs = $this->input->post("inputs");
		$schoolID = $this->session->userdata('schoolID');
		$expload = explode("$" , $inputs);
		$ex_array = array();
		foreach ($expload as $key => $value) {
			if($value == "") {
				break;
			} else {
				$ar_exp = explode(":", $value);
				$ex_array[$ar_exp[0]] = $ar_exp[1];
			}
		}

		$students = $this->student_m->get_order_by_student(array('schoolID' => $schoolID, "classesID" => $classesID));
		foreach ($students as $student) {
			foreach ($ex_array as $key => $mark) {
				if($key == $student->studentID) {
					$array = array("mark" => $mark);
					$this->mark_m->update_mark_assessment_classes($array, array("testID" => $examID, "classesID" => $classesID, "subjectID" => $subjectID, "studentID" => $student->studentID));
					break;
				}
			}
		}
		echo $this->lang->line('mark_success');
	}

	public function view() {
		$usertype = $this->session->userdata("usertype");
		$schoolID = $this->session->userdata('schoolID');
		if($usertype == "Admin" || $usertype == "Super Admin" || $usertype == "Teacher") {
			$id = htmlentities($this->db->escape_str($this->uri->segment(3)));
			$url = htmlentities($this->db->escape_str($this->uri->segment(4)));
			
			if ((int)$id && (int)$url) {
				$this->data["student"] = $this->student_m->get_single_student(array("schoolID" => $schoolID, 'studentID' => $id));
				$this->data["classes"] = $this->student_m->get_class($url);
				if($this->data["student"] && $this->data["classes"]) {
					$this->data['set'] = $url;
					$this->data["exams"] = $this->exam_m->get_order_by_exam(array("schoolID" => $schoolID));
					$this->data["grades"] = $this->grade_m->get_order_by_grade(array("schoolID" => $schoolID));
					$this->data["marks"] = $this->mark_m->get_order_by_mark(array('schoolID' => $schoolID, "studentID" =>$id, "classesID" => $url));
					$this->data["section"] = $this->section_m->get_single_section(array('schoolID' => $schoolID, 'sectionID' => $this->data['student']->sectionID));

					$this->data["subview"] = "mark/view";
					$this->load->view('_layout_main', $this->data);
				} else {
					$this->data["subview"] = "error";
					$this->load->view('_layout_main', $this->data);
				}
			} else {
				$this->data["subview"] = "error";
				$this->load->view('_layout_main', $this->data);
			}
		} elseif($usertype == "Student") {
			$username = $this->session->userdata("username");
			$student = "";
			if($usertype == "Student") {
				$student = $this->user_m->get_username_row("student", array('schoolID' => $schoolID, "username" => $username));
			} elseif($usertype == "Parent") {
				$user = $this->user_m->get_username_row("parent", array('schoolID' => $schoolID, "username" => $username));
				$student = $this->student_m->get_single_student(array('schoolID' => $schoolID, 'studentID' => $user->studentID));
			}
			$this->data["student"] = $this->student_m->get_single_student(array('schoolID' => $schoolID, 'studentID' => $student->studentID));
			$this->data["classes"] = $this->student_m->get_class($student->classesID);
			if($this->data["student"] && $this->data["classes"]) {

				$this->data["exams"] = $this->exam_m->get_order_by_exam(array("schoolID" => $schoolID));
				$this->data["grades"] = $this->grade_m->get_order_by_grade(array("schoolID" => $schoolID));
				$this->data["marks"] = $this->mark_m->get_order_by_mark(array("studentID" => $student->studentID, "classesID" => $student->classesID));

				$this->data["section"] = $this->section_m->get_single_section(array('schoolID' => $schoolID, 'sectionID' => $this->data['student']->sectionID));
				$this->data["subview"] = "mark/view";
				$this->load->view('_layout_main', $this->data);
			} else {
				$this->data["subview"] = "error";
				$this->load->view('_layout_main', $this->data);
			}
		} else {
			$this->data["subview"] = "error";
			$this->load->view('_layout_main', $this->data);
		}
	}
	
	public function view_exams() {
		$usertype = $this->session->userdata("usertype");
		$schoolID = $this->session->userdata('schoolID');
		if($usertype == "Admin" || $usertype == "Super Admin" || $usertype == "Teacher") {
			$id = htmlentities($this->db->escape_str($this->uri->segment(3)));
			$url = htmlentities($this->db->escape_str($this->uri->segment(4)));
			
			if ((int)$id && (int)$url) {
				$this->data["student"] = $this->student_m->get_single_student(array("schoolID" => $schoolID, 'studentID' => $id));
				$this->data["classes"] = $this->student_m->get_class($url);
				if($this->data["student"] && $this->data["classes"]) {
					$this->data['set'] = $url;
					$this->data["exams"] = $this->exam_m->get_order_by_exam(array("schoolID" => $schoolID));
					$this->data["grades"] = $this->grade_m->get_order_by_grade(array("schoolID" => $schoolID));
					$this->data["marks"] = $this->mark_m->get_order_by_mark(array('schoolID' => $schoolID, "studentID" =>$id, "classesID" => $url));
					$this->data["section"] = $this->section_m->get_single_section(array('schoolID' => $schoolID, 'sectionID' => $this->data['student']->sectionID));

					$this->data["subview"] = "mark/view";
					$this->load->view('_layout_main', $this->data);
				} else {
					$this->data["subview"] = "error";
					$this->load->view('_layout_main', $this->data);
				}
			} else {
				$this->data["subview"] = "error";
				$this->load->view('_layout_main', $this->data);
			}
		} elseif($usertype == "Student") {
			$username = $this->session->userdata("username");
			$student = "";
			if($usertype == "Student") {
				$student = $this->user_m->get_username_row("student", array('schoolID' => $schoolID, "username" => $username));
			} elseif($usertype == "Parent") {
				$user = $this->user_m->get_username_row("parent", array('schoolID' => $schoolID, "username" => $username));
				$student = $this->student_m->get_single_student(array('schoolID' => $schoolID, 'studentID' => $user->studentID));
			}
			$this->data["student"] = $this->student_m->get_single_student(array('schoolID' => $schoolID, 'studentID' => $student->studentID));
			$this->data["classes"] = $this->student_m->get_class($student->classesID);
			if($this->data["student"] && $this->data["classes"]) {

				$this->data["exams"] = $this->exam_m->get_order_by_exam(array("schoolID" => $schoolID));
				$this->data["grades"] = $this->grade_m->get_order_by_grade(array("schoolID" => $schoolID));
				$this->data["marks"] = $this->mark_m->get_order_by_mark(array("studentID" => $student->studentID, "classesID" => $student->classesID));

				$this->data["section"] = $this->section_m->get_single_section(array('schoolID' => $schoolID, 'sectionID' => $this->data['student']->sectionID));
				$this->data["subview"] = "mark/view";
				$this->load->view('_layout_main', $this->data);
			} else {
				$this->data["subview"] = "error";
				$this->load->view('_layout_main', $this->data);
			}
		} else {
			$this->data["subview"] = "error";
			$this->load->view('_layout_main', $this->data);
		}
	}
	
	public function view_assessment() {
		$usertype = $this->session->userdata("usertype");
		$schoolID = $this->session->userdata('schoolID');
		if($usertype == "Admin" || $usertype == "Super Admin" || $usertype == "Teacher") {
			$id = htmlentities($this->db->escape_str($this->uri->segment(3)));
			$url = htmlentities($this->db->escape_str($this->uri->segment(4)));
			
			if ((int)$id && (int)$url) {
				$this->data["student"] = $this->student_m->get_single_student(array("schoolID" => $schoolID, 'studentID' => $id));
				$this->data["classes"] = $this->student_m->get_class($url);
				if($this->data["student"] && $this->data["classes"]) {
					$this->data['set'] = $url;
					$this->data["assessments"] = $this->assessments_m->get_order_by_assessments(array("schoolID" => $schoolID));
					$this->data["grades"] = $this->grade_m->get_order_by_grade(array("schoolID" => $schoolID));
					$this->data["marks"] = $this->mark_m->get_order_by_mark_assessment(array('schoolID' => $schoolID, "studentID" =>$id, "classesID" => $url));
					$this->data["section"] = $this->section_m->get_single_section(array('schoolID' => $schoolID, 'sectionID' => $this->data['student']->sectionID));

					$this->data["subview"] = "mark/view_assessment";
					$this->load->view('_layout_main', $this->data);
				} else {
					$this->data["subview"] = "error";
					$this->load->view('_layout_main', $this->data);
				}
			} else {
				$this->data["subview"] = "error";
				$this->load->view('_layout_main', $this->data);
			}
		} elseif($usertype == "Student") {
			$username = $this->session->userdata("username");
			$student = "";
			if($usertype == "Student") {
				$student = $this->user_m->get_username_row("student", array('schoolID' => $schoolID, "username" => $username));
			} elseif($usertype == "Parent") {
				$user = $this->user_m->get_username_row("parent", array('schoolID' => $schoolID, "username" => $username));
				$student = $this->student_m->get_single_student(array('schoolID' => $schoolID, 'studentID' => $user->studentID));
			}
			$this->data["student"] = $this->student_m->get_single_student(array('schoolID' => $schoolID, 'studentID' => $student->studentID));
			$this->data["classes"] = $this->student_m->get_class($student->classesID);
			if($this->data["student"] && $this->data["classes"]) {

				$this->data["exams"] = $this->exam_m->get_order_by_exam(array("schoolID" => $schoolID));
				$this->data["grades"] = $this->grade_m->get_order_by_grade(array("schoolID" => $schoolID));
				$this->data["marks"] = $this->mark_m->get_order_by_mark(array("studentID" => $student->studentID, "classesID" => $student->classesID));

				$this->data["section"] = $this->section_m->get_single_section(array('schoolID' => $schoolID, 'sectionID' => $this->data['student']->sectionID));
				$this->data["subview"] = "mark/view_assessment";
				$this->load->view('_layout_main', $this->data);
			} else {
				$this->data["subview"] = "error";
				$this->load->view('_layout_main', $this->data);
			}
		} else {
			$this->data["subview"] = "error";
			$this->load->view('_layout_main', $this->data);
		}
	}
	
	public function view_all() {
		$usertype = $this->session->userdata("usertype");
		$schoolID = $this->session->userdata('schoolID');
		if($usertype == "Admin" || $usertype == "Super Admin" || $usertype == "Teacher") {
			$id = htmlentities($this->db->escape_str($this->uri->segment(3)));
			$url = htmlentities($this->db->escape_str($this->uri->segment(4)));
			
			if ((int)$id && (int)$url) {
				$this->data["student"] = $this->student_m->get_single_student(array("schoolID" => $schoolID, 'studentID' => $id));
				$this->data["classes"] = $this->student_m->get_class($url);
				if($this->data["student"] && $this->data["classes"]) {
					$this->data['set'] = $url;
					$this->data["exams"] = $this->exam_m->get_order_by_exam(array("schoolID" => $schoolID));
					$this->data["grades"] = $this->grade_m->get_order_by_grade(array("schoolID" => $schoolID));
					$this->data["marks"] = $this->mark_m->get_order_by_mark(array('schoolID' => $schoolID, "studentID" =>$id, "classesID" => $url));
					$this->data["section"] = $this->section_m->get_single_section(array('schoolID' => $schoolID, 'sectionID' => $this->data['student']->sectionID));

					$this->data["subview"] = "mark/view";
					$this->load->view('_layout_main', $this->data);
				} else {
					$this->data["subview"] = "error";
					$this->load->view('_layout_main', $this->data);
				}
			} else {
				$this->data["subview"] = "error";
				$this->load->view('_layout_main', $this->data);
			}
		} elseif($usertype == "Student") {
			$username = $this->session->userdata("username");
			$student = "";
			if($usertype == "Student") {
				$student = $this->user_m->get_username_row("student", array('schoolID' => $schoolID, "username" => $username));
			} elseif($usertype == "Parent") {
				$user = $this->user_m->get_username_row("parent", array('schoolID' => $schoolID, "username" => $username));
				$student = $this->student_m->get_single_student(array('schoolID' => $schoolID, 'studentID' => $user->studentID));
			}
			$this->data["student"] = $this->student_m->get_single_student(array('schoolID' => $schoolID, 'studentID' => $student->studentID));
			$this->data["classes"] = $this->student_m->get_class($student->classesID);
			if($this->data["student"] && $this->data["classes"]) {

				$this->data["exams"] = $this->exam_m->get_order_by_exam(array("schoolID" => $schoolID));
				$this->data["grades"] = $this->grade_m->get_order_by_grade(array("schoolID" => $schoolID));
				$this->data["marks"] = $this->mark_m->get_order_by_mark(array("studentID" => $student->studentID, "classesID" => $student->classesID));

				$this->data["section"] = $this->section_m->get_single_section(array('schoolID' => $schoolID, 'sectionID' => $this->data['student']->sectionID));
				$this->data["subview"] = "mark/view";
				$this->load->view('_layout_main', $this->data);
			} else {
				$this->data["subview"] = "error";
				$this->load->view('_layout_main', $this->data);
			}
		} else {
			$this->data["subview"] = "error";
			$this->load->view('_layout_main', $this->data);
		}
	}

	function print_preview() {
		$usertype = $this->session->userdata("usertype");
		$schoolID = $this->session->userdata('schoolID');
		if($usertype == "Admin" || $usertype == "Super Admin" || $usertype == "Teacher") {
			$id = htmlentities($this->db->escape_str($this->uri->segment(3)));
			$url = htmlentities($this->db->escape_str($this->uri->segment(4)));
			
			if ((int)$id && (int)$url) {
				$this->data["student"] = $this->student_m->get_single_student(array("schoolID" => $schoolID, "studentID" => $id));
				$this->data["classes"] = $this->student_m->get_class($url);
				if($this->data["student"] && $this->data["classes"]) {
					$this->data['set'] = $url;

					$this->data["exams"] = $this->exam_m->get_order_by_exam(array("schoolID" => $schoolID));
					$this->data["grades"] = $this->grade_m->get_order_by_grade(array("schoolID" => $schoolID));
					$this->data["marks"] = $this->mark_m->get_order_by_mark(array('schoolID' => $schoolID, "studentID" =>$id, "classesID" => $url));
					$this->data["section"] = $this->section_m->get_single_section(array('schoolID' => $schoolID, 'sectionID' => $this->data['student']->sectionID));

					$this->load->library('html2pdf');
				    $this->html2pdf->folder('./assets/pdfs/');
				    $this->html2pdf->filename('Report.pdf');
				    $this->html2pdf->paper('a4', 'portrait');
				    $this->data['panel_title'] = $this->lang->line('panel_title');
					$html = $this->load->view('mark/print_preview', $this->data, true);
					$this->html2pdf->html($html);
					$this->html2pdf->create();
				} else {
					$this->data["subview"] = "error";
					$this->load->view('_layout_main', $this->data);
				}
			} else {
				$this->data["subview"] = "error";
				$this->load->view('_layout_main', $this->data);
			}
		} else {
			$this->data["subview"] = "error";
			$this->load->view('_layout_main', $this->data);
		}
	}

	public function send_mail() {
		$usertype = $this->session->userdata("usertype");
		$schoolID = $this->session->userdata('schoolID');
		if($usertype == "Admin" || $usertype == "Teacher" || $usertype == "Super Admin") {
			$id = $this->input->post('id');
			$url = $this->input->post('set');
			if ((int)$id && (int)$url) {
				$this->data["student"] = $this->student_m->get_single_student(array('schoolID' => $schoolID, 'studentID' => $id));
				$this->data["classes"] = $this->classes_m->get_single_classes(array('schoolID' => $schoolID, 'classesID' =>  $url));
				if($this->data["student"] && $this->data["classes"]) {

					$this->data['set'] = $url;
					$this->data["exams"] = $this->exam_m->get_order_by_exam(array('schoolID' => $schoolID));
					$this->data["grades"] = $this->grade_m->get_order_by_grade(array('schoolID' => $schoolID));
					$this->data["marks"] = $this->mark_m->get_order_by_mark(array('schoolID' => $schoolID, "studentID" =>$id, "classesID" => $url));
					$this->data["section"] = $this->section_m->get_single_section(array('schoolID' => $schoolID, 'sectionID' => $this->data['student']->sectionID));
					
				    $this->load->library('html2pdf');
				    $this->html2pdf->folder('uploads/report');
				    $this->html2pdf->filename('Report.pdf');
				    $this->html2pdf->paper('a4', 'portrait');
				    $this->data['panel_title'] = $this->lang->line('panel_title');
					$html = $this->load->view('mark/print_preview', $this->data, true);
					$this->html2pdf->html($html);
					$this->html2pdf->create('save');
					
					if($path = $this->html2pdf->create('save')) {
					$this->load->library('email');
					$this->email->set_mailtype("html");
					$this->email->from($this->data["siteinfos"]->email, $this->session->userdata('school'));
					$this->email->to($this->input->post('to'));
					$this->email->subject($this->input->post('subject'));
					$this->email->message($this->input->post('message'));	
					$this->email->attach($path);
						if($this->email->send()) {
							$this->session->set_flashdata('success', $this->lang->line('mail_success'));
						} else {
							$this->session->set_flashdata('error', $this->lang->line('mail_error'));
						}
					}
				} else {
					$this->data["subview"] = "error";
					$this->load->view('_layout_main', $this->data);
				}
			} else {
				$this->data["subview"] = "error";
				$this->load->view('_layout_main', $this->data);
			}
		} else {
			$this->data["subview"] = "error";
			$this->load->view('_layout_main', $this->data);
		}
	}

	function mark_list() {
		$classID = $this->input->post('id');
		if((int)$classID) {
			$string = base_url("mark/index/$classID");
			echo $string;
		} else {
			redirect(base_url("mark/index"));
		}
	}

	function student_list() {
		$studentID = $this->input->post('id');
		if((int)$studentID) {
			$string = base_url("mark/index/$studentID");
			echo $string;
		} else {
			redirect(base_url("mark/index"));
		}
	}

	function subjectcall() {
		$usertype = $this->session->userdata("usertype");
		$schoolID = $this->session->userdata('schoolID');
		$id = $this->input->post('id');
		if((int)$id) {
			if($usertype == "Admin" || $usertype == "Super Admin") {
				$allsubject = $this->subject_m->get_order_by_subject(array('schoolID' => $schoolID, "classesID" => $id));
				echo "<option value='0'>", $this->lang->line("mark_select_subject"),"</option>";
				foreach ($allsubject as $value) {
					echo "<option value=\"$value->subjectID\">",$value->subject,"</option>";
				}
			} elseif($usertype == "Teacher") {
				$username = $this->session->userdata("username");
				$teacher = $this->user_m->get_username_row("teacher", array('schoolID' => $schoolID, "username" => $username));
				$allsubject = $this->subject_m->get_order_by_subject(array('schoolID' => $schoolID, "classesID" => $id, "teacherID" => $teacher->teacherID));
				echo "<option value='0'>", $this->lang->line("mark_select_subject"),"</option>";
				foreach ($allsubject as $value) {
					echo "<option value=\"$value->subjectID\">",$value->subject,"</option>";
				}
			}
		} 
	}

	function check_exam() {
		if($this->input->post('examID') == 0) {
			$this->form_validation->set_message("check_exam", "The %s field is required");
	     	return FALSE;
		}
		return TRUE;
	}

	function check_classes() {
		if($this->input->post('classesID') == 0) {
			$this->form_validation->set_message("check_classes", "The %s field is required");
	     	return FALSE;
		}
		return TRUE;
	}

	function check_subject() {
		if($this->input->post('subjectID') == 0) {
			$this->form_validation->set_message("check_subject", "The %s field is required");
	     	return FALSE;
		}
		return TRUE;
	}
}

/* End of file class.php */
/* Location: .//D/xampp/htdocs/school/mvc/controllers/class.php */