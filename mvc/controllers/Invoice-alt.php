<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

use Omnipay\Omnipay;
class Invoice extends MY_Controller {
/*
| -----------------------------------------------------
| PRODUCT NAME: 	INILABS SCHOOL MANAGEMENT SYSTEM
| -----------------------------------------------------
| AUTHOR:			INILABS TEAM
| -----------------------------------------------------
| EMAIL:			info@inilabs.net
| -----------------------------------------------------
| COPYRIGHT:		RESERVED BY INILABS IT
| -----------------------------------------------------
| WEBSITE:			http://inilabs.net
| -----------------------------------------------------
*/
	function __construct() {
		parent::__construct();
		$this->load->model("invoice_m");
		$this->load->model("feetype_m");
		$this->load->model('payment_m');
		$this->load->model("classes_m");
		$this->load->model("student_m");
		$this->load->model("parentes_m");
		$this->load->model("section_m");
		$this->load->model("school_m");
		$this->load->model('user_m');
		$this->load->model("payment_settings_m");
		$this->load->library("remita");
		$this->load->library("ozioma");
		$language = $this->session->userdata('lang');
		$this->lang->load('invoice', $language);
		require_once(APPPATH."libraries/Omnipay/vendor/autoload.php");
	}

	public function index() {
		$usertype = $this->session->userdata("usertype");
		$schoolID = $this->session->userdata('schoolID');
		if($usertype == "Admin" || $usertype == "Super Admin" || $usertype == "Accountant") {
			$this->data['invoices'] = $this->invoice_m->get_order_by_invoice(array('schoolID' => $schoolID));
			$this->data["subview"] = "invoice/index";
			$this->load->view('_layout_main', $this->data);
		} elseif($usertype == "Student") {
			$username = $this->session->userdata("username");
			$student = $this->student_m->get_single_student(array('schoolID' => $schoolID, "username" => $username));
			$this->data['invoices'] = $this->invoice_m->get_order_by_invoice(array('schoolID' => $schoolID, 'studentID' => $student->studentID));
			$this->data["subview"] = "invoice/index";
			$this->load->view('_layout_main', $this->data);
		} elseif($usertype == "Parent") {
			
			$username = $this->session->userdata("username");
			$parent = $this->parentes_m->get_single_parentes(array('schoolID' => $schoolID, 'username' => $username));
			$this->data['students'] = $this->student_m->get_order_by_student(array('schoolID' => $schoolID, 'parentID' => $parent->parentID));
			$id = htmlentities($this->db->escape_str($this->uri->segment(3)));
			if((int)$id) {
				$checkstudent = $this->student_m->get_single_student(array('schoolID' => $schoolID, 'studentID' => $id));
				if(count($checkstudent)) {
					if($checkstudent->parentID == $parent->parentID) {
						$classesID = $checkstudent->classesID;
						$this->data['set'] = $id;
						$this->data['invoices'] = $this->invoice_m->get_order_by_invoice(array('schoolID' => $schoolID, 'studentID' => $id));
						$this->data["subview"] = "invoice/index_parent";
						$this->load->view('_layout_main', $this->data); 
					} else {
						$this->data["subview"] = "error";
						$this->load->view('_layout_main', $this->data);
					}
				} else {
					$this->data["subview"] = "error";
					$this->load->view('_layout_main', $this->data);
				}
			} else {
				$this->data["subview"] = "invoice/search_parent";
				$this->load->view('_layout_main', $this->data);
			}
		} else {
			$this->data["subview"] = "error";
			$this->load->view('_layout_main', $this->data);
		}
	}
	
	private function calculateInstallmentAmount($amount1, $amount2, $amount3, $noOfInstallments, $installmentRef, $studentID){
		$usertype = $this->session->userdata("usertype");
		if($usertype == "Parent"){
			
			//$returnArray = array();
			//check to see the selected checkboxes 
			if(isset($_POST['installment1']) && isset($_POST['installment2']) && isset($_POST['installment3'])){
				
				$amountPaid = $amount1 + $amount2 + $amount3;
				return array("installment_desc" => 'Complete Payment', "amount_paid" => $amountPaid);
				
			}
				
			if(isset($_POST['installment1']) && isset($_POST['installment2']) && $noOfInstallments == 2){
				
				$amountPaid = $amount1 + $amount2;
				return array("installment_desc" => 'Complete Payment', "amount_paid" => $amountPaid);
				
			}elseif(isset($_POST['installment1']) && isset($_POST['installment2']) && $noOfInstallments == 3){
				
				$amountPaid = $amount1 + $amount2;
				return array("installment_desc" => '1st and 2nd Installment Payment', "amount_paid" => $amountPaid);
				
			}
				
			if(isset($_POST['installment2']) && isset($_POST['installment3'])){
				
				$amountPaid = $amount2 + $amount3;
				return array("installment_desc" => '2nd and 3rd Installment Payment', "amount_paid" => $amountPaid);
				
			}
				
			if(isset($_POST['installment1']) && isset($_POST['installment3'])){
				
				$amountPaid = $amount1 + $amount3;
				return array("installment_desc" => '1st and 3rd Installment Payment', "amount_paid" => $amountPaid);
				
			}
				
			if(isset($_POST['installment1']) && $noOfInstallments == 2){
				
				$amountPaid = $amount1;
				return array("installment_desc" => '1st Installment Payment', "amount_paid" => $amountPaid);
				
			}elseif(isset($_POST['installment1']) && $noOfInstallments == 3){
				
				$amountPaid = $amount1;
				return array("installment_desc" => '1st Installment Payment', "amount_paid" => $amountPaid);
				
			}elseif(isset($_POST['installment1']) && $noOfInstallments == 1){
				
				$amountPaid = $amount1;
				return array("installment_desc" => 'Complete Payment', "amount_paid" => $amountPaid);
				
			}
				
			if(isset($_POST['installment2'])){
				
				$amountPaid = $amount2;
				return array("installment_desc" => '2nd Installment Payment', "amount_paid" => $amountPaid);
				
			}
				
			if(isset($_POST['installment3'])){
				
				$amountPaid = $amount3;
				return array("installment_desc" => '3rd Installment Payment', "amount_paid" => $amountPaid);
				
			}
			
			if(!isset($_POST['installment1']) && !isset($_POST['installment2']) && !isset($_POST['installment3'])){
				
				$this->session->set_flashdata('error', "No Installmental Plan Selected");
				redirect(base_url("invoice/installments/1/" . $installmentRef . '/' . $studentID));
				
			}
			
		}else {
			$this->data["subview"] = "error";
			$this->load->view('_layout_main', $this->data);
		}
	}
	
	public function installments($process, $installmentRef, $studentID){
		$usertype = $this->session->userdata("usertype");
		$schoolID = $this->session->userdata('schoolID');
		
		if($usertype == "Parent"){
			if($process == 'process'){
				
				//get the amount from the database 
				$invoiceInstallment = $this->invoice_m->getInstallmentByInstallmentRef($installmentRef);
				$amount1 = $invoiceInstallment->first_installment_amount;
				$amount2 = $invoiceInstallment->second_installment_amount;
				$amount3 = $invoiceInstallment->third_installment_amount;
				$noOfInstallments = $invoiceInstallment->no_of_installments;
				
				//get the amount and payment description
				$returnArray = $this->calculateInstallmentAmount($amount1, $amount2, $amount3, $noOfInstallments, $installmentRef, $studentID);
				$studentInvoiceTableDetails = $this->invoice_m->getStudentInvoiceDetails($installmentRef, $studentID);
				$invoiceID = $studentInvoiceTableDetails->invoiceID;
				
				//create a installment status ID
				$installmentStatusID = time() * rand(1234, 9999);
				
				//get the other details
				$installmentStatusTable = array(
					"schoolID" => $studentInvoiceTableDetails->schoolID,
					"studentID" => $studentInvoiceTableDetails->studentID,
					"installment_ref" => $studentInvoiceTableDetails->installment_ref,
					"invoiceID" => $studentInvoiceTableDetails->invoiceID,
					"no_of_installments" => $studentInvoiceTableDetails->installment_plan,
					"installment_statusID" => $installmentStatusID,
					"amount_paid" => $returnArray['amount_paid'],
					"installment_desc" => $returnArray['installment_desc']
				);
				
				$this->session->set_userdata('installmentStatusID', $installmentStatusID);
				$this->db->insert('installment_status', $installmentStatusTable);
				redirect(base_url('invoice/view/' . $invoiceID . '/' . $installmentStatusID));	
				
			}else{
				$this->data["installmentData"] = $this->invoice_m->getInstallmentByInstallmentRef($installmentRef);
				$this->data["invoiceData"] = $this->invoice_m->getStudentInvoiceDetails($installmentRef, $studentID);
				$this->data["installmentRef"] = $installmentRef;
				$this->data["studentID"] = $studentID;
				$this->data["subview"] = "invoice/installments";
				$this->load->view('_layout_main', $this->data);
			}
			
		}else {
			
			$this->data["subview"] = "error";
			$this->load->view('_layout_main', $this->data);
			
		}
	}
	
	protected function rules() {
		$rules = array(
				array(
					'field' => 'classesID', 
					'label' => $this->lang->line("invoice_classesID"), 
					'rules' => 'trim|required|xss_clean|max_length[11]|numeric|callback_unique_classID'
				),
				array(
					'field' => 'studentID', 
					'label' => $this->lang->line("invoice_studentID"), 
					'rules' => 'trim|required|xss_clean|max_length[11]|numeric|callback_unique_studentID'
				),
				array(
					'field' => 'feetype', 
					'label' => $this->lang->line("invoice_feetype"), 
					'rules' => 'trim|required|xss_clean|max_length[128]'
				),
				array(
					'field' => 'amount',
					'label' => $this->lang->line("invoice_amount"), 
					'rules' => 'trim|required|xss_clean|max_length[20]|numeric|callback_valid_number'
				),
				array(
					'field' => 'date', 
					'label' => $this->lang->line("invoice_date"), 
					'rules' => 'trim|required|xss_clean|max_length[10]|callback_date_valid'
				),

			);
		return $rules;
	}

	protected function payment_rules() {
		$rules = array(
				array(
					'field' => 'amount', 
					'label' => $this->lang->line("invoice_amount"), 
					'rules' => 'trim|required|xss_clean|max_length[11]|numeric|callback_valid_number'
				),
				array(
					'field' => 'payment_method', 
					'label' => $this->lang->line("invoice_paymentmethod"), 
					'rules' => 'trim|required|xss_clean|max_length[11]|callback_unique_paymentmethod'
				)
			);
		return $rules;
	}

	public function payment_type(){

		$type = $this->input->post('type');

		if($type !== ''){
			if($type == "UPL" || $type == "Interswitch"){
				echo "1000.00";

			}elseif($type == 'BANK_BRANCH'){
				echo "1500.00";
			}
		}else
		echo "Payment type not selected";
	}

	public function add($installmentalPlan = '') {
		$usertype = $this->session->userdata("usertype");
		$schoolID = $this->session->userdata('schoolID');
		if($usertype == "Admin" || $usertype == "Super Admin" || $usertype == "Accountant") {

				$this->data['feecat'] = $this->feetype_m->feeCategory();
				$this->data['classes'] = $this->classes_m->get_order_by_classes(array('schoolID' => $schoolID));
				$this->data['feetypes'] = $this->feetype_m->get_order_by_feetype(array('schoolID' => $schoolID));
				$classesID = $this->input->post("classesID");
				if($classesID != 0) {
					$this->data['students'] = $this->student_m->get_order_by_student(array('schoolID' => $schoolID, "classesID" => $classesID));
				} else {
					$this->data['students'] = "empty";
				}
				$this->data['studentID'] = 0;

			if($installmentalPlan == '1'){

				$this->data['feecat'] = $this->feetype_m->feeCategory();
				$this->data['classes'] = $this->classes_m->get_order_by_classes(array('schoolID' => $schoolID));
				$this->data['feetypes'] = $this->feetype_m->get_order_by_feetype(array('schoolID' => $schoolID));
				$classesID = $this->input->post("classesID");
				if($classesID != 0) {
					$this->data['students'] = $this->student_m->get_order_by_student(array('schoolID' => $schoolID, "classesID" => $classesID));
				} else {
					$this->data['students'] = "empty";
				}
				$this->data['studentID'] = 0;
				if($_POST) {

					$this->data['studentID'] = $this->input->post('studentID');

					$rules = $this->rules();
					$this->form_validation->set_rules($rules);

					if ($this->form_validation->run() == FALSE) {

						$this->data["subview"] = "invoice/add";
						$this->load->view('_layout_main', $this->data);

					}else{

						if($this->input->post('studentID')) {

							$classesID = $this->input->post('classesID');
							$getclasses = $this->classes_m->get_single_classes(array('schoolID' => $schoolID, 'classesID' => $classesID));
							$studentID = $this->input->post('studentID');
							$getstudent = $this->student_m->get_single_student(array('schoolID' => $schoolID, 'studentID' => $studentID));
							$getparent = $this->parentes_m->get_single_parentes(array('schoolID' => $schoolID, 'parentID' => $getstudent->parentID));
							$getschool = $this->school_m->get_school($schoolID);
							$amount = $this->input->post("amount");
							//$timesammp=DATE("dmyHis");
							$code  = 'CS';
							$code .= substr(number_format(time() * rand(),0,'',''),0,6);
							
							$installmentRef = 'CSI';
							$installmentRef .= substr((time() * rand(123, 999)),0,7);
							
							$installmentTable = array(
								'schoolID' => $schoolID,
								'installment_ref' => $installmentRef,
								'feecategory' => $this->input->post('feecategory'),
								'feetype' => $this->input->post('feetype'),
								'total_amount' => $this->input->post('amount'),
								'no_of_installments' => 1
							);
							
							$array = array(
								'schoolID' => $this->session->userdata('schoolID'),
								'classesID' => $classesID,
								'classes' => $getclasses->classes,
								'invoice_ref' => $code,
								'installment_ref' => $installmentRef,
								'studentID' => $studentID,
								'installment_plan' => 1,
								'student' => $getstudent->name,
								'roll' => $getstudent->roll,
								'feetype' => $this->input->post("feetype"),
								'amount' => $amount,
								'status' => 0,
								'date' => date("Y-m-d", strtotime($this->input->post("date"))),
								'year' => date('Y'),
								'create_date' => date("Y-m-d h:i:s"),
								'modify_date' => date("Y-m-d h:i:s"),
								'create_userID' => $this->session->userdata('loginuserID'),
								'create_username' => $this->session->userdata('username'),
								'create_usertype' => $this->session->userdata('usertype')

							);
							
							$amount = number_format(floatval($amount),2);
							$message = "An invoice for this term fees has been generated ";
							$message .="for your WARD ".$getstudent->name." with code ".$code.","; 
							$message .=" Amount:N".$amount.". Kindly Pay at http://www.cloudskul.com/payments. Thank You";
							$this->ozioma->set_message($message);//message from textfield
				            $this->ozioma->set_recipient($getparent->phone);//separate numbers with commas and include zip code in every number
				            $this->ozioma->set_sender($getschool->school);//sender from database
				            $this->ozioma->send();
							//var_dump($message);
							//break;
							$oldamount = $getstudent->totalamount;
							$nowamount = $oldamount+$amount;

							$this->student_m->update_student(array('totalamount' => $nowamount), $getstudent->studentID);
							$returnID = $this->invoice_m->insert_invoice($array);

							//insert into the installment table
							$this->db->insert('installments', $installmentTable);

							$this->session->set_flashdata('success', $this->lang->line('menu_success'));
						 	redirect(base_url("invoice/view/$returnID"));
						} else {
							$classesID = $this->input->post('classesID');
							$getclasses = $this->classes_m->get_single_classes(array('schoolID' => $schoolID, 'classesID' => $classesID));
							$getstudents = $this->student_m->get_order_by_student(array('schoolID' => $schoolID, "classesID" => $classesID));
							$amount = $this->input->post("amount");
							$code  = 'CS';
							$code .= substr(number_format(time() * rand(),0,'',''),0,6);
							
							$installmentRef = 'CSI';
							$installmentRef .= substr((time() * rand(123, 999)),0,7);
							
							$installmentTable = array(
								'schoolID' => $schoolID,
								'installment_ref' => $installmentRef,
								'feecategory' => $this->input->post('feecategory'),
								'feetype' => $this->input->post('feetype'),
								'total_amount' => $this->input->post('amount'),
								'no_of_installments' => 1
							);
							
							$getschool = $this->school_m->get_school($schoolID);
							foreach ($getstudents as $key => $getstudent) {
								$code .= substr(number_format(time() * rand(),0,'',''),0,6); //autogenerated code
								$getparent = $this->parentes_m->get_single_parentes(array('schoolID' => $schoolID, 'parentID' => $getstudent->parentID));
								
								$array = array(
									'schoolID' => $this->session->userdata('schoolID'),
									'classesID' => $classesID,
									'classes' => $getclasses->classes,
									'invoice_ref' => $code,
									'installment_ref' => $installmentRef,
									'studentID' => $getstudent->studentID,
									'installment_plan' => 1,
									'student' => $getstudent->name,
									'roll' => $getstudent->roll,
									'feetype' => $this->input->post("feetype"),
									'amount' => $amount,
									'status' => 0,
									'date' => date("Y-m-d", strtotime($this->input->post("date"))),
									'year' => date('Y'),
									'create_date' => date("Y-m-d h:i:s"),
									'modify_date' => date("Y-m-d h:i:s"),
									'create_userID' => $this->session->userdata('loginuserID'),
									'create_username' => $this->session->userdata('username'),
									'create_usertype' => $this->session->userdata('usertype')
								);
								$amount = number_format(floatval($amount),2);
								$message = "An invoice for this term fees has been generated ";
								$message .="for your WARD ".$getstudent->name." with code ".$code.","; 
								$message .=" Amount:N".$amount.". Kindly Pay at http://www.cloudskul.com/payments. Thank You";
								$this->ozioma->set_message($message);//message from textfield
					            $this->ozioma->set_recipient($getparent->phone);//separate numbers with commas and include zip code in every number
					            $this->ozioma->set_sender($getschool->school);//sender from database
					            $this->ozioma->send();
					            $amount = $this->input->post("amount");//re initialize variable
					            $code = 'CS';//re initialize variable

								$oldamount = $getstudent->totalamount;
								$nowamount = $oldamount+$amount;

								//var_dump($message);
								$this->student_m->update_student(array('totalamount' => $nowamount), $getstudent->studentID);
								$this->invoice_m->insert_invoice($array);
							}
							
							//insert into the installment table
							$this->db->insert('installments', $installmentTable);
							
							$this->session->set_flashdata('success', $this->lang->line('menu_success'));
						 	redirect(base_url("invoice/index"));
						}
					}
				} else {
					$this->data["subview"] = "invoice/add";
					$this->load->view('_layout_main', $this->data);
				}
			}

			elseif($installmentalPlan == '2'){

				$this->data['feecat'] = $this->feetype_m->feeCategory();
				$this->data['classes'] = $this->classes_m->get_order_by_classes(array('schoolID' => $schoolID));
				$this->data['feetypes'] = $this->feetype_m->get_order_by_feetype(array('schoolID' => $schoolID));
				$classesID = $this->input->post("classesID");
				if($classesID != 0) {
					$this->data['students'] = $this->student_m->get_order_by_student(array('schoolID' => $schoolID, "classesID" => $classesID));
				} else {
					$this->data['students'] = "empty";
				}
				$this->data['studentID'] = 0;
				if($_POST) {

					$this->data['studentID'] = $this->input->post('studentID');

					$rules = $this->rules();
					$this->form_validation->set_rules($rules);

					if ($this->form_validation->run() == FALSE) {

						$this->data["subview"] = "invoice/add";
						$this->load->view('_layout_main', $this->data);

					}else{
						
						//check if the total amount = installments amounts
						$installmentTotal = $this->input->post('amount1') + $this->input->post('amount2') + $this->input->post('amount3');
						if($this->input->post('amount') != $installmentTotal){
							$this->session->set_flashdata('error', "Total Amount is not equal to the sum of the installments!");
							redirect(base_url("invoice/add"));
						}

						if($this->input->post('studentID')) {

							$classesID = $this->input->post('classesID');
							$getclasses = $this->classes_m->get_single_classes(array('schoolID' => $schoolID, 'classesID' => $classesID));
							$studentID = $this->input->post('studentID');
							$getstudent = $this->student_m->get_single_student(array('schoolID' => $schoolID, 'studentID' => $studentID));
							$getparent = $this->parentes_m->get_single_parentes(array('schoolID' => $schoolID, 'parentID' => $getstudent->parentID));
							$getschool = $this->school_m->get_school($schoolID);
							$amount = $this->input->post("amount");
							//$timesammp=DATE("dmyHis");
							$code  = 'CS';
							$code .= substr(number_format(time() * rand(),0,'',''),0,6);
							
							$installmentRef = 'CSI';
							$installmentRef .= substr((time() * rand(123, 999)),0,7);
							
							$installmentTable = array(
								'schoolID' => $schoolID,
								'installment_ref' => $installmentRef,
								'feecategory' => $this->input->post('feecategory'),
								'feetype' => $this->input->post('feetype'),
								'total_amount' => $this->input->post('amount'),
								'no_of_installments' => 2,
								'first_installment_amount' => $this->input->post('amount1'),
								'second_installment_amount' => $this->input->post('amount2')
							);
							
							$array = array(
								'schoolID' => $this->session->userdata('schoolID'),
								'classesID' => $classesID,
								'classes' => $getclasses->classes,
								'invoice_ref' => $code,
								'installment_ref' => $installmentRef,
								'studentID' => $studentID,
								'installment_plan' => 2,
								'student' => $getstudent->name,
								'roll' => $getstudent->roll,
								'feetype' => $this->input->post("feetype"),
								'amount' => $amount,
								'status' => 0,
								'date' => date("Y-m-d", strtotime($this->input->post("date"))),
								'year' => date('Y'),
								'create_date' => date("Y-m-d h:i:s"),
								'modify_date' => date("Y-m-d h:i:s"),
								'create_userID' => $this->session->userdata('loginuserID'),
								'create_username' => $this->session->userdata('username'),
								'create_usertype' => $this->session->userdata('usertype')

							);
							
							$amount = number_format(floatval($amount),2);
							$message = "An invoice for this term fees has been generated ";
							$message .="for your WARD ".$getstudent->name." with code ".$code.","; 
							$message .=" Amount:N".$amount.". Kindly Pay at http://www.cloudskul.com/payments. Thank You";
							$this->ozioma->set_message($message);//message from textfield
				            $this->ozioma->set_recipient($getparent->phone);//separate numbers with commas and include zip code in every number
				            $this->ozioma->set_sender($getschool->school);//sender from database
				            $this->ozioma->send();
							//var_dump($message);
							//break;
							$oldamount = $getstudent->totalamount;
							$nowamount = $oldamount+$amount;

							$this->student_m->update_student(array('totalamount' => $nowamount), $getstudent->studentID);
							$returnID = $this->invoice_m->insert_invoice($array);

							//insert into the installment table
							$this->db->insert('installments', $installmentTable);

							$this->session->set_flashdata('success', $this->lang->line('menu_success'));
						 	redirect(base_url("invoice/view/$returnID"));
							
							//else
						} else {
							
							$classesID = $this->input->post('classesID');
							$getclasses = $this->classes_m->get_single_classes(array('schoolID' => $schoolID, 'classesID' => $classesID));
							$getstudents = $this->student_m->get_order_by_student(array('schoolID' => $schoolID, "classesID" => $classesID));
							$amount = $this->input->post("amount");
							$code  = 'CS';
							$code .= substr(number_format(time() * rand(),0,'',''),0,6);
							
							$installmentRef = 'CSI';
							$installmentRef .= substr((time() * rand(123, 999)),0,7);
							
							$installmentTable = array(
								'schoolID' => $schoolID,
								'installment_ref' => $installmentRef,
								'feecategory' => $this->input->post('feecategory'),
								'feetype' => $this->input->post('feetype'),
								'total_amount' => $this->input->post('amount'),
								'no_of_installments' => 2,
								'first_installment_amount' => $this->input->post('amount1'),
								'second_installment_amount' => $this->input->post('amount2')
							);
							
							$getschool = $this->school_m->get_school($schoolID);
							foreach ($getstudents as $key => $getstudent) {
								$code .= substr(number_format(time() * rand(),0,'',''),0,6); //autogenerated code
								$getparent = $this->parentes_m->get_single_parentes(array('schoolID' => $schoolID, 'parentID' => $getstudent->parentID));
								
								$array = array(
									'schoolID' => $this->session->userdata('schoolID'),
									'classesID' => $classesID,
									'classes' => $getclasses->classes,
									'invoice_ref' => $code,
									'installment_ref' => $installmentRef,
									'studentID' => $getstudent->studentID,
									'installment_plan' => 2,
									'student' => $getstudent->name,
									'roll' => $getstudent->roll,
									'feetype' => $this->input->post("feetype"),
									'amount' => $amount,
									'status' => 0,
									'date' => date("Y-m-d", strtotime($this->input->post("date"))),
									'year' => date('Y'),
									'create_date' => date("Y-m-d h:i:s"),
									'modify_date' => date("Y-m-d h:i:s"),
									'create_userID' => $this->session->userdata('loginuserID'),
									'create_username' => $this->session->userdata('username'),
									'create_usertype' => $this->session->userdata('usertype')
								);
								$amount = number_format(floatval($amount),2);
								$message = "An invoice for this term fees has been generated ";
								$message .="for your WARD ".$getstudent->name." with code ".$code.","; 
								$message .=" Amount:N".$amount.". Kindly Pay at http://www.cloudskul.com/payments. Thank You";
								$this->ozioma->set_message($message);//message from textfield
					            $this->ozioma->set_recipient($getparent->phone);//separate numbers with commas and include zip code in every number
					            $this->ozioma->set_sender($getschool->school);//sender from database
					            $this->ozioma->send();
					            $amount = $this->input->post("amount");//re initialize variable
					            $code = 'CS';//re initialize variable

								$oldamount = $getstudent->totalamount;
								$nowamount = $oldamount+$amount;

								//var_dump($message);
								$this->student_m->update_student(array('totalamount' => $nowamount), $getstudent->studentID);
								$this->invoice_m->insert_invoice($array);
							}
							
							//insert into the installment table
							$this->db->insert('installments', $installmentTable);
							
							$this->session->set_flashdata('success', $this->lang->line('menu_success'));
						 	redirect(base_url("invoice/index"));
						}
					}
				} else {
					$this->data["subview"] = "invoice/add";
					$this->load->view('_layout_main', $this->data);
				}
			}

			elseif($installmentalPlan == '3'){

				$this->data['feecat'] = $this->feetype_m->feeCategory();
				$this->data['classes'] = $this->classes_m->get_order_by_classes(array('schoolID' => $schoolID));
				$this->data['feetypes'] = $this->feetype_m->get_order_by_feetype(array('schoolID' => $schoolID));
				$classesID = $this->input->post("classesID");
				if($classesID != 0) {
					$this->data['students'] = $this->student_m->get_order_by_student(array('schoolID' => $schoolID, "classesID" => $classesID));
				} else {
					$this->data['students'] = "empty";
				}
				$this->data['studentID'] = 0;
				if($_POST) {

					$this->data['studentID'] = $this->input->post('studentID');

					$rules = $this->rules();
					$this->form_validation->set_rules($rules);

					if ($this->form_validation->run() == FALSE) {

						$this->data["subview"] = "invoice/add";
						$this->load->view('_layout_main', $this->data);

					}else{
						
						//check if the total amount = installments amounts
						$installmentTotal = $this->input->post('amount1') + $this->input->post('amount2') + $this->input->post('amount3');
						if($this->input->post('amount') != $installmentTotal){
							$this->session->set_flashdata('error', "Total Amount is not equal to the sum of the installments!");
							redirect(base_url("invoice/add"));
						}

						if($this->input->post('studentID')) {

							$classesID = $this->input->post('classesID');
							$getclasses = $this->classes_m->get_single_classes(array('schoolID' => $schoolID, 'classesID' => $classesID));
							$studentID = $this->input->post('studentID');
							$getstudent = $this->student_m->get_single_student(array('schoolID' => $schoolID, 'studentID' => $studentID));
							$getparent = $this->parentes_m->get_single_parentes(array('schoolID' => $schoolID, 'parentID' => $getstudent->parentID));
							$getschool = $this->school_m->get_school($schoolID);
							$amount = $this->input->post("amount");
							//$timesammp=DATE("dmyHis");
							$code  = 'CS';
							$code .= substr(number_format(time() * rand(),0,'',''),0,6);
							
							$installmentRef = 'CSI';
							$installmentRef .= substr((time() * rand(123, 999)),0,7);
							
							$installmentTable = array(
								'schoolID' => $schoolID,
								'installment_ref' => $installmentRef,
								'feecategory' => $this->input->post('feecategory'),
								'feetype' => $this->input->post('feetype'),
								'total_amount' => $this->input->post('amount'),
								'no_of_installments' => 3,
								'first_installment_amount' => $this->input->post('amount1'),
								'second_installment_amount' => $this->input->post('amount2'),
								'third_installment_amount' => $this->input->post('amount3')
							);
							
							$array = array(
								'schoolID' => $this->session->userdata('schoolID'),
								'classesID' => $classesID,
								'classes' => $getclasses->classes,
								'invoice_ref' => $code,
								'installment_ref' => $installmentRef,
								'studentID' => $studentID,
								'installment_plan' => 3,
								'student' => $getstudent->name,
								'roll' => $getstudent->roll,
								'feetype' => $this->input->post("feetype"),
								'amount' => $amount,
								'status' => 0,
								'date' => date("Y-m-d", strtotime($this->input->post("date"))),
								'year' => date('Y'),
								'create_date' => date("Y-m-d h:i:s"),
								'modify_date' => date("Y-m-d h:i:s"),
								'create_userID' => $this->session->userdata('loginuserID'),
								'create_username' => $this->session->userdata('username'),
								'create_usertype' => $this->session->userdata('usertype')

							);
							
							$amount = number_format(floatval($amount),2);
							$message = "An invoice for this term fees has been generated ";
							$message .="for your WARD ".$getstudent->name." with code ".$code.","; 
							$message .=" Amount:N".$amount.". Kindly Pay at http://www.cloudskul.com/payments. Thank You";
							$this->ozioma->set_message($message);//message from textfield
				            $this->ozioma->set_recipient($getparent->phone);//separate numbers with commas and include zip code in every number
				            $this->ozioma->set_sender($getschool->school);//sender from database
				            $this->ozioma->send();
							//var_dump($message);
							//break;
							$oldamount = $getstudent->totalamount;
							$nowamount = $oldamount+$amount;

							$this->student_m->update_student(array('totalamount' => $nowamount), $getstudent->studentID);
							$returnID = $this->invoice_m->insert_invoice($array);

							//insert into the installment table
							$this->db->insert('installments', $installmentTable);

							$this->session->set_flashdata('success', $this->lang->line('menu_success'));
						 	redirect(base_url("invoice/view/$returnID"));
							
							//else
						} else {
							
							$classesID = $this->input->post('classesID');
							$getclasses = $this->classes_m->get_single_classes(array('schoolID' => $schoolID, 'classesID' => $classesID));
							$getstudents = $this->student_m->get_order_by_student(array('schoolID' => $schoolID, "classesID" => $classesID));
							$amount = $this->input->post("amount");
							$code  = 'CS';
							$code .= substr(number_format(time() * rand(),0,'',''),0,6);
							
							$installmentRef = 'CSI';
							$installmentRef .= substr((time() * rand(123, 999)),0,7);
							
							$installmentTable = array(
								'schoolID' => $schoolID,
								'installment_ref' => $installmentRef,
								'feecategory' => $this->input->post('feecategory'),
								'feetype' => $this->input->post('feetype'),
								'total_amount' => $this->input->post('amount'),
								'no_of_installments' => 3,
								'first_installment_amount' => $this->input->post('amount1'),
								'second_installment_amount' => $this->input->post('amount2'),
								'third_installment_amount' => $this->input->post('amount3')
							);
							
							$getschool = $this->school_m->get_school($schoolID);
							foreach ($getstudents as $key => $getstudent) {
								$code .= substr(number_format(time() * rand(),0,'',''),0,6); //autogenerated code
								$getparent = $this->parentes_m->get_single_parentes(array('schoolID' => $schoolID, 'parentID' => $getstudent->parentID));
								
								
								
								
								$array = array(
									'schoolID' => $this->session->userdata('schoolID'),
									'classesID' => $classesID,
									'classes' => $getclasses->classes,
									'invoice_ref' => $code,
									'installment_ref' => $installmentRef,
									'studentID' => $getstudent->studentID,
									'installment_plan' => 3,
									'student' => $getstudent->name,
									'roll' => $getstudent->roll,
									'feetype' => $this->input->post("feetype"),
									'amount' => $amount,
									'status' => 0,
									'date' => date("Y-m-d", strtotime($this->input->post("date"))),
									'year' => date('Y'),
									'create_date' => date("Y-m-d h:i:s"),
									'modify_date' => date("Y-m-d h:i:s"),
									'create_userID' => $this->session->userdata('loginuserID'),
									'create_username' => $this->session->userdata('username'),
									'create_usertype' => $this->session->userdata('usertype')
								);
								$amount = number_format(floatval($amount),2);
								$message = "An invoice for this term fees has been generated ";
								$message .="for your WARD ".$getstudent->name." with code ".$code.","; 
								$message .=" Amount:N".$amount.". Kindly Pay at http://www.cloudskul.com/payments. Thank You";
								$this->ozioma->set_message($message);//message from textfield
					            $this->ozioma->set_recipient($getparent->phone);//separate numbers with commas and include zip code in every number
					            $this->ozioma->set_sender($getschool->school);//sender from database
					            $this->ozioma->send();
					            $amount = $this->input->post("amount");//re initialize variable
					            $code = 'CS';//re initialize variable

								$oldamount = $getstudent->totalamount;
								$nowamount = $oldamount+$amount;

								//var_dump($message);
								$this->student_m->update_student(array('totalamount' => $nowamount), $getstudent->studentID);
								$this->invoice_m->insert_invoice($array);
								
								
							}
							
							//insert into the installment table
							$this->db->insert('installments', $installmentTable);
							
							$this->session->set_flashdata('success', $this->lang->line('menu_success'));
						 	redirect(base_url("invoice/index"));
						}
					}
				} else {
					$this->data["subview"] = "invoice/add";
					$this->load->view('_layout_main', $this->data);
				}
			}else{
				$this->data["subview"] = "invoice/add";
				$this->load->view('_layout_main', $this->data);
			}


		} else {
			$this->data["subview"] = "error";
			$this->load->view('_layout_main', $this->data);
		}
	}

	public function getFeeType(){
		$usertype = $this->session->userdata("usertype");
		
		if($usertype == "Admin" || $usertype == "Super Admin" || $usertype == "Accountant") {

			$category =  $_POST['category'];

			$this->data['feetypes'] = $this->invoice_m->getFeeTypesByCategory($category);
			$this->load->view('invoice/feetypes', $this->data);
		}
	}
	

	public function edit() {
		$usertype = $this->session->userdata("usertype");
		$schoolID = $this->session->userdata('schoolID');
		if($usertype == "Admin" || $usertype == "Super Admin" || $usertype == "Accountant") {
			$id = htmlentities($this->db->escape_str($this->uri->segment(3)));
			if((int)$id) {
				$this->data['invoice'] = $this->invoice_m->get_single_invoice(array('schoolID' => $schoolID, 'invoiceID' => $id));
				$this->data['classes'] = $this->classes_m->get_order_by_classes(array('schoolID' => $schoolID));

				if($this->data['invoice']) {

					if($this->data['invoice']->classesID != 0) {
						$this->data['students'] = $this->student_m->get_order_by_student(array('schoolID' => $schoolID, "classesID" => $this->data['invoice']->classesID));
					} else {
						$this->data['students'] = "empty";
					}
					$this->data['studentID'] = $this->data['invoice']->studentID;

					if($_POST) {
						$this->data['studentID'] = $this->input->post('studentID');
						$rules = $this->rules();
						$this->form_validation->set_rules($rules);
						if ($this->form_validation->run() == FALSE) {
							$this->data["subview"] = "invoice/edit";
							$this->load->view('_layout_main', $this->data);			
						} else {
							$status = 0;
							$oldstudent = $this->student_m->get_single_student(array('schoolID' => $schoolID, 'studentID' => $this->data['invoice']->studentID));
							$osoldamount = $oldstudent->totalamount;
							$oldnowamount = ($osoldamount)-($this->data['invoice']->amount);
							$this->student_m->update_student(array('totalamount' => $oldnowamount), $oldstudent->studentID);
							$classesID = $this->input->post('classesID');
							$getclasses = $this->classes_m->get_single_classes(array('schoolID' => $schoolID, 'classesID' => $classesID));
							$studentID = $this->input->post('studentID');
							$getstudent = $this->student_m->get_single_student(array('schoolID' => $schoolID, 'studentID' => $studentID));
							$amount = $this->input->post("amount");

							if(empty($this->data['invoice']->paidamount)) {
								$status = 0;
							} elseif($this->data['invoice']->paidamount == $amount) { 
								$status = 2;
							} else {
								$status = 1;
							}

							$array = array(
								'classesID' => $classesID,
								'classes' => $getclasses->classes,
								'studentID' => $studentID,
								'student' => $getstudent->name,
								'roll' => $getstudent->roll,
								'feetype' => $this->input->post("feetype"),
								'amount' => $amount,
								'status' => $status,
								'modify_date' => date("Y-m-d h:i:s")
							);
							$oldamount = $getstudent->totalamount;
							$nowamount = $oldamount+$amount;

							$this->student_m->update_student(array('totalamount' => $nowamount), $getstudent->studentID);
							$this->invoice_m->update_invoice($array, $id);
							$this->session->set_flashdata('success', $this->lang->line('menu_success'));
						 	redirect(base_url("invoice/index"));

						}
					} else {
						$this->data["subview"] = "invoice/edit";
						$this->load->view('_layout_main', $this->data);
					}				
				} else {
					$this->data["subview"] = "error";
					$this->load->view('_layout_main', $this->data);
				}
			} else {
				$this->data["subview"] = "error";
				$this->load->view('_layout_main', $this->data);
			}
		} else {
			$this->data["subview"] = "error";
			$this->load->view('_layout_main', $this->data);
		}
	}

	public function delete() {
		$usertype = $this->session->userdata("usertype");
		$schoolID = $this->session->userdata('schoolID');
		if($usertype == "Admin" || $usertype == "Super Admin" || $usertype == "Accountant") {
			$id = htmlentities($this->db->escape_str($this->uri->segment(3)));
			if((int)$id) {
				$this->data['invoice'] = $this->invoice_m->get_single_invoice(array('schoolID' => $schoolID, 'invoiceID' => $id));
				if($this->data['invoice']) {
					$oldstudent = $this->student_m->get_single_student(array('schoolID' => $schoolID, 'studentID' => $this->data['invoice']->studentID));
					$osoldamount = $oldstudent->totalamount;
					$oldnowamount = ($osoldamount)-($this->data['invoice']->amount);
					$this->student_m->update_student(array('totalamount' => $oldnowamount), $oldstudent->studentID);
					$this->invoice_m->delete_invoice($id);
					$this->session->set_flashdata('success', $this->lang->line('menu_success'));
					redirect(base_url('invoice/index'));
				} else {
					redirect(base_url('invoice/index'));
				}
			} else {
				$this->data["subview"] = "error";
				$this->load->view('_layout_main', $this->data);
			}
		} else {
			$this->data["subview"] = "error";
			$this->load->view('_layout_main', $this->data);
		}
	}

	public function view() {
		$usertype = $this->session->userdata("usertype");
		$schoolID = $this->session->userdata('schoolID');
		if($usertype == "Admin" || $usertype == "Super Admin" || $usertype == "Accountant") {
			$id = htmlentities($this->db->escape_str($this->uri->segment(3)));
			if((int)$id) {
				$this->data["invoice"] = $this->invoice_m->get_single_invoice(array('schoolID' => $schoolID, 'invoiceID' => $id));
				if($this->data["invoice"]) {
					$this->data["student"] = $this->student_m->get_single_student(array('schoolID' => $schoolID, 'studentID' =>  $this->data["invoice"]->studentID));
					$this->data["subview"] = "invoice/view";
					$this->load->view('_layout_main', $this->data);
				} else {
					$this->data["subview"] = "error";
					$this->load->view('_layout_main', $this->data);
				}
			} else {
				$this->data["subview"] = "error";
				$this->load->view('_layout_main', $this->data);
			}
		} elseif($usertype == "Student") {
			$id = htmlentities($this->db->escape_str($this->uri->segment(3)));
			if((int)$id) {
				$username = $this->session->userdata("username");
				$getstudent = $this->student_m->get_single_student(array('schoolID' => $schoolID, "username" => $username));
				$this->data["invoice"] = $this->invoice_m->get_single_invoice(array('schoolID' => $schoolID, 'invoiceID' => $id));
				if($this->data['invoice'] && ($this->data['invoice']->studentID == $getstudent->studentID)) {
					$this->data["student"] = $this->student_m->get_single_student(array('schoolID' => $schoolID, 'studentID' => $this->data["invoice"]->studentID));
					$this->data["subview"] = "invoice/view";
					$this->load->view('_layout_main', $this->data);
				} else {
					$this->data["subview"] = "error";
					$this->load->view('_layout_main', $this->data);
				}
			} else {
				$this->data["subview"] = "error";
				$this->load->view('_layout_main', $this->data);
			}
		} elseif($usertype == "Parent") {
			$id = htmlentities($this->db->escape_str($this->uri->segment(3)));
			$installmentStatusID = htmlentities($this->db->escape_str($this->uri->segment(4)));
			if((int)$id) {
				$username = $this->session->userdata("username");
				$parent = $this->student_m->get_parent_info($username);
				$this->data["invoice"] = $this->invoice_m->get_single_invoice(array('schoolID' => $schoolID, 'invoiceID' => $id));
				$this->data["invoiceData"] = $this->invoice_m->getInstallmentTableData($installmentStatusID);
				if($this->data['invoice']) {
					$getstudent = $this->student_m->get_single_student(array('schoolID' => $schoolID, "studentID" => $this->data['invoice']->studentID));
					if($this->data['invoice'] && ($parent->parentID == $getstudent->parentID)) {	
						$this->data["student"] = $this->student_m->get_single_student(array('schoolID' => $schoolID, 'studentID' => $this->data["invoice"]->studentID));
						$this->data["subview"] = "invoice/view";
						$this->load->view('_layout_main', $this->data);
					} else {
						$this->data["subview"] = "error";
						$this->load->view('_layout_main', $this->data);
					}
				} else {
					$this->data["subview"] = "error";
					$this->load->view('_layout_main', $this->data);
				}
			} else {
				$this->data["subview"] = "error";
				$this->load->view('_layout_main', $this->data);
			}
		} else {
			$this->data["subview"] = "error";
			$this->load->view('_layout_main', $this->data);
		}
	}

	public function print_preview() {
		$usertype = $this->session->userdata("usertype");
		$schoolID = $this->session->userdata('schoolID');
		if($usertype == "Admin" || $usertype == "Super Admin" || $usertype == "Accountant") {
			$id = htmlentities($this->db->escape_str($this->uri->segment(3)));
			if((int)$id) {
				$this->data["invoice"] = $this->invoice_m->get_single_invoice(array('schoolID' => $schoolID, 'invoiceID' => $id));
				if($this->data["invoice"]) {
					$this->data["student"] = $this->student_m->get_single_student(array('schoolID' => $schoolID, 'studentID' => $this->data["invoice"]->studentID));
					
					$this->load->library('html2pdf');
				    $this->html2pdf->folder('./assets/pdfs/');
				    $this->html2pdf->filename('Report.pdf');
				    $this->html2pdf->paper('a4', 'portrait');
				    $this->data['panel_title'] = $this->lang->line('panel_title');

				    $html = $this->load->view('invoice/print_preview', $this->data, true);
					$this->html2pdf->html($html);
					$this->html2pdf->create();

				} else {
					$this->data["subview"] = "error";
					$this->load->view('_layout_main', $this->data);
				}
			} else {
				$this->data["subview"] = "error";
				$this->load->view('_layout_main', $this->data);
			}
		} 
	}

	public function send_mail() {
		$usertype = $this->session->userdata("usertype");
		$schoolID = $this->session->userdata('schoolID');
		if($usertype == "Admin" || $usertype == "Accountant" || $usertype == "Super Admin") {
			$id = $this->input->post('id');
			
			if ((int)$id) {
				$this->data["invoice"] = $this->invoice_m->get_single_invoice(array('schoolID' => $schoolID, 'invoiceID' => $id));
				if($this->data["invoice"]) {
					$this->data["student"] = $this->student_m->get_single_student(array('schoolID' => $schoolID, 'studentID' => $this->data["invoice"]->studentID));
					
					$this->load->library('html2pdf');
				    $this->html2pdf->folder('./assets/pdfs/');
				    $this->html2pdf->filename('Report.pdf');
				    $this->html2pdf->paper('a4', 'portrait');
				    $this->data['panel_title'] = $this->lang->line('panel_title');
				    $html = $this->load->view('invoice/print_preview', $this->data, true);
					$this->html2pdf->html($html);
					$this->html2pdf->create('save');
					
					if($path = $this->html2pdf->create('save')) {
					$this->load->library('email');
					$this->email->set_mailtype("html");
					$this->email->from($this->data["siteinfos"]->email, $this->session->userdata('school'));
					$this->email->to($this->input->post('to'));
					$this->email->subject($this->input->post('subject'));
					$this->email->message($this->input->post('message'));	
					$this->email->attach($path);
						if($this->email->send()) {
							$this->session->set_flashdata('success', $this->lang->line('mail_success'));
						} else {
							$this->session->set_flashdata('error', $this->lang->line('mail_error'));
						}
					}

				} else {
					$this->data["subview"] = "error";
					$this->load->view('_layout_main', $this->data);
				}
			} else {
				$this->data["subview"] = "error";
				$this->load->view('_layout_main', $this->data);
			}
		} else {
			$this->data["subview"] = "error";
			$this->load->view('_layout_main', $this->data);
		}
	}

	public function payment() {
		$usertype = $this->session->userdata("usertype");
		$schoolID = $this->session->userdata('schoolID');
		if($usertype == "Admin" || $usertype == "Super Admin" || $usertype == "Accountant") {
			$id = htmlentities($this->db->escape_str($this->uri->segment(3)));
			if((int)$id) {
				$this->data['invoice'] = $this->invoice_m->get_single_invoice(array('schoolID' => $schoolID, 'invoiceID' => $id));
				//var_dump($this->data['invoice']);
				if($this->data['invoice']) {
					if(($this->data['invoice']->paidamount != $this->data['invoice']->amount) && ($this->data['invoice']->status == 0 || $this->data['invoice']->status == 1 || $this->data['invoice']->status == 3)) {
						if($_POST) {
							$rules = $this->payment_rules();
							$this->form_validation->set_rules($rules);
							if ($this->form_validation->run() == FALSE) {
								$this->data["subview"] = "invoice/view";
								$this->load->view('_layout_main', $this->data);			
							} else {

								$payable_amount = $this->input->post('amount')+$this->data['invoice']->paidamount;//calculate payable ammount
								if ($payable_amount > $this->data['invoice']->amount+$this->input->post('service_charge')) {
									$this->session->set_flashdata('error', 'Payment amount is much than invoice amount');
									redirect(base_url("invoice/view/$id"));
								} else {							
									$this->post_data = $this->input->post();	
									if ($this->input->post('payment_method') == 'Paypal') {
										$this->post_data['id'] = $this->uri->segment(3);
										$this->invoice_data = $this->invoice_m->get_single_invoice(array('schoolID' => $schoolID, 'invoiceID' => $this->post_data['id']));
										$this->Paypal();
									} elseif ($this->input->post('payment_method') == 'Remita') {
										//var_dump($_POST);
										$this->post_data['id'] = $this->uri->segment(3);
										$this->invoice_data = $this->invoice_m->get_single_invoice(array('schoolID' => $schoolID, 'invoiceID' => $this->post_data['id']));
										$this->Remita();
									} elseif($this->input->post('payment_method') == 'Cash') {
										$status = 0;
										if($payable_amount == $this->data['invoice']->amount) {
											$status = 2;
										} else {
											$status = 1;
										}

										$username = $this->session->userdata('username');
										$dbuserID = $this->session->userdata('loginuserID');
										$dbusertype = $this->session->userdata('usertype');
										$dbuname = $this->session->userdata('name');

										$nowpaymenttype = '';
										if(empty($this->data['invoice']->paymenttype)) {
											$nowpaymenttype = 'Cash';
										} else {
											if($this->data['invoice']->paymenttype == 'Cash') {
												$nowpaymenttype = 'Cash';
											} else {
												$exp = explode(',', $this->data['invoice']->paymenttype);
												if(!in_array('Cash', $exp)) {
													$nowpaymenttype =  $this->data['invoice']->paymenttype.','.'Cash';
												} else {
													$nowpaymenttype =  $this->data['invoice']->paymenttype;
												}
											}
										}

										$array = array(
											"paidamount" => $payable_amount,
											"status" => $status,
											"paymenttype" => $nowpaymenttype,
											"paiddate" => date('Y-m-d'),
											"userID" => $dbuserID,
											"usertype" => $dbusertype,
											'uname' => $dbuname
										);

										$payment_array = array(
											"schoolID" => $this->session->userdata('schoolID'),
											"invoiceID" => $id,
											"studentID"	=> $this->data['invoice']->studentID,
											"paymentamount" => $this->input->post('amount'),
											"paymenttype" => $this->input->post('payment_method'),
											"paymentdate" => date('Y-m-d'),
											"paymentmonth" => date('M'),
											"paymentyear" => date('Y'),
											"create_date" => date("Y-m-d h:i:s"),
											"modify_date" => date("Y-m-d h:i:s"),
											"create_userID" => $this->session->userdata('loginuserID'),
											"create_username" => $this->session->userdata('username'),
											"create_usertype" => $this->session->userdata('usertype'),
										);

										$this->payment_m->insert_payment($payment_array);

										$studentID = $this->data['invoice']->studentID;
										$getstudent = $this->student_m->get_single_student(array('schoolID' => $schoolID, 'studentID' => $studentID));
										$nowamount = ($getstudent->paidamount)+($this->input->post('amount'));
										$this->student_m->update_student(array('paidamount' => $nowamount), $studentID);

										$this->invoice_m->update_invoice($array, $id);
										$this->session->set_flashdata('success', $this->lang->line('menu_success'));
										redirect(base_url("invoice/view/$id"));
									} elseif($this->input->post('payment_method') == 'Cheque') {
										$status = 0;
										if($payable_amount == $this->data['invoice']->amount) {
											$status = 2;
										} else {
											$status = 1;
										}

										$username = $this->session->userdata('username');
										$dbuserID = $this->session->userdata('loginuserID');
										$dbusertype = $this->session->userdata('usertype');
										$dbuname = $this->session->userdata('name');

										$nowpaymenttype = '';
										if(empty($this->data['invoice']->paymenttype)) {
											$nowpaymenttype = 'Cheque';
										} else {
											if($this->data['invoice']->paymenttype == 'Cheque') {
												$nowpaymenttype = 'Cheque';
											} else {
												$exp = explode(',', $this->data['invoice']->paymenttype);
												if(!in_array('Cheque', $exp)) {
													$nowpaymenttype =  $this->data['invoice']->paymenttype.','.'Cheque';
												} else {
													$nowpaymenttype =  $this->data['invoice']->paymenttype;
												}
											}
										}

										$array = array(
											"paidamount" => $payable_amount,
											"status" => $status,
											"paymenttype" => $nowpaymenttype,
											"paiddate" => date('Y-m-d'),
											"userID" => $dbuserID,
											"usertype" => $dbusertype,
											'uname' => $dbuname
										);

										$payment_array = array(
											"schoolID" => $this->session->userdata('schoolID'),
											"invoiceID" => $id,
											"studentID"	=> $this->data['invoice']->studentID,
											"paymentamount" => $this->input->post('amount'),
											"paymenttype" => $this->input->post('payment_method'),
											"paymentdate" => date('Y-m-d'),
											"paymentmonth" => date('M'),
											"paymentyear" => date('Y'),
											"create_date" => date("Y-m-d h:i:s"),
											"modify_date" => date("Y-m-d h:i:s"),
											"create_userID" => $this->session->userdata('loginuserID'),
											"create_username" => $this->session->userdata('username'),
											"create_usertype" => $this->session->userdata('usertype'),
										);

										$this->payment_m->insert_payment($payment_array);

										$studentID = $this->data['invoice']->studentID;
										$getstudent = $this->student_m->get_single_student(array('schoolID' => $schoolID, 'studentID' => $studentID));
										$nowamount = ($getstudent->paidamount)+($this->input->post('amount'));
										$this->student_m->update_student(array('paidamount' => $nowamount), $studentID);

										$this->invoice_m->update_invoice($array, $id);
										$this->session->set_flashdata('success', $this->lang->line('menu_success'));
										redirect(base_url("invoice/view/$id"));
									} else {
										$this->data["subview"] = "invoice/payment";
										$this->load->view('_layout_main', $this->data);
									}	
								}	
							}				
						} else {
							$this->data["subview"] = "invoice/payment";
							$this->load->view('_layout_main', $this->data);
						}
					} else {
						$this->data["subview"] = "error";
						$this->load->view('_layout_main', $this->data);
					}
				} else {
					$this->data["subview"] = "error";
					$this->load->view('_layout_main', $this->data);
				}
			} else {
				$this->data["subview"] = "error";
				$this->load->view('_layout_main', $this->data);
			}
		} elseif($usertype == "Student") {
			$id = htmlentities($this->db->escape_str($this->uri->segment(3)));
			if((int)$id) {
				$this->data["invoice"] = $this->invoice_m->get_single_invoice(array('schoolID' => $schoolID, 'invoiceID' => $id));
				$username = $this->session->userdata("username");
				$getstudent = $this->student_m->get_single_student(array('schoolID' => $schoolID, "username" => $username));

				if($this->data['invoice'] && ($this->data['invoice']->studentID == $getstudent->studentID)) {
					if(($this->data['invoice']->paidamount != $this->data['invoice']->amount) && ($this->data['invoice']->status == 0 || $this->data['invoice']->status == 1 || $this->data['invoice']->status == 3)) {
						if($_POST) {
							$rules = $this->payment_rules();
							unset($rules[1]);
							$this->form_validation->set_rules($rules);
							if ($this->form_validation->run() == FALSE) {
								$this->data["subview"] = "invoice/payment";
								$this->load->view('_layout_main', $this->data);			
							} else {
								$payable_amount = $this->input->post('amount')+$this->data['invoice']->paidamount;//calculate payable ammount
								if ($payable_amount > $this->data['invoice']->amount+$this->input->post('service_charge')) {
									$this->session->set_flashdata('error', 'Payment amount is much than invoice amount');
									redirect(base_url("invoice/view/$id"));
								} else {
									$this->post_data = $this->input->post();
									$this->post_data['id'] = $id;
									$this->invoice_data = $this->invoice_m->get_single_invoice(array('schoolID' => $schoolID, 'invoiceID' => $id));
									$this->Remita(); //remita function
								}
							}
						} else {
							$this->data["subview"] = "invoice/view";
							$this->load->view('_layout_main', $this->data);
						}
					} else {
						$this->data["subview"] = "error";
						$this->load->view('_layout_main', $this->data);
					}
				} else {
					$this->data["subview"] = "error";
					$this->load->view('_layout_main', $this->data);
				}
			} else {
				$this->data["subview"] = "error";
				$this->load->view('_layout_main', $this->data);
			}
		} elseif($usertype == "Parent") {
			$id = htmlentities($this->db->escape_str($this->uri->segment(3)));
			if((int)$id) {
				$this->data['invoice'] = $this->invoice_m->get_single_invoice(array('schoolID' => $schoolID, 'invoiceID' => $id));
				$username = $this->session->userdata("username");

				if($this->data["invoice"]) {
					$getstudent = $this->student_m->get_single_student(array('schoolID' => $schoolID, "studentID" => $this->data['invoice']->studentID));
					if($this->data['invoice'] && ($this->data['invoice']->studentID == $getstudent->studentID)) {
						if(($this->data['invoice']->paidamount != $this->data['invoice']->amount) && ($this->data['invoice']->status == 0 || $this->data['invoice']->status == 1 || $this->data['invoice']->status == 3)) {
							if($_POST) {
								$rules = $this->payment_rules();
								unset($rules[1]);
								$this->form_validation->set_rules($rules);
								if ($this->form_validation->run() == FALSE) {
									$this->data["subview"] = "invoice/payment";
									$this->load->view('_layout_main', $this->data);			
								} else {
									$payable_amount = $this->input->post('amount')+$this->data['invoice']->paidamount;//calculate payable ammount
									if ($payable_amount > $this->data['invoice']->amount+$this->input->post('service_charge')) {
									//$payable_amount = $this->input->post('amount')+$this->data['invoice']->paidamount;
									//if ($payable_amount > $this->data['invoice']->amount) {
										$this->session->set_flashdata('error', 'Payment amount is much than invoice amount');
										redirect(base_url("invoice/view/$id"));
									} else {
										$this->post_data = $this->input->post();
										$this->post_data['id'] = $id;
										$this->invoice_data = $this->invoice_m->get_single_invoice(array('schoolID' => $schoolID, 'invoiceID' => $id));
										$this->Remita($id); //remita function
									}
								}
							} else {
								$this->data["subview"] = "invoice/view";
								$this->load->view('_layout_main', $this->data);
							}
						} else {
							$this->data["subview"] = "error";
							$this->load->view('_layout_main', $this->data);
						}
					} else {
						$this->data["subview"] = "error";
						$this->load->view('_layout_main', $this->data);
					}

				} else {
					$this->data["subview"] = "error";
					$this->load->view('_layout_main', $this->data);
				}
			} else {
				$this->data["subview"] = "error";
				$this->load->view('_layout_main', $this->data);
			}
		} else {
			$this->data["subview"] = "error";
			$this->load->view('_layout_main', $this->data);
		}
	}

	/* Paypal payment start*/
	public function Paypal() {
		$api_config = array();
		$get_configs = $this->payment_settings_m->get_order_by_config();
		foreach ($get_configs as $key => $get_key) {
			$api_config[$get_key->config_key] = $get_key->value;
		}
		$this->data['set_key'] = $api_config;
		if($api_config['paypal_api_username'] =="" || $api_config['paypal_api_password'] =="" || $api_config['paypal_api_signature']==""){
			$this->session->set_flashdata('error', 'Paypal settings not available');
			redirect($_SERVER['HTTP_REFERER']);
		} else {
			$this->item_data = $this->post_data;
			$this->invoice_info = (array) $this->invoice_data;

			$params = array(
	  		'cancelUrl' 	=> base_url('invoice/getSuccessPayment'),
	  		'returnUrl' 	=> base_url('invoice/getSuccessPayment'),
	  		'invoice_id'	=> $this->item_data['id'],
	    	'name'		=> $this->invoice_info['student'],
	    	'description' 	=> $this->invoice_info['feetype'], 
	    	'amount' 	=> number_format(floatval($this->item_data['amount']),2),
	    	'currency' 	=> $this->data["setschool"]->currency_code,
			);
			$this->session->set_userdata("params", $params);
			$gateway = Omnipay::create('PayPal_Express');
			$gateway->setUsername($api_config['paypal_api_username']);
			$gateway->setPassword($api_config['paypal_api_password']);
			$gateway->setSignature($api_config['paypal_api_signature']);

			$gateway->setTestMode($api_config['paypal_demo']);

			$response = $gateway->purchase($params)->send();

			if ($response->isSuccessful()) {
				// payment was successful: update database 
			} elseif ($response->isRedirect()) {
				$response->redirect();
			} else {
			  // payment failed: display message to customer
			  echo $response->getMessage();
			}
		}
		/*omnipay Paypal end*/
	}

	/* Remita payment start*/
	public function Remita($installmentStatusID) {

		$schoolID = $this->session->userdata('schoolID');
		//$this->load->library('etranzact'); //load the etranzact library  $this->input->post('message')
		$this->invoice_info = (array) $this->invoice_data;
		$api_config = array();
		$get_configs = $this->payment_settings_m->get_order_by_config();
		foreach ($get_configs as $key => $get_key) {
			$api_config[$get_key->config_key] = $get_key->value;
		}
		$this->data['set_key'] = $api_config;
		if($api_config['service_type_id'] =="" || $api_config['merchant_id'] =="" || $api_config['api_key']==""){
			$this->session->set_flashdata('error', 'Remita settings not available');
			redirect($_SERVER['HTTP_REFERER']);
		} else {
			$this->item_data = $this->post_data;
			$this->invoice_info = (array) $this->invoice_data;
			//var_dump($api_config);
			$params = array(
	  		'cancelUrl' 	=> base_url('invoice/getRemitaSuccess'),
	  		'returnUrl' 	=> base_url('invoice/getRemitaSuccess'),
	  		'invoice_id'	=> $this->item_data['id'],
	    	'name'		=> $this->invoice_info['student'],
	    	'invoice_ref'		=> $this->invoice_info['invoice_ref'],
	    	'description' 	=> $this->invoice_info['feetype'], 
	    	'amount' 	=> intval($this->item_data['fees']),
	    	'currency' 	=> $this->data["setschool"]->currency_code,
			);
			
			//get the payment details by the installmentStatusID
			$paymentDetails = $this->invoice_m->getInstallmentTableData($installmentStatusID);

			$type = $this->post_data['payment_type'];
			//var_dump($params);
			//var_dump($this->item_data);
			$this->session->set_userdata("params", $params);

	        $this->remita->set_responseurl(base_url('invoice/getRemitaSuccess'));//set return url
	        $this->remita->set_payer_name($this->invoice_info['student']);
	        $checkstudent = $this->student_m->get_single_student(array('schoolID' => $schoolID, 'studentID' => $this->invoice_info['studentID']));
			$parent = $this->parentes_m->get_single_parentes(array('schoolID' => $schoolID, 'parentID' => $checkstudent->parentID));
	        $this->remita->set_payer_phone($parent->phone);
	        $this->remita->set_payer_email($parent->email);
	        $this->remita->set_total_amount($this->post_data['amount']);//set total amount
	        $this->remita->set_cloudskul_fee($this->post_data['service_charge']);//set service charge
	        $account_cloudskul = $this->payment_settings_m->get_account_cloudskul();
	        $account_school = $this->payment_settings_m->get_account_schools();        
	        $this->remita->set_beneficiary_cloud($account_cloudskul);
	        $this->remita->set_beneficiary($account_school);
	        $this->remita->set_payment_type($this->post_data['payment_type']);//set payment type
	        $response = $this->remita->remita_connect($this->remita->get_content());//check remita service status
	        //var_dump($this->remita->get_content());
	        $jsonData = substr($response, 6, -1);
			//var_dump($jsonData);
			$check = json_decode($jsonData, true);
			//var_dump($check);
			$status = $check['statuscode'];
			$rrr = $check['RRR'];
	        //var_dump($status);
	        //die;
	        if($status == "025" && $type != 'BANK_BRANCH'){
		        if($status == "025"){
		        	if($status != 404){
		        	$this->remita->execute_remita();
		        	}
		        }
		    	elseif($status != "025"){
		    		$this->session->set_flashdata('error', 'Error generating Payment reference, Service is down!');
		    		redirect(base_url("invoice/view/".$params['invoice_id']));
		    	}
		    }elseif($status == "025" && $type == 'BANK_BRANCH') {
		    	$params = $this->session->userdata('params');
		    	//array with contents
		    	$status = 3;
		    	$recent_paidamount = '';//$params['amount'];
		    	$nowpaymenttype = "Remita,Bank";

				$dbuserID = $this->session->userdata('loginuserID');
				$dbusertype = $this->session->userdata('usertype');
				$dbuname = $this->session->userdata('name');

		    	$array = array(
					"paidamount" => $recent_paidamount,
					"status" => $status,
					"paymenttype" => $nowpaymenttype,
					"paiddate" => date('Y-m-d'),
					"rrr" => $rrr,
					"userID" => $dbuserID,
					"usertype" => $dbusertype,
					'uname' => $dbuname
				);

		    	$this->invoice_m->update_invoice($array, $params['invoice_id']);
		    	$this->session->set_flashdata('success', 'RRR was generated. Receipt coming soon. ');
		    	redirect(base_url("invoice/view/".$params['invoice_id']));
		    }elseif($status != "025" && $type == 'BANK_BRANCH') {
		    	$this->session->set_flashdata('error', 'Error generating Payment reference, Service is down!');
		    	redirect(base_url("invoice/view/".$params['invoice_id']));
		    	
		    }
			/*$gateway = Omnipay::create('PayPal_Express');
			$gateway->setUsername($api_config['paypal_api_username']);
			$gateway->setPassword($api_config['paypal_api_password']);
			$gateway->setSignature($api_config['paypal_api_signature']);

			$gateway->setTestMode($api_config['paypal_demo']);

			$response = $gateway->purchase($params)->send();

			if ($response->isSuccessful()) {
				// payment was successful: update database 
			} elseif ($response->isRedirect()) {
				$response->redirect();
			} else {
			  // payment failed: display message to customer
			  echo $response->getMessage();
			}*/
		}
		
	}

	public function getRemitaSuccess(){
		$schoolID = $this->session->userdata('schoolID');
		$params = $this->session->userdata('params');
		$orderID  = $_GET['orderID'];
		$rrr  = $_GET['RRR'];

		$remitaResponse = $this->remita->remita_transaction_details($orderID);
		
		$this->data['invoice'] = $this->invoice_m->get_invoice($params['invoice_id']);

  		$recent_paidamount = $params['amount']+$this->data['invoice']->paidamount;
  		if(isset($orderID) && $remitaResponse['message'] === 'Approved' && $remitaResponse['status'] === '01' ) {
  			// Response
  			if ($orderID) {

				$status = 0;
				if($recent_paidamount == $this->data['invoice']->amount) {
					$status = 2;
				} else {
					$status = 1;
				}

				$usertype = $this->session->userdata("usertype");
				$username = $this->session->userdata('username');
				$dbuserID = $this->session->userdata('loginuserID');
				$dbusertype = $this->session->userdata('usertype');
				$dbuname = $this->session->userdata('name');

				$nowpaymenttype = '';
				if(empty($this->data['invoice']->paymenttype)) {
					$nowpaymenttype = 'Remita';
				} else {
					if($this->data['invoice']->paymenttype == 'Remita') {
						$nowpaymenttype = 'Remita';
					} else {
						$exp = explode(',', $this->data['invoice']->paymenttype);
						if(!in_array('Remita', $exp)) {
							$nowpaymenttype =  $this->data['invoice']->paymenttype.','.'Remita';
						} else {
							$nowpaymenttype =  $this->data['invoice']->paymenttype;
						}
					}
				}

				$array = array(
					"paidamount" => $recent_paidamount,
					"status" => $status,
					"paymenttype" => $nowpaymenttype,
					"paiddate" => date('Y-m-d'),
					"userID" => $dbuserID,
					"usertype" => $dbusertype,
					'uname' => $dbuname
				);

				$payment_array = array(
					"schoolID" => $this->session->userdata('schoolID'),
					"invoiceID" => $params['invoice_id'],
					"studentID"	=> $this->data['invoice']->studentID,
					"paymentamount" => $params['amount'],
					"paymenttype" => 'Remita',					
					"order_id" => $orderID,
					"rrr" => $rrr,
					"paymentdate" => date('Y-m-d'),
					"paymentmonth" => date('M'),
					"paymentyear" => date('Y'),
					"create_date" => date("Y-m-d h:i:s"),
					"modify_date" => date("Y-m-d h:i:s"),
					"create_userID" => $this->session->userdata('loginuserID'),
					"create_username" => $this->session->userdata('username'),
					"create_usertype" => $this->session->userdata('usertype'),
				);
				$this->payment_m->insert_payment($payment_array);

				$studentID = $this->data['invoice']->studentID;
				$getstudent = $this->student_m->get_single_student(array('schoolID' => $schoolID, 'studentID' => $studentID));
				$nowamount = ($getstudent->paidamount)+($params['amount']);
				$this->student_m->update_student(array('paidamount' => $nowamount), $studentID);

				$this->invoice_m->update_invoice($array, $params['invoice_id']);
				$this->session->set_flashdata('success', $this->lang->line('menu_success'));
  			} else {
  				$this->session->set_flashdata('error', 'Error generating Payment reference!');
  			}
  			redirect(base_url("invoice/view/".$params['invoice_id']));
		}else {
      			
      		//Failed transaction
      		$this->session->set_flashdata('error', 'Sorry, your payment was not successful!');
  			redirect(base_url("invoice/view/".$params['invoice_id']));
      			
  		}

	}
	
	public function getSuccessPayment() {	
  		$schoolID = $this->session->userdata('schoolID');
  		$api_config = array();
		$get_configs = $this->payment_settings_m->get_order_by_config();
		foreach ($get_configs as $key => $get_key) {
			$api_config[$get_key->config_key] = $get_key->value;
		}
		$this->data['set_key'] = $api_config;

   		$gateway = Omnipay::create('PayPal_Express');
		$gateway->setUsername($api_config['paypal_api_username']);
		$gateway->setPassword($api_config['paypal_api_password']);
		$gateway->setSignature($api_config['paypal_api_signature']);

		$gateway->setTestMode($api_config['paypal_demo']);
      	
		$params = $this->session->userdata('params');
  		$response = $gateway->completePurchase($params)->send();
  		$paypalResponse = $response->getData(); // this is the raw response object
  		$purchaseId = $_GET['PayerID'];
  		$this->data['invoice'] = $this->invoice_m->get_invoice($params['invoice_id']);
  		$recent_paidamount = $params['amount']+$this->data['invoice']->paidamount;
  		if(isset($paypalResponse['PAYMENTINFO_0_ACK']) && $paypalResponse['PAYMENTINFO_0_ACK'] === 'Success') {
  			// Response
  			if ($purchaseId) {

				$status = 0;
				if($recent_paidamount == $this->data['invoice']->amount) {
					$status = 2;
				} else {
					$status = 1;
				}

				$usertype = $this->session->userdata("usertype");
				$username = $this->session->userdata('username');
				$dbuserID = $this->session->userdata('loginuserID');
				$dbusertype = $this->session->userdata('usertype');
				$dbuname = $this->session->userdata('name');

				$nowpaymenttype = '';
				if(empty($this->data['invoice']->paymenttype)) {
					$nowpaymenttype = 'Paypal';
				} else {
					if($this->data['invoice']->paymenttype == 'Paypal') {
						$nowpaymenttype = 'Paypal';
					} else {
						$exp = explode(',', $this->data['invoice']->paymenttype);
						if(!in_array('Paypal', $exp)) {
							$nowpaymenttype =  $this->data['invoice']->paymenttype.','.'Paypal';
						} else {
							$nowpaymenttype =  $this->data['invoice']->paymenttype;
						}
					}
				}

				$array = array(
					"paidamount" => $recent_paidamount,
					"status" => $status,
					"paymenttype" => $nowpaymenttype,
					"paiddate" => date('Y-m-d'),
					"userID" => $dbuserID,
					"usertype" => $dbusertype,
					'uname' => $dbuname
				);

				$payment_array = array(
					"schoolID" => $this->session->userdata('schoolID'),
					"invoiceID" => $params['invoice_id'],
					"studentID"	=> $this->data['invoice']->studentID,
					"paymentamount" => $params['amount'],
					"paymenttype" => 'Paypal',
					"paymentdate" => date('Y-m-d'),
					"paymentmonth" => date('M'),
					"paymentyear" => date('Y'),
					"create_date" => date("Y-m-d h:i:s"),
					"modify_date" => date("Y-m-d h:i:s"),
					"create_userID" => $this->session->userdata('loginuserID'),
					"create_username" => $this->session->userdata('username'),
					"create_usertype" => $this->session->userdata('usertype'),
				);

				$this->payment_m->insert_payment($payment_array);

				$studentID = $this->data['invoice']->studentID;
				$getstudent = $this->student_m->get_single_student(array('schoolID' => $schoolID, 'studentID' => $studentID));
				$nowamount = ($getstudent->paidamount)+($params['amount']);
				$this->student_m->update_student(array('paidamount' => $nowamount), $studentID);

				$this->invoice_m->update_invoice($array, $params['invoice_id']);
				$this->session->set_flashdata('success', $this->lang->line('menu_success'));
  			} else {
  				$this->session->set_flashdata('error', 'Payer id not found!');
  			}
  			redirect(base_url("invoice/view/".$params['invoice_id']));
  		} else {
      			
      		//Failed transaction
      		$this->session->set_flashdata('error', 'Payment not success!');
  			redirect(base_url("invoice/view/".$params['invoice_id']));
      			
  		}
  	}
	/* Paypal payment end*/

	/*Batched Response start*/
	public function batched_response(){

		$raw_post_data = file_get_contents('php://input');
		$response_arr = json_decode(stripcslashes($raw_post_data), TRUE); //use json_decode when the post body is in json format

		if($response_arr !== NULL){
			
		     foreach($response_arr as $value)
		     {
				if(!empty($value['rrr'])){

					/*$str  = "insert into eduportal_remita_payment(payment_id, rrr, channel, amount, payer_name, payer_email, payer_phone, unique_id, response_code, trans_date,"; 
					$str .=	"debit_date, bank, branch, service_type, date_sent, date_requested, order_ref)"; 
					$str .=	"values ('".$value['id']."', '".$value['rrr']."', '".$value['channnel']."', '".$value['amount']."',";
					$str .=	" '".$value['payerName']."', '".$value['payerEmail']."', '".$value['payerPhoneNumber']."','".$value['uniqueIdentifier']."',";
					$str .= " '".$value['responseCode']."', '".$value['transactiondate']."', '".$value['debitdate']."', '".$value['bank']."',";
					$str .= " '".$value['branch']."', '".$value['serviceTypeId']."', '".$value['dateSent']."', '".$value['dateRequested']."', '".$value['orderRef']."') ";
					
					echo $str;
					return;*/
					
				//check if rrr already exists
					$invoice = $this->invoice_m->get_single_invoice(array('rrr'=>$value['rrr']));
					if(count($invoice) > 0){
						echo "Invoice found, proceeding to confirm  payment for RRR: ".$value['rrr']."! \n";
						$insertedRecord = $this->payment_m->get_single_payment(array('rrr'=>$value['rrr']));
						if(count($insertedRecord) > 0){
							echo "Payment already confirmed for RRR: ".$value['rrr']."! \n";
						}else{
							
							$bad_keys = array('invoiceextra1','invoiceextra2','create_date','modify_date','create_userID','create_username','create_usertype','date','paiddate','year','feetype','paymenttype');
							$out = array_diff_key(json_decode(json_encode($invoice),true),array_flip($bad_keys));

							$studentID = $out['studentID'];
							$schoolID = $out['schoolID'];
							$getstudent = $this->student_m->get_single_student(array('schoolID' => $schoolID, 'studentID' => $studentID));
							$getparent = $this->parentes_m->get_single_parentes(array('schoolID' => $schoolID, 'parentID' => $getstudent->parentID));

							//$values = json_encode($out); //$values = json_decode($values);
							$diff = $value['amount'] - $out['amount'];//taking out the commision
							//echo $diff."\n";
							$status = 0;
							if($value['amount'] > $out['amount']) {
								$status = 2;
							} else {
								$status = 1;
							}

							$array = array(
								"paidamount" => intval($value['amount']) - $diff,
								"status" => $status,
								"paymenttype" => 'Remita,Bank',
								"paiddate" => date('Y-m-d'),
								"userID" => $getparent->parentID,
								"usertype" => 'Parent',
								'uname' => $getparent->username
							);

							$payment_array = array(
								"schoolID" => $out['schoolID'],
								"invoiceID" => $out['invoiceID'],
								"studentID"	=> $out['studentID'],
								"paymentamount" => $out['amount'],
								"paymenttype" => 'Remita,Bank',					
								"order_id" => $value['order_ref'],
								"rrr" => $out['rrr'],
								"paymentdate" => date('Y-m-d'),
								"paymentmonth" => date('M'),
								"paymentyear" => date('Y'),
								"create_date" => date("Y-m-d h:i:s"),
								"modify_date" => date("Y-m-d h:i:s"),
								"create_userID" => 4,
								"create_username" => 'etti',
								"create_usertype" => 'Super Admin',
							);

							$this->payment_m->insert_payment($payment_array); //insert into payment table
							
							$nowamount = ($getstudent->paidamount)+($out['amount']);
							$this->student_m->update_student(array('paidamount' => $nowamount), $studentID);//update total amount of money student has paid

							$this->invoice_m->update_invoice($array, $out['invoiceID']);//update invoice

							echo "Payment was made successfully for RRR: ".$value['rrr']."! \n";
						}
					}else{
						echo "Invoice not found for RRR: ".$value['rrr']."! \n";
					}
					/*die;
					
					if(mysqli_num_rows($insertedRecord) > 0){
						echo "Payment already confirmed for RRR: ".$value['rrr']."! \n";
					}else{

						$updateRow = $mysqli->query("update eduportal_remita_temp_data set status = 'Payment Confirmed' where rrr = '".$value['rrr']."'");
						
						//if(mysqli_num_rows($updateRow) > 0){
							$str  = "insert into eduportal_remita_payment(payment_id, rrr, channel, amount, payer_name, payer_email, payer_phone, unique_id, response_code, trans_date,"; 
							$str .=	"debit_date, bank, branch, service_type, date_sent, date_requested, order_ref)"; 
							$str .=	"values ('".$value['id']."', '".$value['rrr']."', '".$value['channnel']."', '".$value['amount']."',";
							$str .=	" '".$value['payerName']."', '".$value['payerEmail']."', '".$value['payerPhoneNumber']."','".$value['uniqueIdentifier']."',";
							$str .= " '".$value['responseCode']."', '".$value['transactiondate']."', '".$value['debitdate']."', '".$value['bank']."',";
							$str .= " '".$value['branch']."', '".$value['serviceTypeId']."', '".$value['dateSent']."', '".$value['dateRequested']."', '".$value['orderRef']."') ";
					
							$insertRow = $mysqli->query($str);
							
							if($insertRow){
								echo "Payment Confirmed for RRR: ".$value['rrr']."\n";
							}else{
								die('Error: ' . $mysqli->error);
							}
						//}else{
						//	die('Error: ' . $mysqli->error);
						//}
					}*/
				}else{
					echo "Invalid Data: The Data that was sent could not be processed because the RRR value is NULL";
				}
			}
		}else
			echo "No data has been received";

	}

	function call_all_student() {
		$classesID = $this->input->post('id');
		$schoolID = $this->session->userdata('schoolID');
		if((int)$classesID) {
			echo "<option value='". 0 ."'>". $this->lang->line('invoice_select_student') ."</option>";
			$students = $this->student_m->get_order_by_student(array('schoolID' => $schoolID, 'classesID' => $classesID));
			foreach ($students as $key => $student) {
				echo "<option value='". $student->studentID ."'>". $student->name ."</option>";
			}
		} else {
			echo "<option value='". 0 ."'>". $this->lang->line('invoice_select_student') ."</option>";
		}
	}

	function feetypecall() {
		$feetype = $this->input->post('feetype');
		if($feetype) {
			$allfeetypes = $this->feetype_m->allfeetype($feetype);
			
			foreach ($allfeetypes as $allfeetype) {
				echo "<li id='". $allfeetype->feetypeID ."'>".$allfeetype->feetype."</li>";
			}
		}
	}

	function date_valid($date) {
		if(strlen($date) <10) {
			$this->form_validation->set_message("date_valid", "%s is not valid dd-mm-yyyy");
	     	return FALSE;
		} else {
	   		$arr = explode("-", $date);   
	        $dd = $arr[0];            
	        $mm = $arr[1];              
	        $yyyy = $arr[2];
	      	if(checkdate($mm, $dd, $yyyy)) {
	      		return TRUE;
	      	} else {
	      		$this->form_validation->set_message("date_valid", "%s is not valid dd-mm-yyyy");
	     		return FALSE;
	      	}
	    } 
	}

	function unique_classID() {
		if($this->input->post('classesID') == 0) {
			$this->form_validation->set_message("unique_classID", "The %s field is required");
	     	return FALSE;
		}
		return TRUE;
	}

	function valid_number() {
		if($this->input->post('amount') && $this->input->post('amount') < 0) {
			$this->form_validation->set_message("valid_number", "%s is invalid number");
			return FALSE;
		}
		return TRUE;
	}

	function unique_paymentmethod() {
		if($this->input->post('payment_method') === '0') {
			$this->form_validation->set_message("unique_paymentmethod", "The %s field is required");
	     	return FALSE;
		} elseif($this->input->post('payment_method') === 'Paypal') {
			$api_config = array();
			$get_configs = $this->payment_settings_m->get_order_by_config();
			foreach ($get_configs as $key => $get_key) {
				$api_config[$get_key->config_key] = $get_key->value;
			}
			if($api_config['paypal_api_username'] =="" || $api_config['paypal_api_password'] =="" || $api_config['paypal_api_signature']==""){
				$this->form_validation->set_message("unique_paymentmethod", "Paypal settings required");
				return FALSE;
			}
		}
		return TRUE;
	} 

	public function student_list() {
		$studentID = $this->input->post('id');
		if((int)$studentID) {
			$string = base_url("invoice/index/$studentID");
			echo $string;
		} else {
			redirect(base_url("invoice/index"));
		}
	}

	// public function userID() {
	// 	$usertype = $this->session->userdata('usertype');
	// 	$username = $this->session->userdata('username');
	// 	if ($usertype=="Admin") {
	// 		$table = "setting";
	// 		$tableID = "settingID";
	// 	} elseif($usertype=="Accountant" || $usertype=="Librarian") {
	// 		$table = "user";
	// 		$tableID = "userID";
	// 	} else {
	// 		$table = strtolower($usertype);
	// 		$tableID = $table."ID";
	// 	}				
	// 	$query = $this->db->get_where($table, array('username' => $username));
	// 	$userID = $query->row()->$tableID;
	// 	return $userID;
	// }
}

/* End of file invoice.php */
/* Location: .//D/xampp/htdocs/school/mvc/controllers/invoice.php */