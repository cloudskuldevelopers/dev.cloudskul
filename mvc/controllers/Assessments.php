<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Assessments extends MY_Controller {
/*
| -----------------------------------------------------
| PRODUCT NAME: 	CloudSkul
| -----------------------------------------------------
| AUTHOR:			CLOUDSKUL TEAM
| -----------------------------------------------------
| EMAIL:			info@cloudskul.com
| -----------------------------------------------------
| COPYRIGHT:		RESERVED BY Cloudskul
| -----------------------------------------------------
| WEBSITE:			http://cloudskul.com
| -----------------------------------------------------
*/
	function __construct() {
		parent::__construct();
		$this->load->model("assessments_m");
		$language = $this->session->userdata('lang');
		$this->lang->load('assessments', $language);	
	}

	public function index() {
		$usertype = $this->session->userdata("usertype");
		$schoolID = $this->session->userdata('schoolID');
		if($usertype == "Admin" || $usertype == "Super Admin") {
			$this->data['assessments'] = $this->assessments_m->get_order_by_assessments(array('schoolID' => $schoolID));
			$this->data["subview"] = "assessments/index";
			$this->load->view('_layout_main', $this->data);
		} else {
			$this->data["subview"] = "error";
			$this->load->view('_layout_main', $this->data);
		}
	}

	protected function rules() {
		$rules = array(
			array(
				'field' => 'assessments', 
				'label' => $this->lang->line("assessments_name"), 
				'rules' => 'trim|required|xss_clean|max_length[60]|callback_unique_assessments'
			), 
			array(
				'field' => 'note', 
				'label' => $this->lang->line("assessments_note"), 
				'rules' => 'trim|max_length[200]|xss_clean'
			)
		);
		return $rules;
	}

	public function add() {
		$usertype = $this->session->userdata("usertype");
		if($usertype == "Admin" || $usertype == "Super Admin") {
			if($_POST) {
				$rules = $this->rules();
				$this->form_validation->set_rules($rules);
				if ($this->form_validation->run() == FALSE) {
					$this->data['form_validation'] = validation_errors(); 
					$this->data["subview"] = "assessments/add";
					$this->load->view('_layout_main', $this->data);			
				} else {
					$array = array(
						"schoolID" => $this->session->userdata('schoolID'),
						"assessments" => $this->input->post("assessments"),
						"note" => $this->input->post("note"),
						"create_date" => date("Y-m-d h:i:s"),
						"modify_date" => date("Y-m-d h:i:s"),
						"create_userID" => $this->session->userdata('loginuserID'),
						"create_username" => $this->session->userdata('username'),
						"create_usertype" => $this->session->userdata('usertype'),
					);

					$this->assessments_m->insert_assessment($array);
					$this->session->set_flashdata('success', $this->lang->line('menu_success'));
					redirect(base_url("assessments/index"));
				}
			} else {
				$this->data["subview"] = "assessments/add";
				$this->load->view('_layout_main', $this->data);
			}
		} else {
			$this->data["subview"] = "error";
			$this->load->view('_layout_main', $this->data);
		}
	}

	public function edit() {
		$usertype = $this->session->userdata("usertype");
		$schoolID = $this->session->userdata('schoolID');
		if($usertype == "Admin" || $usertype == "Super Admin") {
			$id = htmlentities($this->db->escape_str($this->uri->segment(3)));
			if((int)$id) {
				$this->data['assessments'] = $this->assessments_m->get_single_assessments(array("schoolID" => $schoolID, 'testID' => $id));
				if($this->data['assessments']) {
					if($_POST) {
						$rules = $this->rules();
						$this->form_validation->set_rules($rules);
						if ($this->form_validation->run() == FALSE) {
							$this->data["subview"] = "assessments/edit";
							$this->load->view('_layout_main', $this->data);			
						} else {
							$array = array(
								"assessments" => $this->input->post("assessments"),
								"note" => $this->input->post("note"),
								"modify_date" => date("Y-m-d h:i:s")
							);

							$this->assessments_m->update_assessments($array, $id);
							$this->session->set_flashdata('success', $this->lang->line('menu_success'));
							redirect(base_url("assessments/index"));
						}
					} else {
						$this->data["subview"] = "assessments/edit";
						$this->load->view('_layout_main', $this->data);
					}
				} else {
					$this->data["subview"] = "error";
					$this->load->view('_layout_main', $this->data);	
				}
			} else {
				$this->data["subview"] = "error";
				$this->load->view('_layout_main', $this->data);
			}	
		} else {
			$this->data["subview"] = "error";
			$this->load->view('_layout_main', $this->data);
		}
	}

	public function delete() {
		$usertype = $this->session->userdata("usertype");
		$schoolID = $this->session->userdata('schoolID');
		if($usertype == "Admin" || $usertype == "Super Admin") {
			$id = htmlentities($this->db->escape_str($this->uri->segment(3)));
			if((int)$id) {
				$this->data['assessments'] = $this->assessments_m->get_single_assessments(array("schoolID" => $schoolID, 'testID' => $id));
				if($this->data['assessments']) {
					$this->assessments_m->delete_assessments($id);
					$this->session->set_flashdata('success', $this->lang->line('menu_success'));
					redirect(base_url("assessments/index"));
				} else {
					redirect(base_url("assessments/index"));
				}
			} else {
				redirect(base_url("assessments/index"));
			}
		} else {
			$this->data["subview"] = "error";
			$this->load->view('_layout_main', $this->data);
		}	
	}

	public function unique_assessments() {
		$id = htmlentities($this->db->escape_str($this->uri->segment(3)));
		$schoolID = $this->session->userdata('schoolID');
		if((int)$id) {
			$assessments = $this->assessments_m->get_order_by_assessments(array('schoolID' => $schoolID, "assessments" => $this->input->post("assessments"), "testID !=" => $id));
			if(count($assessments)) {
				$this->form_validation->set_message("unique_assessments", "%s already exists");
				return FALSE;
			}
			return TRUE;
		} else {
			$assessments = $this->assessments_m->get_order_by_assessments(array('schoolID' => $schoolID, "assessments" => $this->input->post("assessments")));
			if(count($assessments)) {
				$this->form_validation->set_message("unique_assessments", "%s already exists");
				return FALSE;
			}
			return TRUE;
		}	
	}

	function date_valid($date) {
	  	if(strlen($date) <10) {
			$this->form_validation->set_message("date_valid", "%s is not valid dd-mm-yyyy");
	     	return FALSE;
		} else {
	   		$arr = explode("-", $date);   
	        $dd = $arr[0];            
	        $mm = $arr[1];              
	        $yyyy = $arr[2];
	      	if(checkdate($mm, $dd, $yyyy)) {
	      		return TRUE;
	      	} else {
	      		$this->form_validation->set_message("date_valid", "%s is not valid dd-mm-yyyy");
	     		return FALSE;
	      	}
	    } 

	} 
}

/* End of file assessments.php */
/* Location: .//D/xampp/htdocs/school/mvc/controllers/assessments.php */