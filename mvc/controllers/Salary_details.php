<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Salary_details extends MY_Controller {
/*
| -----------------------------------------------------
| PRODUCT NAME: 	CloudSkul
| -----------------------------------------------------
| AUTHOR:			CLOUDSKUL TEAM
| -----------------------------------------------------
| EMAIL:			info@cloudskul.com
| -----------------------------------------------------
| COPYRIGHT:		RESERVED BY Cloudskul
| -----------------------------------------------------
| WEBSITE:			http://cloudskul.com
| -----------------------------------------------------
*/
	public function __construct(){
		parent::__construct();
		$this->load->model('salary_details_m');	
	}
	
	public function index(){
		//get user session
		$usertype = $this->session->userdata("usertype");
		$schoolID = $this->session->userdata('schoolID');
		
		if($usertype == "Super Admin" || $usertype == "Admin") {
			// retrive all data from department table
			$this->data['all_department_info'] = $this->salary_details_m->get_departments($schoolID);
		
			$this->data['subview'] = 'salary_details/index';
			$this->load->view('_layout_main', $this->data);
		} else {
			//load error view
			$this->data["subview"] = "error";
			$this->load->view('_layout_main', $this->data);
		}
	}
	
	public function employee_salary_details($department_id){
		$usertype = $this->session->userdata("usertype");
		$schoolID = $this->session->userdata('schoolID');
		
		if($usertype == "Super Admin" || $usertype == "Admin") {
			
			$tthis->data['department_id'] = $department_id;
			// get all designation info by Department id
            $this->salary_details_m->_table_name = 'designations';
            $this->salary_details_m->_order_by = 'designation_id';
            $designation_info = $this->salary_details_m->get_by(array('department_id' => $department_id), FALSE);
			
			if (!empty($designation_info)) {
                foreach ($designation_info as $v_designatio) {
                    $this->data['employee_info'] = $this->salary_details_m->get_emp_information($department_id, $schoolID, $v_designatio->designation_id);
                }
            }
			//$this->data['employee_details'] = $this->salary_details_m->get_employees_in_departments($department_id, $schoolID);
			$this->data["salary_template"] = $this->salary_details_m->get_salary_template($schoolID);
			$this->load->view('salary_details/asign_template', $this->data); 
		} else {
			//load error view
			$this->data["subview"] = "error";
			$this->load->view('_layout_main', $this->data);
		}
	}
	
	public function save_salary_details(){
		$usertype = $this->session->userdata("usertype");
		$schoolID = $this->session->userdata('schoolID');
		
		if($usertype == "Super Admin" || $usertype == "Admin") {
			$employee_id = $this->input->post('employee_id', TRUE);
			$payroll_id = $this->input->post('payroll_id');
			if($payroll_id == null){
					$this->session->set_flashdata('error', 'Please select a salary template');
					redirect(base_url("salary_details/index"));
			}else{
				foreach($employee_id as $key => $v_emp_id){
					$employee_array = array(
							"employee_id" => $v_emp_id,
							"schoolID" => $schoolID
						);
					$id = $payroll_id[$key];
					$this->salary_details_m->update_salary_details($employee_array, $id, $schoolID);
				}
				
				$this->session->set_flashdata('success', $this->lang->line('menu_success'));
				redirect(base_url("salary_details/salary_list"));
			} 
		}else {
			//load error view
			$this->data["subview"] = "error";
			$this->load->view('_layout_main', $this->data);
		}
	}
	
	public function salary_list(){
		$usertype = $this->session->userdata("usertype");
		$schoolID = $this->session->userdata('schoolID');
		
		if($usertype == "Super Admin" || $usertype == "Admin") {
			// retrive all data from department table
			$this->data['salary_info'] = $this->salary_details_m->get_employee_salary_list($schoolID);
		
			$this->data['subview'] = 'salary_details/salary_list';
			$this->load->view('_layout_main', $this->data);
		} else {
			//load error view
			$this->data["subview"] = "error";
			$this->load->view('_layout_main', $this->data);
		}
	}
}
?>