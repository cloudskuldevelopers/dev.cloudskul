<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Child_care extends MY_Controller {
/*
| -----------------------------------------------------
| PRODUCT NAME: 	CloudSkul
| -----------------------------------------------------
| AUTHOR:			CLOUDSKUL TEAM
| -----------------------------------------------------
| EMAIL:			info@cloudskul.com
| -----------------------------------------------------
| COPYRIGHT:		RESERVED BY Cloudskul
| -----------------------------------------------------
| WEBSITE:			http://cloudskul.com
| -----------------------------------------------------
*/
	public function __construct () {
		parent::__construct();
		$this->load->model("parentes_m");
		$this->load->model("student_m");
		$this->load->model("child_care_m");
		$this->load->model("section_m");
		$this->load->model("classes_m");
		$language = $this->session->userdata('lang');
		$this->lang->load('child_care', $language);
	}

	public function index() {
		$usertype = $this->session->userdata("usertype");
		$schoolID = $this->session->userdata('schoolID');
		$id = $this->session->userdata('loginuserID');
		if($usertype == "Parent") {

			$schoolID = $this->session->userdata('schoolID');
			$this->data['pickups'] = $this->child_care_m->get_order_by_guardian(array("parentID" => $id));
			$this->data['policy'] = $this->parentes_m->get_order_by_parent_policy(array('schoolID' => $schoolID,"parentID" => $id))->pickup;
			$this->data["subview"] = "child_care/index";
			$this->load->view('_layout_main', $this->data);

		}elseif($usertype == "Admin" || $usertype == "Super Admin" ||$usertype == "Teacher") {
			$id = htmlentities($this->db->escape_str($this->uri->segment(3)));
			$schoolID = $this->session->userdata('schoolID');
			if((int)$id) {
				$this->data['set'] = $id;
				$this->data['classes'] = $this->student_m->get_order_by_classes(array("schoolID" => $schoolID));
				//$this->data['students'] = $this->student_m->get_order_by_student(array("schoolID" => $schoolID, 'classesID' => $id));
				$this->data['students'] = $this->child_care_m->get_join_all($id);
				if($this->data['students']) {
					$sections = $this->section_m->get_order_by_section(array("schoolID" => $schoolID, "classesID" => $id));
					$this->data['sections'] = $sections;
					foreach ($sections as $key => $section) {
						$this->data['allsection'][$section->sectionID] = $this->student_m->get_order_by_student(array("schoolID" => $schoolID, 'classesID' => $id, "sectionID" => $section->sectionID));
					}
				} else {
					$this->data['students'] = NULL;
				}

				$this->data["subview"] = "child_care/index_list";
				$this->load->view('_layout_main', $this->data);
			} else {
				$this->data['classes'] = $this->student_m->get_order_by_classes(array("schoolID" => $schoolID));
				$this->data["subview"] = "child_care/search";
				$this->load->view('_layout_main', $this->data);
			}
		}else {
			$this->data["subview"] = "error";
			$this->load->view('_layout_main', $this->data);
		}
	}

	public function student_list() {
		$classID = $this->input->post('id');
		if((int)$classID) {
			$string = base_url("child_care/index/$classID");
			echo $string;
		} else {
			redirect(base_url("child_care/index"));
		}
	}

	protected function rules() {
		$rules = array(
			array(
				'field' => 'first_name', 
				'label' => $this->lang->line("pickup_last_name"), 
				'rules' => 'trim|required|xss_clean|max_length[60]'
			),
			array(
				'field' => 'last_name',
				'label' => $this->lang->line("pickup_first_name"), 
				'rules' => 'trim|required|xss_clean|max_length[60]'
			),
			array(
				'field' => 'email', 
				'label' => $this->lang->line("pickup_email"), 
				'rules' => 'trim|required|max_length[40]|valid_email|xss_clean'
			),
			array(
				'field' => 'phone', 
				'label' => $this->lang->line("pickup_phone"), 
				'rules' => 'trim|required|min_length[5]|max_length[25]|xss_clean'
			),
			array(
				'field' => 'address', 
				'label' => $this->lang->line("pickup_address"), 
				'rules' => 'trim|max_length[200]|xss_clean'
			),
			array(
				'field' => 'photo', 
				'label' => $this->lang->line("parentes_photo"), 
				'rules' => 'trim|max_length[200]|xss_clean'
			),
			array(
				'field' => 'profession', 
				'label' => $this->lang->line("pickup_profession"), 
				'rules' => 'trim|required|min_length[4]|max_length[40]|xss_clean|callback_lol_username'
			)
		);
		return $rules;
	}

	function insert_with_image($username) {
	    $random = rand(1, 10000000000000000);
	    $makeRandom = hash('sha512', $random. 'CS' .$username . config_item("encryption_key"));
	    return $makeRandom;
	}

	public function add() {
		$usertype = $this->session->userdata("usertype");
		if($usertype == "Admin" || $usertype == "Super Admin" || $usertype == "Parent") {

			if($_POST) {
				$rules = $this->rules();
				$this->form_validation->set_rules($rules);
				if ($this->form_validation->run() == FALSE) {
					$this->data['form_validation'] = validation_errors(); 
					$this->data["subview"] = "child_care/add";
					$this->load->view('_layout_main', $this->data);		
				} else {
					$array = array();
					for($i=0; $i<count($rules); $i++) {
						$array[$rules[$i]['field']] = $this->input->post($rules[$i]['field']);
					}
					//$array['password'] = $this->student_m->hash($this->input->post("password"));
					//$array['usertype'] = "Parent";
					$array['parentID'] = $this->session->userdata('loginuserID');
					$array["schoolID"] = $this->session->userdata('schoolID');
					$array["create_date"] = date("Y-m-d h:i:s");
					$array["modify_date"] = date("Y-m-d h:i:s");
					$array["create_userID"] = $this->session->userdata('loginuserID');
					$array["create_username"] = $this->session->userdata('username');
					$array["create_usertype"] = $this->session->userdata('usertype');
					$array["active"] = 1;

					$new_file = "defualt.png";
					if($_FILES["image"]['name'] !="") {
						$file_name = $_FILES["image"]['name'];
						$file_name_rename = $this->insert_with_image($this->input->post("first_name"));
			            $explode = explode('.', $file_name);
			            if(count($explode) >= 2) {
				            $new_file = $file_name_rename.'.'.$explode[1];
							$config['upload_path'] = "./uploads/images";
							$config['allowed_types'] = "gif|jpg|png";
							$config['file_name'] = $new_file;
							$config['max_size'] = '1024';
							$config['max_width'] = '3000';
							$config['max_height'] = '3000';
							$array['photo'] = $new_file;
							$this->load->library('upload', $config);
							if(!$this->upload->do_upload("image")) {
								$this->data["image"] = $this->upload->display_errors();
								$this->data["subview"] = "child_care/add";
								$this->load->view('_layout_main', $this->data);
							} else {
								$data = array("upload_data" => $this->upload->data());
								$this->child_care_m->insert_guardian($array);
								$this->session->set_flashdata('success', $this->lang->line('menu_success'));
								redirect(base_url("child_care/index"));
							}
						} else {
							$this->data["image"] = "Invalid file";
							$this->data["subview"] = "child_care/add";
							$this->load->view('_layout_main', $this->data);
						}
					} else {
						$array["photo"] = $new_file;
						$this->child_care_m->insert_guardian($array);
						$this->session->set_flashdata('success', $this->lang->line('menu_success'));
						redirect(base_url("child_care/index"));
					}
				}
			} else {
				$this->data["subview"] = "child_care/add";
				$this->load->view('_layout_main', $this->data);
			}

		} else {
			$this->data["subview"] = "error";
			$this->load->view('_layout_main', $this->data);
		}
	}

	public function edit() {
		return false;
		$usertype = $this->session->userdata("usertype");
		if($usertype == "Admin" || $usertype == "Super Admin" || $usertype == "Parent") {
			$id = htmlentities($this->db->escape_str($this->uri->segment(3)));
			
			if ((int)$id) {
				$schoolID = $this->session->userdata('schoolID');
				$this->data['parentes'] = $this->parentes_m->get_single_parentes(array("schoolID" => $schoolID, "parentID" => $id));
				if($this->data['parentes']) {
					if($_POST) {
						$rules = $this->rules();
						unset($rules[8],$rules[9], $rules[10]);
						$this->form_validation->set_rules($rules);
						if ($this->form_validation->run() == FALSE) { 
							$this->data["subview"] = "parentes/edit";
							$this->load->view('_layout_main', $this->data);
						} else {

							$array = array();
							for($i=0; $i<count($rules); $i++) {
								$array[$rules[$i]['field']] = $this->input->post($rules[$i]['field']);
							}
							$array["modify_date"] = date("Y-m-d h:i:s");
						
							if($_FILES["image"]['name'] !="") {
								if($this->data['parentes']->photo != 'defualt.png') {
									unlink(FCPATH.'uploads/images/'.$this->data['parentes']->photo);
								}
								$file_name = $_FILES["image"]['name'];
								$file_name_rename = $this->insert_with_image($this->data['parentes']->username);
					            $explode = explode('.', $file_name);
					            if(count($explode) >= 2) {
						            $new_file = $file_name_rename.'.'.$explode[1];
									$config['upload_path'] = "./uploads/images";
									$config['allowed_types'] = "gif|jpg|png";
									$config['file_name'] = $new_file;
									$config['max_size'] = '1024';
									$config['max_width'] = '3000';
									$config['max_height'] = '3000';
									$array['photo'] = $new_file;
									$this->load->library('upload', $config);
									if(!$this->upload->do_upload("image")) {
										$this->data["image"] = $this->upload->display_errors();
										$this->data["subview"] = "parentes/edit";
										$this->load->view('_layout_main', $this->data);
									} else {
										$data = array("upload_data" => $this->upload->data());
										$this->parentes_m->update_parentes($array, $id);
										$this->session->set_flashdata('success', $this->lang->line('menu_success'));
										redirect(base_url("parentes/index"));
									}
								} else {
									$this->data["image"] = "invalid file";
									$this->data["subview"] = "parentes/edit";
									$this->load->view('_layout_main', $this->data);
								}
							} else {
								$this->parentes_m->update_parentes($array, $id);
								$this->session->set_flashdata('success', $this->lang->line('menu_success'));
								redirect(base_url("parentes/index"));
							}
						}
					} else {
						$this->data["subview"] = "parentes/edit";
						$this->load->view('_layout_main', $this->data);
					}
				} else {
					$this->data["subview"] = "error";
					$this->load->view('_layout_main', $this->data);
				}
			} else {
				$this->data["subview"] = "error";
				$this->load->view('_layout_main', $this->data);
			}
		} else {
			$this->data["subview"] = "error";
			$this->load->view('_layout_main', $this->data);
		}
	}


	public function view() {
		return false;
		$usertype = $this->session->userdata("usertype");
		if($usertype == "Admin" || $usertype == "Super Admin" || $usertype == "Parent") {
			$id = htmlentities($this->db->escape_str($this->uri->segment(3)));
			if ((int)$id) {
				$schoolID = $this->session->userdata('schoolID');
				$this->data['parentes'] = $this->parentes_m->get_single_parentes(array("schoolID" => $schoolID, "parentID" => $id));

				if($this->data['parentes']) {
					$this->data["subview"] = "parentes/view";
					$this->load->view('_layout_main', $this->data);
				} else {
					$this->data["subview"] = "error";
					$this->load->view('_layout_main', $this->data);
				}
			} else {
				$this->data["subview"] = "error";
				$this->load->view('_layout_main', $this->data);
			}
		} else {
			$this->data["subview"] = "error";
			$this->load->view('_layout_main', $this->data);
		}
	}

	public function print_preview() {
		$id = htmlentities($this->db->escape_str($this->uri->segment(3)));
		$url = htmlentities($this->db->escape_str($this->uri->segment(4)));

		if ((int)$id) {
			$usertype = $this->session->userdata('usertype');
			if ($usertype == "Admin" || $usertype == "Super Admin" || $usertype == "Parent") {
				$this->load->library('html2pdf');
			    $this->html2pdf->folder('./assets/pdfs/');
			    $this->html2pdf->filename('Report.pdf');
			    $this->html2pdf->paper('a4', 'portrait');

				$schoolID = $this->session->userdata('schoolID');
				$this->data['parentes'] = $this->parentes_m->get_single_parentes(array("schoolID" => $schoolID, "parentID" => $id));
				if($this->data['parentes']) {
					$this->data['panel_title'] = $this->lang->line('panel_title');
					$html = $this->load->view('parentes/print_preview', $this->data, true);
					$this->html2pdf->html($html);
					$this->html2pdf->create();
				} else {
					$this->data["subview"] = "error";
					$this->load->view('_layout_main', $this->data);
				}
			} else {
				$this->data["subview"] = "error";
				$this->load->view('_layout_main', $this->data);
			}	
		} else {
			$this->data["subview"] = "error";
			$this->load->view('_layout_main', $this->data);
		}
	}

	public function send_mail() {
		$usertype = $this->session->userdata("usertype");
		$schoolID = $this->session->userdata('schoolID');
		if($usertype == "Admin" || $usertype == "Super Admin" || $usertype == "Parent") {
			$id = $this->input->post('id');
			if ((int)$id) {
				$this->data['parentes'] = $this->parentes_m->get_single_parentes(array('schoolID' => $schoolID, 'parentID' => $id));
				if($this->data['parentes']) {
					$html = $this->load->view('parentes/print_preview', $this->data, true);
					$this->load->library('html2pdf');
				    $this->html2pdf->folder('uploads/report');
				    $this->html2pdf->filename('Report.pdf');
				    $this->html2pdf->paper('a4', 'portrait');
				    $this->data['panel_title'] = $this->lang->line('panel_title');
					$this->html2pdf->html($html);
					$this->html2pdf->create('save');
					
					if($path = $this->html2pdf->create('save')) {
					$this->load->library('email');
					$this->email->set_mailtype("html");
					$this->email->from($this->data["siteinfos"]->email, $this->session->userdata('school'));
					$this->email->to($this->input->post('to'));
					$this->email->subject($this->input->post('subject'));
					$this->email->message($this->input->post('message'));	
					$this->email->attach($path);
						if($this->email->send()) {
							$this->session->set_flashdata('success', $this->lang->line('mail_success'));
						} else {
							$this->session->set_flashdata('error', $this->lang->line('mail_error'));
						}
					}
				} else {
					$this->data["subview"] = "error";
					$this->load->view('_layout_main', $this->data);
				}
			} else {
				$this->data["subview"] = "error";
				$this->load->view('_layout_main', $this->data);
			}
		} else {
			$this->data["subview"] = "error";
			$this->load->view('_layout_main', $this->data);
		}
	}

	public function delete() {
		$usertype = $this->session->userdata("usertype");
		if($usertype == "Admin" || $usertype == "Super Admin" || $usertype == "Parent") {
			$id = htmlentities($this->db->escape_str($this->uri->segment(3)));
			if((int)$id) {
				$schoolID = $this->session->userdata('schoolID');
				$this->data['pickups'] = $this->child_care_m->get_single_guardian(array("schoolID" => $schoolID, "policyID" => $id));
				if($this->data['pickups']) {
					if($this->data['pickups']->photo != 'defualt.png') {
						unlink(FCPATH.'uploads/images/'.$this->data['pickups']->photo);
					}
					$this->child_care_m->delete_guardian($id);
					$this->session->set_flashdata('success', $this->lang->line('menu_success'));
					redirect(base_url("child_care/index"));
				} else {
					redirect(base_url("child_care/index"));
				}
			} else {
				redirect(base_url("child_care/index"));
			}
		} else {
			$this->data["subview"] = "error";
			$this->load->view('_layout_main', $this->data);
		}
	}

	public function unique_email() {
		$id = htmlentities($this->db->escape_str($this->uri->segment(3)));
		if((int)$id) {
			$parents = $this->child_care_m->get_single_guardian(array('policyID' => $id));
			$tables = array('student' => 'student', 'parent' => 'parent', 'teacher' => 'teacher', 'user' => 'user', 'systemadmin' => 'systemadmin');
			$array = array();
			$i = 0;
			foreach ($tables as $table) {
				$user = $this->parentes_m->get_username($table, array("email" => $this->input->post('email'), 'username !=' => $parents->username ));
				if(count($user)) {
					$this->form_validation->set_message("unique_email", "%s already exists");
					$array['permition'][$i] = 'no';
				} else {
					$array['permition'][$i] = 'yes';
				}
				$i++;
			}
			if(in_array('no', $array['permition'])) {
				return FALSE;
			} else {
				return TRUE;
			}
		} else {
			$tables = array('student' => 'student', 'parent' => 'parent', 'teacher' => 'teacher', 'user' => 'user', 'systemadmin' => 'systemadmin');
			$array = array();
			$i = 0;
			foreach ($tables as $table) {
				$user = $this->student_m->get_username($table, array("email" => $this->input->post('email')));
				if(count($user)) {
					$this->form_validation->set_message("unique_email", "%s already exists");
					$array['permition'][$i] = 'no';
				} else {
					$array['permition'][$i] = 'yes';
				}
				$i++;
			}

			if(in_array('no', $array['permition'])) {
				return FALSE;
			} else {
				return TRUE;
			}
		}	
	}
	
	function active() {
		$usertype = $this->session->userdata("usertype");
		if($usertype == "Admin" || $usertype == "Super Admin" || $usertype == "Parent") {
			$id = $this->input->post('id');
			$status = $this->input->post('status');
			if($id != '' && $status != '') {
				if((int)$id) {
					if($status == 'checked') {
						$this->child_care_m->update_guardian(array('active' => 1), $id);
						echo 'Success';
					} elseif($status == 'unchecked') {
						$this->child_care_m->update_guardian(array('active' => 0), $id);
						echo 'Success';
					} else {
						echo "Error";
					}
				} else {
					echo "Error";
				}
			} else {
				echo "Error";
			}
		} else {
			echo "Error";
		}
	}

	function activate() {
		$usertype = $this->session->userdata("usertype");
		if($usertype == "Parent") {
			$id = $this->input->post('id');
			$status = $this->input->post('status');
			if($id != '' && $status != '') {
				if((int)$id) {
					if($status == 'active') {
						$this->parentes_m->update_parentes(array('pickup' => 1), $id);
						echo 'Success';
					} elseif($status == 'inactive') {
						$this->parentes_m->update_parentes(array('pickup' => 0), $id);
						echo 'Success';
					} else {
						echo "Error";
					}
				} else {
					echo "Error";
				}
			} else {
				echo "Error";
			}
		} else {
			echo "Error";
		}
	}
}

/* End of file child_care.php */