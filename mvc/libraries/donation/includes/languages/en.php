<?php
//Constant

define("TRANSACTIONDETAIL","Transaction Detail");
define("SELLANG","Select Language");
define("SELCURR","Select Currency");
define("DONATIONTEXT","How much would you like to donate");
define("CUSTOM","Custom");
define("PAYMENTTYPE","Payment Type");
define("RECCURING","Reccuring");
define("ONETIMEPAYMEMT","One Time Payment");
define("NAME","Name");
define("LNAME","Last Name");
define("EMAIL","Email id");
define("PHONE","Phone No");
define("ADDRESS","Address");
define("ADDITIONALNOTE","Additional Note");
define("DONATE","Donate");
define("WIATINGTEXT","Please Wait......");
define("SELECTCYCLE","Select Cycle");
define("WEEKLY","Weekly");
define("MONTHLY","Monthly");
define("YEARLY","Yearly");
define("NOCYCLE","Select number of cycle ");
define("SELPAYOPT","Select payment option");
define("PAYPAL","Paypal");
define("STRIPEPAY","Stripe PayG");
define("PERSONALDETAIL" ,"Personal Detail");
define("CREDITCARD","Credit Card");
define("BANKTRANSFER","Banking Transfer");
define("CARDTYPE","Card Type");
define("SELLANG","Select Language");
define("SELCARDTYPE","Select Card Type");
define("VISA","VISA");
define("MASTERCARD","Master Card");
define("DISCOVER","Discover");
define("AMERICANEXP","American Express");
define("CARDNUMBER","Card Number");
define("EXPDATE","Expiration Date");
define("SELMONTH","Select Month");
define("SELYEAR","Select Year");
define("CARDVERINUM","Card Verification Number ");
define("TITLE","EasyDonation");
define("SEARCHBYNAME","Search By Name");
define("DATEFROM","Date From");
define("DATETO","Date To");
define("DONATIONAMOUNT","Donation Amount");
define("PAYMENTMODE","Payment Mode");
define("TRANSACTIONID","Transaction ID");
define("TRANSACTIONDATE","Transaction Date");
define("CURRENCY","Currency");
define("CYCLETYPE","Cycle Type");
define("NUMBEROFCYCLETEXT","Number Of Cycle");
define("TRANSACTIONSTATUS","Transaction Status");
define("SEARCH","Search");
define("RESET","Reset");
define("SUBSCRIID","Subscribe Id");
define("SELECTPAYCYCLE","Select Payment Cycle");
define("SELECTPAYCYCLETIME","Select Payment Number of Cycle ");
define("MONTHANDYEAROFEXPIRY","Month & Year of Expiry");
define("MSGFORCARDDIGIT","Enter your 16 digit card number");
define("CREDITCARDMSG","In Case of credit card ,Transaction in Doller($) will be accepted.");
define("PAMENTREQUESTSERVICE","Payment Request Service");
define("THANKSMSG","Thanks for your valuable help");
define("DEAR","Dear");
define("SUCESSDISPLAYMSG","Your donation was successful. Your commitment to helping  in our community is sincerely appreciated");
define("BELOWAREYOURAMOUNTTRNASACTIONDETAILS","Below are your amount transaction details");
define("ISTHERESOMETHINGTOSAHARE","Is there something you want to share with us?");
define("FEEDBACKTO","Feedback, comments, suggestions or compliments - do write to");
define("SHAREFORMAILHELP","Share for more help :");
define("ORDERID","Order id");
define("LOCALPAYMENT","Local Payment");
define("PAYABLETO","Payable to");
define("BANKNAME","Bank Name");
define("ACCOUNTNUMBER","Account No");
define("BRANCHCODE","Branch Code");
define("INTERNATIONALPAYMENT","Internation Payment");
define("PAYABLETO","Paybale To");
define("BIC", "BIC / SWIFT code");
define("IBANNUMBER","IBAN number/Account number");
define("STREETADDRESS","Street Address");
define("POSTALADDRESS","Postal Address");
define("TELEPHONENUMBER","Telephone Number");
define("FAXNUMBER","Fax number");
define("BRANCHCODE","Branch Code");
define("FAILPAGEMSG","Oops! Your transaction is failed. Please try again");
define("CLICKHERE","Click Here");
define("MAINPAGEERROR","Error occurred while sending email");
define("CURRENCYNOTFOUNDERROR","Error:Missing attribute in currency.xml . There are $valuesInXml value attributes and $nameInXml name attributes .Both should be same");
define("LANGUAGEVARIABLEERROR","Error:Missing attribute in language.xml . There are $idInXml value attributes and $LnameInXml name attributes .Both should be same");
define("DONATIONAMOUNTERROR","ERROR: Only numeric string is allowed in donationAmount array in configuration.php");
define("PAYMENTVARIABLEERROR","Error:There should be atleast Five Element in paymentMethods in Configuration.php");
define("PAYMENTVARIABLFORRECCURRINGEERROR","Error in  paymentMethods array in Configuration.php. Please define recurring element of paymentMethods");
define("PAYMENTVARIABLFORPAYPALEERROR","Error in  paymentMethods array in Configuration.php. Please define recurring element of paymentMethods");
define("PAYMENTVARIABLFORCREDITEERROR","Error in  paymentMethods array in Configuration.php. Please define creditCard element of paymentMethods array");
define("PAYMENTVARIABLFORBANKINGEERROR","Error:Error in  paymentMethods array in Configuration.php. Please define bankingTransfer element of paymentMethods array");
define("PAYMENTVARIABLFORSTRIPEERROR","Error:Error in  paymentMethods array in Configuration.php. Please define StripePay element of paymentMethods array");
define("FILENOTFOUND","File Not found");
define("UPDATEDATBASE","Updating database");
define("NORESULTFOUND","No result Found");
define("TOTALTRANSACTION","Total Transactions :");
define("ALL","All");
define("SUCCESSFUL","Successful");
define("FAILED","Failed");
define("CLEARFILTERS","Clear Filters");
define("DATE","Date");
define("TO","To");
define("SHOWMORE", "Show More");
define("VIEWINVOICE", "View Invoice");
define("INVALIDINVOICEID","Invalid invoice ID supplied");
define("SELECTPAYMENTTYPE","Select Payment Type");
define("SUBJECTTOADMIN","Benevolent Transaction Detail");
define("SUBJECTTOBENOVELENT"," Your Transaction Detail");
define("ITEMNAME","Donation");
define("NA","NA");
define("COMPLETED","Completed");
define("PENDING","Pending");
define("FAIL","Fail");
define("BENOVELENTDETAIL","Benevolen Detail");
define("ALLPAYMENTMETHODSCANNOTBEFALSE", "All Payment methods can not be False");
//Constant










