<?php
//Constant
define("TRANSACTIONDETAIL","transacción detalle");
define("SELLANG","seleccionar idioma");
define("SELCURR","seleccionar moneda");
define("DONATIONTEXT","Combien seriez-vous souhaitez faire un don");
define("CUSTOM","costumbre");
define("PAYMENTTYPE","pago tipo");
define("RECCURING","periódico");
define("ONETIMEPAYMEMT","uno tiempo pago");
define("NAME","nombre");
define("LNAME","último nombre");
define("EMAIL","Correo electrónico");
define("PHONE","teléfono número");
define("ADDRESS","dirección");
define("ADDITIONALNOTE","adicional nota");
define("DONATE","donar");
define("WIATINGTEXT","Por Favor esperar......");
define("SELECTCYCLE","seleccionar ciclo");
define("WEEKLY","semanal");
define("MONTHLY","mensual");
define("YEARLY","anual");
define("NOCYCLE","Seleccione el número de ciclo");
define("SELPAYOPT","Seleccione forma de pago");
define("PAYPAL","payPal");
define("CREDITCARD","tarjeta de crédito");
define("BANKTRANSFER","Transferencia Bancaria");
define("CARDTYPE","Tipo de tarjeta");
define("PERSONALDETAIL" ,"Détail personnelles");
define("STRIPEPAY","Stripe PayG");
define("SELLANG","Sélectionnez la langue");
define("SELCARDTYPE","Seleccionar el tipo de tarjeta");
define("VISA","VISA");
define("MASTERCARD","tarjeta MasterCard");
define("DISCOVER","Descubra");
define("AMERICANEXP","tarjeta American Express");
define("CARDNUMBER","número de tarjeta");
define("EXPDATE","fecha de expiración");
define("SELMONTH","Seleccione Mes");
define("SELYEAR","Seleccionar año");
define("CARDVERINUM","Tarjeta del número de Verificación");
define("TITLE","Easy Donation");
define("SEARCHBYNAME","Buscar por Nombre");
define("DATEFROM","Desde la fecha");
define("DATETO","Fecha Para");
define("DONATIONAMOUNT","Cantidad de Donación");
define("PAYMENTMODE","Modo de Pago");
define("TRANSACTIONID","identificación de la transacción");
define("TRANSACTIONDATE","Fecha de Transacción");
define("CURRENCY","moneda");
define("CYCLETYPE","Tipo de ciclo");
define("NUMBEROFCYCLETEXT","Número de ciclos");
define("TRANSACTIONSTATUS","situación de la operación");
define("SEARCH","búsqueda");
define("RESET","reajustar");
define("SUBSCRIID","Abonnez-Id");
define("MONTHANDYEAROFEXPIRY","Mois et Année d'expiration");
define("MSGFORCARDDIGIT","Entrez votre numéro de carte de 16 chiffres");
define("SELECTPAYCYCLE","Sélectionner le cycle de paiement");
define("SELECTPAYCYCLETIME","Sélectionnez paiement Nombre de cycle ");
define("CREDITCARDMSG","En cas de carte de crédit, Transaction en Doller ($) seront acceptés.");
define("PAMENTREQUESTSERVICE","Demande de paiement service");
define("THANKSMSG","Merci pour votre aide précieuse");
define("DEAR","cher");
define("SUCESSDISPLAYMSG","Votre don a été un succès. Votre engagement à aider dans notre communauté est sincèrement appréciée");
define("BELOWAREYOURAMOUNTTRNASACTIONDETAILS","Voici le montant de votre détails de la transaction");
define("ISTHERESOMETHINGTOSAHARE","Y at-il quelque chose que vous souhaitez partager avec nous?");
define("FEEDBACKTO","Réactions, commentaires, suggestions ou des compliments - écrivez-");
define("SHAREFORMAILHELP","Partager pour plus d'aide:");
define("ORDERID","Numéro de commande");
define("LOCALPAYMENT","Paiement locale");
define("PAYABLETO","à payer à");
define("BANKNAME","Nom de la banque");
define("ACCOUNTNUMBER","n ° de compte");
define("BRANCHCODE","Numéro de compte");
define("INTERNATIONALPAYMENT","Internation paiement");
define("BIC", "Code BIC / SWIFT");
define("IBANNUMBER","IBAN / Numéro de compte");
define("STREETADDRESS","Adresse De La Rue");
define("POSTALADDRESS","Adresse postale");
define("TELEPHONENUMBER","numéro de téléphone");
define("FAXNUMBER","Numéro de fax");
define("BRANCHCODE","Code Direction");
define("FAILPAGEMSG","Oops! Votre transaction a échoué. Se il vous plaît essayer à nouveau");
define("CLICKHERE","Cliquez Ici");
define("MAINPAGEERROR","Une erreur est survenue lors de l'envoi email");
define("CURRENCYNOTFOUNDERROR","Erreur: attribut manquant dans Currency.xml. Il ya $valuesInXml attributs de valeur et $nameInXml attributs de nom .Les deux doit être identique");
define("LANGUAGEVARIABLEERROR","Erreur: attribut manquant dans language.xml. Il ya $idInXml attributs de valeur et $LnameInXml attributs de nom .Les deux doit être identique");
define("DONATIONAMOUNTERROR","Erreur: Seulement chaîne numérique est autorisée dans le tableau donationAmount dans configuration.php");
define("PAYMENTVARIABLEERROR","Erreur: Il devrait être atleast quatre éléments dans paymentMethods dans Configuration.php");
define("PAYMENTVARIABLFORRECCURRINGEERROR","Erreur dans paymentMethods tableau dans Configuration.php. Se il vous plaît définir élément récurrent des paymentMethods");
define("PAYMENTVARIABLFORPAYPALEERROR","Erreur dans paymentMethods tableau dans Configuration.php. Se il vous plaît définir élément récurrent des paymentMethods");
define("PAYMENTVARIABLFORCREDITEERROR","Erreur dans paymentMethods tableau dans Configuration.php. Se il vous plaît définir élément de creditCard de paymentMethods tableau");
define("PAYMENTVARIABLFORBANKINGEERROR","Erreur: Erreur dans paymentMethods tableau dans Configuration.php. Se il vous plaît définir élément bankingTransfer de paymentMethods tableau");
define("FILENOTFOUND","Fichier introuvable");
define("UPDATEDATBASE","Base de données de mise à jour");
define("NORESULTFOUND","Aucun résultat trouvé");
define("TOTALTRANSACTION","total des opérations :");
define("ALL","tous");
define("SUCCESSFUL","réussi");
define("FAILED","manqué");
define("CLEARFILTERS","Effacer les filtres");
define("DATE","Date");
define("TO","à");
define("SHOWMORE", "montrer Plus");
define("VIEWINVOICE", "Voir facture");
define("INVALIDINVOICEID","Facture valide ID fourni");
define("SELECTPAYMENTTYPE","Sélectionner le type de paiement");
define("SUBJECTTOADMIN","Benevolent Transaction Détail");
define("SUBJECTTOBENOVELENT","Votre Transaction Détail");
define("ITEMNAME","don");
define("NA","N / A");
define("COMPLETED","terminé");
define("PENDING","en attendant");
define("FAIL","échouer");
define("BENOVELENTDETAIL","Détail de bienfaisance");
define("ALLPAYMENTMETHODSCANNOTBEFALSE", "All Payment methods can not be False");

//Constant















