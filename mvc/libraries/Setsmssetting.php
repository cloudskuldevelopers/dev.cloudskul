<?php
class Setsmssetting {
	public function set_sms_setting($schoolID) {
		$smsgetways = array(
			'clickatell' => array(
				0 => array(
					'schoolID' => $schoolID,
					'types' => 'clickatell',
					'field_names' => 'clickatell_username'
				),
				1 => array(
					'schoolID' => $schoolID,
					'types' => 'clickatell',
					'field_names' => 'clickatell_password'
				),
				2 => array(
					'schoolID' => $schoolID,
					'types' => 'clickatell',
					'field_names' => 'clickatell_api_key'
				)
			),
			'twilio' => array(
				0 => array(
					'schoolID' => $schoolID,
					'types' => 'twilio',
					'field_names' => 'twilio_accountSID'
				),
				1 => array(
					'schoolID' => $schoolID,
					'types' => 'twilio',
					'field_names' => 'twilio_authtoken'
				),
				2 => array(
					'schoolID' => $schoolID,
					'types' => 'twilio',
					'field_names' => 'twilio_fromnumber'
				)
			),
			'bulk' => array(
				0 => array(
					'schoolID' => $schoolID,
					'types' => 'bulk',
					'field_names' => 'bulk_username'
				),
				1 => array(
					'schoolID' => $schoolID,
					'types' => 'bulk',
					'field_names' => 'bulk_password'
				),
			)
		);
		return $smsgetways;
	}
}
?>