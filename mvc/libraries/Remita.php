<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
| -----------------------------------------------------
| PRODUCT NAME: 	CloudSkul
| -----------------------------------------------------
| AUTHOR:			Emmanuel Etti
| ----------------------------------------------------- 
| Contributor:		Sunday Okoi
| -----------------------------------------------------
| EMAIL:			info@cloudskul.com
| -----------------------------------------------------
| COPYRIGHT:		
| -----------------------------------------------------
| WEBSITE:			http://www.cloudskul.com
| -----------------------------------------------------
*/


  class Remita extends MY_Model

  {
  	
  	function __construct()
	{
		parent::__construct();

		$this->load->model("payment_settings_m");
		$this->getSettings();

	}

	//this function gets the Remita Merchant details from DB
	//I will make this function private, so it only gets called from inside the class
	public function getSettings(){
		$api_config = [];
		$get_configs = $this->payment_settings_m->get_order_by_config();
		foreach ($get_configs as $key => $get_key) {
			$api_config[$get_key->config_key] = $get_key->value;
		}
		// return $api_config['service_type_id'];
		$this->MERCHANTID = $api_config['merchant_id'];

		$this->SERVICETYPEID = $api_config['service_type_id'];
 
		$this->APIKEY  = $api_config['api_key'];
	}
	// public $get_configs = $this->payment_settings_m->get_order_by_config();
	// public $get_configs = $this->getSettings();
	//print($get_configs); die;

	//var_dump($get_configs); die;

	// private $MERCHANTID = "2547916";//should come from database $api_config['merchant_id']

	// private $SERVICETYPEID = "4430731";//should come from database $api_config['service_type_id']
 
	// private $APIKEY  = "1946";//should come from database $api_config['api_key']

	private $SPLIT_GATEWAYURL = "http://www.remitademo.net/remita/ecomm/v2/init.reg"; //split payment url

	private $GATEWAYRRRPAYMENTURL = "http://www.remitademo.net/remita/ecomm/finalize.reg";//split payment gateway url

	private $SPLIT_CHECKSTATUSURL= "http://www.remitademo.net/remita/ecomm";//SPLIT PAYMENT CHECK STATUS URL

	private $CHECKSTATUSURL = "http://www.remitademo.net/remita/ecomm";//normal payment check status url

	private $GATEWAYURL = "http://www.remitademo.net/remita/ecomm/init.reg";//normal payment url

    private $status_msg = "";

    private $status_code = "";
	

	//get method for amount
	private function get_total_amount()
	{
		return $this->total_amount;
	}

	//set method for the total amount
	public function set_total_amount($amt) 
	{
		$this->total_amount = $amt;
	}
	//get method for payer name
	private function get_payer_name()
	{
		return $this->payer_name;
	}

	//set method for the payer_name
	public function set_payer_name($name) 
	{
		$this->payer_name = $name;
	}
	//get method for payer email
	private function get_payer_email()
	{
		return $this->payer_email;
	}

	//set method for the payer email
	public function set_payer_email($email) 
	{
		$this->payer_email = $email;
	}

	//get method for payer phone
	private function get_payer_phone()
	{
		return $this->payer_phone;
	}

	//set method for the payer details
	public function set_payer_phone($phone) 
	{
		$this->payer_phone = $phone;
	}

	//get method for school account
	private function get_beneficiary()
	{
		return $this->beneficiary;
	}

	//set method for the school account
	public function set_beneficiary($ben) 
	{
		$this->beneficiary = $ben;
	}
	//get cloudskul service charge
	private function get_cloudskul_fee()
	{
		return $this->cloudskul_fee;
	}

	//set method for cloudskul service charge
	public function set_cloudskul_fee($fee) 
	{
		$this->cloudskul_fee = $fee;
	}
	//get method for cloud school account
	private function get_beneficiary_cloud()
	{
		return $this->beneficiary_cloud;
	}

	//set method for fee amount
	public function set_school_fee($fee) 
	{
		$this->school_fee = $fee;
	}
	//get method for school fee amount
	private function get_school_fee()
	{
		return $this->school_fee;
	}

	//set method for the cloudskul account
	public function set_beneficiary_cloud($ben) 
	{
		$this->beneficiary_cloud = $ben;
	}

	//get method for payment type
	private function get_payment_type()
	{
		return $this->payment_type;
	}

	//set method for the payment type
	public function set_payment_type($type) 
	{
		$this->payment_type = $type;
	}
	//get method for payment type
	private function get_responseurl()
	{
		return $this->responseurl;
	}

	//set method for the payment type
	public function set_responseurl($url) 
	{
		$this->responseurl = $url;
	}
	
	//get method for order ID
	private function get_orderID()
	{
		return $this->orderID;
	}

	//set method for the orderID 
	public function set_orderID($orderID) 
	{
		$this->orderID = $orderID;
	}
	
	//get method for payment description
	private function get_payment_descr()
	{
		return $this->descr;
	}

	//set method for the payment description
	public function set_payment_descr($descr) 
	{
		$this->descr = $descr;
	}

	public function get_donation_content(){
		$beneficiaryAmount = $this->get_total_amount();
		$totalAmount = $this->get_total_amount();
		$orderID = $this->get_orderID();
		$payerName = $this->get_payer_name();
		$payerEmail = $this->get_payer_email();
		$payerPhone = $this->get_payer_phone();
		$description = $this->get_payment_descr();
		$responseurl = $this->get_responseurl();

		$hash_string = $this->MERCHANTID . $this->SERVICETYPEID . $orderID . $totalAmount . $responseurl . $this->APIKEY;
		$hash = hash('sha512', $hash_string);

		$itemid="itemid1".$orderID;
		$cloudskul = $this->get_beneficiary_cloud();
		$beneficiaryName=$cloudskul->account_name;//cloudskul account name

		//$beneficiaryName="Oshadami Mke";
		$beneficiaryAccount=$cloudskul->account_number;//cloudskul account number

		//$beneficiaryAccount="3455665434";
		$bankCode=$cloudskul->bank_code;//cloudskul bank code
		$deductFeeFrom=1;

		$content = '{"merchantId":"'. $this->MERCHANTID
		.'"'.',"serviceTypeId":"'.$this->SERVICETYPEID
		.'"'.",".'"totalAmount":"'.$totalAmount
		.'","hash":"'. $hash
		.'","description":"'. $description
		.'"'.',"orderId":"'.$orderID
		.'"'.",".'"responseurl":"'.$responseurl
		.'","payerName":"'. $payerName
		.'"'.',"payerEmail":"'.$payerEmail
		.'"'.",".'"payerPhone":"'.$payerPhone
		.'","lineItems":[
		{"lineItemsId":"'.$itemid.'","beneficiaryName":"'.$beneficiaryName.'","beneficiaryAccount":"'.$beneficiaryAccount.'","bankCode":"'.$bankCode.'","beneficiaryAmount":"'.$beneficiaryAmount.'","deductFeeFrom":"'.$deductFeeFrom.'"}
		]}';

		//var_dump($content);

		return $content;
	}


	 public function get_content(){
		
		$beneficiaryAmount = $this->get_cloudskul_fee();//cloudskul service charge
		//$beneficiaryAmount = "5000";//cloudskul service charge
		$beneficiaryAmount2 = $this->get_school_fee();//school fees
		$totalAmount = intval($this->get_total_amount());// + intval($beneficiaryAmount);
		//$totalAmount = (string)$totalAmount;
		
		//$totalAmount = "7500";
		//$timesammp=DATE("dmyHis");		
		$orderID = $this->get_orderID();
		$payerName = $this->get_payer_name();
		$payerEmail = $this->get_payer_email();
		$payerPhone = $this->get_payer_phone();
		$description = $this->get_payment_descr();
		//$responseurl = "http://localhost/remita/sample-receipt-page.php";
		$responseurl = $this->get_responseurl();
		$hash_string = $this->MERCHANTID . $this->SERVICETYPEID . $orderID . $totalAmount . $responseurl . $this->APIKEY;
		$hash = hash('sha512', $hash_string);
		//$itemtimestamp = $timesammp;
		$itemid="1".$orderID;
		$itemid2="2".$orderID;
		$cloudskul = $this->get_beneficiary_cloud();
		$school_account = $this->get_beneficiary();


		$beneficiaryName=$cloudskul->account_name;//cloudskul account name
		$beneficiaryName2=$school_account->account_name;//school account name

		$beneficiaryAccount=$cloudskul->account_number;//cloudskul account number
		$beneficiaryAccount2=$school_account->account_number;//school account number

		$bankCode=$cloudskul->bank_code;//cloudskul bank code
		$bankCode2=$school_account->bank_code;//school bank code

		$deductFeeFrom=1;
		$deductFeeFrom2=0;
		//The JSON data.
		$content = '{"merchantId":"'. $this->MERCHANTID
		.'"'.',"serviceTypeId":"'.$this->SERVICETYPEID
		.'"'.",".'"totalAmount":"'.$totalAmount
		.'","hash":"'. $hash
		.'","description":"'. $description
		.'"'.',"orderId":"'.$orderID
		.'"'.",".'"responseurl":"'.$responseurl
		.'","payerName":"'. $payerName
		.'"'.',"payerEmail":"'.$payerEmail
		.'"'.",".'"payerPhone":"'.$payerPhone
		.'","lineItems":[
		{"lineItemsId":"'.$itemid.'","beneficiaryName":"'.$beneficiaryName.'","beneficiaryAccount":"'.$beneficiaryAccount.'","bankCode":"'.$bankCode.'","beneficiaryAmount":"'.$beneficiaryAmount.'","deductFeeFrom":"'.$deductFeeFrom.'"},
		{"lineItemsId":"'.$itemid2.'","beneficiaryName":"'.$beneficiaryName2.'","beneficiaryAccount":"'.$beneficiaryAccount2.'","bankCode":"'.$bankCode2.'","beneficiaryAmount":"'.$beneficiaryAmount2.'","deductFeeFrom":"'.$deductFeeFrom2.'"}
		]}';
		// var_dump($content); die;

		return $content;
	}

	public function execute_remita(){

		$params = $this->get_content();
		$response = $this->remita_connect($params);
		// var_dump($response); die;
		$type = $this->get_payment_type();

		/*Trial*/
		/*Trail was a success*/
		if($response && $type != 'BANK_BRANCH'){
			$this->remita_response($response);
		}elseif($response && $type == 'BANK_BRANCH'){
			return $this->remita_response_bank($response);
		}else{
			return false;
		}

	}

	public function execute_donation(){

		$params = $this->get_donation_content();
		$response = $this->remita_connect($params);
		$output = $this->remita_response($response);
		//var_dump($output);
		if(!$output){
			//echo "Failed Terribly";
			redirect(base_url("donation/payment_fail"));
			//return false;
		}else
		return $output;
		//var_dump($params);
	}

	private function remita_response_bank($res){
		$jsonData = substr($res, 6, -1);
		$response = json_decode($jsonData, true);
		//return trim($response['RRR']);
		return $response;
	}

	private function remita_response($remit){

		//var_dump($remit);
		$jsonData = substr($remit, 6, -1);
		//var_dump($jsonData);
		$response = json_decode($jsonData, true);
		// var_dump($response);
		// die;
		$statuscode = $response['statuscode'];
		$statusMsg = $response['status'];
		//$statuscode = '025';
		if($statuscode == '025' || $statuscode == '055'){
		$rrr = trim($response['RRR']);
		// $query = $this->remita_transaction_details($response['orderID']);
		// var_dump($query);
		// die;
		$new_hash_string = $this->MERCHANTID . $rrr . $this->APIKEY;
		$new_hash = hash('sha512', $new_hash_string);
		echo '<html>
		<head>
		<link rel="stylesheet" href="<?php echo base_url(\'assets/theme/plugins/bootstrap/css/bootstrap.min.css\'); ?>">
		<link rel="stylesheet" href="css/bootstrap-dark.min.css">
		</head>
		<body>
		  <div class="container">
			<div class="row">
		    <div class="col-xs-12 col-md-9 col-lg-7">
		    <h4 style="text-center margin-left:20px;">Redirecting to Remita... Please wait!</h4>"

		<form action="'.$this->GATEWAYRRRPAYMENTURL.'" method="POST">
		<input id="merchantId" name="merchantId" value="'.$this->MERCHANTID.'" type="hidden"/>
		<input id="rrr" name="rrr" value="'.$rrr.'" type="hidden"/>
		<input id="paymenttype" name="paymenttype" value="'.$this->get_payment_type().'" type="hidden"/>
		<input id="responseurl" name="responseurl" value="'.$this->get_responseurl().'" type="hidden"/>
		<input id="hash" name="hash" value="'.$new_hash.'" type="hidden"/>
		<div class="form-group">
			<!--label class="col-sm-4 control-label">Payment Type</label-->
			<div class="col-sm-8">
				<select style="display:none" name="paymenttypee" class="form-control">
					<option value=""> -- Select --</option>
					<option value="REMITA_PAY"> Remita Account Transfer</option>
					<option value="Interswitch"> Verve Card</option>
					<option value="UPL"> Visa</option>
					<option value="UPL"> MasterCard</option>
					<option value="PocketMoni"> PocketMoni</option>
					<option value="RRRGEN"> POS</option>
					<option value="ATM"> ATM</option>
					<option selected value="BANK_BRANCH">BANK BRANCH</option>
					<option value="BANK_INTERNET">BANK INTERNET</option>
				</select>
			</div>
		</div>
		 <div class="form-group">
			<div class="col-sm-8 col-sm-offset-4">
				<!--input type="submit" class="btn btn-sm btn-primary" name="submit" value="Submit" /-->
			</div>
		</div>
			</form>
			<script type="text/javascript">
			var form = document.forms[0];
            form.submit()</script>
		</div>
		</div>
		</div>
		</body>
		</html>';
		}else{
			//echo "Error Generating RRR - " .$statusMsg;
			return false;
		}
		/*

		if($statuscode == '055'){
			$query = $this->remita_transaction_details($response['orderID']);
			 var_dump($query);
			die;
		}
		*/
	}

	public function remita_connect($content){

		$curl = curl_init($this->SPLIT_GATEWAYURL);
		curl_setopt($curl, CURLOPT_HEADER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_HTTPHEADER,
		array("Content-type: application/json"));
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $content);
		$json_response = curl_exec($curl);
		$status = curl_getinfo($curl, CURLINFO_HTTP_CODE);

		//var_dump($status); die; //= 400;
		if($status == 200){
			return $json_response;
		}else{
			return false;
		}

		/*$jsRes = substr($json_response, 6, -1);
		$res = json_decode($jsRes,true);
		//var_dump($status);
		
		$alert = trim($res['statuscode']," ");
		//var_dump($alert);
		curl_close($curl);

		if($alert == "025"){
			if($status == 200)
				return $json_response;
			elseif($status == 404)
				return $status;
		}elseif($alert != "025" || $status == "404"){
			//return 'status='.$status.'&statuscode='.$alert;
			return $json_response;
		}*/

	}

	public function remita_status(){

		$curl = curl_init($this->SPLIT_GATEWAYURL);
		curl_setopt($curl, CURLOPT_HEADER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_HTTPHEADER,
		array("Content-type: application/json"));
		curl_setopt($curl, CURLOPT_POST, true);
		//curl_setopt($curl, CURLOPT_POSTFIELDS, $content);
		$json_response = curl_exec($curl);
		$status = curl_getinfo($curl, CURLINFO_HTTP_CODE);

		//return $status;

		$jsRes = substr($json_response, 6, -1);
		$res = json_decode($jsRes,true);
		//var_dump($status);
		
		$alert = trim($res['statuscode']," ");
		//var_dump($alert);
		curl_close($curl);

		if($alert == "025"){
			if($status == 200)
				return $json_response;
			elseif($status == 404)
				return $status;
		}elseif($alert != "025" || $status == "404"){
			//return 'status='.$status.'&statuscode='.$alert;
			return $json_response;
		}

	}

	//This functions confirms that the transaction was succesful
	public function remita_transaction_details($orderId){

		$concatString = $orderId . $this->APIKEY . $this->MERCHANTID;
		$hash = hash('sha512', $concatString);
		$url 	= $this->SPLIT_CHECKSTATUSURL . '/' . $this->MERCHANTID  . '/' . $orderId . '/' . $hash . '/' . 'orderstatus.reg';
		//  Initiate curl
		$ch = curl_init();
		// Disable SSL verification
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		// Will return the response, if false it print the response
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		// Set the url
		curl_setopt($ch, CURLOPT_URL,$url);
		// Execute
		$result=curl_exec($ch);
		// Closing
		curl_close($ch);
		$response = json_decode($result, true);
		return $response;
	}

	

}

/* End of file Remita.php */
