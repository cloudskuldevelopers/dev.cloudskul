<?php

/* List Language  */
$lang['panel_title'] = "School";
$lang['add_title'] = "Add a school";
$lang['slno'] = "#";
$lang['school_school'] = "School";
$lang['school_alias'] = "Alias";
$lang['school_email'] = "Email";
$lang['school_phone'] = "Phone";
$lang['school_capacity'] = "Capacity";
$lang['school_automation'] = "Recurrence Interval";
$lang['school_currency_code'] = "Currency Code";
$lang['school_currency_symbol'] = "Currency Symbol";
$lang['school_footer'] = "Footer";
$lang['school_logo'] = "Logo";
$lang['school_address'] = "Address";
$lang['school_status'] = "Status";
$lang['upload_setting'] = "Upload";
$lang['school_create_by'] = "Created By";
$lang['school_create_date'] = "Created On";
$lang['school_allschool'] = "All School";

$lang['action'] = "Action";
$lang['view'] = 'View';
$lang['edit'] = 'Edit';
$lang['delete'] = 'Delete';
$lang['pdf_preview'] = 'PDF Preview';
$lang['print'] = 'Print';
$lang["mail"] = "Send Pdf to Mail";

$lang['school_information'] = 'School Information';

/* Add Language */

$lang['add_school'] = 'Add School';
$lang['update_school'] = 'Update School';
$lang['school_error'] = "You have just one school. You can't delete it.";

$lang['to'] = 'To';
$lang['subject'] = 'Subject';
$lang['message'] = 'Message';
$lang['send'] = 'Send';
$lang['mail_to'] = "The To field is required.";
$lang['mail_valid'] = "The To field must contain a valid email address.";
$lang['mail_subject'] = "The Subject field is required.";
$lang['mail_success'] = 'Email send successfully!';
$lang['mail_error'] = 'oops! Email not send!';