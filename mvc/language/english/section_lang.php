<?php

/* List Language  */
$lang['panel_title'] = "Class Arm";
$lang['add_title'] = "Add a Class Arm";
$lang['slno'] = "#";
$lang['section_name'] = "Class Arm";
$lang['section_category'] = "Category";
$lang['section_classes'] = "Class";
$lang['section_teacher_name'] = "Teacher Name";
$lang['section_note'] = "Note";
$lang['action'] = "Action";

$lang['section_select_class'] = "Select Class";
$lang['section_select_teacher'] = "Select Teacher";

$lang['view'] = 'View';
$lang['edit'] = 'Edit';
$lang['delete'] = 'Delete';

/* Add Language */

$lang['add_class'] = 'Add Class Arm';
$lang['update_class'] = 'Update Class Arm';