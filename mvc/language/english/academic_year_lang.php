<?php


/* List Language  */
$lang['panel_title'] = "Academic Years";
$lang['add_title'] = "Add an Academic year";
$lang['slno'] = "#";
$lang['year_title'] = "Year Title";
$lang['year_status'] = "Status";

$lang['action'] = "Action";
$lang['view'] = 'View';
$lang['edit'] = 'Edit';
$lang['delete'] = 'Delete';

/* Add Language */

$lang['add_year'] = 'Add Academic Year';
$lang['update_year'] = 'Update Academic Year';