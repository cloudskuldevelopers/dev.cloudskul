<?php

/* List Language  */
$lang['panel_title'] = "Assignment";
$lang['add_title'] = "Add an Assignment";
$lang['slno'] = "#";
$lang['assignment_title'] = "Title";
$lang['assignment_assignment'] = "Assignment";
$lang['description'] = "Description";
$lang['section'] = "Class Arm";
$lang['classes'] = "Class";
$lang['created_by'] = "Created By";
$lang['assignment_doc'] = "Assignment Document";
$lang['note'] = "Note";
$lang['attachment_submit_date'] = "Submission Date";
$lang['student_select_section'] = 'Select Class Arm';
$lang['select_class'] = 'Select Class';
$lang['subject'] = 'Subject';
$lang['select_subject'] = 'Select Subject';
$lang['assignment_end_date'] = "Due Date";
$lang['action'] = "Action";

$lang['view'] = 'View';
$lang['edit'] = 'Edit';
$lang['delete'] = 'Delete';
$lang['print'] = 'Print';
$lang['pdf_preview'] = 'PDF Preview';
$lang["mail"] = "Send Pdf to Mail";

/* Add Language */

$lang['add_class'] = 'Add Assignment';
$lang['update_class'] = 'Update Assignment';
$lang['successfull_msg'] = 'Assignment created successfully!';

$lang['to'] = 'To';
$lang['subject'] = 'Subject';
$lang['message'] = 'Message';
$lang['send'] = 'Send';
$lang['mail_to'] = "The To field is required.";
$lang['mail_valid'] = "The To field must contain a valid email address.";
$lang['mail_subject'] = "The Subject field is required.";
$lang['mail_success'] = 'Email send successfully!';
$lang['mail_error'] = 'oops! Email not send!';