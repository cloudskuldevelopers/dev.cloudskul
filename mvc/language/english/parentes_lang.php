<?php
/* List Language  */
$lang['panel_title'] = "Parents";
$lang['add_title'] = "Add a parents";
$lang['slno'] = "#";
$lang['parentes_photo'] = "Photo";
$lang['parentes_name'] = "Name";
$lang['parentes_email'] = "Email";
$lang['parentes_status'] = "Status";
$lang['parentes_dob'] = "Date of Birth";
$lang['parentes_sex'] = "Sex";
$lang['parentes_religion'] = "Religion";
$lang['parentes_phone'] = "Phone";
$lang['parentes_address'] = "Address";
$lang['parentes_classes'] = "Class";
$lang['parentes_roll'] = "Roll";
$lang['parentes_photo'] = "Photo";
$lang['parentes_username'] = "Username";
$lang['parentes_password'] = "Password";
$lang['parentes_select_class'] = "Select Class";

/* Parentes */
$lang['parentes_guargian_name'] = "Guardian Name";
$lang['parentes_father_name'] = "Father's Name";
$lang['parentes_mother_name'] = "Mother's Name";
$lang['parentes_father_profession'] = "Father's Profession";
$lang['parentes_mother_profession'] = "Mother's Profession";


$lang['action'] = "Action";
$lang['view'] = 'View';
$lang['edit'] = 'Edit';
$lang['delete'] = 'Delete';
$lang['pdf_preview'] = 'PDF Preview';
$lang['print'] = 'Print';
$lang["mail"] = "Send Pdf to Mail";

$lang['to'] = 'To';
$lang['subject'] = 'Subject';
$lang['message'] = 'Message';
$lang['send'] = 'Send';
$lang['mail_to'] = "The To field is required.";
$lang['mail_valid'] = "The To field must contain a valid email address.";
$lang['mail_subject'] = "The Subject field is required.";
$lang['mail_success'] = 'Email send successfully!';
$lang['mail_error'] = 'oops! Email not sent!';

/* Add Language */

$lang['add_parentes'] = 'Add Parents';
$lang['update_parentes'] = 'Update Parents';

/* ini code starts here*/
$lang['personal_information'] = "Personal Information";
$lang['parentess_information'] = "Parents Information";
$lang['parents_pickup_policy'] = "Drop Off/Pick Up Policy";
$lang['add_individual'] = "Add an Individual";
$lang['pickup_last_name'] = "Last Name";
$lang['pickup_first_name'] = "First Name";
$lang['pickup_email'] = "Email";
$lang['pickup_phone'] = "Phone";
$lang['pickup_address'] = "Address";
$lang['pickup_profession'] = "Profession";
$lang['childcare_classes'] = "Class";
$lang['childcare_select_class'] = "Select class";
$lang['childcare_all_students'] = "All Students";
$lang['student_section'] = "Class Arm";
$lang['child_care_student_name'] = "Student Name";
$lang['child_care_parent_name'] = "Parents Name(s)";
$lang['child_care_authorized'] = "Authorized Adult";