<?php

/* List Language  */
$lang['panel_title'] = "Assessments";
$lang['add_title'] = "Add an Assessment";
$lang['slno'] = "#";
$lang['assessments_name'] = "Assessment Name";
$lang['assessments_date'] = "Date";
$lang['assessments_note'] = "Note";

$lang['action'] = "Action";
$lang['view'] = 'View';
$lang['edit'] = 'Edit';
$lang['delete'] = 'Delete';

/* Add Language */

$lang['add_assessments'] = 'Add Assessment';
$lang['update_assessments'] = 'Update Assessment';