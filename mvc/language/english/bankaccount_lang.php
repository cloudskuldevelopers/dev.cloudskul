<?php

/* List Language  */
$lang['panel_title'] = "Bank Account";
$lang['add_title'] = "Add a Bank Account";
$lang['slno'] = "#";
$lang['account_name'] = "Account Name";
$lang['bank_name'] = "Bank Name";
$lang['account_number'] = "Account Number";
$lang['account_type'] = "Account Type";
$lang['school_name'] = "School Name";
$lang['bank_code'] = "Bank Code";
$lang['school_capacity'] = "Capacity";
$lang['school_automation'] = "Automation";
$lang['school_currency_code'] = "Currency Code";
$lang['school_currency_symbol'] = "Currency Symbol";
$lang['school_footer'] = "Footer";
$lang['school_logo'] = "Logo";
$lang['school_address'] = "Address";
$lang['school_status'] = "Status";
$lang['upload_setting'] = "Upload";
$lang['school_create_by'] = "Create By";
$lang['school_create_date'] = "Create Date";
$lang['school_allschool'] = "All School";

$lang['action'] = "Action";
$lang['view'] = 'View';
$lang['edit'] = 'Edit';
$lang['delete'] = 'Delete';
$lang['pdf_preview'] = 'PDF Preview';
$lang['print'] = 'Print';
$lang["mail"] = "Send Pdf to Mail";

$lang['school_information'] = 'School Information';

/* Add Language */

$lang['add_school'] = 'Add School';
$lang['update_bank'] = 'Update Bank Details';
$lang['school_error'] = "You have just one school. You can't delete it.";

$lang['to'] = 'To';
$lang['subject'] = 'Subject';
$lang['message'] = 'Message';
$lang['send'] = 'Send';
$lang['mail_to'] = "The To field is required.";
$lang['mail_valid'] = "The To field must contain a valid email address.";
$lang['mail_subject'] = "The Subject field is required.";
$lang['mail_success'] = 'Email send successfully!';
$lang['mail_error'] = 'oops! Email not send!';