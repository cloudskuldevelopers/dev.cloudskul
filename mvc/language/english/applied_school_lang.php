<?php

/* List Language  */
$lang['panel_title'] = "School Application";
$lang['slno'] = "#";
$lang['school_school'] = "School";
$lang['school_email'] = "Email";
$lang['school_phone'] = "Phone";
$lang['school_logo'] = "Logo";
$lang['school_address'] = "Address";
$lang['school_enroll'] = "Enroll School";
$lang['applicant_name'] = "Applicant Name";
$lang['applicant_dob'] = "Applicant Date Of Birth";
$lang['applicant_sex'] = "Applicant Gender";
$lang['applicant_username'] = "Applicant Username";
$lang['create_date'] = "Date of Application";
$lang['add_success'] = "School successfully enrolled!";
$lang['add_error'] = "School not enrolled!";
$lang['delete_success'] = "School successfully deleted!";
$lang['delete_error'] = "School not deleted!";

$lang['action'] = "Action";
$lang['view'] = 'View';
$lang['edit'] = 'Edit';
$lang['delete'] = 'Delete';
$lang['pdf_preview'] = 'PDF Preview';
$lang['print'] = 'Print';
$lang["mail"] = "Send Pdf to Mail";

$lang['school_information'] = 'Applied School Information';

/* Add Language */
$lang['update_school'] = 'Update School';
$lang['school_error'] = "You have just one school. You can't delete it.";

$lang['to'] = 'To';
$lang['subject'] = 'Subject';
$lang['message'] = 'Message';
$lang['send'] = 'Send';
$lang['mail_to'] = "The To field is required.";
$lang['mail_valid'] = "The To field must contain a valid email address.";
$lang['mail_subject'] = "The Subject field is required.";
$lang['mail_success'] = 'Email send successfully!';
$lang['mail_error'] = 'oops! Email not send!';