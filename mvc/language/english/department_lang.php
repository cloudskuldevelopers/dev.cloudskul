<?php

/* List Language  */
$lang['select_department'] = "Select Department";
$lang['panel_title'] = "Department";
$lang['slno'] = "#";
$lang['details'] = "View Details";
$lang['action'] = "Action";
$lang['department_name'] = "Department Name";

$lang['view'] = 'View';
$lang['edit'] = 'Edit';
$lang['delete'] = 'Delete';