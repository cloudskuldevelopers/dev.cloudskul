<?php

/* List Language  */
$lang['panel_title'] = "Invoice";
$lang['add_title'] = "Add a invoice";
$lang['slno'] = "#";
$lang['invoice_classesID'] = "Class";
$lang['invoice_studentID'] = "Student";
$lang['invoice_select_classes'] = "Select Class";
$lang['invoice_select_account'] = "Select Account";
$lang['invoice_select_student'] = "Select Student";
$lang['invoice_select_paymentmethod'] = "Select Payment Method";
$lang['invoice_amount'] = "Amount";
$lang['invoice_student'] = "Student";
$lang['invoice_date'] = "Date";
$lang['invoice_feetype'] = "Fee Type";
$lang['invoice_invoice'] = "Invoice #";
$lang['invoice_pdate'] = "Payment Date";
$lang['invoice_total'] = "Total";
$lang['invoice_from'] = "From";
$lang['invoice_to'] = "To";
$lang['invoice_create_date'] = "Created On";
$lang['invoice_subtotal'] = "Sub Total";
$lang['invoice_sremove'] = "Student Remove";
$lang['invoice_roll'] = "Roll";
$lang['invoice_phone'] = "Phone";
$lang['invoice_email'] = "Email";
$lang['invoice_status'] = "Payment Status";
$lang['invoice_notpaid'] = "Not Paid";
$lang['invoice_partially_paid'] = "Partially Paid";
$lang['invoice_fully_paid'] = "Fully Paid";
$lang['invoice_paymentmethod'] = 'Payment Method';
$lang['installments'] = "Installments";
$lang['fee_cat'] = "Fee Category";

$lang['invoice_cash'] = "Cash";
$lang['invoice_cheque'] = "Cheque";
$lang['invoice_paypal'] = "Paypal";
$lang['invoice_stripe'] = "Stripe";


$lang['to'] = 'To';
$lang['subject'] = 'Subject';
$lang['message'] = 'Message';
$lang['send'] = 'Send';
$lang['mail_to'] = "The To field is required.";
$lang['mail_valid'] = "The To field must contain a valid email address.";
$lang['mail_subject'] = "The Subject field is required.";
$lang['mail_success'] = 'Email send successfully!';
$lang['mail_error'] = 'oops! Email not send!';


$lang['pdf_preview'] = 'Reciept PDF Preview';
$lang['print'] = 'Print Receipt';
$lang["mail"] = "Send Receipt to Mail";
$lang['payment'] = 'Make Payment';
$lang['invoice_due'] = "Due Amount";
$lang['invoice_made'] = "Payment Made";
$lang['invoice_account'] = "Bank Account";

$lang['view'] = "View";
$lang['add_invoice'] = 'Add Invoice';
$lang['update_invoice'] = 'Update Invoice';
$lang['add_payment'] = 'Add Payment';
$lang['action'] = 'Action';
$lang['edit'] = 'Edit';
$lang['delete'] = 'Delete';
$lang['invoice_instalment_no'] = 'Instalment Plan';
$lang['payment_plan'] = 'Payment Plan';
$lang['instalment_plan_one'] = 'One-time Payment';
$lang['instalment_plan_two'] = 'Two-time Payment';
$lang['instalment_plan_three'] = 'Three-time Payment';
$lang['invoice_ref'] = 'Invoice No';
$lang['invoice_1st'] = '1st Installment';
$lang['invoice_2nd'] = '2nd Installment';
$lang['invoice_3rd'] = '3rd Installment';
$lang['installment_total'] = 'Total Amount';
$lang['installment'] = 'Installment';
$lang['paid'] = 'Paid';
