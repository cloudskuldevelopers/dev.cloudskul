<?php

/* List Language  */
$lang['panel_title'] = "Mark";
$lang['add_title'] = 'Add a mark';
$lang['slno'] = "#";
$lang['mark_exam'] = "Exam";
$lang['mark_assessment'] = "Assessment";
$lang['mark_classes'] = "Class";
$lang['mark_student'] = "Student";
$lang['mark_subject'] = "Subject";
$lang['mark_photo'] = "Photo";
$lang['mark_name'] = "Name";
$lang['mark_roll'] = "Roll";
$lang['mark_phone'] = "Phone";
$lang['mark_dob'] = "Date of Birth";
$lang['mark_sex'] = "Gender";
$lang['mark_religion'] = "Religion";
$lang['mark_email'] = "Email";
$lang['mark_address'] = "Address";
$lang['mark_username'] = "Username";

$lang['mark_subject'] = "Subject";
$lang['mark_mark'] = "Mark";
$lang['mark_point'] = "Point";
$lang['mark_grade'] = "Grade";
$lang['add_mark_exam'] = "Add Exam Mark";
$lang['add_mark_assessment'] = "Add Assessment Mark";
$lang['mark_an_assessment'] = "Mark Assessment";
$lang['mark_an_exam'] = "Mark Exam";


$lang['mark_select_classes'] = "Select Class";
$lang['mark_select_exam'] = "Select Exam";
$lang['mark_select_assessment'] = "Select Assessment";
$lang['mark_select_subject'] = "Select Subject";
$lang['mark_select_student'] = "Select Student";
$lang['mark_success'] = "Success";
$lang['personal_information'] = "Personal Information";
$lang['mark_information'] = "Mark Information";
$lang['action'] = "Action";
$lang['view'] = 'View';
$lang['view_exam'] = 'View Exam';
$lang['view_assessment'] = 'View Assessment';
$lang['view_all'] = 'View All';
$lang['pdf_preview'] = 'PDF Preview';
$lang['print'] = 'Print';
$lang["mail"] = "Send Pdf to Mail";

// /* Add Language */
$lang['add_mark'] = 'Mark';
$lang['add_sub_mark'] = 'Add Mark';

$lang['to'] = 'To';
$lang['subject'] = 'Subject';
$lang['message'] = 'Message';
$lang['send'] = 'Send';
$lang['mail_to'] = "The To field is required.";
$lang['mail_valid'] = "The To field must contain a valid email address.";
$lang['mail_subject'] = "The Subject field is required.";
$lang['mail_success'] = 'Email send successfully!';
$lang['mail_error'] = 'oops! Email not send!';