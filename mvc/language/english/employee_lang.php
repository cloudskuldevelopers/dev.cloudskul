<?php

/* List Language  */
$lang['panel_title'] = "Employee";
$lang['add_title'] = "Add Employee";
$lang['slno'] = "#";
$lang['employeeID'] = "Employee ID";
$lang['employee_name'] = "Employee Name";
$lang['dept_desig'] = "Dept > Designation";
$lang['mobile'] = "Mobile";
$lang['status'] = "Status";
$lang['details'] = "View Details";
$lang['action'] = "Action";