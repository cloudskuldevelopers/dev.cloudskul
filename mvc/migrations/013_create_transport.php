<?php

class Migration_create_transport extends CI_Migration {

	public function up()
	{
		$this->dbforge->add_field(array(
			'transportID' => array(
				'type' => 'INT',
				'constraint' => 11,
				'unsigned' => TRUE,
				'auto_increment' => TRUE
			),
			'schoolID' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => FALSE
			),
			'route' => array(
				'type' => 'TEXT',
				'constraint' => '128',
				'null' => FALSE
			),
			'vehicle' => array(
				'type' => 'INT',
				'constraint' => '11',
				'null' => FALSE
			),
			'fare' => array(
				'type' => 'VARCHAR',
				'constraint' => '11',
				'null' => FALSE
			),
			'note' => array(
				'type' => 'text',
				'constraint' => '200',
				'null' => TRUE
			),
			'create_date' => array(
				'type' => 'DATETIME',
				'null' => FALSE
			),
			'modify_date' => array(
				'type' => 'DATETIME',
				'null' => FALSE
			),
			'create_userID' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => FALSE
			),
			'create_username' => array(
				'type' => 'VARCHAR',
				'constraint' => 40,
				'null' => FALSE
			),
			'create_usertype' => array(
				'type' => 'VARCHAR',
				'constraint' => 20,
				'null' => FALSE
			),
			'transportextra1' => array(
				'type' => 'VARCHAR',
				'constraint' => 128,
				'null' => TRUE
			),
			'transportextra2' => array(
				'type' => 'VARCHAR',
				'constraint' => 128,
				'null' => TRUE
			)
		));
		$this->dbforge->add_key('transportID', TRUE);
		$this->dbforge->create_table('transport');
	}

	public function down()
	{
		$this->dbforge->drop_table('transport');
	}
}