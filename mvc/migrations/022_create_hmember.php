<?php

class Migration_create_hmember extends CI_Migration {

	public function up()
	{
		$this->dbforge->add_field(array(
			'hmemberID' => array(
				'type' => 'INT',
				'constraint' => 11,
				'unsigned' => TRUE,
				'auto_increment' => TRUE
			),
			'schoolID' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => FALSE
			),
			'hostelID' => array(
				'type' => 'INT',
				'constraint' => 11,
				"null" => FAlSE
			),
			'categoryID' => array(
				'type' => 'INT',
				'constraint' => '11',
				'null' => FAlSE
			),
			'studentID' => array(
				'type' => 'INT',
				'constraint' => '11',
				'null' => FAlSE
			),
			'hbalance' => array(
				'type' => 'VARCHAR',
				'constraint' => '20',
				'null' => TRUE
			),
			'hjoindate' => array(
				'type' => 'DATE',
				'null' => FAlSE
			),
			'create_date' => array(
				'type' => 'DATETIME',
				'null' => FALSE
			),
			'modify_date' => array(
				'type' => 'DATETIME',
				'null' => FALSE
			),
			'create_userID' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => FALSE
			),
			'create_username' => array(
				'type' => 'VARCHAR',
				'constraint' => 40,
				'null' => FALSE
			),
			'create_usertype' => array(
				'type' => 'VARCHAR',
				'constraint' => 20,
				'null' => FALSE
			),
			'hmemberextra1' => array(
				'type' => 'VARCHAR',
				'constraint' => 128,
				'null' => TRUE
			),
			'hmemberextra2' => array(
				'type' => 'VARCHAR',
				'constraint' => 128,
				'null' => TRUE
			)
		));
		$this->dbforge->add_key('hmemberID', TRUE);
		$this->dbforge->create_table('hmember');
	}

	public function down()
	{
		$this->dbforge->drop_table('hmember');
	}
}