<?php

class Migration_create_expense extends CI_Migration {

	public function up()
	{
		$this->dbforge->add_field(array(
			'expenseID' => array(
				'type' => 'INT',
				'constraint' => 11,
				'unsigned' => TRUE,
				'auto_increment' => TRUE
			),
			'schoolID' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => FALSE
			),
			'expense' => array(
				'type' => 'VARCHAR',
				'constraint' => 128,
				'null' => FAlSE
			),
			'amount' => array(
				'type' => 'VARCHAR',
				'constraint' => 11,
				'null' => FAlSE
			),
			'date' => array(
				'type' => 'DATE',
				'null' => FAlSE
			),
			'expenseyear' => array(
				'type' => 'YEAR',
				'null' => FAlSE 
			),
			'note' => array(
				'type' => 'text',
				'constraint' => 200,
				'null' => TRUE
			),
			'uname' => array(
				'type' => 'VARCHAR',
				'constraint' => 60,
				'null' => FAlSE
			),
			'create_date' => array(
				'type' => 'DATETIME',
				'null' => FALSE
			),
			'modify_date' => array(
				'type' => 'DATETIME',
				'null' => FALSE
			),
			'create_userID' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => FALSE
			),
			'create_username' => array(
				'type' => 'VARCHAR',
				'constraint' => 40,
				'null' => FALSE
			),
			'create_usertype' => array(
				'type' => 'VARCHAR',
				'constraint' => 20,
				'null' => FALSE
			),
			'expenseextra1' => array(
				'type' => 'VARCHAR',
				'constraint' => 128,
				'null' => TRUE
			),
			'expenseextra2' => array(
				'type' => 'VARCHAR',
				'constraint' => 128,
				'null' => TRUE
			)
		));
		$this->dbforge->add_key('expenseID', TRUE);
		$this->dbforge->create_table('expense');
	}

	public function down()
	{
		$this->dbforge->drop_table('expense');
	}
}