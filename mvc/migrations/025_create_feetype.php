<?php

class Migration_create_feetype extends CI_Migration {

	public function up()
	{
		$this->dbforge->add_field(array(
			'feetypeID' => array(
				'type' => 'INT',
				'constraint' => 11,
				'unsigned' => TRUE,
				'auto_increment' => TRUE
			),
			'schoolID' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => FALSE
			),
			'feetype' => array(
				'type' => 'VARCHAR',
				'constraint' => 60,
				'null' => FAlSE
			),
			'note' => array(
				'type' => 'TEXT',
				'constraint' => 200,
				'null' => TRUE
			),
			'create_date' => array(
				'type' => 'DATETIME',
				'null' => FALSE
			),
			'modify_date' => array(
				'type' => 'DATETIME',
				'null' => FALSE
			),
			'create_userID' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => FALSE
			),
			'create_username' => array(
				'type' => 'VARCHAR',
				'constraint' => 40,
				'null' => FALSE
			),
			'create_usertype' => array(
				'type' => 'VARCHAR',
				'constraint' => 20,
				'null' => FALSE
			),
			'feetypeextra1' => array(
				'type' => 'VARCHAR',
				'constraint' => 128,
				'null' => TRUE
			),
			'feetypeextra2' => array(
				'type' => 'VARCHAR',
				'constraint' => 128,
				'null' => TRUE
			)
		));
		$this->dbforge->add_key('feetypeID', TRUE);
		$this->dbforge->create_table('feetype');
	}

	public function down()
	{
		$this->dbforge->drop_table('feetype');
	}
}