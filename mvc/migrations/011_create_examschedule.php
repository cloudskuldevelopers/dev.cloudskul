<?php

class Migration_create_examschedule extends CI_Migration {

	public function up()
	{
		$this->dbforge->add_field(array(
			'examscheduleID' => array(
				'type' => 'INT',
				'constraint' => 11,
				'unsigned' => TRUE,
				'auto_increment' => TRUE
			),
			'schoolID' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => FALSE
			),
			'examID' => array(
				'type' => 'INT',
				'constraint' => '11',
				'null' => FALSE
			),
			'classesID' => array(
				'type' => 'INT',
				'constraint' => '11',
				'null' => FALSE
			),
			'sectionID' => array(
				'type' => 'INT',
				'constraint' => '11',
				'null' => FALSE
			),
			'subjectID' => array(
				'type' => 'INT',
				'constraint' => '11',
				'null' => FALSE
			),
			'edate' => array(
				'type' => 'DATE',
				'null' => FALSE
			),
			'examfrom' => array(
				'type' => 'VARCHAR',
				'constraint' => '10',
				'null' => FALSE
			),
			'examto' => array(
				'type' => 'VARCHAR',
				'constraint' => '10',
				'null' => FALSE
			),
			'room' => array(
				'type' => 'TEXT',
				'constraint' => '10',
				'null' => TRUE
			), 
			'year' => array(
				'type' => 'YEAR',
				'null' => FALSE
			),
			'create_date' => array(
				'type' => 'DATETIME',
				'null' => FALSE
			),
			'modify_date' => array(
				'type' => 'DATETIME',
				'null' => FALSE
			),
			'create_userID' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => FALSE
			),
			'create_username' => array(
				'type' => 'VARCHAR',
				'constraint' => 40,
				'null' => FALSE
			),
			'create_usertype' => array(
				'type' => 'VARCHAR',
				'constraint' => 20,
				'null' => FALSE
			),
			'examscheduleextra1' => array(
				'type' => 'VARCHAR',
				'constraint' => 128,
				'null' => TRUE
			),
			'examscheduleextra2' => array(
				'type' => 'VARCHAR',
				'constraint' => 128,
				'null' => TRUE
			)
		));
		$this->dbforge->add_key('examscheduleID', TRUE);
		$this->dbforge->create_table('examschedule');
	}

	public function down()
	{
		$this->dbforge->drop_table('examschedule');
	}
}