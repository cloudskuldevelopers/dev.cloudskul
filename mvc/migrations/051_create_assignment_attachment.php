<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Migration_Create_assignment_attachment extends CI_Migration {

	public function up()
	{
		$this->dbforge->add_field(array(
			'assignment_attachmentID' => array(
				'type' => 'INT',
				'constraint' => 11,
				'unsigned' => TRUE,
				'auto_increment' => TRUE
			),
			'schoolID' => array(
				'type' => 'TEXT',
				'constraint' => 200,
				'null' => FALSE
			),
			'assignmentID' => array(
				'type' => 'INT',
				'constraint' => 11,
				'unsigned' => TRUE,
			),
			'attachment' => array(
				'type' => 'VARCHAR',
				'constraint' => '255',
				'null' => TRUE
			),			
			'create_date' => array(
				'type' => 'DATETIME',
				'null' => FALSE
			),
			'modify_date' => array(
				'type' => 'DATETIME',
				'null' => FALSE
			),
			'create_userID' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => FALSE
			),
			'create_username' => array(
				'type' => 'VARCHAR',
				'constraint' => 40,
				'null' => FALSE
			),
			'create_usertype' => array(
				'type' => 'VARCHAR',
				'constraint' => 20,
				'null' => FALSE
			),
			'recevied_status' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => FALSE
			),

		));
		$this->dbforge->add_key('assignment_attachmentID', TRUE);
		$this->dbforge->create_table('assignment_attachment');
	}

	public function down()
	{
		$this->dbforge->drop_table('assignment_attachment');
	}

}

/* End of file 003_create_user.php */
/* Location: .//D/xampp/htdocs/school/mvc/migrations/003_create_user.php */