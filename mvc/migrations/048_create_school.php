<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Migration_Create_school extends CI_Migration {

	public function up()
	{
		$this->dbforge->add_field(array(
			'schoolID' => array(
				'type' => 'INT',
				'constraint' => 11,
				'unsigned' => TRUE,
				'auto_increment' => TRUE
			),
			'school' => array(
				'type' => 'TEXT',
				'constraint' => 200,
				'null' => FALSE
			),
			'alias' => array(
				'type' => 'TEXT',
				'constraint' => 200,
				'null' => TRUE
			),
			'address' => array(
				'type' => 'TEXT',
				'constraint' => 200,
				'null' => TRUE
			),
			'capacity' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => TRUE
			),
			'email' => array(
				'type' => 'VARCHAR',
				'constraint' => '40',
				'null' => TRUE
			),
			'phone' => array(
				'type' => 'TEXT',
				'constraint' => '25',
				'null' => TRUE
			),
			'automation' => array(
				'type' => 'INT',
				'constraint' => '11',
				'null' => TRUE
			),
			'currency_code' => array(
				'type' => 'VARCHAR',
				'constraint' => '11',
				'null' => TRUE
			),
			'currency_symbol' => array(
				'type' => 'TEXT',
				'constraint' => '128',
				'null' => TRUE
			),
			'footer' => array(
				'type' => 'TEXT',
				'constraint' => '200',
				'null' => TRUE
			),
			'photo' => array(
				'type' => 'VARCHAR',
				'constraint' => '200',
				'null' => TRUE
			),
			'create_date' => array(
				'type' => 'DATETIME',
				'null' => FALSE
			),
			'modify_date' => array(
				'type' => 'DATETIME',
				'null' => FALSE
			),
			'create_userID' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => FALSE
			),
			'create_username' => array(
				'type' => 'VARCHAR',
				'constraint' => 40,
				'null' => FALSE
			),
			'create_usertype' => array(
				'type' => 'VARCHAR',
				'constraint' => 20,
				'null' => FALSE
			),
			'schoolactive' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => FALSE
			),
			'schoolextra1' => array(
				'type' => 'VARCHAR',
				'constraint' => 128,
				'null' => TRUE
			),
			'schoolextra2' => array(
				'type' => 'VARCHAR',
				'constraint' => 128,
				'null' => TRUE
			)
		));
		$this->dbforge->add_key('schoolID', TRUE);
		$this->dbforge->create_table('school');
	}

	public function down()
	{
		$this->dbforge->drop_table('school');
	}

}

/* End of file 003_create_user.php */
/* Location: .//D/xampp/htdocs/school/mvc/migrations/003_create_user.php */