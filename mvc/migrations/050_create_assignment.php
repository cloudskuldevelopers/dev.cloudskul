<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Migration_Create_assignment extends CI_Migration {

	public function up()
	{
		$this->dbforge->add_field(array(
			'assignmentID' => array(
				'type' => 'INT',
				'constraint' => 11,
				'unsigned' => TRUE,
				'auto_increment' => TRUE
			),
			'schoolID' => array(
				'type' => 'TEXT',
				'constraint' => 200,
				'null' => FALSE
			),
			'title' => array(
				'type' => 'VARCHAR',
				'constraint' => '255',
				'null' => TRUE
			),
			'description' => array(
				'type' => 'TEXT',
				'null' => TRUE
			),			
			'classID' => array(
				'type' => 'INT',
				'constraint' => '11',
				'unsigned' => TRUE,
				'null' => FALSE
			),
			'TeacherID' => array(
				'type' => 'INT',
				'constraint' => '11',
				'unsigned' => TRUE,
				'null' => TRUE
			),
			'subjectID' => array(
				'type' => 'INT',
				'constraint' => '11',
				'unsigned' => TRUE,
				'null' => FALSE
			),
			'create_date' => array(
				'type' => 'DATETIME',
				'null' => FALSE
			),
			'modify_date' => array(
				'type' => 'DATETIME',
				'null' => FALSE
			),
			'create_userID' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => FALSE
			),
			'create_username' => array(
				'type' => 'VARCHAR',
				'constraint' => 40,
				'null' => FALSE
			),
			'create_usertype' => array(
				'type' => 'VARCHAR',
				'constraint' => 20,
				'null' => FALSE
			),
			'assignmentActive' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => FALSE
			),

		));
		$this->dbforge->add_key('assignmentID', TRUE);
		$this->dbforge->create_table('assignment');
	}

	public function down()
	{
		$this->dbforge->drop_table('assignment');
	}

}

/* End of file 003_create_user.php */
/* Location: .//D/xampp/htdocs/school/mvc/migrations/003_create_user.php */