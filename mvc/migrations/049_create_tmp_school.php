<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Migration_Create_tmp_school extends CI_Migration {

	public function up()
	{
		$this->dbforge->add_field(array(
			'tmp_schoolID' => array(
				'type' => 'INT',
				'constraint' => 11,
				'unsigned' => TRUE,
				'auto_increment' => TRUE
			),
			'school' => array(
				'type' => 'TEXT',
				'constraint' => 200,
				'null' => FALSE
			),
			'email' => array(
				'type' => 'VARCHAR',
				'constraint' => '40',
				'null' => TRUE
			),
			'address' => array(
				'type' => 'TEXT',
				'constraint' => 200,
				'null' => TRUE
			),			
			'phone' => array(
				'type' => 'TEXT',
				'constraint' => '25',
				'null' => TRUE
			),
			'photo' => array(
				'type' => 'VARCHAR',
				'constraint' => '200',
				'null' => TRUE
			),
			'name' => array(
				'type' => 'VARCHAR',
				'constraint' => 128,
				'null' => TRUE
			),
			'dob' => array(
				'type' => 'DATE',
				'null' => TRUE
			),
			'sex' => array(
				'type' => 'VARCHAR',
				'constraint' => '10',
				'null' => TRUE
			),
			'username' => array(
				'type' => 'VARCHAR',
				'constraint' => '40',
				'null' => TRUE
			),
			'password' => array(
				'type' => 'VARCHAR',
				'constraint' => '128',
				'null' => TRUE
			),
			'usertype' => array(
				'type' => 'VARCHAR',
				'constraint' => '20',
				'null' => TRUE
			),
			'create_date' => array(
				'type' => 'DATETIME',
				'null' => TRUE
			),

		));
		$this->dbforge->add_key('tmp_schoolID', TRUE);
		$this->dbforge->create_table('tmp_school');
	}

	public function down()
	{
		$this->dbforge->drop_table('tmp_school');
	}

}

/* End of file 003_create_user.php */
/* Location: .//D/xampp/htdocs/school/mvc/migrations/003_create_user.php */