<div id="container">
<!-- TOP BAR -->
<div class="row">
	<div class="col-md-1 hidden-xs">
		<img src="<?php echo base_url('uploads/images/logo.png'); ?>" style="width: auto; height: 40px; margin-top: -8px;" class="img-thumbnail img-responsive" />
	</div>
	<div class="col-md-6 col-xs-9 text-center">
		<h1 class="page-header"><?php echo $this->session->userdata('school');  ?></h1>
	</div>
	<div class="col-md-5 col-xs-3">
		<!-- begin breadcrumb -->
		<ol class="breadcrumb pull-right">
			<li><a href="<?=base_url("dashboard/index")?>"><i class="fa fa-laptop"></i> <?=$this->lang->line('menu_dashboard')?></a></li>
            <li><a href="<?=base_url("notice/index")?>"><?=$this->lang->line('menu_notice')?></a></li>
            <li class="active"><?=$this->lang->line('menu_edit')?> <?=$this->lang->line('menu_notice')?></li>
		</ol>
		<!-- end breadcrumb -->
	</div>
</div>
<!-- END TOP BAR -->

<!-- begin panel -->
<div class="panel panel-inverse" data-sortable-id="ui-widget-3">
	<div class="panel-heading">
		<h4 class="panel-title"><i class="fa fa-calendar"></i> <?=$this->lang->line('panel_title')?></h4>
	</div>
	
	<div class="panel-body">
		<div class="col-sm-12">
			 <form class="form-horizontal" role="form" method="post">
				<?php 
					if(form_error('title')) 
						echo "<div class='form-group has-error' >";
					else     
						echo "<div class='form-group' >";
				?>
					<label for="title" class="col-sm-1 control-label">
						<?=$this->lang->line("notice_title")?>
					</label>
					<div class="col-sm-4">
						<input type="text" class="form-control" id="title" name="title" value="<?=set_value('title' , $notice->title)?>" >
					</div>
					<span class="col-sm-4 control-label">
						<?php echo form_error('title'); ?>
					</span>
				</div>

				<?php 
					if(form_error('date')) 
						echo "<div class='form-group has-error' >";
					else     
						echo "<div class='form-group' >";
				?>
					<label for="date" class="col-sm-1 control-label">
						<?=$this->lang->line("notice_date")?>
					</label>
					<div class="col-sm-4">
						<input type="text" class="form-control" id="date" name="date" value="<?=set_value('date', date("d-m-Y", strtotime($notice->date)))?>" >
					</div>
					<span class="col-sm-4 control-label">
						<?php echo form_error('date'); ?>
					</span>
				</div>

				<?php 
					if(form_error('notice')) 
						echo "<div class='form-group has-error' >";
					else     
						echo "<div class='form-group' >";
				?>
					<label for="notice" class="col-sm-1 control-label">
						<?=$this->lang->line("notice_notice")?>
					</label>
					<div class="panel-body panel-form col-sm-10">
						<textarea class="textarea form-control" id="wysihtml5" rows="8" name="notice" placeholder="Enter text ..."><?=set_value('notice', $notice->notice)?></textarea>
					</div>
					<span class="col-sm-3 control-label">
						<?php echo form_error('notice'); ?>
					</span>
				</div>

				<div class="form-group">
					<div class="col-sm-offset-1 col-sm-8">
						<input type="submit" class="btn btn-success" value="<?=$this->lang->line("update_class")?>" >
					</div>
				</div>

			</form>
		</div>
	</div>
</div>
<!-- end panel -->

<!-- begin scroll to top btn -->
<a href="javascript:;" class="btn btn-icon btn-circle btn-success btn-scroll-to-top fade" data-click="scroll-top"><i class="fa fa-angle-up"></i></a>
<!-- end scroll to top btn -->

</div>
<!-- end page container -->




<?php /*


<div class="box">
    <div class="box-header">
        <h3 class="box-title"><i class="fa fa-calendar"></i> <?=$this->lang->line('panel_title')?></h3>

       
        <ol class="breadcrumb">
            <li><a href="<?=base_url("dashboard/index")?>"><i class="fa fa-laptop"></i> <?=$this->lang->line('menu_dashboard')?></a></li>
            <li><a href="<?=base_url("notice/index")?>"><?=$this->lang->line('menu_notice')?></a></li>
            <li class="active"><?=$this->lang->line('menu_edit')?> <?=$this->lang->line('menu_notice')?></li>
        </ol>
    </div><!-- /.box-header -->
    <!-- form start -->
    <div class="box-body">
        <div class="row">
            <div class="col-sm-12">
                <form class="form-horizontal" role="form" method="post">
                    <?php 
                        if(form_error('title')) 
                            echo "<div class='form-group has-error' >";
                        else     
                            echo "<div class='form-group' >";
                    ?>
                        <label for="title" class="col-sm-1 control-label">
                            <?=$this->lang->line("notice_title")?>
                        </label>
                        <div class="col-sm-4">
                            <input type="text" class="form-control" id="title" name="title" value="<?=set_value('title' , $notice->title)?>" >
                        </div>
                        <span class="col-sm-4 control-label">
                            <?php echo form_error('title'); ?>
                        </span>
                    </div>

                    <?php 
                        if(form_error('date')) 
                            echo "<div class='form-group has-error' >";
                        else     
                            echo "<div class='form-group' >";
                    ?>
                        <label for="date" class="col-sm-1 control-label">
                            <?=$this->lang->line("notice_date")?>
                        </label>
                        <div class="col-sm-4">
                            <input type="text" class="form-control" id="date" name="date" value="<?=set_value('date', date("d-m-Y", strtotime($notice->date)))?>" >
                        </div>
                        <span class="col-sm-4 control-label">
                            <?php echo form_error('date'); ?>
                        </span>
                    </div>

                    <?php 
                        if(form_error('notice')) 
                            echo "<div class='form-group has-error' >";
                        else     
                            echo "<div class='form-group' >";
                    ?>
                        <label for="notice" class="col-sm-1 control-label">
                            <?=$this->lang->line("notice_notice")?>
                        </label>
                        <div class="col-sm-8">
                            <textarea class="form-control" id="notice" name="notice" ><?=set_value('notice', $notice->notice)?></textarea>
                        </div>
                        <span class="col-sm-3 control-label">
                            <?php echo form_error('notice'); ?>
                        </span>
                    </div>

                    <div class="form-group">
                        <div class="col-sm-offset-1 col-sm-8">
                            <input type="submit" class="btn btn-success" value="<?=$this->lang->line("update_class")?>" >
                        </div>
                    </div>

                </form>
            </div>
        </div>
    </div>
</div>
*/?>

<script type="text/javascript" src="<?php echo base_url('assets/editor/jquery-te-1.4.0.min.js'); ?>"></script>
<script type="text/javascript">
$('#date').datepicker();
$('#notice').jqte();
</script>

