<div class="row">
	<div class="col-md-2 hidden-xs">
		<img src="<?php echo base_url('uploads/images/logo.png'); ?>" style="width: auto; height: 40px; margin-top: -8px;" class="img-thumbnail img-responsive" />
	</div>
	<div class="col-md-7 col-xs-9 text-center">
		<h1 class="page-header"><?php echo $this->session->userdata('school');  ?></h1>
	</div>
	<div class="col-md-3 col-xs-3">
		<!-- begin breadcrumb -->
		<ol class="breadcrumb pull-right">
			<li><a href="<?=base_url("dashboard/index")?>"><i class="fa fa-laptop"></i> <?=$this->lang->line('menu_dashboard')?></a></li>
            <li class="active"><?=$this->lang->line('menu_transport')?></li>
		</ol>
		<!-- end breadcrumb -->
		
	</div>
</div>
<div id="page-container">
<!-- begin panel -->
<div class="panel panel-inverse" data-sortable-id="ui-widget-3">
	<div class="panel-heading">
		<?php 
			$usertype = $this->session->userdata("usertype");
			if($usertype == "Super Admin" || $usertype == "Admin") {
		?>
		<div class="btn-group-vertical pull-right">
			<a href="<?php echo base_url('transport/add') ?>" type="button" class="btn btn-success btn-xs"><i class="fa fa-plus"></i> <?=$this->lang->line('add_title')?></a>
		</div>
		<?php } ?>
		<h4 class="panel-title"><i class="fa fa-pencil"></i> <?=$this->lang->line('panel_title')?></h4>
	</div>
	<div class="panel-body">
		<div class="col-sm-12">
			<?php
				$usertype = $this->session->userdata("usertype");
				if($usertype == "Admin" || $usertype == "Super Admin") { 
			?>
			<table id="example1" class="table table-striped table-bordered table-hover dataTable no-footer">
				<thead>
					<tr>
						<th class="col-sm-1"><?=$this->lang->line('slno')?></th>
						<th class="col-sm-3"><?=$this->lang->line('transport_route')?></th>
						<th class="col-sm-2"><?=$this->lang->line('transport_vehicle')?></th>
						<th class="col-sm-2"><?=$this->lang->line('transport_fare')?></th>
						<th class="col-sm-2"><?=$this->lang->line('transport_note')?></th>
						<th class="col-sm-2"><?=$this->lang->line('action')?></th>
					</tr>
				</thead>
				<tbody>
					<?php if(count($transports)) {$i = 1; foreach($transports as $transport) { ?>
						<tr>
							<td data-title="<?=$this->lang->line('slno')?>">
								<?php echo $i; ?>
							</td>
							<td data-title="<?=$this->lang->line('transport_route')?>">
								<?php echo $transport->route; ?>
							</td>
							<td data-title="<?=$this->lang->line('transport_vehicle')?>">
								<?php echo $transport->vehicle; ?>
							</td>
							<td data-title="<?=$this->lang->line('transport_fare')?>">
								<?php echo $transport->fare; ?>
							</td>
							<td data-title="<?=$this->lang->line('transport_note')?>">
								<?php echo $transport->note; ?>
							</td>

							<td data-title="<?=$this->lang->line('action')?>">
								<?php echo btn_edit('transport/edit/'.$transport->transportID, $this->lang->line('edit')) ?>
								<?php echo btn_delete('transport/delete/'.$transport->transportID, $this->lang->line('delete')) ?>
							</td>
						</tr>
					<?php $i++; }} ?>
				</tbody>
			</table>
			<?php } else { ?>
			<div id="hide-table">
				<table id="example1" class="table table-striped table-bordered table-hover dataTable no-footer">
					<thead>
						<tr>
							<th class="col-sm-2"><?=$this->lang->line('slno')?></th>
							<th class="col-sm-3"><?=$this->lang->line('transport_route')?></th>
							<th class="col-sm-2"><?=$this->lang->line('transport_vehicle')?></th>
							<th class="col-sm-2"><?=$this->lang->line('transport_fare')?></th>
							<th class="col-sm-3"><?=$this->lang->line('transport_note')?></th>
						   
						</tr>
					</thead>
					<tbody>
						<?php if(count($transports)) {$i = 1; foreach($transports as $transport) { ?>
							<tr>
								<td data-title="<?=$this->lang->line('slno')?>">
									<?php echo $i; ?>
								</td>
								<td data-title="<?=$this->lang->line('transport_route')?>">
									<?php echo $transport->route; ?>
								</td>
								<td data-title="<?=$this->lang->line('transport_vehicle')?>">
									<?php echo $transport->vehicle; ?>
								</td>
								<td data-title="<?=$this->lang->line('transport_fare')?>">
									<?php echo $transport->fare; ?>
								</td>
								<td data-title="<?=$this->lang->line('transport_note')?>">
									<?php echo $transport->note; ?>
								</td>
							</tr>
						<?php $i++; }} ?>
					</tbody>
				</table>
			</div>
			<?php } ?>

		</div>
	</div>
</div>
</div>
