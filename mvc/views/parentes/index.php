<div class="row">
	<div class="col-md-2 hidden-xs">
		<img src="<?php echo base_url('uploads/images/logo.png'); ?>" style="width: auto; height: 40px; margin-top: -8px;" class="img-thumbnail img-responsive" />
	</div>
	<div class="col-md-7 col-xs-9 text-center">
		<h1 class="page-header"><?php echo $this->session->userdata('school');  ?></h1>
	</div>
	<div class="col-md-3 col-xs-3">
		<!-- begin breadcrumb -->
		<ol class="breadcrumb pull-right">
			<li ><a href="<?php echo base_url('dashboard/index');?>"><i class="fa fa-laptop"></i> Dashboard</a></li>
			<li class=""><?=$this->lang->line('menu_parent')?></li>
		</ol>
		<!-- end breadcrumb -->
	</div>
</div>

<div id="page-container">
<!-- begin panel -->
<div class="panel panel-inverse" data-sortable-id="ui-widget-3">
	<div class="panel-heading">
		 <?php 
			$usertype = $this->session->userdata("usertype");
			if($usertype == "Admin" || $usertype == "Super Admin") {
		?>
			<div class="btn-group-vertical pull-right">
				<a href="<?php echo base_url('parentes/add') ?>" type="button" class="btn btn-success btn-xs"><i class="fa fa-plus"></i> Add Parent</a>
			</div>
		<?php } ?>
		<h4 class="panel-title"><i class="fa fa-desktop"></i> <?=$this->lang->line('panel_title')?></h4>
	</div>
	<div class="panel-body">
		<table id="data-table" class="table table-striped table-bordered nowrap" width="100%">
			<thead>
				<tr>
					<th class="col-sm-2"><?=$this->lang->line('slno')?></th>
					<th class="col-sm-2"><?=$this->lang->line('parentes_photo')?></th>
					<th class="col-sm-2"><?=$this->lang->line('parentes_name')?></th>
					<th class="col-sm-2"><?=$this->lang->line('parentes_email')?></th>
					<th class="col-sm-2"><?=$this->lang->line('parentes_status')?></th>
					<th class="col-sm-2"><?=$this->lang->line('action')?></th>
				</tr>
			</thead>
			<tbody>
				<?php if(count($parentes)) {$i = 1; foreach($parentes as $parent) { ?>
					<tr>
						<td data-title="<?=$this->lang->line('slno')?>">
							<?php echo $i; ?>
						</td>
						<td data-title="<?=$this->lang->line('parentes_photo')?>">
							<?php $array = array(
									"src" => base_url('uploads/images/'.$parent->photo),
									'width' => '35px',
									'height' => '35px',
									'class' => 'img-rounded'

								);
								echo img($array); 
							?>
						</td>
						<td data-title="<?=$this->lang->line('parentes_name')?>">
							<?php echo $parent->name; ?>
						</td>
						<td data-title="<?=$this->lang->line('parentes_email')?>">
							<?php echo $parent->email; ?>
						</td>
						<td data-title="<?=$this->lang->line('parentes_status')?>">
						  <div class="onoffswitch-small" id="<?=$parent->parentID?>">
							  <input type="checkbox" id="myonoffswitch<?=$parent->parentID?>" class="onoffswitch-small-checkbox" name="parentes_status" <?php if($parent->parentactive === '1') echo "checked='checked'"; ?>>
							  <label for="myonoffswitch<?=$parent->parentID?>" class="onoffswitch-small-label">
								  <span class="onoffswitch-small-inner"></span>
								  <span class="onoffswitch-small-switch"></span>
							  </label>
						  </div>           
						</td>
						<td data-title="<?=$this->lang->line('action')?>">
							<?php echo btn_view('parentes/view/'.$parent->parentID, $this->lang->line('view')) ?>
							<?php echo btn_edit('parentes/edit/'.$parent->parentID, $this->lang->line('edit')) ?>
							<?php echo btn_delete('parentes/delete/'.$parent->parentID, $this->lang->line('delete')) ?>
						</td>
					</tr>
				<?php $i++; }} ?>
			</tbody>
		</table>
	</div>
</div>




<script>
  var status = '';
  var id = 0;
  $('.onoffswitch-small-checkbox').click(function() {
      if($(this).prop('checked')) {
          status = 'chacked';
          id = $(this).parent().attr("id");
      } else {
          status = 'unchacked';
          id = $(this).parent().attr("id");
      }

      if((status != '' || status != null) && (id !='')) {
          $.ajax({
              type: 'POST',
              url: "<?=base_url('parentes/active')?>",
              data: "id=" + id + "&status=" + status,
              dataType: "html",
              success: function(data) {
                  if(data == 'Success') {
                      toastr["success"]("Success")
                      toastr.options = {
                        "closeButton": true,
                        "debug": false,
                        "newestOnTop": false,
                        "progressBar": false,
                        "positionClass": "toast-top-right",
                        "preventDuplicates": false,
                        "onclick": null,
                        "showDuration": "500",
                        "hideDuration": "500",
                        "timeOut": "5000",
                        "extendedTimeOut": "1000",
                        "showEasing": "swing",
                        "hideEasing": "linear",
                        "showMethod": "fadeIn",
                        "hideMethod": "fadeOut"
                      }
                  } else {
                      toastr["error"]("Error")
                      toastr.options = {
                        "closeButton": true,
                        "debug": false,
                        "newestOnTop": false,
                        "progressBar": false,
                        "positionClass": "toast-top-right",
                        "preventDuplicates": false,
                        "onclick": null,
                        "showDuration": "500",
                        "hideDuration": "500",
                        "timeOut": "5000",
                        "extendedTimeOut": "1000",
                        "showEasing": "swing",
                        "hideEasing": "linear",
                        "showMethod": "fadeIn",
                        "hideMethod": "fadeOut"
                      }
                  }
              }
          });
      }
  }); 
</script>