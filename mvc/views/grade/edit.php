<div class="row">
	<div class="col-md-2 hidden-xs">
		<img src="<?php echo base_url('uploads/images/logo.png'); ?>" style="width: auto; height: 40px; margin-top: -8px;" class="img-thumbnail img-responsive" />
	</div>
	<div class="col-md-7 col-xs-9 text-center">
		<h1 class="page-header"><?php echo $this->session->userdata('school');  ?></h1>
	</div>
	<div class="col-md-3 col-xs-3">
		<!-- begin breadcrumb -->
		<ol class="breadcrumb pull-right">
			<li><a href="<?=base_url("dashboard/index")?>"><i class="fa fa-laptop"></i> <?=$this->lang->line('menu_dashboard')?></a></li>
            <li><a href="<?=base_url("grade/index")?>"><?=$this->lang->line('menu_grade')?></a></li>
            <li class="active"><?=$this->lang->line('menu_edit')?> <?=$this->lang->line('menu_grade')?></li>
		</ol>
		<!-- end breadcrumb -->
	</div>
</div>

<div id="page-container">
<!-- begin panel -->
<div class="panel panel-inverse" data-sortable-id="ui-widget-3">
	<div class="panel-heading">
		 <?php 
			$usertype = $this->session->userdata("usertype");
			if($usertype == "Admin" || $usertype == "Super Admin") {
		?>
			<div class="btn-group-vertical pull-right">
				<a href="<?php echo base_url('grade/add') ?>" type="button" class="btn btn-success btn-xs"><i class="fa fa-plus"></i> <?=$this->lang->line('add_title')?></a>
			</div>
		<?php } ?>
		<h4 class="panel-title"><i class="fa fa-desktop"></i> <?=$this->lang->line('panel_title')?></h4>
	</div>
	<div class="panel-body">
		<div class="col-sm-8">
			<form class="form-horizontal" role="form" method="post">

				<?php 
					if(form_error('grade')) 
						echo "<div class='form-group has-error' >";
					else     
						echo "<div class='form-group' >";
				?>
					<label for="grade" class="col-sm-2 control-label">
						<?=$this->lang->line("grade_name")?>
					</label>
					<div class="col-sm-6">
						<input type="text" class="form-control" id="grade" name="grade" value="<?=set_value('grade', $grade->grade)?>" >
					</div>
					<span class="col-sm-4 control-label">
						<?php echo form_error('grade'); ?>
					</span>
				</div>

				<?php 
					if(form_error('point')) 
						echo "<div class='form-group has-error' >";
					else     
						echo "<div class='form-group' >";
				?>
					<label for="point" class="col-sm-2 control-label">
						<?=$this->lang->line("grade_point")?>
					</label>
					<div class="col-sm-6">
						<input type="text" class="form-control" id="point" name="point" value="<?=set_value('point', $grade->point)?>" >
					</div>
					<span class="col-sm-4 control-label">
						<?php echo form_error('point'); ?>
					</span>
				</div>

				<?php 
					if(form_error('gradefrom')) 
						echo "<div class='form-group has-error' >";
					else     
						echo "<div class='form-group' >";
				?>
					<label for="gradefrom" class="col-sm-2 control-label">
						<?=$this->lang->line("grade_gradefrom")?>
					</label>
					<div class="col-sm-6">
						<input type="text" class="form-control" id="gradefrom" name="gradefrom" value="<?=set_value('gradefrom', $grade->gradefrom)?>" >
					</div>
					<span class="col-sm-4 control-label">
						<?php echo form_error('gradefrom'); ?>
					</span>
				</div>

				<?php 
					if(form_error('gradeupto')) 
						echo "<div class='form-group has-error' >";
					else     
						echo "<div class='form-group' >";
				?>
					<label for="gradeupto" class="col-sm-2 control-label">
						<?=$this->lang->line("grade_gradeupto")?>
					</label>
					<div class="col-sm-6">
						<input type="text" class="form-control" id="gradeupto" name="gradeupto" value="<?=set_value('gradeupto', $grade->gradeupto)?>" >
					</div>
					<span class="col-sm-4 control-label">
						<?php echo form_error('gradeupto'); ?>
					</span>
				</div>

				<?php 
					if(form_error('note')) 
						echo "<div class='form-group has-error' >";
					else     
						echo "<div class='form-group' >";
				?>
					<label for="note" class="col-sm-2 control-label">
						<?=$this->lang->line("grade_note")?>
					</label>
					<div class="col-sm-6">
						<textarea style="resize:none;" class="form-control" id="note" name="note"><?=set_value('note', $grade->note)?></textarea>
					</div>
					<span class="col-sm-4 control-label">
						<?php echo form_error('note'); ?>
					</span>
				</div>


				<div class="form-group">
					<div class="col-sm-offset-2 col-sm-8">
						<input type="submit" class="btn btn-success" value="<?=$this->lang->line("update_class")?>" >
					</div>
				</div>

			</form>
		</div> 
	</body>
</div>
