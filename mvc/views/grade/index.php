<div class="row">
	<div class="col-md-2 hidden-xs">
		<img src="<?php echo base_url('uploads/images/logo.png'); ?>" style="width: auto; height: 40px; margin-top: -8px;" class="img-thumbnail img-responsive" />
	</div>
	<div class="col-md-7 col-xs-9 text-center">
		<h1 class="page-header"><?php echo $this->session->userdata('school');  ?></h1>
	</div>
	<div class="col-md-3 col-xs-3">
		<!-- begin breadcrumb -->
		<ol class="breadcrumb pull-right">
			<li><a href="<?=base_url("dashboard/index")?>"><i class="fa fa-laptop"></i> <?=$this->lang->line('menu_dashboard')?></a></li>
            <li class="active"><?=$this->lang->line('menu_grade')?></li>
		</ol>
		<!-- end breadcrumb -->
	</div>
</div>

<div id="page-container">
<!-- begin panel -->
<div class="panel panel-inverse" data-sortable-id="ui-widget-3">
	<div class="panel-heading">
		 <?php 
			$usertype = $this->session->userdata("usertype");
			if($usertype == "Admin" || $usertype == "Super Admin") {
		?>
			<div class="btn-group-vertical pull-right">
				<a href="<?php echo base_url('grade/add') ?>" type="button" class="btn btn-success btn-xs"><i class="fa fa-plus"></i> <?=$this->lang->line('add_title')?></a>
			</div>
		<?php } ?>
		<h4 class="panel-title"><i class="fa fa-desktop"></i> <?=$this->lang->line('panel_title')?></h4>
	</div>
	<div class="panel-body">
		<table id="data-table" class="table table-striped table-bordered nowrap" width="100%">
			<thead>
				<tr>
					<th class="col-lg-1"><?=$this->lang->line('slno')?></th>
					<th class="col-lg-2"><?=$this->lang->line('grade_name')?></th>
					<th class="col-lg-2"><?=$this->lang->line('grade_point')?></th>
					<th class="col-lg-2"><?=$this->lang->line('grade_gradefrom')?></th>
					<th class="col-lg-2"><?=$this->lang->line('grade_gradeupto')?></th>
					<th class="col-lg-1"><?=$this->lang->line('grade_note')?></th>
					<th class="col-lg-2"><?=$this->lang->line('action')?></th>
				</tr>
			</thead>
			<tbody>
				<?php if(count($grades)) {$i = 1; foreach($grades as $grade) { ?>
					<tr>
						<td data-title="<?=$this->lang->line('slno')?>">
							<?php echo $i; ?>
						</td>
						<td data-title="<?=$this->lang->line('grade_name')?>">
							<?php echo $grade->grade; ?>
						</td>
						<td data-title="<?=$this->lang->line('grade_point')?>">
							<?php echo $grade->point; ?>
						</td>
						<td data-title="<?=$this->lang->line('grade_gradefrom')?>">
							<?php echo $grade->gradefrom; ?>
						</td>
						<td data-title="<?=$this->lang->line('grade_gradeupto')?>">
							<?php echo $grade->gradeupto; ?>
						</td>
						<td data-title="<?=$this->lang->line('grade_note')?>">
							<?php echo $grade->note; ?>
						</td>

						<td data-title="<?=$this->lang->line('action')?>">
							<?php echo btn_edit('grade/edit/'.$grade->gradeID, $this->lang->line('edit')) ?>
							<?php echo btn_delete('grade/delete/'.$grade->gradeID, $this->lang->line('delete')) ?>
						</td>
					</tr>
				<?php $i++; }} ?>
			</tbody>
		</table>
	</div>
</div>
