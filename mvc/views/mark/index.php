<div class="row">
	<div class="col-md-2 hidden-xs">
		<img src="<?php echo base_url('uploads/images/logo.png'); ?>" style="width: auto; height: 40px; margin-top: -8px;" class="img-thumbnail img-responsive" />
	</div>
	<div class="col-md-7 col-xs-9 text-center">
		<h1 class="page-header"><?php echo $this->session->userdata('school');  ?></h1>
	</div>
	<div class="col-md-3 col-xs-3">
		<!-- begin breadcrumb -->
		<ol class="breadcrumb pull-right">
			<li ><a href="<?=base_url('dashboard/index');?>"><i class="fa fa-laptop"></i><?=$this->lang->line('menu_dashboard')?></a></li>
			<li class="active"><?=$this->lang->line('menu_mark')?></a></li>
		</ol>
		<!-- end breadcrumb -->
	</div>
</div>
<!-- begin panel -->
<div class="panel panel-inverse" data-sortable-id="ui-widget-3">
	<div class="panel-heading">
		<?php 
			$usertype = $this->session->userdata("usertype");
			if($usertype == "Super Admin" || $usertype == "Admin" || $usertype == "Teacher") {
		?>
		<div class="btn-group-vertical pull-right">
			<a href="<?php echo base_url('mark/add_exam') ?>" class="btn btn-success btn-xs">
				<i class="fa fa-plus add_margin"></i> <?=$this->lang->line('add_mark_exam')?>
			</a>
		</div>
		<div class="btn-group-vertical pull-right m-r-10">
			<a href="<?php echo base_url('mark/add_assessment') ?>" class="btn btn-success btn-xs">
				<i class="fa fa-plus add_margin"></i> <?=$this->lang->line('add_mark_assessment')?>
			</a>
		</div>
		<?php } ?>
		<h4 class="panel-title"><i class="fa fa-flask"></i> <?=$this->lang->line('panel_title')?></h4>
	</div>
	<div class="panel-body">
		<div class="col-md-6 add_padding m-b-10">
			<form style="" class="form-horizontal" role="form" method="post">
				
				<div class="input-group">
					<span class="input-group-addon offset_addon"><i class="fa fa-sitemap"></i></span>
					<?php
						$array = array("0" => $this->lang->line("mark_select_classes"));
						foreach ($classes as $classa) {
							$array[$classa->classesID] = $classa->classes;
						}
						echo form_dropdown("classesID", $array, set_value("classesID", $set), "id='classesID' class='form-control offset_height'");
					?>
				</div>
			</form>
		</div>
		<div class="col-md-12">
			<div id="hide-table">
				<table id="example1" class="table table-striped table-bordered table-hover dataTable no-footer">
					<thead>
						<tr>
							<th class="col-sm-2"><?=$this->lang->line('slno')?></th>
							<th class="col-sm-2"><?=$this->lang->line('mark_photo')?></th>
							<th class="col-sm-2"><?=$this->lang->line('mark_name')?></th>
							<th class="col-sm-2"><?=$this->lang->line('mark_roll')?></th>
							<th class="col-sm-2"><?=$this->lang->line('mark_phone')?></th>
							<th class="col-sm-2"><?=$this->lang->line('action')?></th>
						</tr>
					</thead>
					<tbody>
						<?php if(count($student)) {$i = 1; foreach($student as $student) { ?>
							<tr>
								<td data-title="<?=$this->lang->line('slno')?>">
									<?php echo $i; ?>
								</td>
								<td data-title="<?=$this->lang->line('mark_photo')?>">
									<?php $array = array(
											"src" => base_url('uploads/images/'.$student->photo),
											'width' => '35px',
											'height' => '35px',
											'class' => 'img-rounded'

										);
										echo img($array); 
									?>
								</td>
								<td data-title="<?=$this->lang->line('mark_name')?>">
									<?php echo $student->name; ?>
								</td>
								<td data-title="<?=$this->lang->line('mark_roll')?>">
									<?php echo $student->roll; ?>
								</td>
								<td data-title="<?=$this->lang->line('mark_phone')?>">
									<?php echo $student->phone; ?>
								</td>
								<td data-title="<?=$this->lang->line('action')?>">
									<?php 
										if($usertype == "Admin" || $usertype == "Super Admin" || $usertype == "Teacher") {
											echo btn_view_exam('mark/view_exams/'.$student->studentID."/".$set, $this->lang->line('view_exam')) . ' &nbsp; ' . btn_view_assessment('mark/view_assessment/'.$student->studentID."/".$set, $this->lang->line('view_assessment')) . ' &nbsp; ' . btn_view('mark/view_all/'.$student->studentID."/".$set, $this->lang->line('view_all'));
										}
									?>
								</td>
							</tr>
						<?php $i++; }} ?>
						
					</tbody>
				</table>
			</div>

		</div>

	</div>
</div>	

<script type="text/javascript">
    $('#classesID').change(function() {
        var classesID = $(this).val();
        if(classesID == 0) {
            $('#hide-table').hide();
        } else {
            $.ajax({
                type: 'POST',
                url: "<?=base_url('mark/mark_list')?>",
                data: "id=" + classesID,
                dataType: "html",
                success: function(data) {
                    window.location.href = data;
                }
            });
        }
    });
</script>