<div class="row">
	<div class="col-md-1 hidden-xs">
		<img src="<?php echo base_url('uploads/images/logo.png'); ?>" style="width: auto; height: 40px; margin-top: -8px;" class="img-thumbnail img-responsive" />
	</div>
	<div class="col-md-7 col-xs-9 text-center">
		<h1 class="page-header"><?php echo $this->session->userdata('school');  ?></h1>
	</div>
	<div class="col-md-4 col-xs-3">
		<!-- begin breadcrumb -->
		<ol class="breadcrumb pull-right">
			<li><a href="<?=base_url("dashboard/index")?>"><i class="fa fa-laptop"></i> <?=$this->lang->line('menu_dashboard')?></a></li>
            <li><a href="<?=base_url("mark/index")?>"><?=$this->lang->line('menu_mark')?></a></li>
            <li class="active"><?=$this->lang->line('menu_add')?> <?=$this->lang->line('menu_mark')?></li>
		</ol>
		<!-- end breadcrumb -->
	</div>
</div>
<!-- begin panel -->
<div class="panel panel-inverse" data-sortable-id="ui-widget-3">
	<div class="panel-heading">
		<h4 class="panel-title"><i class="fa fa-puzzle-piece"></i> <?=$this->lang->line('panel_title')?></h4>
	</div>
	<div class="panel-body">
		<div class="col-sm-8">
			<form class="form-horizontal" method="post" >
                            <?php 
                                if(form_error('examID')) 
                                    echo "<div class='form-group has-error' >";
                                else     
                                    echo "<div class='form-group' >";
                            ?>
                                <label for="examID" class="col-sm-2 control-label">
                                    <?=$this->lang->line('mark_assessment')?>
                                </label>
                                <div class="col-sm-6">
                                    <?php
                                        $array = array("0" => $this->lang->line("mark_select_assessment"));
                                        foreach ($assessments as $assessment) {
                                            $array[$assessment->testID] = $assessment->assessments;
                                        }
                                        echo form_dropdown("examID", $array, set_value("examID", $set_exam), "id='examID' class='form-control'");
                                    ?>
                                </div>
                            </div>


                            <?php 
                                if(form_error('classesID')) 
                                    echo "<div class='form-group has-error' >";
                                else     
                                    echo "<div class='form-group' >";
                            ?>
                                <label for="classesID" class="col-sm-2 control-label">
                                    <?=$this->lang->line('mark_classes')?>
                                </label>
                                <div class="col-sm-6">
                                    <?php
                                        $array = array("0" => $this->lang->line("mark_select_classes"));
                                        foreach ($classes as $classa) {
                                            $array[$classa->classesID] = $classa->classes;
                                        }
                                        echo form_dropdown("classesID", $array, set_value("classesID", $set_classes), "id='classesID' class='form-control'");
                                    ?>
                                </div>
                            </div>
                            <?php 
                                if(form_error('subjectID')) 
                                    echo "<div class='form-group has-error' >";
                                else     
                                    echo "<div class='form-group' >";
                            ?>
                                <label for="subjectID" class="col-sm-2 control-label">
                                    <?=$this->lang->line('mark_subject')?>
                                </label>
                                <div class="col-sm-6">
                                    <?php
                                        $array = array("0" => $this->lang->line("mark_select_subject"));
                                        if($subjects != 0) {
                                            foreach ($subjects as $subject) {
                                                $array[$subject->subjectID] = $subject->subject;
                                            }
                                        }
                                        echo form_dropdown("subjectID", $array, set_value("subjectID", $set_subject), "id='subjectID' class='form-control'");
                                    ?>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-sm-offset-2 col-sm-8">
                                    <input type="submit" class="btn btn-success" style="margin-bottom:0px" value="<?=$this->lang->line("mark_an_assessment")?>" >
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
				
                <?php if(count($students)) { ?>
                <div class="col-md-12" id="hide-table">
					<hr />
                    <table id="example1" class="table table-striped table-bordered table-hover dataTable no-footer">
                        <thead>
                            <tr>
                                <th class="col-sm-2"><?=$this->lang->line('slno')?></th>
                                <th class="col-sm-2"><?=$this->lang->line('mark_photo')?></th>
                                <th class="col-sm-2"><?=$this->lang->line('mark_name')?></th>
                                <th class="col-sm-2"><?=$this->lang->line('mark_roll')?></th>
                                <th class="col-sm-2"><?=$this->lang->line('mark_phone')?></th>
                                <th class="col-sm-2"><?=$this->lang->line('action')?>
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php if(count($students)) {$i = 1; foreach($students as $student) { foreach ($marks as $mark) { if($student->studentID == $mark->studentID) {   ?>
                                <tr>
                                    <td data-title="<?=$this->lang->line('slno')?>">
                                        <?php echo $i; ?>
                                    </td>
                                    <td data-title="<?=$this->lang->line('mark_photo')?>">
                                        <?php $array = array(
                                                "src" => base_url('uploads/images/'.$student->photo),
                                                'width' => '35px',
                                                'height' => '35px',
                                                'class' => 'img-rounded'

                                            );
                                            echo img($array); 
                                        ?>
                                    </td>
                                    <td data-title="<?=$this->lang->line('mark_name')?>">
                                        <?php echo $student->name; ?>
                                    </td>
                                    <td data-title="<?=$this->lang->line('mark_roll')?>">
                                        <?php echo $student->roll; ?>
                                    </td>
                                    <td data-title="<?=$this->lang->line('mark_phone')?>">
                                        <?php echo $student->phone; ?>
                                    </td>
                                    <td data-title='Action'>
                                        <input class="form-control mark" type="number" name="<?=$student->studentID?>" id="<?=$student->studentID?>" value="<?=set_value($student->studentID, $mark->mark)?>" />
                                    </td>
                                </tr>
                            <?php $i++;  }}}} ?>
                        </tbody>
                    </table>
                </div>
				<div class="m-l-10">
				<input type="button" class="btn btn-success" style="float:none; margin:10px auto;" id="add_mark" name="add_mark" value="<?=$this->lang->line("add_mark_assessment")?>" />
				</div>

					<script type="text/javascript">
						$("#add_mark").click(function() {
							var inputs = "";
							var inputs_value = "";
							$('.mark').each(function(index, value) {
								inputs_value = $(this).val(); 
								if(inputs_value == '' || inputs_value == null) {
									inputs += $(this).attr("id") +":"+'0'+"$";
								} else {
									inputs += $(this).attr("id") +":"+inputs_value+"$";
								}

							});

							$.ajax({
								type: 'POST',
								url: "<?=base_url('mark/mark_assessment_send')?>",
								data: {"examID" : "<?=$set_exam?>", "classesID" : "<?=$set_classes?>", "subjectID" : "<?=$set_subject?>", "inputs" : inputs},
								dataType: "html",
								success: function(data) {
									toastr["success"](data)
									toastr.options = {
									  "closeButton": true,
									  "debug": false,
									  "newestOnTop": false,
									  "progressBar": false,
									  "positionClass": "toast-top-right",
									  "preventDuplicates": false,
									  "onclick": null,
									  "showDuration": "500",
									  "hideDuration": "500",
									  "timeOut": "5000",
									  "extendedTimeOut": "1000",
									  "showEasing": "swing",
									  "hideEasing": "linear",
									  "showMethod": "fadeIn",
									  "hideMethod": "fadeOut"
									}

								}
							});
						});
					</script>
                <?php } ?>
			</div>
		</div>
	</div>
</div>	

 <script type="text/javascript">

$("#classesID").change(function() {
var id = $(this).val();
if(parseInt(id)) {
    if(id === '0') {
        $('#subjectID').val(0);
    } else {
        $.ajax({
            type: 'POST',
            url: "<?=base_url('mark/subjectcall')?>",
            data: {"id" : id},
            dataType: "html",
            success: function(data) {
               $('#subjectID').html(data);
            }
        });
    }
}
});

</script>