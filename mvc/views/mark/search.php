<div class="row">
	<div class="col-md-2 hidden-xs">
		<img src="<?php echo base_url('uploads/images/logo.png'); ?>" style="width: auto; height: 40px; margin-top: -8px;" class="img-thumbnail img-responsive" />
	</div>
	<div class="col-md-7 col-xs-9 text-center">
		<h1 class="page-header"><?php echo $this->session->userdata('school');  ?></h1>
	</div>
	<div class="col-md-3 col-xs-3">
		<!-- begin breadcrumb -->
		<ol class="breadcrumb pull-right">
			<li ><a href="<?php echo base_url('dashboard/index');?>"><i class="fa fa-laptop"></i><?=$this->lang->line('menu_dashboard')?></a></li>
			<li><a class="active"><?=$this->lang->line('menu_mark')?></a></li>
		</ol>
		<!-- end breadcrumb -->
	</div>
</div>
<!-- begin panel -->
<div class="panel panel-inverse" data-sortable-id="ui-widget-3">
	<div class="panel-heading">
		<?php 
			$usertype = $this->session->userdata("usertype");
			if($usertype == "Super Admin" || $usertype == "Admin" || $usertype == "Teacher") {
		?>
		<div class="btn-group-vertical pull-right">
			<a href="<?php echo base_url('mark/add_exam') ?>" class="btn btn-success btn-xs">
				<i class="fa fa-plus add_margin"></i> <?=$this->lang->line('add_mark_exam')?>
			</a>
		</div>
		<div class="btn-group-vertical pull-right m-r-10">
			<a href="<?php echo base_url('mark/add_assessment') ?>" class="btn btn-success btn-xs">
				<i class="fa fa-plus add_margin"></i> <?=$this->lang->line('add_mark_assessment')?>
			</a>
		</div>
		<?php } ?>
		<h4 class="panel-title"><i class="fa fa-flask"></i> <?=$this->lang->line('panel_title')?></h4>
	</div>
	<div class="panel-body">
		<div class="col-md-6 add_padding">
			<form style="" class="form-horizontal" role="form" method="post">
				<?php 
						if(form_error('name')) 
							echo "<div class='form-group has-error' >";
						else     
							echo "<div class='form-group' >";
				?>
				<div class="input-group" style="width: 97% !important;">
					<span class="input-group-addon offset_addon"><i class="fa fa-sitemap"></i></span>
					<?php
						$array = array("0" => $this->lang->line("mark_select_classes"));
						foreach ($classes as $classa) {
							$array[$classa->classesID] = $classa->classes;
						}
						echo form_dropdown("classesID", $array, set_value("classesID"), "id='classesID' class='form-control offset_height'");
					?>
				</div></div> </div>
			</form>
		</div>		
	</div>
</div>	

<script type="text/javascript">
    $('#classesID').change(function() {
        var classesID = $(this).val();
        if(classesID == 0) {
            $('#hide-table').hide();
        } else {
            $.ajax({
                type: 'POST',
                url: "<?=base_url('mark/mark_list')?>",
                data: "id=" + classesID,
                dataType: "html",
                success: function(data) {
                    window.location.href = data;
                }
            });
        }
    });
</script>