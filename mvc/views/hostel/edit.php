<div class="row">
	<div class="col-md-2 hidden-xs">
		<img src="<?php echo base_url('uploads/images/logo.png'); ?>" style="width: auto; height: 40px; margin-top: -8px;" class="img-thumbnail img-responsive" />
	</div>
	<div class="col-md-7 col-xs-9 text-center">
		<h1 class="page-header"><?php echo $this->session->userdata('school');  ?></h1>
	</div>
	<div class="col-md-3 col-xs-3">
		<!-- begin breadcrumb -->
		<ol class="breadcrumb pull-right">
			<li><a href="<?=base_url("dashboard/index")?>"><i class="fa fa-laptop"></i> <?=$this->lang->line('menu_dashboard')?></a></li>
            <li><a href="<?=base_url("hostel/index")?>"><?=$this->lang->line('menu_hostel')?></a></li>
            <li class="active"><?=$this->lang->line('menu_edit')?> <?=$this->lang->line('menu_hostel')?></li>
		</ol>
		<!-- end breadcrumb -->
	</div>
</div>

<div id="page-container">
<!-- begin panel -->
<div class="panel panel-inverse" data-sortable-id="ui-widget-3">
	<div class="panel-heading">
		<?php
			$usertype = $this->session->userdata("usertype");
			if($usertype == "Admin" || $usertype == "Super Admin") { 
		?>
		<div class="btn-group-vertical pull-right">
			<a href="<?php echo base_url('hostel/add') ?>" type="button" class="btn btn-success btn-xs"><i class="fa fa-plus"></i> <?=$this->lang->line('add_title')?></a>
		</div>
			<?php } ?>
		<h4 class="panel-title"><i class="fa fa-desktop"></i> <?=$this->lang->line('panel_title')?></h4>
	</div>
	<div class="panel-body">
		<div class="col-sm-8">
			<form class="form-horizontal" role="form" method="post">
			   
				<?php 
					if(form_error('name')) 
						echo "<div class='form-group has-error' >";
					else     
						echo "<div class='form-group' >";
				?>
					<label for="name" class="col-sm-2 control-label">
						<?=$this->lang->line("hostel_name")?>
					</label>
					<div class="col-sm-6">
						<input type="text" class="form-control" id="name" name="name" value="<?=set_value('name', $hostel->name)?>" >
					</div>
					<span class="col-sm-4 control-label">
						<?php echo form_error('name'); ?>
					</span>
				</div>

				<?php 
					if(form_error('htype')) 
						echo "<div class='form-group has-error' >";
					else     
						echo "<div class='form-group' >";
				?>
					<label for="htype" class="col-sm-2 control-label">
						<?=$this->lang->line("hostel_htype")?>
					</label>
					<div class="col-sm-6">
						<?php
							$array = array(
								0 => $this->lang->line('select_hostel_type'),
								$this->lang->line('hostel_boys') => $this->lang->line('hostel_boys'),
								$this->lang->line('hostel_girls') => $this->lang->line('hostel_girls'),
								$this->lang->line('hostel_combine') => $this->lang->line('hostel_combine')
							);
							echo form_dropdown("htype", $array, set_value("htype", $hostel->htype), "id='htype' class='form-control'");
						?>
					</div>
					<span class="col-sm-4 control-label">
						<?php echo form_error('htype'); ?>
					</span>
				</div>

				<?php 
					if(form_error('address')) 
						echo "<div class='form-group has-error' >";
					else     
						echo "<div class='form-group' >";
				?>
					<label for="address" class="col-sm-2 control-label">
						<?=$this->lang->line("hostel_address")?>
					</label>
					<div class="col-sm-6">
						<input type="text" class="form-control" id="address" name="address" value="<?=set_value('address', $hostel->address)?>" >
					</div>
					<span class="col-sm-4 control-label">
						<?php echo form_error('address'); ?>
					</span>
				</div>

				<?php 
					if(form_error('note')) 
						echo "<div class='form-group has-error' >";
					else     
						echo "<div class='form-group' >";
				?>
					<label for="note" class="col-sm-2 control-label">
						<?=$this->lang->line("hostel_note")?>
					</label>
					<div class="col-sm-6">
						<textarea class="form-control" style="resize:none;" id="note" name="note"><?=set_value('note', $hostel->note)?></textarea>
					</div>
					<span class="col-sm-4 control-label">
						<?php echo form_error('note'); ?>
					</span>
				</div>

				<div class="form-group">
					<div class="col-sm-offset-2 col-sm-8">
						<input type="submit" class="btn btn-success" value="<?=$this->lang->line("update_hostel")?>" >
					</div>
				</div>
			</form>
		</div>
	</div>
</div>
</div>

