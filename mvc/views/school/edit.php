<div class="row">
	<div class="col-md-2 hidden-xs">
		<img src="<?php echo base_url('uploads/images/logo.png'); ?>" style="width: auto; height: 40px; margin-top: -8px;" class="img-thumbnail img-responsive" />
	</div>
	<div class="col-md-7 col-xs-9 text-center">
		<h1 class="page-header"><?php echo $this->session->userdata('school');  ?></h1>
	</div>
	<div class="col-md-3 col-xs-3">
		<!-- begin breadcrumb -->
		<ol class="breadcrumb pull-right">
			<li><a href="<?=base_url("dashboard/index")?>"><i class="fa fa-laptop"></i> <?=$this->lang->line('menu_dashboard')?></a></li>
            <li><a href="<?=base_url("school/index")?>"><?=$this->lang->line('menu_school')?></a></li>
            <li class="active"><?=$this->lang->line('menu_edit')?> <?=$this->lang->line('menu_school')?></li>
		</ol>
		<!-- end breadcrumb -->
	</div>
</div>

<div id="page-container">
<!-- begin panel -->
<div class="panel panel-inverse" data-sortable-id="ui-widget-3">
	<div class="panel-heading">
		<h4 class="panel-title"><i class="fa fa-desktop"></i> <?=$this->lang->line('panel_title')?></h4>
	</div>
	<div class="panel-body">
		<h2 class="profileBody" style="font-weight: lighter; font-size: 22px; margin-bottom: 30px;">Edit School Information</h2>
		<form class="form-horizontal" role="form" method="post" enctype="multipart/form-data">
					
			<?php 
				if(form_error('school')) 
					echo "<div class='form-group has-error' >";
				else     
					echo "<div class='form-group col-md-12' >";
			?>
				<label for="school" class="col-sm-2 col-xs-2 col-md-2 control-label">
					 <?=$this->lang->line("school_school")?>
				</label>
				<div class="col-md-6 col-xs-10 col-sm-10">
					<div class="input-group">
						<span class="input-group-addon"><i class="fa icon-school"></i></span>
						<input type="text" class="form-control" id="school" name="school" value="<?=set_value('school', $school->school)?>" >
					</div>
				</div>
				<span class="col-md-4 col-xs-12 col-sm-12 control-label">
					<?php echo form_error('school'); ?>
				</span>
			</div>
			<?php 
				if(form_error('alias')) 
					echo "<div class='form-group has-error' >";
				else     
					echo "<div class='form-group col-md-12' >";
			?>
				<label for="alias" class="col-sm-2 col-xs-2 col-md-2 control-label">
					 <?=$this->lang->line("school_alias")?>
				</label>
				 <div class="col-md-6 col-xs-10 col-sm-10">
					<div class="input-group">
						<span class="input-group-addon"><i class="fa fa-tag"></i></span>
						 <input type="text" class="form-control" id="alias" name="alias" value="<?=set_value('alias', $school->alias)?>" >
					</div>
				</div>
				<span class="col-md-4 col-xs-12 col-sm-12 control-labell">
					<?php echo form_error('alias'); ?>
				</span>
			</div>

			<?php 
				if(form_error('email')) 
					echo "<div class='form-group has-error' >";
				else     
					echo "<div class='form-group col-md-12' >";
			?>
				<label for="email" class="col-sm-2 col-xs-2 col-md-2 control-label">
					  <?=$this->lang->line("school_email")?>
				</label>
				 <div class="col-md-6 col-xs-10 col-sm-10">
					<div class="input-group">
						<span class="input-group-addon"><i class="fa fa-envelope-o"></i></span>
						 <input type="text" class="form-control" id="email" name="email" value="<?=set_value('email', $school->email)?>" >
					</div>
				</div>
				<span class="col-md-4 col-xs-12 col-sm-12 control-labell">
					<?php echo form_error('email'); ?>
				</span>
			</div>

			<?php 
				if(form_error('phone')) 
					echo "<div class='form-group has-error' >";
				else      
					echo "<div class='form-group col-md-12' >";
			?>
				<label for="phone" class="col-sm-2 col-xs-2 col-md-2 control-label">
					  <?=$this->lang->line("school_phone")?>
				</label>
				 <div class="col-md-6 col-xs-10 col-sm-10">
					<div class="input-group">
						<span class="input-group-addon"><i class="fa fa-phone"></i></span>
						 <input type="text" class="form-control" id="phone" name="phone" value="<?=set_value('phone', $school->phone)?>" >
					</div>
				</div>
				<span class="col-md-4 col-xs-12 col-sm-12 control-labell">
					<?php echo form_error('phone'); ?>
				</span>
			</div>
			<?php 
				if(form_error('capacity')) 
					echo "<div class='form-group has-error' >";
				else  
					echo "<div class='form-group col-md-12' >";
			?>
				<label for="capacity" class="col-sm-2 col-xs-2 col-md-2 control-label">
					  <?=$this->lang->line("school_capacity")?>
				</label>
				 <div class="col-md-6 col-xs-10 col-sm-10">
					<div class="input-group">
						<span class="input-group-addon"><i class="fa fa-list"></i></span>
						 <input type="text" class="form-control" id="capacity" name="capacity" value="<?=set_value('capacity', $school->capacity)?>" >
					</div>
				</div>
				<span class="col-md-4 col-xs-12 col-sm-12 control-labell">
					<?php echo form_error('capacity'); ?>
				</span>
			</div>

			<?php 
				if(form_error('service_charge')) 
					echo "<div class='form-group has-error' >";
				else  
					echo "<div class='form-group col-md-12' >";
			?>
				<label for="service_charge" class="col-sm-2 col-xs-2 col-md-2 control-label">
					  Display Service Charge on invoice?
				</label>
				 <div class="col-md-6 col-xs-10 col-sm-10">
					<div class="onoffswitch-small" id="<?=$school->schoolID?>">
						<input type="checkbox" id="myonoffswitch<?=$school->schoolID?>" class="onoffswitch-small-checkbox" name="service_charge" <?php if($school->service_charge === '2') echo "checked='checked'"; ?>>
						<label for="myonoffswitch<?=$school->schoolID?>" class="onoffswitch-small-label">
							<span class="onoffswitch-small-inner"></span>
							<span class="onoffswitch-small-switch"></span>
						</label>
					</div>
				</div>
				<span class="col-md-4 col-xs-12 col-sm-12 control-labell">
					<?php echo form_error('service_charge'); ?>
				</span>
			</div>

			<?php 
				if(form_error('automation')) 
					echo "<div class='form-group has-error hidden' >";
				else
					echo "<div class='form-group col-md-12 hidden' >";
			?>
				<label for="automation" class="col-sm-2 col-xs-2 col-md-2 control-label">
					  <?=$this->lang->line("school_automation")?>
				</label>
				 <div class="col-md-6 col-xs-10 col-sm-10">
					<div class="input-group">
						<span class="input-group-addon"><i class="fa fa-crosshairs"></i></span>
						  <input type="text" class="form-control" id="automation" name="automation" value="<?=set_value('automation', $school->automation)?>" >
					</div>
				</div>
				<span class="col-md-4 col-xs-12 col-sm-12 control-labell">
					<?php echo form_error('automation'); ?>
				</span>
			</div>
			<?php 
				if(form_error('currency_code')) 
					echo "<div class='form-group has-error' >";
				else     
				   echo "<div class='form-group col-md-12' >";
			?>
				<label for="currency_code" class="col-sm-2 col-xs-2 col-md-2 control-label">
					  <?=$this->lang->line("school_currency_code")?>
				</label>
				 <div class="col-md-6 col-xs-10 col-sm-10">
					<div class="input-group">
						<span class="input-group-addon"><i class="fa fa-money"></i></span>
						   <input type="text" class="form-control" id="currency_code" name="currency_code" value="<?=set_value('currency_code', $school->currency_code)?>" >
					</div>
				</div>
				<span class="col-md-4 col-xs-12 col-sm-12 control-labell">
					<?php echo form_error('currency_code'); ?>
				</span>
			</div>

			<?php 
				if(form_error('currency_symbol')) 
					echo "<div class='form-group has-error' >";
				else     
				   echo "<div class='form-group col-md-12' >";
			?>
				<label for="currency_symbol" class="col-sm-2 col-xs-2 col-md-2 control-label">
					  <?=$this->lang->line("school_currency_symbol")?>
				</label>
				 <div class="col-md-6 col-xs-10 col-sm-10">
					<div class="input-group">
						<span class="input-group-addon"><i class="fa fa-dollar"></i></span>
						   <input type="text" class="form-control" id="currency_symbol" name="currency_symbol" value="<?=set_value('currency_symbol', $school->currency_symbol)?>" >
					</div>
				</div>
				<span class="col-md-4 col-xs-12 col-sm-12 control-labell">
					<?php echo form_error('currency_symbol'); ?>
				</span>
			</div>
			<?php 
				if(form_error('footer')) 
					echo "<div class='form-group has-error' >";
				 else     
				   echo "<div class='form-group col-md-12' >";
			?>
				<label for="footer" class="col-sm-2 col-xs-2 col-md-2 control-label">
					  <?=$this->lang->line("school_footer")?>
				</label>
				 <div class="col-md-6 col-xs-10 col-sm-10">
					<div class="input-group">
						<span class="input-group-addon"><i class="fa fa-archive"></i></span>
						   <input type="text" class="form-control" id="footer" name="footer" value="<?=set_value('footer', $school->footer)?>" />
					</div>
				</div>
				<span class="col-md-4 col-xs-12 col-sm-12 control-labell">
					<?php echo form_error('footer'); ?>
				</span>
			</div>
			 <?php 
				if(isset($image)) 
					echo "<div class='form-group has-error' >";
				else     
					echo "<div class='form-group col-md-12' >";
			?>
				<label for="photo" class="col-sm-2 col-xs-2 col-md-2 control-label">
					<?=$this->lang->line("school_logo")?>
				</label>
				<div class="col-sm-6">
					<div class="js">
						<input id="uploadBtn" type="file" name="image" class="inputfile inputfile-1" data-multiple-caption="{count} files selected" multiple />
						<label for="uploadBtn"><svg xmlns="http://www.w3.org/2000/svg" width="20" height="17" viewBox="0 0 20 17"><path d="M10 0l-5.2 4.9h3.3v5.1h3.8v-5.1h3.3l-5.2-4.9zm9.3 11.5l-3.2-2.1h-2l3.4 2.6h-3.5c-.1 0-.2.1-.2.1l-.8 2.3h-6l-.8-2.2c-.1-.1-.1-.2-.2-.2h-3.6l3.4-2.6h-2l-3.2 2.1c-.4.3-.7 1-.6 1.5l.6 3.1c.1.5.7.9 1.2.9h16.3c.6 0 1.1-.4 1.3-.9l.6-3.1c.1-.5-.2-1.2-.7-1.5z"/></svg> <span>Upload school logo &hellip;</span></label>
						<p style="font-weight: bold; font-size:12px;" class="text-info"><i>Image dimensions - 120px * 100px</i></p>
					</div>
				</div>
				 <span class="col-md-4 col-xs-12 col-sm-12 control-labell">
					<?php if(isset($image)) echo $image; ?>
				</span>
			</div>
			<?php 
				if(form_error('address')) 
					echo "<div class='form-group has-error' >";
				else     
				   echo "<div class='form-group col-md-12' >";
			?>
				<label for="address" class="col-sm-2 col-xs-2 col-md-2 control-label">
					  <?=$this->lang->line("school_address")?>
				</label>
				 <div class="col-md-6 col-xs-10 col-sm-10">
					<div class="input-group">
						<span class="input-group-addon"><i class="fa fa-building-o"></i></span>
						<textarea class="form-control" style="resize:none;" id="address" name="address"><?=set_value('address', $school->address)?></textarea>
					</div>
				</div>
				<span class="col-md-4 col-xs-12 col-sm-12 control-labell">
					<?php echo form_error('address'); ?>
				</span>
			</div>
		
			<div class="col-md-12" style="border-top: 1px solid rgba(220, 220, 220, 0.45); margin-top: 20px; padding-top: 20px; margin-bottom: 0px;">
				<div class="form-group">
					<div class="col-sm-offset-2 col-xs-offset-2 col-sm-6">
					<button type="submit" class="btn btn-lg btn-success btn-block" style="width: 97%; margin-left: -7px;">
						<i class="fa fa-check-circle" style="margin-left:8px;"></i>
						<?=$this->lang->line("update_school")?>
					</button>
					</div>
				</div>
			</div>
		
		</form>
	</div>
</div>
</div>

<script type="text/javascript">
document.getElementById("uploadBtn").onchange = function() {
    document.getElementById("uploadFile").value = this.value;
};
</script>

<script>
  var status = '';
  var id = 0;
  $('.onoffswitch-small-checkbox').click(function() {
      if($(this).prop('checked')) {
          status = 'checked';
          id = $(this).parent().attr("id");
      } else {
          status = 'unchecked';
          id = $(this).parent().attr("id");
      }
      return false;
      if((status != '' || status != null) && (id !='')) {
          $.ajax({
              type: 'POST',
              url: "<?=base_url('school/service_charge_status')?>",
              data: "id=" + id + "&status=" + status,
              dataType: "html",
              success: function(data) {
                  if(data == 'Success') {
                      toastr["success"]("Success")
                      toastr.options = {
                        "closeButton": true,
                        "debug": false,
                        "newestOnTop": false,
                        "progressBar": false,
                        "positionClass": "toast-top-right",
                        "preventDuplicates": false,
                        "onclick": null,
                        "showDuration": "500",
                        "hideDuration": "500",
                        "timeOut": "5000",
                        "extendedTimeOut": "1000",
                        "showEasing": "swing",
                        "hideEasing": "linear",
                        "showMethod": "fadeIn",
                        "hideMethod": "fadeOut"
                      }
                  } else {
                      toastr["error"]("Error")
                      toastr.options = {
                        "closeButton": true,
                        "debug": false,
                        "newestOnTop": false,
                        "progressBar": false,
                        "positionClass": "toast-top-right",
                        "preventDuplicates": false,
                        "onclick": null,
                        "showDuration": "500",
                        "hideDuration": "500",
                        "timeOut": "5000",
                        "extendedTimeOut": "1000",
                        "showEasing": "swing",
                        "hideEasing": "linear",
                        "showMethod": "fadeIn",
                        "hideMethod": "fadeOut"
                      }
                  }
              }
          });
      }
  }); 
</script>


