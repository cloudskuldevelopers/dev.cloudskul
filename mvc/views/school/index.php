<div class="row">
	<div class="col-md-2 hidden-xs">
		<img src="<?php echo base_url('uploads/images/logo.png'); ?>" style="width: auto; height: 40px; margin-top: -8px;" class="img-thumbnail img-responsive" />
	</div>
	<div class="col-md-7 col-xs-9 text-center">
		<h1 class="page-header"><?php echo $this->session->userdata('school');  ?></h1>
	</div>
	<div class="col-md-3 col-xs-3">
		<!-- begin breadcrumb -->
		<ol class="breadcrumb pull-right">
			<li><a href="<?=base_url("dashboard/index")?>"><i class="fa fa-laptop"></i> <?=$this->lang->line('menu_dashboard')?></a></li>
            <li class="active"><?=$this->lang->line('menu_school')?></li>
		</ol>
		<!-- end breadcrumb -->
	</div>
</div>

<div id="page-container">
<!-- begin panel -->
<div class="panel panel-inverse" data-sortable-id="ui-widget-3">
	<div class="panel-heading">
		<h4 class="panel-title"><i class="fa fa-desktop"></i> <?=$this->lang->line('panel_title')?></h4>
	</div>
	<div class="panel-body">
		<?php 
			$usertype = $this->session->userdata("usertype");
			if($usertype == "Super Admin") {
		?>
		<table id="example1" class="table table-striped table-bordered table-hover dataTable no-footer">
		<?php } ?>
		<?php 
			$usertype = $this->session->userdata("usertype");
			if($usertype == "Admin") {
		?>
		<table id="exampl" class="table table-responsive table-hover dataTable no-footer">
		<?php } ?>
			<thead>
				<tr>
					<th class="col-lg-1"><?=$this->lang->line('slno')?></th>
					<th class="col-lg-1"><?=$this->lang->line('school_logo')?></th>
					<th class="col-lg-3"><?=$this->lang->line('school_school')?></th>
					<th class="col-lg-2"><?=$this->lang->line('school_email')?></th>
					<th class="col-lg-2"><?=$this->lang->line('school_phone')?></th>
					<?php 
						$usertype = $this->session->userdata("usertype");
						if($usertype == "Super Admin") {
					?>
					<th class="col-lg-1"><?=$this->lang->line('school_status')?></th>
					<th class="col-lg-2"><?=$this->lang->line('action')?></th>
					<?php } ?>
					<?php 
						$usertype = $this->session->userdata("usertype");
						if($usertype == "Admin") {
					?>
					<th class="col-lg-2"><?=$this->lang->line('action')?></th>
					<?php } ?>
				</tr>
			</thead>
			<tbody>
				<?php if(count($schools)) {$i = 1; foreach($schools as $school) { ?>
					<tr>
						<td data-title="<?=$this->lang->line('slno')?>">
							<?php echo $i; ?>
						</td>
						<td data-title="<?=$this->lang->line('school_school')?>">
							<?php $array = array(
									"src" => base_url('uploads/images/'.$school->photo),
									'width' => '35px',
									'height' => '35px',
									'class' => 'img-thumbnail'

								);
								echo img($array); 
							?>
						</td>
						<td data-title="<?=$this->lang->line('school_school')?>">
							<?php echo $school->school; ?>
						</td>
						<td data-title="<?=$this->lang->line('school_email')?>">
							<span class="label label-primary"><?=$school->email; ?></span>
						</td>
						<td data-title="<?=$this->lang->line('school_phone')?>">
							<?=$school->phone; ?>
						</td>
						<?php 
							$usertype = $this->session->userdata("usertype");
							if($usertype == "Super Admin") {
						?>
						<td data-title="<?=$this->lang->line('school_status')?>">
							<div class="onoffswitch-small" id="<?=$school->schoolID?>">
								<input type="checkbox" id="myonoffswitch<?=$school->schoolID?>" class="onoffswitch-small-checkbox" name="paypal_demo" <?php if($school->schoolactive === '1') echo "checked='checked'"; ?>>
								<label for="myonoffswitch<?=$school->schoolID?>" class="onoffswitch-small-label">
									<span class="onoffswitch-small-inner"></span>
									<span class="onoffswitch-small-switch"></span>
								</label>
							</div>
						</td>

						<td data-title="<?=$this->lang->line('action')?>">
							<?php echo btn_view('school/view/'.$school->schoolID, $this->lang->line('view')) ?>
							<?php echo btn_edit('school/edit/'.$school->schoolID, $this->lang->line('edit')) ?>
							<?php echo btn_delete('school/delete/'.$school->schoolID, $this->lang->line('delete')) ?>
						</td>
						<?php } ?>

						<?php 
							$usertype = $this->session->userdata("usertype");
							if($usertype == "Admin") {
						?>
						<td data-title="<?=$this->lang->line('action')?>">
							<?php echo btn_view('school/view/'.$school->schoolID, $this->lang->line('view')) ?>
							<?php echo btn_edit('school/edit/'.$school->schoolID, $this->lang->line('edit')) ?>
						</td>
						<?php } ?>
					</tr>
				<?php $i++; }} ?>
			</tbody>
		</table>
	</div>
</div>
</div>

<script>
  var status = '';
  var id = 0;
  $('.onoffswitch-small-checkbox').click(function() {
      if($(this).prop('checked')) {
          status = 'chacked';
          id = $(this).parent().attr("id");
      } else {
          status = 'unchacked';
          id = $(this).parent().attr("id");
      }

      if((status != '' || status != null) && (id !='')) {
          $.ajax({
              type: 'POST',
              url: "<?=base_url('school/active')?>",
              data: "id=" + id + "&status=" + status,
              dataType: "html",
              success: function(data) {
                  if(data == 'Success') {
                      toastr["success"]("Success")
                      toastr.options = {
                        "closeButton": true,
                        "debug": false,
                        "newestOnTop": false,
                        "progressBar": false,
                        "positionClass": "toast-top-right",
                        "preventDuplicates": false,
                        "onclick": null,
                        "showDuration": "500",
                        "hideDuration": "500",
                        "timeOut": "5000",
                        "extendedTimeOut": "1000",
                        "showEasing": "swing",
                        "hideEasing": "linear",
                        "showMethod": "fadeIn",
                        "hideMethod": "fadeOut"
                      }
                  } else {
                      toastr["error"]("Error")
                      toastr.options = {
                        "closeButton": true,
                        "debug": false,
                        "newestOnTop": false,
                        "progressBar": false,
                        "positionClass": "toast-top-right",
                        "preventDuplicates": false,
                        "onclick": null,
                        "showDuration": "500",
                        "hideDuration": "500",
                        "timeOut": "5000",
                        "extendedTimeOut": "1000",
                        "showEasing": "swing",
                        "hideEasing": "linear",
                        "showMethod": "fadeIn",
                        "hideMethod": "fadeOut"
                      }
                  }
              }
          });
      }
  }); 
</script>

