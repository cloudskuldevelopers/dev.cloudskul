<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Cloudskul ::: <?=$this->lang->line('panel_title')?></title>
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        <!--link rel="SHORTCUT ICON" href="<?php //base_url("uploads/images/$siteinfos->photo") ?>" /-->
        <link rel="icon" href="<?=base_url("uploads/images/cloud_icon.ico")?>" type="image/x-icon" />
        <link rel="shortcut icon" href="<?=base_url("uploads/images/cloud_icon.ico")?>" type="image/x-icon" />
        <script type="text/javascript" src="<?php echo base_url('assets/cloudskul/jquery.js'); ?>"></script>
		
        <!-- ================== BEGIN BASE CSS STYLE ================== -->
		<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet">
		<link href="<?php echo base_url('assets/theme/plugins/jquery-ui/themes/base/minified/jquery-ui.min.css'); ?>" rel="stylesheet" />
		<link href="<?php echo base_url('assets/theme/plugins/bootstrap/css/bootstrap.min.css'); ?>" rel="stylesheet" />
		<link href="<?php echo base_url('assets/theme/plugins/font-awesome/css/font-awesome.min.css'); ?>" rel="stylesheet" />
		<link href="<?php echo base_url('assets/theme/css/animate.min.css'); ?>" rel="stylesheet" />
		<link href="<?php echo base_url('assets/theme/css/style.min.css'); ?>" rel="stylesheet" />
		<link href="<?php echo base_url('assets/theme/css/style-responsive.min.css'); ?>" rel="stylesheet" />
		<link href="<?php echo base_url('assets/theme/css/theme/default.css'); ?>" rel="stylesheet" id="theme" />
		<link href="<?php echo base_url('assets/datatables/dataTables.bootstrap.css'); ?>" rel="stylesheet">
		<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.2/css/select2.min.css" rel="stylesheet" />
		<!--link href="assets/plugins/bootstrap-calendar/css/bootstrap_calendar.css" rel="stylesheet" /-->
		<!-- ================== END BASE CSS STYLE ================== -->
		
		<!-- ================== BEGIN PAGE LEVEL CSS STYLE ================== -->
		<link href="<?php echo base_url('assets/theme/plugins/jquery-jvectormap/jquery-jvectormap-1.2.2.css'); ?>" rel="stylesheet" />
		<link href="<?php echo base_url('assets/theme/plugins/bootstrap-calendar/css/bootstrap_calendar.css'); ?>" rel="stylesheet" />
		<link href="<?php echo base_url('assets/theme/plugins/gritter/css/jquery.gritter.css'); ?>" rel="stylesheet" />
		<link href="<?php echo base_url('assets/theme/plugins/morris/morris.css'); ?>" rel="stylesheet" />
		<link href="<?php echo base_url('assets/theme/plugins/ionicons/css/ionicons.min.css'); ?>" rel="stylesheet" />
		<link href="<?php echo base_url('assets/theme/plugins/bootstrap-wysihtml5/src/bootstrap-wysihtml5.css'); ?>" rel="stylesheet" />
		<link href="<?php echo base_url('assets/theme/css/cloudskul.css'); ?>" rel="stylesheet" />
		<!-- ================== END PAGE LEVEL CSS STYLE ================== -->
		
		<!-- ================== BEGIN BASE JS ================== -->
		<script src="<?php echo base_url('assets/theme/plugins/jquery/jquery-1.9.1.min.js'); ?>"></script>
		<script src="<?php echo base_url('assets/theme/plugins/jquery/jquery-migrate-1.1.0.min.js'); ?>"></script>
		<script src="<?php echo base_url('assets/theme/plugins/jquery-ui/ui/minified/jquery-ui.min.js'); ?>"></script>
		<script src="<?php echo base_url('assets/theme/plugins/pace/pace.min.js'); ?>"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.2/js/select2.min.js"></script>
		<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/underscore.js/1.4.4/underscore-min.js"></script>
		<!-- ================== END BASE JS ================== -->
		
		<!-- ================== TOASTR ==================== -->
		<link href="<?php echo base_url('assets/theme/toastr/toastr.min.css'); ?>" rel="stylesheet">
		<script type="text/javascript" src="<?php echo base_url('assets/theme/toastr/toastr.min.js'); ?>"></script>
		
		<script type="text/javascript">
			/* var script = document.createElement('script');
			script.type = 'text/javascript';
			script.src = 'https://api.affin.io/scripts/libs/underscore-min.js';
			document.head.appendChild(script);*/
		</script>
    </head>
    <body class="skin-blue sidebar-mini">
		<!-- begin #page-loader -->
		<div id="page-loader" class="fade in"><span class="spinner"></span></div>
		<!-- end #page-loader -->