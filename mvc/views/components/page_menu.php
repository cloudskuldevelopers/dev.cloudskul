<!-- begin #sidebar -->
<div id="sidebar" class="sidebar">
	<!-- begin sidebar scrollbar -->
	<div data-scrollbar="true" data-height="100%">
		<!-- begin sidebar user -->
		<ul class="nav">
			<li class="nav-profile">
				<div class="image">
					<a href=""><img src="<?=base_url("uploads/images/".$this->session->userdata('photo')); ?>" class="img-circle" alt="User Image" style="width: 50px; height: 50px;" /></a>
				</div>
				<div class="info">
					<?php
						$name = $this->session->userdata("name");
						if(strlen($name) > 11) {
						   $name = substr($name, 0,11). ".."; 
						}
						echo $name;
					?>
					<small><?=$this->lang->line(str_replace(' ', '', $this->session->userdata("usertype")))?></small>
				</div>
			</li>
		</ul>
		<!-- end sidebar user -->
		<!-- begin sidebar nav -->
		<?php $usertype = $this->session->userdata("usertype"); ?>
		<ul class="nav search_enabled">
			<li class="nav-header">Navigation</li>
			
			<li class="<? echo ($this->uri->segment(1)=='dashboard') ? 'active' : ''; ?>">
				<a href="<?php echo base_url(); ?>dashboard/index"><i class="fa fa-laptop"></i> <span><?php echo $this->lang->line('menu_dashboard')?></span></a>
			</li>
			
			<?php if($usertype == "Admin" || $usertype == "Super Admin") { ?>
				<li class="has-sub <? echo ($this->uri->segment(1)=='feetype' || $this->uri->segment(1)=='invoice' || $this->uri->segment(1)=='bank_account' || $this->uri->segment(1)=='new_invoice' || $this->uri->segment(1)=='balance' || $this->uri->segment(1)=='expense') ? 'active treeview' : ''; ?>">
					<a href="javascript:;">
						<b class="caret pull-right"></b>
						<i class="fa fa-suitcase"></i> 
						<span><?=$this->lang->line('menu_account');?></span>
					</a>
					<ul class="sub-menu">
						<?php if($usertype == "Admin") { ?>
							<li class="<? echo ($this->uri->segment(1)=='bank_account') ? 'active' : '';?>">
								<a href="<?php echo base_url(); ?>bank_account/index"><span><?php echo $this->lang->line('menu_bank_account')?></span></a>
							</li>
						<?php } ?>
						<li class="<? echo ($this->uri->segment(1)=='feetype') ? 'active' : '';?>">
							<a href="<?php echo base_url(); ?>feetype/index"><span><?php echo $this->lang->line('menu_feetype')?></span></a>
						</li>
						<li class="<? echo ($this->uri->segment(1)=='invoice') ? 'active' : '';?>">
							<a href="<?php echo base_url(); ?>invoice/index"><span><?php echo $this->lang->line('menu_invoice')?></span></a>
						</li>
						<li class="<? echo ($this->uri->segment(1)=='new_invoice') ? 'active' : '';?>">
							<a href="<?php echo base_url(); ?>new_invoice/index"><span> New Invoice</span></a>
						</li>
						<li class="<? echo ($this->uri->segment(1)=='balance') ? 'active' : '';?>">
							<a href="<?php echo base_url(); ?>balance/index"><span><?php echo $this->lang->line('menu_balance')?></span></a>
						</li>
						<li class="<? echo ($this->uri->segment(1)=='expense') ? 'active' : '';?>">
							<a href="<?php echo base_url(); ?>expense/index"><span><?php echo $this->lang->line('menu_expense')?></span></a>
						</li>
					</ul>
				</li>
            <?php } ?>
			
			<?php if($usertype == "Accountant") { ?>
				<li class="<? echo ($this->uri->segment(1)=='feetype') ? 'active' : '';?>">
					<a href="<?php echo base_url(); ?>feetype/index"><span><?php echo $this->lang->line('menu_feetype')?></span></a>
				</li>
				<li class="<? echo ($this->uri->segment(1)=='invoice') ? 'active' : '';?>">
					<a href="<?php echo base_url(); ?>invoice/index"><span><?php echo $this->lang->line('menu_invoice')?></span></a>
				</li>
				<li class="<? echo ($this->uri->segment(1)=='balance') ? 'active' : '';?>">
					<a href="<?php echo base_url(); ?>balance/index"><span><?php echo $this->lang->line('menu_balance')?></span></a>
				</li>
				<li class="<? echo ($this->uri->segment(1)=='expense') ? 'active' : '';?>">
					<a href="<?php echo base_url(); ?>expense/index"><span><?php echo $this->lang->line('menu_expense')?></span></a>
				</li>
			<?php } ?>

			<?php if($usertype == "Student" || $usertype == "Parent") { ?>
				<li class="<? echo ($this->uri->segment(1)=='invoice') ? 'active' : '';?>">
					<a href="<?php echo base_url(); ?>invoice/index"><span><?php echo $this->lang->line('menu_invoice')?></span></a>
				</li>
				<li class="<? echo ($this->uri->segment(1)=='new_invoice') ? 'active' : '';?>">
					<a href="<?php echo base_url(); ?>new_invoice/index"><span> New Invoice</span></a>
				</li>
			<?php } ?>
			
			<?php if($usertype == "Admin" || $usertype == "Super Admin" || $usertype == "Student" || $usertype == "Parent" || $usertype == "Teacher") { ?>
				<li class="has-sub <? echo ($this->uri->segment(1)=='student' || $this->uri->segment(1)=='parentes' || $this->uri->segment(1)=='teacher' || $this->uri->segment(1)=='user') ? 'active treeview' : ''; ?>">
					<a href="javascript:;">
						<b class="caret pull-right"></b>
						<i class="fa fa-male"></i> 
						<span>People</span>
					</a>
					<ul class="sub-menu">
						<?php if($usertype == "Admin" || $usertype == "Super Admin" || $usertype == "Teacher") { ?>
							<li class="<? echo ($this->uri->segment(1)=='student') ? 'active' : '';?>">
								<a href="<?php echo base_url(); ?>student/index"><span><?php echo $this->lang->line('menu_student')?></span></a>
							</li>
						<?php } ?>
						
						<?php if($usertype == "Admin" || $usertype == "Super Admin") { ?>
							<li class="<? echo ($this->uri->segment(1)=='parentes') ? 'active' : '';?>">
								<a href="<?php echo base_url(); ?>parentes/index"><span><?php echo $this->lang->line('menu_parent')?></span></a>
							</li>
						<?php } ?>
						
						<?php if($usertype) { ?>
							<li class="<? echo ($this->uri->segment(1)=='teacher') ? 'active' : '';?>">
								<a href="<?php echo base_url(); ?>teacher/index"><span><?php echo $this->lang->line('menu_teacher')?></span></a>
							</li>
						<?php } ?>
						
						<?php if($usertype == "Admin" || $usertype == "Super Admin") { ?>
							<li class="<? echo ($this->uri->segment(1)=='user') ? 'active' : '';?>">
								<a href="<?php echo base_url(); ?>user/index"><span><?php echo $this->lang->line('menu_user')?></span></a>
							</li>
						<?php } ?>
					</ul>
				</li>
			<?php } ?>
			
			<?php if($usertype == "Admin" || $usertype == "Super Admin" || $usertype == "Student" || $usertype == "Parent" || $usertype == "Teacher") { ?>
				<li class="has-sub <? echo ($this->uri->segment(1)=='classes' || $this->uri->segment(1)=='section' || $this->uri->segment(1)=='subject' || $this->uri->segment(1)=='grade' || $this->uri->segment(1)=='routine') ? 'active treeview' : ''; ?>">
					<a href="javascript:;">
						<b class="caret pull-right"></b>
						<i class="fa fa-book"></i> 
						<span><?php echo $this->lang->line('menu_academics')?></span>
					</a>
					<ul class="sub-menu">
						<?php if($usertype == "Admin" || $usertype == "Super Admin") { ?>
							<li class="<? echo ($this->uri->segment(1)=='classes') ? 'active' : '';?>">
								<a href="<?php echo base_url(); ?>classes/index"><span><?php echo $this->lang->line('menu_classes')?></span></a>
							</li>
						<?php } ?>
						
						<?php if($usertype == "Admin" || $usertype == "Super Admin") { ?>
							<li class="<? echo ($this->uri->segment(1)=='section') ? 'active' : '';?>">
								<a href="<?php echo base_url(); ?>section/index"><span><?php echo $this->lang->line('menu_section')?></span></a>
							</li>
						<?php } ?>
						
						<?php if($usertype == "Super Admin" || $usertype == "Admin" || $usertype == "Student" || $usertype == "Parent" || $usertype == "Librarian" || $usertype == "Teacher") { ?>
							<li class="<? echo ($this->uri->segment(1)=='subject') ? 'active' : '';?>">
								<a href="<?php echo base_url(); ?>subject/index"><span><?php echo $this->lang->line('menu_subject')?></span></a>
							</li>
						<?php } ?>
						
						<?php if($usertype == "Admin" || $usertype == "Super Admin") { ?>
							<li class="<? echo ($this->uri->segment(1)=='subject') ? 'active' : '';?>">
								<a href="<?php echo base_url(); ?>subject/category"><span>Subject Category</span></a>
							</li>
						<?php } ?>
						
						<?php if($usertype == "Admin" || $usertype == "Super Admin") { ?>
							<li class="<? echo ($this->uri->segment(1)=='grade') ? 'active' : '';?>">
								<a href="<?php echo base_url(); ?>grade/index"><span><?php echo $this->lang->line('menu_grade')?></span></a>
							</li>
						<?php } ?>
						
						<?php if($usertype == "Admin" || $usertype == "Super Admin" || $usertype == "Student" || $usertype == "Parent" || $usertype == "Teacher") { ?>
							<li class="<? echo ($this->uri->segment(1)=='routine') ? 'active' : '';?>">
								<a href="<?php echo base_url(); ?>routine/index"><span><?php echo $this->lang->line('menu_routine')?></span></a>
							</li>
						<?php } ?>
					</ul>
				</li>
			<?php } ?>
			
		
		
					<!-- Performance -->

		<?php if($usertype == "Admin" || $usertype == "Super Admin" || $usertype == "Student" || $usertype == "Parent" || $usertype == "Teacher") { ?>
				<li class="has-sub <? echo ($this->uri->segment(1)=='assignment' || $this->uri->segment(1)=='exam' || $this->uri->segment(1)=='assessments' || $this->uri->segment(1)=='examschedule' || $this->uri->segment(1)=='mark' || $this->uri->segment(1)=='sattendance' || $this->uri->segment(1)=='tattendance' || $this->uri->segment(1)=='assessmentsettings' || $this->uri->segment(1)=='eattendance' || $this->uri->segment(1)=='promotion') ? 'active treeview' : ''; ?>">
					<a href="javascript:;">
						<b class="caret pull-right"></b>
						<i class="fa fa-bar-chart-o"></i> 
						<span><?php echo $this->lang->line('menu_performance')?></span>
					</a>
					<ul class="sub-menu">
						<?php if($this->session->userdata("usertype") == "Admin" || $this->session->userdata("usertype") == "Super Admin" || $this->session->userdata('usertype') == 'Teacher' || $this->session->userdata('usertype') == 'Student') { ?>
							<li class="<? echo ($this->uri->segment(1)=='assignment') ? 'active' : '';?>">
								<a href="<?php echo base_url(); ?>assignment/index"><span><?php echo $this->lang->line('menu_assignment')?></span></a>
							</li>
						<?php } ?>
						
						<?php if($usertype == "Admin" || $usertype == "Super Admin") { ?>
							<li class="<? echo ($this->uri->segment(1)=='exam') ? 'active' : '';?>">
								<a href="<?php echo base_url(); ?>exam/index"><span><?php echo $this->lang->line('menu_exam')?></span></a>
							</li>
						<?php } ?>
						
						<?php if($usertype == "Admin" || $usertype == "Super Admin") { ?>
							<li class="<? echo ($this->uri->segment(1)=='assessments') ? 'active' : '';?>">
								<a href="<?php echo base_url(); ?>assessments/index"><span>Assessments</span></a>
							</li>
						<?php } ?>
						
						<?php if($usertype == "Admin" || $usertype == "Super Admin") { ?>
							<li class="<? echo ($this->uri->segment(1)=='examschedule') ? 'active' : '';?>">
								<a href="<?php echo base_url(); ?>examschedule/index"><span><?php echo $this->lang->line('menu_examschedule')?></span></a>
							</li>
						<?php } ?>
						
						<?php if($usertype == "Student" || $usertype == "Parent" || $usertype == "Teacher") { ?>
							<li class="<? echo ($this->uri->segment(1)=='examschedule') ? 'active' : '';?>">
								<a href="<?php echo base_url(); ?>examschedule/index"><span><?php echo $this->lang->line('menu_examschedule')?></span></a>
							</li>
						<?php } ?>
						
						<?php if($usertype == "Admin" || $usertype == "Super Admin" || $usertype == "Teacher") { ?>
							<li class="<? echo ($this->uri->segment(1)=='mark') ? 'active' : '';?>">
								<a href="<?php echo base_url(); ?>mark/index"><span><?php echo $this->lang->line('menu_mark')?></span></a>
							</li>
						<?php } ?>
						
						<?php if($usertype == "Student") { ?>
							<li class="<? echo ($this->uri->segment(1)=='mark') ? 'active' : '';?>">
								<a href="<?php echo base_url(); ?>mark/view"><span><?php echo $this->lang->line('menu_mark')?></span></a>
							</li>
						<?php } ?>
						
						<?php if($usertype == "Parent") { ?>
							<li class="<? echo ($this->uri->segment(1)=='mark') ? 'active' : '';?>">
								<a href="<?php echo base_url(); ?>mark/index"><span><?php echo $this->lang->line('menu_mark')?></span></a>
							</li>
						<?php } ?>
						
						<?php if($usertype == "Admin" || $usertype == "Super Admin" || $usertype == "Teacher") { ?>
							<li class="<? echo ($this->uri->segment(1)=='sattendance') ? 'active' : '';?>">
								<a href="<?php echo base_url(); ?>sattendance/index"><span><?php echo $this->lang->line('menu_sattendance')?></span></a>
							</li>
						<?php } ?>
						
						<?php if($usertype == "Teacher") { ?>
							<li class="<? echo ($this->uri->segment(1)=='sattendance') ? 'active' : '';?>">
								<a href="<?php echo base_url(); ?>sattendance/index"><span><?php echo $this->lang->line('menu_sattendance')?></span></a>
							</li>
						<?php } ?>
						
						<?php if($usertype == "Teacher") { ?>
							<li class="<? echo ($this->uri->segment(1)=='tattendance') ? 'active' : '';?>">
								<a href="<?php echo base_url(); ?>tattendance/view"><span><?php echo $this->lang->line('menu_tattendance')?></span></a>
							</li>
						<?php } else { ?>
							<li class="<? echo ($this->uri->segment(1)=='tattendance') ? 'active' : '';?>">
								<a href="<?php echo base_url(); ?>tattendance/index"><span><?php echo $this->lang->line('menu_tattendance')?></span></a>
							</li>
						<?php } ?>
						
						<?php if($usertype == "Admin" || $usertype == "Super Admin" || $usertype == "Teacher") { ?>
							<li class="<? echo ($this->uri->segment(1)=='eattendance') ? 'active' : '';?>">
								<a href="<?php echo base_url(); ?>eattendance/index"><span><?php echo $this->lang->line('menu_eattendance')?></span></a>
							</li>
						<?php } ?>
						
						<?php if($usertype == "Admin" || $usertype == "Super Admin" || $usertype == "Teacher") { ?>
							<li class="<? echo ($this->uri->segment(1)=='promotion') ? 'active' : '';?>">
								<a href="<?php echo base_url(); ?>promotion/index"><span><?php echo $this->lang->line('menu_promotion')?></span></a>
							</li>
						<?php } ?>
						
						<?php if($usertype == "Admin" || $usertype == "Super Admin") { ?>
							<li class="<? echo ($this->uri->segment(1)=='assessmentsettings') ? 'active' : '';?>">
								<a href="<?php echo base_url(); ?>assessmentsettings/index"><span>Settings</span></a>
							</li>
						<?php } ?>
						
						<?php if($usertype == "Student" || $usertype == "Parent") { ?>
							<li class="<? echo ($this->uri->segment(1)=='sattendance') ? 'active' : '';?>">
								<a href="<?php echo base_url(); ?>sattendance/view"><span><?php echo $this->lang->line('menu_attendance') ?></span></a>
							</li>
						<?php } ?>
					</ul>
				</li>
			<?php } ?>
			
			<!-- End Performance -->
			
			
			<!--Child Care -->
			
			<?php if($usertype == "Admin" || $usertype == "Super Admin" || $usertype == "Parent") { ?>
				<li class="has-sub <? echo ($this->uri->segment(1)=='child_care') ? 'active treeview' : ''; ?>">
					<a href="javascript:;">
						<b class="caret pull-right"></b>
						<i class="fa fa-female"></i> 
						<span><?=$this->lang->line('menu_childcare')?></span>
					</a>
					<ul class="sub-menu">
						<li class="<? echo ($this->uri->segment(1)=='child_care') ? 'active' : '';?>">
							<a href="<?php echo base_url(); ?>child_care/index"><span><?php echo $this->lang->line('menu_pickup') ?></span></a>
						</li>
					</ul>
				</li>
			<?php } ?>
			
			<!--  End Child Care -->
			
			
			
			<!-- Messages -->
			
			<?php if($usertype == "Admin" || $usertype == "Super Admin" || $usertype == "Student" || $usertype == "Parent") { ?>
				<li class="has-sub <? echo ($this->uri->segment(1)=='mailandsmstemplate' || $this->uri->segment(1)=='mailandsms' || $this->uri->segment(1)=='message' || $this->uri->segment(1)=='notice') ? 'active treeview' : ''; ?>">
					<a href="javascript:;">
						<b class="caret pull-right"></b>
						<i class="fa fa-inbox "></i> 
						<span><?php echo $this->lang->line('menu_message') ?></span>
					</a>
					<ul class="sub-menu">
						<?php if($usertype == "Admin" || $usertype == "Super Admin") { ?>
							<li class="<? echo ($this->uri->segment(1)=='mailandsmstemplate') ? 'active' : '';?>">
								<a href="<?php echo base_url(); ?>mailandsmstemplate/index"><span><?php echo $this->lang->line('menu_mailandsmstemplate') ?></span></a>
							</li>
						<?php } ?>
						
						<?php if($usertype == "Admin" || $usertype == "Super Admin") { ?>
							<li class="<? echo ($this->uri->segment(1)=='mailandsms') ? 'active' : '';?>">
								<a href="<?php echo base_url(); ?>mailandsms/index"><span><?php echo $this->lang->line('menu_mailandsms') ?></span></a>
							</li>
						<?php } ?>
						
						<?php if($usertype) { ?>
							<li class="<? echo ($this->uri->segment(1)=='message') ? 'active' : '';?>">
								<a href="<?php echo base_url(); ?>message/index"><span><?php echo $this->lang->line('menu_message') ?></span></a>
							</li>
						<?php } ?>
						
						<?php if($usertype) { ?>
							<li class="<? echo ($this->uri->segment(1)=='notice') ? 'active' : '';?>">
								<a href="<?php echo base_url(); ?>notice/index"><span><?php echo $this->lang->line('menu_notice') ?></span></a>
							</li>
						<?php } ?>
						
						
					</ul>
				</li>
			<?php } ?>
			
			<!-- End Messages -->
			
			
			
			
			
			<!-- REPORT -->
			
			
				<?php if($usertype) { ?>
				<li class="<? echo ($this->uri->segment(1)=='report') ? 'active' : '';?>">
					<a href="<?php echo base_url(); ?>report/index"><i class="fa fa-file-text"></i> <span><?php echo $this->lang->line('menu_report') ?></span></a>
				</li>
			<?php } ?>
			
			
			<!-- End Report -->
			
			
			
			
			<?php if($usertype == "Super Admin" || $usertype == "Admin" ) { ?>
				<li class="has-sub <? echo ($this->uri->segment(1)=='department' || $this->uri->segment(1)=='employee')  ? 'active treeview' : ''; ?>">
					<a href="javascript:;">
						<b class="caret pull-right"></b>
						<i class="fa fa-user  "></i> 
						<span>Employee Mgt</span>
					</a>
					<ul class="sub-menu">
						<li class="<? echo ($this->uri->segment(1)=='department') ? 'active' : '';?>">
							<a href="<?php echo base_url(); ?>department/index"><span>Departments</span></a>
						</li>
						<li class="<? echo ($this->uri->segment(1)=='employee') ? 'active' : '';?>">
							<a href="<?php echo base_url(); ?>employee/index"><span>Employee</span></a>
						</li>
					</ul>
				</li>
			<?php } ?>
			
			<?php if($usertype == "Super Admin" || $usertype == "Admin" ) { ?>
				<li class="has-sub <? echo ($this->uri->segment(1)=='payroll_template' || $this->uri->segment(1)=='salary_details')  ? 'active treeview' : ''; ?>">
					<a href="javascript:;">
						<b class="caret pull-right"></b>
						<i class="fa fa-money  "></i> 
						<span>Payroll</span>
					</a>
					<ul class="sub-menu">
						<li class="<? echo ($this->uri->segment(1)=='payroll_template') ? 'active' : '';?>">
							<a href="<?php echo base_url(); ?>payroll_template/index"><span>Salary Template</span></a>
						</li>
						<li class="<? echo ($this->uri->segment(1)=='salary_details' && $this->uri->segment(2)=='index') ? 'active' : '';?>">
							<a href="<?php echo base_url(); ?>salary_details/index"><span>Set Salary Details</span></a>
						</li>
						<li class="<? echo ($this->uri->segment(1)=='salary_details' && $this->uri->segment(2)=='salary_list')  ? 'active' : ''; ?>">
							<a href="<?php echo base_url(); ?>salary_details/salary_list"><span>Employee Salary List</span></a>
						</li>
					</ul>
				</li>
			<?php } ?>
			
			<?php if($usertype == "Admin" || $usertype == "Super Admin") { ?>
				<li class="has-sub <? echo ($this->uri->segment(1)=='lmember' || $this->uri->segment(1)=='book' || $this->uri->segment(1)=='issue') ? 'active treeview' : ''; ?>">
					<a href="javascript:;">
						<b class="caret pull-right"></b>
						<i class="fa fa-bank   "></i> 
						<span><?=$this->lang->line('menu_library');?></span>
					</a>
					<ul class="sub-menu">
						<li class="<? echo ($this->uri->segment(1)=='lmember') ? 'active' : '';?>">
							<a href="<?php echo base_url(); ?>lmember/index"><span><?php echo $this->lang->line('menu_member') ?></span></a>
						</li>
						<li class="<? echo ($this->uri->segment(1)=='book') ? 'active' : '';?>">
							<a href="<?php echo base_url(); ?>book/index"><span><?php echo $this->lang->line('menu_books') ?></span></a>
						</li>
						<li class="<? echo ($this->uri->segment(1)=='issue') ? 'active' : '';?>">
							<a href="<?php echo base_url(); ?>issue/index"><span><?php echo $this->lang->line('menu_issue') ?></span></a>
						</li>
						<li class="<? echo ($this->uri->segment(1)=='issue') ? 'active' : '';?>">
							<a href="<?php echo base_url(); ?>issue/fine"><span><?php echo $this->lang->line('menu_fine') ?></span></a>
						</li>
					</ul>
				</li>
			<?php } ?>
			
			<?php if($usertype == "Librarian") { ?>
				<li class="<? echo ($this->uri->segment(1)=='lmember') ? 'active' : '';?>">
					<a href="<?php echo base_url(); ?>lmember/index"><span><?php echo $this->lang->line('menu_member') ?></span></a>
				</li>
				<li class="<? echo ($this->uri->segment(1)=='issue') ? 'active' : '';?>">
					<a href="<?php echo base_url(); ?>issue/fine"><span><?php echo $this->lang->line('menu_fine') ?></span></a>
				</li>
			<?php } ?>
			
			<?php if($usertype == "Librarian" || $usertype == "Parent" || $usertype == "Teacher") { ?>
				<li class="<? echo ($this->uri->segment(1)=='book') ? 'active' : '';?>">
					<a href="<?php echo base_url(); ?>book/index"><span><?php echo $this->lang->line('menu_books') ?></span></a>
				</li>
			<?php } ?>
			
			<?php if($usertype == "Librarian" || $usertype == "Parent") { ?>
				<li class="<? echo ($this->uri->segment(1)=='issue') ? 'active' : '';?>">
					<a href="<?php echo base_url(); ?>issue/index"><span><?php echo $this->lang->line('menu_issue') ?></span></a>
				</li>
			<?php } ?>
			
			<?php if($usertype == "Student") { ?>
				<li class="has-sub">
					<a href="javascript:;">
						<b class="caret pull-right"></b>
						<i class="fa fa-bank   "></i> 
						<span><?=$this->lang->line('menu_library');?></span>
					</a>
					<ul class="sub-menu">
						<li class="<? echo ($this->uri->segment(1)=='book') ? 'active' : '';?>">
							<a href="<?php echo base_url(); ?>book/index"><span><?php echo $this->lang->line('menu_books') ?></span></a>
						</li>
						<li class="<? echo ($this->uri->segment(1)=='issue') ? 'active' : '';?>">
							<a href="<?php echo base_url(); ?>issue/index"><span><?php echo $this->lang->line('menu_issue') ?></span></a>
						</li>
						<li class="<? echo ($this->uri->segment(1)=='lmember') ? 'active' : '';?>">
							<a href="<?php echo base_url(); ?>lmember/view"><span><?php echo $this->lang->line('menu_profile') ?></span></a>
						</li>
					</ul>
				</li>
			<?php } ?>
			
			<?php if($usertype == "Admin" || $usertype == "Super Admin" || $usertype == "Accountant") { ?>
				<li class="has-sub <? echo ($this->uri->segment(1)=='transport' || $this->uri->segment(1)=='tmember') ? 'active treeview' : ''; ?>">
					<a href="javascript:;">
						<b class="caret pull-right"></b>
						<i class="fa fa-train    "></i> 
						<span><?=$this->lang->line('menu_transport');?></span>
					</a>
					<ul class="sub-menu">
						<li class="<? echo ($this->uri->segment(1)=='transport') ? 'active' : '';?>">
							<a href="<?php echo base_url(); ?>transport/index"><span><?php echo $this->lang->line('menu_transport') ?></span></a>
						</li>
						<li class="<? echo ($this->uri->segment(1)=='tmember') ? 'active' : '';?>">
							<a href="<?php echo base_url(); ?>tmember/index"><span><?php echo $this->lang->line('menu_member') ?></span></a>
						</li>
					</ul>
				</li>
			<?php } ?>
			
			<?php if($usertype == "Student") { ?>
				<li class="has-sub">
					<a href="javascript:;">
						<b class="caret pull-right"></b>
						<i class="fa fa-train    "></i> 
						<span><?=$this->lang->line('menu_transport');?></span>
					</a>
					<ul class="sub-menu">
						<li class="<? echo ($this->uri->segment(1)=='transport') ? 'active' : '';?>">
							<a href="<?php echo base_url(); ?>transport/index"><span><?php echo $this->lang->line('menu_transport') ?></span></a>
						</li>
						<li class="<? echo ($this->uri->segment(1)=='tmember') ? 'active' : '';?>">
							<a href="<?php echo base_url(); ?>tmember/view"><span><?php echo $this->lang->line('menu_profile') ?></span></a>
						</li>
					</ul>
				</li>
			<?php } ?>
			
			<?php if($usertype == "Parent" || $usertype == "Teacher" || $usertype == "Librarian") { ?>
				<li class="<? echo ($this->uri->segment(1)=='transport') ? 'active' : '';?>">
					<a href="<?php echo base_url(); ?>transport/index"><span><?php echo $this->lang->line('menu_transport') ?></span></a>
				</li>
			<?php } ?>
			
			  <?php if($usertype) { ?>
				<li class="has-sub <? echo ($this->uri->segment(1)=='hostel' || $this->uri->segment(1)=='category' || $this->uri->segment(1)=='hmember') ? 'active treeview' : ''; ?>">
					<a href="javascript:;">
						<b class="caret pull-right"></b>
						<i class="fa fa-bed    "></i> 
						<span><?=$this->lang->line('menu_hostel');?></span>
					</a>
					<ul class="sub-menu">
						<li class="<? echo ($this->uri->segment(1)=='hostel') ? 'active' : '';?>">
							<a href="<?php echo base_url(); ?>hostel/index"><span><?php echo $this->lang->line('menu_hostel') ?></span></a>
						</li>
						<li class="<? echo ($this->uri->segment(1)=='category') ? 'active' : '';?>">
							<a href="<?php echo base_url(); ?>category/index"><span><?php echo $this->lang->line('menu_category') ?></span></a>
						</li>
						<li class="<? echo ($this->uri->segment(1)=='hmember') ? 'active' : '';?>">
							<?php if($usertype == "Admin" || $usertype == "Super Admin" || $usertype == "Accountant") { echo anchor('hmember/index', '<span>'.$this->lang->line('menu_member').'</span>'); } ?>
						</li>
						<li class="<? echo ($this->uri->segment(1)=='hmember') ? 'active' : '';?>">
							<?php if($usertype == "Student") { echo anchor('hmember/view', '<span>'.$this->lang->line('menu_profile').'</span>'); } ?>
						</li>
					</ul>
				</li>
			<?php } ?>
			
			<?php if($usertype) { ?>
				<li class="<? echo ($this->uri->segment(1)=='media') ? 'active' : '';?>">
					<a href="<?php echo base_url(); ?>media/index"><i class="fa fa-film"></i> <span><?php echo $this->lang->line('menu_media') ?></span></a>
				</li>
			<?php } ?>
			
			
			<?php if($usertype == "Admin" || $usertype == "Super Admin") { ?>
				<li class="<? echo ($this->uri->segment(1)=='bulkimport') ? 'active' : '';?>">
					<a href="<?php echo base_url(); ?>bulkimport/index"><i class="fa fa-download "></i><span><?php echo $this->lang->line('menu_import') ?></span></a>
				</li>
			<?php } ?>
			
			<?php if($usertype == "Super Admin") { ?>
				<li class="<? echo ($this->uri->segment(1)=='school') ? 'active' : '';?>">
					<a href="<?php echo base_url(); ?>school/index"><i class="fa fa-graduation-cap"></i><span><?php echo $this->lang->line('menu_school') ?></span></a>
				</li>
			<?php } ?>
			
			<?php if($usertype == "Super Admin") { ?>
				<li class="<? echo ($this->uri->segment(1)=='applied_school') ? 'active' : '';?>">
					<a href="<?php echo base_url(); ?>applied_school/index"><i class="fa fa-graduation-cap"></i><span><?php echo $this->lang->line('menu_applied_school') ?></span></a>
				</li>
			<?php } ?>
			
			
			<?php if($usertype == "Super Admin" || $usertype == "Admin" ) { ?>
				<li class="has-sub <? echo ($this->uri->segment(1)=='setting' || $this->uri->segment(1)=='systemadmin' || $this->uri->segment(1)=='school' || $this->uri->segment(1)=='academic_year' || $this->uri->segment(1)=='smssettings' || $this->uri->segment(1)=='payment_settings' || $this->uri->segment(1)=='bank_account')  ? 'active treeview' : ''; ?>">
					<a href="javascript:;">
						<b class="caret pull-right"></b>
						<i class="fa fa-gears    "></i> 
						<span><?=$this->lang->line('menu_setting');?></span>
					</a>
					<ul class="sub-menu">
						<?php if($this->session->userdata("usertype") == "Super Admin") { ?>
							<li class="<? echo ($this->uri->segment(1)=='setting') ? 'active' : '';?>">
								<a href="<?php echo base_url(); ?>setting/index"><i class="fa fa-gears"></i> <span><?php echo $this->lang->line('menu_setting') ?></span></a>
							</li>
							<li class="<? echo ($this->uri->segment(1)=='systemadmin') ? 'active' : '';?>">
								<a href="<?php echo base_url(); ?>systemadmin/index"><i class="fa fa-gears"></i> <span><?php echo $this->lang->line('menu_systemadmin') ?></span></a>
							</li>
						<?php } ?>
						
						<?php if($this->session->userdata("usertype") == "Admin") { ?>
							<li class="<? echo ($this->uri->segment(1)=='school') ? 'active' : '';?>">
								<a href="<?php echo base_url(); ?>school/index"><span><?php echo $this->lang->line('menu_my_school') ?></span></a>
							</li>
						<?php } ?>
						
						<li class="<? echo ($this->uri->segment(1)=='academic_year') ? 'active' : '';?>">
							<a href="<?php echo base_url(); ?>academic_year/index"><span><?php echo $this->lang->line('menu_academic_year') ?></span></a>
						</li>
						
						<?php if($this->session->userdata("usertype") == "Super Admin") { ?>
							<li class="<? echo ($this->uri->segment(1)=='smssettings') ? 'active' : '';?>">
								<a href="<?php echo base_url(); ?>smssettings/index"><span><?php echo $this->lang->line('menu_smssettings') ?></span></a>
							</li>
							<li class="<? echo ($this->uri->segment(1)=='payment_settings') ? 'active' : '';?>">
								<a href="<?php echo base_url(); ?>payment_settings/index"><span><?php echo $this->lang->line('menu_paymentsettings') ?></span></a>
							</li>
							<li class="<? echo ($this->uri->segment(1)=='bank_account') ? 'active' : '';?>">
								<a href="<?php echo base_url(); ?>bank_account/index"><span><?php echo $this->lang->line('menu_bank_account') ?></span></a>
							</li>
						<?php } ?>
					</ul>
				</li>
			<?php } ?>
			<!-- begin sidebar minify button -->
			<li><a href="javascript:;" class="sidebar-minify-btn" data-click="sidebar-minify"><i class="fa fa-angle-double-left"></i></a></li>
			<!-- end sidebar minify button -->
		</ul>
		<!-- end sidebar nav -->
	</div>
	<!-- end sidebar scrollbar -->
</div>
<div class="sidebar-bg"></div>
<!-- end #sidebar -->