<style type="text/css">
.navbar-form .full-width-chart, .topbar_search_result {
    width: 100%;
    height: 40px;
}
</style>

<!-- begin #page-container -->
	<div id="page-container" class="fade page-sidebar-fixed page-header-fixed">
		<!-- begin #header -->
		<div id="header" class="header navbar navbar-default navbar-fixed-top">
			<!-- begin container-fluid -->
			<div class="container-fluid">
				<!-- begin mobile sidebar expand / collapse button -->
				<div class="navbar-header">
					<a href="<?php echo base_url('dashboard/index'); ?>" class="navbar-brand" style="text-overflow: ellipsis !important; overflow: hidden !important; white-space: nowrap; text-transform: uppercase;">
						<?php $id = $this->session->userdata('schoolID'); $school = $this->school_m->get_school($id); //e.etti ?>
						<span class="" style="height: 40px !important; width: auto !important; ">
							<img src="<?=base_url("uploads/images/".$school->photo); ?>" class="img-circle" alt="School Logo" style="height: 40px; width: auto; margin-right: 10px; margin-top: -10px;" />
						</span> 
						
						<span style="font-family: 'Segoe UI'; font-size: 30px; line-height: 25px;">
							<?php 
								if($this->session->userdata('school')) { 
									echo $school->alias; 
								}
							?>
						</span>
					</a>
					
					<button type="button" class="navbar-toggle" data-click="sidebar-toggled">
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
				</div>
				<!-- end mobile sidebar expand / collapse button -->
				
				<!-- begin header navigation right -->
				<ul class="nav navbar-nav navbar-right">
					<li>
						<form class="navbar-form full-width">
							<div class="form-group">
								<input type="text" id="topbar_search" class="form-control" placeholder="Enter keyword" autocomplete="off"/>
								<button type="submit" class="btn btn-search"><i class="fa fa-search"></i></button>
							</div>
							<div id="topbar_search_result ">
								<li class="dropdown">
									<ul class="dropdown-menu media-list results pull-right animated fadeInDown">
			                            <li class="dropdown-header">Search Results : (<span id="search_count"></span>)</li>
									</ul>
								</li>
							</div>
						</form>
					</li>
					
					 <!-- school: style can be found in dropdown.less-->
                        <?php 
                            $usertype = $this->session->userdata('usertype');
                            if($usertype == "Super Admin") {
                        ?> 
                        <li class="dropdown">
							<a href="javascript:;" data-toggle="dropdown" class="dropdown-toggle f-s-14" style="padding: 15px;">
								<i class="ion-android-alert" style="font-size: 22px;"></i>
								<span class="label label-primary"><?=count($applied_school)?></span>
							</a>
                             <ul class="dropdown-menu media-list pull-right animated fadeInDown" style="padding-bottom: 10px;">
                               <div data-scrollbar="true" data-height="270px" class="col-md-12" id="navbar-override">
									 <li class="dropdown-header" style="margin-left: -15px;"><?php echo $this->lang->line("la_fs").' '.count($applied_school) ." new ".$this->lang->line("la_ls_school")."s waiting";?></li>
									<?php 
										foreach ($applied_school as $key => $allschool) {
											 echo '<li class="media" id="'.$allschool->tmp_schoolID.'">';
											  echo '<a href="'.base_url("applied_school/view/$allschool->tmp_schoolID").'">';
												echo '<div class="media-left">';
												echo '<img class="img-circle" style="width: auto; height: 20px; margin-top: -3px;" src="'.base_url("uploads/images/site.png").'"/>';
												echo '</div>';
												echo ' <div class="media-body">';
													 echo '<h6 class="media-heading">';
														if(strlen($allschool->school) >25) {
															echo substr($allschool->school, 0,25). "..";
														} else {
															echo $allschool->school;
														}
													echo '</h6>';
												echo '</div>';
											echo '</a>';
											echo '</li>';
										}
									?>
									<li class='notifications-footer'>
										<a href="<?=base_url("applied_school/index")?>">
											<?=$this->lang->line("view_more_school")?>
										</a>
									</li>
							   </div>
                            </ul>
                        </li>
						
                        <li class="dropdown">
                            <a href="javascript:;" data-toggle="dropdown" class="dropdown-toggle f-s-14">
                                <img class="school-img" style="height: 20px; width: auto;" src="<?php 
                                $image = $this->session->userdata('schoollogo'); 
                                echo base_url("uploads/images/$image"); ?>" 
                                /> 
                                <?php if($this->session->userdata('loginuserID') == 1 || $this->session->userdata('loginuserID') == 4){ ?>
                                <span class="label label-success"><?=count($allschools)?></span>
                                <?php }elseif($this->session->userdata('loginuserID') != 1 && !is_null($current_access)){ ?>
                                <span class="label label-success"><?=count($current_access)?></span>
                                <?php } ?>
                            </a>
							<!-- inner menu: contains the actual data -->
							<ul  class="dropdown-menu media-list pull-right animated fadeInDown" style="padding-bottom: 10px;">
								<div data-scrollbar="true" data-height="270px" class="col-md-12" id="navbar-override-notification">
									<?php if($this->session->userdata('loginuserID') == 1 || $this->session->userdata('loginuserID') == 4){ ?>
									<li class="dropdown-header" style="margin-left: -15px; margin-right: -15px;"><?php echo $this->lang->line("la_fs").' '.count($allschools) ." ".$this->lang->line("la_ls_school");?></li>
									<?php }elseif($this->session->userdata('loginuserID') != 1 && !is_null($current_access)){ ?>
									<li class="dropdown-header hidden" style="margin-left: -15px; margin-right: -15px;"><?php echo $this->lang->line("la_fs").' '.count($current_access) ." ".$this->lang->line("la_ls_school");?></li>
									<?php } ?>
								
									<?php 
										$sess_school = $this->session->userdata('schoolID');
										foreach ($allschools as $key => $allschool) {
											if(in_array($allschool->schoolID, $current_access) && $this->session->userdata('loginuserID') != 1){
												echo '<li class="media" id="'.$allschool->schoolID.'" style="margin-top: 0px;">';
													echo '<a href="'.base_url("school/activeschoolnow/$allschool->schoolID/$allschool->photo").'" class="navbar-override-link">';
														echo '<div class="media-left">';
															echo '<img class="img-circle" style="width: 20px; height: 20px; margin-top: 9px;" src="'.base_url("uploads/images/$allschool->photo").'"/>';
														echo '</div>';
														echo ' <div class="media-body">';
															echo '<h6 style="text-overflow: ellipsis; white-space: nowrap;">';
																if(strlen($allschool->school) >25) {
																	echo substr($allschool->school, 0,25). "..";
																} else {
																	echo $allschool->school;
																}
																if($sess_school == $allschool->schoolID) { 
																	echo " <i class='glyphicon glyphicon-ok'></i>";
																}
															echo '</h6>';
														echo '</div>';
													echo '</a>';
												echo '</li>';
											} elseif($this->session->userdata('loginuserID') == 1 || $this->session->userdata('loginuserID') == 4) {
												echo '<li class="media" id="'.$allschool->schoolID.'" style="margin-top: 0px;">';
													echo '<a href="'.base_url("school/activeschoolnow/$allschool->schoolID/$allschool->photo").'" class="navbar-override-link">';
														echo '<div class="media-left">';
															echo '<img class="img-circle" style="width: 20px; height: 20px; margin-top: 9px;" src="'.base_url("uploads/images/$allschool->photo").'"/>';
														echo '</div>';
														echo ' <div class="media-body">';
															echo '<h6 style="text-overflow: ellipsis; white-space: nowrap;">';
																if(strlen($allschool->school) >25) {
																	echo substr($allschool->school, 0,25). "..";
																} else {
																	echo $allschool->school;
																}
																if($sess_school == $allschool->schoolID) { 
																	echo " <i class='glyphicon glyphicon-ok'></i>";
																}
															echo '</h6>';
													echo '</div>';
													echo '</a>';
												echo '</li>';
										}
										}
									?>
									
									<li class='notifications-footer'>
										<?php if($this->session->userdata('loginuserID') == 1) {?>
										<a href="<?=base_url("school/allschoolnow")?>">
											<?=$this->lang->line("view_more_school")?>
										</a>
										<?php } ?>
									</li>
								</div>
							</ul>
                        </li>
                        <?php } ?>

                        <!-- Message: style can be found in dropdown.less-->
                        <li class="dropdown messages-menu">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <i class="fa fa-bell-o"></i>
                                <?php
                                    $alCounter = 0; 
                                    if(count($alert) > 0) {
                                        $alCounter += count($alert);
                                        echo "<span class='label label-danger'>";
                                            echo "<lable class='alert-image'>".count($alert)."</lable>";
                                        echo "</span>";
                                    } 
                                ?>
                            </a>
                            <?php
                                if(count($alert) > 0) {
                                    $pdate = date("Y-m-d H:i:s");
                                    $title = '';
                                    $discription = '';
                                    $i = 1;
                                    echo "<ul class='dropdown-menu'>";
                                        echo "<li class='header'>";
                                            echo $this->lang->line("la_fs")." ".$alCounter ." ".$this->lang->line("la_ls");
                                        echo '</li>';
                                        echo '<li>';
                                            echo "<ul class='menu'>";
                                                foreach ($alert as $alt) {
                                                    if(strlen($alt->title) > 16) {
                                                       $title = substr($alt->title, 0,16). ".."; 
                                                    } else {
                                                        $title = $alt->title;
                                                    }
                                                    if(strlen($alt->notice) > 30) {
                                                       $discription = substr($alt->notice, 0,30). ".."; 
                                                    } else {
                                                        $discription = $alt->notice;
                                                    }
                                                    echo '<li>';
                                                        echo "<a href=".base_url("notice/view")."/".$alt->noticeID.">";
                                                            echo "<div class='pull-left'>" ;
                                                                echo "<img class='img-circle' src='".base_url('uploads/images/'.$this->session->userdata('schoollogo'))."'>";
                                                            echo '</div>';
                                                            echo '<h4>';
                                                                echo strip_tags($title) ;
                                                                    echo "<small><i class='fa fa-clock-o'></i> ";
                                                                        $dafstdate = $alt->create_date;
                                                                        $first_date = new DateTime($alt->create_date);
                                                                        $second_date = new DateTime($pdate);
                                                                        $difference = $first_date->diff($second_date);
                                                                        if($difference->y >= 1) {
                                                                            $format = 'Y-m-d H:i:s';
                                                                            $date = DateTime::createFromFormat($format, $dafstdate);
                                                                            echo $date->format('M d Y');
                                                                        } elseif($difference->m ==1 && $difference->m !=0) {
                                                                            echo $difference->m . " month";
                                                                        } elseif($difference->m <=12 && $difference->m !=0) {
                                                                            echo $difference->m . " months";
                                                                        } elseif($difference->d == 1 && $difference->d != 0) {
                                                                            echo "Yesterday";
                                                                        } elseif($difference->d <= 31 && $difference->d != 0) {
                                                                            echo $difference->d . " days";
                                                                        } else if($difference->h ==1 && $difference->h !=0) {
                                                                            echo $difference->h . " hr";
                                                                        } else if($difference->h <=24 && $difference->h !=0) {
                                                                            echo $difference->h . " hrs";
                                                                        } elseif($difference->i <= 60 && $difference->i !=0) {
                                                                          echo $difference->i . " mins";
                                                                        } elseif($difference->s <= 10) {
                                                                          echo "Just Now";
                                                                        } elseif($difference->s <= 60 && $difference->s !=0) {
                                                                          echo $difference->s . " sec";
                                                                        }
                                                                    echo '</small>';
                                                            echo '</h4>';
                                                            echo '<p>'. strip_tags($discription).'</p>';
                                                        echo '</a>';
                                                    echo '</li>';
                                                } 
                                            echo '</ul>';
                                        echo '</li>';
                                        echo "<li class='footer'>";
                                            echo "<a href=".base_url("notice/index").">";
                                                echo $this->lang->line("view_more");
                                            echo '</a>';
                                        echo '</li>';
                                    echo '</ul>';
                                }
                            ?>
                        </li>
					<!-- language dropdown has been commented out -->
					<!-- Notifications: style can be found in dropdown.less -->
					<!--li class="dropdown notifications-menu">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown">
							<img class="language-img" src="<?php 
							$image = $this->session->userdata('lang'); 
							echo base_url('uploads/language_image/'.$image.'.png'); ?>" 
							/> 
							<span class="label label-warning">14</span>
						</a>
						<ul class="dropdown-menu">
							<li class="header"> <?=$this->lang->line("language")?></li>
							<li>
								<!-inner menu: contains the actual data ->
								<ul class="menu">
									<li class="language" id="arabic">
										<a href="<?php echo base_url('language/index/arabic')?>">
											<div class="pull-left">
												<img src="<?php echo base_url('uploads/language_image/arabic.png'); ?>"/>
											</div>
											<h4>
												Arabic
												<?php if($image == 'arabic') echo " <i class='glyphicon glyphicon-ok'></i>";  ?>
											</h4>
										</a>
									</li>

									<li class="language" id="bengali">
										<a href="<?php echo base_url('language/index/bengali')?>">
											<div class="pull-left">
												<img src="<?php echo base_url('uploads/language_image/bengali.png'); ?>"/>
											</div>
											<h4>
												Bengali
												<?php if($image == 'bengali') echo " <i class='glyphicon glyphicon-ok'></i>";  ?>
											</h4>
										</a>
									</li>

									<li class="language" id="chinese">
										<a href="<?php echo base_url('language/index/chinese')?>">
											<div class="pull-left">
												<img src="<?php echo base_url('uploads/language_image/chinese.png'); ?>"/>
											</div>
											<h4>
												Chinese
												<?php if($image == 'chinese') echo " <i class='glyphicon glyphicon-ok'></i>";  ?>
											</h4>
										</a>
									</li>

									<li class="language" id="english">
										<a href="<?php echo base_url('language/index/english')?>">
											<div class="pull-left">
												<img src="<?php echo base_url('uploads/language_image/english.png'); ?>"/>
											</div>
											<h4>
												English
												<?php if($image == 'english') echo " <i class='glyphicon glyphicon-ok'></i>";  ?>
											</h4>
										</a>
									</li>

									<li class="language" id="french">
										<a href="<?php echo base_url('language/index/french')?>">
											<div class="pull-left">
												<img src="<?php echo base_url('uploads/language_image/french.png'); ?>"/>
											</div>
											<h4>
												French
												<?php if($image == 'french') echo " <i class='glyphicon glyphicon-ok'></i>";  ?>
											</h4>
										</a>
									</li>

									<li class="language" id="german">
										<a href="<?php echo base_url('language/index/german')?>">
											<div class="pull-left">
												<img src="<?php echo base_url('uploads/language_image/german.png'); ?>"/>
											</div>
											<h4>
												German
												<?php if($image == 'german') echo " <i class='glyphicon glyphicon-ok'></i>";  ?>
											</h4>
										</a>
									</li>

									<li class="language" id="hindi">
										<a href="<?php echo base_url('language/index/hindi')?>">
											<div class="pull-left">
												<img src="<?php echo base_url('uploads/language_image/hindi.png'); ?>"/>
											</div>
											<h4>
												Hindi
												<?php if($image == 'hindi') echo " <i class='glyphicon glyphicon-ok'></i>";  ?>
											</h4>
										</a>
									</li>

									<li class="language" id="indonesian">
										<a href="<?php echo base_url('language/index/indonesian')?>">
											<div class="pull-left">
												<img src="<?php echo base_url('uploads/language_image/indonesian.png'); ?>"/>
											</div>
											<h4>
												Indonesian
												<?php if($image == 'indonesian') echo " <i class='glyphicon glyphicon-ok'></i>";  ?>
											</h4>
										</a>
									</li>

									<li class="language" id="italian">
										<a href="<?php echo base_url('language/index/italian')?>">
											<div class="pull-left">
												<img src="<?php echo base_url('uploads/language_image/italian.png'); ?>"/>
											</div>
											<h4>
												Italian
												<?php if($image == 'italian') echo " <i class='glyphicon glyphicon-ok'></i>";  ?>
											</h4>
										</a>
									</li>

									<li class="language" id="romanian">
										<a href="<?php echo base_url('language/index/romanian')?>">
											<div class="pull-left">
												<img src="<?php echo base_url('uploads/language_image/romanian.png'); ?>"/>
											</div>
											<h4>
												Romanian
												<?php if($image == 'romanian') echo " <i class='glyphicon glyphicon-ok'></i>";  ?>
											</h4>
										</a>
									</li>

									<li class="language" id="russian">
										<a href="<?php echo base_url('language/index/russian')?>">
											<div class="pull-left">
												<img src="<?php echo base_url('uploads/language_image/russian.png'); ?>"/>
											</div>
											<h4>
												Russian
												<?php if($image == 'russian') echo " <i class='glyphicon glyphicon-ok'></i>";  ?>
											</h4>
										</a>
									</li>

									<li class="language" id="spanish">
										<a href="<?php echo base_url('language/index/spanish')?>">
											<div class="pull-left">
												<img src="<?php echo base_url('uploads/language_image/spanish.png'); ?>"/>
											</div>
											<h4>
												Spanish
												<?php if($image == 'spanish') echo " <i class='glyphicon glyphicon-ok'></i>";  ?>
											</h4>
										</a>
									</li>

									<li class="language" id="thai">
										<a href="<?php echo base_url('language/index/thai')?>">
											<div class="pull-left">
												<img src="<?php echo base_url('uploads/language_image/thai.png'); ?>"/>
											</div>
											<h4>
												Thai
												<?php if($image == 'thai') echo " <i class='glyphicon glyphicon-ok'></i>";  ?>
											</h4>
										</a>
									</li>

									<li class="language" id="turkish">
										<a href="<?php echo base_url('language/index/turkish')?>">
											<div class="pull-left">
												<img src="<?php echo base_url('uploads/language_image/turkish.png'); ?>"/>
											</div>
											<h4>
												Turkish
												<?php if($image == 'turkish') echo " <i class='glyphicon glyphicon-ok'></i>";  ?>
											</h4>
										</a>
									</li>
		 
								</ul>
							</li>
							<li class="footer"></li>
						</ul>
					</li-->

					<li class="dropdown">
						<a href="javascript:;" data-toggle="dropdown" class="dropdown-toggle f-s-14">
							<i class="fa fa-bell-o"></i>
							<span class="label">0</span>
						</a>
						<ul class="dropdown-menu media-list pull-right animated fadeInDown">
                            <li class="dropdown-header">Notifications (0)</li>
                            <li class="dropdown-footer text-center">
                                <a href="javascript:;">View more</a>
                            </li>
						</ul>
					</li>
					<li class="dropdown navbar-user">
						<a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown">
							 <img src="<?=base_url("uploads/images/".$this->session->userdata('photo')); ?>" class="user-logo" alt="User Image" />
							<span class="hidden-xs">
								 <span>
                                    <?php
                                        $name = $this->session->userdata('name');
                                        if(strlen($name) > 9) {
                                           echo substr($name, 0,9). ".."; 
                                        } else {
                                            echo $name;
                                        }
                                    ?>
                                </span>   
							</span> 
							<b class="caret"></b>
						</a>
						<ul class="dropdown-menu animated fadeInLeft">
							<li class="arrow"></li>
							<li><a href="<?=base_url("profile/index")?>"><?=$this->lang->line("profile")?> </a></li>
							<li class="divider"></li>
							<li><a href="<?=base_url("signin/cpassword")?>"><?=$this->lang->line("change_password")?></a></li>
							<li class="divider"></li>
							<li><a href="<?=base_url("signin/signout")?>"><?=$this->lang->line("logout")?></a></li>
						</ul>
					</li>
				</ul>
				<!-- end header navigation right -->
			</div>
			<!-- end container-fluid -->
		</div>
		<!-- end #header -->
<script type="text/javascript">
// add underscore:
//@emmanuel etti
var nav_content = []
	var index_keys = []
	var output = {}
	var my = []
$(document).ready(function(){

	index_keys = _.map($('ul.nav.search_enabled  > :not(nav-header) > ul >li >a'),function(obj,iter){
	    var title = $(obj).text().trim().toLowerCase()
		return title
	})
	nav_content = _.map($('ul.nav.search_enabled  > :not(nav-header) > ul >li >a'),function(obj,iter){
		var val = $(obj).context.href 
		return val
	});

	output = _.object(index_keys,nav_content)

	// console.log(JSON.stringify(output))

	$.each(output,function( index, value ) {
		my.push({
	        name: index,
	        link: value,
	    });
	})

})

$("ul .results > .dropdown-header").after("<li class='dropBody'></li>")


$("#topbar_search").on("keyup",function(){
	if($("#topbar_search").val() == ''){
		$(".dropBody").html("")
		$("ul .results").css('display','none')
		return true
	}
	$(".dropBody").html("")
	var entered = $("#topbar_search").val()
	// console.log(entered)
	var result = _.filter(my, function(obj,iter){
	  return obj['name'].indexOf(entered) > -1;
	})

	$("#search_count").html(result.length)

	// console.log(result)
	$.each(result, function(key,value){
		$(".dropBody").append('<li class="result_text"><a target="_blank" href='+result[key].link+'>'+result[key].name+'</a></li>')
	})
	
	$("ul .results").css('display','block')
	$('.result_text').css('textTransform', 'capitalize')
	$("ul .results").css("width",$('#topbar_search').css("width"))
	$("ul .results").css("overflow-y","scroll")
});

</script>