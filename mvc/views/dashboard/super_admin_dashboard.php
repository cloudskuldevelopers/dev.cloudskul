<!-- dont touch below this line -->
<style type="text/css">
#interactive-chart > *{
  float:right
}
</style>


  <div class="row" id="super_admin">
	<!-- begin col-3 -->
	<div class="col-md-3 col-sm-6">
		<div class="widget widget-stats bg-green">
			<div class="stats-icon"><i class="ion-android-contacts"></i></div>
			<div class="stats-info">
				<h4 class="text-uppercase"><?=$this->lang->line("menu_student")?></h4>
				<p><?=count($student)?></p>	
			</div>
			<div class="stats-link">
				<a href="<?=base_url('student')?>">View Detail <i class="ion-android-checkmark-circle"></i></a>
			</div>
		</div>
	</div>
	<!-- end col-3 -->
	
	<!-- begin col-3 -->
	<div class="col-md-3 col-sm-6">
		<div class="widget widget-stats bg-blue">
			<div class="stats-icon"><i class="ion-ios-personadd-outline"></i></div>
			<div class="stats-info">
				<h4 class="text-uppercase"><?=$this->lang->line("menu_teacher")?></h4>
				<p><?=count($teacher)?></p>	
			</div>
			<div class="stats-link">
				<a href="<?=base_url('teacher')?>">View Detail <i class="ion-android-checkmark-circle"></i></a>
			</div>
		</div>
	</div>
	<!-- end col-3 -->
	
	<!-- begin col-3 -->
	<div class="col-md-3 col-sm-6">
		<div class="widget widget-stats bg-purple">
			<div class="stats-icon"><i class="ion-ios-people"></i></div>
			<div class="stats-info">
				<h4 class="text-uppercase"><?=$this->lang->line("menu_parent")?></h4>
				<p><?=count($parents)?></p>	
			</div>
			<div class="stats-link">
				<a href="<?=base_url('parentes')?>">View Detail <i class="ion-android-checkmark-circle"></i></a>
			</div>
		</div>
	</div>
	<!-- end col-3 -->
	
	<!-- begin col-3 -->
	<div class="col-md-3 col-sm-6">
		<div class="widget widget-stats bg-red">
			<div class="stats-icon"><i class="ion-android-list"></i></div>
			<div class="stats-info">
				<h4 class="text-uppercase"><?=$this->lang->line("menu_attendance")?></h4>
				<p><?=count($attendance)?></p>	
			</div>
			<div class="stats-link">
				<a href="<?=base_url('sattendance')?>">View Detail <i class="ion-android-checkmark-circle"></i></a>
			</div>
		</div>
	</div>
	<!-- end col-3 -->
	
	<div class="col-md-8">
		<div class="widget-chart with-sidebar bg-black">
			<div class="widget-chart-content">
				<h4 class="chart-title">
					Visitors Analytics
					<small>Where do our visitors come from</small>
				</h4>
				<div id="visitors-line-chart" class="morris-inverse" style="height: 260px;"></div>
			</div>
			<div class="widget-chart-sidebar bg-black-darker">
				<div class="chart-number">
					1,225,729
					<small>visitors</small>
				</div>
				<div id="visitors-donut-chart" style="height: 160px"></div>
				<ul class="chart-legend">
					<li><i class="fa fa-circle-o fa-fw text-success m-r-5"></i> 34.0% <span>New Visitors</span></li>
					<li><i class="fa fa-circle-o fa-fw text-primary m-r-5"></i> 56.0% <span>Return Visitors</span></li>
				</ul>
			</div>
		</div>
		
		<div class="panel panel-inverse" data-sortable-id="index-6">
			<div class="panel-heading">
				<div class="panel-heading-btn">
					<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
					<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
					<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
					<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
				</div>
				<h4 class="panel-title"><?=$this->lang->line('dashboard_notice')?></h4>
			</div>
			<div class="panel-body p-t-0" style="padding-top: 25px !important;;">
				<table class="table table-hover">
					<tbody>
						<?php 
							if(count($notices)) {
								$i =1;
								foreach ($notices as $key => $notice) {
								  if($i != 8) {
									echo "<tr>";
									  echo "<td class='align-vertical-table col-md-1'>";
										echo $i;
									  echo "</td>";

									  echo "<td class='align-vertical-table col-md-3'>";
										echo "<span class=''>";
											if(strlen($notice->title) > 20) {
											   $title = substr($notice->title, 0,20). ".."; 
											} else {
												$title = $notice->title;
											}
										echo strip_tags($title);
										echo "</span>";
									  echo "</td>";

									  echo "<td class='align-vertical-table col-md-7'>";
										echo "<span class=''>";
											if(strlen($notice->notice) > 80) {
											  $discription = substr($notice->notice, 0,80). ".."; 
											} else {
												$discription = $notice->notice;
											}
											echo strip_tags($discription);
										echo "</span>";
									  echo "</td>";

									  echo "<td class='align-vertical-table col-md-1'>";
										echo btn_dash_view('notice/view/'.$notice->noticeID, $this->lang->line('view'));
									  echo "</td>";
									echo "</tr>";
									$i++;
								  } else {
									break;
								  }  
								}
							  }
							?>
					</tbody>
				</table>
			</div>
		</div>
	</div>
	
	<div class="col-md-4">
		<div class="panel panel-inverse" data-sortable-id="index-7" style="height: 335px;">
			<div class="panel-heading">
				<div class="panel-heading-btn">
					<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
					<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
					<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
					<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
				</div>
				<h4 class="panel-title">Visitors User Agent</h4>
			</div>
			<div class="panel-body">
				<div id="donut-chart" class="height-sm"></div>
			</div>
		</div>
		
		<div class="panel panel-inverse" data-sortable-id="index-6">
			<div class="panel-heading">
				<div class="panel-heading-btn">
					<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
					<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
					<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
					<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
				</div>
				<h4 class="panel-title">User Profile Details</h4>
			</div>
			<div class="panel-body p-t-0">
				<div class="row" style="margin: 0 -15px;">
					<div class="col-md-12 text-center" style="padding-top: 15px; padding-bottom: 15px; background: #2d353c !important;">
						<a href="<?=base_url('profile/index')?>">
							<img src="<?php echo base_url('uploads/images/'.$user->photo); ?>" class="img-circle" style="height: 60px; width: auto;" />
						</a>

						<h4 style="text-transform: uppercase; color: #fff !important; font-weight: 300;"><?=$user->name?></h4>
						<p><?=$this->lang->line($user->usertype)?></p>
					</div>
				</div>
				<table class="table table-valign-middle m-b-0">
					<tbody>
						<tr>
							<td><span class="text-success"><i class="ion-ios-personadd-outline override-icon"></i></span></td>
							<td><span><?=$this->lang->line('dashboard_username')?></span></td>
							<td><label class="label label-success"><?=$user->username?></label></td>
						</tr>
						<tr>
							<td><span class="text-success"><i class="ion-ios-email-outline"></i></span></td>
							<td><span><?=$this->lang->line('dashboard_email')?></span></td>
							<td><label class="label label-warning"><?=$user->email?></label></td>
						</tr>
						<tr>
							<td><span class="text-success"><i class="ion-ios-telephone-outline"></i></span></td>
							<td><span><?=$this->lang->line('dashboard_phone')?></span></td>
							<td><label class="label label-info"><?=$user->phone?></label></td>
						</tr>
						<tr>
							<td><span class="text-success"><i class="ion-ios-list-outline"></i></span></td>
							<td><span><?=$this->lang->line('dashboard_address')?></span></td>
							<td><label class="label label-primary"><?=$user->address?></label></td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
	</div>
	
  </div>
