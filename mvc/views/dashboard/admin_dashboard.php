<!-- dont touch below this line -->
<style type="text/css">

#interactive-chart {
    display: inline-block;
}
#interactive-chart, canvas {
    width: 100%;
    height: 100%;
}
</style>
<div class="row" style="margin-bottom: 20px;">

  <div class="col-md-3 col-sm-6 col-xs-12">
    <div class="widget widget-stats bg-green">
      <div class="stats-icon"><i class="fa fa-group"></i></div>
      <div class="stats-info">
        <h4>TOTAL <?=$this->lang->line("menu_student")?>s</h4>
        <p><?=count($student)?></p>  
      </div>
      <div class="stats-link">
        <a href="<?=base_url('student')?>">View Detail <i class="fa fa-arrow-circle-o-right"></i></a>
      </div>
    </div>
  </div>

  <div class="col-md-3 col-sm-6 col-xs-12">
    <div class="widget widget-stats bg-blue">
      <div class="stats-icon"><i class="fa fa-user"></i></div>
      <div class="stats-info">
        <h4>TOTAL <?=$this->lang->line("menu_teacher")?>s</h4>
        <p><?=count($teacher)?></p>  
      </div>
      <div class="stats-link">
        <a href="<?=base_url('teacher')?>">View Detail <i class="fa fa-arrow-circle-o-right"></i></a>
      </div>
    </div>
  </div>

  <div class="col-md-3 col-sm-6 col-xs-12">
    <div class="widget widget-stats bg-purple">
      <div class="stats-icon"><i class="fa fa-user"></i></div>
      <div class="stats-info">
        <h4>TOTAL <?=$this->lang->line("menu_parent")?></h4>
        <p><?=count($parents)?></p>  
      </div>
      <div class="stats-link">
        <a href="<?=base_url('parentes')?>">View Detail <i class="fa fa-arrow-circle-o-right"></i></a>
      </div>
    </div>
  </div>

  
  <div class="col-md-3 col-sm-6 col-xs-12">
    <div class="widget widget-stats bg-black">
      <div class="stats-icon"><i class="fa fa-calendar"></i></div>
      <div class="stats-info">
        <h4><?=$this->lang->line("menu_attendance")?></h4>
        <p><?=count($attendance)?></p>  
      </div>
      <div class="stats-link">
        <a href="<?=base_url('sattendance')?>">View Detail <i class="fa fa-arrow-circle-o-right"></i></a>
      </div>
    </div>
  </div>
  
  <div class="col-md-3 col-sm-6 col-xs-12 hidden">
    <div class="info-box">
    <a class="info-box-link" href="<?=base_url('teacher')?>">
      <span class="info-box-icon bg-red"><i class="fa icon-teacher"></i></span>
      <div class="info-box-content">
        <span class="info-box-number"><?=count($teacher)?></span>
        <span class="info-box-text"><?=$this->lang->line("menu_teacher")?></span>
      </div><!-- /.info-box-content -->
    </a>
    </div><!-- /.info-box -->
  </div>
  
  <div class="col-md-3 col-sm-6 col-xs-12 hidden">
    <div class="info-box">
    <a class="info-box-link" href="<?=base_url('student')?>">
      <span class="info-box-icon bg-aqua"><i class="fa icon-student"></i></span>
      <div class="info-box-content">
        <span class="info-box-number"><?=count($student)?></span>
        <span class="info-box-text"><?=$this->lang->line("menu_student")?></span>
      </div><!-- /.info-box-content -->
    </a>
    </div><!-- /.info-box -->
  </div>
  
  <div class="col-md-3 col-sm-6 col-xs-12 hidden">
    <div class="info-box">
    <a class="info-box-link" href="<?=base_url('parentes')?>">
      <span class="info-box-icon bg-green"><i class="fa fa-user"></i></span>
      <div class="info-box-content">
        <span class="info-box-number"><?=count($parents)?></span>
        <span class="info-box-text"><?=$this->lang->line("menu_parent")?></span>
      </div><!-- /.info-box-content -->
    </a>
    </div><!-- /.info-box -->
  </div>
  
  <div class="col-md-3 col-sm-6 col-xs-12 hidden">
    <div class="info-box">
    <a class="info-box-link" href="<?=base_url('sattendance')?>">
      <span class="info-box-icon bg-yellow"><i class="fa icon-attendance"></i></span>
      <div class="info-box-content">
        <span class="info-box-number"><?=count($attendance)?></span>
        <span class="info-box-text"><?=$this->lang->line("menu_attendance")?></span>
      </div><!-- /.info-box-content -->
    </a>
    </div><!-- /.info-box -->
  </div>

    <!--<div class="col-lg-3 col-xs-6">
        <div class="small-box ">
            <a class="small-box-footer" href="<?=base_url('sattendance')?>">
                <div class="icon bg-blue" style="padding: 9.5px 18px 8px 18px;">
                    <i class="fa icon-attendance"></i>
                </div>
                <div class="inner ">
                  <h3>
                      <?=count($attendance)?>
                  </h3>
                  <p>
                    <?=$this->lang->line("menu_attendance")?>
                  </p>
                </div>
            </a>
        </div>
    </div>-->

</div>

  <script type="text/javascript" src="<?php echo base_url('assets/chartjs/chart.js'); ?>"></script>
   <script>

   function addCommas(nStr)
    {
        nStr += '';
        x = nStr.split('.');
        x1 = x[0];
        x2 = x.length > 1 ? '.' + x[1] : '';
        var rgx = /(\d+)(\d{3})/;
        while (rgx.test(x1)) {
            x1 = x1.replace(rgx, '$1' + ',' + '$2');
        }
        return x1 + x2;
    }

    var randomScalingFactor = function(){ return Math.round(Math.random()*100)};
    var datasets = []
      $.ajax({
        type: 'GET',
        dataType: "json",
        url: "<?=base_url('dashboard/graphcall')?>",
        dataType: "html",
        success: function(data) {
          var response = jQuery.parseJSON(data);
          var barChartData = {
            labels : ["Jan","Feb","Mar","Apr","May","Jun","Jul", "Aug", 'Sep', "Oct", "Nov", "Dec"],
            datasets : [{
              fillColor : "rgba(0, 166, 90, 0.94)", // bar color
              strokeColor : "rgba(0, 143, 77, 0.9)", //hover color
              highlightFill : "rgba(0, 143, 77, 0.9)", // hithlight color
              highlightStroke : "rgba(221, 75, 57, 0.15", // highlight hover color
              data : response.balance
            }]
          }

          datasets = barChartData.datasets
          datasets_labels = barChartData.labels

          $(document).ready(function() { 
            var graph = document.getElementById("graph").getContext("2d");

            //(245, 105, 84, 0.6)
            var mygraph = new Chart(graph).Bar(barChartData, { 
              responsive : true,
              animationEasing: "easeInOutElastic",
              scaleLineColor: "rgba(0,0,0,.1)",
              scaleLabel : "<%= 'N '+ addCommas(value) %>",
              scaleGridLineColor : "rgba(0,0,0,.1)",
              scaleShowHorizontalLines: true,
              scaleShowVerticalLines: false,
              barStrokeWidth : 1,
              barValueSpacing : 15,
              tooltipTemplate: "<%if (label){%><%=label%>: <%}%>N <%= value.toLocaleString() %>",

            });

           /* setInterval(function() {
              barChartDemo.removeData();
              barChartDemo.addData([dData()], "dD " + index);
              index++;
            }, 3000);*/

            var legendHolder = document.createElement('div');
            // legendHolder.innerHTML = mygraph.generateLegend();
            // document.getElementById('legend').appendChild(legendHolder.firstChild);
          });
        }
      });

      $.ajax({
          type: 'GET',
          dataType: "json",
          url: "<?=base_url('dashboard/paymentscall')?>",
          dataType: "html",
          success: function(data) {
            var response = jQuery.parseJSON(data);
            npaid = response.npaid;
            ppaid = response.ppaid;
            fpaid = response.fpaid;
            cash = response.cash;
            cheque = response.cheque;
            paypal = response.paypal;
            remita = response.remita;
            stripe = response.stripe;
            st = response.st;

            if(st == 1) {
              var pieData = [{
                value: npaid,
                color:"#F7464A",
                highlight: "#FF5A5E",
                label: "<?=$this->lang->line('dashboard_notpaid')?>"
              }, {
                value: ppaid,
                color: "#FDB45C",
                highlight: "#FFC870",
                label: "<?=$this->lang->line('dashboard_partially_paid')?>"
              }, {
                value: fpaid,
                color: "#46BFBD",
                highlight: "#5AD3D1",
                label: "<?=$this->lang->line('dashboard_fully_paid')?>"
              }];

              if((cash == 0 && cheque == 0 && paypal == 0)) {
                var doughnutData = [{
                  value: 1,
                  color:"#4D5360",
                  highlight: "#616774",
                  label: "<?=$this->lang->line('dashboard_sample')?>"
                }];
              } else {
                var doughnutData = [{
                  value: cash,
                  color:"#084D5F",
                  highlight: "#0B6B85",
                  label: "<?=$this->lang->line('dashboard_cash')?>"
                }, {
                  value: cheque,
                  color: "#D48197",
                  highlight: "#F294AD  ",
                  label: "<?=$this->lang->line('dashboard_cheque')?>"
                }, {
                  value: remita,
                  color: "#FDB45C",
                  highlight: "#FFC870",
                  label: "<?=$this->lang->line('dashboard_remita')?>"
                }, {
                  value: paypal,
                  color: "#D70D71",
                  highlight: "#B50B5F",
                  label: "<?=$this->lang->line('dashboard_paypal')?>"
                }];
              }
            } else {
              var pieData = [{
                value: 1,
                color:"#4D5360",
                highlight: "#616774",
                label: "<?=$this->lang->line('dashboard_sample')?>"
              }];

              var doughnutData = [{
                value: 1,
                color:"#4D5360",
                highlight: "#616774",
                label: "<?=$this->lang->line('dashboard_sample')?>"
              }];
            }


            window.onload = function(){
              var ctx = document.getElementById("pai").getContext("2d");
              window.myPie = new Chart(ctx).Pie(pieData);

              var ctx = document.getElementById("chart-area").getContext("2d");
              window.myPie = new Chart(ctx).Pie(doughnutData);
            }
          }
      });

      

      
  </script>
<div class="row">
    
    <div class="col-lg-8 hidden">

      <div class="box box-danger">
        <div class="box-header" style="padding-bottom: 16px; border-bottom: 1px solid gainsboro;">
          <h3 class="box-title" style="margin-top: 10px;">
              <?=$this->lang->line('dashboard_earning_graph')?>
            </h3>
        </div>

        <div class="box-body" style="border-radius: 0px !important;">
          
        </div>
        

      </div>
    </div>

    <div class="col-lg-4">
      <div class="row">

        <!-- application users hidden -->
    <div class="col-md-12 hidden">
      <div class="box box-danger">
        <div class="box-header with-border">
          <h3 class="box-title">Application Users</h3>
          <div class="box-tools pull-right" style="width: 58%; padding-bottom: 0px; padding-top: 10px;">
                     <form class="" role="form" action="#">
            <div class="form-group" style="margin-bottom: 8px;">
              <div class="input-group">
                <span class="input-group-addon" style="background-color: rgb(255, 255, 255);"><i class="fa fa-user"></i></span>
                <input class="form-control" placeholder="Search users" type="text">
              </div>
            </div> 
          </form>
                    </div>
        
        </div><!-- /.box-header -->
        <div class="box-body no-padding" >
          <ul class="users-list clearfix">
          <li>
            <img src="<?php echo base_url('uploads/images/user8-128x128.jpg'); ?>" alt="User Image">
            <a class="users-list-name" href="#">Alexander Pierce</a>
            <span class="users-list-date">Today</span>
          </li>
          <li>
            <img src="<?php echo base_url('uploads/images/user8-128x128.jpg'); ?>" alt="User Image">
            <a class="users-list-name" href="#">Norman</a>
            <span class="users-list-date">Yesterday</span>
          </li>
          <li>
            <img src="<?php echo base_url('uploads/images/user8-128x128.jpg'); ?>" alt="User Image">
            <a class="users-list-name" href="#">Jane</a>
            <span class="users-list-date">12 Jan</span>
          </li>
          <li>
            <img src="<?php echo base_url('uploads/images/user8-128x128.jpg'); ?>" alt="User Image">
            <a class="users-list-name" href="#">John</a>
            <span class="users-list-date">12 Jan</span>
          </li>
          <li>
            <img src="<?php echo base_url('uploads/images/user8-128x128.jpg'); ?>" alt="User Image">
            <a class="users-list-name" href="#">Alexander</a>
            <span class="users-list-date">13 Jan</span>
          </li>
          <li>
            <img src="<?php echo base_url('uploads/images/user8-128x128.jpg'); ?>" alt="User Image">
            <a class="users-list-name" href="#">Sarah</a>
            <span class="users-list-date">14 Jan</span>
          </li>
          <li>
            <img src="<?php echo base_url('uploads/images/user8-128x128.jpg'); ?>" alt="User Image">
            <a class="users-list-name" href="#">Sarah</a>
            <span class="users-list-date">14 Jan</span>
          </li>
          <li>
            <img src="<?php echo base_url('uploads/images/user8-128x128.jpg'); ?>" alt="User Image">
            <a class="users-list-name" href="#">Sarah</a>
            <span class="users-list-date">14 Jan</span>
          </li>
          <li>
            <img src="<?php echo base_url('uploads/images/user8-128x128.jpg'); ?>" alt="User Image">
            <a class="users-list-name" href="#">Sarah</a>
            <span class="users-list-date">14 Jan</span>
          </li>
          <li>
            <img src="<?php echo base_url('uploads/images/user8-128x128.jpg'); ?>" alt="User Image">
            <a class="users-list-name" href="#">Sarah</a>
            <span class="users-list-date">14 Jan</span>
          </li>
          <li>
            <img src="<?php echo base_url('uploads/images/user8-128x128.jpg'); ?>" alt="User Image">
            <a class="users-list-name" href="#">Sarah</a>
            <span class="users-list-date">14 Jan</span>
          </li>
          <li>
            <img src="<?php echo base_url('uploads/images/user8-128x128.jpg'); ?>" alt="User Image">
            <a class="users-list-name" href="#">Sarah</a>
            <span class="users-list-date">14 Jan</span>
          </li>
          </ul><!-- /.users-list -->
        </div><!-- /.box-body -->
        <div class="box-footer text-center">
          <a href="javascript::" class="uppercase" style="color: #428BCA;">View All Users</a>
        </div><!-- /.box-footer -->
      </div>
    </div>
        <!--<div class="col-lg-12">
          <section class="panel">
            <center><canvas id="pai" width="200" height="200"/></canvas></center>
          </section>
        </div>

        <div class="col-lg-12">
          <section class="panel">
            <center><canvas id="chart-area" width="200" height="200"/></canvas></center>
          </section>
        </div>-->
      </div>
    </div>
</div>

<div class="row">
  <div class="col-lg-8">
    <div class="panel panel-inverse">
      <div class="panel-heading">
        <div class="panel-heading-btn">
          <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
          <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
          <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
          <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
        </div>
        <h4 class="panel-title"><?=$this->lang->line('dashboard_earning_graph')?></h4>
      </div>
      <div class="panel-body">
        <div id="interactive-chart" class="height-sm" style="padding: 0px; position: relative; width: 100%; margin: 0px auto;">
          <canvas width="1274" height="600" style=" direction: ltr; left: 0px; top: 0px;" class="flot-base" id="graph" /></canvas>
          <div id="legend"></div>
        </div>
      </div>
    </div>
  </div>
  <div class="col-lg-4">
    <div class="panel panel-inverse" data-sortable-id="index-10">
      <div class="panel-heading">
        <div class="panel-heading-btn">
          <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
          <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
          <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
          <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove" data-original-title="" title=""><i class="fa fa-times"></i></a>
        </div>
        <h4 class="panel-title">Calendar</h4>
      </div>
      <div class="panel-body">
        <div id="admin_calendar" style="width: 100%" class="datepicker-full-width"><div></div></div>
      </div>
    </div>
  </div>
</div>

  <script type="text/javascript">
    $(function() {
      return false;
        var date = new Date();
        var d = date.getDate(),
                m = date.getMonth(),
                y = date.getFullYear();
        $('#admin_calendar').fullCalendar({
            header: {
                left: 'prev,next',
                center: 'title',
                right: 'prev,next'
            },
            buttonText: {//This is to add icons to the visible buttons
                prev: "<span class='fa fa-caret-left'></span>",
                next: "<span class='fa fa-caret-right'></span>",
                today: 'today',
                month: 'month',
                week: 'week',
                day: 'day'
            }
        });
    });
  </script>
  
<div class="row hidden">
  
  <div class="col-md-6 col-xs-12 col-sm-6 col-lg-3">
    <div class="box box-danger">
      <div class="box-header with-border">
        <h3 class="box-title">Chart</h3>
        <div class="box-tools pull-right">
        <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
        <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
        </div>
      </div>
      <div class="box-body">
         <center><canvas id="pai" width="200" height="200"/></canvas></center>
      </div><!-- /.box-body -->
    </div> 
  </div>
  
  <div class="col-md-6 col-xs-12 col-sm-6 col-lg-3">
    <div class="box box-danger">
      <div class="box-header with-border">
        <h3 class="box-title">Chart</h3>
        <div class="box-tools pull-right">
        <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
        <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
        </div>
      </div>
      <div class="box-body">
         <center><canvas id="chart-area" width="200" height="200"/></canvas></center>
      </div><!-- /.box-body -->
    </div> 
  </div>
  
  <div class="col-md-12 col-xs-12 col-sm-12 col-lg-6">
    <div class="box box-danger">
        <div class="box-header">
          <h3 class="box-title">
              <?=$this->lang->line('dashboard_notice')?>
            </h3>
      <div class="box-tools pull-right">
        <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
        <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
      </div>
        </div>

        <div class="box-body" style="padding: 0px;">
          <table class="table table-hover">
              <tbody>
                <?php 

                  if(count($notices)) {
                    $i =1;
                    foreach ($notices as $key => $notice) {
                      if($i != 8) {
                        echo "<tr>";
                          echo "<td>";
                            echo $i;
                          echo "</td>";

                          echo "<td>";
                            if(strlen($notice->title) > 20) {
                               $title = substr($notice->title, 0,20). ".."; 
                            } else {
                                $title = $notice->title;
                            }
                            echo strip_tags($title);
                          echo "</td>";

                          echo "<td>";
                            if(strlen($notice->notice) > 80) {
                              $discription = substr($notice->notice, 0,80). ".."; 
                            } else {
                                $discription = $notice->notice;
                            }
                            echo strip_tags($discription);
                          echo "</td>";

                          echo "<td>";
                            echo btn_dash_view('notice/view/'.$notice->noticeID, $this->lang->line('view'));
                          echo "</td>";
                        echo "</tr>";
                        $i++;
                      } else {
                        break;
                      }
                      
                    }
                  }


                ?>
              </tbody>
          </table>
        </div>
    
    <div class="box-footer text-center">
      <a href="javascript::" class="uppercase" style="color: #428BCA;">View All Notice</a>
    </div><!-- /.box-footer -->
      </div>
  </div>
  
    <!--<div class="col-sm-4">
      <?php if(count($user)) { ?>
        <section class="panel">
          <div class="profile-db-head">
            <a href="<?=base_url('profile/index')?>">
              <?=img(base_url('uploads/images/'.$user->photo));?>
            </a>

            <h1><?=$user->name?></h1>
            <p><?=$this->lang->line($user->usertype)?></p>

          </div>
          <table class="table table-hover">
              <tbody>
                  <tr>
                    <td>
                      <i class="glyphicon glyphicon-user" style="color:#FDB45C;"></i>
                    </td>
                    <td><?=$this->lang->line('dashboard_username')?></td>
                    <td><?=$user->username?></td>
                  </tr>
                  <tr>
                      <td>
                        <i class="fa fa-envelope" style="color:#FDB45C;"></i>
                      </td>
                      <td><?=$this->lang->line('dashboard_email')?></td>
                    <td><?=$user->email?></td>
                  </tr>
                  <tr>
                    <td>
                      <i class="fa fa-phone" style="color:#FDB45C;"></i>
                    </td>
                    <td><?=$this->lang->line('dashboard_phone')?></td>
                    <td><?=$user->phone?></td>
                  </tr>
                  <tr>
                    <td>
                      <i class=" fa fa-globe" style="color:#FDB45C;"></i>
                    </td>
                    <td><?=$this->lang->line('dashboard_address')?></td>
                    <td><?=$user->address?></td>
                  </tr>
              </tbody>
          </table>
        </section>
      <?php } ?>
    </div>

    <div class="col-sm-8">
      <div class="box box-info">
        <div class="box-header">
          <h3 class="box-title">
              <?=$this->lang->line('dashboard_notice')?>
            </h3>
        </div>

        <div class="box-body" style="padding: 0px;">
          <table class="table table-hover">
              <tbody>
                <?php 

                  if(count($notices)) {
                    $i =1;
                    foreach ($notices as $key => $notice) {
                      if($i != 8) {
                        echo "<tr>";
                          echo "<td>";
                            echo $i;
                          echo "</td>";

                          echo "<td>";
                            if(strlen($notice->title) > 20) {
                               $title = substr($notice->title, 0,20). ".."; 
                            } else {
                                $title = $notice->title;
                            }
                            echo strip_tags($title);
                          echo "</td>";

                          echo "<td>";
                            if(strlen($notice->notice) > 80) {
                              $discription = substr($notice->notice, 0,80). ".."; 
                            } else {
                                $discription = $notice->notice;
                            }
                            echo strip_tags($discription);
                          echo "</td>";

                          echo "<td>";
                            echo btn_dash_view('notice/view/'.$notice->noticeID, $this->lang->line('view'));
                          echo "</td>";
                        echo "</tr>";
                        $i++;
                      } else {
                        break;
                      }
                      
                    }
                  }


                ?>
              </tbody>
          </table>
        </div>

      </div>
    </div>-->
  </div>