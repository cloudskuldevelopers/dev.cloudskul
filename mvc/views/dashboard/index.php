
<div class="row">
	<div class="col-md-2 hidden-xs">
		<img src="<?php echo base_url('uploads/images/logo.png'); ?>" style="width: auto; height: 40px; margin-top: -8px;" class="img-thumbnail img-responsive" />
	</div>
	<div class="col-md-7 col-xs-9 text-center">
		<h1 class="page-header"><?php echo $this->session->userdata('school');  ?></h1>
	</div>
	<div class="col-md-3 col-xs-3">
		<!-- begin breadcrumb -->
		<ol class="breadcrumb pull-right">
			<li><a href="javascript:;">Home</a></li>
			<li class="active">Dashboard</li>
		</ol>
		<!-- end breadcrumb -->
	</div>
</div>

<!-- dont touch below this line -->
<style type="text/css">
#interactive-chart > *{

}
</style>
<script type="text/javascript" src="<?php echo base_url('assets/fullcalendar/fullcalendar.min.js'); ?>"></script>

<!-- All Dashboard Views for various usertypes are now in their respective files.  -->

<?php 
    $usertype = $this->session->userdata("usertype");
    if($usertype == "Admin") {
?>
  <?php $this->load->view("dashboard/admin_dashboard"); ?>

<?php } elseif($usertype == "Teacher") { ?>
  <?php $this->load->view("dashboard/teacher_dashboard"); ?>

<?php } elseif($usertype == "Accountant") { ?>

  <?php $this->load->view("dashboard/accountant_dashboard"); ?>

<?php } elseif($usertype == "Librarian") { ?>

  <?php $this->load->view("dashboard/librarian_dashboard"); ?>

<?php } elseif($usertype == "Student") { ?>

  <?php $this->load->view("dashboard/student_dashboard"); ?>

<?php } elseif($usertype == "Parent") { ?>

  <?php $this->load->view("dashboard/parent_dashboard"); ?>

<?php } elseif($usertype == "Super Admin") { ?>

  <?php $this->load->view("dashboard/super_admin_dashboard"); ?>

<?php
  }

?>

<a href="javascript:;" class="btn btn-icon btn-circle btn-success btn-scroll-to-top fade in" data-click="scroll-top"><i class="fa fa-angle-up"></i></a>
