<div class="row">
	<div class="col-md-2 hidden-xs">
		<img src="<?php echo base_url('uploads/images/logo.png'); ?>" style="width: auto; height: 40px; margin-top: -8px;" class="img-thumbnail img-responsive" />
	</div>
	<div class="col-md-7 col-xs-9 text-center">
		<h1 class="page-header"><?php echo $this->session->userdata('school');  ?></h1>
	</div>
	<div class="col-md-3 col-xs-3">
		<!-- begin breadcrumb -->
		<ol class="breadcrumb pull-right">
			<li><a href="<?=base_url("dashboard/index")?>"><i class="fa fa-laptop"></i> <?=$this->lang->line('menu_dashboard')?></a></li>
            <li class="active"><?=$this->lang->line('menu_childcare')?></li>
		</ol>
		<!-- end breadcrumb -->
		
	</div>
</div>
<div id="page-container">
<!-- begin panel -->
<div class="panel panel-inverse" data-sortable-id="ui-widget-3">
	<div class="panel-heading">
		<h4 class="panel-title"><i class="fa fa-pencil"></i> <?=$this->lang->line('panel_title')?></h4>
	</div>
	<div class="panel-body">
		
	</div>
</div>
</div>









<div class="box box-danger">
    <div class="box-header with-border">
        <h3 class="box-title"><i class="fa icon-invoice"></i> <?=$this->lang->line('parents_pickup_policy')?></h3>

       
        <ol class="breadcrumb">
            <li><a href="<?=base_url("dashboard/index")?>"><i class="fa fa-laptop"></i> <?=$this->lang->line('menu_dashboard')?></a></li>
            <li class="active"><?=$this->lang->line('menu_childcare')?></li>
        </ol>
    </div><!-- /.box-header -->
    <!-- form start -->
    <div class="box-body">
        <div class="row">
            <div class="col-sm-12">
                <h3 class="box-title"><i class="fa icon-invoice"></i> Pickup Instruction</h3>
                <div class="row" style="margin-bottom: 10px;">
                  <div style="padding:10px;" class="col-md-6"><a href="#only_by_parent" id="pickup_policy_da" class="btn btn-block btn-success"> Only By Parent </a></div>
                  <div style="padding:10px;" class="col-md-6"><a href="#others" id="pickup_policy" class="btn btn-block btn-success"> Another Person Other than Parent </a></div>
                </div>
                <!--div class="col-sm-6 col-sm-offset-3 list-group">
                    <div class="list-group-item list-group-item-warning">
                        <!-form style="" class="form-horizontal" role="form" method="post">  
                            <div class="form-group">              
                                <label for="studentID" class="col-sm-2 col-sm-offset-2 control-label">
                                    <?=$this->lang->line("setfee_student")?>
                                </label>
                                <div class="col-sm-6">
                                    <?php /*
                                        $array = array("0" => $this->lang->line("invoice_select_student"));
                                        if($students) {
                                            foreach ($students as $student) {
                                                $array[$student->studentID] = $student->name;
                                            }
                                        }
                                        echo form_dropdown("studentID", $array, set_value("studentID", $set), "id='studentID' class='form-control'");*/
                                    ?>
                                </div>
                            </div>
                        </form->
                    </div>
                </div-->
                <?php //if($policy == '1'){ ?>
                <div id="hide-table">
                    <?php
                    $usertype = $this->session->userdata("usertype");
                    if($usertype == "Admin" || $usertype == "Super Admin" || $usertype == "Parent") {
                ?>
                <h5 class="page-header">
                    <a href="<?php echo base_url('child_care/add') ?>">
                        <i class="fa fa-plus"></i> 
                        <?=$this->lang->line('add_individual')?>
                    </a>
                </h5>
                <?php } ?>

                    <table id="example1" class="table table-striped table-bordered table-hover dataTable no-footer">
                        <thead>
                            <tr>
                                <th><?=$this->lang->line('slno')?></th>
                                <th><?=$this->lang->line('pickup_first_name')?></th>
                                <th><?=$this->lang->line('pickup_last_name')?></th>
                                <th><?=$this->lang->line('pickup_email')?></th>
                                <th><?=$this->lang->line('pickup_phone')?></th>
                                <th><?=$this->lang->line('pickup_profession')?></th>
                                <th><?=$this->lang->line('action')?></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php if(isset($pickups)){ if(count($pickups)) {$i = 1; foreach($pickups as $pickup) { ?>
                                <tr>
                                    <td data-title="<?=$this->lang->line('slno')?>">
                                        <?php echo $i; ?>
                                    </td>
                                    <td data-title="<?=$this->lang->line('pickup_first_name')?>">
                                        <?php echo $pickup->first_name; ?>
                                    </td>

                                    <td data-title="<?=$this->lang->line('pickup_last_name')?>">
                                        <?php echo $pickup->last_name; ?>
                                    </td>
       
                                    <td data-title="<?=$this->lang->line('pickup_email')?>">
                                        <?php echo $pickup->email; ?>
                                    </td>

                                    <td data-title="<?=$this->lang->line('pickup_phone')?>">
                                        <?php echo $pickup->phone; ?>
                                    </td>

                                    <td data-title="<?=$this->lang->line('pickup_profession')?>">
                                        <?php echo $pickup->profession; ?>
                                    </td>

                                    
                                    <td data-title="<?=$this->lang->line('action')?>">
                                        <?php echo btn_view('child_care/view/'.$pickup->policyID, $this->lang->line('view')) ?>
                                        <?php //if($usertype == "Admin" || $usertype == "Super Admin" || $usertype == "Accountant") { ?>
                                        <?php echo btn_edit('child_care/edit/'.$pickup->policyID, $this->lang->line('edit')) ?>
                                        <?php echo btn_delete('child_care/delete/'.$pickup->policyID, $this->lang->line('delete'))?>
                                        <?php //} ?>
                                    </td>
                                </tr>
                            <?php $i++; }} } ?>
                        </tbody>
                    </table>
                </div>
                <?php //} ?>

            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    //$('#hide-table').hide();
    var policy = "<?=$policy?>";
    if(policy == 0){
        $('#hide-table').hide();
    }

    $('#pickup_policy').click(function() {
        
        var ID = "<?=$this->session->userdata('loginuserID')?>";
        var status = "active";
        $.ajax({
            type: 'POST',
            url: "<?=base_url('child_care/activate')?>",
            data: "id=" + ID +"&status="+status,
            dataType: "html",
            success: function(data) {
                $('#hide-table').show();
            }
        });
    });
    $('#pickup_policy_da').click(function() {
        
        var ID = "<?=$this->session->userdata('loginuserID')?>";
        var status = "inactive";
        $.ajax({
            type: 'POST',
            url: "<?=base_url('child_care/activate')?>",
            data: "id=" + ID +"&status="+status,
            dataType: "html",
            success: function(data) {
                $('#hide-table').hide();
            }
        });
    });
</script>
