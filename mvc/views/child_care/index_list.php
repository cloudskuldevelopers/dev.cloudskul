<style type="text/css">
	ul{
		list-style:circle;
	}
</style>

<div class="row">
	<div class="col-md-2 hidden-xs">
		<img src="<?php echo base_url('uploads/images/logo.png'); ?>" style="width: auto; height: 40px; margin-top: -8px;" class="img-thumbnail img-responsive" />
	</div>
	<div class="col-md-7 col-xs-9 text-center">
		<h1 class="page-header"><?php echo $this->session->userdata('school');  ?></h1>
	</div>
	<div class="col-md-3 col-xs-3">
		<!-- begin breadcrumb -->
		<ol class="breadcrumb pull-right">
			<li><a href="<?=base_url("dashboard/index")?>"><i class="fa fa-laptop"></i> <?=$this->lang->line('menu_dashboard')?></a></li>
            <li class="active"><?=$this->lang->line('menu_childcare')?></li>
		</ol>
		<!-- end breadcrumb -->
		
	</div>
</div>
<div id="page-container">
<!-- begin panel -->
<div class="panel panel-inverse" data-sortable-id="ui-widget-3">
	<div class="panel-heading">
		<h4 class="panel-title"><i class="fa fa-pencil"></i> <?=$this->lang->line('parents_pickup_policy')?></h4>
	</div>
	<div class="panel-body">
		<?php 
		   $usertype = $this->session->userdata("usertype");
		?>
		<div class="col-sm-12" style="border-bottom:1px solid #F0F3F5; margin-bottom:10px;">
			<div class="col-md-6 add_padding" style="margin-bottom:10px;">
				<form style="" class="form-horizontal" role="form" method="post">
					<div class="input-group">
						<span class="input-group-addon offset_addon"><i class="fa fa-sitemap"></i></span>
						<?php
								$array = array("0" => $this->lang->line("childcare_select_class"));
								foreach ($classes as $classa) {
									$array[$classa->classesID] = $classa->classes;
								}
								echo form_dropdown("classesID", $array, set_value("classesID", $set), "id='classesID' class='form-control offset_height'");
							?>
					</div>
				</form>
			</div>
		</div>
		
		<?php if(count($students) > 0 ) { ?>

		<div class="col-sm-12">

			<div class="nav-tabs-custom" style="border:1px solid #C1CCD1; border-radius:5px;">
				<ul class="nav nav-tabs">
					<li class="active"><a data-toggle="tab" href="#all" aria-expanded="true"><?=$this->lang->line("childcare_all_students")?></a></li>
					<?php foreach ($sections as $key => $section) {
						echo '<li class=""><a data-toggle="tab" href="#'.$section->schoolID.$section->sectionID .'" aria-expanded="false">'. $this->lang->line("student_section")." ".$section->section. " ( ". $section->category." )".'</a></li>';
					} ?>
				</ul>

				<div class="tab-content">
					<div id="all" class="tab-pane active">
						<div id="hide-table">
							<table id="example1" class="table table-striped table-bordered table-hover dataTable no-footer">
								<thead>
									<tr>
										<th class="col-sm-2"><?=$this->lang->line('slno')?></th>
										<th class="col-sm-2"><?=$this->lang->line('child_care_student_name')?></th>
										<th class="col-sm-2"><?=$this->lang->line('child_care_parent_name')?></th>
										<th class="col-sm-3"><?=$this->lang->line('child_care_authorized')?></th>
										<th class="col-sm-2"><?=$this->lang->line('action')?></th>
									</tr>
								</thead>
								<tbody> <?php /*echo "<pre>"; var_dump($students);echo "</pre>";*/?>
									<?php if(count($students)) {$i = 1; foreach($students as $student) { ?>
										<tr> 
											<td data-title="<?=$this->lang->line('slno')?>">
												<?php echo $i; ?>
											</td>

											<td data-title="<?=$this->lang->line('child_care_student_name')?>">
												<?php echo $student->name; ?>
											</td>
											<td data-title="<?=$this->lang->line('child_care_parent_name')?>">
												<?php echo "<strong>Father : </strong>".$student->father_name."<br> <strong>Mother : </strong>".$student->mother_name; ?>
											</td>
											<td data-title="<?=$this->lang->line('child_care_authorized')?>">
												<?php if(is_null($student->adult) || empty($student->adult) || $student->pickup ==  0){ echo "<p>NIL</p>" ; }else{ ?>
												<?php $num = explode(';', $student->adult) ; ?>
												<?php echo "<ul class=''>" ; ?>
												<?php foreach ($num as $key => $value) {
													$adult = explode('&', $value) ;
													//print_r($adult);
													echo "<a href='#'><li emman='".$student->studentID."' etti='".$student->name."' class='adult' id='".$adult[0]."'>".$adult[1].", ".$adult[2]."</li></a>";
												}
												?>
												<?php echo "</ul>"; ?>
												<?php } ?>
											</td>
											<td data-title="<?=$this->lang->line('action')?>">
												<?php 
													if($usertype == "Admin" || $usertype == "Super Admin") {
														echo btn_view('student/view/'.$student->studentID."/".$set, $this->lang->line('view'));
														//echo btn_edit('student/edit/'.$student->studentID."/".$set, $this->lang->line('edit'));
														//echo btn_delete('student/delete/'.$student->studentID."/".$set, $this->lang->line('delete'));
													} elseif ($usertype == "Teacher") {
														echo btn_view('student/view/'.$student->studentID."/".$set, $this->lang->line('view'));
													}

												?>
											</td>
									   </tr>
									<?php $i++; }} ?>
								</tbody>
							</table>
						</div>

					</div>

					<?php foreach ($sections as $key => $section) { ?>
							<div id="<?=$section->schoolID.$section->sectionID?>" class="tab-pane">
								<div id="hide-table">
									<table class="table table-striped table-bordered table-hover dataTable no-footer">
										<thead>
											<tr>
												<th class="col-sm-2"><?=$this->lang->line('slno')?></th>
												<th class="col-sm-2"><?=$this->lang->line('child_care_student_name')?></th>
												<th class="col-sm-2"><?=$this->lang->line('child_care_parent_name')?></th>
												<th class="col-sm-2"><?=$this->lang->line('child_care_authorized')?></th>
												<th class="col-sm-2"><?=$this->lang->line('action')?></th>
											</tr>
										</thead>
										<tbody>
											<?php if(count($allsection[$section->sectionID])) { $i = 1; foreach($allsection[$section->sectionID] as $student) { if($section->sectionID === $student->sectionID) { ?>
												<tr>
													<td data-title="<?=$this->lang->line('slno')?>">
														<?php echo $i; ?>
													</td>

													<td data-title="<?=$this->lang->line('child_care_student_name')?>">
														<?php echo $student->name; ?>
													</td>
													<td data-title="<?=$this->lang->line('child_care_parent_name')?>">
														<?php echo $student->roll; ?>
													</td>
													<td data-title="<?=$this->lang->line('child_care_authorized')?>">
														<?php echo $student->phone; ?>
													</td>
													<td data-title="<?=$this->lang->line('action')?>">
														<?php 
															if($usertype == "Admin" || $usertype == "Super Admin") {
																//echo btn_view('student/view/'.$student->studentID."/".$set, $this->lang->line('view'));
																//echo btn_edit('student/edit/'.$student->studentID."/".$set, $this->lang->line('edit'));
																//echo btn_delete('student/delete/'.$student->studentID."/".$set, $this->lang->line('delete'));
															} elseif ($usertype == "Teacher") {
																//echo btn_view('student/view/'.$student->studentID."/".$set, $this->lang->line('view'));
															}

														?>
													</td>
											   </tr>
											<?php $i++; }}} ?>
										</tbody>
									</table>
								</div>
							</div>
					<?php } ?>
				</div>

			</div> <!-- nav-tabs-custom -->
		</div> <!-- col-sm-12 for tab -->

	<?php } else { ?>
		<div class="col-sm-12">

			<div class="nav-tabs-custom">
				<ul class="nav nav-tabs">
					<li class="active"><a data-toggle="tab" href="#all" aria-expanded="true"><?=$this->lang->line("childcare_all_students")?></a></li>
				</ul>


				<div class="tab-content">
					<div id="all" class="tab-pane active">
						<div id="hide-table">
							<table id="example1" class="table table-striped table-bordered table-hover dataTable no-footer">
								<thead>
									<tr>
										<th class="col-sm-2"><?=$this->lang->line('slno')?></th>
										<th class="col-sm-2"><?=$this->lang->line('child_care_student_name')?></th>
										<th class="col-sm-2"><?=$this->lang->line('child_care_parent_name')?></th>
										<th class="col-sm-2"><?=$this->lang->line('child_care_authorized')?></th>
										<th class="col-sm-2"><?=$this->lang->line('action')?></th>
									</tr>
								</thead>
								<tbody>
									<?php if(count($students)) {$i = 1; foreach($students as $student) { ?>
										<tr>
											<td data-title="<?=$this->lang->line('slno')?>">
												<?php echo $i; ?>
											</td>

											<td data-title="<?=$this->lang->line('child_care_student_name')?>">
												<?php echo $student->name; ?>
											</td>
											<td data-title="<?=$this->lang->line('child_care_parent_name')?>">
												<?php echo $student->roll; ?>
											</td>
											<td data-title="<?=$this->lang->line('child_care_authorized')?>">
												<?php echo $student->phone; ?>
											</td>
											<td data-title="<?=$this->lang->line('action')?>">
												<?php 
													if($usertype == "Admin" || $usertype == "Super Admin") {
														echo btn_view('student/view/'.$student->studentID."/".$set, $this->lang->line('view'));
														//echo btn_edit('student/edit/'.$student->studentID."/".$set, $this->lang->line('edit'));
														//echo btn_delete('student/delete/'.$student->studentID."/".$set, $this->lang->line('delete'));
													} elseif ($usertype == "Teacher") {
														echo btn_view('student/view/'.$student->studentID."/".$set, $this->lang->line('view'));
													}

												?>
											</td>
									   </tr>
									<?php $i++; }} ?>
								</tbody>
							</table>
						</div>

					</div>
				</div>
			</div> <!-- nav-tabs-custom -->
		</div>
	<?php } ?>
	</div>
</div>
</div>

<form class="form-horizontal" role="form" action="<?=base_url('#');?>" method="post">
    <div class="modal fade" id="mail">
      <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title">Student/Pupil Pickup</h4>
            </div>
            <div class="modal-body">
            
                <?php 
                    if(form_error('to')) 
                        echo "<div class='form-group has-error' >";
                    else     
                        echo "<div class='form-group' >";
                ?>
                    <label for="to" class="col-sm-4 control-label">
                        Name: 
                    </label>
                    <div class="col-sm-6">
                        <p id="adultName">Name of Adult</p>
                    </div>
                    <span class="col-sm-3 control-label" id="to_error">
                    </span>
                </div>

                <input type="hidden" id="adultID" value="" name="adultID" />
                <input type="hidden" id="studentID" value="" name="studentID" />

                <?php 
                    if(form_error('subject')) 
                        echo "<div class='form-group has-error' >";
                    else     
                        echo "<div class='form-group' >";
                ?>
                    <label for="subject" class="col-sm-4 control-label">
                        Student/Pupil Name:
                    </label>
                    <div class="col-sm-6">
                        <p id="studentName">Name of Student/Pupil</p>
                    </div>
                    <span class="col-sm-3 control-label" id="subject_error">
                    </span>

                </div>

                <?php 
                    if(form_error('time')) 
                        echo "<div class='form-group has-error' >";
                    else     
                        echo "<div class='form-group' >";
                ?>
                    <label for="message" class="col-sm-4 control-label">
                        Time:
                    </label>
                    <div class="col-sm-6">
                        <input class="form-control" id="time" name="time" value="" ></textarea>
                    </div>
                </div>

                <?php 
                    if(form_error('date')) 
                        echo "<div class='form-group has-error' >";
                    else     
                        echo "<div class='form-group' >";
                ?>
                    <label for="message" class="col-sm-4 control-label">
                        Date: 
                    </label>
                    <div class="col-sm-6">
                        <input class="form-control" id="date" name="date" value="<?php echo Date('d-m-Y'); ?>" ></textarea>
                    </div>
                </div>

            
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" style="margin-bottom:0px;" data-dismiss="modal"><?=$this->lang->line('close')?></button>
                <input type="button" id="send_pdf" class="btn btn-success" value="<?=$this->lang->line("submit")?>" />
            </div>
        </div>
      </div>
    </div>
</form>

<script type="text/javascript">
$('.adult').click(function() {
    var type = $(this).attr('id');
    var name = $(this).html();
    var studentID = $(this).attr('emman');
    var studentName = $(this).attr('etti');
    var d = new Date();
    console.log("time: "+d);
    var curr_hour = (d.getHours()<10?'0':'') + d.getHours();
    var curr_min = (d.getMinutes()<10?'0':'') + d.getMinutes();
    var time = curr_hour+":"+curr_min;
    //var time = "<?php echo Date('H:i:s'); ?>"

    $("#time").val(time);
    $("#adultName").html(name);
    $("#studentName").html(studentName);
    $("#adultID").val(type);
    $("#studentID").val(studentID);
    //alert(name);
    $("#mail").modal("show");
    return;

    /*alert(type);service_c
    payment" 
    payment_t
    total" id
    //return false;*/
    $.ajax({
        type: 'POST',
        url: "<?=base_url('invoice/payment_type')?>",
        data: "type=" + type,
        dataType: "html",
        beforeSend:function(){
                createAjaxLoader();
        },
        success: function(data) {
            if(data != "") {
                deleteAjaxLoader();
                //alert(data);
                //return false;
                if(type != 'BANK_BRANCH'){
                    $(".transFeePopup").html(data);
                    $(".transactionFeePopup").modal("show");
                }else if(type == 'BANK_BRANCH'){
                    $(".transFeePopup").html(data);
                    $(".bankFeePopup").modal("show");
                }
                $('#payment_type').val(type);
                $('#service_charge').val(data);
                var total = parseInt(fees) + parseInt(data);
                $('#total').val(total);
                $('#total0').html('N'+parseInt(total));
                $('#amt').html('N'+parseInt(data));

                /*var width = $("#bank_name").width();
                $(".book").css('width', width+25 + "px").show();
                $(".result").html(data);

                $('.result li').click(function(){
                    var result_value = $(this).text();
                    var result_code = $(this).attr('id');
                    $('#bank_name').val(result_value);
                    $('#bank_code').val(result_code);
                    $('.result').html(' ');
                    $('.book').hide();
                });*/
            } else {
                $(".book").hide();
            }
           
        }
    });
});

    $('#classesID').change(function() {
        var classesID = $(this).val();
        if(classesID == 0) {
            $('#hide-table').hide();
            $('.nav-tabs-custom').hide();
        } else {
            $.ajax({
                type: 'POST',
                url: "<?=base_url('child_care/student_list')?>",
                data: "id=" + classesID,
                dataType: "html",
                success: function(data) {
                    window.location.href = data;
                }
            });
        }
    });


    var status = '';
    var id = 0;
    $('.onoffswitch-small-checkbox').click(function() {
        if($(this).prop('checked')) {
            status = 'chacked';
            id = $(this).parent().attr("id");
        } else {
            status = 'unchacked';
            id = $(this).parent().attr("id");
        }

        if((status != '' || status != null) && (id !='')) {
            $.ajax({
                type: 'POST',
                url: "<?=base_url('student/active')?>",
                data: "id=" + id + "&status=" + status,
                dataType: "html",
                success: function(data) {
                    if(data == 'Success') {
                        toastr["success"]("Success")
                        toastr.options = {
                            "closeButton": true,
                            "debug": false,
                            "newestOnTop": false,
                            "progressBar": false,
                            "positionClass": "toast-top-right",
                            "preventDuplicates": false,
                            "onclick": null,
                            "showDuration": "500",
                            "hideDuration": "500",
                            "timeOut": "5000",
                            "extendedTimeOut": "1000",
                            "showEasing": "swing",
                            "hideEasing": "linear",
                            "showMethod": "fadeIn",
                            "hideMethod": "fadeOut"
                        }
                    } else {
                        toastr["error"]("Error")
                        toastr.options = {
                            "closeButton": true,
                            "debug": false,
                            "newestOnTop": false,
                            "progressBar": false,
                            "positionClass": "toast-top-right",
                            "preventDuplicates": false,
                            "onclick": null,
                            "showDuration": "500",
                            "hideDuration": "500",
                            "timeOut": "5000",
                            "extendedTimeOut": "1000",
                            "showEasing": "swing",
                            "hideEasing": "linear",
                            "showMethod": "fadeIn",
                            "hideMethod": "fadeOut"
                        }
                    }
                }
            });
        }
    }); 
</script>