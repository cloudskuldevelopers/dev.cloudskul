<div class="row">
	<div class="col-md-2 hidden-xs">
		<img src="<?php echo base_url('uploads/images/logo.png'); ?>" style="width: auto; height: 40px; margin-top: -8px;" class="img-thumbnail img-responsive" />
	</div>
	<div class="col-md-7 col-xs-9 text-center">
		<h1 class="page-header"><?php echo $this->session->userdata('school');  ?></h1>
	</div>
	<div class="col-md-3 col-xs-3">
		<!-- begin breadcrumb -->
		<ol class="breadcrumb pull-right">
			<li ><a href="<?php echo base_url('dashboard/index');?>"><i class="fa fa-laptop"></i> Dashboard</a></li>
			<li class=""><?=$this->lang->line('menu_user')?></li>
		</ol>
		<!-- end breadcrumb -->
	</div>
</div>

<div id="page-container">
<!-- begin panel -->
<div class="panel panel-inverse" data-sortable-id="ui-widget-3">
	<div class="panel-heading">
		 <?php 
			$usertype = $this->session->userdata("usertype");
			if($usertype == "Admin" || $usertype == "Super Admin") {
		?>
			<div class="btn-group-vertical pull-right">
				<a href="<?php echo base_url('user/add') ?>" type="button" class="btn btn-success btn-xs"><i class="fa fa-plus"></i> Add User</a>
			</div>
		<?php } ?>
		<h4 class="panel-title"><i class="fa fa-desktop"></i> <?=$this->lang->line('panel_title')?></h4>
	</div>
	<div class="panel-body">
		<table id="data-table" class="table table-striped table-bordered nowrap" width="100%">
			<thead>
				<tr>
					<th class="col-lg-1"><?=$this->lang->line('slno')?></th>
					<th class="col-lg-2"><?=$this->lang->line('user_photo')?></th>
					<th class="col-lg-2"><?=$this->lang->line('user_name')?></th>
					<th class="col-lg-2"><?=$this->lang->line('user_email')?></th>
					<th class="col-lg-2"><?=$this->lang->line('user_type')?></th>
					<th class="col-lg-1"><?=$this->lang->line('user_status')?></th>
					<th class="col-lg-2"><?=$this->lang->line('action')?></th>
				</tr>
			</thead>
			<tbody>
				<?php if(count($users)) {$i = 1; foreach($users as $user) { ?>
					<tr>
						<td data-title="<?=$this->lang->line('slno')?>">
							<?php echo $i; ?>
						</td>
						<td data-title="<?=$this->lang->line('user_photo')?>">
							<?php $array = array(
									"src" => base_url('uploads/images/'.$user->photo),
									'width' => '35px',
									'height' => '35px',
									'class' => 'img-rounded'

								);
								echo img($array); 
							?>
						</td>
						<td data-title="<?=$this->lang->line('user_name')?>">
							<?php echo $user->name; ?>
						</td>
						<td data-title="<?=$this->lang->line('user_email')?>">
							<?php echo $user->email; ?>
						</td>
						<td data-title="<?=$this->lang->line('user_type')?>">
							<?=$this->lang->line($user->usertype)?>
						</td>
						<td data-title="<?=$this->lang->line('user_status')?>">
						  <div class="onoffswitch-small" id="<?=$user->userID?>">
							  <input type="checkbox" id="myonoffswitch<?=$user->userID?>" class="onoffswitch-small-checkbox" name="paypal_demo" <?php if($user->useractive === '1') echo "checked='checked'"; ?>>
							  <label for="myonoffswitch<?=$user->userID?>" class="onoffswitch-small-label">
								  <span class="onoffswitch-small-inner"></span>
								  <span class="onoffswitch-small-switch"></span>
							  </label>
						  </div>           
						</td>
						<td data-title="<?=$this->lang->line('action')?>">
							<?php echo btn_view('user/view/'.$user->userID, $this->lang->line('view')) ?>
							<?php echo btn_edit('user/edit/'.$user->userID, $this->lang->line('edit')) ?>
							<?php echo btn_delete('user/delete/'.$user->userID, $this->lang->line('delete')) ?>
						</td>
					</tr>
				<?php $i++; }} ?>
			</tbody>
		</table>
	</div>
</div>
</div>

<script>
  var status = '';
  var id = 0;
  $('.onoffswitch-small-checkbox').click(function() {
      if($(this).prop('checked')) {
          status = 'chacked';
          id = $(this).parent().attr("id");
      } else {
          status = 'unchacked';
          id = $(this).parent().attr("id");
      }

      if((status != '' || status != null) && (id !='')) {
          $.ajax({
              type: 'POST',
              url: "<?=base_url('user/active')?>",
              data: "id=" + id + "&status=" + status,
              dataType: "html",
              success: function(data) {
                  if(data == 'Success') {
                      toastr["success"]("Success")
                      toastr.options = {
                        "closeButton": true,
                        "debug": false,
                        "newestOnTop": false,
                        "progressBar": false,
                        "positionClass": "toast-top-right",
                        "preventDuplicates": false,
                        "onclick": null,
                        "showDuration": "500",
                        "hideDuration": "500",
                        "timeOut": "5000",
                        "extendedTimeOut": "1000",
                        "showEasing": "swing",
                        "hideEasing": "linear",
                        "showMethod": "fadeIn",
                        "hideMethod": "fadeOut"
                      }
                  } else {
                      toastr["error"]("Error")
                      toastr.options = {
                        "closeButton": true,
                        "debug": false,
                        "newestOnTop": false,
                        "progressBar": false,
                        "positionClass": "toast-top-right",
                        "preventDuplicates": false,
                        "onclick": null,
                        "showDuration": "500",
                        "hideDuration": "500",
                        "timeOut": "5000",
                        "extendedTimeOut": "1000",
                        "showEasing": "swing",
                        "hideEasing": "linear",
                        "showMethod": "fadeIn",
                        "hideMethod": "fadeOut"
                      }
                  }
              }
          });
      }
  }); 
</script>

