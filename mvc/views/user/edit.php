<style type="text/css">
.fileUpload {
    margin: 0;
    overflow: hidden;
    position: relative;
}

.fileUpload input.upload {
    position: absolute;
    top: 0;
    right: 0;
    margin: 0;
    padding: 0;
    font-size: 20px;
    cursor: pointer;
    opacity: 0;
    filter: alpha(opacity=0);
}
</style>
<div class="row">
	<div class="col-md-2 hidden-xs">
		<img src="<?php echo base_url('uploads/images/logo.png'); ?>" style="width: auto; height: 40px; margin-top: -8px;" class="img-thumbnail img-responsive" />
	</div>
	<div class="col-md-7 col-xs-9 text-center">
		<h1 class="page-header"><?php echo $this->session->userdata('school');  ?></h1>
	</div>
	<div class="col-md-3 col-xs-3">
		<!-- begin breadcrumb -->
		<ol class="breadcrumb pull-right">
			<li><a href="<?=base_url("dashboard/index")?>"><i class="fa fa-laptop"></i> <?=$this->lang->line('menu_dashboard')?></a></li>
            <li><a href="<?=base_url("user/index")?>"><?=$this->lang->line('menu_user')?></a></li>
            <li class="active"><?=$this->lang->line('menu_edit')?> <?=$this->lang->line('menu_user')?></li>
		</ol>
		<!-- end breadcrumb -->
	</div>
</div>

<div id="page-container">
<!-- begin panel -->
<div class="panel panel-inverse" data-sortable-id="ui-widget-3">
	<div class="panel-heading">
		<?php 
			$usertype = $this->session->userdata("usertype");
			if($usertype == "Admin" || $usertype == "Super Admin") {
		?>
			<div class="btn-group-vertical pull-right">
				<a href="<?php echo base_url('user/add') ?>" type="button" class="btn btn-success btn-xs"><i class="fa fa-plus"></i> Add User</a>
			</div>
		<?php } ?>
		<h4 class="panel-title"><i class="fa fa-desktop"></i> <?=$this->lang->line('panel_title')?></h4>
	</div>
	<div class="panel-body">
		<div class="col-sm-8">
			<form class="form-horizontal" role="form" method="post" enctype="multipart/form-data">
				<?php 
					if(form_error('name')) 
						echo "<div class='form-group has-error' >";
					else     
						echo "<div class='form-group' >";
				?>
					<label for="user_name" class="col-sm-2 control-label">
						<?=$this->lang->line("user_name")?>
					</label>
					<div class="col-sm-6">
						<input type="text" class="form-control" id="name" name="name" value="<?=set_value('name', $user->name)?>" >

					</div>
					<span class="col-sm-4 control-label">
						<?php echo form_error('name'); ?>
					</span>
				</div>

				<?php 
					if(form_error('dob')) 
						echo "<div class='form-group has-error' >";
					else     
						echo "<div class='form-group' >";
				?>
					<label for="dob" class="col-sm-2 control-label">
						<?=$this->lang->line("user_dob")?>
					</label>
					<div class="col-sm-6">
						<input type="text" class="form-control" id="dob" name="dob" value="<?=set_value('dob', date("d-m-Y", strtotime($user->dob)))?>" >
					</div>
					<span class="col-sm-4 control-label">
						<?php echo form_error('dob'); ?>
					</span>
				</div>

				<?php 
					if(form_error('sex')) 
						echo "<div class='form-group has-error' >";
					else     
						echo "<div class='form-group' >";
				?>
					<label for="sex" class="col-sm-2 control-label">
						<?=$this->lang->line("user_sex")?>
					</label>
					<div class="col-sm-6">
						<?php 
							echo form_dropdown("sex", array($this->lang->line("user_sex_male") => $this->lang->line("user_sex_male"), $this->lang->line("user_sex_female") => $this->lang->line("user_sex_female")), set_value("sex", $user->sex), "id='sex' class='form-control'"); 
						?>
					</div>
					<span class="col-sm-4 control-label">
						<?php echo form_error('sex'); ?>
					</span>
				</div>

				<?php 
					if(form_error('religion')) 
						echo "<div class='form-group has-error' >";
					else     
						echo "<div class='form-group' >";
				?>
					<label for="religion" class="col-sm-2 control-label">
						<?=$this->lang->line("user_religion")?>
					</label>
					<div class="col-sm-6">
						<input type="text" class="form-control" id="religion" name="religion" value="<?=set_value('religion', $user->religion)?>" >
					</div>
					<span class="col-sm-4 control-label">
						<?php echo form_error('religion'); ?>
					</span>
				</div>

				<?php 
					if(form_error('email')) 
						echo "<div class='form-group has-error' >";
					else     
						echo "<div class='form-group' >";
				?>
					<label for="email" class="col-sm-2 control-label">
						<?=$this->lang->line("user_email")?>
					</label>
					<div class="col-sm-6">
						<input type="text" class="form-control" id="email" name="email" value="<?=set_value('email', $user->email)?>" >
					</div>
					<span class="col-sm-4 control-label">
						<?php echo form_error('email'); ?>
					</span>
				</div>

				<?php 
					if(form_error('phone')) 
						echo "<div class='form-group has-error' >";
					else     
						echo "<div class='form-group' >";
				?>
					<label for="phone" class="col-sm-2 control-label">
						<?=$this->lang->line("user_phone")?>
					</label>
					<div class="col-sm-6">
						<input type="text" class="form-control" id="phone" name="phone" value="<?=set_value('phone', $user->phone)?>" >
					</div>
					<span class="col-sm-4 control-label">
						<?php echo form_error('phone'); ?>
					</span>
				</div>

				<?php 
					if(form_error('address')) 
						echo "<div class='form-group has-error' >";
					else     
						echo "<div class='form-group' >";
				?>
					<label for="address" class="col-sm-2 control-label">
						<?=$this->lang->line("user_address")?>
					</label>
					<div class="col-sm-6">
						<input type="text" class="form-control" id="address" name="address" value="<?=set_value('address', $user->address)?>" >
					</div>
					<span class="col-sm-4 control-label">
						<?php echo form_error('address'); ?>
					</span>
				</div>

				<?php 
					if(form_error('jod')) 
						echo "<div class='form-group has-error' >";
					else     
						echo "<div class='form-group' >";
				?>
					<label for="jod" class="col-sm-2 control-label">
						<?=$this->lang->line("user_jod")?>
					</label>
					<div class="col-sm-6">
						<input type="text" class="form-control" id="jod" name="jod" value="<?=set_value('jod', date("d-m-Y", strtotime($user->jod)))?>" >
					</div>
					<span class="col-sm-4 control-label">
						<?php echo form_error('jod'); ?>
					</span>
				</div>

				<?php 
					if(isset($image)) 
						echo "<div class='form-group has-error' >";
					else     
						echo "<div class='form-group' >";
				?>
					<label for="photo" class="col-sm-2 control-label col-xs-8 col-md-2">
						<?=$this->lang->line("user_photo")?>
					</label>
					<div class="col-sm-4 col-xs-6 col-md-4">
						<input class="form-control"  id="uploadFile" placeholder="Choose File" disabled />  
					</div>

					<div class="col-sm-2 col-xs-6 col-md-2">
						<div class="fileUpload btn btn-success form-control">
							<span class="fa fa-repeat"></span>
							<span><?=$this->lang->line("upload")?></span>
							<input id="uploadBtn" type="file" class="upload" name="image" />
						</div>
					</div>
					 <span class="col-sm-4 control-label col-xs-6 col-md-4">
					   
						<?php if(isset($image)) echo $image; ?>
					</span>
				</div>

				<?php 
					if(form_error('usertype')) 
						echo "<div class='form-group has-error' >";
					else     
						echo "<div class='form-group' >";
				?>
					<label for="usertype" class="col-sm-2 control-label">
						<?=$this->lang->line("user_type")?>
					</label>
					<div class="col-sm-6">
						<?php 
							if($this->session->userdata('usertype') == "Super Admin") {
								echo form_dropdown("usertype", array("Admin" => $this->lang->line("admin"), "Accountant" => $this->lang->line("accountant"), "Librarian" => $this->lang->line("librarian")), set_value("usertype", $user->usertype), "id='usertype' class='form-control'"); 
							} else {
								echo form_dropdown("usertype", array("Accountant" => $this->lang->line("accountant"), "Librarian" => $this->lang->line("librarian")), set_value("usertype", $user->usertype), "id='usertype' class='form-control'"); 
							}
						?>
					</div>
					<span class="col-sm-4 control-label">
						<?php echo form_error('usertype'); ?>
					</span>
				</div>

				<div class="form-group">
					<div class="col-sm-offset-2 col-sm-8">
						<input type="submit" class="btn btn-success" value="<?=$this->lang->line("update_user")?>" >
					</div>
				</div>

			</form>
		</div>
	</div>
</div>
</div>





<script type="text/javascript">
document.getElementById("uploadBtn").onchange = function() {
    document.getElementById("uploadFile").value = this.value;
};
$('#dob').datepicker({ startView: 2 });
$('#jod').datepicker();
</script>
