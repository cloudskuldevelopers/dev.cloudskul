
<?php 
    $usertype = $this->session->userdata("usertype");
    if(count($user) && $usertype == "Admin") {
        
        if($usertype == "Admin" || $usertype == "Super iAdmin") {
?>
    <div class="well" style="background-color: #fff;">
        <div class="row">
            <div class="col-sm-6">
                <button class="btn btn-success btn-sm-cs" onclick="javascript:printDiv('printablediv')"><span class="fa fa-print"></span> <?=$this->lang->line('print')?> </button>
                <?php
                 echo btn_add_pdf('profile/print_preview/'.$user->userID."/", $this->lang->line('pdf_preview')) 
                ?>

                <button class="btn btn-primary btn-sm-cs" data-toggle="modal" data-target="#idCard"><span class="fa fa-floppy-o"></span> <?=$this->lang->line('idcard')?> </button>

                <?php echo btn_sm_edit('profile/edit/'.$user->userID."/", $this->lang->line('edit')) 
                ?>
                <button class="btn btn-default btn-sm-cs" data-toggle="modal" data-target="#mail"><span class="fa fa-envelope-o"></span> <?=$this->lang->line('mail')?></button>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb">
                    <li><a href="<?=base_url("dashboard/index")?>"><i class="fa fa-laptop"></i> <?=$this->lang->line('menu_dashboard')?></a></li>
                    <li><a href="<?=base_url("profile/index")?>"><?=$this->lang->line('menu_profile')?></a></li>
                    <li class="active"><?=$this->lang->line('view')?></li>
                </ol>
            </div>
        </div>
    </div>
    <?php } ?>
    
    <div id="printablediv">
        <section class="panel">
            <div class="profile-view-head">
                <a href="#">
                    <?=img(base_url('uploads/images/'.$user->photo))?>
                </a>

                <h1><?=$user->name?></h1>
                <p><?=$this->lang->line($user->usertype)?></p>

            </div>
			
			<div class="panel-body profile-view-dis">
				<div class="profileInfo">
						<div style="text-align: center;">
							<h1 class="profile_heading_offset"><?=$this->lang->line("personal_information")?></h1>
						</div>
						
						<div class="col-md-12 col-xs-12 col-sm-12 profileInformation" style="border: 1px solid gainsboro; padding-top: 20px; padding-bottom: 0px; margin-bottom: 30px;">
							<div class="col-md-6 col-xs-12">
								<blockquote>
									<p><span class="textBold"><i class="fa fa-calendar iconSchoolDetails"></i> 
										<span class="schoolDetails"><?=$this->lang->line("user_dob")?></span>
									</span>: <?=date("d M Y", strtotime($user->dob));?></p>
									
									<p><span class="textBold"><i class="fa fa-calendar iconSchoolDetails"></i> 
										<span class="schoolDetails"><?=$this->lang->line("user_jod")?></span>
									</span>:  <?=date("d M Y", strtotime($user->jod))?></p>
									
									<p><span class="textBold"><i class="fa fa-list iconSchoolDetails"></i> 
										<span class="schoolDetails"><?=$this->lang->line("user_sex")?></span>
									</span>: <?=$user->sex?></p>
									
									<p><span class="textBold"><i class="fa fa-crosshairs iconSchoolDetails"></i> 
										<span class="schoolDetails"><?=$this->lang->line("user_religion")?></span>
									</span>: <?=$user->religion?></p>
								</blockquote>
							</div>
							
							<div class="col-md-6 col-xs-12">
								<blockquote>
									<p><span class="textBold"><i class="fa fa-envelope-o iconSchoolDetails"></i> 
										<span class="schoolDetails"><?=$this->lang->line("user_email")?></span>
									</span>:  <?=$user->email?></p>
									
									<p><span class="textBold"><i class="fa fa-phone iconSchoolDetails"></i> 
										<span class="schoolDetails"><?=$this->lang->line("user_phone")?></span>
									</span>: <?=$user->phone?></p>
									
									<p style="font-size: 16px !important;"><span class="textBold"><i class="fa fa-building-o iconSchoolDetails"></i> 
										<span class="schoolDetails"><?=$this->lang->line("user_address")?></span>
									</span>: <?=$user->address?></p>
									
									<?php if($usertype == "Admin" || $usertype == "Super Admin") { ?>
										<p><span class="textBold"><i class="fa fa-user iconSchoolDetails"></i> 
											<span class="schoolDetails"><?=$this->lang->line("user_username")?></span>
										</span>: <?=$user->username?></p>
									<?php } ?>
								</blockquote>
							</div>
						</div>
					</div>
			</div>
            <!--<div class="panel-body profile-view-dis">
				<div class="profileBody" style="margin: 0px auto; margin-top: 120px;">
					<h1><?=$this->lang->line("personal_information")?></h1>
				</div>
                
                <div class="row">
                    <div class="profile-view-tab">
                        <p><span><?=$this->lang->line("user_dob")?> </span>: <?=date("d M Y", strtotime($user->dob));?></p>
                    </div>
                    <div class="profile-view-tab">
                        <p><span><?=$this->lang->line("user_jod")?> </span>: <?=date("d M Y", strtotime($user->jod))?></p>
                    </div>
                    <div class="profile-view-tab">
                        <p><span><?=$this->lang->line("user_sex")?> </span>: <?=$user->sex?></p>
                    </div>
                    <div class="profile-view-tab">
                        <p><span><?=$this->lang->line("user_religion")?> </span>: <?=$user->religion?></p>
                    </div>
                    <div class="profile-view-tab">
                        <p><span><?=$this->lang->line("user_email")?> </span>: <?=$user->email?></p>
                    </div>
                    <div class="profile-view-tab">
                        <p><span><?=$this->lang->line("user_phone")?> </span>: <?=$user->phone?></p>
                    </div>
                    <div class="profile-view-tab">
                        <p><span><?=$this->lang->line("user_address")?> </span>: <?=$user->address?></p>
                    </div>
                    <?php if($usertype == "Admin" || $usertype == "Super Admin") { ?>
                    <div class="profile-view-tab">
                        <p><span><?=$this->lang->line("user_username")?> </span>: <?=$user->username?></p>
                    </div>
                    <?php } ?>
                </div>-->

            </div>
        </section>
    </div>
<?php } ?>


<?php 
    $usertype = $this->session->userdata('usertype');
    if($usertype == "iAdmin") {
?>
    <section class="panel">
        <div class="profile-view-head">
            <a href="#">
                <?=img(base_url('uploads/images/'.$siteinfos->photo))?>
            </a>
            <h1><?=$siteinfos->name?></h1>
            <p><?=$this->lang->line($this->lang->line($siteinfos->usertype))?></p>
        </div>
        <div class="panel-body profile-view-dis">
            <h1><?=$this->lang->line("personal_information")?></h1>
            <div class="row">
                <div class="profile-view-tab">
                    <p><span><?=$this->lang->line("profile_email")?> </span>: <?=$siteinfos->email?></p>
                </div>
                <div class="profile-view-tab">
                    <p><span><?=$this->lang->line("profile_phone")?> </span>: <?=$siteinfos->phone?></p>
                </div>
                <div class="profile-view-tab">
                    <p><span><?=$this->lang->line("profile_address")?> </span>: <?=$siteinfos->address?></p>
                </div>
            </div>

        </div>
    </section>
<?php } elseif($usertype == "Librarian" || $usertype == "Accountant") { ?>
    <section class="panel">
        <div class="profile-view-head">
            <a href="#">
                <?=img(base_url('uploads/images/'.$user->photo))?>
            </a>

            <h1><?=$user->name?></h1>
            <p><?=$this->lang->line($user->usertype)?></p>
        </div>

        <div class="panel-body profile-view-dis">
            <h1><?=$this->lang->line("personal_information")?></h1>
            <div class="row">
                <div class="profile-view-tab">
                    <p><span><?=$this->lang->line("profile_dob")?> </span>: <?=date("d M Y", strtotime($user->dob));?></p>
                </div>
                <div class="profile-view-tab">
                    <p><span><?=$this->lang->line("profile_jod")?> </span>: <?=date("d M Y", strtotime($user->jod))?></p>
                </div>
                <div class="profile-view-tab">
                    <p><span><?=$this->lang->line("profile_sex")?> </span>: <?=$user->sex?></p>
                </div>
                <div class="profile-view-tab">
                    <p><span><?=$this->lang->line("profile_religion")?> </span>: <?=$user->religion?></p>
                </div>
                <div class="profile-view-tab">
                    <p><span><?=$this->lang->line("profile_email")?> </span>: <?=$user->email?></p>
                </div>
                <div class="profile-view-tab">
                    <p><span><?=$this->lang->line("profile_phone")?> </span>: <?=$user->phone?></p>
                </div>
                <div class="profile-view-tab">
                    <p><span><?=$this->lang->line("profile_address")?> </span>: <?=$user->address?></p>
                </div>
            </div>
        </div>
    </section>
<?php } elseif($usertype == "Teacher") { ?>
    <section class="panel">
        <div class="profile-view-head">
            <a href="#">
                <?=img(base_url('uploads/images/'.$teacher->photo))?>
            </a>

            <h1><?=$teacher->name?></h1>
            <p><?=$teacher->designation?></p>
        </div>
        <div class="panel-body profile-view-dis">
            <h1><?=$this->lang->line("personal_information")?></h1>
            <div class="row">
                <div class="profile-view-tab">
                    <p><span><?=$this->lang->line("profile_dob")?> </span>: <?=date("d M Y", strtotime($teacher->dob))?></p>
                </div>
                <div class="profile-view-tab">
                    <p><span><?=$this->lang->line("profile_jod")?> </span>: <?=date("d M Y", strtotime($teacher->jod))?></p>
                </div>
                <div class="profile-view-tab">
                    <p><span><?=$this->lang->line("profile_sex")?> </span>: <?=$teacher->sex?></p>
                </div>
                <div class="profile-view-tab">
                    <p><span><?=$this->lang->line("profile_religion")?> </span>: <?=$teacher->religion?></p>
                </div>
                <div class="profile-view-tab">
                    <p><span><?=$this->lang->line("profile_email")?> </span>: <?=$teacher->email?></p>
                </div>
                <div class="profile-view-tab">
                    <p><span><?=$this->lang->line("profile_phone")?> </span>: <?=$teacher->phone?></p>
                </div>
                <div class="profile-view-tab">
                    <p><span><?=$this->lang->line("profile_address")?> </span>: <?=$teacher->address?></p>
                </div>
            </div>
        </div>
    </section>
<?php } elseif($usertype == "Student") { ?>
    <section class="panel">

        <div class="profile-view-head">
            <a href="#">
                <?=img(base_url('uploads/images/'.$student->photo))?>
            </a>

            <h1><?=$student->name?></h1>
            <p><?=$this->lang->line("profile_classes")." ".$class->classes?></p>

        </div>
        <div class="panel-body profile-view-dis">
            <h1><?=$this->lang->line("personal_information")?></h1>
            <div class="row">
                <div class="profile-view-tab">
                    <p><span><?=$this->lang->line("profile_roll")?> </span>: <?=$student->roll?></p>
                </div>
                <div class="profile-view-tab">
                    <p><span><?=$this->lang->line("profile_dob")?> </span>: <?=date("d M Y", strtotime($student->dob))?></p>
                </div>
                <div class="profile-view-tab">
                    <p><span><?=$this->lang->line("profile_sex")?> </span>: <?=$student->sex?></p>
                </div>
                <div class="profile-view-tab">
                    <p><span><?=$this->lang->line("profile_religion")?> </span>: <?=$student->religion?></p>
                </div>
                <div class="profile-view-tab">
                    <p><span><?=$this->lang->line("profile_email")?> </span>: <?=$student->email?></p>
                </div>
                <div class="profile-view-tab">
                    <p><span><?=$this->lang->line("profile_phone")?> </span>: <?=$student->phone?></p>
                </div>
                <div class="profile-view-tab">
                    <p><span><?=$this->lang->line("profile_address")?> </span>: <?=$student->address?></p>
                </div>
                <?php if($usertype == "Admin") { ?>
                <div class="profile-view-tab">
                    <p><span><?=$this->lang->line("profile_username")?> </span>: <?=$student->username?></p>
                </div>
                <?php } ?>
            </div>

            <h1><?=$this->lang->line("parents_information")?></h1>
            <?php   if(count($parent)) { ?>

            <div class="row">
                <div class="profile-view-tab">
                    <p><span><?=$this->lang->line("profile_guargian_name")?> </span>: <?=$parent->name?></p>
                </div>
                <div class="profile-view-tab">
                    <p><span><?=$this->lang->line("profile_father_name")?> </span>: <?=$parent->father_name?></p>
                </div>
                <div class="profile-view-tab">
                    <p><span><?=$this->lang->line("profile_mother_name")?> </span>: <?=$parent->mother_name?></p>
                </div>
                <div class="profile-view-tab">
                    <p><span><?=$this->lang->line("profile_father_profession")?> </span>: <?=$parent->father_profession?></p>
                </div>
                <div class="profile-view-tab">
                    <p><span><?=$this->lang->line("profile_mother_profession")?> </span>: <?=$parent->mother_profession?></p>
                </div>
                <div class="profile-view-tab">
                    <p><span><?=$this->lang->line("profile_email")?> </span>: <?=$parent->email?></p>
                </div>
                <div class="profile-view-tab">
                    <p><span><?=$this->lang->line("profile_phone")?> </span>: <?=$parent->phone?></p>
                </div>
                <div class="profile-view-tab">
                    <p><span><?=$this->lang->line("profile_address")?> </span>: <?=$parent->address?></p>
                </div>
            </div>
            <?php 
                } else { 
                    echo "<div class='col-sm-12'><div class='col-sm-12 alert alert-warning'><span class='fa fa-exclamation-triangle'></span> " .$this->lang->line("parent_error"). "</div></div>";
                }
            ?>

        </div>
    </section>
<?php } elseif($usertype == "Parent") { ?>
    <!-- -->
    <div class="well">
        <div class="row">
            <div class="col-sm-6">
                <button class="btn-cs btn-sm-cs" onclick="javascript:printDiv('printablediv')"><span class="fa fa-print"></span> <?=$this->lang->line('print')?> </button>
                <?php
                 echo btn_add_pdf('profile/print_preview/'.$parentes->parentID."/", $this->lang->line('pdf_preview')) 
                ?>

                <button class="btn-cs btn-sm-cs" data-toggle="modal" data-target="#idCard"><span class="fa fa-floppy-o"></span> <?=$this->lang->line('idcard')?> </button>

                <?php echo btn_sm_edit('profile/edit/'.$parentes->parentID."/", $this->lang->line('edit')) 
                ?>
                <button class="btn-cs btn-sm-cs" data-toggle="modal" data-target="#mail"><span class="fa fa-envelope-o"></span> <?=$this->lang->line('mail')?></button>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb">
                    <li><a href="<?=base_url("dashboard/index")?>"><i class="fa fa-laptop"></i> <?=$this->lang->line('menu_dashboard')?></a></li>
                    <li><a href="<?=base_url("profile/index")?>"><?=$this->lang->line('menu_profile')?></a></li>
                    <li class="active"><?=$this->lang->line('view')?></li>
                </ol>
            </div>
        </div>
    </div>
    <!-- -->
    <section class="panel">
        <div class="profile-view-head">
            <a href="#">
                <?=img(base_url('uploads/images/'.$parentes->photo))?>
            </a>

            <h1><?=$parentes->name?></h1>
            <p><?=$parentes->email?></p>
        </div>
        <div class="panel-body profile-view-dis">
            <h1><?=$this->lang->line("personal_information")?></h1>
            <div class="row">
                <div class="profile-view-tab">
                    <p><span><?=$this->lang->line("profile_father_name")?> </span>: <?=$parentes->father_name?></p>
                </div>
                <div class="profile-view-tab">
                    <p><span><?=$this->lang->line("profile_father_profession")?> </span>: <?=$parentes->father_profession?></p>
                </div>
                <div class="profile-view-tab">
                    <p><span><?=$this->lang->line("profile_mother_name")?> </span>: <?=$parentes->mother_name?></p>
                </div>
                <div class="profile-view-tab">
                    <p><span><?=$this->lang->line("profile_mother_profession")?> </span>: <?=$parentes->mother_profession?></p>
                </div>
                <div class="profile-view-tab">
                    <p><span><?=$this->lang->line("profile_phone")?> </span>: <?=$parentes->phone?></p>
                </div>
                <div class="profile-view-tab">
                    <p><span><?=$this->lang->line("profile_address")?> </span>: <?=$parentes->address?></p>
                </div>
            </div>
        </div>
    </section>
<?php } elseif($usertype == "Super Admin") { ?>
    <section class="panel">
        <div class="profile-view-head">
            <a href="#">
                <?=img(base_url('uploads/images/'.$systemadmin->photo))?>
            </a>

            <h1><?=$systemadmin->name?></h1>
            <p><?=$systemadmin->email?></p>
        </div>
        <div class="panel-body profile-view-dis">
            <h1><?=$this->lang->line("personal_information")?></h1>
            <div class="row">
                <div class="profile-view-tab">
                    <p><span><?=$this->lang->line("profile_dob")?> </span>: <?=date("d M Y", strtotime($systemadmin->dob))?></p>
                </div>
                <div class="profile-view-tab">
                    <p><span><?=$this->lang->line("profile_jod")?> </span>: <?=date("d M Y", strtotime($systemadmin->jod))?></p>
                </div>
                <div class="profile-view-tab">
                    <p><span><?=$this->lang->line("profile_sex")?> </span>: <?=$systemadmin->sex?></p>
                </div>
                <div class="profile-view-tab">
                    <p><span><?=$this->lang->line("profile_religion")?> </span>: <?=$systemadmin->religion?></p>
                </div>
                <div class="profile-view-tab">
                    <p><span><?=$this->lang->line("profile_email")?> </span>: <?=$systemadmin->email?></p>
                </div>
                <div class="profile-view-tab">
                    <p><span><?=$this->lang->line("profile_phone")?> </span>: <?=$systemadmin->phone?></p>
                </div>
                <div class="profile-view-tab">
                    <p><span><?=$this->lang->line("profile_address")?> </span>: <?=$systemadmin->address?></p>
                </div>
            </div>
        </div>
    </section>
<?php } ?>

 <?php if($usertype == "Admin" || $usertype == "Super Admin" || $usertype == "Parent") { ?>

    <!-- Modal content start here -->
    <div class="modal fade" id="idCard">
      <div class="modal-dialog">
        <div class="modal-content">
            <div id="idCardPrint">
              <div class="modal-header">
                <?=$this->lang->line('idcard')?>
              </div>
              <div class="modal-body" > 
                    <table>
                        <tr>
                            <td>
                                <h4 style="margin:0;">
                                <?php 
                                    if($siteinfos->photo) {
                                        $array = array(
                                            "src" => base_url('uploads/images/'.$this->session->userdata('schoollogo')),
                                            'width' => '25px',
                                            'height' => '25px',
                                            "style" => "margin-bottom:10px;"
                                        );
                                        echo img($array);
                                    }
                                ?>
                                </h4>
                            </td>
                            <td style="padding-left:5px;">
                                <h4><?=$this->session->userdata('school');?></h4>
                            </td>
                        </tr>
                    </table>

                <table class="idcard-Table" style="background-color:#524259">
                    <tr>
                        <td>
                            <h4>
                                <?php 
                                    $image_properties = array(
                                        'src' => base_url('uploads/images/'.$user->photo),
                                        'style' => 'border: 8px solid #886E94',
                                    );
                                    echo img($image_properties);
                                ?>
                            </h4> 
                        </td>
                        <td class="row-style">
                            <h3><?php  echo $user->name; ?></h3>
                            <h5><?php  echo $this->lang->line("user_designation")." : ".$user->usertype; ?>
                            <h5><?php  echo $this->lang->line("user_email")." : ".$user->email; ?>
                            </h5>
                            <h5>
                                <?php  echo $this->lang->line("user_phone")." : ".$user->phone; ?>
                            </h5>
                        </td>
                    </tr>
                </table>    
              </div>
            </div>
          <div class="modal-footer">
            <button type="button" style="margin-bottom:0px;" class="btn btn-default" data-dismiss="modal" onclick="javascript:closeWindow()"><?=$this->lang->line('close')?></button>
            <button type="button" class="btn btn-success" onclick="javascript:printDiv('idCardPrint')"><?=$this->lang->line('print')?></button>
          </div>
        </div>
      </div>
    </div>
    <!-- Modal content End here -->

<!-- email modal starts here -->
<form class="form-horizontal" role="form" action="<?=base_url('teacher/send_mail');?>" method="post">
    <div class="modal fade" id="mail">
      <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title"><?=$this->lang->line('mail')?></h4>
            </div>
            <div class="modal-body">
            
                <?php 
                    if(form_error('to')) 
                        echo "<div class='form-group has-error' >";
                    else     
                        echo "<div class='form-group' >";
                ?>
                    <label for="to" class="col-sm-2 control-label">
                        <?=$this->lang->line("to")?>
                    </label>
                    <div class="col-sm-6">
                        <input type="email" class="form-control" id="to" name="to" value="<?=set_value('to')?>" >
                    </div>
                    <span class="col-sm-4 control-label" id="to_error">
                    </span>
                </div>

                <?php 
                    if(form_error('subject')) 
                        echo "<div class='form-group has-error' >";
                    else     
                        echo "<div class='form-group' >";
                ?>
                    <label for="subject" class="col-sm-2 control-label">
                        <?=$this->lang->line("subject")?>
                    </label>
                    <div class="col-sm-6">
                        <input type="text" class="form-control" id="subject" name="subject" value="<?=set_value('subject')?>" >
                    </div>
                    <span class="col-sm-4 control-label" id="subject_error">
                    </span>

                </div>

                <?php 
                    if(form_error('message')) 
                        echo "<div class='form-group has-error' >";
                    else     
                        echo "<div class='form-group' >";
                ?>
                    <label for="message" class="col-sm-2 control-label">
                        <?=$this->lang->line("message")?>
                    </label>
                    <div class="col-sm-6">
                        <textarea class="form-control" id="message" style="resize: vertical;" name="message" value="<?=set_value('message')?>" ></textarea>
                    </div>
                </div>

            
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" style="margin-bottom:0px;" data-dismiss="modal"><?=$this->lang->line('close')?></button>
                <input type="button" id="send_pdf" class="btn btn-success" value="<?=$this->lang->line("send")?>" />
            </div>
        </div>
      </div>
    </div>
</form>
<!-- email end here -->

    
    <?php } ?>

<script language="javascript" type="text/javascript">
        function printDiv(divID) {
            //Get the HTML of div
            var divElements = document.getElementById(divID).innerHTML;
            //Get the HTML of whole page
            var oldPage = document.body.innerHTML;

            //Reset the page's HTML with div's HTML only
            document.body.innerHTML = 
              "<html><head><title></title></head><body>" + 
              divElements + "</body>";

            //Print Page
            window.print();

            //Restore orignal HTML
            document.body.innerHTML = oldPage;
        }
        function closeWindow() {
            location.reload(); 
        }
 
        function check_email(email) {
            var status = false;     
            var emailRegEx = /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i;
            if (email.search(emailRegEx) == -1) {
                $("#to_error").html('');
                $("#to_error").html("<?=$this->lang->line('mail_valid')?>").css("text-align", "left").css("color", 'red');
            } else {
                status = true;
            }
            return status;
        }

        $("#send_pdf").click(function(){
            var to = $('#to').val();
            var subject = $('#subject').val();
            var message = $('#message').val();
            var id = "<?=$user->userID?>";
            var error = 0;

            if(to == "" || to == null) {
                error++;
                $("#to_error").html("");
                $("#to_error").html("<?=$this->lang->line('mail_to')?>").css("text-align", "left").css("color", 'red');
            } else {
                if(check_email(to) == false) {
                    error++
                }
            } 

            if(subject == "" || subject == null) {
                error++;
                $("#subject_error").html("");
                $("#subject_error").html("<?=$this->lang->line('mail_subject')?>").css("text-align", "left").css("color", 'red');
            } else {
                $("#subject_error").html("");
            }

            if(error == 0) {
                $.ajax({
                    type: 'POST',
                    url: "<?=base_url('user/send_mail')?>",
                    data: 'to='+ to + '&subject=' + subject + "&id=" + id+ "&message=" + message,
                    dataType: "html",
                    success: function(data) {
                        location.reload();
                    }
                });
            }
        });
    </script>
