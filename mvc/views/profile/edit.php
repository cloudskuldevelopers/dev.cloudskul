
<div class="box">
    <div class="box-header">
        <h3 class="box-title"><i class="fa fa-users"></i> <?=$this->lang->line('panel_title')?></h3>

       
        <ol class="breadcrumb">
            <li><a href="<?=base_url("dashboard/index")?>"><i class="fa fa-laptop"></i> <?=$this->lang->line('menu_dashboard')?></a></li>
            <li><a href="<?=base_url("profile/index")?>"><?=$this->lang->line('menu_profile')?></a></li>
            <li class="active"><?=$this->lang->line('menu_edit')?> <?=$this->lang->line('menu_user')?></li>
        </ol>
    </div><!-- /.box-header -->
    <!-- form start -->
    <div class="box-body">
        <div class="row">
            <div class="col-sm-8">
                <form class="form-horizontal" role="form" method="post" enctype="multipart/form-data">
                   
                    <?php 
                        if(form_error('name')) 
                            echo "<div class='form-group has-error' >";
                        else     
                            echo "<div class='form-group' >";
                    ?>
                        <label for="user_name" class="col-sm-2 control-label">
                            <?=$this->lang->line("user_name")?>
                        </label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" id="name" name="name" value="<?=set_value('name', $user->name)?>" >

                        </div>
                        <span class="col-sm-4 control-label">
                            <?php echo form_error('name'); ?>
                        </span>
                    </div>

                    <?php 

                        $usertype = $this->session->userdata("usertype");
                        if($usertype != 'Parent'){

                    ?>

                    <?php 
                        if(form_error('dob')) 
                            echo "<div class='form-group has-error' >";
                        else     
                            echo "<div class='form-group' >";
                    ?>
                        <label for="dob" class="col-sm-2 control-label">
                            <?=$this->lang->line("user_dob")?>
                        </label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" id="dob" name="dob" value="<?=set_value('dob', date("d-m-Y", strtotime($user->dob)))?>" >
                        </div>
                        <span class="col-sm-4 control-label">
                            <?php echo form_error('dob'); ?>
                        </span>
                    </div>

                    <?php 
                        if(form_error('sex')) 
                            echo "<div class='form-group has-error' >";
                        else     
                            echo "<div class='form-group' >";
                    ?>
                        <label for="sex" class="col-sm-2 control-label">
                            <?=$this->lang->line("user_sex")?>
                        </label>
                        <div class="col-sm-6">
                            <?php 
                                echo form_dropdown("sex", array($this->lang->line("user_sex_male") => $this->lang->line("user_sex_male"), $this->lang->line("user_sex_female") => $this->lang->line("user_sex_female")), set_value("sex", $user->sex), "id='sex' class='form-control'"); 
                            ?>
                        </div>
                        <span class="col-sm-4 control-label">
                            <?php echo form_error('sex'); ?>
                        </span>
                    </div>

                    <?php 
                        if(form_error('religion')) 
                            echo "<div class='form-group has-error' >";
                        else     
                            echo "<div class='form-group' >";
                    ?>
                        <label for="religion" class="col-sm-2 control-label">
                            <?=$this->lang->line("user_religion")?>
                        </label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" id="religion" name="religion" value="<?=set_value('religion', $user->religion)?>" >
                        </div>
                        <span class="col-sm-4 control-label">
                            <?php echo form_error('religion'); ?>
                        </span>
                    </div>
                    <?php } ?>

                    <?php 
                        if(form_error('email')) 
                            echo "<div class='form-group has-error' >";
                        else     
                            echo "<div class='form-group' >";
                    ?>
                        <label for="email" class="col-sm-2 control-label">
                            <?=$this->lang->line("user_email")?>
                        </label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" id="email" name="email" value="<?=set_value('email', $user->email)?>" >
                        </div>
                        <span class="col-sm-4 control-label">
                            <?php echo form_error('email'); ?>
                        </span>
                    </div>

                    <?php 
                        if(form_error('phone')) 
                            echo "<div class='form-group has-error' >";
                        else     
                            echo "<div class='form-group' >";
                    ?>
                        <label for="phone" class="col-sm-2 control-label">
                            <?=$this->lang->line("user_phone")?>
                        </label>
                        <div class="col-sm-6">
                            <div class="input-group">
                              <input type="text" class="form-control" id="phone" name="phone" placeholder="080xxxxxxxx" value="<?=set_value('phone', $user->phone)?>" >
                              <span class="input-group-btn">
                                <button id="verify" onclick="return false;" class="btn btn-success">
                                    verify
                                </button>
                                <button id="verify1" style="display:none;" onclick="return false;" class="btn btn-success">
                                Send again
                                </button>
                              <button id="verify3" style="display:none;" onclick="return false;" class="btn btn-success">
                                verified
                                </button>
                              </span>
                            </div><!-- /.input group -->
                        </div>
                        <span class="col-sm-4 control-label">
                            <?php echo form_error('phone'); ?>
                        </span>
                    </div>

                    <div id="verify_code1" style="display:none; font-weight: bold;" class="form-group">
                        <p>Sending you a text message with your verification code.</p>
                        <p>Once received, enter it here:</p>
                    </div>

                    <div id="verify_code" class='form-group' style="display:none;">
                        <label for="phone" class="col-sm-2 control-label">
                            Verification Code
                        </label>
                        <div class="col-sm-6">
                            <div class="input-group">
                                <input type="text" class="form-control" id="verification_code" name="verification_code" placeholder="Check your phone">
                                <span class="input-group-btn">
                                    <button id="verify2" onclick="return false;" class="btn btn-success">
                                        Submit
                                    </button>
                              </span>
                            </div><!-- /.input group -->
                        </div>
                            <span class="col-sm-4 control-label">
                                <?php //echo form_error('phone'); ?>
                            </span>
                        </div>

                    <?php 
                        if(form_error('address')) 
                            echo "<div class='form-group has-error' >";
                        else     
                            echo "<div class='form-group' >";
                    ?>
                        <label for="address" class="col-sm-2 control-label">
                            <?=$this->lang->line("user_address")?>
                        </label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" id="address" name="address" value="<?=set_value('address', $user->address)?>" >
                        </div>
                        <span class="col-sm-4 control-label">
                            <?php echo form_error('address'); ?>
                        </span>
                    </div>

                   
                    <?php 
                        if(isset($image)) 
                            echo "<div class='form-group has-error' >";
                        else     
                            echo "<div class='form-group' >";
                    ?>
                        <label for="photo" class="col-sm-2 control-label col-xs-8 col-md-2">
                            <?=$this->lang->line("user_photo")?>
                        </label>
                        <div class="col-sm-4 col-xs-6 col-md-4">
                            <input class="form-control"  id="uploadFile" placeholder="Choose File" disabled />  
                        </div>

                        <div class="col-sm-2 col-xs-6 col-md-2">
                            <div class="fileUpload btn btn-success form-control">
                                <span class="fa fa-repeat"></span>
                                <span><?=$this->lang->line("upload")?></span>
                                <input id="uploadBtn" type="file" class="upload" name="image" />
                            </div>
                        </div>
                         <span class="col-sm-4 control-label col-xs-6 col-md-4">
                           
                            <?php if(isset($image)) echo $image; ?>
                        </span>
                    </div>

                    

                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-8">
                            <input type="submit" class="btn btn-success" value="<?=$this->lang->line("update_user")?>" >
                        </div>
                    </div>

                </form>
            </div>    
        </div>
    </div>
</div>

<script type="text/javascript">
document.getElementById("uploadBtn").onchange = function() {
    document.getElementById("uploadFile").value = this.value;
};
$('#dob').datepicker({ startView: 2 });
$('#jod').datepicker();
</script>

<script type="text/javascript">
        $(document).ready(function(){
            $("#verify").click(function(e) {
                e.preventDefault();
                $("#verify").fadeOut("fast");
                $("#verify1").fadeIn("slow");
                $("#verify_code1").fadeIn();
                $("#verify_code").fadeIn();
                initiateSms();
            });

            $("#verify1").click(function(e) {
                e.preventDefault();
                /*$("#verify").fadeIn("fast");
                $("#verify1").fadeOut("slow");
                $("#verify_code1").fadeOut("fast");
                $("#verify_code").fadeOut("fast");*/
                initiateSms();
            });

            $("#verify2").click(function(e) {
                e.preventDefault();
                verifySms();
            });
        });

        function verifySms(){
            var code = $("#verification_code").val();
             $.ajax({
                type: 'POST',
                url: "<?=base_url('profile/verify_phone_code')?>",
                data: "",
                dataType: "html",
                success: function(data) {
                    if(data != "") {
                        //alert(data);
                        if(data == code){
                            //code = data;
                            $("#verify_code1").fadeOut("fast");
                            $("#verify_code").fadeOut("fast");
                            $("#verify1").fadeOut("fast");
                            $("#verify3").fadeIn("fast");
                            alert("Correct code entered.");

                        }else{
                            alert("incorrect code entered.");
                        }
                        
                    } else {
                        alert("no data returned, service is down");
                    }
                    
                   
                }
            });

        }
        
        function initiateSms() {
            /*$.post("sms.php", { phone : $("#phone").val() }, 
                    showVerifyForm, 
                    "json");*/
            var phone = $("#phone").val();
            //alert(bank_name);
            $.ajax({
                type: 'POST',
                url: "<?=base_url('profile/verify_phone')?>",
                data: "phone=" + phone,
                dataType: "html",
                success: function(data) {
                    /*if(data != "") {
                        var width = $("#bank_name").width();
                        $(".book").css('width', width+25 + "px").show();
                        $(".result").html(data);

                        $('.result li').click(function(){
                            var result_value = $(this).text();
                            var result_code = $(this).attr('id');
                            $('#bank_name').val(result_value);
                            $('#bank_code').val(result_code);
                            $('.result').html(' ');
                            $('.book').hide();
                        });
                    } else {
                        $(".book").hide();
                    }*/
                    alert(data);
                   
                }
            });
        }
        
        function showVerifyForm() {
            $("#phone_number2").val($("#phone_number").val());
            $("#verify").fadeOut("fast");
            $("#verify_code").fadeIn();
        }
    </script>
