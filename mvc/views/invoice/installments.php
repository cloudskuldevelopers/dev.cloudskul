<?php
	
	
	
	//echo $combinedSelected;

	//get the combined selected installments
	//get the number of installments
	//pass the combined installments and the num of installments to the control function
	//the control function compares the approved payments with the num of installments
	//and returns an array telling which installments has been paid
	//then for each istallment, check is that installment status is set in the return array,
	//if it is, then make it paid
?>


<div class="box">
    <div class="box-header">
        <h3 class="box-title"><i class="fa icon-invoice"></i> <?=$this->lang->line('panel_title')?>  | <span style="color:#9d0000;"><?php echo $invoiceData->invoice_ref; ?></span></h3>

       
        <ol class="breadcrumb">
            <li><a href="<?=base_url("dashboard/index")?>"><i class="fa fa-laptop"></i> <?=$this->lang->line('menu_dashboard')?></a></li>
            <li class="active"><?=$this->lang->line('installment')?></li>
        </ol>
    </div><!-- /.box-header -->
    <!-- form start -->
    <div class="box-body">
        <div class="row">
            <div class="col-sm-12">
                <div id="hide-table">
					<form method ="post" action="<?php echo base_url() . 'invoice/installments/process/' . $installmentRef . '/' . $studentID; ?>">
						<table id="example1" class="table table-striped table-bordered table-hover dataTable no-footer">
							<thead>
								<tr>
									<th><?=$this->lang->line('slno')?></th>
									<th><?=$this->lang->line('invoice_feetype')?></th>
									<th><?=$this->lang->line('invoice_student')?></th>
									<th><?=$this->lang->line('invoice_installment_no')?></th>
									<th><?=$this->lang->line('invoice_1st')?></th>
									<th><?=$this->lang->line('invoice_2nd')?></th>
									<th><?=$this->lang->line('invoice_3rd')?></th>
									<th><?=$this->lang->line('installment_total')?></th>
									<th><?=$this->lang->line('action')?></th>
								</tr>
							</thead>
							<tbody>
									<tr>
										<td data-title="<?=$this->lang->line('slno')?>">
											<?php echo 1; ?>
										</td>
										<td data-title="<?=$this->lang->line('invoice_feetype')?>">
											<?php echo $invoiceData->feetype; ?>
										</td>
		
										<!--td data-title="<?=$this->lang->line('invoice_status')?>">
											<?php 
	
												$status = $invoice->status;
												$setstatus = '';
												if($status == 0) {
													$status = $this->lang->line('invoice_notpaid');
												} elseif($status == 1) {
													$status = $this->lang->line('invoice_partially_paid');
												} elseif($status == 2) {
													$status = $this->lang->line('invoice_fully_paid');
												} elseif($status == 3) {
													$status = "RRR Generated";
												}
	
												echo "<button class='btn btn-success btn-xs'>".$status."</button>";
	
											?>
										</td-->
										<td data-title="<?=$this->lang->line('invoice_student')?>">
											<?php echo $invoiceData->student; ?>
										</td>
										<td data-title="<?=$this->lang->line('invoice_installment_no')?>">
											<?php echo $invoiceData->installment_plan . ' Installment(s)'; ?>
										</td>
										<td data-title="<?=$this->lang->line('invoice_1st')?>">
											<?php 
												
												if(isset($returnApproved['first_installment']) && $returnApproved['first_installment'] == 'paid'){
													$status = $this->lang->line('paid');
													echo "<p class='btn btn-success btn-xs'>".$status."</p>";
												}else{ ?>
													<label style="cursor:pointer;">
													<input type="checkbox" id="firstInstallment" name="installment1"  value="<?php echo ($installmentData->no_of_installments == 1) ? $installmentData->total_amount : $installmentData->first_installment_amount; ?>">
													&nbsp;
													<?php echo ($installmentData->no_of_installments == 1) ? number_format(floatval($installmentData->total_amount),2) : number_format(floatval($installmentData->first_installment_amount),2); ?> 
													</label>
													&nbsp; 
													<?php 
														
														$status = $this->lang->line('invoice_notpaid');
														echo "<p class='btn btn-success btn-xs'>".$status."</p>";
														/*
														$status = $invoiceData->first_installment_status;
														$setstatus = '';
														if($status == 0) {
															$status = $this->lang->line('invoice_notpaid');
														} elseif($status == 1) {
															$status = $this->lang->line('invoice_partially_paid');
														} elseif($status == 2) {
															$status = $this->lang->line('invoice_fully_paid');
														} elseif($status == 3) {
															$status = "RRR Generated";
														}
														*/
			
												}
												?>
													
										</td>
										<td data-title="<?=$this->lang->line('invoice_2nd')?>">
											<?php if($installmentData->no_of_installments == 2 || $installmentData->no_of_installments == 3){ ?>
												<?php
												if(isset($returnApproved['second_installment']) && $returnApproved['second_installment'] == 'paid'){
													$status = $this->lang->line('paid');
													echo "<p class='btn btn-success btn-xs'>".$status."</p>";
												}else{ ?>
													
													<label style="cursor:pointer;">
													<input type="checkbox" id="secondInstallment" name="installment2" value="<?php echo $installmentData->second_installment_amount; ?>"> &nbsp;
													<?php echo number_format(floatval($installmentData->second_installment_amount),2); ?>
													</label> 
													&nbsp; 
													<?php
														$status = $this->lang->line('invoice_notpaid');
														echo "<p class='btn btn-success btn-xs'>".$status."</p>";
												}
											} ?>
										</td>
										<td data-title="<?=$this->lang->line('invoice_3rd')?>">
											<?php if($installmentData->no_of_installments == 3){ ?>
												<?php
												if(isset($returnApproved['third_installment']) && $returnApproved['third_installment'] == 'paid'){
													$status = $this->lang->line('paid');
													echo "<p class='btn btn-success btn-xs'>".$status."</p>";
												}else{ ?>
												
													<label style="cursor:pointer;">
													<input type="checkbox" id="thirdInstallment" name="installment3" value="<?php echo $installmentData->third_installment_amount; ?>">
													&nbsp;
													<?php echo number_format(floatval($installmentData->third_installment_amount),2); ?> 
													</label>
													&nbsp;
													<?php 
														$status = $this->lang->line('invoice_notpaid');
														echo "<p class='btn btn-success btn-xs'>".$status."</p>";
												}
												?>
											<?php } ?>
										</td>
										<td id="totalInstallment" data-title="<?=$this->lang->line('installment_total')?>">
											0.00
										</td>
										<td data-title="<?=$this->lang->line('action')?>">
											<button type="submit" class="btn btn-success btn-xs mrg" data-placement="top" data-toggle="tooltip" data-original-title="Pay Selected">Pay Selected</button>
										</td>
									</tr>
							</tbody>
						</table>
					</form>
                </div>

            </div> <!-- col-sm-12 -->
            
        </div><!-- row -->
    </div><!-- Body -->
</div><!-- /.box -->
<script type="text/javascript">
    $('#studentID').change(function() {
        var studentID = $(this).val();
        if(studentID == 0) {
            $('#hide-table').hide();
        } else {
            $.ajax({
                type: 'POST',
                url: "<?=base_url('invoice/student_list')?>",
                data: "id=" + studentID,
                dataType: "html",
                success: function(data) {
                    window.location.href = data;
                }
            });
        }
    });
	
		//code for calculating total installmental payment
		function calculateTotalInstallmentalPayment(){
			var firstInstallment  = $("#firstInstallment").val();
			var secondInstallment  = $("#secondInstallment").val();
			var thirdInstallment  = $("#thirdInstallment").val();
			
			if($("#firstInstallment").prop("checked") != true){
				firstInstallment = 0;
			}
			if($("#secondInstallment").prop("checked") != true){
				secondInstallment = 0;
			}
			if($("#thirdInstallment").prop("checked") != true){
				thirdInstallment = 0;
			}
			
			var totalInstallment = Number(firstInstallment) + Number(secondInstallment) + Number(thirdInstallment);
			
			$('#totalInstallment').html(totalInstallment.toLocaleString() + '.00');
		}
		
		$('#firstInstallment').click(function(){
			//if($(this).prop("checked") == true){
				calculateTotalInstallmentalPayment();
			//}
		});
		
		$('#secondInstallment').click(function(){
			//if($(this).prop("checked") == true){
				calculateTotalInstallmentalPayment();
			//}
		});
		
		$('#thirdInstallment').click(function(){
			//if($(this).prop("checked") == true){
				calculateTotalInstallmentalPayment();
			//}
		});
	
	
</script>