<!-- TOP BAR -->
<div class="row">
	<div class="col-md-2 hidden-xs">
		<img src="<?php echo base_url('uploads/images/logo.png'); ?>" style="width: auto; height: 40px; margin-top: -8px;" class="img-thumbnail img-responsive" />
	</div>
	<div class="col-md-6 col-xs-9 text-center">
		<h1 class="page-header"><?php echo $this->session->userdata('school');  ?></h1>
	</div>
	<div class="col-md-4 col-xs-3">
		<!-- begin breadcrumb -->
		<ol class="breadcrumb pull-right">
			<li ><a href="<?php echo base_url('dashboard/index');?>"><i class="fa fa-laptop"></i> Dashboard</a></li>
			<li><a href="<?php echo base_url('balance/index');?>">Balance</a></li>
		</ol>
		<!-- end breadcrumb -->
	</div>
</div>
<!-- END TOP BAR -->
<div id="page-container">
<!-- begin panel -->
<div class="panel panel-inverse" data-sortable-id="ui-widget-3">
	<div class="panel-heading">
		<h4 class="panel-title"><i class="fa fa-pencil"></i> <?=$this->lang->line('panel_title')?></h4>
	</div>
	<div class="panel-body">
		<div class="col-md-6">
			<form style="" class="form-horizontal" role="form" method="post">
				<div class="input-group" >
					
					<span class="input-group-addon offset_addon"><i class="fa fa-sitemap"></i></span>
					<?php
						$array = array("0" => $this->lang->line("balance_select_classes"));
						foreach ($classes as $classa) {
							$array[$classa->classesID] = $classa->classes;
						}
						echo form_dropdown("classesID", $array, set_value("classesID"), "id='classesID' class='form-control offset_height'");
					?>
				</div>
			</form>
		</div>
	</div>
</div>
</div>	


<div class="box box-danger" hidden>
    <div class="box-header with-border">
        <h3 class="box-title"><i class="fa icon-payment"></i> <?=$this->lang->line('panel_title')?></h3>

       
        <ol class="breadcrumb">
            <li><a href="<?=base_url("dashboard/index")?>"><i class="fa fa-laptop"></i> <?=$this->lang->line('menu_dashboard')?></a></li>
            <li class="active"><?=$this->lang->line('menu_balance')?></li>
        </ol>
    </div><!-- /.box-header -->
    <!-- form start -->
    <div class="box-body">
        <div class="row">
            <div class="col-sm-12">

                <?php
                    $usertype = $this->session->userdata("usertype");
                    if($usertype == "Admin" || $usertype == "Super Admin" || $usertype == "Accountant") {
                ?>
				
				<div class="col-md-6 col-sm-offset-3 add_padding">
					<blockquote style="border-right: 5px solid #eee;">
						<form style="" class="form-horizontal" role="form" method="post">
							<div class="input-group">
								<label for="classesID" class="col-sm-2 control-label" style="padding-left: 0px;">
									<?=$this->lang->line("balance_classesID")?>
								</label>
								<span class="input-group-addon offset_addon"><i class="fa fa-sitemap"></i></span>
								<?php
									$array = array("0" => $this->lang->line("balance_select_classes"));
									foreach ($classes as $classa) {
										$array[$classa->classesID] = $classa->classes;
									}
									echo form_dropdown("classesID", $array, set_value("classesID"), "id='classesID' class='form-control offset_height'");
								?>
							</div>
						</form>
					</blockquote>
				</div>

                <?php } ?>

            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $('#classesID').change(function() {
        var classesID = $(this).val();
        if(classesID == 0) {
            $('#hide-table').hide();
            $('.nav-tabs-custom').hide();
        } else {
            $.ajax({
                type: 'POST',
                url: "<?=base_url('balance/balance_list')?>",
                data: "id=" + classesID,
                dataType: "html",
                success: function(data) {
                    window.location.href = data;
                }
            });
        }
    });
</script>