<!-- TOP BAR -->
<div class="row">
	<div class="col-md-2 hidden-xs">
		<img src="<?php echo base_url('uploads/images/logo.png'); ?>" style="width: auto; height: 40px; margin-top: -8px;" class="img-thumbnail img-responsive" />
	</div>
	<div class="col-md-6 col-xs-9 text-center">
		<h1 class="page-header"><?php echo $this->session->userdata('school');  ?></h1>
	</div>
	<div class="col-md-4 col-xs-3">
		<!-- begin breadcrumb -->
		<ol class="breadcrumb pull-right">
			<li><a href="<?=base_url("dashboard/index")?>"><i class="fa fa-laptop"></i> <?=$this->lang->line('menu_dashboard')?></a></li>
            <li class="active"><?=$this->lang->line('menu_balance')?></li>
		</ol>
		<!-- end breadcrumb -->
	</div>
</div>
<!-- END TOP BAR -->

<div class="panel panel-inverse" data-sortable-id="ui-widget-3">
	<div class="panel-heading">
		<h4 class="panel-title"><i class="fa fa-pencil"></i> <?=$this->lang->line('panel_title')?></h4>
	</div>
	<div class="panel-body">
		<?php
			$array = array("0" => $this->lang->line("balance_select_classes"));
			foreach ($classes as $classa) {
				$array[$classa->classesID] = $classa->classes;
			}
			echo form_dropdown("classesID", $array, set_value("classesID"), "id='classesID' class='form-control m20 offset_height'");
		?>
		<?php if(count($students) > 0 ) { ?>
			
			<div class="panel panel-default panel-with-tabs" data-sortable-id="ui-unlimited-tabs-1" style="border:1px solid #C1CCD1;">
				<div class="panel-heading p-0">
					<!-- begin nav-tabs -->
					<div class="tab-overflow">
						<ul class="nav nav-tabs nav-tabs-default">
							<li class="active"><a data-toggle="tab" href="#all" aria-expanded="true"><?=$this->lang->line("balance_all_students")?></a></li>
							<?php foreach ($sections as $key => $section) {
								echo '<li class=""><a data-toggle="tab" href="#'.$section->schoolID.$section->sectionID .'" aria-expanded="false">'. $this->lang->line("student_section")." ".$section->section. " ( ". $section->category." )".'</a></li>';
							} ?>
						</ul>
					</div>
				</div>
				<div class="tab-content">
					<div class="tab-pane fade active in" id="all">
						<table id="data-table" class="table table-striped table-bordered nowrap" width="100%">
							<thead>
								<tr>
									<th class="col-sm-2"><?=$this->lang->line('slno')?></th>
									<th class="col-sm-2"><?=$this->lang->line('balance_photo')?></th>
									<th class="col-sm-2"><?=$this->lang->line('balance_name')?></th>
									<th class="col-sm-2"><?=$this->lang->line('balance_roll')?></th>
									<th class="col-sm-2"><?=$this->lang->line('balance_phone')?></th>
									<th class="col-sm-2"><?=$this->lang->line('balance_totalbalance')?></th>
								</tr>
							</thead>
							<tbody>
								<?php if(count($students)) {$i = 1; foreach($students as $student) { ?>
									<tr>
										<td data-title="<?=$this->lang->line('slno')?>">
											<?php echo $i; ?>
										</td>

										<td data-title="<?=$this->lang->line('balance_photo')?>">
											<?php $array = array(
													"src" => base_url('uploads/images/'.$student->photo),
													'width' => '35px',
													'height' => '35px',
													'class' => 'img-rounded'

												);
												echo img($array); 
											?>
										</td>
										<td data-title="<?=$this->lang->line('balance_name')?>">
											<?php echo $student->name; ?>
										</td>
										<td data-title="<?=$this->lang->line('balance_roll')?>">
											<?php echo $student->roll; ?>
										</td>
										<td data-title="<?=$this->lang->line('balance_phone')?>">
											<?php echo $student->phone; ?>
										</td>
										<td data-title="<?=$this->lang->line('balance_totalbalance')?>">
											<?php 
												$totalsbalance = ($student->totalamount - $student->paidamount); 
												
												if($student->totalamount > $student->paidamount) {
													echo "- ". number_format(abs($totalsbalance), 2, '.', ',');
												} else {
													echo "+ ". number_format(abs($totalsbalance), 2, '.', ',');
												}
											?>
										</td>
								   </tr>
								<?php $i++; }} ?>
							</tbody>
						</table>
					</div>
					
					<?php foreach ($sections as $key => $section) { ?>
                        <div id="<?=$section->schoolID.$section->sectionID?>" class="tab-pane fade in">
							<table id="data-table" class="table table-striped table-bordered nowrap" width="100%">
								<thead>
									<tr>
										<th class="col-sm-2"><?=$this->lang->line('slno')?></th>
										<th class="col-sm-2"><?=$this->lang->line('balance_photo')?></th>
										<th class="col-sm-2"><?=$this->lang->line('balance_name')?></th>
										<th class="col-sm-2"><?=$this->lang->line('balance_roll')?></th>
										<th class="col-sm-2"><?=$this->lang->line('balance_phone')?></th>
										<th class="col-sm-2"><?=$this->lang->line('balance_totalbalance')?></th>
									</tr>
								</thead>
								<tbody>
									<?php if(count($allsection[$section->sectionID])) { $i = 1; foreach($allsection[$section->sectionID] as $student) { if($section->sectionID === $student->sectionID) { ?>
										<tr>
											<td data-title="<?=$this->lang->line('slno')?>">
												<?php echo $i; ?>
											</td>

											<td data-title="<?=$this->lang->line('balance_photo')?>">
												<?php $array = array(
														"src" => base_url('uploads/images/'.$student->photo),
														'width' => '35px',
														'height' => '35px',
														'class' => 'img-rounded'

													);
													echo img($array); 
												?>
											</td>
											<td data-title="<?=$this->lang->line('balance_name')?>">
												<?php echo $student->name; ?>
											</td>
											<td data-title="<?=$this->lang->line('balance_roll')?>">
												<?php echo $student->roll; ?>
											</td>
											<td data-title="<?=$this->lang->line('balance_phone')?>">
												<?php echo $student->phone; ?>
											</td>
											<td data-title="<?=$this->lang->line('balance_totalbalance')?>">
												<?php 
													$totalsbalance = ($student->totalamount - $student->paidamount); 
													
													if($student->totalamount > $student->paidamount) {
														echo "- ". number_format(abs($totalsbalance), 2, '.', ',');
													} else {
														echo "+ ". number_format(abs($totalsbalance), 2, '.', ',');
													}
												?>
											</td>
									   </tr>
									<?php $i++; }}} ?>
								</tbody>
							</table>
						</div>
					<?php } ?>
				</div>
			</div>
		<?php } ?>
	</div>
</div>
	



<script type="text/javascript">
    $('#classesID').change(function() {
        var classesID = $(this).val();
        if(classesID == 0) {
            $('#hide-table').hide();
            $('.nav-tabs-custom').hide();
        } else {
            $.ajax({
                type: 'POST',
                url: "<?=base_url('balance/balance_list')?>",
                data: "id=" + classesID,
                dataType: "html",
                success: function(data) {
                    window.location.href = data;
                }
            });
        }
    });
</script>
