<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
        <title>Charity</title>	
        <?= link_tag(base_url().'assets/donation/assets/css/reset.css'); ?>
        <?= link_tag(base_url().'assets/donation/assets/css/styles.css'); ?>
        <?= link_tag(base_url().'assets/donation/assets/css/select.css'); ?>
    </head>
    <body>
        <div id="wrapper">
            <!-- Content Section Starts here -->			
            <div id="main">
                <section class="container" id="page-info">			
                    <div style="text-align:center;"><img src="<?=base_url()?>uploads/images/logo.png" width="auto" height="70" title=""/></div>
                        <div class="payment-success-box  payment-fail">
                            <div><img src="<?=base_url()?>assets/donation/assets/images/cartoon-icon.png" width="111" height="123" title="Transaction failed"/></div>
                            <header class="page-header failed-page-header">
                                <h2><?php echo "Oops! Your transaction failed. Please try again";?></h2>
                            </header>
                            <div class="click-section">
                                <a class="btn btn-default btn-again btn-yellow" href="<?=base_url()?>donation/index"><?php echo "Click Here";?></a>	
                            </div>	
                        </div>
                </section>
            </div>
            <!-- Content Section Starts here -->	
        </div>
    </body>
</html>