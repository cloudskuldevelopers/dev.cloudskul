<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
        <title><?=$title?></title>
        <?= link_tag(base_url().'assets/donation/assets/css/reset.css'); ?>
        <?= link_tag(base_url().'assets/donation/assets/css/styles.css'); ?>
        <?= link_tag(base_url().'assets/donation/assets/css/select.css'); ?>
    </head>
    <body>
        <?php
        session_start();
        ?>
        <div id="main">
            <section class="container container" id="page-info">
                <div class="row">
                    <div style="text-align:center;"><img src="<?=base_url()?>uploads/images/logo.png" width="auto" height="70" title=""/></div>
                    <!-- Table Section Start Here -->
                    <div class="payment-success-box">
                        <div><img src="<?=base_url()?>assets/donation/assets/images/cartoon-icon.png" width="" height="" title=""/></div>
                        <div class="page-header">
                            <h2><?php echo "Thanks for your valuable contribution";?></h2>
                        </div>
                        <div class="transition-content">
                            <p class="name"><?php echo "Dear"."  " ?><strong><?=$donor->donor_fname." ".$donor->donor_lname?>,</strong></p>
                            <p>  <?php echo "Your donation was successful. Your contribution to the growth of Cloudskul is sincerely appreciated."; ?>  
                                <strong> <?php echo "You can find the details of your transaction below."; ?> </strong> </p>
                            <div class="transition-detail">
                                <div class="donate-value-section">
                                    <a href="#" class="left active"><?=$donation->currency?></a>
                                    <span class="right">
                                    <?php if($donation->amount){?>
                                        <a href="#"><?php echo "Order id"." : ";?>
                                            <?=$remita->orderID?></a>
                                      <?php }?> 
                                        <a href="#" class="last"><?=$donation->create_date?></a>
                                    </span>
                                </div>
                                <div class="money-donate">
                                    <span id="sign">
                                        <img src="<?php echo base_url(); ?>assets/donation/assets/images/naira_icon.png" style="height: 50px; width: auto;" />
                                    </span>
                                    <span><?=number_format($donation->amount,2,".",",")?></span>
                                </div>
                                <?php if($donation->amount){ /*echo getBankDetails();*/  } ?>
                                <p><?php echo "Is there something you want to share with us?"; ?></p>
                                <p><?php echo "Feedback, comments, suggestions or compliments - do write to: ";?><strong><a href="mailto:donate@cloudskul.com">donations@cloudskul.com</a></strong>&nbsp;<strong><a href="mailto:support@cloudskul.com">support@cloudskul.com</a></strong></p>
                            </div>
                            <div class="social">
                                <span><?php echo "Share for more help :";?></span>	
                                <ul>
                                    <li><a href="#"><img src="<?=base_url()?>assets/donation/assets/images/facebook.jpg" width="23" height="23" alt="Facebook" title="Facebook" /></a></li>
                                    <li><a href="#"><img src="<?=base_url()?>assets/donation/assets/images/linkedin.jpg" width="23" height="23" alt="Linkedin" title="Linkedin" /></a></li>
                                    <li><a href="#"><img src="<?=base_url()?>assets/donation/assets/images/twitter.jpg" width="23" height="23" alt="Twitter" title="Twitter" /></a></li>
                                    <li><a href="#"><img src="<?=base_url()?>assets/donation/assets/images/google-plus.jpg" width="23" height="23" alt="Google plus" title="Google plus" /></a></li>
                                </ul>
                            </div>
                        </div>
                        </section>
                    </div>
                    </body>
                    </html>
