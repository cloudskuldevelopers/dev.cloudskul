<!DOCTYPE html>
<html>
    <head>
        <title>Donate to Cloudskul</title>   
        <meta http-equiv="Content-type" content="text/html; charset=utf-8" /> 
		<?= link_tag(base_url().'assets/donation/assets/css/reset.css'); ?>
		<?= link_tag(base_url().'assets/donation/assets/css/styles.css'); ?>
		<?= link_tag(base_url().'assets/donation/assets/css/select.css'); ?>
        <script type='text/javascript' src="<?php echo base_url(); ?>assets/donation/assets/js/jquery.min.js"></script>
        <script src="http://ajax.aspnetcdn.com/ajax/jQuery/jquery-1.11.1.min.js"></script>
        <script type='text/javascript' src="<?php echo base_url(); ?>assets/donation/assets/js/validation.js"></script>
        <script type='text/javascript' src="<?php echo base_url(); ?>assets/donation/assets/js/chosen.jquery.js"></script>
        <script type='text/javascript' src="<?php echo base_url(); ?>assets/donation/assets/js/custom.js"></script>
        <script type='text/javascript' src="<?php echo base_url(); ?>assets/donation/assets/js/jquery.dd.min.js"></script>
		<style type="text/css">.btn, input, .pay-mod span, .radio-wrapper span{border-radius: 0px !important;}</style>
    </head>
	
	
    <!--body onload="initialLook()"-->
	<body>
        <div class="form-container">

            <!-- Header -->
            <div class="donate-heading">
                <h2>CloudSkul Donation</h2>
                <div class="lang">
                    <!--check-->
                    <form id="lang" action="" method="post"> 
                         
                    <select style="width:200px" class="tech" name="language" id="tech" onchange="defineLang()">
						<option value="" >Select Language</option>                        
                    </select> 
                    </form>    
                    

                </div>
            </div>
            <!--       seclect-box  add here -->

            <!-- Content -->
            <div class="form-content"> 
                <div id="popupWait" ><b><img src="<?=base_url()?>assets/donation/assets/images/preloader.gif" /></b></div>
                <div class="modal donate-form">
                    <div class="modal-dialog">
                        <div class="modal-body">                    
                            <!--               form Start     -->
                            <form action="payment" method="post" enctype="multipart/form-data" name="frmdata" id="formData">
                              
                                 <input type="hidden" name="pLang" id="pLang" value="en"/>
                                <!--  Currency Select box Division -->
                                
                                <div class="select-country">
                                    <div class="form-group  ">
                                        <div class="dropdown">
                                            <select name="currency" id="currency" class="selectBox" onchange="changeCurrencySignAndAmount(this.value)">
                                                <option value="">Select Currency</option>
												<option selected value="NGR">NGR - Nigerian Naira</option>
											</select>
                                        </div>
                                    </div>
                                </div>
                                <!--     End Currency Select Box-->

                                <!--  Donation Amounts tag-->
                                <div class="content-inner-section">
                                    <div class="row">                            
                                        <div class="form-group ">
                                            <fieldset>
                                                <legend>How much would you like to donate</legend>
                                                <div class="choose-pricing">
                                                    <div class="btn-group">
                                                        <button type="button" class="btn btn-default active selectvalue">1000</button>
                                                        <button type="button" class="btn btn-default selectvalue">3000</button>
                                                        <button type="button" class="btn btn-default selectvalue">5000</button>
                                                        <button type="button" class="btn btn-default selectvalue">10000</button>
                                                        <button type="button" class="btn btn-default selectvalue">25000</button>
                                                        <button type="button" class="btn btn-default selectvalue">50000</button>
                                                        <input type="text" placeholder="Custom" name="donation-amount" class="inpt-first form-control " id="donation-amount" onclick="if (this.defaultValue==this.value) this.value=''" onblur="if (this.value=='') this.value=this.defaultValue" value="Custom">
                                                        <input type="hidden" name="donation-amount-value"  id="donation-amount-value" value="1000"/>
                                                    </div>                                        
                                                    <div class="money-donate">
                                                        <span id="displayAmount"><span id="sign">
															<img src="<?php echo base_url(); ?>assets/donation/assets/images/naira_icon.png" style="height: 50px; width: auto;" />
														</span>
														<span id="amountToShow">1000</span></span>
                                                    </div>                                                      
                                                </div>
                                            </fieldset>
                                        </div>
                                    </div> 

                                    <!-- End Donation Amounts tag-->            

                                    <!--   Payment Type Section-->
                                    <div class="select-payment-type" style="display: none"> 
                                        <fieldset>
                                            <legend class="upper">
                                            Payment Type                                            
											</legend>
                                                <span class="radio-wrapper pay active" id="oneTime-not-allow"><input type="radio" onclick="enableBlock(2)" name="paymentType" value="2" checked><span>One Time Payment</span></span>
                                            <p style="color:red" id="payment-type-error"></p>
                                        </fieldset>
                                    </div>
                                    <!--    End  Payment Type Section       -->




                                    <!--  Div block for selecting cycle of recurring period -->
                                    <div class="select-payment-container">
                                        <div class="row display cycle" style="display: none" id="cycle_div">
                                            <span class="full">Select Payment Cycle </span>
                                            <select name="cycle" id="cycle" class="selectBox" onchange="validateCycle()">
                                                <option value="">Select Cycle</option><option value="Weekly">Weekly</option>
                       <option value="Montly">Monthly</option>
                      <option value="Yearly">Yearly</option>                                                
                                            </select>
                                        </div>
                                        <!-- End  Div block for selecting cycle of recurring period -->                



                                        <!--   Div block for selecting number of cycle of recurring period -->

                                        <div class="row display" style="display: none" id="num_cycle_div">
                                            <span class="full">Select Payment Number of Cycle </span>
                                            <select name="nubOfCycle" id="num_cycle" class="selectBox" onchange="validateNumberOfCycle()" >
                                               <option value="">Select number of cycle </option><option value="1">1</option><option value="2">2</option><option value="3">3</option><option value="4">4</option><option value="5">5</option><option value="6">6</option><option value="7">7</option><option value="8">8</option><option value="9">9</option><option value="10">10</option><option value="11">11</option><option value="12">12</option>                                                
                                                
                                                
                                            </select>
                                                                       
                                        </div>
                                    </div>
                                    <!-- End  Div block for selecting number of cycle of recurring period -->

                                    <!--    Div Block for Select payment Option    -->
                                    <div class="row display cycle" style="" id="paymentOption-div">
                                        <div class="payment-mode">
                                            <h3 class="pay-mode-heading">Select payment option</h3>
                                            <div class="pay-mod">
                                                <span class="link active creditcard" id="2">Debit/Credit Card</span>
												<span style="display: none" id="3" class="link banking-transfer ">Bank Transfer</span>
												<span style="display: none" id="1" class="link paypal ">Paypal</span>
												<span style="display: none" id="4" class="link  stripe ">Stripe PayG</span>
                                            </div>
                                            <input type="hidden" name="pay-mod" id="pay-mod" value="2">
                                            <!--p id="creMsg">In Case of credit card ,Transaction in Doller(N) will be accepted.</p-->
                                            <p style="display: none" id="creMsg">If you use Paypal OR Stripe, the transaction will be made in dollars($USD).</p>
                                            </br>
                                            <p id="creMsg">Please note that you would require your credit/debit card to make payment.</p>
                                        </div>                       
                                    </div>
                                    
                                     
                                    <!--  benevolent Detail Section -->
                                    <fieldset class="personal-detail">
                                        <legend class="upper">Personal Details</legend>                      	
                                        <div class="row">
                                            <div class="form-group ">
                                                <input type="text" name="name" class="form-control" id="name" value="Name" onclick="if (this.defaultValue==this.value) this.value=''" 
                                                       onblur="if (this.value=='') this.value=this.defaultValue">
                                                <input type="text" name="last_name" class="form-control right" id="last_name" value="Last Name" onclick="if (this.defaultValue==this.value) this.value=''" 
                                                       onblur="if (this.value=='') this.value=this.defaultValue">
                                            </div>          
                                        </div>
                                        <div class="row">
                                            <div class="form-group ">
                                                <input type="text" name="email" class="form-control" id="email" value="Email" onclick="if (this.defaultValue==this.value) this.value=''" 
                                                       onblur="if (this.value=='') this.value=this.defaultValue">
                                                <input type="text" name="phone" class="form-control right" id="phone" value="Phone No" onclick="if (this.defaultValue==this.value) this.value=''" 
                                                       onblur="if (this.value=='') this.value=this.defaultValue">
                                            </div>      
                                        </div>

                                        <div class="row">
                                            <div class="form-group ">
                                                <textarea class="form-control" name="address" id="message" onfocus="if(this.value==this.defaultValue)this.value='';" onblur="if(this.value=='')this.value=this.defaultValue;">Address</textarea>
                                                <textarea class="form-control" id="add-note" name="notes" onfocus="if(this.value==this.defaultValue)this.value='';" onblur="if(this.value=='')this.value=this.defaultValue;" >Additional Note</textarea>

                                            </div>
                                        </div>


                                        <!--  End benevolent Detail Section-->
                                        <div class="row">
                                            <div class="form-group button">
                                                <input type="button" id="donate-btn" value="Donate" class="btn btn-default pull-right">
                                            </div>
                                        </div>
                                    </fieldset>
                                </div>
                            </form>
                        </div>
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->      
        </div>

        <script type="text/javascript">
            $(".selectvalue").click(function(){
                //$( this ).find( 'button.active' ).removeClass( 'active' );
                $( "button.active" ).removeClass( 'active' );
                $( this ).addClass( 'active' );
                $("#amountToShow").html($(this).html());
                $("#donation-amount-value").val($(this).html());
                $('input[name=donation-amount]').val("Custom");
            });

            $("#donation-amount").keyup(function(){
                $("#amountToShow").html($(this).val());
                $("#donation-amount-value").val($(this).val());
                
            });

            $(".link").click(function(){
                //$( this ).find( 'button.active' ).removeClass( 'active' );
                $( "span.link.active" ).removeClass( 'active' );
                $( this ).addClass( 'active' );
                //console.log($(this).attr("id"));
                //$("#amountToShow").html($(this).html());
                $("#pay-mod").val($(this).attr("id"));
            });

            function check_amt(amt){
                if(amt < 1000){
                    //alert("Amount should be atleast N1000. Thank you");
                    return false;
                }
            }

            $("#donate-btn").click(function(){
                var amount = $("#donation-amount-value").val();
                var p_type = $(".link.active").html();
                console.log("amt: "+amount+" type: "+p_type);

                if(validateCurrencyCode() & validateName() & validatelastName() & validateEmail() & validatePhone() & validateAddress()){
                    /*if($('input[name=donation-amount]').val() < 999){
                    //if($("#donation-amount-value").val() < 999){
                        
                    }*/
                    if(amount < 1000){
                        alert("Amount should be atleast N1000. Thank you");
                        return true;
                    }
                    var paymentOption=$(".pay-mod span.active").attr("id");
                    var b_name=$("#name").val();
                    var b_lastName=$("#last_name").val();
                    var b_clientMail=$("#email").val();
                    var b_amt=$.trim(amount);
                    var b_donationType="One Time donation";
                    var b_currency=$("#currency").val();
                    var b_phone=$("#phone").val();
                    var b_address=$("#message").val();
                    var b_note=$("#add-note").val();

                    $.ajax({
                        type: "POST",
                        url: "<?=base_url('donation/payment')?>",
                        dataType: "html",
                        data: {
                            fname: b_name,
                            lname: b_lastName,
                            donationType: b_donationType,
                            amount: b_amt,
                            currency: b_currency,
                            email: b_clientMail,
                            phone: b_phone,
                            address: b_address,
                            note: b_note
                        },
                        beforeSend: function() {
                            $('#popupWait').fadeIn();
                        },
                        success: function(response) { 
                            if(response != "") {
                                $('#popupWait').fadeIn();
                                window.location.href = "<?=base_url('donation/make_payment')?>"+"/"+response; //baseurl+'donation-payment-success.php?amount='+b_amt+'&currency='+b_currency+'&name='+b_name+" "+b_lastName;                         
                            }else{
                                alert("Transaction failed");
                            }
                        }
                    });
                }else{
                }
            });
            
        </script>
		
		
    </body>
</html> 
