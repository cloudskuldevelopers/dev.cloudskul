
<div class="box">
    <div class="box-header">
        <h3 class="box-title"><i class="fa icon-eattendance"></i> <?=$this->lang->line('panel_title')?></h3>

       
        <ol class="breadcrumb">
            <li><a href="<?=base_url("dashboard/index")?>"><i class="fa fa-laptop"></i> <?=$this->lang->line('menu_dashboard')?></a></li>
            <li><a href="<?=base_url("systemadmin/index")?>"><?=$this->lang->line('menu_systemadmin')?></a></li>
            <li class="active"><?=$this->lang->line('menu_add')?> <?=$this->lang->line('menu_access')?></li>
        </ol>
    </div><!-- /.box-header -->
    <!-- form start -->
    <div class="box-body">
        <div class="row">
            <div class="col-sm-12">
                <?php if(count($schools)) { ?>
                    <div id="hide-table">
                        <table id="example1" class="table table-striped table-bordered table-hover dataTable no-footer">
                            <thead>
                                <tr>
                                    <th class="col-sm-1"><?=$this->lang->line('slno')?></th>
                                    <th class="col-lg-1"><?=$this->lang->line('school_logo')?></th>
                                    <th class="col-lg-3"><?=$this->lang->line('school_school')?></th>
                                    <th class="col-sm-1"><?=btn_attendance('', '', 'all_attendance', $this->lang->line('add_all_attendance')).$this->lang->line('action')?></th>
                                </tr>
                            </thead>
                            <tbody id="list">
                                    <?php if(count($schools)) {$i = 1; foreach($schools as $school) { ?>
                                    <tr>
                                        <td data-title="<?=$this->lang->line('slno')?>">
                                            <?php echo $i; ?>
                                        </td>
                                        <td data-title="<?=$this->lang->line('school_school')?>">
                                            <?php $array = array(
                                                    "src" => base_url('uploads/images/'.$school->photo),
                                                    'width' => '35px',
                                                    'height' => '35px',
                                                    'class' => 'img-thumbnail'

                                                );
                                                echo img($array); 
                                            ?>
                                        </td>
                                        <td data-title="<?=$this->lang->line('school_school')?>">
                                            <?php echo $school->school; ?>
                                        </td>
                                        <td data-title="<?=$this->lang->line('action')?>">
                                            <?php 
                                                // $aday = "a".abs($day);
                                                $method = '';
                                                echo  btn_attendance($systemadmin->systemadminID, $method, 'attendance btn btn-warning', $this->lang->line('add_title'));

                                                /*foreach ($eattendances as $eattendance) {
                                                    if($eattendance->studentID == $student->studentID && $examID == $eattendance->examID && $classesID == $eattendance->classesID && $subjectID == $eattendance->subjectID) {
                                                        $method = '';
                                                        if($eattendance->eattendance == "Present") {$method = "checked";}
                                                        echo  btn_attendance($student->studentID, $method, 'attendance btn btn-warning', $this->lang->line('add_title'));
                                                        break;
                                                    }
                                                }*/
                                            ?>
                                        </td>
                                    </tr>
                                <?php $i++; }} ?>
                                
                            </tbody>
                        </table>
                    </div>

                    <script type="text/javascript">

                        $('.attendance').click(function() {

                            var examID = "<?=$examID?>";
                            var classesID = "<?=$classesID?>";
                            var subjectID = "<?=$subjectID?>";
                            var studentID = $(this).attr('id');
                            var status = "";

                            if($(this).prop('checked')) {
                                status = "checked";
                            } else {
                                status = "unchecked";
                            }

                            if(parseInt(examID) && parseInt(classesID) && parseInt(subjectID)) {
                                $.ajax({
                                    type: 'POST',
                                    url: "<?=base_url('eattendance/single_add')?>",
                                    data: {"examID" : examID, "classesID" : classesID, "subjectID" : subjectID, "studentID" : studentID , "status" : status },
                                    dataType: "html",
                                    success: function(data) {
                                        toastr["success"](data)
                                        toastr.options = {
                                          "closeButton": true,
                                          "debug": false,
                                          "newestOnTop": false,
                                          "progressBar": false,
                                          "positionClass": "toast-top-right",
                                          "preventDuplicates": false,
                                          "onclick": null,
                                          "showDuration": "500",
                                          "hideDuration": "500",
                                          "timeOut": "5000",
                                          "extendedTimeOut": "1000",
                                          "showEasing": "swing",
                                          "hideEasing": "linear",
                                          "showMethod": "fadeIn",
                                          "hideMethod": "fadeOut"
                                        }
                                    }
                                });
                            }
                        });


                        $('.all_attendance').click(function() {
                            var examID = "<?=$examID?>";
                            var classesID = "<?=$classesID?>";
                            var subjectID = "<?=$subjectID?>";
                            var status = "";

                            if($(".all_attendance").prop('checked')) {
                                status = "checked";
                                $('.attendance').prop("checked", true);
                            } else {
                                status = "unchecked";
                                $('.attendance').prop("checked", false);
                            }

                            if(parseInt(examID) && parseInt(classesID) && parseInt(subjectID)) {
                                $.ajax({
                                    type: 'POST',
                                    url: "<?=base_url('eattendance/all_add')?>",
                                    data: {"examID" : examID, "classesID" : classesID, "subjectID" : subjectID , "status" : status },
                                    dataType: "html",
                                    success: function(data) {
                                        toastr["success"](data)
                                        toastr.options = {
                                          "closeButton": true,
                                          "debug": false,
                                          "newestOnTop": false,
                                          "progressBar": false,
                                          "positionClass": "toast-top-right",
                                          "preventDuplicates": false,
                                          "onclick": null,
                                          "showDuration": "500",
                                          "hideDuration": "500",
                                          "timeOut": "5000",
                                          "extendedTimeOut": "1000",
                                          "showEasing": "swing",
                                          "hideEasing": "linear",
                                          "showMethod": "fadeIn",
                                          "hideMethod": "fadeOut"
                                        }

                                    }
                                });
                            }
                        });
                    </script>
                <?php } ?>
                
                    
            </div> <!-- col-sm-12 -->
        </div><!-- row -->
    </div><!-- Body -->
</div><!-- /.box -->

<script type="text/javascript">
$('#classesID').change(function(event) {
    var classesID = $(this).val();
    if(classesID === '0') {
        $('#subjectID').val(0);
    } else {
        $.ajax({
            type: 'POST',
            url: "<?=base_url('eattendance/subjectcall')?>",
            data: "id=" + classesID,
            dataType: "html",
            success: function(data) {
               $('#subjectID').html(data);
            }
        });
    }
});

</script>