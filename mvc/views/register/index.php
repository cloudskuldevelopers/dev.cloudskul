<style type="text/css">
	.form-control{
		border:1px solid #999;
	}
</style>
	
	<div id="page-loader" class="fade in"><span class="spinner"></span></div>
	<!-- end #page-loader -->
	
	<!-- begin #page-container -->
	<div id="page-container" class="fade">
	    <!-- begin register -->
        <div class="register register-with-news-feed">
            <!-- begin news-feed -->
            <div class="news-feed">
                <div class="news-image">
                    <img src="<?php echo base_url('assets/theme/img/login-bg/bg-8.jpg'); ?>" alt="" />
                </div>
                <div class="news-caption">
					<h4 class="caption-title"><img src="<?php echo base_url('uploads/images/logo.png'); ?>" class="img-responsive" style="vertical-align:sub; width:80px; display:inline-block" /> A 21st Century Educational Revolution</h4>
					<p>
						 A revolutionary school management system with the best educational apps for schools.
					</p>
                </div>
            </div>
            <!-- end news-feed -->
            <!-- begin right-content -->
            <div class="right-content">
                <h1 class="register-header">
                    <span style="color:#00ACAC;"><i class="fa fa-user-plus"></i></span> <span style="color:rgba(36, 42, 48, 0.76);"> Sign Up </span>
                    <small>Register your school with Cloudskul | It is absolutely free</small>
                </h1>
				
                <!-- begin register-content -->
                <div class="register-content">
                    <form role="form" method="post"  enctype="multipart/form-data" class="margin-bottom-0">
						<?php 
							if($form_validation == "No"){
							} else {
								if(count($form_validation)) {
									echo "<div class=\"alert alert-danger alert-dismissable\">
										<i class=\"fa fa-ban\"></i>
										<button aria-hidden=\"true\" data-dismiss=\"alert\" class=\"close\" type=\"button\">×</button>
										$form_validation
									</div>";
								}
							}
						?>
                        <div class="row m-b-15">
                            <div class="col-md-12">
                                <input type="text" placeholder="School name" class="form-control" id="school" name="school" value="<?=set_value('school')?>" required>
                            </div>
                        </div>
						<div class="row m-b-15">
                            <div class="col-md-12">
                                <input type="text" placeholder="School Alias [Eg: ABCD]" class="form-control" id="alias" name="alias" value="<?=set_value('alias')?>" maxlength="4" required>
                            </div>
                        </div>
						<div class="row m-b-15">
                            <div class="col-md-12">
                                <input type="email" placeholder="Email" class="form-control" id="email" name="email" value="<?=set_value('email')?>" required>
                            </div>
                        </div>
						<div class="row m-b-15">
                            <div class="col-md-12">
                                <input type="text" placeholder="e.g 080xxxxxxxx" class="form-control" id="phone" name="phone" value="<?=set_value('phone')?>" required>
                            </div>
                        </div>
						<div class="row m-b-15">
                            <div class="col-md-12">
                                <textarea class="form-control" placeholder="Address" style="resize:none;" id="address" name="address" required><?=set_value('address')?></textarea>
                            </div>
                        </div>
						<div class="row m-b-15">
                            <div class="col-md-12">
                                <input type="text" placeholder="Partner Referrer ID [optional]" class="form-control" id="referrerID" name="referrerID" value="<?=set_value('referrerID')?>">
                            </div>
                        </div>
                        <div class="row m-b-15">
                            <div class="col-md-12">
                                <div class="js">
									<input type="file" name="image" id="file-1" class="inputfile inputfile-1" data-multiple-caption="{count} files selected" multiple />
									<label for="file-1"><svg xmlns="http://www.w3.org/2000/svg" width="20" height="17" viewBox="0 0 20 17"><path d="M10 0l-5.2 4.9h3.3v5.1h3.8v-5.1h3.3l-5.2-4.9zm9.3 11.5l-3.2-2.1h-2l3.4 2.6h-3.5c-.1 0-.2.1-.2.1l-.8 2.3h-6l-.8-2.2c-.1-.1-.1-.2-.2-.2h-3.6l3.4-2.6h-2l-3.2 2.1c-.4.3-.7 1-.6 1.5l.6 3.1c.1.5.7.9 1.2.9h16.3c.6 0 1.1-.4 1.3-.9l.6-3.1c.1-.5-.2-1.2-.7-1.5z"/></svg> <span>Upload school logo &hellip;</span></label>
									<p style="font-weight: bold; font-size:12px;" class="text-info"><i>Image dimensions - 120px * 100px</i></p>
								</div>
                            </div>
                        </div>
						<div class="row m-b-15">
                            <div class="col-md-12">
                                <input type="text" placeholder="First name and Last name " class="form-control" id="name" name="name" value="<?=set_value('name')?>" required>
                            </div>
                        </div>
						<div class="row m-b-15">
                            <div class="col-md-12">
								<input type="text" name="dob" class="form-control" id="datepicker-default" placeholder="Date of birth" value="<?=set_value('dob')?>" />
                            </div>
                        </div>
						<div class="row m-b-15">
                            <div class="col-md-12">
                                <?php 
									echo form_dropdown("sex", array('Male' => 'Male', 'Female' => 'Female'), set_value("sex"), "id='sex' class='form-control' required"); 
								?>
                            </div>
                        </div>
						<div class="row m-b-15">
                            <div class="col-md-12">
                                <input type="text" placeholder="username" class="form-control" id="username" name="username" value="<?=set_value('username')?>" required>
                            </div>
                        </div>
						<div class="row m-b-15">
                            <div class="col-md-12">
                                <input type="password" placeholder="Password" class="form-control" id="password" name="password" value="<?=set_value('password')?>" required>
                            </div>
                        </div>
						
                        <div class="checkbox m-b-30">
                            <label>
                                By registering, you agree to Cloudskul's <a style="color: #428BCA;" href="https://www.cloudskul.com/index.php?/cloudus/terms" target="_blank">Terms and Conditions</a>.
                            </label>
                        </div>
                        <div class="register-buttons">
                            <button type="submit" class="btn btn-primary btn-block btn-lg" style="border-radius:0px;">Sign Up</button>
                        </div>
                    </form>
					<hr />
					<div class="row row-space-10 m-t-20">
						<div class="col-md-6" style="padding:0px">
							<a href="<?php echo base_url(); ?>" class="btn btn-primary btn-block btn-lg" style="background:#F35A57; border:none; border-radius:0px; font-size:15px;"><i class="fa fa-home"></i> Back to Home</a>
						</div>
						<div class="col-md-6" style="padding:0px">
							<a href="<?php echo base_url('signin/index'); ?>" class="btn btn-primary btn-block btn-lg" style="background:#49B6D6; border:none; border-radius:0px; font-size:15px;"><i class="fa fa-lock"></i> Back to Login</a>
						</div>
					</div>
					<hr />
					<p class="text-center text-inverse">
						&copy; Cloudskul All Right Reserved <?=date("Y")?>
					</p>
                </div>
                <!-- end register-content -->
            </div>
            <!-- end right-content -->
        </div>
	</div>

<?php if ($this->session->flashdata('success')): ?>
	<style>
	 #reg_form{ display: none;}
	 #success_msg{ display: block;}
	</style>
    <script type="text/javascript">
        toastr["success"]("<?=$this->session->flashdata('success');?>")
        toastr.options = {
          "closeButton": true,
          "debug": false,
          "newestOnTop": false,
          "progressBar": false,
          "positionClass": "toast-top-right",
          "preventDuplicates": false,
          "onclick": null,
          "showDuration": "1500",
          "hideDuration": "1500",
          "timeOut": "5000",
          "extendedTimeOut": "1000",
          "showEasing": "swing",
          "hideEasing": "linear",
          "showMethod": "fadeIn",
          "hideMethod": "fadeOut"
        }
    </script>
<?php endif ?>
<?php if ($this->session->flashdata('error')): ?>
   <script type="text/javascript">
        toastr["error"]("<?=$this->session->flashdata('error');?>")
        toastr.options = {
          "closeButton": true,
          "debug": false,
          "newestOnTop": false,
          "progressBar": false,
          "positionClass": "toast-top-right",
          "preventDuplicates": false,
          "onclick": null,
          "showDuration": "500",
          "hideDuration": "500",
          "timeOut": "5000",
          "extendedTimeOut": "1000",
          "showEasing": "swing",
          "hideEasing": "linear",
          "showMethod": "fadeIn",
          "hideMethod": "fadeOut"
        }
    </script>
<?php endif ?>