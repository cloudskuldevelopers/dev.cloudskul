<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->
<head>
	<meta charset="utf-8" />
	<title>Registration | Cloudskul</title>
	<meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" name="viewport" />
	<meta content="" name="description" />
	<meta content="" name="author" />
	
	<!-- ================== BEGIN BASE CSS STYLE ================== -->
	<link href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet" />
	<link href="<?php echo base_url('assets/theme/page/assets/plugins/bootstrap/css/bootstrap.min.css'); ?>" rel="stylesheet" />
	<link href="<?php echo base_url('assets/theme/page/assets/plugins/font-awesome/css/font-awesome.min.css'); ?>" rel="stylesheet" />
	<link href="<?php echo base_url('assets/theme/page/assets/css/animate.min.css'); ?>" rel="stylesheet" />
	<link href="<?php echo base_url('assets/theme/page/assets/css/style.min.css'); ?>" rel="stylesheet" />
	<link href="<?php echo base_url('assets/theme/page/assets/css/style-responsive.min.css'); ?>" rel="stylesheet" />
	<link href="<?php echo base_url('assets/theme/page/assets/css/theme/default.css'); ?>" id="theme" rel="stylesheet" />
	<!-- ================== END BASE CSS STYLE ================== -->
	
	<!-- ================== BEGIN BASE JS ================== -->
	<script src="<?php echo base_url('assets/theme/page/assets/plugins/pace/pace.min.js'); ?>"></script>
	<!-- ================== END BASE JS ================== -->
</head>
<body data-spy="scroll" data-target="#header-navbar" data-offset="51">
    <!-- begin #page-container -->
    <div id="page-container" class="fade">
        <!-- begin #header -->
        <div id="header" class="header navbar navbar-transparent navbar-fixed-top" style="padding-bottom:10px;">
            <!-- begin container -->
            <div class="container">
                <!-- begin navbar-header -->
                <div class="navbar-header" style="padding-top:10px;">
                    <img src="<?php echo base_url('uploads/images/logo.png') ?>" width="100px" class="img img-responsive" />
                </div>
                <!-- end navbar-header -->
                <!-- begin navbar-collapse -->
                <div class="collapse navbar-collapse" id="header-navbar">
                    <ul class="nav navbar-nav navbar-right">
                    </ul>
                </div>
                <!-- end navbar-collapse -->
            </div>
            <!-- end container -->
        </div>
        <!-- end #header -->
        
        <!-- begin #home -->
        <div id="home" class="content has-bg home">
            <!-- begin content-bg -->
            <div class="content-bg">
                <img src="<?php echo base_url('assets/theme/page/assets/img/home-bg.jpg'); ?>" alt="Home" />
            </div>
            <!-- end content-bg -->
            <!-- begin container -->
            <div class="container home-content">
                <h1 style='font-family: "Segoe UI"'>Your Registration was Successful</h1>
                <h3 style='font-family: "Segoe UI"'>You will be contacted shortly.</h3>
                <h3 style='font-family: "Segoe UI"'>Thank you for choosing Cloudskul</h3>
                <!--a href="<?php echo base_url('signin/index'); ?>" class="btn btn-theme"><i class="fa fa-lock"></i> Back to Login</a--> <a href="<?php echo base_url(); ?>" class="btn btn-theme"><i class="fa fa-home"></i> Back to Home</a><br />
                <br />
            </div>
            <!-- end container -->
        </div>
        <!-- end #home -->
        
        <!-- begin #footer -->
        <div id="footer" class="footer">
            <div class="container">
                <div class="footer-brand">
                    <img src="<?php echo base_url('uploads/images/logo.png') ?>" width="100px" />
                </div>
                <p>
                    &copy; Copyright Cloudskul <?=date("Y")?> <br />
                </p>
                <p class="social-list">
                    <a href="#"><i class="fa fa-facebook fa-fw"></i></a>
                    <a href="#"><i class="fa fa-instagram fa-fw"></i></a>
                    <a href="#"><i class="fa fa-twitter fa-fw"></i></a>
                    <a href="#"><i class="fa fa-google-plus fa-fw"></i></a>
                </p>
            </div>
        </div>
        <!-- end #footer -->
        
    </div>
    <!-- end #page-container -->
	
	<!-- ================== BEGIN BASE JS ================== -->
	<script src="<?php echo base_url('assets/theme/page/assets/plugins/jquery/jquery-1.9.1.min.js'); ?>"></script>
	<script src="<?php echo base_url('assets/theme/page/assets/plugins/jquery/jquery-migrate-1.1.0.min.js'); ?>"></script>
	<script src="<?php echo base_url('assets/theme/page/assets/plugins/bootstrap/js/bootstrap.min.js'); ?>"></script>
	<!--[if lt IE 9]>
		<script src="assets/crossbrowserjs/html5shiv.js"></script>
		<script src="assets/crossbrowserjs/respond.min.js"></script>
		<script src="assets/crossbrowserjs/excanvas.min.js"></script>
	<![endif]-->
	<script src="<?php echo base_url('assets/theme/page/assets/plugins/jquery-cookie/jquery.cookie.js'); ?>"></script>
	<script src="<?php echo base_url('assets/theme/page/assets/plugins/scrollMonitor/scrollMonitor.js'); ?>"></script>
	<script src="<?php echo base_url('assets/theme/page/assets/js/apps.min.js'); ?>"></script>
	<!-- ================== END BASE JS ================== -->
	
	<script>    
	    $(document).ready(function() {
	        App.init();
	    });
	</script>
</body>
</html>
