<?php 
$usertype = $this->session->userdata("usertype");
if($usertype == "Admin" || $usertype == "Super Admin" || $usertype == "Teacher") {
?>
    <div class="well">
        <div class="row">
            <div class="col-sm-6">
                <button class="btn-cs btn-sm-cs" onclick="javascript:printDiv('printablediv')"><span class="fa fa-print"></span> <?=$this->lang->line('print')?> </button>
                <?php
                 // echo btn_add_pdf('assignment/print_preview/'.$assignment->assignmentID.'/'.$set, $this->lang->line('pdf_preview')) 
                ?>
                <?php echo btn_sm_edit('assignment/edit/'.$assignment->assignmentID.'/'.$set, $this->lang->line('edit')) 
                ?>  
                <!-- <button class="btn-cs btn-sm-cs" data-toggle="modal" data-target="#mail"><span class="fa fa-envelope-o"></span> <?=$this->lang->line('mail')?></button> -->
            </div>


            <div class="col-sm-6">
                <ol class="breadcrumb">
                    <li><a href="<?=base_url("dashboard/index")?>"><i class="fa fa-laptop"></i> <?=$this->lang->line('menu_dashboard')?></a></li>
                    <li><a href="<?=base_url("assignment/index/$set")?>"><?=$this->lang->line('menu_assignment')?></a></li>
                    <li class="active"><?=$this->lang->line('menu_view')?></li>
                </ol>
            </div>

        </div>

    </div>

<?php } ?>


<section class="panel">
    <div class="panel-body bio-graph-info">
        <div id="printablediv" class="box-body">
            <div class="row">
                <div class="col-sm-12">
                    <div class="profile-view-tab">
                        <p>
                        <span><?=$this->lang->line('assignment_title')?></span>
                        <?php echo $assignment->title; ?>
                        </p>
                    </div>
                    <div class="profile-view-tab">
                        <p>
                        <span><?=$this->lang->line('assignment_assignment')?></span>
                        <?php echo $assignment->description; ?>
                        </p>
                    </div>
                    <div class="profile-view-tab">
                        <p>
                        <span><?=$this->lang->line('assignment_end_date')?></span>
                        <?php echo date("d M Y", strtotime($assignment->end_date)); ?>
                        </p>
                    </div>
                    <div class="profile-view-tab">
                        <p>
                        <span><?=$this->lang->line('classes')?></span>
                        <?php echo $class->classes; ?>
                        </p>
                    </div>
                    <div class="profile-view-tab">
                        <p>
                        <span><?=$this->lang->line('section')?></span>
                        <?php echo $section->section; ?>
                        </p>
                    </div>
                    <div class="profile-view-tab">
                        <p>
                        <span><?=$this->lang->line('subject')?></span>
                        <?php echo $subject->subject; ?>
                        </p>
                    </div>
                    <div class="profile-view-tab">
                        <p>
                        <span><?=$this->lang->line('created_by')?></span>
                        <?php echo $assignment->create_username; ?>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="panel">
    <div class="panel-body bio-graph-info">
        <div id="printablediv" class="box-body">
            <div class="row">
                <div class="col-sm-12">
                    <div id="hide-table">
                        <table id="example1" class="table table-striped table-bordered table-hover dataTable no-footer">
                            <thead>
                                <tr>
                                    <th class="col-sm-1"><?=$this->lang->line('slno')?></th>
                                    <th class="col-sm-2"><?=$this->lang->line('note')?></th>
                                    <th class="col-sm-2"><?=$this->lang->line('attachment_submit_date')?></th>
                                    <th class="col-sm-4"><?=$this->lang->line('assignment_doc')?></th>
                                    <?php if ($usertype=="Admin" || $usertype == "Super Admin" || $usertype=="Student"): ?>
                                    <th class="col-sm-2"><?=$this->lang->line('action')?></th>
                                    <?php endif ?>
                                </tr>
                            </thead>
                            <tbody>
                                <?php if(count($assignment_attachment)) {$i = 1; foreach($assignment_attachment as $attachment) { ?>
                                    <tr>
                                        <td data-title="<?=$this->lang->line('slno')?>">
                                            <?php echo $i; ?>
                                        </td>
                                        <td data-title="<?=$this->lang->line('note')?>">
                                            <?php 
                                                echo $attachment->note;
                                            ?>
                                        </td>
                                        <td data-title="<?=$this->lang->line('attachment_submit_date')?>">
                                            <?php echo date("d M Y", strtotime($attachment->create_date)); ?>
                                        </td>
                                        <td data-title="<?=$this->lang->line('assignment_doc')?>" >
                                            <a class="text-info" href="<?=base_url('uploads/assignment/'.$attachment->attachment);?>" target="_blank"><?php echo "Assignment Document"; ?></a> 
                                        </td>
                                        <?php if ($usertype=="Admin" || $usertype == "Super Admin" || $usertype=="Student"): ?>
                                        <td data-title="<?=$this->lang->line('action')?>">
                                        
                                            <?php echo btn_delete('assignment/delete_attachment/'.$attachment->assignment_attachmentID."/".$set, $this->lang->line('delete')) ?>
                                        </td>
                                        <?php endif ?>
                                    </tr>
                                <?php $i++; }} ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<?php if ($usertype=="Student"): ?>    
<section class="panel">
    <div class="panel-body bio-graph-info">
        <div id="printablediv" class="box-body">
            <div class="row">
                <div class="col-sm-12">
                    <form class="form-horizontal" role="form" method="post" enctype="multipart/form-data">
                        <?php 
                            if(isset($attachment_error)) 
                                echo "<div class='form-group has-error' >";
                            else     
                                echo "<div class='form-group' >";
                        ?>
                            <label for="photo" class="col-sm-2 control-label col-xs-8 col-md-2">
                                <?=$this->lang->line("assignment_doc")?>
                            </label>
                            <div class="col-sm-4 col-xs-6 col-md-4">
                                <input class="form-control"  id="uploadFile" placeholder="Choose File" disabled />  
                            </div>
                            <div class="col-sm-2 col-xs-6 col-md-2">
                                <div class="fileUpload btn btn-success form-control">
                                    <span class="fa fa-repeat"></span>
                                    <span><?=$this->lang->line("upload")?></span>
                                    <input id="uploadBtn" type="file" class="upload" name="attachment" />
                                </div>
                            </div>
                             <span class="col-sm-4 control-label col-xs-6 col-md-4">
                               
                                <?php if(isset($attachment_error)) echo $attachment_error; ?>
                            </span>
                        </div>

                        <?php 
                            if(form_error('note')) 
                                echo "<div class='form-group has-error' >";
                            else     
                                echo "<div class='form-group' >";
                        ?>
                            <label for="note" class="col-sm-2 control-label">
                                <?=$this->lang->line("note")?>
                            </label>
                            <div class="col-sm-6">
                                <textarea class="form-control" id="note" name="note" ><?=set_value('note')?></textarea>
                            </div>
                            <span class="col-sm-3 control-label">
                                <?php echo form_error('note'); ?>
                            </span>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-offset-2 col-sm-8">
                                <input type="submit" class="btn btn-success" value="<?=$this->lang->line("add_class")?>" >
                            </div>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
<?php endif ?>

<!-- email modal starts here -->
<!-- <form class="form-horizontal" role="form" action="<?=base_url('assignment/send_mail');?>" method="post">
    <div class="modal fade" id="mail">
      <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title"><?=$this->lang->line('mail')?></h4>
            </div>
            <div class="modal-body">
            
                <?php 
                    if(form_error('to')) 
                        echo "<div class='form-group has-error' >";
                    else     
                        echo "<div class='form-group' >";
                ?>
                    <label for="to" class="col-sm-2 control-label">
                        <?=$this->lang->line("to")?>
                    </label>
                    <div class="col-sm-6">
                        <input type="email" class="form-control" id="to" name="to" value="<?=set_value('to')?>" >
                    </div>
                    <span class="col-sm-4 control-label" id="to_error">
                    </span>
                </div>

                <?php 
                    if(form_error('subject')) 
                        echo "<div class='form-group has-error' >";
                    else     
                        echo "<div class='form-group' >";
                ?>
                    <label for="subject" class="col-sm-2 control-label">
                        <?=$this->lang->line("subject")?>
                    </label>
                    <div class="col-sm-6">
                        <input type="text" class="form-control" id="subject" name="subject" value="<?=set_value('subject')?>" >
                    </div>
                    <span class="col-sm-4 control-label" id="subject_error">
                    </span>

                </div>

                <?php 
                    if(form_error('message')) 
                        echo "<div class='form-group has-error' >";
                    else     
                        echo "<div class='form-group' >";
                ?>
                    <label for="message" class="col-sm-2 control-label">
                        <?=$this->lang->line("message")?>
                    </label>
                    <div class="col-sm-6">
                        <textarea class="form-control" id="message" style="resize: vertical;" name="message" value="<?=set_value('message')?>" ></textarea>
                    </div>
                </div>

            
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" style="margin-bottom:0px;" data-dismiss="modal"><?=$this->lang->line('close')?></button>
                <input type="button" id="send_pdf" class="btn btn-success" value="<?=$this->lang->line("send")?>" />
            </div>
        </div>
      </div>
    </div>
</form> -->
<!-- email end here -->
<script language="javascript" type="text/javascript">
    function printDiv(divID) {
        //Get the HTML of div
        var divElements = document.getElementById(divID).innerHTML;
        //Get the HTML of whole page
        var oldPage = document.body.innerHTML;

        //Reset the page's HTML with div's HTML only
        document.body.innerHTML = 
          "<html><head><title></title></head><body>" + 
          divElements + "</body>";

        //Print Page
        window.print();

        //Restore orignal HTML
        document.body.innerHTML = oldPage;
    }
    document.getElementById("uploadBtn").onchange = function() {
        document.getElementById("uploadFile").value = this.value;
    };
    function check_email(email) {
        var status = false;     
        var emailRegEx = /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i;
        if (email.search(emailRegEx) == -1) {
            $("#to_error").html('');
            $("#to_error").html("<?=$this->lang->line('mail_valid')?>").css("text-align", "left").css("color", 'red');
        } else {
            status = true;
        }
        return status;
    }


    // $("#send_pdf").click(function(){
    //     var to = $('#to').val();
    //     var subject = $('#subject').val();
    //     var message = $('#message').val();
    //     var id = "<?=$assignment->assignmentID;?>";
    //     var set = "<?=$set;?>";
    //     var error = 0;

    //     if(to == "" || to == null) {
    //         error++;
    //         $("#to_error").html("");
    //         $("#to_error").html("<?=$this->lang->line('mail_to')?>").css("text-align", "left").css("color", 'red');
    //     } else {
    //         if(check_email(to) == false) {
    //             error++
    //         }
    //     } 

    //     if(subject == "" || subject == null) {
    //         error++;
    //         $("#subject_error").html("");
    //         $("#subject_error").html("<?=$this->lang->line('mail_subject')?>").css("text-align", "left").css("color", 'red');
    //     } else {
    //         $("#subject_error").html("");
    //     }

    //     if(error == 0) {
    //         $.ajax({
    //             type: 'POST',
    //             url: "<?=base_url('assignment/send_mail')?>",
    //             data: 'to='+ to + '&subject=' + subject + "&id=" + id+ "&message=" + message,
    //             dataType: "html",
    //             success: function(data) {
    //                 location.reload();
    //             }
    //         });
    //     }
    // });
</script>

