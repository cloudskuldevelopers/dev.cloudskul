<div class="row">
	<div class="col-md-2 hidden-xs">
		<img src="<?php echo base_url('uploads/images/logo.png'); ?>" style="width: auto; height: 40px; margin-top: -8px;" class="img-thumbnail img-responsive" />
	</div>
	<div class="col-md-7 col-xs-9 text-center">
		<h1 class="page-header"><?php echo $this->session->userdata('school');  ?></h1>
	</div>
	<div class="col-md-3 col-xs-3">
		<!-- begin breadcrumb -->
		<ol class="breadcrumb pull-right">
			<li><a href="<?=base_url("dashboard/index")?>"><i class="fa fa-laptop"></i> <?=$this->lang->line('menu_dashboard')?></a></li>
            <li><a href="<?=base_url("assignment/index")?>"><?=$this->lang->line('menu_assignment')?></a></li>
            <li class="active"><?=$this->lang->line('menu_edit')?> <?=$this->lang->line('panel_title')?></li>
		</ol>
		<!-- end breadcrumb -->
	</div>
</div>
<!-- begin panel -->
<div class="panel panel-inverse" data-sortable-id="ui-widget-3">
	<div class="panel-heading">
		<h4 class="panel-title"><i class="fa fa-pencil"></i> <?=$this->lang->line('panel_title')?></h4>
	</div>
	 <?php if (isset($assignment)): ?>
	<div class="panel-body">
		<div class="col-sm-12">
			<form class="form-horizontal" role="form" method="post">
				<?php 
					if(form_error('title')) 
						echo "<div class='form-group has-error' >";
					else     
						echo "<div class='form-group' >";
				?>
					<label for="title" class="col-sm-1 control-label">
						<?=$this->lang->line("assignment_title")?>
					</label>
					<div class="col-sm-4">
						<input type="text" class="form-control" id="title" name="title" value="<?=set_value('title', $assignment->title)?>" >
					</div>
					<span class="col-sm-4 control-label">
						<?php echo form_error('title'); ?>
					</span>
				</div>

				<?php 
					if(form_error('classesID')) 
						echo "<div class='form-group has-error' >";
					else     
						echo "<div class='form-group' >";
				?>
					<label for="classesID" class="col-sm-1 control-label">
						<?=$this->lang->line("classes")?>
					</label>
					<div class="col-sm-4">
					   <?php
							$array = array(0 => $this->lang->line("select_class"));
							foreach ($classes as $classa) {
								$array[$classa->classesID] = $classa->classes;
							}
							echo form_dropdown("classesID", $array, set_value("classesID", $assignment->classesID), "id='classesID' class='form-control'");
						?>
					</div>
					<span class="col-sm-4 control-label">
						<?php echo form_error('classesID'); ?>
					</span>
				</div>
				
				<?php 
					if(form_error('sectionID')) 
						echo "<div class='form-group has-error' >";
					else     
						echo "<div class='form-group' >";
				?>
					<label for="sectionID" class="col-sm-1 control-label">
						<?=$this->lang->line("section")?>
					</label>
					<div class="col-sm-4">
						<?php
							$array = array();
							$array = array(0 => $this->lang->line("student_select_section"));
							foreach ($sections as $section) {
								$array[$section->sectionID] = $section->section;
							}
							echo form_dropdown("sectionID", $array, set_value("sectionID", $assignment->sectionID), "id='sectionID' class='form-control'");
						?>
					</div>
					<span class="col-sm-3 control-label">
						<?php echo form_error('sectionID'); ?>
					</span>
				</div>
				<?php 
					if(form_error('subjectID')) 
						echo "<div class='form-group has-error' >";
					else     
						echo "<div class='form-group' >";
				?>
					<label for="subjectID" class="col-sm-1 control-label">
						<?=$this->lang->line('subject')?>
					</label>
					<div class="col-sm-4">
						<?php
						$array = array();
						$array = array('0' => 'Select Subject');
							foreach ($subjects as $subject) {
								$array[$subject->subjectID] = $subject->subject;
							}
						
						echo form_dropdown("subjectID", $array, set_value("subjectID", $assignment->subjectID), "id='subjectID' class='form-control'");
						?>
					</div>
				</div>
				<?php 
					if(form_error('end_date')) 
						echo "<div class='form-group has-error' >";
					else     
						echo "<div class='form-group' >";
				?>
					<label for="end_date" class="col-sm-1 control-label">
						<?=$this->lang->line("assignment_end_date")?>
					</label>
					<div class="col-sm-4">
						<input type="text" class="form-control" id="end_date" name="end_date" value="<?=set_value('end_date', date("d-m-Y", strtotime($assignment->end_date)))?>">
					</div>
					<span class="col-sm-4 control-label">
						<?php echo form_error('end_date'); ?>
					</span>
				</div>

				<?php 
					if(form_error('description')) 
						echo "<div class='form-group has-error' >";
					else     
						echo "<div class='form-group' >";
				?>
					<label for="description" class="col-sm-1 control-label">
						<?=$this->lang->line("description")?>
					</label>
					<div class="col-sm-8">
						<textarea class="form-control" id="wysihtml5" name="description" style="height:200px;" ><?=set_value('description', $assignment->description)?></textarea>
					</div>
					<span class="col-sm-3 control-label">
						<?php echo form_error('description'); ?>
					</span>
				</div>

				<div class="form-group">
					<div class="col-sm-offset-1 col-sm-8">
						<input type="submit" class="btn btn-success" value="<?=$this->lang->line("add_class")?>" >
					</div>
				</div>

			</form>

		</div>
	</div>
</div>
<?php endif ?>
	
<script type="text/javascript" src="<?php echo base_url('assets/editor/jquery-te-1.4.0.min.js'); ?>"></script>
<script type="text/javascript">

$('#description').jqte();
$('#classesID').change(function(event) {
    var classesID = $(this).val();
    if(classesID === '0') {
        $('#subjectID').val(0);
    } else {
        $.ajax({
            type: 'POST',
            url: "<?=base_url('student/sectioncall')?>",
            data: "id=" + classesID,
            dataType: "html",
            success: function(data) {
               $('#sectionID').html(data);
            }
        });
        $.ajax({
            type: 'POST',
            url: "<?=base_url('mark/subjectcall')?>",
            data: {"id" : classesID},
            dataType: "html",
            success: function(data) {
               $('#subjectID').html(data);
            }
        });
    }
});
$('#end_date').datepicker();
</script>
