<div class="row">
	<div class="col-md-2 hidden-xs">
		<img src="<?php echo base_url('uploads/images/logo.png'); ?>" style="width: auto; height: 40px; margin-top: -8px;" class="img-thumbnail img-responsive" />
	</div>
	<div class="col-md-7 col-xs-9 text-center">
		<h1 class="page-header"><?php echo $this->session->userdata('school');  ?></h1>
	</div>
	<div class="col-md-3 col-xs-3">
		<!-- begin breadcrumb -->
		<ol class="breadcrumb pull-right">
			<li ><a href="<?php echo base_url('dashboard/index');?>"><i class="fa fa-laptop"></i> Dashboard</a></li>
			<li class="active"><?=$this->lang->line('menu_assignment')?></a></li>
		</ol>
		<!-- end breadcrumb -->
	</div>
</div>

<div id="page-container">
<!-- begin panel -->
<div class="panel panel-inverse" data-sortable-id="ui-widget-3">
	<div class="panel-heading">
		<?php 
			$usertype = $this->session->userdata("usertype");
			if($usertype == "Super Admin" || $usertype == "Admin" || $usertype=="Teacher") {
		?>
		<div class="btn-group-vertical pull-right">
			<a href="<?php echo base_url('assignment/add') ?>" type="button" class="btn btn-success btn-xs"><i class="fa fa-plus"></i> Add Assignment</a>
		</div>
		<h4 class="panel-title"><i class="fa fa-pencil"></i> <?=$this->lang->line('panel_title')?></h4>
	</div>
	<div class="panel-body">
		<div class="col-md-6 add_padding m-b-20">
			<form style="" class="form-horizontal" role="form" method="post">
				<div class="input-group">
					<span class="input-group-addon"><?=$this->lang->line("student_classes")?><i class="fa fa-pencil"></i></span>
					<?php
				$array = array("0" => $this->lang->line("select_class"));
				foreach ($classes as $classa) {
					$array[$classa->classesID] = $classa->classes;
				}
				echo form_dropdown("classesID", $array, set_value("classesID", $set), "id='classesID' class='form-control offset_height'");
			?>
				</div>
			</form>
		</div>
						
		<div class="col-sm-12" >
			<div id="hide-table">
				<table id="example1" class="table table-striped table-bordered table-hover dataTable no-footer">
					<thead>
						<tr>
							<th class="col-sm-1"><?=$this->lang->line('slno')?></th>
							<th class="col-sm-2"><?=$this->lang->line('assignment_title')?></th>
							<th class="col-sm-2"><?=$this->lang->line('assignment_end_date')?></th>
							<th class="col-sm-4"><?=$this->lang->line('assignment_assignment')?></th>
							<th class="col-sm-2"><?=$this->lang->line('action')?></th>
						</tr>
					</thead>
					<tbody>
						<?php if(count($assignments)) {$i = 1; foreach($assignments as $assignment) { ?>
							<tr>
								<td data-title="<?=$this->lang->line('slno')?>">
									<?php echo $i; ?>
								</td>
								<td data-title="<?=$this->lang->line('assignment_title')?>">
									<?php 
										if(strlen($assignment->title) > 25)
											echo strip_tags(substr($assignment->title, 0, 25)."...");
										else 
											echo strip_tags(substr($assignment->title, 0, 25));
									?>
								</td>
								<td data-title="<?=$this->lang->line('assignment_end_date')?>">
									<?php echo date("d M Y", strtotime($assignment->end_date)); ?>
								</td>
								<td data-title="<?=$this->lang->line('assignment_assignment')?>">
									<?php 
										if(strlen($assignment->description) > 60)
											echo strip_tags(substr($assignment->description, 0, 60)."...");
										else 
											echo strip_tags(substr($assignment->description, 0, 60));
									?>
								</td>
								<td data-title="<?=$this->lang->line('action')?>">
									<?php echo btn_view('assignment/view/'.$assignment->assignmentID."/".$set, $this->lang->line('view')) ?>
									<?php echo btn_edit('assignment/edit/'.$assignment->assignmentID."/".$set, $this->lang->line('edit')) ?>
									<?php echo btn_delete('assignment/delete/'.$assignment->assignmentID."/".$set, $this->lang->line('delete')) ?>
								</td>
							</tr>
						<?php $i++; }} ?>
					</tbody>
				</table>
			</div>
			<?php } else {  ?>
			<div id="hide-table">
				<table id="example1" class="table table-striped table-bordered table-hover dataTable no-footer">
					<thead>
						<tr>
							<th class="col-sm-1"><?=$this->lang->line('slno')?></th>
							<th class="col-sm-2"><?=$this->lang->line('assignment_title')?></th>
							<th class="col-sm-2"><?=$this->lang->line('assignment_end_date')?></th>
							<th class="col-sm-4"><?=$this->lang->line('assignment_assignment')?></th>
							<th class="col-sm-2"><?=$this->lang->line('action')?></th>
						</tr>
					</thead>
					<tbody>
						<?php if(count($assignments)) {$i = 1; foreach($assignments as $assignment) { ?>
							<tr>
								<td data-title="<?=$this->lang->line('slno')?>">
									<?php echo $i; ?>
								</td>
								<td data-title="<?=$this->lang->line('assignment_title')?>">
									<?php 
										if(strlen($assignment->title) > 25)
											echo strip_tags(substr($assignment->title, 0, 25)."...");
										else 
											echo strip_tags(substr($assignment->title, 0, 25));
									?>
								</td>
								<td data-title="<?=$this->lang->line('assignment_end_date')?>">
									<?php echo date("d M Y", strtotime($assignment->end_date)); ?>
								</td>
								<td data-title="<?=$this->lang->line('assignment_assignment')?>">
									<?php 
										if(strlen($assignment->description) > 60)
											echo strip_tags(substr($assignment->description, 0, 60)."...");
										else 
											echo strip_tags(substr($assignment->description, 0, 60));
									?>
								</td>
								<td data-title='Action'>
									<?php echo btn_view('assignment/view/'.$assignment->assignmentID."/".$set, $this->lang->line('view')) ?>
								</td>
							</tr>
						<?php $i++; }} ?>
					</tbody>
				</table>
			</div>
			<?php } ?>

		</div>

	</div>
</div>	

<script type="text/javascript">
    $('#classesID').change(function() {
        var classesID = $(this).val();
        if(classesID == 0) {
            $('#hide-table').hide();
        } else {
            $.ajax({
                type: 'POST',
                url: "<?=base_url('assignment/assignment_list')?>",
                data: "id=" + classesID,
                dataType: "html",
                success: function(data) {
                    window.location.href = data;
                }
            });
        }
    });
</script>