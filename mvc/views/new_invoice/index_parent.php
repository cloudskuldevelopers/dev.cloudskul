 <div class="box box-danger">
    <div class="box-header with-border">
        <h3 class="box-title"><i class="fa icon-invoice"></i> <?=$this->lang->line('panel_title')?></h3>

       
        <ol class="breadcrumb">
            <li><a href="<?=base_url("dashboard/index")?>"><i class="fa fa-laptop"></i> <?=$this->lang->line('menu_dashboard')?></a></li>
            <li class="active"><?=$this->lang->line('menu_setfee')?></li>
        </ol>
    </div><!-- /.box-header -->
    <!-- form start -->
    <div class="box-body">
        <div class="row">
            <div class="col-sm-12">

                <div class="col-sm-6 col-sm-offset-3 list-group">
                    <div class="list-group-item list-group-item-warning">
                        <form style="" class="form-horizontal" role="form" method="post">  
                            <div class="form-group">              
                                <label for="studentID" class="col-sm-2 col-sm-offset-2 control-label">
                                    <?=$this->lang->line("setfee_student")?>
                                </label>
                                <div class="col-sm-6">
                                    <?php
                                        $array = array("0" => $this->lang->line("invoice_select_student"));
                                        if($students) {
                                            foreach ($students as $student) {
                                                $array[$student->studentID] = $student->name;
                                            }
                                        }
                                        echo form_dropdown("studentID", $array, set_value("studentID", $set), "id='studentID' class='form-control'");
                                    ?>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>

                <div id="hide-table">
                    <table id="example1" class="table table-striped table-bordered table-hover dataTable no-footer">
                        <thead>
                            <tr>
                                <th><?=$this->lang->line('slno')?></th>
                                <th><?=$this->lang->line('invoice_feetype')?></th>
                                <th><?=$this->lang->line('invoice_date')?></th>
                                <th><?=$this->lang->line('invoice_status')?></th>
                                <th><?=$this->lang->line('payment_plan')?></th>
                                <th><?=$this->lang->line('invoice_student')?></th>
                                <th><?=$this->lang->line('invoice_paymentmethod')?></th>
                                <th><?=$this->lang->line('invoice_amount')?></th>
                                <th><?=$this->lang->line('invoice_due')?></th>
                                <th><?=$this->lang->line('action')?></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php if(count($invoices)) {$i = 1; foreach($invoices as $invoice) { ?>
                                <tr>
                                    <td data-title="<?=$this->lang->line('slno')?>">
                                        <?php echo $i; ?>
                                    </td>
                                    <td data-title="<?=$this->lang->line('invoice_feetype')?>">
                                        <?php echo $invoice->feetype; ?>
                                    </td>

                                    <td data-title="<?=$this->lang->line('invoice_date')?>">
                                        <?php echo $invoice->date; ?>
                                    </td>
       
                                    <td data-title="<?=$this->lang->line('invoice_status')?>">
                                        <?php 

                                            $status = $invoice->status;
                                            $setstatus = '';
                                            if($status == 0) {
                                                $status = $this->lang->line('invoice_notpaid');
                                            } elseif($status == 1) {
                                                $status = $this->lang->line('invoice_partially_paid');
                                            } elseif($status == 2) {
                                                $status = $this->lang->line('invoice_fully_paid');
                                            } elseif($status == 3) {
                                                $status = "RRR Generated";
                                            }

                                            echo "<button class='btn btn-success btn-xs'>".$status."</button>";

                                        ?>
                                    </td>
									<td data-title="<?=$this->lang->line('payment_plan')?>">
                                        <?php 

                                            $plan = $invoice->instalment_plan; 
                                            $msg = '';
                                            if($plan == 1) {
                                                $msg = $this->lang->line('instalment_plan_one');
                                            } elseif($plan == 2) {
                                                $msg = $this->lang->line('instalment_plan_two');
                                            } elseif($plan == 3) {
                                                $msg = $this->lang->line('instalment_plan_three');
                                            }

                                            echo "<button class='btn btn-info btn-xs'>".$msg."</button>";


                                        ?>
                                    </td>

                                    <td data-title="<?=$this->lang->line('invoice_student')?>">
                                        <?php echo $invoice->student; ?>
                                    </td>

                                    <td data-title="<?=$this->lang->line('invoice_paymentmethod')?>">
                                        <?php echo $invoice->paymenttype; ?>
                                    </td>

                                    <td data-title="<?=$this->lang->line('invoice_amount')?>">
                                        <?php echo $setschool->currency_symbol. $invoice->amount; ?>
                                    </td>

                                    <td data-title="<?=$this->lang->line('invoice_due')?>">
                                        <?php echo $setschool->currency_symbol. ($invoice->amount - $invoice->paidamount); ?>
                                    </td>

                                    
                                    <td data-title="<?=$this->lang->line('action')?>">
                                        <?php //echo btn_view('new_invoice/view/' . $invoice->invoiceID, $this->lang->line('view')) ?>
										<a href="#" pay-mode="<?=$invoice->instalment_plan?>" invoice="<?=$invoice->invoiceID?>" class="btn btn-success btn-xs mrg payMode" data-placement="top" data-toggle="tooltip" data-original-title="Click here to Proceed">Proceed</a>
                                    </td>
                                </tr>
                            <?php $i++; }} ?>
                        </tbody>
                    </table>
                </div>

            </div> <!-- col-sm-12 -->
            
        </div><!-- row -->
    </div><!-- Body -->
</div><!-- /.box -->

<!-- Modal Starts Here -->
<div class="modal fade payModal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
                <h4 class="modal-title">Payment Method</h4>
            </div>
            <div class="modal-body">
            <div id="rules">
                <h4>Did you know that you can make the following payment in two ways ? </h4>
                </br>
                <!--ul>
                    <li-->
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="col-sm-6 text-right">
                                <a style="border-radius: 5px !important;" class="btn btn-info payLink" href="<?=base_url('new_invoice/view/')?>">Pay all at once</a>
                            </div>
                            <div class="col-sm-6">
                                <a style="border-radius: 5px !important;" class="btn btn-success payLink2" href="<?=base_url('new_invoice/instalments/')?>">Pay in <span class="paySeq"></span> instalments</a>
                            </div>
                        </div>
                    </div>
                        <!--a style="border-radius: 5px !important;" class="btn text-center btn-info payLink" href="<?=base_url('new_invoice/view/')?>">Pay all at once</a>
                    <!/li>
                </br>
                    <li>
                        <a style="border-radius: 5px !important;" class="btn btn-success payLink" href="<?=base_url('new_invoice/view/')?>">Pay in <span class="paySeq"></span> instalments</a>
                    </li>
                </ul-->

                

            </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                 <a id="ok" href="#"><button type="button" data-dismiss="modal" class="btn btn-default">OK</button> </a>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
        <!-- /.modal-dialog -->
</div>
<script type="text/javascript">
    $(".payMode").click(function(){
        // alert("You clicked me!");
        $(".payModal").modal("show");
        var link = $(".payModal .payLink").attr("href");
        var link2= $(".payModal .payLink2").attr("href");
        var paySeq = $(this).attr("pay-mode");
        var invoiceID = $(this).attr("invoice");
        var newLink = link+"/"+invoiceID;
        var newLink2 = link2+"/"+invoiceID;
        $(".payModal .payLink").attr("href",newLink);
        $(".payModal .payLink2").attr("href",newLink2);
        $(".paySeq").html(paySeq);
        // console.log("here!");
    });

    $('#studentID').change(function() {
        var studentID = $(this).val();
        if(studentID == 0) {
            $('#hide-table').hide();
        } else {
            $.ajax({
                type: 'POST',
                url: "<?=base_url('new_invoice/student_list')?>",
                data: "id=" + studentID,
                dataType: "html",
                success: function(data) {
                    window.location.href = data;
                }
            });
        }
    });
</script>