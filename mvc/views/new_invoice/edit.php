<div id="container">
<!-- TOP BAR -->
<div class="row">
	<div class="col-md-2 hidden-xs">
		<img src="<?php echo base_url('uploads/images/logo.png'); ?>" style="width: auto; height: 40px; margin-top: -8px;" class="img-thumbnail img-responsive" />
	</div>
	<div class="col-md-6 col-xs-9 text-center">
		<h1 class="page-header"><?php echo $this->session->userdata('school');  ?></h1>
	</div>
	<div class="col-md-4 col-xs-3">
		<!-- begin breadcrumb -->
		<ol class="breadcrumb pull-right">
			<li ><a href="<?php echo base_url('dashboard/index');?>"><i class="fa fa-laptop"></i> Dashboard</a></li>
			<li><a href="<?php echo base_url('new_invoice/index');?>">Invoice</a></li>
			<li class="">Edit Invoice</li>
		</ol>
		<!-- end breadcrumb -->
	</div>
</div>
<!-- END TOP BAR -->

<!-- begin panel -->
<div class="panel panel-inverse" data-sortable-id="ui-widget-3">
	<div class="panel-heading">
		<h4 class="panel-title"><i class="fa fa-institution"></i> <?=$this->lang->line('panel_title')?></h4>
	</div>
	<div class="panel-body">
		<div class="col-sm-8 form-horizontal" style="padding-bottom:10px; margin-bottom:10px;">
			<div class='form-group'>
				<label for="classesID" class="col-sm-3 control-label">
					<?=$this->lang->line("installments")?>
				</label>
				<div class="col-sm-6 invoice-installments">

					<label class="radio-inline">
						<input type="radio" name="invoiveRadio" id="one" value="One" checked /> One
					</label>
					<label class="radio-inline">
						<input type="radio" name="invoiveRadio" id="two" value="Two" /> Two
					</label>
					<label class="radio-inline">
						<input type="radio" name="invoiveRadio" id="three" value="Three" /> Three
					</label>
				</div>
			</div>
		</div>
		
		<div class="col-sm-8">
			<form class="form-horizontal" role="form" method="post">
				<?php 
					if(form_error('account')) 
						echo "<div class='form-group has-error' >";
					else     
						echo "<div class='form-group' >";
				?>
					<label for="account" class="col-sm-3 control-label">
						<?=$this->lang->line("invoice_account")?>
					</label>
					<div class="col-sm-6">

						<?php
							$array = array('' => $this->lang->line("invoice_select_account"));
							foreach ($accounts as $account) {
								$array[$account->accountID] = $account->account_name.", ".$account->account_number;
							}
							echo form_dropdown("account", $array, set_value("account", $invoice->accountID), "id='account' class='form-control'");
						?>
					</div>
					<span class="col-sm-3 control-label">
						<?php echo form_error('account'); ?>
					</span>
				</div>
				
				<?php 
					if(form_error('classesID')) 
						echo "<div class='form-group has-error' >";
					else     
						echo "<div class='form-group' >";
				?>
					<label for="classesID" class="col-sm-3 control-label">
						<?=$this->lang->line("invoice_classesID")?>
					</label>
					<div class="col-sm-6">

						<?php
							$array = array('0' => $this->lang->line("invoice_select_classes"));
							foreach ($classes as $classa) {
								$array[$classa->classesID] = $classa->classes;
							}
							echo form_dropdown("classesID", $array, set_value("classesID", $invoice->classesID), "id='classesID' class='form-control'");
						?>
					</div>
					<span class="col-sm-4 control-label">
						<?php echo form_error('classesID'); ?>
					</span>
				</div>

				<?php 
					if(form_error('studentID')) 
						echo "<div class='form-group has-error' >";
					else     
						echo "<div class='form-group' >";
				?>
					<label for="studentID" class="col-sm-3 control-label">
						<?=$this->lang->line("invoice_studentID")?>
					</label>
					<div class="col-sm-6">

						<?php

							$array = $array = array('0' => $this->lang->line("invoice_select_student"));
							if($students != "empty") {
								foreach ($students as $student) {
									$array[$student->studentID] = $student->name;
								}
							}

							$stID = 0;
							if($studentID == 0) {
								$stID = 0;
							} else {
								$stID = $studentID;
							}

							echo form_dropdown("studentID", $array, set_value("studentID", $stID), "id='studentID' class='form-control'");
						?>
					</div>
					<span class="col-sm-4 control-label">
						<?php echo form_error('studentID'); ?>
					</span>
				</div>
				
				<?php 
                        if(form_error('feecat'))
                            echo "<div class='form-group has-error' >";
                        else     
                            echo "<div class='form-group' >";
                    ?>
                        <label for="feetype" class="col-sm-3 control-label">
                            <?=$this->lang->line("fee_cat")?>
                        </label>
                        <div class="col-sm-6">
                            <select name="feecategory" class="form-control invoice_feecategory" id="invoice_feecategory" required>
                                <option value="">Select A Fee Category</option>
                                <?php foreach($feecat as $fee => $val): ?>
                                    <option value="<?php echo $val['code']; ?>"><?php echo $val['category']; ?></option>
                                <?php  endforeach; ?>
                            </select>
                        </div>
                        <span class="col-sm-3 control-label">
                            <?php echo form_error('feecategory'); ?>
                        </span>
                    </div>
				
				<?php 
					if(form_error('feetype'))
						echo "<div class='form-group has-error' >";
					else     
						echo "<div class='form-group' >";
				?>
					<label for="feetype" class="col-sm-3 control-label">
						<?=$this->lang->line("invoice_feetype")?>
					</label>
					<div class="col-sm-6">
						<input type="text" class="form-control" id="feetype" name="feetype" value="<?=set_value('feetype', $invoice->feetype)?>" >
						<div class="book"><ul  class="result"></ul></div>
					</div>
					<span class="col-sm-4 control-label">
						<?php echo form_error('feetype'); ?>
					</span>
				</div>

				<?php 
					if(form_error('amount')) 
						echo "<div class='form-group has-error' >";
					else     
						echo "<div class='form-group' >";
				?>
					<label for="amount" class="col-sm-3 control-label">
						<?=$this->lang->line("invoice_amount")?>
					</label>
					<div class="col-sm-6">
						<input type="text" class="form-control" id="amount" name="amount" value="<?=set_value('amount', $invoice->amount)?>" >
					</div>
					<span class="col-sm-4 control-label">
						<?php echo form_error('amount'); ?>
					</span>
				</div>

				<?php 
					if(form_error('date')) 
						echo "<div class='form-group has-error' >";
					else     
						echo "<div class='form-group' >";
				?>
					<label for="date" class="col-sm-3 control-label">
						<?=$this->lang->line("invoice_date")?>
					</label>
					<div class="col-sm-6">
						<input type="text" class="form-control" id="date" name="date" value="<?=set_value('date', date("d-m-Y", strtotime($invoice->date)))?>" >
					</div>
					<span class="col-sm-4 control-label">
						<?php echo form_error('date'); ?>
					</span>
				</div>

				<div class="form-group">
					<div class="col-sm-offset-4 col-sm-8">
						<input type="submit" class="btn btn-success" value="<?=$this->lang->line("update_invoice")?>" >
					</div>
				</div>

			</form>
		</div>
	</div>
</div>

<!-- begin scroll to top btn -->
<a href="javascript:;" class="btn btn-icon btn-circle btn-success btn-scroll-to-top fade" data-click="scroll-top"><i class="fa fa-angle-up"></i></a>
<!-- end scroll to top btn -->

</div>
<!-- end page container -->

<?php /*
<div class="box">
    <div class="box-header">
        <h3 class="box-title"><i class="fa icon-invoice"></i> <?=$this->lang->line('panel_title')?></h3>

       
        <ol class="breadcrumb">
            <li><a href="<?=base_url("dashboard/index")?>"><i class="fa fa-laptop"></i> <?=$this->lang->line('menu_dashboard')?></a></li>
            <li><a href="<?=base_url("invoice/index")?>"><?=$this->lang->line('menu_invoice')?></a></li>
            <li class="active"><?=$this->lang->line('menu_add')?> <?=$this->lang->line('menu_invoice')?></li>
        </ol>
    </div><!-- /.box-header -->
    <!-- form start -->
    <div class="box-body">
        <div class="row">
            <div class="col-sm-8">
                <form class="form-horizontal" role="form" method="post">

                    <?php 
                        if(form_error('classesID')) 
                            echo "<div class='form-group has-error' >";
                        else     
                            echo "<div class='form-group' >";
                    ?>
                        <label for="classesID" class="col-sm-2 control-label">
                            <?=$this->lang->line("invoice_classesID")?>
                        </label>
                        <div class="col-sm-6">

                            <?php
                                $array = array('0' => $this->lang->line("invoice_select_classes"));
                                foreach ($classes as $classa) {
                                    $array[$classa->classesID] = $classa->classes;
                                }
                                echo form_dropdown("classesID", $array, set_value("classesID", $invoice->classesID), "id='classesID' class='form-control'");
                            ?>
                        </div>
                        <span class="col-sm-4 control-label">
                            <?php echo form_error('classesID'); ?>
                        </span>
                    </div>

                    <?php 
                        if(form_error('studentID')) 
                            echo "<div class='form-group has-error' >";
                        else     
                            echo "<div class='form-group' >";
                    ?>
                        <label for="studentID" class="col-sm-2 control-label">
                            <?=$this->lang->line("invoice_studentID")?>
                        </label>
                        <div class="col-sm-6">

                            <?php

                                $array = $array = array('0' => $this->lang->line("invoice_select_student"));
                                if($students != "empty") {
                                    foreach ($students as $student) {
                                        $array[$student->studentID] = $student->name;
                                    }
                                }

                                $stID = 0;
                                if($studentID == 0) {
                                    $stID = 0;
                                } else {
                                    $stID = $studentID;
                                }

                                echo form_dropdown("studentID", $array, set_value("studentID", $stID), "id='studentID' class='form-control'");
                            ?>
                        </div>
                        <span class="col-sm-4 control-label">
                            <?php echo form_error('studentID'); ?>
                        </span>
                    </div>

                    <?php 
                        if(form_error('feetype'))
                            echo "<div class='form-group has-error' >";
                        else     
                            echo "<div class='form-group' >";
                    ?>
                        <label for="feetype" class="col-sm-2 control-label">
                            <?=$this->lang->line("invoice_feetype")?>
                        </label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" id="feetype" name="feetype" value="<?=set_value('feetype', $invoice->feetype)?>" >
                            <div class="book"><ul  class="result"></ul></div>
                        </div>
                        <span class="col-sm-4 control-label">
                            <?php echo form_error('feetype'); ?>
                        </span>
                    </div>

                    <?php 
                        if(form_error('amount')) 
                            echo "<div class='form-group has-error' >";
                        else     
                            echo "<div class='form-group' >";
                    ?>
                        <label for="amount" class="col-sm-2 control-label">
                            <?=$this->lang->line("invoice_amount")?>
                        </label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" id="amount" name="amount" value="<?=set_value('amount', $invoice->amount)?>" >
                        </div>
                        <span class="col-sm-4 control-label">
                            <?php echo form_error('amount'); ?>
                        </span>
                    </div>

                    <?php 
                        if(form_error('date')) 
                            echo "<div class='form-group has-error' >";
                        else     
                            echo "<div class='form-group' >";
                    ?>
                        <label for="date" class="col-sm-2 control-label">
                            <?=$this->lang->line("invoice_date")?>
                        </label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" id="date" name="date" value="<?=set_value('date', date("d-m-Y", strtotime($invoice->date)))?>" >
                        </div>
                        <span class="col-sm-4 control-label">
                            <?php echo form_error('date'); ?>
                        </span>
                    </div>

                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-8">
                            <input type="submit" class="btn btn-success" value="<?=$this->lang->line("update_invoice")?>" >
                        </div>
                    </div>

                </form>
            </div>
        </div>
    </div>
</div>
*/ ?>
<script type="text/javascript">
$('#classesID').change(function(event) {
    var classesID = $(this).val();
    if(classesID === '0') {
        $('#studentID').val(0);
    } else {
        $.ajax({
            type: 'POST',
            url: "<?=base_url('invoice/call_all_student')?>",
            data: "id=" + classesID,
            dataType: "html",
            success: function(data) {
               $('#studentID').html(data);
            }
        });
    }
});


$('#feetype').keyup(function() {
    var feetype = $(this).val();
    $.ajax({
        type: 'POST',
        url: "<?=base_url('invoice/feetypecall')?>",
        data: "feetype=" + feetype,
        dataType: "html",
        success: function(data) {
            if(data != "") {
                var width = $("#feetype").width();
                $(".book").css('width', width+25 + "px").show();
                $(".result").html(data);

                $('.result li').click(function(){
                    var result_value = $(this).text();
                    $('#feetype').val(result_value);
                    $('.result').html(' ');
                    $('.book').hide();
                });
            } else {
                $(".book").hide();
            }
           
        }
    });
});

$('#date').datepicker();
</script>