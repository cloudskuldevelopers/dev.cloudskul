<style type="text/css">
.doubleUnderline{
	text-decoration:underline;
    border-bottom: 1px solid #000;
    /*border-bottom: double 3px;*/
}
</style>
<script type="text/javascript">
$(document).ready(function(){
	$('a.my_disabled').removeAttr("href")
});

</script>
<div class="box">
    <div class="box-header">
        <h3 class="box-title"><i class="fa icon-invoice"></i> <?=$this->lang->line('panel_title')?></h3>

       
        <ol class="breadcrumb">
            <li><a href="<?=base_url("dashboard/index")?>"><i class="fa fa-laptop"></i> <?=$this->lang->line('menu_dashboard')?></a></li>
            <li><a href="<?=base_url("new_invoice/index")?>"></i><?=$this->lang->line('panel_title')?></a></li>
            <li class="active"><?=$this->lang->line('installment')?></li>
        </ol>
    </div><!-- /.box-header -->
    <!-- form start -->
    <div class="box-body">
        <div class="row">
            <div class="col-sm-12">
                <div id="hide-table">
                	<!-- This table Display the Payment Summary -->
                	<h5>Payment Summary</h5>
                	 <table class="table table-hover table-responsive">
			            <thead>
			                <tr>
			                    <th class="col-lg-1"><?=$this->lang->line('slno')?></th>
			                    <th class="col-lg-4"><?=$this->lang->line('invoice_feetype')?></th>
			                    <th class="col-lg-2"><?=$this->lang->line('invoice_total')?> Amount</th>
			                </tr>
			            </thead>
			            <tbody>
			                <tr>
			                    <td data-title="<?=$this->lang->line('slno')?>">
			                        <?php echo 1; ?>
			                    </td>
			                    <td data-title="<?=$this->lang->line('invoice_feetype')?>">
			                        <?php echo $invoice->feetype; ?>
			                    </td>
			                    <td data-title="<?=$this->lang->line('invoice_total')?>">
	                                <span class="doubleUnderline"><?php echo $invoice->amount; ?></span>
			                    </td>
			                </tr>
			            </tbody>
			        </table>
			    	</br>
			    	<h5>Payment Breakdown</h5>
			    	</br>
                	<h6><u>Please Note: You can only make payment for a single instalment at any given time</u></h6>
			    	</br><?php $clickable=1;//This number makes sure that only the first on the list is clickable ?>
					<?php foreach ($invoice_details as $key) {?>
					<div style="margin-bottom:5px;" class="row">
						<div class="col">
							<div class="col-sm-6"> <u><?=print_instalment($key->pay_seq)?> Instalment</u>  Amount: <strong><?="N".$key->amount?></strong> Status: <?=$key->status=="0"?"Not Paid":"Paid"; ?></div> 
							<div class="col-sm-6"><a style="border-radius: 5px !important; background-color: #5cb85c;border-color: #4cae4c;" <?=$clickable!=1?"disabled ":""?> class="btn btn-success payLink <?=$clickable!=1?'my_disabled':''?>" href="<?=base_url('new_invoice/view/'.$key->invoiceID.'/'.$key->pay_seq)?>">Pay </a></div>
						</div>
					</div>
					<?php $clickable++; } ?>
                </div>

            </div> <!-- col-sm-12 -->
            
        </div><!-- row -->
    </div><!-- Body -->
</div><!-- /.box -->
<script type="text/javascript">

    $('#studentID').change(function() {
        var studentID = $(this).val();
        if(studentID == 0) {
            $('#hide-table').hide();
        } else {
            $.ajax({
                type: 'POST',
                url: "<?=base_url('invoice/student_list')?>",
                data: "id=" + studentID,
                dataType: "html",
                success: function(data) {
                    window.location.href = data;
                }
            });
        }
    });
	
		//code for calculating total installmental payment
		function calculateTotalInstallmentalPayment(){
			var firstInstallment  = $("#firstInstallment").val();
			var secondInstallment  = $("#secondInstallment").val();
			var thirdInstallment  = $("#thirdInstallment").val();
			
			if($("#firstInstallment").prop("checked") != true){
				firstInstallment = 0;
			}
			if($("#secondInstallment").prop("checked") != true){
				secondInstallment = 0;
			}
			if($("#thirdInstallment").prop("checked") != true){
				thirdInstallment = 0;
			}
			
			var totalInstallment = Number(firstInstallment) + Number(secondInstallment) + Number(thirdInstallment);
			
			$('#totalInstallment').html(totalInstallment.toLocaleString() + '.00');
		}
		
		$('#firstInstallment').click(function(){
			//if($(this).prop("checked") == true){
				calculateTotalInstallmentalPayment();
			//}
		});
		
		$('#secondInstallment').click(function(){
			//if($(this).prop("checked") == true){
				calculateTotalInstallmentalPayment();
			//}
		});
		
		$('#thirdInstallment').click(function(){
			//if($(this).prop("checked") == true){
				calculateTotalInstallmentalPayment();
			//}
		});
	
	
</script>