<div id="container">
<!-- TOP BAR -->
<div class="row">
	<div class="col-md-2 hidden-xs">
		<img src="<?php echo base_url('uploads/images/logo.png'); ?>" style="width: auto; height: 40px; margin-top: -8px;" class="img-thumbnail img-responsive" />
	</div>
	<div class="col-md-6 col-xs-9 text-center">
		<h1 class="page-header"><?php echo $this->session->userdata('school');  ?></h1>
	</div>
	<div class="col-md-4 col-xs-3">
		<!-- begin breadcrumb -->
		<ol class="breadcrumb pull-right">
			<li ><a href="<?php echo base_url('dashboard/index');?>"><i class="fa fa-laptop"></i> Dashboard</a></li>
			<li><a href="<?php echo base_url('new_invoice/index');?>">Invoice</a></li>
			<li class="">Add Invoice</li>
		</ol>
		<!-- end breadcrumb -->
	</div>
</div>
<!-- END TOP BAR -->

<!-- begin panel -->
<div class="panel panel-inverse" data-sortable-id="ui-widget-3">
	<div class="panel-heading">
		<h4 class="panel-title"><i class="fa icon-invoice"></i> <?=$this->lang->line('panel_title')?></h4>
	</div>
	<div class="panel-body">
		<h3 class="profileBody" style="font-weight: lighter; font-size: 22px; margin-bottom: 30px;">Create New Invoice</h3>
		<div class="row">
            <div class="col-sm-8 form-horizontal" style="padding-bottom:10px; margin-bottom:10px;">
                <div class='form-group'>
                    <label for="classesID" class="col-sm-3 control-label">
                        <?=$this->lang->line("installments")?>
                    </label>
                    <div class="col-sm-6 invoice-installments">

                        <label class="radio-inline">
                            <input type="radio" name="invoiveRadio" id="one" value="One" checked /> One
                        </label>
                        <label class="radio-inline">
                            <input type="radio" name="invoiveRadio" id="two" value="Two" /> Two
                        </label>
                        <label class="radio-inline">
                            <input type="radio" name="invoiveRadio" id="three" value="Three" /> Three
                        </label>
                    </div>
                </div>
            </div>

            <!-- INSTALMENT ONE -->
            <div class="col-sm-8 one-form">
                <form class="form-horizontal" role="form" method="post" action="<?php  echo base_url() . 'new_invoice/add/1'; ?>">

                    <?php 
                        if(form_error('account')) 
                            echo "<div class='form-group has-error' >";
                        else     
                            echo "<div class='form-group' >";
                    ?>
                        <label for="account" class="col-sm-3 control-label">
                            <?=$this->lang->line("invoice_account")?>
                        </label>
                        <div class="col-sm-6">

                            <?php
                                $array = array('' => $this->lang->line("invoice_select_account"));
                                foreach ($accounts as $account) {
                                    $array[$account->accountID] = $account->account_name.", ".$account->account_number;
                                }
                                echo form_dropdown("account", $array, set_value("account"), "id='account' class='form-control'");
                            ?>
                        </div>
                        <span class="col-sm-3 control-label">
                            <?php echo form_error('account'); ?>
                        </span>
                    </div>

                    <?php 
                        if(form_error('classesID')) 
                            echo "<div class='form-group has-error' >";
                        else     
                            echo "<div class='form-group' >";
                    ?>
                        <label for="classesID" class="col-sm-3 control-label">
                            <?=$this->lang->line("invoice_classesID")?>
                        </label>
                        <div class="col-sm-6">

                            <?php
                                $array = array('0' => $this->lang->line("invoice_select_classes"));
                                foreach ($classes as $classa) {
                                    $array[$classa->classesID] = $classa->classes;
                                }
                                echo form_dropdown("classesID", $array, set_value("classesID"), "id='classesID' class='form-control'");
                            ?>
                        </div>
                        <span class="col-sm-3 control-label">
                            <?php echo form_error('classesID'); ?>
                        </span>
                    </div>

                    <?php 
                        if(form_error('studentID')) 
                            echo "<div class='form-group has-error' >";
                        else     
                            echo "<div class='form-group' >";
                    ?>
                        <label for="studentID" class="col-sm-3 control-label">
                            <?=$this->lang->line("invoice_studentID")?>
                        </label>
                        <div class="col-sm-6">

                            <?php

                                $array = $array = array('0' => $this->lang->line("invoice_select_student"));
                                if($students != "empty") {
                                    foreach ($students as $student) {
                                        $array[$student->studentID] = $student->name;
                                    }
                                }

                                $stID = 0;
                                if($studentID == 0) {
                                    $stID = 0;
                                } else {
                                    $stID = $studentID;
                                }

                                echo form_dropdown("studentID", $array, set_value("studentID", $stID), "id='studentID' class='form-control'");
                            ?>
                        </div>
                        <span class="col-sm-3 control-label">
                            <?php echo form_error('studentID'); ?>
                        </span>
                    </div>

                    <?php 
                        if(form_error('feecat'))
                            echo "<div class='form-group has-error' >";
                        else     
                            echo "<div class='form-group' >";
                    ?>
                        <label for="feetype" class="col-sm-3 control-label">
                            <?=$this->lang->line("fee_cat")?>
                        </label>
                        <div class="col-sm-6">
                            <select name="feecategory" class="form-control invoice_feecategory" id="invoice_feecategory" required>
                                <option value="">Select A Fee Category</option>
                                <?php foreach($feecat as $fee => $val): ?>
                                    <option value="<?php echo $val['code']; ?>"><?php echo $val['category']; ?></option>
                                <?php  endforeach; ?>
                            </select>
                        </div>
                        <span class="col-sm-3 control-label">
                            <?php echo form_error('feecategory'); ?>
                        </span>
                    </div>

                    <?php 
                        if(form_error('feetype'))
                            echo "<div class='form-group has-error' >";
                        else     
                            echo "<div class='form-group' >";
                    ?>
                        <label for="feetype" class="col-sm-3 control-label">
                            <?=$this->lang->line("invoice_feetype")?>
                        </label>
                        <div class="col-sm-6">
                            <select name="feetype" class="form-control invoice_feetype" id="invoice_feetype" required>
                                <option value="">Select A Fee Type</option>
                                
                            </select>
                            <!--input type="text" class="form-control" id="feetype" name="feetype" value="<?=set_value('feetype')?>" >
                            <div class="book"><ul  class="result"></ul></div-->
                        </div>
                        <span class="col-sm-3 control-label">
                            <?php echo form_error('feetype'); ?>
                        </span>
                    </div>
                    
					<?php 
                        if(form_error('amount')) 
                            echo "<div class='form-group has-error' >";
                        else     
                            echo "<div class='form-group' >";
                    ?>
                        <label for="amount" class="col-sm-3 control-label">
                            Amount
                        </label>
                        <div class="col-sm-6">
                            <input type="number" class="form-control" id="amount" name="amount" required value="<?=set_value('amount')?>" />
                        </div>
                        <span class="col-sm-3 control-label">
                            <?php echo form_error('amount'); ?>
                        </span>
                    </div>
                    
                    <?php 
                        if(form_error('date')) 
                            echo "<div class='form-group has-error' >";
                        else     
                            echo "<div class='form-group' >";
                    ?>
                        <label for="date" class="col-sm-3 control-label">
                            <?=$this->lang->line("invoice_date")?>
                        </label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" id="date" name="date" value="<?=set_value('date')?>" >
                        </div>
                        <span class="col-sm-3 control-label">
                            <?php echo form_error('date'); ?>
                        </span>
                    </div>

                     

                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-8">
                            <input type="submit" class="btn btn-success" value="<?=$this->lang->line("add_invoice")?>" >
                        </div>
                    </div>
                    <input id="instVal1" name="instVal" type="hidden" value="" />
                </form>
            </div>


            <!-- INSTALMENT TWO -->
            <div class="col-sm-8 two-form" style="display:none;">
                <form class="form-horizontal" role="form" method="post" action="<?php  echo base_url() . 'new_invoice/add/2'; ?>">

                    <?php 
                        if(form_error('account')) 
                            echo "<div class='form-group has-error' >";
                        else     
                            echo "<div class='form-group' >";
                    ?>
                        <label for="account" class="col-sm-3 control-label">
                            <?=$this->lang->line("invoice_account")?>
                        </label>
                        <div class="col-sm-6">

                            <?php
                                $array = array('' => $this->lang->line("invoice_select_account"));
                                foreach ($accounts as $account) {
                                    $array[$account->accountID] = $account->account_name;
                                }
                                echo form_dropdown("account", $array, set_value("account"), "id='account' class='form-control'");
                            ?>
                        </div>
                        <span class="col-sm-3 control-label">
                            <?php echo form_error('account'); ?>
                        </span>
                    </div>

                    <?php 
                        if(form_error('classesID')) 
                            echo "<div class='form-group has-error' >";
                        else     
                            echo "<div class='form-group' >";
                    ?>
                        <label for="classesID" class="col-sm-3 control-label">
                            <?=$this->lang->line("invoice_classesID")?>
                        </label>
                        <div class="col-sm-6">

                            <?php
                                $array = array('0' => $this->lang->line("invoice_select_classes"));
                                foreach ($classes as $classa) {
                                    $array[$classa->classesID] = $classa->classes;
                                }
                                echo form_dropdown("classesID", $array, set_value("classesID"), "id='classesID2' class='form-control'");
                            ?>
                        </div>
                        <span class="col-sm-3 control-label">
                            <?php echo form_error('classesID'); ?>
                        </span>
                    </div>

                    <?php 
                        if(form_error('studentID')) 
                            echo "<div class='form-group has-error' >";
                        else     
                            echo "<div class='form-group' >";
                    ?>
                        <label for="studentID" class="col-sm-3 control-label">
                            <?=$this->lang->line("invoice_studentID")?>
                        </label>
                        <div class="col-sm-6">

                            <?php

                                $array = $array = array('0' => $this->lang->line("invoice_select_student"));
                                if($students != "empty") {
                                    foreach ($students as $student) {
                                        $array[$student->studentID] = $student->name;
                                    }
                                }

                                $stID = 0;
                                if($studentID == 0) {
                                    $stID = 0;
                                } else {
                                    $stID = $studentID;
                                }

                                echo form_dropdown("studentID", $array, set_value("studentID", $stID), "id='studentID2' class='form-control'");
                            ?>
                        </div>
                        <span class="col-sm-3 control-label">
                            <?php echo form_error('studentID'); ?>
                        </span>
                    </div>

                    <?php 
                        if(form_error('feecat'))
                            echo "<div class='form-group has-error' >";
                        else     
                            echo "<div class='form-group' >";
                    ?>
                        <label for="feetype" class="col-sm-3 control-label">
                            <?=$this->lang->line("fee_cat")?>
                        </label>
                        <div class="col-sm-6">
                            <select name="feecategory" class="form-control invoice_feecategory" id="invoice_feecategory2" required>
                                <option value="">Select A Fee Category</option>
                                <?php foreach($feecat as $fee => $val): ?>
                                    <option value="<?php echo $val['code']; ?>"><?php echo $val['category']; ?></option>
                                <?php  endforeach; ?>
                            </select>
                        </div>
                        <span class="col-sm-3 control-label">
                            <?php echo form_error('feecategory'); ?>
                        </span>
                    </div>

                    <?php 
                        if(form_error('feetype'))
                            echo "<div class='form-group has-error' >";
                        else     
                            echo "<div class='form-group' >";
                    ?>
                        <label for="feetype" class="col-sm-3 control-label">
                            <?=$this->lang->line("invoice_feetype")?>
                        </label>
                        <div class="col-sm-6">
                            <select name="feetype" class="form-control invoice_feetype" id="invoice_feetype2" required>
                                <option value="">Select A Fee Type</option>
                                
                            </select>
                            <!--input type="text" class="form-control" id="feetype" name="feetype" value="<?=set_value('feetype')?>" >
                            <div class="book"><ul  class="result"></ul></div-->
                        </div>
                        <span class="col-sm-3 control-label">
                            <?php echo form_error('feetype'); ?>
                        </span>
                    </div>

                    <?php 
                        if(form_error('date')) 
                            echo "<div class='form-group has-error' >";
                        else     
                            echo "<div class='form-group' >";
                    ?>
                        <label for="date" class="col-sm-3 control-label">
                            <?=$this->lang->line("invoice_date")?>
                        </label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" id="date2" name="date" value="<?=set_value('date')?>" >
                        </div>
                        <span class="col-sm-3 control-label">
                            <?php echo form_error('date'); ?>
                        </span>
                    </div>
					
					 <?php 
                        if(form_error('amount')) 
                            echo "<div class='form-group has-error' >";
                        else     
                            echo "<div class='form-group' >";
                    ?>
                        <label for="amount" class="col-sm-3 control-label">
                            Amount
                        </label>
                        <div class="col-sm-6">
                            <input type="number" class="form-control" id="amount" name="amount" required value="<?=set_value('amount')?>" />
                        </div>
                        <span class="col-sm-3 control-label">
                            <?php echo form_error('amount'); ?>
                        </span>
                    </div>

                    <div class='form-group'>
                        <label for="amount" class="col-sm-3 control-label">
                            1st Installment
                        </label>
                        <div class="col-sm-6">
                            <input type="number" class="form-control" id="amount" name="amount1" required value="<?=set_value('amount1')?>" />
                        </div>
                    </div>
                    <div class='form-group'>
                        <label for="amount" class="col-sm-3 control-label">
                            2nd Installment
                        </label>
                        <div class="col-sm-6">
                            <input type="number" class="form-control" id="amount" name="amount2" required value="<?=set_value('amount2')?>" />
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-8">
                            <input type="submit" class="btn btn-success" value="<?=$this->lang->line("add_invoice")?>" >
                        </div>
                    </div>
                    <input id="instVal2" name="instVal" type="hidden" value="" />
                </form>
            </div>


            <!-- INSTALMENT THREE -->
            <div class="col-sm-8 three-form" style="display:none;">
                <form class="form-horizontal" role="form" method="post" action="<?php  echo base_url() . 'new_invoice/add/3'; ?>">

                    <?php 
                        if(form_error('account')) 
                            echo "<div class='form-group has-error' >";
                        else     
                            echo "<div class='form-group' >";
                    ?>
                        <label for="account" class="col-sm-3 control-label">
                            <?=$this->lang->line("invoice_account")?>
                        </label>
                        <div class="col-sm-6">

                            <?php
                                $array = array('' => $this->lang->line("invoice_select_account"));
                                foreach ($accounts as $account) {
                                    $array[$account->accountID] = $account->account_name;
                                }
                                echo form_dropdown("account", $array, set_value("account"), "id='account' class='form-control'");
                            ?>
                        </div>
                        <span class="col-sm-3 control-label">
                            <?php echo form_error('account'); ?>
                        </span>
                    </div>

                    <?php 
                        if(form_error('classesID')) 
                            echo "<div class='form-group has-error' >";
                        else     
                            echo "<div class='form-group' >";
                    ?>
                        <label for="classesID" class="col-sm-3 control-label">
                            <?=$this->lang->line("invoice_classesID")?>
                        </label>
                        <div class="col-sm-6">

                            <?php
                                $array = array('0' => $this->lang->line("invoice_select_classes"));
                                foreach ($classes as $classa) {
                                    $array[$classa->classesID] = $classa->classes;
                                }
                                echo form_dropdown("classesID", $array, set_value("classesID"), "id='classesID3' class='form-control'");
                            ?>
                        </div>
                        <span class="col-sm-3 control-label">
                            <?php echo form_error('classesID'); ?>
                        </span>
                    </div>

                    <?php 
                        if(form_error('studentID')) 
                            echo "<div class='form-group has-error' >";
                        else     
                            echo "<div class='form-group' >";
                    ?>
                        <label for="studentID" class="col-sm-3 control-label">
                            <?=$this->lang->line("invoice_studentID")?>
                        </label>
                        <div class="col-sm-6">

                            <?php

                                $array = $array = array('0' => $this->lang->line("invoice_select_student"));
                                if($students != "empty") {
                                    foreach ($students as $student) {
                                        $array[$student->studentID] = $student->name;
                                    }
                                }

                                $stID = 0;
                                if($studentID == 0) {
                                    $stID = 0;
                                } else {
                                    $stID = $studentID;
                                }

                                echo form_dropdown("studentID", $array, set_value("studentID", $stID), "id='studentID3' class='form-control'");
                            ?>
                        </div>
                        <span class="col-sm-3 control-label">
                            <?php echo form_error('studentID'); ?>
                        </span>
                    </div>

                    <?php 
                        if(form_error('feecat'))
                            echo "<div class='form-group has-error' >";
                        else     
                            echo "<div class='form-group' >";
                    ?>
                        <label for="feetype" class="col-sm-3 control-label">
                            <?=$this->lang->line("fee_cat")?>
                        </label>
                        <div class="col-sm-6">
                            <select name="feecategory" class="form-control" id="invoice_feecategory3" required>
                                <option value="">Select A Fee Category</option>
                                <?php foreach($feecat as $fee => $val): ?>
                                    <option value="<?php echo $val['code']; ?>"><?php echo $val['category']; ?></option>
                                <?php  endforeach; ?>
                            </select>
                        </div>
                        <span class="col-sm-3 control-label">
                            <?php echo form_error('feecategory'); ?>
                        </span>
                    </div>

                    <?php 
                        if(form_error('feetype'))
                            echo "<div class='form-group has-error' >";
                        else     
                            echo "<div class='form-group' >";
                    ?>
                        <label for="feetype" class="col-sm-3 control-label">
                            <?=$this->lang->line("invoice_feetype")?>
                        </label>
                        <div class="col-sm-6">
                            <select name="feetype" class="form-control invoice_feetype" id="invoice_feetype3" required>
                                <option value="">Select A Fee Type</option>
                                
                            </select>
                            <!--input type="text" class="form-control" id="feetype" name="feetype" value="<?=set_value('feetype')?>" >
                            <div class="book"><ul  class="result"></ul></div-->
                        </div>
                        <span class="col-sm-3 control-label">
                            <?php echo form_error('feetype'); ?>
                        </span>
                    </div>

                    <?php 
                        if(form_error('date')) 
                            echo "<div class='form-group has-error' >";
                        else     
                            echo "<div class='form-group' >";
                    ?>
                        <label for="date" class="col-sm-3 control-label">
                            <?=$this->lang->line("invoice_date")?>
                        </label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" id="date3" name="date" value="<?=set_value('date')?>" >
                        </div>
                        <span class="col-sm-3 control-label">
                            <?php echo form_error('date'); ?>
                        </span>
                    </div>
					
					 <?php 
                        if(form_error('amount')) 
                            echo "<div class='form-group has-error' >";
                        else     
                            echo "<div class='form-group' >";
                    ?>
                        <label for="amount" class="col-sm-3 control-label">
                            Amount
                        </label>
                        <div class="col-sm-6">
                            <input type="number" class="form-control" id="amount" name="amount" required value="<?=set_value('amount')?>" />
                        </div>
                        <span class="col-sm-3 control-label">
                            <?php echo form_error('amount'); ?>
                        </span>
                    </div>

                    <div class='form-group'>
                        <label for="amount" class="col-sm-3 control-label">
                            1st Installment
                        </label>
                        <div class="col-sm-6">
                            <input type="number" class="form-control" id="amount" name="amount1"  value="<?=set_value('amount1')?>" />
                        </div>
                    </div>
                    <div class='form-group'>
                        <label for="amount" class="col-sm-3 control-label">
                            2nd Installment
                        </label>
                        <div class="col-sm-6">
                            <input type="number" class="form-control" id="amount" name="amount2"  value="<?=set_value('amount2')?>" />
                        </div>
                    </div>
                    <div class='form-group'>
                        <label for="amount" class="col-sm-3 control-label">
                            3rd Installment
                        </label>
                        <div class="col-sm-6">
                            <input type="number" class="form-control" id="amount" name="amount3"  value="<?=set_value('amount3')?>" />
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-8">
                            <input type="submit" class="btn btn-success" value="<?=$this->lang->line("add_invoice")?>" >
                        </div>
                    </div>
                    <input id="instVal3" name="instVal" type="hidden" value="" />
                </form>
            </div>

        </div>
	</div>
</div>
<!-- end panel -->

<!-- begin scroll to top btn -->
<a href="javascript:;" class="btn btn-icon btn-circle btn-success btn-scroll-to-top fade" data-click="scroll-top"><i class="fa fa-angle-up"></i></a>
<!-- end scroll to top btn -->

</div>
<!-- end page container -->

<?php /*
<div class="box">
    <div class="box-header">
        <h3 class="box-title"><i class="fa icon-invoice"></i> <?=$this->lang->line('panel_title')?></h3>

       
        <ol class="breadcrumb">
            <li><a href="<?=base_url("dashboard/index")?>"><i class="fa fa-laptop"></i> <?=$this->lang->line('menu_dashboard')?></a></li>
            <li><a href="<?=base_url("new_invoice/index")?>"><?=$this->lang->line('menu_invoice')?></a></li>
            <li class="active"><?=$this->lang->line('menu_add')?> <?=$this->lang->line('menu_invoice')?></li>
        </ol>
    </div><!-- /.box-header -->
    <!-- form start -->
    <div class="box-body">
        <div class="row">
            <div class="col-sm-8 form-horizontal" style="padding-bottom:10px; margin-bottom:10px;">
                <div class='form-group'>
                    <label for="classesID" class="col-sm-3 control-label">
                        <?=$this->lang->line("installments")?>
                    </label>
                    <div class="col-sm-6 invoice-installments">

                        <label class="radio-inline">
                            <input type="radio" name="invoiveRadio" id="one" value="One" checked /> One
                        </label>
                        <label class="radio-inline">
                            <input type="radio" name="invoiveRadio" id="two" value="Two" /> Two
                        </label>
                        <label class="radio-inline">
                            <input type="radio" name="invoiveRadio" id="three" value="Three" /> Three
                        </label>
                    </div>
                </div>
            </div>

            <!-- INSTALMENT ONE -->
            <div class="col-sm-8 one-form">
                <form class="form-horizontal" role="form" method="post" action="<?php  echo base_url() . 'new_invoice/add/1'; ?>">

                    <?php 
                        if(form_error('account')) 
                            echo "<div class='form-group has-error' >";
                        else     
                            echo "<div class='form-group' >";
                    ?>
                        <label for="account" class="col-sm-3 control-label">
                            <?=$this->lang->line("invoice_account")?>
                        </label>
                        <div class="col-sm-6">

                            <?php
                                $array = array('' => $this->lang->line("invoice_select_account"));
                                foreach ($accounts as $account) {
                                    $array[$account->accountID] = $account->account_name.", ".$account->account_number;
                                }
                                echo form_dropdown("account", $array, set_value("account"), "id='account' class='form-control'");
                            ?>
                        </div>
                        <span class="col-sm-3 control-label">
                            <?php echo form_error('account'); ?>
                        </span>
                    </div>

                    <?php 
                        if(form_error('classesID')) 
                            echo "<div class='form-group has-error' >";
                        else     
                            echo "<div class='form-group' >";
                    ?>
                        <label for="classesID" class="col-sm-3 control-label">
                            <?=$this->lang->line("invoice_classesID")?>
                        </label>
                        <div class="col-sm-6">

                            <?php
                                $array = array('0' => $this->lang->line("invoice_select_classes"));
                                foreach ($classes as $classa) {
                                    $array[$classa->classesID] = $classa->classes;
                                }
                                echo form_dropdown("classesID", $array, set_value("classesID"), "id='classesID' class='form-control'");
                            ?>
                        </div>
                        <span class="col-sm-3 control-label">
                            <?php echo form_error('classesID'); ?>
                        </span>
                    </div>

                    <?php 
                        if(form_error('studentID')) 
                            echo "<div class='form-group has-error' >";
                        else     
                            echo "<div class='form-group' >";
                    ?>
                        <label for="studentID" class="col-sm-3 control-label">
                            <?=$this->lang->line("invoice_studentID")?>
                        </label>
                        <div class="col-sm-6">

                            <?php

                                $array = $array = array('0' => $this->lang->line("invoice_select_student"));
                                if($students != "empty") {
                                    foreach ($students as $student) {
                                        $array[$student->studentID] = $student->name;
                                    }
                                }

                                $stID = 0;
                                if($studentID == 0) {
                                    $stID = 0;
                                } else {
                                    $stID = $studentID;
                                }

                                echo form_dropdown("studentID", $array, set_value("studentID", $stID), "id='studentID' class='form-control'");
                            ?>
                        </div>
                        <span class="col-sm-3 control-label">
                            <?php echo form_error('studentID'); ?>
                        </span>
                    </div>

                    <?php 
                        if(form_error('feecat'))
                            echo "<div class='form-group has-error' >";
                        else     
                            echo "<div class='form-group' >";
                    ?>
                        <label for="feetype" class="col-sm-3 control-label">
                            <?=$this->lang->line("fee_cat")?>
                        </label>
                        <div class="col-sm-6">
                            <select name="feecategory" class="form-control invoice_feecategory" id="invoice_feecategory" required>
                                <option value="">Select A Fee Category</option>
                                <?php foreach($feecat as $fee => $val): ?>
                                    <option value="<?php echo $val['code']; ?>"><?php echo $val['category']; ?></option>
                                <?php  endforeach; ?>
                            </select>
                        </div>
                        <span class="col-sm-3 control-label">
                            <?php echo form_error('feecategory'); ?>
                        </span>
                    </div>

                    <?php 
                        if(form_error('feetype'))
                            echo "<div class='form-group has-error' >";
                        else     
                            echo "<div class='form-group' >";
                    ?>
                        <label for="feetype" class="col-sm-3 control-label">
                            <?=$this->lang->line("invoice_feetype")?>
                        </label>
                        <div class="col-sm-6">
                            <select name="feetype" class="form-control invoice_feetype" id="invoice_feetype" required>
                                <option value="">Select A Fee Type</option>
                                
                            </select>
                            <!--input type="text" class="form-control" id="feetype" name="feetype" value="<?=set_value('feetype')?>" >
                            <div class="book"><ul  class="result"></ul></div-->
                        </div>
                        <span class="col-sm-3 control-label">
                            <?php echo form_error('feetype'); ?>
                        </span>
                    </div>

                    <?php 
                        if(form_error('date')) 
                            echo "<div class='form-group has-error' >";
                        else     
                            echo "<div class='form-group' >";
                    ?>
                        <label for="date" class="col-sm-3 control-label">
                            <?=$this->lang->line("invoice_date")?>
                        </label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" id="date" name="date" value="<?=set_value('date')?>" >
                        </div>
                        <span class="col-sm-3 control-label">
                            <?php echo form_error('date'); ?>
                        </span>
                    </div>

                     <?php 
                        if(form_error('amount')) 
                            echo "<div class='form-group has-error' >";
                        else     
                            echo "<div class='form-group' >";
                    ?>
                        <label for="amount" class="col-sm-3 control-label">
                            Amount
                        </label>
                        <div class="col-sm-6">
                            <input type="number" class="form-control" id="amount" name="amount" required value="<?=set_value('amount')?>" />
                        </div>
                        <span class="col-sm-3 control-label">
                            <?php echo form_error('amount'); ?>
                        </span>
                    </div>

                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-8">
                            <input type="submit" class="btn btn-success" value="<?=$this->lang->line("add_invoice")?>" >
                        </div>
                    </div>
                    <input id="instVal1" name="instVal" type="hidden" value="" />
                </form>
            </div>


            <!-- INSTALMENT TWO -->
            <div class="col-sm-8 two-form" style="display:none;">
                <form class="form-horizontal" role="form" method="post" action="<?php  echo base_url() . 'new_invoice/add/2'; ?>">

                    <?php 
                        if(form_error('account')) 
                            echo "<div class='form-group has-error' >";
                        else     
                            echo "<div class='form-group' >";
                    ?>
                        <label for="account" class="col-sm-3 control-label">
                            <?=$this->lang->line("invoice_account")?>
                        </label>
                        <div class="col-sm-6">

                            <?php
                                $array = array('' => $this->lang->line("invoice_select_account"));
                                foreach ($accounts as $account) {
                                    $array[$account->accountID] = $account->account_name;
                                }
                                echo form_dropdown("account", $array, set_value("account"), "id='account' class='form-control'");
                            ?>
                        </div>
                        <span class="col-sm-3 control-label">
                            <?php echo form_error('account'); ?>
                        </span>
                    </div>

                    <?php 
                        if(form_error('classesID')) 
                            echo "<div class='form-group has-error' >";
                        else     
                            echo "<div class='form-group' >";
                    ?>
                        <label for="classesID" class="col-sm-3 control-label">
                            <?=$this->lang->line("invoice_classesID")?>
                        </label>
                        <div class="col-sm-6">

                            <?php
                                $array = array('0' => $this->lang->line("invoice_select_classes"));
                                foreach ($classes as $classa) {
                                    $array[$classa->classesID] = $classa->classes;
                                }
                                echo form_dropdown("classesID", $array, set_value("classesID"), "id='classesID2' class='form-control'");
                            ?>
                        </div>
                        <span class="col-sm-3 control-label">
                            <?php echo form_error('classesID'); ?>
                        </span>
                    </div>

                    <?php 
                        if(form_error('studentID')) 
                            echo "<div class='form-group has-error' >";
                        else     
                            echo "<div class='form-group' >";
                    ?>
                        <label for="studentID" class="col-sm-3 control-label">
                            <?=$this->lang->line("invoice_studentID")?>
                        </label>
                        <div class="col-sm-6">

                            <?php

                                $array = $array = array('0' => $this->lang->line("invoice_select_student"));
                                if($students != "empty") {
                                    foreach ($students as $student) {
                                        $array[$student->studentID] = $student->name;
                                    }
                                }

                                $stID = 0;
                                if($studentID == 0) {
                                    $stID = 0;
                                } else {
                                    $stID = $studentID;
                                }

                                echo form_dropdown("studentID", $array, set_value("studentID", $stID), "id='studentID2' class='form-control'");
                            ?>
                        </div>
                        <span class="col-sm-3 control-label">
                            <?php echo form_error('studentID'); ?>
                        </span>
                    </div>

                    <?php 
                        if(form_error('feecat'))
                            echo "<div class='form-group has-error' >";
                        else     
                            echo "<div class='form-group' >";
                    ?>
                        <label for="feetype" class="col-sm-3 control-label">
                            <?=$this->lang->line("fee_cat")?>
                        </label>
                        <div class="col-sm-6">
                            <select name="feecategory" class="form-control invoice_feecategory" id="invoice_feecategory2" required>
                                <option value="">Select A Fee Category</option>
                                <?php foreach($feecat as $fee => $val): ?>
                                    <option value="<?php echo $val['code']; ?>"><?php echo $val['category']; ?></option>
                                <?php  endforeach; ?>
                            </select>
                        </div>
                        <span class="col-sm-3 control-label">
                            <?php echo form_error('feecategory'); ?>
                        </span>
                    </div>

                    <?php 
                        if(form_error('feetype'))
                            echo "<div class='form-group has-error' >";
                        else     
                            echo "<div class='form-group' >";
                    ?>
                        <label for="feetype" class="col-sm-3 control-label">
                            <?=$this->lang->line("invoice_feetype")?>
                        </label>
                        <div class="col-sm-6">
                            <select name="feetype" class="form-control invoice_feetype" id="invoice_feetype2" required>
                                <option value="">Select A Fee Type</option>
                                
                            </select>
                            <!--input type="text" class="form-control" id="feetype" name="feetype" value="<?=set_value('feetype')?>" >
                            <div class="book"><ul  class="result"></ul></div-->
                        </div>
                        <span class="col-sm-3 control-label">
                            <?php echo form_error('feetype'); ?>
                        </span>
                    </div>

                    <?php 
                        if(form_error('date')) 
                            echo "<div class='form-group has-error' >";
                        else     
                            echo "<div class='form-group' >";
                    ?>
                        <label for="date" class="col-sm-3 control-label">
                            <?=$this->lang->line("invoice_date")?>
                        </label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" id="date2" name="date" value="<?=set_value('date')?>" >
                        </div>
                        <span class="col-sm-3 control-label">
                            <?php echo form_error('date'); ?>
                        </span>
                    </div>
					
					 <?php 
                        if(form_error('amount')) 
                            echo "<div class='form-group has-error' >";
                        else     
                            echo "<div class='form-group' >";
                    ?>
                        <label for="amount" class="col-sm-3 control-label">
                            Amount
                        </label>
                        <div class="col-sm-6">
                            <input type="number" class="form-control" id="amount" name="amount" required value="<?=set_value('amount')?>" />
                        </div>
                        <span class="col-sm-3 control-label">
                            <?php echo form_error('amount'); ?>
                        </span>
                    </div>

                    <div class='form-group'>
                        <label for="amount" class="col-sm-3 control-label">
                            1st Installment
                        </label>
                        <div class="col-sm-6">
                            <input type="number" class="form-control" id="amount" name="amount1" required value="<?=set_value('amount1')?>" />
                        </div>
                    </div>
                    <div class='form-group'>
                        <label for="amount" class="col-sm-3 control-label">
                            2nd Installment
                        </label>
                        <div class="col-sm-6">
                            <input type="number" class="form-control" id="amount" name="amount2" required value="<?=set_value('amount2')?>" />
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-8">
                            <input type="submit" class="btn btn-success" value="<?=$this->lang->line("add_invoice")?>" >
                        </div>
                    </div>
                    <input id="instVal2" name="instVal" type="hidden" value="" />
                </form>
            </div>


            <!-- INSTALMENT THREE -->
            <div class="col-sm-8 three-form" style="display:none;">
                <form class="form-horizontal" role="form" method="post" action="<?php  echo base_url() . 'new_invoice/add/3'; ?>">

                    <?php 
                        if(form_error('account')) 
                            echo "<div class='form-group has-error' >";
                        else     
                            echo "<div class='form-group' >";
                    ?>
                        <label for="account" class="col-sm-3 control-label">
                            <?=$this->lang->line("invoice_account")?>
                        </label>
                        <div class="col-sm-6">

                            <?php
                                $array = array('' => $this->lang->line("invoice_select_account"));
                                foreach ($accounts as $account) {
                                    $array[$account->accountID] = $account->account_name;
                                }
                                echo form_dropdown("account", $array, set_value("account"), "id='account' class='form-control'");
                            ?>
                        </div>
                        <span class="col-sm-3 control-label">
                            <?php echo form_error('account'); ?>
                        </span>
                    </div>

                    <?php 
                        if(form_error('classesID')) 
                            echo "<div class='form-group has-error' >";
                        else     
                            echo "<div class='form-group' >";
                    ?>
                        <label for="classesID" class="col-sm-3 control-label">
                            <?=$this->lang->line("invoice_classesID")?>
                        </label>
                        <div class="col-sm-6">

                            <?php
                                $array = array('0' => $this->lang->line("invoice_select_classes"));
                                foreach ($classes as $classa) {
                                    $array[$classa->classesID] = $classa->classes;
                                }
                                echo form_dropdown("classesID", $array, set_value("classesID"), "id='classesID3' class='form-control'");
                            ?>
                        </div>
                        <span class="col-sm-3 control-label">
                            <?php echo form_error('classesID'); ?>
                        </span>
                    </div>

                    <?php 
                        if(form_error('studentID')) 
                            echo "<div class='form-group has-error' >";
                        else     
                            echo "<div class='form-group' >";
                    ?>
                        <label for="studentID" class="col-sm-3 control-label">
                            <?=$this->lang->line("invoice_studentID")?>
                        </label>
                        <div class="col-sm-6">

                            <?php

                                $array = $array = array('0' => $this->lang->line("invoice_select_student"));
                                if($students != "empty") {
                                    foreach ($students as $student) {
                                        $array[$student->studentID] = $student->name;
                                    }
                                }

                                $stID = 0;
                                if($studentID == 0) {
                                    $stID = 0;
                                } else {
                                    $stID = $studentID;
                                }

                                echo form_dropdown("studentID", $array, set_value("studentID", $stID), "id='studentID3' class='form-control'");
                            ?>
                        </div>
                        <span class="col-sm-3 control-label">
                            <?php echo form_error('studentID'); ?>
                        </span>
                    </div>

                    <?php 
                        if(form_error('feecat'))
                            echo "<div class='form-group has-error' >";
                        else     
                            echo "<div class='form-group' >";
                    ?>
                        <label for="feetype" class="col-sm-3 control-label">
                            <?=$this->lang->line("fee_cat")?>
                        </label>
                        <div class="col-sm-6">
                            <select name="feecategory" class="form-control" id="invoice_feecategory3" required>
                                <option value="">Select A Fee Category</option>
                                <?php foreach($feecat as $fee => $val): ?>
                                    <option value="<?php echo $val['code']; ?>"><?php echo $val['category']; ?></option>
                                <?php  endforeach; ?>
                            </select>
                        </div>
                        <span class="col-sm-3 control-label">
                            <?php echo form_error('feecategory'); ?>
                        </span>
                    </div>

                    <?php 
                        if(form_error('feetype'))
                            echo "<div class='form-group has-error' >";
                        else     
                            echo "<div class='form-group' >";
                    ?>
                        <label for="feetype" class="col-sm-3 control-label">
                            <?=$this->lang->line("invoice_feetype")?>
                        </label>
                        <div class="col-sm-6">
                            <select name="feetype" class="form-control invoice_feetype" id="invoice_feetype3" required>
                                <option value="">Select A Fee Type</option>
                                
                            </select>
                            <!--input type="text" class="form-control" id="feetype" name="feetype" value="<?=set_value('feetype')?>" >
                            <div class="book"><ul  class="result"></ul></div-->
                        </div>
                        <span class="col-sm-3 control-label">
                            <?php echo form_error('feetype'); ?>
                        </span>
                    </div>

                    <?php 
                        if(form_error('date')) 
                            echo "<div class='form-group has-error' >";
                        else     
                            echo "<div class='form-group' >";
                    ?>
                        <label for="date" class="col-sm-3 control-label">
                            <?=$this->lang->line("invoice_date")?>
                        </label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" id="date3" name="date" value="<?=set_value('date')?>" >
                        </div>
                        <span class="col-sm-3 control-label">
                            <?php echo form_error('date'); ?>
                        </span>
                    </div>
					
					 <?php 
                        if(form_error('amount')) 
                            echo "<div class='form-group has-error' >";
                        else     
                            echo "<div class='form-group' >";
                    ?>
                        <label for="amount" class="col-sm-3 control-label">
                            Amount
                        </label>
                        <div class="col-sm-6">
                            <input type="number" class="form-control" id="amount" name="amount" required value="<?=set_value('amount')?>" />
                        </div>
                        <span class="col-sm-3 control-label">
                            <?php echo form_error('amount'); ?>
                        </span>
                    </div>

                    <div class='form-group'>
                        <label for="amount" class="col-sm-3 control-label">
                            1st Installment
                        </label>
                        <div class="col-sm-6">
                            <input type="number" class="form-control" id="amount" name="amount1"  value="<?=set_value('amount1')?>" />
                        </div>
                    </div>
                    <div class='form-group'>
                        <label for="amount" class="col-sm-3 control-label">
                            2nd Installment
                        </label>
                        <div class="col-sm-6">
                            <input type="number" class="form-control" id="amount" name="amount2"  value="<?=set_value('amount2')?>" />
                        </div>
                    </div>
                    <div class='form-group'>
                        <label for="amount" class="col-sm-3 control-label">
                            3rd Installment
                        </label>
                        <div class="col-sm-6">
                            <input type="number" class="form-control" id="amount" name="amount3"  value="<?=set_value('amount3')?>" />
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-8">
                            <input type="submit" class="btn btn-success" value="<?=$this->lang->line("add_invoice")?>" >
                        </div>
                    </div>
                    <input id="instVal3" name="instVal" type="hidden" value="" />
                </form>
            </div>

        </div>
    </div>
</div>
*/ ?>
<script type="text/javascript">
$(document).ready(function(){
   /* $(".invoice_feecategory").select2();
    $(".account").select2();
    $(".classesID").select2();
    $(".invoice_feetype").select2();
    $(".studentID").select2();*/
});
</script>
<script type="text/javascript">
    //nothing to add yet. Place holder
    $("#one").on('click', function(){
        $("#instVal1").val("1");
    });

    $("#two").on('click', function(){
        $("#instVal2").val("2");
    });

    $("#three").on('click', function(){
        $("#instVal3").val("3");
    });
</script>

<!-- Sunny - Code to hide and unhide instalmental amounts -->
 <script type="text/javascript">
    // $(document).ready(function(){
        
        $("#one").on('click', function(){
            $('.two-form').fadeOut(20);
            $('.three-form').fadeOut(20);
            $('.one-form').fadeIn(20);
        });

        $("#two").on('click', function(){
            $('.two-form').fadeIn(20);
            $('.three-form').fadeOut(20);
            $('.one-form').fadeOut(20);
        });

        $("#three").on('click', function(){
            $('.two-form').fadeOut(20);
            $('.three-form').fadeIn(20);
            $('.one-form').fadeOut(20);
        });

        if ($('[value="One"]').is(':checked')){
            $('.two-form').fadeOut(20);
            $('.three-form').fadeOut(20);
            $('.one-form').fadeIn(20);
        }

        if ($('[value="Two"]').is(':checked')){
            $('.two-form').fadeIn(20);
            $('.three-form').fadeOut(20);
            $('.one-form').fadeOut(20);
        }

        if ($('[value="Three"]').is(':checked')){
            $('.two-form').fadeOut(20);
            $('.three-form').fadeIn(20);
            $('.one-form').fadeOut(20);
        }



        //function to get the feetypes for each fee category
        function getFeeType(){

            var category = $(".invoice_feecategory").val();

            var a = "<option value=''>Select A Fee Type</option>";

            if(category != ""){
                $.ajax({
                    type:"post",
                    url:"<?php echo base_url() . 'invoice/getFeeType'; ?>",
                    data:"category=" + category,
                    success:function(data){
                        $(".invoice_feetype").html(data);
                     }
                });
            }else{
                $(".invoice_feetype").html(a);
            } 

        }

        $('.invoice_feecategory').change(function() {
            getFeeType();
        });


        function getFeeType2(){

            var category = $("#invoice_feecategory2").val();

            var a = "<option value=''>Select A Fee Type</option>";

            if(category != ""){
                $.ajax({
                    type:"post",
                    url:"<?php echo base_url() . 'invoice/getFeeType'; ?>",
                    data:"category=" + category,
                    success:function(data){
                        $("#invoice_feetype2").html(data);
                     }
                });
            }else{
                $("#invoice_feetype2").html(a);
            } 

        }

        $('#invoice_feecategory2').change(function() {
            getFeeType2();
        });


        function getFeeType3(){

            var category = $("#invoice_feecategory3").val();

            var a = "<option value=''>Select A Fee Type</option>";

            if(category != ""){
                $.ajax({
                    type:"post",
                    url:"<?php echo base_url() . 'invoice/getFeeType'; ?>",
                    data:"category=" + category,
                    success:function(data){
                        $("#invoice_feetype3").html(data);
                     }
                });
            }else{
                $("#invoice_feetype3").html(a);
            } 

        }

        $('#invoice_feecategory3').change(function() {
            getFeeType3();
        });
        

    // });

</script>
<!-- Ends Here -->

<script type="text/javascript">
$('#classesID').change(function(event) {
    var classesID = $(this).val();
    if(classesID === '0') {
        $('#studentID').val(0);
    } else {
        $.ajax({
            type: 'POST',
            url: "<?=base_url('invoice/call_all_student')?>",
            data: "id=" + classesID,
            dataType: "html",
            success: function(data) {
               $('#studentID').html(data);
            }
        });
    }
});

$('#classesID2').change(function(event) {
    var classesID = $(this).val();
    if(classesID === '0') {
        $('#studentID2').val(0);
    } else {
        $.ajax({
            type: 'POST',
            url: "<?=base_url('invoice/call_all_student')?>",
            data: "id=" + classesID,
            dataType: "html",
            success: function(data) {
               $('#studentID2').html(data);
            }
        });
    }
});

$('#classesID3').change(function(event) {
    var classesID = $(this).val();
    if(classesID === '0') {
        $('#studentID3').val(0);
    } else {
        $.ajax({
            type: 'POST',
            url: "<?=base_url('invoice/call_all_student')?>",
            data: "id=" + classesID,
            dataType: "html",
            success: function(data) {
               $('#studentID3').html(data);
            }
        });
    }
});


$('#feetype').keyup(function() {
    var feetype = $(this).val();
    $.ajax({
        type: 'POST',
        url: "<?=base_url('invoice/feetypecall')?>",
        data: "feetype=" + feetype,
        dataType: "html",
        success: function(data) {
            if(data != "") {
                var width = $("#feetype").width();
                $(".book").css('width', width+25 + "px").show();
                $(".result").html(data);

                $('.result li').click(function(){
                    var result_value = $(this).text();
                    $('#feetype').val(result_value);
                    $('.result').html(' ');
                    $('.book').hide();
                });
            } else {
                $(".book").hide();
            }
           
        }
    });
});

$('#date').datepicker();
$('#date2').datepicker();
$('#date3').datepicker();
</script>