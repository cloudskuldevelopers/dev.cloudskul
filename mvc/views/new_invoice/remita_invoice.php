<?php

//QR CODE =============

  $PNG_TEMP_DIR = base_url().'uploads/temp/';
  $PNG_WEB_DIR = 'temp/';

    //include "QR/qrlib.php";
    include APPPATH."libraries/qr/qrlib.php";

    //remember to sanitize user input in real-life solution !!!
    $errorCorrectionLevel = 'L';

    if(isset($_REQUEST['level']) && in_array($_REQUEST['level'], array('L','M','Q','H')))
        $errorCorrectionLevel = $_REQUEST['level'];

      $matrixPointSize = 4;

    if (isset($_REQUEST['size']))

        $matrixPointSize = min(max((int)$_REQUEST['size'], 1), 10);

    $link = base_url('invoice/qr_check/'.$invoice->invoiceID);
    //$link = "http://ng.clouds/nekede/index.php?fee/qr_check/" . $_SESSION['putmeID'] . '/' . $_SESSION['payeeID'];

        $filename = 'uploads/temp/'.md5($link.'|'.$errorCorrectionLevel.'|'.$matrixPointSize).'.png';
        QRcode::png($link, $filename, $errorCorrectionLevel, $matrixPointSize, 2);


?>
<!DOCTYPE html>
<html lang="en">
<head>
<title><?php echo $panel_title; ?></title>

<style type="text/css">
    #page-wrap {
        width: 700px;
        margin: 0 auto;
    }
    .center-justified {
        text-align: justify;
        margin: 0 auto;
        width: 30em;
    }
    /*ini starts here*/
    .list-group {
      padding-left: 0;
      margin-bottom: 15px;
      width: auto;
    }
    .list-group-item {
      position: relative;
      display: block;
      padding: 7.5px 10px;
      margin-bottom: -1px;
      background-color: #fff;
      border: 1px solid #ddd;
      /*margin: 2px;*/
    }
    table {
      border-spacing: 0;
      border-collapse: collapse;
      font-size: 16px;
    }
    td,
    th {
      padding: 0;
    }
    @media print {
      * {
        color: #000 !important;
        text-shadow: none !important;
        background: transparent !important;
        box-shadow: none !important;
      }
      a,
      a:visited {
        text-decoration: underline;
      }
      a[href]:after {
        content: " (" attr(href) ")";
      }
      abbr[title]:after {
        content: " (" attr(title) ")";
      }
      a[href^="javascript:"]:after,
      a[href^="#"]:after {
        content: "";
      }
      pre,
      blockquote {
        border: 1px solid #999;

        page-break-inside: avoid;
      }
      thead {
        display: table-header-group;
      }
      tr,
      img {
        page-break-inside: avoid;
      }
      img {
        max-width: 100% !important;
      }
      p,
      h2,
      h3 {
        orphans: 3;
        widows: 3;
      }
      h2,
      h3 {
        page-break-after: avoid;
      }
      select {
        background: #fff !important;
      }
      .navbar {
        display: none;
      }
      .table td,
      .table th {
        background-color: #fff !important;
      }
      .btn > .caret,
      .dropup > .btn > .caret {
        border-top-color: #000 !important;
      }
      .label {
        border: 1px solid #000;
      }
      .table {
        border-collapse: collapse !important;
      }
      .table-bordered th,
      .table-bordered td {
        border: 1px solid #ddd !important;
      }
    }
    table {
      max-width: 100%;
      background-color: transparent;
      font-size: 16px;
    }
    th {
      text-align: left;
    }
    .table {
      width: 100%;
      margin-bottom: 20px;
    }
    .head {
       border-top: 0px solid #e2e7eb;
       border-bottom: 0px solid #e2e7eb;  
    }
    .table > thead > tr > th,
    .table > tbody > tr > th,
    .table > tfoot > tr > th,
    .table > thead > tr > td,
    .table > tbody > tr > td,
    .table > tfoot > tr > td {
      padding: 8px;
      line-height: 1.428571429;
      vertical-align: top;
      border-top: 1px solid #e2e7eb; 
    }
    /*ini edit default value : border top 1px to 0 px*/
    .table > thead > tr > th {
      font-size: 16px;
      font-weight: 500;
      vertical-align: bottom;
      /*border-bottom: 2px solid #e2e7eb;*/
      color: #242a30;
     
      
    }
    
    .table > caption + thead > tr:first-child > th,
    .table > colgroup + thead > tr:first-child > th,
    .table > thead:first-child > tr:first-child > th,
    .table > caption + thead > tr:first-child > td,
    .table > colgroup + thead > tr:first-child > td,
    .table > thead:first-child > tr:first-child > td {
      border-top: 0;
    }
    .table > tbody + tbody {
      border-top: 2px solid #e2e7eb;
    }
    .table .table {
      background-color: #fff;
    }
    .table-condensed > thead > tr > th,
    .table-condensed > tbody > tr > th,
    .table-condensed > tfoot > tr > th,
    .table-condensed > thead > tr > td,
    .table-condensed > tbody > tr > td,
    .table-condensed > tfoot > tr > td {
      padding: 5px;
    }
    .table-bordered {
      border: 1px solid #e2e7eb;
    }
    .table-bordered > thead > tr > th,
    .table-bordered > tbody > tr > th,
    .table-bordered > tfoot > tr > th,
    .table-bordered > thead > tr > td,
    .table-bordered > tbody > tr > td,
    .table-bordered > tfoot > tr > td {
      border: 1px solid #e2e7eb;
    }
    .table-bordered > thead > tr > th,
    .table-bordered > thead > tr > td {
      border-bottom-width: 2px;
    }
    .table-striped > tbody > tr:nth-child(odd) > td,
    .table-striped > tbody > tr:nth-child(odd) > th {
      background-color: #f0f3f5;
    }
    .panel-title {
      margin-top: 0;
      margin-bottom: 0;
      font-size: 20px;
      color: #fff;
      padding: 0;
    }
    .panel-title > a {
      color: #707478;
      text-decoration: none;
    }
    a {
      background: transparent;
      color: #707478;
      text-decoration: none;
    }
    strong {
        color: #000000;
    }

    .total {
        float: left;
        color: #232A3F;
        margin-left: 80px;
        font-weight: 200;
    }

    .lead {
        font-size: 20px;
    }

    #down {
      margin-top: 250;
  }
</style>
</head>
<body>


<div id="page-wrap">
  <table width="100%" >
    <tr>
      <td width="5%">
        <h2>
          <?php
            if($setschool->photo) {
                $array = array(
                    "src" => base_url('uploads/images/'.$this->session->userdata('schoollogo')),
                    'width' => '50px',
                    'height' => '50px',
                    "style" => "margin-right:0px;"
                );
                echo img($array);
            } 
            
            ?>
        </h2>
      </td>
      <td width="70%" >
      <h3 style="margin-top:15px;font-size:22px;">  <?php  echo "  ".$this->session->userdata('school'); ?></h3>
      </td>
      <td width="25%">
        <!--h5 style="margin-top:35px;"><?php  echo $this->lang->line("invoice_create_date")." : ". date("d M Y"); ?></h5-->
        <div>
          <!--p>A  &nbsp;<?=$setschool->address?> </p> </br-->
          <p>T  &nbsp;<?=$setschool->phone?> </p> </br>
          <p>E  &nbsp;<?=$setschool->email?> </p> </br>
        </div>
      </td>
    </tr>
  </table>
  <br />

<table width="100%">
    <tr >
      <td width="15%" >
      </td>
      <td width="65%" >
      </td>
      <td width="23%">
        <p><?php  echo ""."  ". date("d/m/Y"); ?></p>
      </td>
    </tr>
  </table>
  <br /><br />
  <table width="100%" style="font-size: 12px;">
    <tr>
      <td width="33%">
        <table style="font-size: 12px;">
            <tbody>
                <tr>
                    <th><?php  echo $this->lang->line("invoice_from"); ?></th>
                </tr>
                <tr>
                    <td><?=$setschool->school?></td>
                </tr>
                <tr>
                  <td><?=$setschool->address?></td>
                </tr>
                <tr>
                  <td><?=$this->lang->line("invoice_phone"). " : ". $setschool->phone?></td>
                </tr>
                <tr>
                  <td><?=$this->lang->line("invoice_email"). " : ". $setschool->email?></td>
                </tr>
            </tbody>
          </table>

      </td>
      <td width="33%">
        <?php if(count($student) == "") { ?>
          <table >
              <tbody>
                  <tr>
                      <th><?php  echo $this->lang->line("invoice_to"); ?></th>
                  </tr>
                  <tr>
                      <td><?php  echo $this->lang->line("invoice_sremove"); ?></td>
                  </tr>
              </tbody>
          </table>
        <?php } else { ?>
          <table style="font-size: 12px;">
            <tbody>
                <tr>
                    <th><?php  echo $this->lang->line("invoice_to"); ?></th>
                </tr>
                <tr>
                    <td><?php  echo $student->name; ?></td>
                </tr>
                <!--tr>
                    <td><?php  //echo $this->lang->line("invoice_roll"). " : ". $invoice->roll; ?></t>
                </tr-->
                <tr>
                    <td><?php  echo $this->lang->line("invoice_classesID"). " : ". $invoice->classes; ?></td>
                </tr>
                <tr>
                  <td><?=$this->lang->line("invoice_email"). " : ". $student->email?></td>
                </tr>
            </tbody>
          </table>
        <?php } ?>
      </td>
      <td width="33%" style="vertical-align: text-top;">
        <table style="font-size: 12px;">
          <tbody>
            <tr>
                <th><?php  echo "Remita Payment Details"; ?></th>
            </tr>
            <tr>
              <td><?php echo "Remita Retrieval Reference". " : <strong>" . $invoice->invoiceID."</strong>"; ?></td>
            </tr>
            <tr>
              <td><?php echo "Order ID". " : <strong>" . $invoice->invoiceID."</strong>"; ?></td>
            </tr>
          </tbody>
        </table>
      </td>
    </tr>
  </table>
  <br /><br />

  <table class="table table-striped">
    <!--thead>
      <tr>
          <th><?=$this->lang->line('slno')?></th>
          <th><?=$this->lang->line('invoice_feetype')?></th>
          <th><?=$this->lang->line('invoice_subtotal')?></th>
      </tr>
    </thead>
    <tbody>
      <tr>
          <td width="10%">
              <?php echo 1; ?>
          </td>
          <td width="80%">
              <?php echo $invoice->feetype ?>
          </td>
          <td width="10%">
              <?php echo $invoice->amount; ?>
          </td>
      </tr>
    </tbody-->
  </table>

  <table class="table" width="100%">
    <tr>
      <td width="65%">
        <p class="lead">Fee Description</p>
        <table class="table table-striped">
            <tr>
                <td width="70%"><?=$invoice->feetype?></td>
                <td width="30%"><?php echo number_format($invoice->amount,2,'.',',') ?></td>
            </tr>
          </table>
          <table class = "table table-striped">
          <tr>
              <td width="70%"><? echo "Service Charge" ?></td>
              <td width="30%"><?php echo number_format($invoice->amount,2,'.',',') ?></td>
          </tr>
        </table>
        <table class="table table-striped">
            <tr>
              
                <td style="margin-top:15px;font-size:14px;" width="70%"><strong><?=$this->lang->line('invoice_total')." payable amount (".$setschool->currency_code.")";?></strong></td>
                <td style="margin-top:15px;font-size:14px;" width="30%"><strong><?=$setschool->currency_symbol." ".number_format((intval($invoice->amount)+intval($invoice->amount)),2,'.',',')?></strong></td>

            </tr>
        </table>
      </td>
      <td width="35%">
            <p class="lead">&nbsp;</p>
            <table class="table">
                <tr>
                    <td width="30%"></td>
                    <td width="70%"><img src="<?php echo $filename; ?>" width="100px" style=" margin-top: 0px;" height="100px" /> <br /><p align="center"></p></td>
                </tr>
            </table>      
      </td>
    </tr>
  </table>

<div id="">
  <!--payment options section-->
  <table id="down" class="table" width="100%">
    <tr align="center">
      <td>
        <img src="<?php echo base_url('uploads/images/rem.png'); ?>" class="" style=" margin-top: 0px;" id="remitaImage" />
      </td>
    </tr>
  </table>


<!-- cloudskul copyright section-->
  <table class="table" width="100%">
    <tr>
      <td width="20%">
        <?php
                $array = array(
                    "src" => base_url('uploads/images/logo.png'),
                    'width' => 'auto',
                    'height' => '50px',
                    "style" => "margin-right:0px;"
                );
                echo img($array);
            ?>
      </td>
      <td width="40%"></td>
      <td align="right" width="40%"><i>All rights reserved &#169; Cloudskul <?php echo date('Y')?> </i></td>
    </tr>
  </table>
</div>

</div>


</body>
</html>