<!-- ================== BEGIN BASE JS ================== -->
<script src="assets/plugins/pace/pace.min.js"></script>
<!-- ================== END BASE JS ================== -->
<div id="container">
<div class="row">
	<div class="col-md-2 hidden-xs">
		<img src="<?php echo base_url('uploads/images/logo.png'); ?>" style="width: auto; height: 40px; margin-top: -8px;" class="img-thumbnail img-responsive" />
	</div>
	<div class="col-md-7 col-xs-9 text-center">
		<h1 class="page-header"><?php echo $this->session->userdata('school');  ?></h1>
	</div>
	<div class="col-md-3 col-xs-3">
		<!-- begin breadcrumb -->
		<ol class="breadcrumb pull-right">
			<li ><a href="<?php echo base_url('dashboard/index');?>"><i class="fa fa-laptop"></i> Dashboard</a></li>
			<li >Invoice</li>
		</ol>
		<!-- end breadcrumb -->
	</div>
</div>

<!-- begin panel -->
<div class="panel panel-inverse" data-sortable-id="ui-widget-3">
	<div class="panel-heading">
		<?php
			$usertype = $this->session->userdata("usertype");
			if($usertype == "Admin" || $usertype == "Super Admin" || $usertype == "Accountant") {
		?>
		<div class="btn-group-vertical pull-right">
			<a href="<?php echo base_url('new_invoice/add'); ?>" type="button" class="btn btn-success btn-xs">
				<i class="fa fa-plus"></i> 
				<?=$this->lang->line('add_title');?>
			</a>
		</div>
		<?php } ?>
		<h3 class="panel-title"><i class="fa icon-feetype"></i> <?=$this->lang->line('panel_title')?></h3>
	</div>
	<div class="panel-body">
		<table id="data-table" class="table table-striped table-bordered wrap" width="100%">
			<thead>
				<tr>
					<th><?=$this->lang->line('slno')?></th>
					<th><?=$this->lang->line('invoice_feetype')?></th>
					<th><?=$this->lang->line('invoice_date')?></th>
					<th><?=$this->lang->line('invoice_status')?></th>
					<th><?=$this->lang->line('payment_plan')?></th>
					<th><?=$this->lang->line('invoice_student')?></th>
					<th><?=$this->lang->line('invoice_amount')?></th>
					<th><?=$this->lang->line('invoice_due')?></th>
					<th><?=$this->lang->line('action')?></th>
				</tr>
			</thead>
			<tbody>
				<?php if(count($invoices)) {$i = 1; foreach($invoices as $invoice) { ?>
					<tr>
						<td data-title="<?=$this->lang->line('slno')?>">
							<?php echo $i; ?>
						</td>
						<td data-title="<?=$this->lang->line('invoice_feetype')?>">
							<?php echo $invoice->feetype; ?>
						</td>

						<td data-title="<?=$this->lang->line('invoice_date')?>">
							<?php echo $invoice->date; ?>
						</td>

						<td data-title="<?=$this->lang->line('invoice_status')?>">
							<?php 

								$status = $invoice->status;
								$setstatus = '';
								if($status == 0) {
									$status = $this->lang->line('invoice_notpaid');
								} elseif($status == 1) {
									$status = $this->lang->line('invoice_partially_paid');
								} elseif($status == 2) {
									$status = $this->lang->line('invoice_fully_paid');
								} elseif($status == 3) {
									$status = "RRR Generated";
								}

								echo "<button class='btn btn-success btn-xs'>".$status."</button>";

							?>
						</td>

						<td data-title="<?=$this->lang->line('payment_plan')?>">
							<?php 

								$plan = $invoice->instalment_plan; 
								$msg = '';
								if($plan == 1) {
									$msg = $this->lang->line('instalment_plan_one');
								} elseif($plan == 2) {
									$msg = $this->lang->line('instalment_plan_two');
								} elseif($plan == 3) {
									$msg = $this->lang->line('instalment_plan_three');
								}

								echo "<button class='btn btn-info btn-xs'>".$msg."</button>";


							?>
						</td>


						<td data-title="<?=$this->lang->line('invoice_student')?>">
							<?php echo $invoice->student; ?>
						</td>


						<td data-title="<?=$this->lang->line('invoice_amount')?>">
							<?php echo $setschool->currency_symbol. $invoice->amount; ?>
						</td>

						<td data-title="<?=$this->lang->line('invoice_due')?>">
							<?php echo $setschool->currency_symbol. ($invoice->amount - $invoice->paidamount); ?>
						</td>

						
						<td data-title="<?=$this->lang->line('action')?>">
							<?php echo btn_view('new_invoice/view/' . $invoice->invoiceID, $this->lang->line('view')) ?>
							<?php if($usertype == "Admin" || $usertype == "Super Admin" || $usertype == "Accountant") { ?>
							<?php echo btn_edit('new_invoice/edit/'.$invoice->invoiceID, $this->lang->line('edit')) ?>
							<?php echo btn_delete('new_invoice/delete/'.$invoice->invoiceID, $this->lang->line('delete'))?>
							<?php } ?>
						</td>
					</tr>
				<?php $i++; }} ?>
			</tbody>
		</table>
	</div>
</div>
<!-- end panel -->


<!-- begin scroll to top btn -->
<a href="javascript:;" class="btn btn-icon btn-circle btn-success btn-scroll-to-top fade" data-click="scroll-top"><i class="fa fa-angle-up"></i></a>
<!-- end scroll to top btn -->

</div>
<!-- end page container -->

<?php /*
<div class="box box-danger">
    <div class="box-header with-border">
        <h3 class="box-title"><i class="fa icon-invoice"></i> <?=$this->lang->line('panel_title')?></h3>

       
        <ol class="breadcrumb">
            <li><a href="<?=base_url("dashboard/index")?>"><i class="fa fa-laptop"></i> <?=$this->lang->line('menu_dashboard')?></a></li>
            <li class="active"><?=$this->lang->line('menu_invoice')?></li>
        </ol>
    </div><!-- /.box-header -->
    <!-- form start -->
    <div class="box-body">
        <div class="row">
            <div class="col-sm-12">

                <?php
                    $usertype = $this->session->userdata("usertype");
                    if($usertype == "Admin" || $usertype == "Super Admin" || $usertype == "Accountant") {
                ?>
                <h5 class="page-header">
                    <a href="<?php echo base_url('new_invoice/add') ?>">
                        <i class="fa fa-plus"></i> 
                        <?=$this->lang->line('add_title')?>
                    </a>
                </h5>
                <?php } ?>

                <div id="hide-table">
                    <table id="example1" class="table table-striped table-bordered table-hover dataTable no-footer">
                        <thead>
                            <tr>
                                <th><?=$this->lang->line('slno')?></th>
                                <th><?=$this->lang->line('invoice_feetype')?></th>
                                <th><?=$this->lang->line('invoice_date')?></th>
                                <th><?=$this->lang->line('invoice_status')?></th>
                                <th><?=$this->lang->line('payment_plan')?></th>
                                <th><?=$this->lang->line('invoice_student')?></th>
                                <th><?=$this->lang->line('invoice_amount')?></th>
                                <th><?=$this->lang->line('invoice_due')?></th>
                                <th><?=$this->lang->line('action')?></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php if(count($invoices)) {$i = 1; foreach($invoices as $invoice) { ?>
                                <tr>
                                    <td data-title="<?=$this->lang->line('slno')?>">
                                        <?php echo $i; ?>
                                    </td>
                                    <td data-title="<?=$this->lang->line('invoice_feetype')?>">
                                        <?php echo $invoice->feetype; ?>
                                    </td>

                                    <td data-title="<?=$this->lang->line('invoice_date')?>">
                                        <?php echo $invoice->date; ?>
                                    </td>
       
                                    <td data-title="<?=$this->lang->line('invoice_status')?>">
                                        <?php 

                                            $status = $invoice->status;
                                            $setstatus = '';
                                            if($status == 0) {
                                                $status = $this->lang->line('invoice_notpaid');
                                            } elseif($status == 1) {
                                                $status = $this->lang->line('invoice_partially_paid');
                                            } elseif($status == 2) {
                                                $status = $this->lang->line('invoice_fully_paid');
                                            } elseif($status == 3) {
                                                $status = "RRR Generated";
                                            }

                                            echo "<button class='btn btn-success btn-xs'>".$status."</button>";

                                        ?>
                                    </td>

                                    <td data-title="<?=$this->lang->line('payment_plan')?>">
                                        <?php 

                                            $plan = $invoice->instalment_plan; 
                                            $msg = '';
                                            if($plan == 1) {
                                                $msg = $this->lang->line('instalment_plan_one');
                                            } elseif($plan == 2) {
                                                $msg = $this->lang->line('instalment_plan_two');
                                            } elseif($plan == 3) {
                                                $msg = $this->lang->line('instalment_plan_three');
                                            }

                                            echo "<button class='btn btn-info btn-xs'>".$msg."</button>";


                                        ?>
                                    </td>


                                    <td data-title="<?=$this->lang->line('invoice_student')?>">
                                        <?php echo $invoice->student; ?>
                                    </td>


                                    <td data-title="<?=$this->lang->line('invoice_amount')?>">
                                        <?php echo $setschool->currency_symbol. $invoice->amount; ?>
                                    </td>

                                    <td data-title="<?=$this->lang->line('invoice_due')?>">
                                        <?php echo $setschool->currency_symbol. ($invoice->amount - $invoice->paidamount); ?>
                                    </td>

                                    
                                    <td data-title="<?=$this->lang->line('action')?>">
                                        <?php echo btn_view('new_invoice/view/' . $invoice->invoiceID, $this->lang->line('view')) ?>
                                        <?php if($usertype == "Admin" || $usertype == "Super Admin" || $usertype == "Accountant") { ?>
                                        <?php echo btn_edit('new_invoice/edit/'.$invoice->invoiceID, $this->lang->line('edit')) ?>
                                        <?php echo btn_delete('new_invoice/delete/'.$invoice->invoiceID, $this->lang->line('delete'))?>
                                        <?php } ?>
                                    </td>
                                </tr>
                            <?php $i++; }} ?>
                        </tbody>
                    </table>
                </div>

            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $('#classesID').change(function() {
        var classesID = $(this).val();
        if(classesID == 0) {
            $('#hide-table').hide();
        } else {
            $.ajax({
                type: 'POST',
                url: "<?=base_url('setfee/setfee_list')?>",
                data: "id=" + classesID,
                dataType: "html",
                success: function(data) {
                    window.location.href = data;
                }
            });
        }
    });
</script>
*/ ?>