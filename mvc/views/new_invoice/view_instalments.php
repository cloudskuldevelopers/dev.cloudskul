<style type="text/css">
    section.content.invoice {
        margin: 0;
    }
</style>
<?php 
    if(count($invoice)) {
        $usertype = $this->session->userdata("usertype");
        if($usertype == "Admin" || $usertype == "Super Admin" || $usertype == "Accountant" || $usertype == "Student" || $usertype == "Parent") {
?>
	<?php if($usertype == "Admin" || $usertype == "Super Admin" || $usertype == "Accountant") { ?>
    <div class="well well-offset">
        <div class="row">

            <div class="col-sm-8">
                <button class="btn btn-default btn-sm" style="border-radius: 0px !important;" onclick="printInv	('printablediv')"><span class="fa fa-print"></span> <?=$this->lang->line('print')?> </button>
                <?php
                 echo btn_add_pdf('new_invoice/print_preview/'.$invoice->invoiceID, $this->lang->line('pdf_preview')) 
                ?>
                <?php
                    if($invoice->status == 3) {
                        //echo btn_payment('invoice/payment/'.$invoice->invoiceID, $this->lang->line('payment')); 
                        echo btn_payment('new_invoice/remita_invoice/'.$invoice->invoiceID.'/'.$invoice->invoice_ref, "Print Bank Invoice");
                    }
                ?>
                <button class="btn btn-success btn-sm" data-toggle="modal" data-target="#mail"><span class="fa fa-envelope-o"></span> <?=$this->lang->line('mail')?></button> 

				<script type="text/javascript">
					function printDiv(divName) {
						var remitaImage = document.getElementById("remitaImage");
						remitaImage.style.visibility = 'hidden';
						
						var printContents = document.getElementById(divName).innerHTML;
						var originalContents = document.body.innerHTML;
						
						document.body.innerHTML = printContents;

						window.print();

						document.body.innerHTML = originalContents;
					}
				</script>
            </div>
			
            <div class="col-sm-4">
                <ol class="breadcrumb offset_breadcrumb">
                    <li><a href="<?=base_url("dashboard/index")?>"><i class="fa fa-laptop"></i> <?=$this->lang->line('menu_dashboard')?></a></li>
                    <li><a href="<?=base_url("new_invoice/index")?>"><?=$this->lang->line('menu_invoice')?></a></li>
                    <li class="active"><?=$this->lang->line('view')?></li>
                </ol>
            </div>
        </div>
    </div>
    <?php } elseif($usertype == "Student" || $usertype == "Parent") { ?>
        <?php if($invoice->paidamount != $invoice->amount) { ?>
        <div class="well">
            <div class="row">
                <div class="col-sm-6">
                	<button class="btn btn-default btn-sm" style="border-radius: 0px !important;" onclick="printInv('printablediv')"><span class="fa fa-print"></span> <?=$this->lang->line('print')?> </button>
                
                    <?php //btn_payment('invoice/print_remita/'.$invoice->invoiceID.'/'.$invoice->invoice_ref, "Print Invoice"); ?>
                    <?php
                    if($invoice_details[0]->status == 3) {
                        //echo btn_payment('invoice/payment/'.$invoice->invoiceID, $this->lang->line('payment')); 
                        echo btn_payment('new_invoice/remita_invoice/'.$invoice->invoiceID.'/'.$invoice->invoice_ref, "Print Bank Invoice");
                    }
                    ?>
                </div>

                <div class="col-sm-6">
                    <ol class="breadcrumb">
                        <li><a href="<?=base_url("dashboard/index")?>"><i class="fa fa-laptop"></i> <?=$this->lang->line('menu_dashboard')?></a></li>
                        <li><a href="<?=base_url("invoice/index")?>"><?=$this->lang->line('menu_invoice')?></a></li>
                        <li class="active"><?=$this->lang->line('view')?></li>
                    </ol>
                </div>
            </div>
        </div>
        <?php } ?>
    <?php } ?>
	
	<!--invoice row-->
		<div class="well" style="border-radius: 0px; padding-top: 7px; padding-bottom: 0px;">
			<div class="row">
				<div class="col-md-12">
					<h3><?php //$this->lang->line("invoice_invoice").$invoice->invoiceID ?><?=$this->lang->line("invoice_invoice").$invoice_details[0]->inst_ref?></h3><br>
					<small>
						<?php
							if($invoice_details[0]->paid_date) {
								echo "<b>".$this->lang->line("invoice_pdate")." :  </b>". date("d M Y", strtotime($invoice_details[0]->paid_date)).'<br/>';
							}
						?>
					</small>
				</div>
			</div>
		</div>

<div id="printablediv">
	<section class="content invoice" style="padding-bottom:0px; overflow: hidden !important;">
		<!-- title row -->
		<div class="row">
		    <div class="col-xs-12 invoice-offset">
		        <h2 class="page-header" style="margin-top: 10px; margin-bottom: 0px;">
		            <?php
	                    if($setschool->photo) {
		                    $array = array(
		                        "src" => base_url('uploads/images/'.$this->session->userdata('schoollogo')),
		                        'width' => '25px',
		                        'height' => '25px',
		                        'class' => 'img-circle'
		                    );
		                    echo img($array);
		                } 
	                ?>
	                <?php  echo $this->session->userdata('school'); ?>
		            <small class="pull-right" style="font-size: 12px; font-weight: 400;"><?=$this->lang->line('invoice_create_date').' : '.date('d M Y')?></small>
		        </h2>
		    </div><!-- /.col -->
		</div>
		
		<!-- info row -->
		<div class="row invoice-info">
		    <div class="col-sm-4 invoice-col">
		   
				<?php  echo $this->lang->line("invoice_from"); ?>
				<address>
					<strong><?=$setschool->school;?></strong><br>
					<?=$setschool->address?><br>
					<?=$this->lang->line("invoice_phone"). " : ". $setschool->phone?><br>
					<?=$this->lang->line("invoice_email"). " : ". $setschool->email?><br>
				</address>
	            

		    </div><!-- /.col -->
		    <div class="col-sm-4 invoice-col">
		        <?php if(count($student) == "") { ?>
		        	<?=$this->lang->line("invoice_to")?>
		        	<address>
		        		<?=$this->lang->line("invoice_sremove")?>
		        	</address>
		        <?php } else { ?>
		        	<?=$this->lang->line("invoice_to")?>
		        	<address>
		        		<strong><?=$student->name?></strong><br>
		        		<?=$this->lang->line("invoice_roll"). " : ". $invoice->roll?><br>
		        		<?=$this->lang->line("invoice_classesID"). " : ". $invoice->classes?><br>
		        		<?=$this->lang->line("invoice_email"). " : ". $student->email?><br>
		        	</address>
		        <?php } ?>

		    </div><!-- /.col -->
		    <div class="col-sm-4 invoice-col">
				<p style="margin-bottom: 2px;"><b><?=$this->lang->line("invoice_invoice").$invoice_details[0]->inst_ref?></b></p>
                <?php if($invoice_details[0]->status == 3){ ?>
		        <b><?="RRR : ".$invoice->rrr?></b><br>
                <?php } ?>
		        <?php
                    if($invoice_details[0]->paid_date) {
                        echo "<b>".$this->lang->line("invoice_pdate")." :  </b>". date("d M Y", strtotime($invoice_details[0]->paid_date)).'<br/>';
                    }
                ?>
                <?php 
                    $status = $invoice_details[0]->status;
                    $setstatus = '';
                    if($status == 0) {
                        $status = $this->lang->line('invoice_notpaid');
                    } elseif($status == 1) {
                        $status = $this->lang->line('invoice_fully_paid');
                    } elseif($status == 2) {
                        $status = $this->lang->line('You have made a request to pay at the bank');
                    }

                    echo $this->lang->line('invoice_status'). " : ". "<button class='btn btn-danger btn-xs'>".$status."</button>";
                ?>

		    </div><!-- /.col -->
		</div><!-- /.row -->

		<!-- Table row -->
        <br />
		<div class="row">
			<div class="col-xs-12" id="hide-table">
		        <table class="table table-hover table-responsive">
		            <thead>
		                <tr>
		                    <th class="col-lg-1"><?=$this->lang->line('slno')?></th>
		                    <th class="col-lg-4"><?=$this->lang->line('invoice_feetype')?></th>
		                    <!--th class="col-lg-4">Payment Description</th-->
		                    <th class="col-lg-2"><?=$this->lang->line('invoice_tota')?>Amount</th>
		                </tr>
		            </thead>
		            <tbody>
		                <tr>
		                    <td data-title="<?=$this->lang->line('slno')?>">
		                        <?php echo 1; ?>
		                    </td>
		                    <td data-title="<?=$this->lang->line('invoice_feetype')?>">
		                        <?php echo $invoice->feetype." (".print_instalment($invoice_details[0]->pay_seq)." Instalment)"; ?>
		                    </td>
							<!--td data-title="<?=$this->lang->line('invoice_feetype')?>">
		                        <?php //echo $invoiceData->installment_desc; ?> 
		                    </td-->
		                    <td data-title="<?=$this->lang->line('invoice_total')?>">
                                <?php echo $invoice_details[0]->amount; ?>
		                    </td>
		                </tr>
		            </tbody>
		        </table>
		    </div>
		</div><!-- /.row -->

		<div class="row">    
			<!-- accepted payments column -->
			<div class="col-xs-12 col-sm-6 col-lg-6 col-md-6">
				<!--p class="lead">Payment Methods:</p-->
                <?php if($invoice_details[0]->status != 1) { ?>
				<p class="take">Please Select how you would like to make this payment</p>
                <?php }elseif($invoice_details[0]->status == 2){ ?>
                <p class="take">You have made a request to pay at the bank</p>
                <div class="col-xs-12 col-sm-6 col-lg-6 col-md-6 invoice-col take" style="margin-top: 5px;">
                	<p class="text-center">
                	<?php
                 		echo btn_payment('new_invoice/remita_invoice/'.$invoice->invoiceID, "Print Bank Invoice");
                	?>
            		</p>
                	<p class="text-center">OR</p>
                	<p class="text-center">
                		<button class='btn btn-success btn-sm payDebit' onclick=''>Pay with Debit Card</button>
                	</p>
                </div>
               <?php } ?>

               <!-- Hide Div -->
                <?php if($invoice_details[0]->status == 2){ ?>
				<div class="col-xs-12 col-sm-6 col-lg-6 col-md-6 invoice-col payOptions take" style="margin-top: 5px; display:none;">
				<?php }elseif($invoice_details[0]->status != 2){ ?>
				<div class="col-xs-12 col-sm-6 col-lg-6 col-md-6 invoice-col payOptions take" style="margin-top: 5px;">
				<?php } ?>
                    <?php //if($invoice->paidamount != $invoice->amount && $invoice->status != 3) { ?>
                    <?php if($invoice_details[0]->status == 0 || $invoice_details[0]->status == 2) { ?>
                    <label id="bankOption" class="radio">
                      <input type="radio" name="payment_type" id="pay" value="BANK_BRANCH">
                      Pay at any bank
                    </label>
                    <label class="radio">
                      <input type="radio" name="payment_type" id="pay2" value="Interswitch">
                      Pay with Verve Card
                    </label>
                    <label class="radio">
                      <input type="radio" name="payment_type" id="pay3" value="UPL" >
                      Pay with Visa Card
                    </label>
                    <label class="radio">
                      <input type="radio" name="payment_type" id="pay4" value="UPL">
                      Pay with MasterCard
                    </label>
                    
                    <form id="submitForm" method="post" action="<?php echo base_url('new_invoice/instalment_payment/'.$invoice->invoiceID.'/'.$invoice_details[0]->pay_seq); ?>">
                        <input type="hidden" name="service_charge" id="service_charge" value="" />
                        <input type="hidden" name="payment_method" id="payment" value="Remita" />
                        <input type="hidden" name="payment_type" id="payment_type" value="" />
                        <input type="hidden" name="fees" id="fees" value="<?=$invoice_details[0]->amount; ?>" />
                        <input type="hidden" name="amount" id="total" value="" />
                    </form>
                    <?php } ?>
                    <!--option value="REMITA_PAY"> Remita Account Transfer</option>
                    <option value="Interswitch"> Verve Card</option>
                    <option value="UPL"> Visa</option>
                    <option value="UPL"> MasterCard</option>
                    <option value="PocketMoni"> PocketMoni</option>
                    <option value="RRRGEN"> POS</option>
                    <option value="ATM"> ATM</option>
                    <option selected value="BANK_BRANCH">BANK BRANCH</option>
                    <option value="BANK_INTERNET">BANK INTERNET</option-->
                

                </div>
				
				<!--p class="text-muted well well-sm no-shadow" style="margin-top: 10px; border-radius: 0px !important; width: 75%;">
				</p-->
			</div><!-- /.col -->
			
		    <div class="col-xs-12 col-sm-6 col-lg-6 col-md-6">
		        <p class="lead"><?=$this->lang->line('invoice_amount')?></p>
		        <div class="table-responsive" style="overflow: hidden !important;">
		            <table class="table" style="margin-bottom: 0px !important;">
                        <?php if($invoice_details[0]->status != 1 && $invoice->service_charge_info != 1){ ?>
		                <tr>
                            <th class="col-sm-8 col-xs-8"><?=$this->lang->line('invoice_subtotal')?>-<?=$invoice->feetype." (".print_instalment($invoice_details[0]->pay_seq)." Instalment)"?></th>
                            <td class="col-sm-4 col-xs-4"><?=$setschool->currency_code."".$invoice_details[0]->amount; ?></td>
                        </tr>
                        <tr>
                            <th class="col-sm-8 col-xs-8">Service Charge</th>
                            <!-- Display Service charge for Bank Payment Option -->
                            <td class="col-sm-4 col-xs-4" id="amt"></td>
		                </tr>
		                <?php } ?>
		            </table>
                    <?php if($invoice_details[0]->status != 1) { ?>
                        <table class="table">
                            <tr>
                                <th class="col-sm-8 col-xs-8"><?=$this->lang->line('invoice_total')." Payable (".$setschool->currency_code.")";?></th>
                                <td class="col-sm-4 col-xs-4" id="total0"><?=$setschool->currency_symbol?><?php echo intval($invoice_details[0]->amount); ?></td>
                               
                            </tr>
                        </table>
                    <?php } ?>
		        </div>
		    </div><!-- /.col -->
		</div><!-- /.row -->

		<!-- this row will not appear when printing -->
	</section><!-- /.content -->
</div>

<div class="well" style="border-radius: 0px; border: 0px; background: #fff;">
	<div class="row">
		<div class="col-xs-12 col-sm-6 col-lg-6 col-md-6">
                <!--p class="lead">Payment Methods:</p-->
                
                <img src="<?php echo base_url('uploads/images/rem.png'); ?>" class="img img-responsive printNone" style="width: 75%; margin-top: 0px;" id="remitaImage" />
                
                <!--p class="text-muted well well-sm no-shadow" style="margin-top: 10px; border-radius: 0px !important; width: 75%;">
                </p-->
            </div><!-- /.col -->
		
		<div class="col-md-6 col-xs-12 col-sm-6 col-lg-6 paynow" id="divButton" style="padding:0px; text-align: center; display:none;">
			<!--button class="btn btn-default btn-sm" style="border-radius: 0px !important;" onclick="printDiv('printablediv')"><span class="fa fa-print"></span> <?=$this->lang->line('print')?> </button-->
			 <?php
				//echo btn_add_pdf('invoice/print_preview/'.$invoice->invoiceID, $this->lang->line('pdf_preview')) 
			?>
			<?php
				if($invoice->paidamount != $invoice->amount) {
					//echo btn_payment('invoice/payment/'.$invoice->invoiceID, $this->lang->line('payment')); 
                    echo "<button class='btn btn-success btn-lg' onclick='submitForm()'>Proceed</button>" ;
				}
			?>
			<!--button class="btn btn-success btn-sm" data-toggle="modal" data-target="#mail"><span class="fa fa-envelope-o"></span> <?=$this->lang->line('mail')?></button-->                
		</div>
	</div>
</div>

<!-- Transaction fee popup start-->
<div class="modal fade transactionFeePopup">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
                    <h4 class="modal-title">Debit Card Payment</h4>
                </div>
                <div class="modal-body">
        <div id="rules">
        There is a fee of  NGN <span class="transFeePopup"></span> in order to Make this transaction. 
                </div>
                </div>
                <div class="modal-footer">
                
                
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                     <a id="ok1" href="#"><button type="button" data-dismiss="modal" class="btn btn-default">OK</button> </a>
                    

                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
</div>

<div class="modal fade bankFeePopup">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
                <h4 class="modal-title">Bank Payment</h4>
            </div>
            <div class="modal-body">
            <div id="rules">
                <h4>There is a fee of  NGN <span class="transFeePopup"></span> in order to Make this transaction. </h4>
                </br></br>

                Once you proceed with this payment, you will receive an invoice containing a Remita Retrieval Reference(RRR)</br></br>

                You can then take the invoice to any bank to complete your payment.</br></br>

                Just tell the teller that you would like to make a payment using Remita</br></br></br>

                Thank you.

            </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                 <a id="ok" href="#"><button type="button" data-dismiss="modal" class="btn btn-default">OK</button> </a>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
        <!-- /.modal-dialog -->
</div>
<!-- Transaction fee popup end-->

<!-- email modal starts here -->
<form class="form-horizontal" role="form" action="<?=base_url('teacher/send_mail');?>" method="post">
    <div class="modal fade" id="mail">
      <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title"><?=$this->lang->line('mail')?></h4>
            </div>
            <div class="modal-body">
            
                <?php 
                    if(form_error('to')) 
                        echo "<div class='form-group has-error' >";
                    else     
                        echo "<div class='form-group' >";
                ?>
                    <label for="to" class="col-sm-2 control-label">
                        <?=$this->lang->line("to")?>
                    </label>
                    <div class="col-sm-6">
                        <input type="email" class="form-control" id="to" name="to" value="<?=set_value('to')?>" >
                    </div>
                    <span class="col-sm-4 control-label" id="to_error">
                    </span>
                </div>

                <?php 
                    if(form_error('subject')) 
                        echo "<div class='form-group has-error' >";
                    else     
                        echo "<div class='form-group' >";
                ?>
                    <label for="subject" class="col-sm-2 control-label">
                        <?=$this->lang->line("subject")?>
                    </label>
                    <div class="col-sm-6">
                        <input type="text" class="form-control" id="subject" name="subject" value="<?=set_value('subject')?>" >
                    </div>
                    <span class="col-sm-4 control-label" id="subject_error">
                    </span>

                </div>

                <?php 
                    if(form_error('message')) 
                        echo "<div class='form-group has-error' >";
                    else     
                        echo "<div class='form-group' >";
                ?>
                    <label for="message" class="col-sm-2 control-label">
                        <?=$this->lang->line("message")?>
                    </label>
                    <div class="col-sm-6">
                        <textarea class="form-control" id="message" name="message" style="resize: vertical;" value="<?=set_value('message')?>" ></textarea>
                    </div>
                </div>

            
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" style="margin-bottom:0px;" data-dismiss="modal"><?=$this->lang->line('close')?></button>
                <input type="button" id="send_pdf" class="btn btn-success" value="<?=$this->lang->line("send")?>" />
            </div>
        </div>
      </div>
    </div>
</form>
<!-- email end here -->
<script language="javascript" type="text/javascript">
    function printInv(divID) {
        //Get the HTML of div
        // var contents = document.body.innerHTML;
        $(".take").remove();
        var divElements = document.getElementById(divID).innerHTML;
        //Get the HTML of whole page
        var oldPage = document.body.innerHTML;

        //Reset the page's HTML with div's HTML only
        document.body.innerHTML = 
          "<html><head><title></title></head><body>" + 
          divElements + "</body>";

        //Print Page
        window.print();

        //Restore orignal HTML
        document.body.innerHTML = oldPage;
    }
    function closeWindow() {
        location.reload(); 
    }

    function check_email(email) {
        var status = false;     
        var emailRegEx = /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i;
        if (email.search(emailRegEx) == -1) {
            $("#to_error").html('');
            $("#to_error").html("<?=$this->lang->line('mail_valid')?>").css("text-align", "left").css("color", 'red');
        } else {
            status = true;
        }
        return status;
    }


    $("#send_pdf").click(function(){
        var to = $('#to').val();
        var subject = $('#subject').val();
        var message = $('#message').val();
        var id = "<?=$invoice->invoiceID;?>";
        var error = 0;

        if(to == "" || to == null) {
            error++;
            $("#to_error").html("");
            $("#to_error").html("<?=$this->lang->line('mail_to')?>").css("text-align", "left").css("color", 'red');
        } else {
            if(check_email(to) == false) {
                error++
            }
        } 

        if(subject == "" || subject == null) {
            error++;
            $("#subject_error").html("");
            $("#subject_error").html("<?=$this->lang->line('mail_subject')?>").css("text-align", "left").css("color", 'red');
        } else {
            $("#subject_error").html("");
        }

        if(error == 0) {
            $.ajax({
                type: 'POST',
                url: "<?=base_url('invoice/send_mail')?>",
                data: 'to='+ to + '&subject=' + subject + "&id=" + id+ "&message=" + message,
                dataType: "html",
                success: function(data) {
                    location.reload();
                }
            });
        }
    });
    
</script>

<script type="text/javascript" src="<?php echo base_url('assets/cloudskul/spin.js'); ?>"> </script>
<?php if($invoice_details[0]->status == 2){ ?>
<script type="text/javascript">
$(document).ready(function(){
	$(".payOptions").hide();
	var data = "1500";
	$('#amt').html('N'+parseInt(data));
	var total = parseInt(<?=$invoice->amount?>) + parseInt(data);
	$('#total0').html('N'+parseInt(total));
	$("#bankOption").remove();

	$(".payDebit").click(function(){
		$(".payOptions").toggle("slow");
	});
});

</script>
<?php } ?>
<script type="text/javascript">

function submitForm(){
    document.getElementById("submitForm").submit();
}

$(function(){
 $('#ok').click(function(){
      $('.paynow').fadeIn('fast');
  });
});

$(function(){
 $('#ok1').click(function(){
      $('.paynow').fadeIn('fast');
  });
});

$('input[name=payment_type]:radio').change(function() {
    var type = $(this).val();
    var fees = $("#fees").val();
    var status = <?=$invoice->status?>;
    var service_charge_info = <?=$invoice->service_charge_info== "" ? "2" :$invoice->service_charge_info ?>;
    $('.paynow').fadeOut('fast');

    $.ajax({
        type: 'POST',
        url: "<?=base_url('new_invoice/payment_type')?>",
        data: "type=" + type+"&status="+status,
        dataType: "html",
        beforeSend:function(){
                createAjaxLoader();
        },
        success: function(data) {
            if(data != "") {
                if(type != 'BANK_BRANCH'){
                    $(".transFeePopup").html(data);
                    if(service_charge_info != "1")
                        $(".transactionFeePopup").modal("show");
                    else
                        $('.paynow').fadeIn('fast');
                }else if(type == 'BANK_BRANCH'){
                    $(".transFeePopup").html(data);
                    if(service_charge_info != "1")
                        $(".bankFeePopup").modal("show");
                    else
                        $('.paynow').fadeIn('fast');
                }
                $('#payment_type').val(type);
                $('#service_charge').val(data);

                if(service_charge_info != "1"){
                    var total = parseInt(fees) + parseInt(data);
                    $('#total').val(total);
                    $('#total0').html('N'+parseInt(total));
                    $('#amt').html('N'+parseInt(data));
                }else{
                    var total = parseInt(fees) ;
                    $('#total').val(total);
                    $('#total0').html('N'+parseInt(total));
                }

                deleteAjaxLoader();
               
            } else {
                $(".book").hide();
            }
           
        }
    });
});

</script>

<?php }} ?>
