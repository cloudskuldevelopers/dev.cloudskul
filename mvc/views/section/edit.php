<style type="text/css">
.fileUpload {
    margin: 0;
    overflow: hidden;
    position: relative;
}

.fileUpload input.upload {
    position: absolute;
    top: 0;
    right: 0;
    margin: 0;
    padding: 0;
    font-size: 20px;
    cursor: pointer;
    opacity: 0;
    filter: alpha(opacity=0);
}
</style>
<div class="row">
	<div class="col-md-2 hidden-xs">
		<img src="<?php echo base_url('uploads/images/logo.png'); ?>" style="width: auto; height: 40px; margin-top: -8px;" class="img-thumbnail img-responsive" />
	</div>
	<div class="col-md-7 col-xs-9 text-center">
		<h1 class="page-header"><?php echo $this->session->userdata('school');  ?></h1>
	</div>
	<div class="col-md-3 col-xs-3">
		<!-- begin breadcrumb -->
		<ol class="breadcrumb pull-right">
			<li><a href="<?=base_url("dashboard/index")?>"><i class="fa fa-laptop"></i> <?=$this->lang->line('menu_dashboard')?></a></li>
            <li><a href="<?=base_url("section/index/$set")?>"></i><?=$this->lang->line('menu_section')?></a></li>
            <li class="active"><?=$this->lang->line('menu_edit')?> <?=$this->lang->line('menu_section')?></li>
		</ol>
		<!-- end breadcrumb -->
	</div>
</div>

<div id="page-container">
<!-- begin panel -->
<div class="panel panel-inverse" data-sortable-id="ui-widget-3">
	<div class="panel-heading">
		<?php 
			$usertype = $this->session->userdata("usertype");
			if($usertype == "Admin" || $usertype == "Super Admin") {
		?>
			<div class="btn-group-vertical pull-right">
				<a href="<?php echo base_url('section/add') ?>" type="button" class="btn btn-success btn-xs"><i class="fa fa-plus"></i> Add Class Arm</a>
			</div>
		<?php } ?>
		<h4 class="panel-title"><i class="fa fa-desktop"></i> <?=$this->lang->line('panel_title')?></h4>
	</div>
	<div class="panel-body">
		<div class="col-sm-8">
			<form class="form-horizontal" role="form" method="post">

				<?php 
					if(form_error('section')) 
						echo "<div class='form-group has-error' >";
					else     
						echo "<div class='form-group' >";
				?>
					<label for="section" class="col-sm-2 control-label">
						<?=$this->lang->line("section_name")?>
					</label>
					<div class="col-sm-6">
						<input type="text" class="form-control" id="section" name="section" value="<?=set_value('section', $section->section)?>" >
					</div>
					<span class="col-sm-4 control-label">
						<?php echo form_error('section'); ?>
					</span>
				</div>

				<?php 
					if(form_error('category')) 
						echo "<div class='form-group has-error' >";
					else     
						echo "<div class='form-group' >";
				?>
					<label for="category" class="col-sm-2 control-label">
						<?=$this->lang->line("section_category")?>
					</label>
					<div class="col-sm-6">
						<input type="text" class="form-control" id="category" name="category" value="<?=set_value('category', $section->category)?>" >
					</div>
					<span class="col-sm-4 control-label">
						<?php echo form_error('category'); ?>
					</span>
				</div>

				<?php 
					if(form_error('classesID')) 
						echo "<div class='form-group has-error' >";
					else     
						echo "<div class='form-group' >";
				?>
					<label for="classesID" class="col-sm-2 control-label">
						<?=$this->lang->line("section_classes")?>
					</label>
					<div class="col-sm-6">
						
						<?php
							$array = array();
							$array[0] = $this->lang->line("section_select_class");

							foreach ($classes as $classa) {
								$array[$classa->classesID] = $classa->classes;
							}
							echo form_dropdown("classesID", $array, set_value("classesID", $section->classesID), "id='classesID' class='form-control'");
						?>
					</div>
					<span class="col-sm-4 control-label">
						<?php echo form_error('classesID'); ?>
					</span>
				</div>

				<?php 
					if(form_error('teacherID')) 
						echo "<div class='form-group has-error' >";
					else     
						echo "<div class='form-group' >";
				?>
					<label for="teacherID" class="col-sm-2 control-label">
						<?=$this->lang->line("section_teacher_name")?>
					</label>
					<div class="col-sm-6">
						
						<?php
							$array = array();
							$array[0] = $this->lang->line("section_select_teacher");

							foreach ($teachers as $teacher) {
								$array[$teacher->teacherID] = $teacher->name;
							}
							echo form_dropdown("teacherID", $array, set_value("teacherID", $section->teacherID), "id='teacherID' class='form-control'");
						?>
					</div>
					<span class="col-sm-4 control-label">
						<?php echo form_error('teacherID'); ?>
					</span>
				</div>

				<?php 
					if(form_error('note')) 
						echo "<div class='form-group has-error' >";
					else     
						echo "<div class='form-group' >";
				?>
					<label for="note" class="col-sm-2 control-label">
						<?=$this->lang->line("section_note")?>
					</label>
					<div class="col-sm-6">
						<textarea class="form-control" style="resize:none;" id="note" name="note"><?=set_value('note', $section->note)?></textarea>
					</div>
					<span class="col-sm-4 control-label">
						<?php echo form_error('note'); ?>
					</span>
				</div>

				<div class="form-group">
					<div class="col-sm-offset-2 col-sm-8">
						<input type="submit" class="btn btn-success" value="<?=$this->lang->line("add_class")?>" >
					</div>
				</div>

			</form>
		</div>
	</div>
</div>
</div>


