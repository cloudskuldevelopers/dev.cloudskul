<div class="row">
	<div class="col-md-2 hidden-xs">
		<img src="<?php echo base_url('uploads/images/logo.png'); ?>" style="width: auto; height: 40px; margin-top: -8px;" class="img-thumbnail img-responsive" />
	</div>
	<div class="col-md-7 col-xs-9 text-center">
		<h1 class="page-header"><?php echo $this->session->userdata('school');  ?></h1>
	</div>
	<div class="col-md-3 col-xs-3">
		<!-- begin breadcrumb -->
		<ol class="breadcrumb pull-right">
			<li ><a href="<?php echo base_url('dashboard/index');?>"><i class="fa fa-laptop"></i> Dashboard</a></li>
			<li class="active">Assessments</li>
		</ol>
		<!-- end breadcrumb -->
	</div>
</div>
<!-- begin panel -->
<div class="panel panel-inverse" data-sortable-id="ui-widget-3">
	<div class="panel-heading">
		<?php 
			$usertype = $this->session->userdata("usertype");
			if($usertype == "Super Admin" || $usertype == "Admin") {
		?>
		<div class="btn-group-vertical pull-right">
			<a href="<?php echo base_url('assessments/add') ?>" type="button" class="btn btn-success btn-xs"><i class="fa fa-plus"></i> Add Assessment</a>
		</div>
		<?php } ?>
		<h4 class="panel-title"><i class="fa fa-signal"></i> <?=$this->lang->line('panel_title')?></h4>
	</div>
	<div class="panel-body">
		<div id="hide-table">
			<table id="assessmentsple1" class="table table-striped table-bordered table-hover dataTable no-footer">
				<thead>
					<tr>
						<th class="col-lg-1"><?=$this->lang->line('slno')?></th>
						<th class="col-lg-3"><?=$this->lang->line('assessments_name')?></th>
						<th class="col-lg-3"><?=$this->lang->line('assessments_note')?></th>
						<th class="col-lg-2"><?=$this->lang->line('action')?></th>
					</tr>
				</thead>
				<tbody>
					<?php if(count($assessments)) {$i = 1; foreach($assessments as $assessments) { ?>
						<tr>
							<td data-title="<?=$this->lang->line('slno')?>">
								<?php echo $i; ?>
							</td>
							<td data-title="<?=$this->lang->line('assessments_name')?>">
								<?php echo $assessments->assessments; ?>
							</td>
							<td data-title="<?=$this->lang->line('assessments_note')?>">
								<?php echo $assessments->note; ?>
							</td>

							<td data-title="<?=$this->lang->line('action')?>">
								<?php echo btn_edit('assessments/edit/'.$assessments->testID, $this->lang->line('edit')) ?>
								<?php echo btn_delete('assessments/delete/'.$assessments->testID, $this->lang->line('delete')) ?>
							</td>
						</tr>
					<?php $i++; }} ?>
				</tbody>
			</table>
		</div>
	</div><!--panel body end-->
</div><!--panel end-->
    