<div class="row">
	<div class="col-md-2 hidden-xs">
		<img src="<?php echo base_url('uploads/images/logo.png'); ?>" style="width: auto; height: 40px; margin-top: -8px;" class="img-thumbnail img-responsive" />
	</div>
	<div class="col-md-7 col-xs-9 text-center">
		<h1 class="page-header"><?php echo $this->session->userdata('school');  ?></h1>
	</div>
	<div class="col-md-3 col-xs-3">
		<!-- begin breadcrumb -->
		<ol class="breadcrumb pull-right">
			<li ><a href="<?php echo base_url('dashboard/index');?>"><i class="fa fa-laptop"></i> Dashboard</a></li>
			<li ><a href="<?php echo base_url('assessments/index');?>">Assessments</a></li>
			<li class="active"><?=$this->lang->line('menu_add')?> <?=$this->lang->line('panel_title')?></li>
		</ol>
		<!-- end breadcrumb -->
	</div>
</div>
<!-- begin panel -->
<div class="panel panel-inverse" data-sortable-id="ui-widget-3">
	<div class="panel-heading">
		<h4 class="panel-title"><i class="fa fa-signal"></i> <?=$this->lang->line('panel_title')?></h4>
	</div>
	<div class="panel-body">
		<div class="col-sm-8">
		<form class="form-horizontal" role="form" method="post">

			<?php 
				if(form_error('assessments')) 
					echo "<div class='form-group has-error' >";
				else     
					echo "<div class='form-group' >";
			?>
				<label for="assessments" class="col-sm-2 control-label">
					<?=$this->lang->line("assessments_name")?>
				</label>
				<div class="col-sm-6">
					<input type="text" class="form-control" id="assessments" name="assessments" value="<?=set_value('assessments')?>" >
				</div>
				<span class="col-sm-4 control-label">
					<?php echo form_error('assessments'); ?>
				</span>
			</div>

			<?php 
				if(form_error('note')) 
					echo "<div class='form-group has-error' >";
				else     
					echo "<div class='form-group' >";
			?>
				<label for="note" class="col-sm-2 control-label">
					<?=$this->lang->line("assessments_note")?>
				</label>
				<div class="col-sm-6">
					<textarea style="resize:none;" class="form-control" id="note" name="note"><?=set_value('note')?></textarea>
				</div>
				<span class="col-sm-4 control-label">
					<?php echo form_error('note'); ?>
				</span>
			</div>

			<div class="form-group">
				<div class="col-sm-offset-2 col-sm-8">
					<input type="submit" class="btn btn-success" value="<?=$this->lang->line("add_assessments")?>" >
				</div>
			</div>

		</form>	
		</div>
	</div>
</div>

<script type="text/javascript">
$('#date').datepicker();
</script>
