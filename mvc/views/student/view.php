

<div class="row">
	<div class="col-md-2 hidden-xs">
		<img src="<?php echo base_url('uploads/images/logo.png'); ?>" style="width: auto; height: 40px; margin-top: -8px;" class="img-thumbnail img-responsive" />
	</div>
	<div class="col-md-7 col-xs-9 text-center">
		<h1 class="page-header"><?php echo $this->session->userdata('school');  ?></h1>
	</div>
	<div class="col-md-3 col-xs-3">
		<!-- begin breadcrumb -->
		<ol class="breadcrumb pull-right">
			<li><a href="<?=base_url("dashboard/index")?>"><i class="fa fa-laptop"></i> <?=$this->lang->line('menu_dashboard')?></a></li>
            <li><a href="<?=base_url("student/index")?>"><?=$this->lang->line('menu_student')?></a></li>
            <li class="active"><?=$this->lang->line('view')?></li>
		</ol>
		<!-- end breadcrumb -->
	</div>
</div>

<?php 
    if(count($student)) {
        $usertype = $this->session->userdata("usertype");
        if($usertype == "Admin" || $usertype == "Super Admin") {
?>
    <div class="well" style="background:#F0F3F5">
        <div class="row">
            <div class="col-sm-12">
                <button class="btn btn-success m-r-5 m-b-5" onclick="javascript:printDiv('printablediv')"><span class="fa fa-print"></span> <?=$this->lang->line('print')?> </button>							
                <?php
                 echo btn_add_pdf('student/print_preview/'.$student->studentID."/".$set, $this->lang->line('pdf_preview')) 
                ?>
                <button class="btn btn-success m-r-5 m-b-5" data-toggle="modal" data-target="#idCard"><span class="fa fa-floppy-o"></span> <?=$this->lang->line('idcard')?> </button>
                <?php echo btn_sm_edit('student/edit/'.$student->studentID."/".$set, $this->lang->line('edit')) 
                ?>                    
                <button class="btn btn-success m-r-5 m-b-5" data-toggle="modal" data-target="#mail"><span class="fa fa-envelope-o"></span> <?=$this->lang->line('mail')?></button>                
            </div>
        </div>

    </div>

    <?php } ?>

    <?php } ?>
    <div id="printablediv">
		<div class="profile-container" style="padding:0px;">
			<!-- begin profile-section -->
			<div class="profile-section">
				<div class="col-md-12 profile-head-bg">
					<div class="profile-left">
						<!-- begin profile-image -->
						<div class="profile-image" style="border:none;">
							<img src="<?php echo base_url('uploads/images/'.$student->photo);?>" width="170px" height="170px" />
							<i class="fa fa-user hide"></i>
						</div>
						<div class="profile-highlight">
							<h4><i class="fa fa-user"></i> <?=$student->name?> </h4>
							<div class="checkbox m-b-5 m-t-0">
								<?php echo $this->session->userdata('school');  ?>
							</div>
							<div class="checkbox m-b-0">
								<?=$this->lang->line("student_classes")." ".$class->classes?>
							</div>
						</div>
					</div>
				</div>
				<!-- end profile-left -->
				<!-- begin profile-right -->
				<div class="profile-right col-md-12">
					<!-- begin profile-info -->
					<div class="profile-info">
						<!-- begin table -->
						<div class="table-responsive">
							<table class="table table-profile">
								<tbody>
									<tr class="highlight">
										<td class="field" colspan="4" style="text-align:left;"><h4><?=$this->lang->line("personal_information")?> <small>about me</small></h4></td>
									</tr>
									<tr class="divider">
										<td colspan="4"></td>
									</tr>
									<tr>
										<td class="field"><?=$this->lang->line("student_roll")?> </td>
										<td><?=$student->roll?></td>
										<td class="field"><?=$this->lang->line("student_section")?> </td>
										<td><?php if(count($section)) { echo $section->section;} else { echo $student->section;}?></td>
									</tr>
									<tr>
										<td class="field"><?=$this->lang->line("student_dob")?> </td>
										<td><?=date("d M Y", strtotime($student->dob))?></td>
										<td class="field"><?=$this->lang->line("student_sex")?> </td>
										<td><?=$student->sex?></td>
									</tr>
									<tr>
										<td class="field"><?=$this->lang->line("student_religion")?> </td>
										<td><?=$student->religion?></td>
										<td class="field"><?=$this->lang->line("student_email")?> </td>
										<td><?=$student->email?></td>
									</tr>
									<tr>
										<td class="field"> <?=$this->lang->line("student_phone")?> </td>
										<td><i class="fa fa-mobile fa-lg m-r-5"></i> <?=$student->phone?></td>
										<td class="field"><?=$this->lang->line("student_address")?> </td>
										<td><?=$student->address?></td>
									</tr>
									<tr>
										<?php if($usertype == "Admin" || $usertype == "Super Admin") { ?>
										<td class="field"><?=$this->lang->line("student_username")?></td>
										<td><?=$student->username?></td>
										<?php } ?>
										<td class="field"></td>
										<td></td>
									</tr>
									
									<tr class="highlight">
										<td class="field" colspan="4" style="text-align:left;"><h4><?=$this->lang->line("parents_information")?> <small>about parent/guardian</small></h4></td>
									</tr>
									<tr class="divider">
										<td colspan="4"></td>
									</tr>
									<?php   if(count($parent)) { ?>
										<tr>
											<td class="field"><?=$this->lang->line("parent_guargian_name")?></td>
											<td><?=$parent->name?> </td>
											<td class="field"><?=$this->lang->line("parent_father_name")?> </td>
											<td><?=$parent->father_name?> </td>
										</tr>
										<tr>
											<td class="field"><?=$this->lang->line("parent_mother_name")?> </td>
											<td><?=$parent->mother_name?></td>
											<td class="field"><?=$this->lang->line("parent_father_profession")?> </td>
											<td><?=$parent->father_profession?></td>
										</tr>
										<tr>
											<td class="field"><?=$this->lang->line("parent_mother_profession")?> </td>
											<td><?=$parent->mother_profession?></td>
											<td class="field"><?=$this->lang->line("parent_email")?> </td>
											<td><?=$parent->email?></td>
										</tr>
										<tr>
											<td class="field"> <?=$this->lang->line("parent_phone")?> </td>
											<td><i class="fa fa-mobile fa-lg m-r-5"></i> <?=$parent->phone?></td>
											<td class="field"><?=$this->lang->line("parent_address")?> </td>
											<td><?=$parent->address?></td>
										</tr>
										<tr>
											<?php if($usertype == "Admin" || $usertype == "Super Admin") { ?>
											<td class="field"><?=$this->lang->line("parent_username")?></td>
											<td><?=$parent->username?></td>
											<?php } ?>
											<td class="field"></td>
											<td></td>
										</tr>
									<?php 
										} else { 
											echo "<div class='col-sm-12'><div class='col-sm-12 alert alert-warning'><span class='fa fa-exclamation-triangle'></span> " .$this->lang->line("parent_error"). "</div></div>";
										}
									?>
								</tbody>
							</table>
						</div>
						<!-- end table -->
					</div>
					<!-- end profile-info -->
				</div>
				<!-- end profile-right -->
			</div>
		</div>
    </div>

    <?php if($usertype == "Admin" || $usertype == "Super Admin") { ?>
    <!-- Modal content start here -->
    <div class="modal fade" id="idCard">
      <div class="modal-dialog">
        <div class="modal-content">
            <div id="idCardPrint">
              <div class="modal-header">
                <?=$this->lang->line('idcard')?>
              </div>
              <div class="modal-body" > 
                <table>
                    <tr>
                        <td>
                            <h4 style="margin:0;">
                            <?php 
                                if($siteinfos->photo) {
                                    $array = array(
                                        "src" => base_url('uploads/images/'.$this->session->userdata('schoollogo')),
                                        'width' => '25px',
                                        'height' => '25px',
                                        "style" => "margin-bottom:10px;"
                                    );
                                    echo img($array);
                                }
                                
                            ?>

                            </h4>
                        </td>
                        <td style="padding-left:5px;">
                            <h4><?=$this->session->userdata('school');?></h4>
                        </td>
                    </tr>
                </table>

                <table class="idcard-Table">
                    <tr>
                        <td>
                            <h4>
                                <?php 
                                    echo img(base_url('uploads/images/'.$student->photo));
                                ?>
                            </h4> 
                        </td>
                        <td class="row-style">
                            <h3><?php  echo $student->name; ?></h3>
                            <h5><?php  echo $this->lang->line("student_classes")." : ".$class->classes; ?>
                            </h5>
                            <h5><?php  echo $this->lang->line("student_section")." : ".$section->section; ?>
                            </h5>
                            <h5>
                                <?php  echo $this->lang->line("student_roll")." : ".$student->roll; ?>
                            </h5>
                        </td>
                    </tr>
                </table>    
              </div>
            </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" style="margin-bottom:0px;" onclick="javascript:closeWindow()" data-dismiss="modal"><?=$this->lang->line('close')?></button>
            <button type="button" class="btn btn-success" onclick="javascript:printDiv('idCardPrint')"><?=$this->lang->line('print')?></button>
          </div>
        </div>
      </div>
    </div>
    <!-- Modal content End here -->

<!-- email modal starts here -->
<form class="form-horizontal" role="form" action="<?=base_url('student/send_mail');?>" method="post">
    <div class="modal fade" id="mail">
      <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title"><?=$this->lang->line('mail')?></h4>
            </div>
            <div class="modal-body">
            
                <?php 
                    if(form_error('to')) 
                        echo "<div class='form-group has-error' >";
                    else     
                        echo "<div class='form-group' >";
                ?>
                    <label for="to" class="col-sm-2 control-label">
                        <?=$this->lang->line("to")?>
                    </label>
                    <div class="col-sm-6">
                        <input type="email" class="form-control" id="to" name="to" value="<?=set_value('to')?>" >
                    </div>
                    <span class="col-sm-4 control-label" id="to_error">
                    </span>
                </div>

                <?php 
                    if(form_error('subject')) 
                        echo "<div class='form-group has-error' >";
                    else     
                        echo "<div class='form-group' >";
                ?>
                    <label for="subject" class="col-sm-2 control-label">
                        <?=$this->lang->line("subject")?>
                    </label>
                    <div class="col-sm-6">
                        <input type="text" class="form-control" id="subject" name="subject" value="<?=set_value('subject')?>" >
                    </div>
                    <span class="col-sm-4 control-label" id="subject_error">
                    </span>

                </div>

                <?php 
                    if(form_error('message')) 
                        echo "<div class='form-group has-error' >";
                    else     
                        echo "<div class='form-group' >";
                ?>
                    <label for="message" class="col-sm-2 control-label">
                        <?=$this->lang->line("message")?>
                    </label>
                    <div class="col-sm-6">
                        <textarea class="form-control" id="message" style="resize: vertical;" name="message" value="<?=set_value('message')?>" ></textarea>
                    </div>
                </div>

            
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" style="margin-bottom:0px;" data-dismiss="modal"><?=$this->lang->line('close')?></button>
                <input type="button" id="send_pdf" class="btn btn-success" value="<?=$this->lang->line("send")?>" />
            </div>
        </div>
      </div>
    </div>
</form>
<!-- email end here -->

    <script language="javascript" type="text/javascript">
        function printDiv(divID) {
            //Get the HTML of div
            var divElements = document.getElementById(divID).innerHTML;
            //Get the HTML of whole page
            var oldPage = document.body.innerHTML;

            //Reset the page's HTML with div's HTML only
            document.body.innerHTML = 
              "<html><head><title></title></head><body>" + 
              divElements + "</body>";

            //Print Page
            window.print();

            //Restore orignal HTML
            document.body.innerHTML = oldPage;
        }
        function closeWindow() {
            location.reload(); 
        } 

        function check_email(email) {
            var status = false;     
            var emailRegEx = /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i;
            if (email.search(emailRegEx) == -1) {
                $("#to_error").html('');
                $("#to_error").html("<?=$this->lang->line('mail_valid')?>").css("text-align", "left").css("color", 'red');
            } else {
                status = true;
            }
            return status;
        }


        $("#send_pdf").click(function(){
            var to = $('#to').val();
            var subject = $('#subject').val();
            var message = $('#message').val();
            var id = "<?=$student->studentID;?>";
            var set = "<?=$set;?>";
            var error = 0;

            if(to == "" || to == null) {
                error++;
                $("#to_error").html("");
                $("#to_error").html("<?=$this->lang->line('mail_to')?>").css("text-align", "left").css("color", 'red');
            } else {
                if(check_email(to) == false) {
                    error++
                }
            } 

            if(subject == "" || subject == null) {
                error++;
                $("#subject_error").html("");
                $("#subject_error").html("<?=$this->lang->line('mail_subject')?>").css("text-align", "left").css("color", 'red');
            } else {
                $("#subject_error").html("");
            }

            if(error == 0) {
                $.ajax({
                    type: 'POST',
                    url: "<?=base_url('student/send_mail')?>",
                    data: 'to='+ to + '&subject=' + subject + "&id=" + id+ "&message=" + message+ "&set=" + set,
                    dataType: "html",
                    success: function(data) {
                        location.reload();
                    }
                });
            }
        });   
    </script>
    <?php } ?>
