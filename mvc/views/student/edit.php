<style type="text/css">
.fileUpload {
    margin: 0;
    overflow: hidden;
    position: relative;
}

.fileUpload input.upload {
    position: absolute;
    top: 0;
    right: 0;
    margin: 0;
    padding: 0;
    font-size: 20px;
    cursor: pointer;
    opacity: 0;
    filter: alpha(opacity=0);
}
</style>

<div class="row">
	<div class="col-md-2 hidden-xs">
		<img src="<?php echo base_url('uploads/images/logo.png'); ?>" style="width: auto; height: 40px; margin-top: -8px;" class="img-thumbnail img-responsive" />
	</div>
	<div class="col-md-7 col-xs-9 text-center">
		<h1 class="page-header"><?php echo $this->session->userdata('school');  ?></h1>
	</div>
	<div class="col-md-3 col-xs-3">
		<!-- begin breadcrumb -->
		<ol class="breadcrumb pull-right">
			<li><a href="<?=base_url("dashboard/index")?>"><i class="fa fa-laptop"></i> <?=$this->lang->line('menu_dashboard')?></a></li>
            <li><a href="<?=base_url("student/index")?>"><?=$this->lang->line('menu_student')?></a></li>
            <li class="active"><?=$this->lang->line('menu_edit')?> <?=$this->lang->line('panel_title')?></li>
		</ol>
		<!-- end breadcrumb -->
	</div>
</div>

<div id="page-container">
<!-- begin panel -->
<div class="panel panel-inverse" data-sortable-id="ui-widget-3">
	<div class="panel-heading">
		<h4 class="panel-title"><i class="fa fa-pencil"></i> <?=$this->lang->line('panel_title')?></h4>
	</div>
	<div class="panel-body">
		<div class="col-sm-8">
			<form class="form-horizontal" role="form" method="post" enctype="multipart/form-data">
			   
				<?php 
					if(form_error('name')) 
						echo "<div class='form-group has-error' >";
					else     
						echo "<div class='form-group' >";
				?>
					<label for="name_id" class="col-sm-2 control-label">
						<?=$this->lang->line("student_name")?>
					</label>
					<div class="col-sm-6">
						<input type="text" class="form-control" id="name_id" name="name" value="<?=set_value('name', $student->name)?>" >
					</div>
					<span class="col-sm-4 control-label">
						<?php echo form_error('name'); ?>
					</span>
				</div>

				<?php 
					if(form_error('guargianID')) 
						echo "<div class='form-group has-error' >";
					else     
						echo "<div class='form-group' >";
				?>
					<label for="guargianID" class="col-sm-2 control-label">
						<?=$this->lang->line("student_guargian")?>
					</label>
					<div class="col-sm-6">
						<div class="select2-wrapper">
							<?php
								$array = array('' => '');
								foreach ($parents as $parent) {
									$array[$parent->parentID] = $parent->name." (" . $parent->email ." )";
								}
								echo form_dropdown("guargianID", $array, set_value("guargianID", $student->parentID), "id='guargianID' class='form-control guargianID'");
							?>
						</div>
					</div>
					<span class="col-sm-4 control-label">
						<?php echo form_error('guargianID'); ?>
					</span>
				</div>

				<?php 
					if(form_error('dob')) 
						echo "<div class='form-group has-error' >";
					else     
						echo "<div class='form-group' >";
				?>
					<label for="dob" class="col-sm-2 control-label">
						<?=$this->lang->line("student_dob")?>
					</label>
					<div class="col-sm-6">
						<input type="text" class="form-control" id="dob" name="dob" value="<?=set_value('dob', date("d-m-Y", strtotime($student->dob)))?>" >
					</div>
					<span class="col-sm-4 control-label">
						<?php echo form_error('dob'); ?>
					</span>
				</div>

				<?php 
					if(form_error('sex')) 
						echo "<div class='form-group has-error' >";
					else     
						echo "<div class='form-group' >";
				?>
					<label for="sex" class="col-sm-2 control-label">
						<?=$this->lang->line("student_sex")?>
					</label>
					<div class="col-sm-6">
						<?php 
							echo form_dropdown("sex", array($this->lang->line('student_sex_male') => $this->lang->line('student_sex_male'), $this->lang->line('student_sex_female') => $this->lang->line('student_sex_female')), set_value("sex", $student->sex), "id='sex' class='form-control'");
						?>
					</div>
					<span class="col-sm-4 control-label">
						<?php echo form_error('sex'); ?>
					</span>

				</div>

			   
				<?php 
					if(form_error('religion')) 
						echo "<div class='form-group has-error' >";
					else     
						echo "<div class='form-group' >";
				?>
					<label for="religion" class="col-sm-2 control-label">
						<?=$this->lang->line("student_religion")?>
					</label>
					<div class="col-sm-6">
						<input type="text" class="form-control" id="religion" name="religion" value="<?=set_value('religion', $student->religion)?>" >
					</div>
					<span class="col-sm-4 control-label">
						<?php echo form_error('religion'); ?>
					</span>
				</div>

				<?php 
					if(form_error('email')) 
						echo "<div class='form-group has-error' >";
					else     
						echo "<div class='form-group' >";
				?>
					<label for="email" class="col-sm-2 control-label">
						<?=$this->lang->line("student_email")?>
					</label>
					<div class="col-sm-6">
						<input type="text" class="form-control" id="email" name="email" value="<?=set_value('email', $student->email)?>" >
					</div>
					<span class="col-sm-4 control-label">
						<?php echo form_error('email'); ?>
					</span>
				</div>

				<?php 
					if(form_error('phone')) 
						echo "<div class='form-group has-error' >";
					else     
						echo "<div class='form-group' >";
				?>
					<label for="phone" class="col-sm-2 control-label">
						<?=$this->lang->line("student_phone")?>
					</label>
					<div class="col-sm-6">
						<input type="text" class="form-control" id="phone" name="phone" value="<?=set_value('phone', $student->phone)?>" >
					</div>
					<span class="col-sm-4 control-label">
						<?php echo form_error('phone'); ?>
					</span>
				</div>

				<?php 
					if(form_error('address')) 
						echo "<div class='form-group has-error' >";
					else     
						echo "<div class='form-group' >";
				?>
					<label for="address" class="col-sm-2 control-label">
						<?=$this->lang->line("student_address")?>
					</label>
					<div class="col-sm-6">
						<input type="text" class="form-control" id="address" name="address" value="<?=set_value('address', $student->address)?>" >
					</div>
					<span class="col-sm-4 control-label">
						<?php echo form_error('address'); ?>
					</span>
				</div>

				<?php 
					if(form_error('classesID')) 
						echo "<div class='form-group has-error' >";
					else     
						echo "<div class='form-group' >";
				?>
					<label for="classesID" class="col-sm-2 control-label">
						<?=$this->lang->line("student_classes")?>
					</label>
					<div class="col-sm-6">
					   <?php
							$array = array(0 => $this->lang->line("student_select_class"));
							foreach ($classes as $classa) {
								$array[$classa->classesID] = $classa->classes;
							}
							echo form_dropdown("classesID", $array, set_value("classesID", $student->classesID), "id='classesID' class='form-control'");
						?>
					</div>
					<span class="col-sm-4 control-label">
						<?php echo form_error('classesID'); ?>
					</span>
				</div>

				<?php 
					if(form_error('sectionID')) 
						echo "<div class='form-group has-error' >";
					else     
						echo "<div class='form-group' >";
				?>
					<label for="sectionID" class="col-sm-2 control-label">
						<?=$this->lang->line("student_section")?>
					</label>
					<div class="col-sm-6">
					   <?php
							$array = array(0 => $this->lang->line("student_select_section"));
							foreach ($sections as $section) {
								$array[$section->sectionID] = $section->section;
							}
							echo form_dropdown("sectionID", $array, set_value("sectionID", $student->sectionID), "id='sectionID' class='form-control'");
						?>
					</div>
					<span class="col-sm-4 control-label">
						<?php echo form_error('sectionID'); ?>
					</span>
				</div>

				<?php 
					if(form_error('roll')) 
						echo "<div class='form-group has-error' >";
					else     
						echo "<div class='form-group' >";
				?>
					<label for="roll" class="col-sm-2 control-label">
						<?=$this->lang->line("student_roll")?>
					</label>
					<div class="col-sm-6">
						<input type="text" class="form-control" id="roll" name="roll" value="<?=set_value('roll', $student->roll)?>" >
					</div>
					<span class="col-sm-4 control-label">
						<?php echo form_error('roll'); ?>
					</span>
				</div>

				<?php 
					if(isset($image)) 
						echo "<div class='form-group has-error' >";
					else     
						echo "<div class='form-group' >";
				?>
					<label for="photo" class="col-sm-2 control-label col-xs-8 col-md-2">
						<?=$this->lang->line("student_photo")?>
					</label>
					<div class="col-sm-4 col-xs-6 col-md-4">
						<input class="form-control"  id="uploadFile" placeholder="Choose File" disabled />  
					</div>

					<div class="col-sm-2 col-xs-6 col-md-2">
						<div class="fileUpload btn btn-success form-control">
							<span class="fa fa-repeat"></span>
							<span><?=$this->lang->line("upload")?></span>
							<input id="uploadBtn" type="file" class="upload" name="image" />
						</div>
					</div>
					 <span class="col-sm-4 control-label col-xs-6 col-md-4">
					   
						<?php if(isset($image)) echo $image; ?>
					</span>
				</div>

				<div class="form-group">
					<div class="col-sm-offset-2 col-sm-8">
						<input type="submit" class="btn btn-success" value="<?=$this->lang->line("update_student")?>" >
					</div>
				</div>

			</form>

		</div>
	</div>
</div>

<script type="text/javascript">
document.getElementById("uploadBtn").onchange = function() {
    document.getElementById("uploadFile").value = this.value;
};
$('#dob').datepicker({ startView: 2 });

$('#classesID').change(function(event) {
    var classesID = $(this).val();
    if(classesID === '0') {
        $('#classesID').val(0);
    } else {
        $.ajax({
            type: 'POST',
            url: "<?=base_url('student/sectioncall')?>",
            data: "id=" + classesID,
            dataType: "html",
            success: function(data) {
               $('#sectionID').html(data);
            }
        });
    }
});


</script>
