
	<!-- Header -->

<div class="row">
	<div class="col-md-2 hidden-xs">
		<img src="<?php echo base_url('uploads/images/logo.png'); ?>" style="width: auto; height: 40px; margin-top: -8px;" class="img-thumbnail img-responsive" />
	</div>
	<div class="col-md-7 col-xs-9 text-center">
		<h1 class="page-header"><?php echo $this->session->userdata('school');  ?></h1>
	</div>
	<div class="col-md-3 col-xs-3">
		<!-- begin breadcrumb -->
		<ol class="breadcrumb pull-right">
			<li ><a href="<?php echo base_url('dashboard/index');?>"><i class="fa fa-laptop"></i> Dashboard</a></li>
			<li class="">Expense</li>
		</ol>
		<!-- end breadcrumb -->
	</div>
</div>
<div id="page-container">
<!-- begin panel -->
<div class="panel panel-inverse" data-sortable-id="ui-widget-3">
	<div class="panel-heading">
		<?php 
			$usertype = $this->session->userdata("usertype");
			if($usertype == "Super Admin" || $usertype == "Admin") {
		?>
		<div class="btn-group-vertical pull-right">
			<a href="<?php echo base_url('expense/add') ?>" type="button" class="btn btn-success btn-xs"><i class="fa fa-plus"></i> Add an Expense</a>
		</div>
		<?php } ?>
		<h4 class="panel-title"><i class="fa fa-pencil"></i> <?=$this->lang->line('panel_title')?></h4>
	</div>

	<div class="panel-body">
		<?php 
			$usertype = $this->session->userdata("usertype");
			if($usertype == "Super Admin" || $usertype == "Admin") {
		?>
		<table id="example1" class="table table-striped table-bordered table-hover dataTable no-footer">
			<thead>
				<tr>
					<th class="col-sm-1"><?=$this->lang->line('slno')?></th>
					<th class="col-sm-2"><?=$this->lang->line('expense_expense')?></th>
					<th class="col-sm-2"><?=$this->lang->line('expense_date')?></th>
					<th class="col-sm-2"><?=$this->lang->line('expense_uname')?></th>
					<th class="col-sm-1"><?=$this->lang->line('expense_amount')?></th>
					<th class="col-sm-2"><?=$this->lang->line('expense_note')?></th>
					<?php if($usertype == "Admin" || $usertype == "Super Admin") { ?>
					<th class="col-sm-2"><?=$this->lang->line('action')?></th>
					<?php } ?>
				</tr>
			</thead>
			<tbody>
				<?php $total_expense = 0; if(count($expenses)) {$i = 1; foreach($expenses as $expense) { ?>
					<tr>
						<td data-title="<?=$this->lang->line('slno')?>">
							<?php echo $i; ?>
						</td>
						<td data-title="<?=$this->lang->line('expense_expense')?>">
							<?php echo $expense->expense; ?>
						</td>
						<td data-title="<?=$this->lang->line('expense_date')?>">
							<?php echo date("d M Y", strtotime($expense->date)); ?>
						</td>
						
						<td data-title="<?=$this->lang->line('expense_uname')?>">
							<?php echo $expense->uname; ?>
						</td>

						<?php if($usertype == "Admin") { ?>
						<td data-title="<?=$this->lang->line('expense_amount')?>">
							<?php echo $expense->amount; ?>
						</td>
						<?php } else { ?>
						<td data-title="<?=$this->lang->line('expense_amount')?>">
							<?php echo $expense->amount; ?>
						</td>
						<?php } ?>

						<?php if($usertype == "Admin") { ?>
						<td data-title="<?=$this->lang->line('expense_note')?>">
							<?php echo $expense->note; ?>
						</td>
						<?php } else { ?>
						<td data-title="<?=$this->lang->line('expense_note')?>">
							<?php echo $expense->note; ?>
						</td>
						<?php } ?>

						<?php if($usertype == "Admin" || $usertype == "Super Admin") { ?>
						<td data-title="<?=$this->lang->line('action')?>">
							<?php echo btn_edit('expense/edit/'.$expense->expenseID, $this->lang->line('edit')) ?>
							<?php echo btn_delete('expense/delete/'.$expense->expenseID, $this->lang->line('delete')) ?>
						</td>
						<?php } ?>

						
					</tr>
				<?php $i++; $total_expense+=$expense->amount; }} ?>
			</tbody>
		</table>
		<div class="col-sm-4 col-sm-offset-8 total-marg">
			<div class="well well-sm" style="margin-top: 20px; border-radius: 0px !important;">
				<table style="width:100%; margin:0px;">
					<tr>
						<td width="50%">
							<?php
								echo $this->lang->line('expense_total')." : ";
							?>
						</td>
						<td style="width:50%;padding-left:10px">
							<?php
								echo $total_expense. " TK"; 
							?>
						</td>
					</tr>
				</table>
			</div>
		</div>
		<?php } ?>
    </div>
</div>
</div>
			
			