
<!-- Header -->

<div class="row">
	<div class="col-md-1 hidden-xs">
		<img src="<?php echo base_url('uploads/images/logo.png'); ?>" style="width: auto; height: 40px; margin-top: -8px;" class="img-thumbnail img-responsive" />
	</div>
	<div class="col-md-7 col-xs-9 text-center">
		<h1 class="page-header"><?php echo $this->session->userdata('school');  ?></h1>
	</div>
	<div class="col-md-4 col-xs-3">
		<!-- begin breadcrumb -->
		<ol class="breadcrumb pull-right">
			<li ><a href="<?php echo base_url('dashboard/index');?>"><i class="fa fa-laptop"></i> Dashboard</a></li>
			<li ><a href="<?php echo base_url('expense/index');?>">Expense</a></li>
			<li class="">Edit</li>
		</ol>
		<!-- end breadcrumb -->
	</div>
</div>
<div id="page-container">
<!-- begin panel -->
<div class="panel panel-inverse" data-sortable-id="ui-widget-3">
	<div class="panel-heading">
		<?php 
			$usertype = $this->session->userdata("usertype");
			if($usertype == "Super Admin" || $usertype == "Admin") {
		?>
		
		
		<!--
		<div class="btn-group-vertical pull-right">
			<a href="<?php echo base_url('expense/index') ?>" type="button" class="btn btn-success btn-xs"><i class="fa fa-plus"></i> Edit Expense</a>
		</div>
		-->
		
		<?php } ?>
		<h4 class="panel-title"><i class="fa fa-pencil"></i> <?=$this->lang->line('panel_title')?></h4>
	</div>
	
	
	
	<!--End header -->

	<div class="panel-body">
		<div class="col-sm-8">
			<form class="form-horizontal" role="form" method="post">

				<?php 
					if(form_error('expense')) 
						echo "<div class='form-group has-error' >";
					else     
						echo "<div class='form-group' >";
				?>
					<label for="namea" class="col-sm-2 control-label">
						<?=$this->lang->line("expense_expense")?>
					</label>
					<div class="col-sm-6">
						<input type="text" class="form-control" id="namea" name="expense" value="<?=set_value('expense', $expense->expense)?>" >
					</div>
					<span class="col-sm-4 control-label">
						<?php echo form_error('expense'); ?>
					</span>
				</div>

				<?php 
					if(form_error('date')) 
						echo "<div class='form-group has-error' >";
					else     
						echo "<div class='form-group' >";
				?>
					<label for="date" class="col-sm-2 control-label">
						<?=$this->lang->line("expense_date")?>
					</label>
					<div class="col-sm-6">
						<input type="text" class="form-control" id="date" name="date" value="<?=set_value('date', date("d-m-Y", strtotime($expense->date)))?>" >
					</div>
					<span class="col-sm-4 control-label">
						<?php echo form_error('date'); ?>
					</span>
				</div>

				<?php 
					if(form_error('amount')) 
						echo "<div class='form-group has-error' >";
					else     
						echo "<div class='form-group' >";
				?>
					<label for="amount" class="col-sm-2 control-label">
						<?=$this->lang->line("expense_amount")?>
					</label>
					<div class="col-sm-6">
						
					   <input type="text" class="form-control" id="amount" name="amount" value="<?=set_value('amount', $expense->amount)?>" >
					</div>
					<span class="col-sm-4 control-label">
						<?php echo form_error('amount'); ?>
					</span>
				</div>

				<?php 
					if(form_error('note')) 
						echo "<div class='form-group has-error' >";
					else     
						echo "<div class='form-group' >";
				?>
					<label for="note" class="col-sm-2 control-label">
						<?=$this->lang->line("expense_note")?>
					</label>
					<div class="col-sm-6">
						<textarea style="resize:none;" class="form-control" id="note" name="note"><?=set_value('note', $expense->note)?></textarea>
					</div>
					<span class="col-sm-4 control-label">
						<?php echo form_error('note'); ?>
					</span>
				</div>

				<div class="form-group">
					<div class="col-sm-offset-2 col-sm-8">
						<input type="submit" class="btn btn-success" value="<?=$this->lang->line("update_expense")?>" >
					</div>
				</div>

			</form>
		</div>
    </div>
</div>
</div>

<script type="text/javascript">
    $("#date").datepicker();
</script>
