
<div class="box box-danger">
    <div class="box-header with-border">
        <h3 class="box-title"><i class="fa icon-school"></i> <?=$this->lang->line('panel_title')?></h3>

       
        <ol class="breadcrumb">
            <li><a href="<?=base_url("dashboard/index")?>"><i class="fa fa-laptop"></i> <?=$this->lang->line('menu_dashboard')?></a></li>
            <li><a href="<?=base_url("school/index")?>"><?=$this->lang->line('menu_school')?></a></li>
            <li class="active"><?=$this->lang->line('menu_add')?> <?=$this->lang->line('menu_school')?></li>
        </ol>
    </div><!-- /.box-header -->
    <!-- form start -->
    <div class="box-body">
        <div class="row">
            <div class="col-sm-8">
                <form class="form-horizontal" role="form" method="post" enctype="multipart/form-data">

                    <?php 
                        if(form_error('school')) 
                            echo "<div class='form-group has-error' >";
                        else     
                            echo "<div class='form-group' >";
                    ?>
                        <label for="school" class="col-sm-2 control-label">
                            <?=$this->lang->line("school_school")?>
                        </label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" id="school" name="school" value="<?=set_value('school')?>" >
                        </div>
                        <span class="col-sm-4 control-label">
                            <?php echo form_error('school'); ?>
                        </span>
                    </div>

                    <?php 
                        if(form_error('alias')) 
                            echo "<div class='form-group has-error' >";
                        else     
                            echo "<div class='form-group' >";
                    ?>
                        <label for="alias" class="col-sm-2 control-label">
                            <?=$this->lang->line("school_alias")?>
                        </label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" id="alias" name="alias" value="<?=set_value('alias')?>" >
                        </div>
                        <span class="col-sm-4 control-label">
                            <?php echo form_error('alias'); ?>
                        </span>
                    </div>

                    <?php 
                        if(form_error('email')) 
                            echo "<div class='form-group has-error' >";
                        else     
                            echo "<div class='form-group' >";
                    ?>
                        <label for="email" class="col-sm-2 control-label">
                            <?=$this->lang->line("school_email")?>
                        </label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" id="email" name="email" value="<?=set_value('email')?>" >
                        </div>
                        <span class="col-sm-4 control-label">
                            <?php echo form_error('email'); ?>
                        </span>
                    </div>

                    <?php 
                        if(form_error('phone')) 
                            echo "<div class='form-group has-error' >";
                        else     
                            echo "<div class='form-group' >";
                    ?>
                        <label for="phone" class="col-sm-2 control-label">
                            <?=$this->lang->line("school_phone")?>
                        </label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" id="phone" name="phone" value="<?=set_value('phone')?>" >
                        </div>
                        <span class="col-sm-4 control-label">
                            <?php echo form_error('phone'); ?>
                        </span>
                    </div>

                    <?php 
                        if(form_error('capacity')) 
                            echo "<div class='form-group has-error' >";
                        else     
                            echo "<div class='form-group' >";
                    ?>
                        <label for="capacity" class="col-sm-2 control-label">
                            <?=$this->lang->line("school_capacity")?>
                        </label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" id="capacity" name="capacity" value="<?=set_value('capacity')?>" >
                        </div>
                        <span class="col-sm-4 control-label">
                            <?php echo form_error('capacity'); ?>
                        </span>
                    </div>

                    <?php 
                        if(form_error('automation')) 
                            echo "<div class='form-group has-error' >";
                        else     
                            echo "<div class='form-group' >";
                    ?>
                        <label for="automation" class="col-sm-2 control-label">
                            <?=$this->lang->line("school_automation")?>
                        </label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" id="automation" name="automation" value="<?=set_value('automation')?>" >
                        </div>
                        <span class="col-sm-4 control-label">
                            <?php echo form_error('automation'); ?>
                        </span>
                    </div>

                    <?php 
                        if(form_error('currency_code')) 
                            echo "<div class='form-group has-error' >";
                        else     
                            echo "<div class='form-group' >";
                    ?>
                        <label for="currency_code" class="col-sm-2 control-label">
                            <?=$this->lang->line("school_currency_code")?>
                        </label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" id="currency_code" name="currency_code" value="<?=set_value('currency_code')?>" >
                        </div>
                        <span class="col-sm-4 control-label">
                            <?php echo form_error('currency_code'); ?>
                        </span>
                    </div>

                    <?php 
                        if(form_error('currency_symbol')) 
                            echo "<div class='form-group has-error' >";
                        else     
                            echo "<div class='form-group' >";
                    ?>
                        <label for="currency_symbol" class="col-sm-2 control-label">
                            <?=$this->lang->line("school_currency_symbol")?>
                        </label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" id="currency_symbol" name="currency_symbol" value="<?=set_value('currency_symbol')?>" >
                        </div>
                        <span class="col-sm-4 control-label">
                            <?php echo form_error('currency_symbol'); ?>
                        </span>
                    </div>

                    <?php 
                        if(form_error('footer')) 
                            echo "<div class='form-group has-error' >";
                        else     
                            echo "<div class='form-group' >";
                    ?>
                        <label for="footer" class="col-sm-2 control-label">
                            <?=$this->lang->line("school_footer")?>
                        </label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" id="footer" name="footer" value="<?=set_value('footer')?>" >
                        </div>
                        <span class="col-sm-4 control-label">
                            <?php echo form_error('footer'); ?>
                        </span>
                    </div>

                    <?php 
                        if(isset($image)) 
                            echo "<div class='form-group has-error' >";
                        else     
                            echo "<div class='form-group' >";
                    ?>
                        <label for="photo" class="col-sm-2 control-label">
                            <?=$this->lang->line("school_logo")?>
                        </label>
                        <div class="col-sm-4">
                            <input class="form-control"  id="uploadFile" placeholder="Choose File" disabled />  
                        </div>

                        <div class="col-sm-2">
                            <div class="fileUpload btn btn-success form-control">
                                <span class="fa fa-repeat"></span>
                                <span><?=$this->lang->line("upload_setting")?></span>
                                <input id="uploadBtn" type="file" class="upload" name="image" />
                            </div>
                        </div>
                         <span class="col-sm-4 control-label">
                            <?php if(isset($image)) echo $image; ?>
                        </span>
                    </div>

                    <?php 
                        if(form_error('address')) 
                            echo "<div class='form-group has-error' >";
                        else     
                            echo "<div class='form-group' >";
                    ?>
                        <label for="address" class="col-sm-2 control-label">
                            <?=$this->lang->line("school_address")?>
                        </label>
                        <div class="col-sm-6">
                            <textarea class="form-control" style="resize:none;" id="address" name="address"><?=set_value('address')?></textarea>
                        </div>
                        <span class="col-sm-4 control-label">
                            <?php echo form_error('address'); ?>
                        </span>
                    </div>



                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-8">
                            <input type="submit" class="btn btn-success" value="<?=$this->lang->line("add_school")?>" >
                        </div>
                    </div>

                </form>
            </div>    
        </div>
    </div>
</div>

<script type="text/javascript">
document.getElementById("uploadBtn").onchange = function() {
    document.getElementById("uploadFile").value = this.value;
};
</script>
