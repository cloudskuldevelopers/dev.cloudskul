<!-- TOP BAR -->
<div class="row">
	<div class="col-md-2 hidden-xs">
		<img src="<?php echo base_url('uploads/images/logo.png'); ?>" style="width: auto; height: 40px; margin-top: -8px;" class="img-thumbnail img-responsive" />
	</div>
	<div class="col-md-6 col-xs-9 text-center">
		<h1 class="page-header"><?php echo $this->session->userdata('school');  ?></h1>
	</div>
	<div class="col-md-4 col-xs-3">
		<!-- begin breadcrumb -->
		<ol class="breadcrumb pull-right">
			<li ><a href="<?php echo base_url('dashboard/index');?>"><i class="fa fa-laptop"></i> Dashboard</a></li>
			<li><a href="<?php echo base_url('bank_account/index');?>">Bank Account</a></li>
			<li class="">Edit Bank Account</li>
		</ol>
		<!-- end breadcrumb -->
	</div>
</div>
<!-- END TOP BAR -->

<div id="page-container">
<!-- begin panel -->
<div class="panel panel-inverse" data-sortable-id="ui-widget-3">
	<div class="panel-heading">
		<h4 class="panel-title"><i class="fa fa-institution"></i> <?=$this->lang->line('panel_title')?></h4>
	</div>
	<div class="panel-body">
		<?php echo $this->session->userdata('schoolID'); ?>
		<h3 class="profileBody" style="font-weight: lighter; font-size: 22px; margin-bottom: 30px;">Edit Bank Account Details</h3>
		<form class="form-horizontal" role="form" method="post" enctype="multipart/form-data">
			<?php 
				if(form_error('school')) 
					echo "<div class='form-group has-error' >";
				else     
					echo "<div class='form-group col-md-12' >";
			?>
				<label for="school" class="col-sm-2 col-xs-2 col-md-2 control-label">
					 <?=$this->lang->line("account_name")?>
				</label>
				<div class="col-md-6 col-xs-10 col-sm-10">
					<div class="input-group">
						<span class="input-group-addon"><i class="fa fa-institution"></i></span>
						<input type="text" class="form-control" id="account_name" name="account_name" value="<?=$account->account_name?>">
					</div>
				</div>
				<span class="col-md-4 col-xs-12 col-sm-12 control-label">
					<?php echo form_error('account_name'); ?>
				</span>
			</div>
			
			<?php 
				if(form_error('school')) 
					echo "<div class='form-group has-error' >";
				else     
					echo "<div class='form-group col-md-12' >";
			?>
				<label for="school" class="col-sm-2 col-xs-2 col-md-2 control-label">
					 <?=$this->lang->line("account_number")?>
				</label>
				<div class="col-md-6 col-xs-10 col-sm-10">
					<div class="input-group">
						<span class="input-group-addon"><i class="fa fa-barcode"></i></span>
						<input type="text" class="form-control" id="account_number" name="account_number" value="<?=$account->account_number?>">
					</div>
				</div>
				<span class="col-md-4 col-xs-12 col-sm-12 control-label">
					<?php echo form_error('account_number'); ?>
				</span>
			</div>

			<?php 
				if(form_error('school')) 
					echo "<div class='form-group has-error' >";
				else     
					echo "<div class='form-group col-md-12' >";
			?>
				<label for="school" class="col-sm-2 col-xs-2 col-md-2 control-label">
					 <?=$this->lang->line("account_type")?>
				</label>
				<div class="col-md-6 col-xs-10 col-sm-10">
					<div class="input-group">
						<span class="input-group-addon"><i class="fa fa-th-large"></i></span>
						<input type="text" class="form-control" id="account_type" name="account_type" value="<?=$account->account_type?>">
					</div>
				</div>
				<span class="col-md-4 col-xs-12 col-sm-12 control-label">
					 <?php echo form_error('account_type'); ?>
				</span>
			</div>
			
			<?php 
				if(form_error('school')) 
					echo "<div class='form-group has-error' >";
				else     
					echo "<div class='form-group col-md-12' >";
			?>
				<label for="school" class="col-sm-2 col-xs-2 col-md-2 control-label">
					 <?=$this->lang->line("bank_name")?>
				</label>
				<div class="col-md-6 col-xs-10 col-sm-10">
					<div class="input-group">
						<span class="input-group-addon"><i class="fa fa-tags"></i></span>
						 <input type="text" class="form-control" id="bank_name" name="bank_name" value="<?=$account->bank_name?>">
						 <div class="book"><ul  class="result"></ul></div>
					</div>
				</div>
				<span class="col-md-4 col-xs-12 col-sm-12 control-label">
					 <?php echo form_error('bank_name'); ?>
				</span>
			</div>
			<?php 
				if(form_error('school')) 
					echo "<div class='form-group has-error' >";
				else     
					echo "<div class='form-group col-md-12' >";
			?>
				<label for="school" class="col-sm-2 col-xs-2 col-md-2 control-label">
					 <?=$this->lang->line("bank_code")?>
				</label>
				<div class="col-md-6 col-xs-10 col-sm-10">
					<div class="input-group">
						<span class="input-group-addon" style="background: rgba(238, 238, 238, 0.68)"><i class="fa fa-list-ol"></i></span>
						  <input type="text" class="form-control" id="bank_code" readonly="readonly" name="bank_code" value="<?=$account->bank_code?>" />
					</div>
				</div>
				<span class="col-md-4 col-xs-12 col-sm-12 control-label">
					 <?php echo form_error('bank_code'); ?>
				</span>
			</div>

			<div class="col-md-12" style="border-top: 1px solid rgba(220, 220, 220, 0.45); margin-top: 20px; padding-top: 20px; margin-bottom: 0px;">
				<div class="form-group">
					<div class="col-sm-offset-2 col-xs-offset-2 col-sm-6">
					<button type="submit" class="btn btn-lg btn-success btn-block" style="width: 97%; margin-left: -7px;">
						<i class="fa fa-check-circle" style="margin-left:8px;"></i>
						<?=$this->lang->line("update_bank")?>
					</button>
					</div>
				</div>
			</div>

		</form>
	</div>
</div>

<script type="text/javascript">

$('#bank_name').keyup(function() {
    var bank_name = $(this).val();
    //alert(bank_name);
    $.ajax({
        type: 'POST',
        url: "<?=base_url('bank_account/bankname')?>",
        data: "bank_name=" + bank_name,
        dataType: "html",
        success: function(data) {
            if(data != "") {
                var width = $("#bank_name").width();
                $(".book").css('width', width+25 + "px").show();
                $(".result").html(data);

                $('.result li').click(function(){
                    var result_value = $(this).text();
                    var result_code = $(this).attr('id');
                    $('#bank_name').val(result_value);
                    $('#bank_code').val(result_code);
                    $('.result').html(' ');
                    $('.book').hide();
                });
            } else {
                $(".book").hide();
            }
           
        }
    });
});

</script>
