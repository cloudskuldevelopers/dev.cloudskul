<div class="row">
	<div class="col-md-2 hidden-xs">
		<img src="<?php echo base_url('uploads/images/logo.png'); ?>" style="width: auto; height: 40px; margin-top: -8px;" class="img-thumbnail img-responsive" />
	</div>
	<div class="col-md-7 col-xs-9 text-center">
		<h1 class="page-header"><?php echo $this->session->userdata('school');  ?></h1>
	</div>
	<div class="col-md-3 col-xs-3">
		<!-- begin breadcrumb -->
		<ol class="breadcrumb pull-right">
			<li ><a href="<?php echo base_url('dashboard/index');?>"><i class="fa fa-laptop"></i> Dashboard</a></li>
			<li class="">Bank Account</li>
		</ol>
		<!-- end breadcrumb -->
	</div>
</div>
<div id="page-container">
<!-- begin panel -->
<div class="panel panel-inverse" data-sortable-id="ui-widget-3">
	<div class="panel-heading">
		<?php 
			$usertype = $this->session->userdata("usertype");
			if($usertype == "Super Admin" || $usertype == "Admin") {
		?>
		<div class="btn-group-vertical pull-right">
			<a href="<?php echo base_url('bank_account/addaccount') ?>" type="button" class="btn btn-success btn-xs"><i class="fa fa-plus"></i> Add Bank Account</a>
		</div>
		<?php } ?>
		<h4 class="panel-title"><i class="fa fa-pencil"></i> <?=$this->lang->line('panel_title')?></h4>
	</div>
	<div class="panel-body">
		<?php 
			$usertype = $this->session->userdata("usertype");
			if($usertype == "Super Admin" || $usertype == "Admin") {
		?>
		<table class="table">
			<thead>
				<tr>
					<th><?=$this->lang->line('slno')?></th>
					 <?php 
						$usertype = $this->session->userdata("usertype");
						if($usertype == "Admin") {
					?>
					<th class="col-lg-3"><?=$this->lang->line('bank_name')?></th>
					<?php } ?>
					<th class="col-lg-2"><?=$this->lang->line('account_name')?></th>
					<th class="col-lg-2"><?=$this->lang->line('account_number')?></th>
					<th class="col-lg-2"><?=$this->lang->line('account_type')?></th>
					<th class="col-lg-1"><?=$this->lang->line('bank_code')?></th>
					<?php 
						$usertype = $this->session->userdata("usertype");
						if($usertype == "Super Admin") {
					?>
					<th class="col-lg-1"><?=$this->lang->line('account_status')?></th>
					<th class="col-lg-2"><?=$this->lang->line('action')?></th>
					<?php } ?>
					<?php 
						$usertype = $this->session->userdata("usertype");
						if($usertype == "Admin" || $usertype == "Super Admin") {
					?>
					<th class="col-lg-3"><?=$this->lang->line('action')?></th>
					<?php } ?>
				</tr>
			</thead>
			<tbody>
				<?php if(count($accounts)) {$i = 1; foreach($accounts as $account) { ?>
					<tr>
						<td data-title="<?=$this->lang->line('slno')?>">
							<?php echo $i; ?>
						</td>
						<?php 
							$usertype = $this->session->userdata("usertype");
							if($usertype == "Admin") {
						?>
						<td data-title="<?=$this->lang->line('bank_name')?>">
							<?php echo $account->bank_name; ?>
						</td>
						<?php } ?>
						<td data-title="<?=$this->lang->line('account_name')?>">
							<?php echo $account->account_name; ?>
						</td>
						<td data-title="<?=$this->lang->line('account_number')?>">
							<span class="label label-primary"><?=$account->account_number; ?></span>
						</td>
						<td data-title="<?=$this->lang->line('account_type')?>">
							<?=$account->account_type; ?>
						</td>
						<td data-title="<?=$this->lang->line('bank_code')?>">                         
							<span class="label label-danger"><?=$account->bank_code; ?></span>
						</td>
						<?php 
							$usertype = $this->session->userdata("usertype");
							if($usertype == "Super iAdmin") {
						?>
						<td data-title="<?=$this->lang->line('school_status')?>">
							<div class="onoffswitch-small" id="<?=$school->schoolID?>">
								<input type="checkbox" id="myonoffswitch<?=$school->schoolID?>" class="onoffswitch-small-checkbox" name="paypal_demo" <?php if($school->schoolactive === '1') echo "checked='checked'"; ?>>
								<label for="myonoffswitch<?=$school->schoolID?>" class="onoffswitch-small-label">
									<span class="onoffswitch-small-inner"></span>
									<span class="onoffswitch-small-switch"></span>
								</label>
							</div>
						</td>

						<td data-title="<?=$this->lang->line('action')?>">
							<?php echo btn_view('school/view/'.$school->schoolID, $this->lang->line('view')) ?>
							<?php echo btn_edit('school/edit/'.$school->schoolID, $this->lang->line('edit')) ?>
							<?php echo btn_delete('school/delete/'.$school->schoolID, $this->lang->line('delete')) ?>
						</td>
						<?php } ?>

						<?php 
							$usertype = $this->session->userdata("usertype");
							if($usertype == "Admin") {
						?>
						<td data-title="<?=$this->lang->line('action')?>">
							<?php echo btn_edit('bank_account/edit/'.$account->accountID, $this->lang->line('edit')) ?>
							<?php echo btn_delete('bank_account/delete/'.$account->accountID, $this->lang->line('delete')) ?>
						</td>
						<?php } ?>
						<?php 
							$usertype = $this->session->userdata("usertype");
							if($usertype == "Super Admin") {
						?>
						<td data-title="<?=$this->lang->line('action')?>">
							<?php //echo btn_view('bank_account/admin_view/'.$account->accountID, $this->lang->line('view')) ?>
							<?php echo btn_edit('bank_account/admin_edit/'.$account->accountID, $this->lang->line('edit')) ?>
						</td>
						<?php } ?>
					</tr>
				<?php $i++; }} ?>
			</tbody>
		</table>
		<?php } ?>
	</div>
</div>
<!-- end panel -->
		
<!-- begin scroll to top btn -->
<a href="javascript:;" class="btn btn-icon btn-circle btn-success btn-scroll-to-top fade" data-click="scroll-top"><i class="fa fa-angle-up"></i></a>
<!-- end scroll to top btn -->

</div>
<!-- end page container -->



<?php /*
<!-- <div class="box box-danger">
    <div class="box-header with-border">
        <h3 class="box-title"><i class="fa fa-pencil"></i> <?=$this->lang->line('panel_title')?></h3>

       
        <ol class="breadcrumb">
            <li><a href="<?=base_url("dashboard/index")?>"><i class="fa fa-laptop"></i> <?=$this->lang->line('menu_dashboard')?></a></li>
            <li class="active"><?=$this->lang->line('menu_bank_account')?></li>
        </ol>
    </div><!-- /.box-header --> 
    <!-- form start -->
    <!-- <div class="box-body">
        <!-- <div class="row">
            <div class="col-sm-12">
                <?php 
                    $usertype = $this->session->userdata("usertype");
                    if($usertype == "Super Admin" || $usertype == "Admin") {
                ?>
                    <h5 class="page-header">
					<a href="<?php echo base_url('bank_account/addaccount') ?>" class="btn btn-success btn-lg">
						<i class="fa fa-plus add_margin"></i> Add Bank Account
					</a>
				</h5>
                <?php } ?>

                <div id="hide-table">
                    <?php 
                        $usertype = $this->session->userdata("usertype");
                        if($usertype == "Super Admin") {
                    ?>
                    <table id="example" class="table table-striped table-bordered table-hover dataTable no-footer">
                    <?php } ?>
                    <?php 
                        $usertype = $this->session->userdata("usertype");
                        if($usertype == "Admin") {
                    ?>
                    <table id="exampl" class="table table-responsive table-hover dataTable no-footer">
                    <?php } ?>
                        <thead>
                            <tr>
                                <th class="col-lg-1"><?=$this->lang->line('slno')?></th> 
                                <?php 
                                    $usertype = $this->session->userdata("usertype");
                                    if($usertype == "Admin") {
                                ?>
                                <th class="col-lg-3"><?=$this->lang->line('bank_name')?></th>
                                <?php } ?>
                                <th class="col-lg-3"><?=$this->lang->line('account_name')?></th>
                                <th class="col-lg-2"><?=$this->lang->line('account_number')?></th>
                                <th class="col-lg-2"><?=$this->lang->line('account_type')?></th>
                                <th class="col-lg-1"><?=$this->lang->line('bank_code')?></th>
                                <?php 
                                    $usertype = $this->session->userdata("usertype");
                                    if($usertype == "Super Admin") {
                                ?>
                                <th class="col-lg-1"><?=$this->lang->line('account_status')?></th>
                                <th class="col-lg-2"><?=$this->lang->line('action')?></th>
                                <?php } ?>
                                <?php 
                                    $usertype = $this->session->userdata("usertype");
                                    if($usertype == "Admin" || $usertype == "Super Admin") {
                                ?>
                                <th class="col-lg-2"><?=$this->lang->line('action')?></th>
                                <?php } ?>
                            </tr>
                        </thead>
                        <tbody>
                            <?php if(count($accounts)) {$i = 1; foreach($accounts as $account) { ?>
                                <tr>
                                    <td data-title="<?=$this->lang->line('slno')?>">
                                        <?php echo $i; ?>
                                    </td>
                                    <?php 
                                        $usertype = $this->session->userdata("usertype");
                                        if($usertype == "Admin") {
                                    ?>
                                    <td data-title="<?=$this->lang->line('bank_name')?>">
                                        <?php echo $account->bank_name; ?>
                                    </td>
                                    <?php } ?>
                                    <td data-title="<?=$this->lang->line('account_name')?>">
                                        <?php echo $account->account_name; ?>
                                    </td>
                                    <td data-title="<?=$this->lang->line('account_number')?>">
                                        <span class="label label-primary"><?=$account->account_number; ?></span>
                                    </td>
                                    <td data-title="<?=$this->lang->line('account_type')?>">
                                        <?=$account->account_type; ?>
                                    </td>
                                    <td data-title="<?=$this->lang->line('bank_code')?>">                         
										<span class="label label-danger"><?=$account->bank_code; ?></span>
                                    </td>
                                    <?php 
                                        $usertype = $this->session->userdata("usertype");
                                        if($usertype == "Super iAdmin") {
                                    ?>
                                    <td data-title="<?=$this->lang->line('school_status')?>">
                                        <div class="onoffswitch-small" id="<?=$school->schoolID?>">
                                            <input type="checkbox" id="myonoffswitch<?=$school->schoolID?>" class="onoffswitch-small-checkbox" name="paypal_demo" <?php if($school->schoolactive === '1') echo "checked='checked'"; ?>>
                                            <label for="myonoffswitch<?=$school->schoolID?>" class="onoffswitch-small-label">
                                                <span class="onoffswitch-small-inner"></span>
                                                <span class="onoffswitch-small-switch"></span>
                                            </label>
                                        </div>
                                    </td>

                                    <td data-title="<?=$this->lang->line('action')?>">
                                        <?php echo btn_view('school/view/'.$school->schoolID, $this->lang->line('view')) ?>
                                        <?php echo btn_edit('school/edit/'.$school->schoolID, $this->lang->line('edit')) ?>
                                        <?php echo btn_delete('school/delete/'.$school->schoolID, $this->lang->line('delete')) ?>
                                    </td>
                                    <?php } ?>

                                    <?php 
                                        $usertype = $this->session->userdata("usertype");
                                        if($usertype == "Admin") {
                                    ?>
                                    <td data-title="<?=$this->lang->line('action')?>">
                                        <?php echo btn_edit('bank_account/edit/'.$account->accountID, $this->lang->line('edit')) ?>
                                        <?php echo btn_delete('bank_account/delete/'.$account->accountID, $this->lang->line('delete')) ?>
                                    </td>
                                    <?php } ?>
                                    <?php 
                                        $usertype = $this->session->userdata("usertype");
                                        if($usertype == "Super Admin") {
                                    ?>
                                    <td data-title="<?=$this->lang->line('action')?>">
                                        <?php //echo btn_view('bank_account/admin_view/'.$account->accountID, $this->lang->line('view')) ?>
                                        <?php echo btn_edit('bank_account/admin_edit/'.$account->accountID, $this->lang->line('edit')) ?>
                                    </td>
                                    <?php } ?>
                                </tr>
                            <?php $i++; }} ?>
                        </tbody>
                    </table>
                </div>


            </div> <!-- col-sm-12 -->
        </div><!-- row -->
    </div><!-- Body -->
    <?php 
        $usertype = $this->session->userdata("usertype");
        if($usertype == "Super Admin") {
    ?>
    <div class="box-body">
        <div class="row">
            <div class="col-sm-12">

                <?php 
                    $usertype = $this->session->userdata("usertype");
                    if($usertype == "Super iAdmin") {
                ?>
                    <h5 class="page-header">
                        <a href="<?php echo base_url('bank_account/add') ?>">
                            <i class="fa fa-plus"></i> 
                            <?=$this->lang->line('add_title')?>
                        </a>
                    </h5>
                <?php } ?>

                <div id="hide-table">
                    <?php 
                        $usertype = $this->session->userdata("usertype");
                        if($usertype == "Super Admin") {
                    ?>
                    <table id="example1" class="table table-responsive table-hover dataTable no-footer">
                    <?php } ?>
                        <thead>
                            <tr>
                                <th class="col-lg-1"><?=$this->lang->line('slno')?></th>
                                <th class="col-lg-1"><?=$this->lang->line('school_name')?></th>
                                <th class="col-lg-3"><?=$this->lang->line('account_name')?></th>
                                <th class="col-lg-2"><?=$this->lang->line('account_number')?></th>
                                <th class="col-lg-2"><?=$this->lang->line('account_type')?></th>
                                <th class="col-lg-1"><?=$this->lang->line('bank_code')?></th>
                                <?php 
                                    $usertype = $this->session->userdata("usertype");
                                    if($usertype == "Super iAdmin") {
                                ?>
                                <th class="col-lg-1"><?=$this->lang->line('account_status')?></th>
                                <th class="col-lg-2"><?=$this->lang->line('action')?></th>
                                <?php } ?>
                                <?php 
                                    $usertype = $this->session->userdata("usertype");
                                    if($usertype == "Admin" || $usertype == "Super Admin") {
                                ?>
                                <th class="col-lg-2"><?=$this->lang->line('action')?></th>
                                <?php } ?>
                            </tr>
                        </thead>
                        <tbody>
                            <?php if(count($school_accounts)) {$i = 1; foreach($school_accounts as $account) { ?>
                                <tr>
                                    <td data-title="<?=$this->lang->line('slno')?>">
                                        <?php echo $i; ?>
                                    </td>
                                    <td data-title="<?=$this->lang->line('school_name')?>">
                                        <?php echo $account->school; ?>
                                    </td>
                                    <td data-title="<?=$this->lang->line('account_name')?>">
                                        <?php echo $account->account_name; ?>
                                    </td>
                                    <td data-title="<?=$this->lang->line('account_number')?>">
										<span class="label label-primary"><?=$account->account_number; ?></span>
                                    </td>
                                    <td data-title="<?=$this->lang->line('account_type')?>">
                                        <?=$account->account_type; ?>
                                    </td>
                                    <td data-title="<?=$this->lang->line('bank_code')?>">
                                        <?=$account->bank_code; ?>
                                    </td>

                                    <?php 
                                        $usertype = $this->session->userdata("usertype");
                                        if($usertype == "Super Admin") {
                                    ?>
                                    <td data-title="<?=$this->lang->line('action')?>">
                                        <?php echo btn_edit('bank_account/edit/'.$account->accountID, $this->lang->line('edit')) ?>
                                        <?php echo btn_delete('bank_account/delete/'.$account->accountID, $this->lang->line('delete')) ?>
                                    </td>
                                    <?php } ?>
                                </tr>
                            <?php $i++; }} ?>
                        </tbody>
                    </table>
                </div>


            </div> <!-- col-sm-12 -->
        </div><!-- row -->
    </div><!-- Body -->
    <?php } ?>
</div><!-- /.box -->
!-->
<script>
  var status = '';
  var id = 0;
  $('.onoffswitch-small-checkbox').click(function() {
      if($(this).prop('checked')) {
          status = 'chacked';
          id = $(this).parent().attr("id");
      } else {
          status = 'unchacked';
          id = $(this).parent().attr("id");
      }

      if((status != '' || status != null) && (id !='')) {
          $.ajax({
              type: 'POST',
              url: "<?=base_url('school/active')?>",
              data: "id=" + id + "&status=" + status,
              dataType: "html",
              success: function(data) {
                  if(data == 'Success') {
                      toastr["success"]("Success")
                      toastr.options = {
                        "closeButton": true,
                        "debug": false,
                        "newestOnTop": false,
                        "progressBar": false,
                        "positionClass": "toast-top-right",
                        "preventDuplicates": false,
                        "onclick": null,
                        "showDuration": "500",
                        "hideDuration": "500",
                        "timeOut": "5000",
                        "extendedTimeOut": "1000",
                        "showEasing": "swing",
                        "hideEasing": "linear",
                        "showMethod": "fadeIn",
                        "hideMethod": "fadeOut"
                      }
                  } else {
                      toastr["error"]("Error")
                      toastr.options = {
                        "closeButton": true,
                        "debug": false,
                        "newestOnTop": false,
                        "progressBar": false,
                        "positionClass": "toast-top-right",
                        "preventDuplicates": false,
                        "onclick": null,
                        "showDuration": "500",
                        "hideDuration": "500",
                        "timeOut": "5000",
                        "extendedTimeOut": "1000",
                        "showEasing": "swing",
                        "hideEasing": "linear",
                        "showMethod": "fadeIn",
                        "hideMethod": "fadeOut"
                      }
                  }
              }
          });
      }
  }); 
</script>

 */ ?>

