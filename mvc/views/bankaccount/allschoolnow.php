
<div class="box">
    <div class="box-header">
        <h3 class="box-title"><i class="fa icon-school"></i> <?=$this->lang->line('panel_title')?></h3>

       
        <ol class="breadcrumb">
            <li><a href="<?=base_url("dashboard/index")?>"><i class="fa fa-laptop"></i> <?=$this->lang->line('menu_dashboard')?></a></li>
            <li class="active"><?=$this->lang->line('school_allschool')?></li>
        </ol>
    </div><!-- /.box-header -->
    <!-- form start -->
    <div class="box-body">
        <div class="row">
                <?php 
                    $sess_school = $this->session->userdata('schoolID');
                    if(count($allschools)) {
                        foreach ($allschools as $key => $allschool) {
                ?>
                    <a href="<?=base_url("school/activeschoolnow/$allschool->schoolID/$allschool->photo");?>" >
                        <div class="col-lg-3 col-xs-6">
                            <div class="small-box <?php if( $sess_school == $allschool->schoolID) echo " bg-red "; else { echo " abc "; } ?> allschool">
                                <center><img style="margin:5px;" class="img-circle" src="<?=base_url("uploads/images/$allschool->photo")?>"></center>
                                <div class="inner allschool-text">
                                    <p><?=$allschool->school?></p>
                                </div>
                            </div>
                        </div>
                    <a>
                <?php } } ?>
        </div>
    </div>
</div>
