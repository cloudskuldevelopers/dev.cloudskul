<div id="container">
<!-- TOP BAR -->
<div class="row">
	<div class="col-md-2 hidden-xs">
		<img src="<?php echo base_url('uploads/images/logo.png'); ?>" style="width: auto; height: 40px; margin-top: -8px;" class="img-thumbnail img-responsive" />
	</div>
	<div class="col-md-6 col-xs-9 text-center">
		<h1 class="page-header"><?php echo $this->session->userdata('school');  ?></h1>
	</div>
	<div class="col-md-4 col-xs-3">
		<!-- begin breadcrumb -->
		<ol class="breadcrumb pull-right">
			<li><a href="<?=base_url("dashboard/index")?>"><i class="fa fa-laptop"></i> <?=$this->lang->line('menu_dashboard')?></a></li>
            <li class="active">Employee Salary List</li>
		</ol>
		<!-- end breadcrumb -->
	</div>
</div>
<!-- END TOP BAR -->

<!-- begin panel -->
<div class="panel panel-inverse" data-sortable-id="ui-widget-3">
	<div class="panel-heading">
		<h4 class="panel-title"><i class="fa fa-users"></i> List of Employees and Their Salaries</h4>
	</div>
	<div class="panel-body">
		<div class="col-sm-12">
			<table id="data-table" class="table table-striped table-bordered nowrap" width="100%">
				<thead>
					<tr>
						<th class="col-sm-1">#</th>
						<th class="col-sm-2">Full Name</th>
						<th class="col-sm-2">Salary Type</th>
						<th class="col-sm-1">Action</th>
					</tr>
				</thead>
				<tbody>
					<?php if(count($salary_info)) {$i = 1; foreach($salary_info as $all_employee) { ?>
						<tr>
							<td class="col-md-1">
								<?php echo $i; ?>
							</td>

							<td class="col-md-1">
								<?php echo $all_employee->first_name; ?>&nbsp;<?php echo $all_employee->last_name; ?>
							</td>

							<td>
								<?php echo $all_employee->template_name; ?>
							</td>
							
						   <td class="col-md-2">
								<a href="<?php echo base_url() . 'employee/profile/' . $all_employee->employee_id; ?>" class="btn btn-primary btn-xs" data-toggle="tooltip" data-placement="top" title="" data-original-title="View"><span class="fa fa-list-alt"></span></a>
								<a href="#" class="btn btn-warning btn-xs mrg" data-placement="top" data-toggle="tooltip" data-original-title="Edit"><i class="fa fa-edit"></i></a>
							</td>
						</tr>
					<?php $i++; }} ?>
				</tbody>
			</table>
		</div>
	</div>
</div>
<!-- end panel -->

<!-- begin scroll to top btn -->
<a href="javascript:;" class="btn btn-icon btn-circle btn-success btn-scroll-to-top fade" data-click="scroll-top"><i class="fa fa-angle-up"></i></a>
<!-- end scroll to top btn -->

</div>
<!-- end page container -->


<?php /*
<div class="box box-danger">
    <div class="box-header with-border">
        <h3 class="box-title"><i class="fa fa-users"></i> List of Employees and Their Salaries</h3>

       
        <ol class="breadcrumb">
            <li><a href="<?=base_url("dashboard/index")?>"><i class="fa fa-laptop"></i> <?=$this->lang->line('menu_dashboard')?></a></li>
            <li class="active">Employee Salary List</li>
        </ol>
    </div><!-- /.box-header -->
    <!-- form start -->
    
   <div class="box-body">
   		<div class="row">
			<div class="col-md-12">
				<div id="hide-table">
                    <table id="example1" class="table table-striped table-bordered table-hover dataTable no-footer">
                        <thead>
                            <tr>
                                <th class="col-sm-1">#</th>
                                <th class="col-sm-2">Full Name</th>
                                <th class="col-sm-2">Salary Type</th>
								<th class="col-sm-1">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php if(count($salary_info)) {$i = 1; foreach($salary_info as $all_employee) { ?>
                                <tr>
                                    <td class="col-md-1">
                                        <?php echo $i; ?>
                                    </td>

                                    <td class="col-md-1">
                                        <?php echo $all_employee->first_name; ?>&nbsp;<?php echo $all_employee->last_name; ?>
                                    </td>

                                    <td>
                                        <?php echo $all_employee->template_name; ?>
                                    </td>
									
                                   <td class="col-md-1">
								    <a href="<?php echo base_url() . 'employee/profile/' . $all_employee->employee_id; ?>" class="btn btn-primary btn-xs" data-toggle="tooltip" data-placement="top" title="" data-original-title="View"><span class="fa fa-list-alt"></span></a> &nbsp;
										<a href="#" class="btn btn-warning btn-xs mrg" data-placement="top" data-toggle="tooltip" data-original-title="Edit"><i class="fa fa-edit"></i></a>
									</td>
                                </tr>
                            <?php $i++; }} ?>
                        </tbody>
                    </table>
                </div>
			</div>
		</div>
	</div>
</div>
*/?>