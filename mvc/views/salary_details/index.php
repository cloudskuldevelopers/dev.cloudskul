<div id="container">
<div class="row">
	<div class="col-md-2 hidden-xs">
		<img src="<?php echo base_url('uploads/images/logo.png'); ?>" style="width: auto; height: 40px; margin-top: -8px;" class="img-thumbnail img-responsive" />
	</div>
	<div class="col-md-7 col-xs-9 text-center">
		<h1 class="page-header"><?php echo $this->session->userdata('school');  ?></h1>
	</div>
	<div class="col-md-3 col-xs-3">
		<!-- begin breadcrumb -->
		<ol class="breadcrumb pull-right">
			<li><a href="<?=base_url("dashboard/index")?>"><i class="fa fa-laptop"></i> <?=$this->lang->line('menu_dashboard')?></a></li>
            <li class="active">Payroll Management</li>
		</ol>
		<!-- end breadcrumb -->
	</div>
</div>

<!-- begin panel -->
<div class="panel panel-inverse" data-sortable-id="ui-widget-3">
	<div class="panel-heading">
		<h3 class="panel-title"><i class="fa fa-list-ul"></i> Payroll Management</h3>
	</div>
	<div class="panel-body">
		<div class="col-md-8 col-md-offset-2 col-sm-12" style="padding-top: 30px; padding-bottom: 30px;">
			<div class='form-group' >
				<label for="gender" class="col-sm-3 control-label">Select Department <span class="required"> *</span></label>
				<div class="col-sm-6">
					<select name="department_id" id="department_id" class="form-control" > 
						<option value="">Select department.....</option>
						<?php if (!empty($all_department_info)): foreach ($all_department_info as $v_department_info) : ?>                                                    
							<option value="<?php echo $v_department_info->department_id; ?>" 
							<?php
							if (!empty($department_id)) {
								echo $v_department_info->department_id == $department_id ? 'selected' : '';
							}
							?>><?php echo $v_department_info->department_name ?></option>                                                                                                                                                    
								<?php endforeach; ?>
							<?php endif; ?>
					</select>
					<p class="control-label col-md-12 alert-danger" id="myErrorMessage" style="text-align: left !important; padding-left: 0; margin-top: 5px; padding: 5px 10px !important;">Please select a department.</p>
				</div>
				
				<div class="col-md-3">
					<button type="submit" id="sbtn" class="btn btn-block btn-success"><i class="fa fa-check"></i>&nbsp; GO</button>                            
				</div>
			</div>
		</div>
		<div style="border:1px solid #C1CCD1; border-radius:6px; width:100%; float:left;">
			<div id="showAsignTemplate">
			</div>
		</div>
	</div>
</div>
<!-- end panel -->

<!-- begin scroll to top btn -->
<a href="javascript:;" class="btn btn-icon btn-circle btn-success btn-scroll-to-top fade" data-click="scroll-top"><i class="fa fa-angle-up"></i></a>
<!-- end scroll to top btn -->

</div>
<!-- end page container -->



<?php /*
<div class="box box-danger">
    <div class="box-header with-border">
        <h3 class="box-title"><i class="fa fa-list-ul"></i> Payroll Management</h3>

       
        <ol class="breadcrumb">
            <li><a href="<?=base_url("dashboard/index")?>"><i class="fa fa-laptop"></i> <?=$this->lang->line('menu_dashboard')?></a></li>
            <li class="active">Payroll Management</li>
        </ol>
    </div><!-- /.box-header -->
    <!-- form start -->
    
   <div class="box-body">
   		<div class="row">
			<div class="col-md-8 col-md-offset-2 col-sm-12" style="padding-top: 30px; padding-bottom: 30px;">
			
					<div class='form-group' >
						<label for="gender" class="col-sm-3 control-label">Select Department <span class="required"> *</span></label>
						<div class="col-sm-6">
							<select name="department_id" id="department_id" class="form-control" > 
                                <option value="">Select department.....</option>
                                <?php if (!empty($all_department_info)): foreach ($all_department_info as $v_department_info) : ?>                                                    
									<option value="<?php echo $v_department_info->department_id; ?>" 
									<?php
									if (!empty($department_id)) {
										echo $v_department_info->department_id == $department_id ? 'selected' : '';
									}
									?>><?php echo $v_department_info->department_name ?></option>                                                                                                                                                    
										<?php endforeach; ?>
									<?php endif; ?>
                            </select>
							<p class="control-label col-md-12 alert-danger" id="myErrorMessage" style="text-align: left !important; padding-left: 0; margin-top: 5px; padding: 5px 10px !important;">Please select a department.</p>
						</div>
						
						<div class="col-md-3">
							<button type="submit" id="sbtn" class="btn btn-block btn-success"><i class="fa fa-check"></i>&nbsp; GO</button>                            
						</div>
					</div>
				
			</div>
			
			<div class="col-md-12">
				<div id="showAsignTemplate">
				</div>
			</div>
		
		
		</div>
	</div>
</div>
*/ ?>

<script type="text/javascript">
	$('#myErrorMessage').hide();
	
	
	$("#sbtn").click(function(e){
		if($('#department_id').val() == ""){
			$('#myErrorMessage').show(300);
		}
		else{
			e.preventDefault();
			$('#myErrorMessage').hide();
			getSalaryDetails();
		}
	});
		
	function getSalaryDetails(){
		var departmentID = $("#department_id").val();

		$.ajax({
			type: 'POST',
			url: "<?php echo base_url() . 'salary_details/employee_salary_details/'; ?>" + departmentID,
			data: "department_id=" + departmentID,
			dataType: "html",
			success: function(data) {
				$('#showAsignTemplate').html(data);
			}
		});
	}
</script>