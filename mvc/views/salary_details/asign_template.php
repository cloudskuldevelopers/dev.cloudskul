<div class="nav-tabs-custom">
		<ul class="nav nav-tabs">
			<li class="active"><a data-toggle="tab" href="#all" aria-expanded="true">All Employees</a></li>
		</ul>


		<div class="tab-content">
			<div id="all" class="tab-pane active">
				<div id="hide-table">
					<form action="<?php echo base_url() ?>salary_details/save_salary_details" method="post">
						<table id="example1" class="table table-striped table-bordered table-hover dataTable no-footer">
							<thead>
								<tr>
									<th class="col-sm-2">#</th>
									<th class="col-sm-2">Employee Name</th>
									<th class="col-sm-2">Designation</th>
									<th class="col-sm-2">Payroll Template</th>
								</tr>
							</thead>
							<tbody>
							<?php if(count($employee_info)) {$i = 1; foreach($employee_info as $all_employee) { ?>
									<tr>
										<td class="col-md-1">
											<input type="hidden" name="employee_id[]"  value="<?php echo $all_employee->employee_id ?>"> 
											<?php echo $i; ?>
										</td>

										<td class="col-md-1"><?php echo $all_employee->first_name; ?>&nbsp;<?php echo $all_employee->last_name; ?> </td>
										
										<td><?php echo $all_employee->designation_name ?></td>    
										
										<td>
											<div id="l_category" class="text-center">
												<select name="payroll_id" id="payroll_id" class="select2 form-control" > 
													<option value="">Select salary template.....</option>
													<?php if (!empty($salary_template)): foreach ($salary_template as $template) : ?>                                                    
														<option value="<?php echo $template->payroll_id; ?>" 
														<?php
														if (!empty($payroll_id)) {
															echo $template->payroll_id == $payroll_id ? 'selected' : '';
														}
														?>><?php echo $template->template_name ?></option>                                                                                                                                                    
															<?php endforeach; ?>
														<?php endif; ?>
												</select>
											</div> 
										</td>
									</tr>
								<?php $i++; }} ?>
							</tbody>
						</table>
						
						<div class="pull-right col-md-3" style="margin: 50px 0 30px;">
								<button type="submit" id="sbtn" class="btn btn-block btn-success"><i class="fa fa-check"></i>&nbsp; UPDATE</button>                            
						</div>
					</form>
				</div>

			</div>
		</div>
	</div> <!-- nav-tabs-custom -->