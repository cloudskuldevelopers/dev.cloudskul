<div class="row">
	<div class="col-md-2 hidden-xs">
		<img src="<?php echo base_url('uploads/images/logo.png'); ?>" style="width: auto; height: 40px; margin-top: -8px;" class="img-thumbnail img-responsive" />
	</div>
	<div class="col-md-7 col-xs-9 text-center">
		<h1 class="page-header"><?php echo $this->session->userdata('school');  ?></h1>
	</div>
	<div class="col-md-3 col-xs-3">
		<!-- begin breadcrumb -->
		<ol class="breadcrumb pull-right">
			<li ><a href="<?php echo base_url('dashboard/index');?>"><i class="fa fa-laptop"></i> Dashboard</a></li>
			<li class="active"><?=$this->lang->line('menu_fine')?></li>
		</ol>
		<!-- end breadcrumb -->
		
	</div>
</div>
<div class="panel panel-inverse" data-sortable-id="ui-widget-3">
	<div class="panel-heading">
		<h4 class="panel-title"><i class="fa fa-pencil"></i> <?=$this->lang->line('menu_fine')?></h4>
	</div>
	
	<div class="panel-body">
	
<div class="box box-danger">
       <!-- form start -->
    <div class="box-body">
        <div class="row">
            <div class="col-sm-12">

                <div class="col-sm-6  list-group">
                    
                        <form class="form-horizontal" role="form" method="post">

                            <div class="form-group">
                                <label fore="day" class="col-sm-2  control-label">
                                    <?=$this->lang->line('issue_day')?>
                                </label>
                                <div class="col-sm-6">
                                    <select name="day" class="form-control" id="day">
                                        <option value="0"><?=$this->lang->line('issue_select_day')?></option>
                                        <?php
                                            for ($i=1; $i <=31 ; $i++) { 
                                                echo "<option value='$i'>". $i ."</option>";
                                            }
                                        ?>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="month" class="col-sm-2  control-label">
                                    <?=$this->lang->line('issue_month')?>
                                </label>
                                <div class="col-sm-6">
                                    <select name="month" class="form-control" id="month">
                                        <option value="0"><?=$this->lang->line('issue_select_month')?></option>
                                        <?php
                                            $array = array($this->lang->line('slno'), "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec");
                                            for ($i=1; $i <=12 ; $i++) { 
                                                echo "<option value='$i'>". $array[$i] ."</option>";
                                            }
                                        ?>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="year" class="col-sm-2 control-label">
                                    <?=$this->lang->line('issue_year')?>
                                </label>
                                <div class="col-sm-6">
                                    <select name="year" class="form-control" id="year">
                                        <option value="0"><?=$this->lang->line('issue_select_year')?></option>
                                        <?php
                                            $pyear = date("Y");
                                            for ($i=$pyear; $i >=1990 ; $i--) { 
                                                echo "<option value='$i'>". $i ."</option>";
                                            }
                                        ?>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-sm-offset-2 col-sm-6">
                                    <input type="submit" class="btn btn-success btn-block" style="width: 45% !important;" value="<?=$this->lang->line('add_fine')?>" >
                                </div>
                            </div>

                        </form>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>


