<div class="row">
	<div class="col-md-2 hidden-xs">
		<img src="<?php echo base_url('uploads/images/logo.png'); ?>" style="width: auto; height: 40px; margin-top: -8px;" class="img-thumbnail img-responsive" />
	</div>
	<div class="col-md-7 col-xs-9 text-center">
		<h1 class="page-header"><?php echo $this->session->userdata('school');  ?></h1>
	</div>
	<div class="col-md-3 col-xs-3">
		<!-- begin breadcrumb -->
		<ol class="breadcrumb pull-right">
			<li ><a href="<?php echo base_url('dashboard/index');?>"><i class="fa fa-laptop"></i> Dashboard</a></li>
			<li class="active"><?=$this->lang->line('menu_issue')?></li>
		</ol>
		<!-- end breadcrumb -->
		
	</div>
</div>

<div class="panel panel-inverse" data-sortable-id="ui-widget-3">
	<div class="panel-heading">
	<div class="btn-group-vertical pull-right">
			<a href="<?php echo base_url('issue/add') ?>" type="button" class="btn btn-success btn-xs"><i class="fa fa-plus"></i> <?=$this->lang->line('add_title')?></a>
		</div>
		<h4 class="panel-title"><i class="fa fa-pencil"></i> <?=$this->lang->line('panel_title')?></h4>
		
	</div>
	<div class="panel-body">
	

<div class="box box-danger">
       <!-- form start -->
    <div class="box-body">
        <div class="row">
            <div class="col-sm-12">

                <?php
                    $usertype = $this->session->userdata("usertype");
                    if($usertype == "Admin" || $usertype == "Super Admin" || $usertype == "Librarian") {
                ?>               
                <div class="col-lg-6  list-group">
                    <!--<div class="form-box list-group-item-warning offset_library">-->

                        <form style="" class="form-horizontal" role="form" method="post" enctype="multipart/form-data">
                      		<div class="input-group m-b-10">
								<span class="input-group-addon offset_addon"><?=$this->lang->line("issue_lid")?></span>
								 <input type="text" class="form-control" id="lid" name="lid" value="<?=set_value('lid')?>" >
							</div>	
							<input type="submit" class="btn btn-success iss-mar" value="<?=$this->lang->line('issue_search')?>" >
                        </form>



                    </div>
                </div>
                <?php } ?>
                
            </div>
        </div>
    </div>
	</div>
</div>