<div class="row">
	<div class="col-md-2 hidden-xs">
		<img src="<?php echo base_url('uploads/images/logo.png'); ?>" style="width: auto; height: 40px; margin-top: -8px;" class="img-thumbnail img-responsive" />
	</div>
	<div class="col-md-7 col-xs-9 text-center">
		<h1 class="page-header"><?php echo $this->session->userdata('school');  ?></h1>
	</div>
	<div class="col-md-3 col-xs-3">
		<!-- begin breadcrumb -->
		<ol class="breadcrumb pull-right">
			<li><a href="<?=base_url("dashboard/index")?>"><i class="fa fa-laptop"></i> <?=$this->lang->line('menu_dashboard')?></a></li>
            <li><a href="<?=base_url("lmember/index/$set")?>"><?=$this->lang->line('menu_member')?></a></li>
            <li class="active"><?=$this->lang->line('menu_add')?> <?=$this->lang->line('menu_member')?></li>
		</ol>
		<!-- end breadcrumb -->
		
	</div>
	</div>
	<div class="panel panel-inverse" data-sortable-id="ui-widget-3">
	<div class="panel-heading">
		<h4 class="panel-title"><i class="fa fa-pencil"></i> <?=$this->lang->line('panel_title')?></h4>
	</div>
	<div class="panel-body">
	<div class="col-sm-12" style="border-bottom:1px solid #F0F3F5; margin-bottom:10px;"></div>
<form class="form-horizontal" role="form" method="post">
                    <?php 
                        if(form_error('lID')) 
                            echo "<div class='form-group has-error' >";
                        else     
                            echo "<div class='form-group' >";
                    ?>
                        <label for="lID" class="col-sm-2 control-label">
                            <?=$this->lang->line("lmember_lID")?>
                        </label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" id="lID" name="lID" value="<?=set_value('lID', $libraryID)?>" >
                        </div>
                        <span class="col-sm-4 control-label">
                            <?php echo form_error('lID'); ?>
                        </span>
                    </div>

                    <?php 
                        if(form_error('lbalance')) 
                            echo "<div class='form-group has-error' >";
                        else     
                            echo "<div class='form-group' >";
                    ?>
                        <label for="lbalance" class="col-sm-2 control-label">
                            <?=$this->lang->line("lmember_lfee")?>
                        </label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" id="lbalance" name="lbalance" value="<?=set_value('lbalance', "0.00")?>" >
                        </div>
                        <span class="col-sm-4 control-label">
                            <?php echo form_error('lbalance'); ?>
                        </span>
                    </div>

                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-8">
                            <input type="submit" class="btn btn-success" value="<?=$this->lang->line("add_lmember")?>" >
                        </div>
                    </div>

                </form>

</div>
</div>