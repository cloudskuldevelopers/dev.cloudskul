<div class="row">
	<div class="col-md-2 hidden-xs">
		<img src="<?php echo base_url('uploads/images/logo.png'); ?>" style="width: auto; height: 40px; margin-top: -8px;" class="img-thumbnail img-responsive" />
	</div>
	<div class="col-md-7 col-xs-9 text-center">
		<h1 class="page-header"><?php echo $this->session->userdata('school');  ?></h1>
	</div>
	<div class="col-md-3 col-xs-3">
		<!-- begin breadcrumb -->
		<ol class="breadcrumb pull-right">
			<li><a href="<?=base_url("dashboard/index")?>"><i class="fa fa-laptop"></i> <?=$this->lang->line('menu_dashboard')?></a></li>
            <li class="active"><?=$this->lang->line('menu_academic_year')?></li>
		</ol>
		<!-- end breadcrumb -->
	</div>
</div>

<div id="page-container">
<!-- begin panel -->
<div class="panel panel-inverse" data-sortable-id="ui-widget-3">
	<div class="panel-heading">
		<?php
			$usertype = $this->session->userdata("usertype");
			if($usertype == "Admin" || $usertype == "Super Admin") {
		?>
			<div class="btn-group-vertical pull-right">
				<a href="<?php echo base_url('academic_year/add') ?>" type="button" class="btn btn-success btn-xs"><i class="fa fa-plus"></i> <?=$this->lang->line('add_title')?></a>
			</div>
		<?php } ?>
		<h4 class="panel-title"><i class="fa fa-desktop"></i> <?=$this->lang->line('panel_title')?></h4>
	</div>
	<div class="panel-body">
		<table id="example1" class="table table-striped table-bordered table-hover dataTable no-footer">
			<thead>
				<tr>
					<th class="col-sm-1"><?=$this->lang->line('slno')?></th>
					<th class="col-sm-2"><?=$this->lang->line('year_title')?></th>
					<th class="col-sm-2"><?=$this->lang->line('year_status')?></th>
					<?php if($usertype == "Admin" || $usertype == "Super Admin") { ?>
					<th class="col-sm-1"><?=$this->lang->line('action')?></th>
					<?php } ?>
				</tr>
			</thead>
			<tbody>
				<?php if(count($years)) {$i = 1; foreach($years as $year) { ?>
					<tr>
						<td data-title="<?=$this->lang->line('slno')?>">
							<?php echo $i; ?>
						</td>

						<td data-title="<?=$this->lang->line('year_title')?>">
							<?php echo $year->year_title; ?>
						</td>

						<td data-title="<?=$this->lang->line('year_status')?>">
							<?php echo $year->year_status; ?>
						</td>
						<?php if($usertype == "Admin" || $usertype == "Super Admin") { ?>
						<td data-title="<?=$this->lang->line('action')?>">
							<?php echo btn_edit('academic_year/edit/'.$year->yearID, $this->lang->line('edit')) ?>
							<?php echo btn_delete('academic_year/delete/'.$year->yearID, $this->lang->line('delete')) ?>
						</td>
						<?php } ?>
					</tr>
				<?php $i++; }} ?>
			</tbody>
		</table>
	</div>
</div>
</div>