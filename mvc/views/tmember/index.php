<div class="row">
	<div class="col-md-2 hidden-xs">
		<img src="<?php echo base_url('uploads/images/logo.png'); ?>" style="width: auto; height: 40px; margin-top: -8px;" class="img-thumbnail img-responsive" />
	</div>
	<div class="col-md-7 col-xs-9 text-center">
		<h1 class="page-header"><?php echo $this->session->userdata('school');  ?></h1>
	</div>
	<div class="col-md-3 col-xs-3">
		<!-- begin breadcrumb -->
		<ol class="breadcrumb pull-right">
			<li><a href="<?=base_url("dashboard/index")?>"><i class="fa fa-laptop"></i> <?=$this->lang->line('menu_dashboard')?></a></li>
            <li class="active"><?=$this->lang->line('menu_member')?></li>
		</ol>
		<!-- end breadcrumb -->
		
	</div>
</div>
<div id="page-container">
<!-- begin panel -->
<div class="panel panel-inverse" data-sortable-id="ui-widget-3">
	<div class="panel-heading">
		<h4 class="panel-title"><i class="fa fa-desktop"></i> <?=$this->lang->line('panel_title')?></h4>
	</div>
	<div class="panel-body">
		<div class="col-md-12 m-b-20" style="border-bottom:1px solid #F0F3F5;">
			<div class="col-md-6 add_padding m-b-20">
				<form style="" class="form-horizontal" role="form" method="post">
					<div class="input-group">
						<span class="input-group-addon offset_addon"><i class="fa fa-sitemap"></i></span>
						<?php
							$array = array("0" => $this->lang->line("tmember_select_class"));
							foreach ($classes as $classa) {
								$array[$classa->classesID] = $classa->classes;
							}
							echo form_dropdown("classesID", $array, set_value("classesID"), "id='classesID' class='form-control offset_height'");
						?>
					</div>
				</form>
			</div>
		</div>
		<table id="example1" class="table table-striped table-bordered table-hover dataTable no-footer">
			<thead>
				<tr>
					<th class="col-sm-1"><?=$this->lang->line('slno')?></th>
					<th class="col-sm-1"><?=$this->lang->line('tmember_photo')?></th>
					<th class="col-sm-2"><?=$this->lang->line('tmember_name')?></th>
					<th class="col-sm-2"><?=$this->lang->line('tmember_section')?></th>
					<th class="col-sm-2"><?=$this->lang->line('tmember_roll')?></th>
					<th class="col-sm-2"><?=$this->lang->line('tmember_phone')?></th>
					<th class="col-sm-2"><?=$this->lang->line('action')?></th>
				</tr>
			</thead>
			<tbody id="list">
				<?php if(count($students)) {$i = 1; foreach($students as $student) { ?>
					<tr>
						<td data-title="<?=$this->lang->line('slno')?>">
							<?php echo $i; ?>
						</td>
						<td data-title="<?=$this->lang->line('tmember_photo')?>">
							<?php $array = array(
									"src" => base_url('uploads/images/'.$student->photo),
									'width' => '35px',
									'height' => '35px',
									'class' => 'img-rounded'

								);
								echo img($array); 
							?>
						</td>
						<td data-title="<?=$this->lang->line('tmember_name')?>">
							<?php echo $student->name; ?>
						</td>
						<td data-title="<?=$this->lang->line('tmember_section')?>">
							<?php echo $student->ssection; ?>
						</td>
						<td data-title="<?=$this->lang->line('tmember_roll')?>">
							<?php echo $student->roll; ?>
						</td>
						<td data-title="<?=$this->lang->line('tmember_phone')?>">
							<?php echo $student->phone; ?>
						</td>
						<td data-title="<?=$this->lang->line('action')?>">

							<?php
								if($student->transport == 0) {
									echo btn_add('tmember/add/'.$student->studentID."/".$set, $this->lang->line('tmember'));
								} else {
									echo btn_view('tmember/view/'.$student->studentID."/".$set, $this->lang->line('view')). " ";
									echo btn_edit('tmember/edit/'.$student->studentID."/".$set, $this->lang->line('edit')). " ";
									echo btn_delete('tmember/delete/'.$student->studentID."/".$set, $this->lang->line('delete'));
								}
							?>
						</td>
					</tr>
				<?php $i++; }} ?>
				
			</tbody>
		</table>
	</div>
</div>
</div>

<script type="text/javascript">
    $('#classesID').change(function() {
        var classesID = $(this).val();
        if(classesID == 0) {
            $('#hide-table').hide();
        } else {
            $.ajax({
                type: 'POST',
                url: "<?=base_url('tmember/student_list')?>",
                data: "id=" + classesID,
                dataType: "html",
                success: function(data) {
                    window.location.href = data;
                }
            });
        }
    });
</script>