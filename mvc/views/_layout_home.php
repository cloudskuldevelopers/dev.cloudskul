<?php echo doctype("html5"); ?>
<html class="no-js" lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <title>Developers ::: CloudSkul</title>
	
	<!-- Essential CSS Files -->
	<link rel="stylesheet" href="<?php echo base_url('assets/home/css/normalize.css'); ?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/home/css/font-awesome.min.css'); ?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/home/css/animate.css'); ?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/home/css/simplelightbox.min.css'); ?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/home/css/bootstrap.min.css'); ?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/home/css/owl.carousel.css'); ?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/home/css/owl.theme.css'); ?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/home/css/style.css'); ?>">
	<!--link rel="stylesheet" href="<?php echo base_url('assets/home/css/colors/color-green.css'); ?>"-->
	<link rel="stylesheet" href="<?php echo base_url('assets/home/css/colors/color-blue.css'); ?>">
	<!--link rel="stylesheet" href="<?php echo base_url('assets/home/css/colors/color-aqua.css'); ?>"-->
	<!--link rel="stylesheet" href="<?php echo base_url('assets/home/css/colors/color-purple.css'); ?>"-->

	<!-- Responsive CSS -->
	<link rel="stylesheet" href="<?php echo base_url('assets/home/css/responsive.css'); ?>">

	<!-- Google Web Fonts =:= Raleway , Montserrat and Roboto -->
	<link href='https://fonts.googleapis.com/css?family=Raleway:400,500,600,700' rel='stylesheet' type='text/css'>
	<link href='https://fonts.googleapis.com/css?family=Montserrat:700,400' rel='stylesheet' type='text/css'>
	<link href='https://fonts.googleapis.com/css?family=Roboto:700,500,400' rel='stylesheet' type='text/css'>

	<!-- Essential JS Files -->
	<script src="<?php echo base_url('assets/home/js/vendor/jquery-1.11.3.min.js'); ?>"></script>
	<script src="<?php echo base_url('assets/home/js/vendor/modernizr-2.8.3.min.js'); ?>"></script>
    
</head>

<body>

    <?php $this->load->view($subview); ?>
	
	<script src="<?php echo base_url('assets/home/js/bootstrap.min.js'); ?>"></script>
	<!-- jQuery Easing -->
	<script src="<?php echo base_url('assets/home/js/jquery.easing.min.js'); ?>"></script>
	<!-- PreLoader -->
	<script src="<?php echo base_url('assets/home/js/queryloader2.min.js'); ?>"></script>
	<!-- WOW JS Animation -->
	<script src="<?php echo base_url('assets/home/js/wow.min.js'); ?>"></script>
	<!-- Simple Lightbox -->
	<script src="<?php echo base_url('assets/home/js/simple-lightbox.min.js'); ?>"></script>
	<!-- Sticky -->
	<script src="<?php echo base_url('assets/home/js/jquery.sticky.js'); ?>"></script>
	<!-- OWL-Carousel -->
	<script src="<?php echo base_url('assets/home/js/owl.carousel.min.js'); ?>"></script>
	<!-- jQuery inview -->
	<script src="<?php echo base_url('assets/home/js/jquery.inview.js'); ?>"></script>
	<!-- Shuffle jQuery -->
	<script src="<?php echo base_url('assets/home/js/jquery.shuffle.min.js'); ?>"></script>
	<!-- jQuery CountTo -->
	<script src="<?php echo base_url('assets/home/js/jquery.counTo.js'); ?>"></script>
	<!-- Goole map API -->
	<script src="http://maps.google.com/maps/api/js?sensor=true"></script>
	<!-- gmap.js plugin -->
	<script src="<?php echo base_url('assets/home/js/gmap.js'); ?>"></script>
	<!-- Main JS -->
	<script src="<?php echo base_url('assets/home/js/main.js'); ?>"></script>
</body>
</html>