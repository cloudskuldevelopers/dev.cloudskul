<!-- TOP BAR -->
<div class="row">
	<div class="col-md-1 hidden-xs">
		<img src="<?php echo base_url('uploads/images/logo.png'); ?>" style="width: auto; height: 40px; margin-top: -8px;" class="img-thumbnail img-responsive" />
	</div>
	<div class="col-md-6 col-xs-9 text-center">
		<h1 class="page-header"><?php echo $this->session->userdata('school');  ?></h1>
	</div>
	<div class="col-md-5 col-xs-3">
		<!-- begin breadcrumb -->
		<ol class="breadcrumb pull-right">
			<li><a href="<?=base_url("dashboard/index")?>"><i class="fa fa-laptop"></i> <?=$this->lang->line('menu_dashboard')?></a></li>
            <li><a href="<?php echo base_url().'department/index'; ?>">Department</a></li>
            <li class="active">Add Department</li>
		</ol>
		<!-- end breadcrumb -->
	</div>
</div>
<!-- END TOP BAR -->
	
<!-- begin panel -->
<div class="panel panel-inverse" data-sortable-id="ui-widget-3">
	<div class="panel-heading">
		<h4 class="panel-title"><i class="fa fa-list-ol"></i> Department</h4>
	</div>
	<div class="panel-body">
		<div class="col-sm-12">
			 <form class="form-horizontal" action="" role="form" method="post">
				<?php 
					if(form_error('department_name')) 
						echo "<div class='form-group has-error' >";
					else     
						echo "<div class='form-group' >";
				?>
					<label for="department_name" class="col-sm-3 control-label">Department <span class="required"> *</span></label>
					<div class="col-sm-7">
						<input type="text" name="department_name" class="form-control" placeholder="Enter your department name" />
						<p class="control-label col-md-12" style="text-align: left !important; padding-left: 0;"><?php echo form_error('department_name'); ?></p>
					</div>
					<div class="col-sm-2"></div>
				</div>
				
				<div id="add_new" class="margin">
					<?php 
						if(form_error('designation_name[]')) 
							echo "<div class='form-group has-error' >";
						else     
							echo "<div class='form-group' >";
					?>
						<label for="designations[]" class="col-sm-3 control-label">Add Designations <span class="required"> *</span></label>
						<div class="col-sm-7">
							<input type="text" name="designation_name[]" class="form-control" placeholder="Enter your department name" />
							<p class="control-label col-md-12" style="text-align: left !important; padding-left: 0;"><?php echo form_error('designation_name[]'); ?></p>
						</div>
						<div class="col-sm-2">                            
							<strong><a href="javascript:void(0);" id="add_more" class="btn btn-sm btn-success"><i class="fa fa-plus"></i>&nbsp;Add more</a></strong>
						</div>
					</div>
				</div>
				<div class="form-group margin">
					<div class="col-sm-offset-3 col-sm-5">
						<button type="submit" id="sbtn" class="btn btn-block btn-success"><i class="fa fa-save"></i>&nbsp; Save</button>                            
					</div>
				</div>
			</form>
			
			<script type="text/javascript">
				$(document).ready(function () {
					var maxAppend = 0;
					$("#add_more").click(function () {
						if (maxAppend >= 4)
						{
							alert("Maximum 5 designation is allowed");
						} else {
							var add_new = $('<div class="form-group">\n\
							<label for="field-1" class="col-sm-3 control-label">Add Designations <span class="required"> *</span></label>\n\
							<div class="col-sm-7">\n\<input type="text" name="designation_name[]" class="form-control" placeholder="Enter department designation"/>\n\
							</div>\n\
							<div class="col-sm-2">\n\
							<strong><a href="javascript:void(0);" class="remCF btn btn-sm btn-danger"><i class="fa fa-times"></i>&nbsp; Remove</a></strong>\n\
							</div>');
							maxAppend++;
							$("#add_new").append(add_new);
						}
					});

					$("#add_new").on('click', '.remCF', function () {
						$(this).parent().parent().parent().remove();
						maxAppend--;
					});

					// $(".")
				});
			</script>
		</div>
	</div>
</div>
<!-- end panel -->

<!-- begin scroll to top btn -->
<a href="javascript:;" class="btn btn-icon btn-circle btn-success btn-scroll-to-top fade" data-click="scroll-top"><i class="fa fa-angle-up"></i></a>
<!-- end scroll to top btn -->



<?php /*
<div class="box box-danger">
    <div class="box-header with-border">
        <h3 class="box-title"><i class="fa fa-list-ol"></i> Department</h3>

       
        <ol class="breadcrumb">
            <li><a href="<?=base_url("dashboard/index")?>"><i class="fa fa-laptop"></i> <?=$this->lang->line('menu_dashboard')?></a></li>
            <li><a href="<?php echo base_url().'department/index'; ?>">Department</a></li>
            <li class="active">Add Department</li>
        </ol>
    </div><!-- /.box-header -->
    <!-- form start -->
    <div class="box-body">
        <div class="row">
            <div class="col-sm-8">
                <form class="form-horizontal" action="" role="form" method="post">
					<?php 
						if(form_error('department_name')) 
							echo "<div class='form-group has-error' >";
						else     
							echo "<div class='form-group' >";
					?>
						<label for="department_name" class="col-sm-3 control-label">Department <span class="required"> *</span></label>
						<div class="col-sm-7">
							<input type="text" name="department_name" class="form-control" placeholder="Enter your department name" />
							<p class="control-label col-md-12" style="text-align: left !important; padding-left: 0;"><?php echo form_error('department_name'); ?></p>
						</div>
						<div class="col-sm-2"></div>
					</div>
					
					<div id="add_new" class="margin">
						<?php 
							if(form_error('designation_name[]')) 
								echo "<div class='form-group has-error' >";
							else     
								echo "<div class='form-group' >";
						?>
							<label for="designations[]" class="col-sm-3 control-label">Add Designations <span class="required"> *</span></label>
							<div class="col-sm-7">
								<input type="text" name="designation_name[]" class="form-control" placeholder="Enter your department name" />
								<p class="control-label col-md-12" style="text-align: left !important; padding-left: 0;"><?php echo form_error('designation_name[]'); ?></p>
							</div>
							<div class="col-sm-2">                            
								<strong><a href="javascript:void(0);" id="add_more" class="btn btn-sm btn-success"><i class="fa fa-plus"></i>&nbsp;Add more</a></strong>
							</div>
						</div>
					</div>
					<div class="form-group margin">
						<div class="col-sm-offset-3 col-sm-5">
							<button type="submit" id="sbtn" class="btn btn-block btn-success"><i class="fa fa-save"></i>&nbsp; Save</button>                            
						</div>
					</div>
				</form>
				
				<script type="text/javascript">
					$(document).ready(function () {
						var maxAppend = 0;
						$("#add_more").click(function () {
							if (maxAppend >= 4)
							{
								alert("Maximum 5 designation is allowed");
							} else {
								var add_new = $('<div class="form-group">\n\
								<label for="field-1" class="col-sm-3 control-label">Add Designations <span class="required"> *</span></label>\n\
								<div class="col-sm-7">\n\<input type="text" name="designation_name[]" class="form-control" placeholder="Enter department designation"/>\n\
								</div>\n\
								<div class="col-sm-2">\n\
								<strong><a href="javascript:void(0);" class="remCF btn btn-sm btn-danger"><i class="fa fa-times"></i>&nbsp; Remove</a></strong>\n\
								</div>');
								maxAppend++;
								$("#add_new").append(add_new);
							}
						});

						$("#add_new").on('click', '.remCF', function () {
							$(this).parent().parent().parent().remove();
							maxAppend--;
						});

						// $(".")
					});
				</script>

            </div>
        </div>
    </div>
</div>
*/ ?>