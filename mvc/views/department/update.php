<!-- Header -->
<div class="row">
	<div class="col-md-2 hidden-xs">
		<img src="<?php echo base_url('uploads/images/logo.png'); ?>" style="width: auto; height: 40px; margin-top: -8px;" class="img-thumbnail img-responsive" />
	</div>
	<div class="col-md-7 col-xs-9 text-center">
		<h1 class="page-header"><?php echo $this->session->userdata('school');  ?></h1>
	</div>
	<div class="col-md-3 col-xs-3">
		<!-- begin breadcrumb -->
		<ol class="breadcrumb">
            <li><a href="<?=base_url("dashboard/index")?>"><i class="fa fa-laptop"></i> <?=$this->lang->line('menu_dashboard')?></a></li>
            <li><a href="<?php echo base_url().'department/index'; ?>">Department</a></li>
            <li class="active">Edit Designation</li>
        </ol>
		<!-- end breadcrumb -->
	</div>
</div>
<div id="page-container">
<!-- begin panel -->
<div class="panel panel-inverse" data-sortable-id="ui-widget-3">
	<div class="panel-heading">
		<div class="btn-group-vertical pull-right">
			<a href="<?php echo base_url('department/add') ?>" type="button" class="btn btn-success btn-xs">
				<i class="fa fa-plus add_margin"></i> Add Department
			</a>
		</div>
		<h4 class="panel-title"><i class="fa fa-list-ul"></i> Department</h4>
	</div>
<!--End header -->

<div class="panel-body">
	  <div class="box-body">
        <div class="row">
			<div class="col-sm-8" style="border-bottom:1px solid #F0F3F5; margin-bottom:10px;">
				<form method="post" role="form" >
					<?php 
						if(form_error('designation_name')) 
							echo "<div class='form-group has-error' >";
						else     
							echo "<div class='form-group' >";
					?>
						<label for="designation_name" class="col-sm-3 control-label">Designation Name <span class="required"> *</span></label>
						<div class="col-sm-7">
							<input type="text" name="designation_name" value="<?php echo $designation_details->designation_name ?>" class="form-control" placeholder="Enter new designation name" />
							<input type="hidden" name="department_id"  value="<?php echo $designation_details->designation_name ?>">
							<span class="col-sm-4 control-label">
								<?php echo form_error('designation_name'); ?>
							</span>
						</div>
					</div>
					
					<div class="form-group margin">
						<div class="col-sm-offset-3 col-sm-7" style="margin-top: 10px; padding-left: 10px; padding-right:10px; margin-bottom:20px;">
							<button type="submit" id="sbtn" class="btn btn-block btn-success"><i class="fa fa-save"></i>&nbsp; Save</button>                            
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>