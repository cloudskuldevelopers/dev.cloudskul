<!-- Header -->
<div class="row">
	<div class="col-md-2 hidden-xs">
		<img src="<?php echo base_url('uploads/images/logo.png'); ?>" style="width: auto; height: 40px; margin-top: -8px;" class="img-thumbnail img-responsive" />
	</div>
	<div class="col-md-7 col-xs-9 text-center">
		<h1 class="page-header"><?php echo $this->session->userdata('school');  ?></h1>
	</div>
	<div class="col-md-3 col-xs-3">
		<!-- begin breadcrumb -->
		<ol class="breadcrumb pull-right">
			<li><a href="<?=base_url("dashboard/index")?>"><i class="fa fa-laptop"></i> <?=$this->lang->line('menu_dashboard')?></a></li>
            <li class="active">Department</li>
		</ol>
		<!-- end breadcrumb -->
	</div>
</div>
<div id="page-container">
<!-- begin panel -->
<div class="panel panel-inverse" data-sortable-id="ui-widget-3">
	<div class="panel-heading">
		<div class="btn-group-vertical pull-right">
			<a href="<?php echo base_url('department/add') ?>" type="button" class="btn btn-success btn-xs">
				<i class="fa fa-plus add_margin"></i> Add Department
			</a>
		</div>
		<h4 class="panel-title"><i class="fa fa-list-ul"></i> Department</h4>
	</div>
<!--End header -->

<div class="panel-body">
    <!-- form start -->
    <div class="box-body">
        <div class="row">
			<div class="col-sm-12" style="border-bottom:1px solid #F0F3F5; margin-bottom:10px;">
				<div class="row add_border">
					
					<div class="col-md-6 col-md-offset-3 add_padding" style="margin-bottom:10px; padding: 25px 0px;">
						<div class="input-group">
							<span class="input-group-addon offset_addon"><?php echo $this->lang->line("panel_title"); ?></span>
							 <?php
								$array = array("0" => $this->lang->line("select_department"));
								foreach ($unique_school_departments as $v_department_name) {
									$array[$v_department_name->department_id] = $v_department_name->department_name;
								}
								echo form_dropdown("department_id", $array, set_value("department_id"), "id='department_id' class='form-control select2'");
							?>
						</div>
					</div>
				</div>
			</div>
				
            <div class="col-sm-12" style="padding-top: 25px; padding-bottom: 25px;">
				<span class="spinner" id="busy_icon"></span>
                <div id="hide-table">
                    
                </div>

            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
	$(document).ready(function(){
		$('#busy_icon').hide();
		$('#department_id').change(function(e){
			var department_id = $("#department_id").val();
			if(department_id == 0){
				e.preventDefault();
			}else{
				e.preventDefault();
				getDesignations();
			}
		});
	});
	
	function getDesignations(){
		var department_id = $("#department_id").val();

		$.ajax({
			type: 'POST',
			url: "<?php echo base_url() . 'department/designations_list/'; ?>" + department_id,
			data: "department_id=" + department_id,
			dataType: "html",
			success: function(data) {
				$('#hide-table').html(data);
			}
		});
	}
	
	$.ajaxSetup({
		beforeSend:function(e){
			$("#busy_icon").show();
		},
		complete:function(){
			$("#busy_icon").hide();
		}
	});
</script>