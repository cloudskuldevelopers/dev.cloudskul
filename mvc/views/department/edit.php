
<div class="box box-danger">
    <div class="box-header with-border">
        <h3 class="box-title"><i class="fa fa-list-ul"></i> Department</h3>

       
        <ol class="breadcrumb">
            <li><a href="<?=base_url("dashboard/index")?>"><i class="fa fa-laptop"></i> <?=$this->lang->line('menu_dashboard')?></a></li>
            <li><a href="<?php echo base_url().'department/index'; ?>">Department</a></li>
            <li class="active">Add Department</li>
        </ol>
    </div><!-- /.box-header -->
    <!-- form start -->
    
   <div class="box-body">
   		<div class="row">
            <div class="col-sm-12 col-md-12">
            	<h5 class="page-header">
					<a href="<?php echo base_url('department/add') ?>" class="btn btn-success btn-lg">
						<i class="fa fa-plus add_margin"></i> Add Department
					</a>
				</h5>
				
				<div class="col-sm-8">
					<form method="post" role="form" >
						<?php 
							if(form_error('department_name')) 
								echo "<div class='form-group has-error' >";
							else     
								echo "<div class='form-group' >";
						?>
							<label for="department_name" class="col-sm-3 control-label">Department <span class="required"> *</span></label>
							<div class="col-sm-7">
								<input type="text" name="department_name" value="<?php echo $department_details->department_name ?>" class="form-control" placeholder="Enter your department name" />
								<input type="hidden" name="department_id"  value="<?php echo $department_details->department_id ?>"> 
								<p class="control-label col-md-12" style="text-align: left !important; padding-left: 0; margin-top: 8px;"><?php echo form_error('department_name'); ?></p>
							</div>
							<div class="col-sm-2"></div>
						</div>
						
						<?php foreach ($all_department_info as $v_department_info) : ?>
							<?php 
								if(form_error('designation_name[]')) 
									echo "<div class='form-group has-error' >";
								else     
									echo "<div class='form-group' >";
							?>
								<label for="designations[]" class="col-sm-3 control-label">Add Designations <span class="required"> *</span></label>
								<div class="col-sm-7">
									<input type="text" name="designation_name[]" class="form-control" value="<?php echo $v_department_info->designation_name; ?>" placeholder="Enter your designation name" />
									<input type="hidden" name="designation_id[]"  value="<?php echo $v_department_info->designation_id ?>"> 
									<p class="control-label col-md-12" style="text-align: left !important; padding-left: 0; margin-top: 8px;"><?php echo form_error('designation_name[]'); ?></p>
								</div>
								<div class="col-sm-2"></div>
							</div>
						<?php endforeach; ?>
						
						<div class="form-group margin">
							<div class="col-sm-offset-3 col-sm-7" style="margin-top: 10px; padding-left: 10px; padding-right:10px; margin-bottom:20px;">
								<button type="submit" id="sbtn" class="btn btn-block btn-success"><i class="fa fa-save"></i>&nbsp; Save</button>                            
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>