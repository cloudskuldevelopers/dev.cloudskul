
<table id="data-table" class="table table-striped table-bordered nowrap" width="100%">
	<thead>
		<tr>
			<th class="col-lg-2"><?=$this->lang->line('slno')?></th>
			<th class="col-lg-2"><?=$this->lang->line('department_name')?></th>
			<th class="col-lg-2"><?=$this->lang->line('action')?></th> 
		</tr>
	</thead>
	<tbody>
		<?php if(count($unique_designations)) {$i = 1; foreach($unique_designations as $v_department) { ?>
			<tr>
				<td data-title="<?=$this->lang->line('slno')?>"><?php echo $i; ?>	</td>
				<td data-title="<?=$this->lang->line('department_name')?>"><?php echo $v_department->designation_name; ?></td>
				<td data-title="<?=$this->lang->line('action')?>">
					<?php echo btn_edit('department/update/'.$v_department->designation_id, $this->lang->line('edit')) ?>
					<?php echo btn_delete('department/delete_designation/'.$v_department->designation_id, $this->lang->line('delete')) ?>
				</td>			
			</tr>
		<?php $i++; }} ?>
	</tbody>
</table>