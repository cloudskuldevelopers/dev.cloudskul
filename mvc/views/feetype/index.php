<!-- ================== BEGIN BASE JS ================== -->
<script src="assets/plugins/pace/pace.min.js"></script>
<!-- ================== END BASE JS ================== -->
<div id="container">
<div class="row">
	<div class="col-md-2 hidden-xs">
		<img src="<?php echo base_url('uploads/images/logo.png'); ?>" style="width: auto; height: 40px; margin-top: -8px;" class="img-thumbnail img-responsive" />
	</div>
	<div class="col-md-7 col-xs-9 text-center">
		<h1 class="page-header"><?php echo $this->session->userdata('school');  ?></h1>
	</div>
	<div class="col-md-3 col-xs-3">
		<!-- begin breadcrumb -->
		<ol class="breadcrumb pull-right">
			<li ><a href="<?php echo base_url('dashboard/index');?>"><i class="fa fa-laptop"></i> Dashboard</a></li>
			<li class="">Fee Type</li>
		</ol>
		<!-- end breadcrumb -->
	</div>
</div>

<!-- begin panel -->
<div class="panel panel-inverse" data-sortable-id="ui-widget-3">
	<div class="panel-heading">
		<div class="btn-group-vertical pull-right">
			<a href="<?php echo base_url('feetype/add'); ?>" type="button" class="btn btn-success btn-xs">
				<i class="fa fa-plus"></i> 
				<?=$this->lang->line('add_title')?>
			</a>
		</div>
		<h3 class="panel-title"><i class="fa icon-feetype"></i> <?=$this->lang->line('panel_title')?></h3>
	</div>
	<div class="panel-body">
		<table id="data-table" class="table table-striped table-bordered nowrap" width="100%">
			<thead>
				<tr>
					<th class="col-sm-2"><?=$this->lang->line('slno')?></th>
					<th class="col-sm-2"><?=$this->lang->line('fee_cat')?></th>
					<th class="col-sm-2"><?=$this->lang->line('feetype_name')?></th>
					<th class="col-sm-2"><?=$this->lang->line('feetype_note')?></th>
					<th class="col-sm-2"><?=$this->lang->line('action')?></th>
				</tr>
			</thead>
			<tbody>
				<?php if(count($feetypes)) {$i = 1; foreach($feetypes as $feetype) { ?>
					<tr>
						<td data-title="<?=$this->lang->line('slno')?>">
							<?php echo $i; ?>
						</td>
						<td data-title="<?=$this->lang->line('fee_cat')?>">
							<?php echo $feetype->feecategory; ?>
						</td>
						<td data-title="<?=$this->lang->line('feetype_name')?>">
							<?php echo $feetype->feetype; ?>
						</td>
						<td data-title="<?=$this->lang->line('feetype_note')?>">
							<?php echo $feetype->note; ?>
						</td>
						<td data-title="<?=$this->lang->line('action')?>">
							<?php echo btn_edit('feetype/edit/'.$feetype->feetypeID, $this->lang->line('edit')) ?>
							&nbsp;&nbsp;
							<?php echo btn_delete('feetype/delete/'.$feetype->feetypeID, $this->lang->line('delete')) ?>
						</td>
					</tr>
				<?php $i++; }} ?>
			</tbody>
		</table>
	</div>
</div>
<!-- end panel -->

<!-- begin scroll to top btn -->
<a href="javascript:;" class="btn btn-icon btn-circle btn-success btn-scroll-to-top fade" data-click="scroll-top"><i class="fa fa-angle-up"></i></a>
<!-- end scroll to top btn -->

</div>
<!-- end page container -->

<?php /* 
<div class="box box-danger">
    <div class="box-header with-border">
        <h3 class="box-title"><i class="fa icon-feetype"></i> <?=$this->lang->line('panel_title')?></h3>

       
        <ol class="breadcrumb">
            <li><a href="<?=base_url("dashboard/index")?>"><i class="fa fa-laptop"></i> <?=$this->lang->line('menu_dashboard')?></a></li>
            <li class="active"><?=$this->lang->line('menu_feetype')?></li>
        </ol>
    </div><!-- /.box-header -->
    <!-- form start -->
    <div class="box-body">
        <div class="row">
            <div class="col-sm-12">

                <h5 class="page-header">
                    <a href="<?php echo base_url('feetype/add') ?>">
                        <i class="fa fa-plus"></i> 
                        <?=$this->lang->line('add_title')?>
                    </a>
                </h5>
                <div id="hide-table">
                    <table id="example1" class="table table-striped table-bordered table-hover dataTable no-footer">
                        <thead>
                            <tr>
                                <th class="col-sm-2"><?=$this->lang->line('slno')?></th>
                                <th class="col-sm-2"><?=$this->lang->line('fee_cat')?></th>
                                <th class="col-sm-2"><?=$this->lang->line('feetype_name')?></th>
                                <th class="col-sm-2"><?=$this->lang->line('feetype_note')?></th>
                                <th class="col-sm-2"><?=$this->lang->line('action')?></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php if(count($feetypes)) {$i = 1; foreach($feetypes as $feetype) { ?>
                                <tr>
                                    <td data-title="<?=$this->lang->line('slno')?>">
                                        <?php echo $i; ?>
                                    </td>
									<td data-title="<?=$this->lang->line('fee_cat')?>">
                                        <?php echo $feetype->feecategory; ?>
                                    </td>
                                    <td data-title="<?=$this->lang->line('feetype_name')?>">
                                        <?php echo $feetype->feetype; ?>
                                    </td>
                                    <td data-title="<?=$this->lang->line('feetype_note')?>">
                                        <?php echo $feetype->note; ?>
                                    </td>
                                    <td data-title="<?=$this->lang->line('action')?>">
                                        <?php echo btn_edit('feetype/edit/'.$feetype->feetypeID, $this->lang->line('edit')) ?>
                                        <?php echo btn_delete('feetype/delete/'.$feetype->feetypeID, $this->lang->line('delete')) ?>
                                    </td>
                                </tr>
                            <?php $i++; }} ?>
                        </tbody>
                    </table>
                </div>

            </div>
        </div>
    </div>
</div>
*/ ?>