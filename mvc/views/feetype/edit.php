<div id="container">
<!-- TOP BAR -->
<div class="row">
	<div class="col-md-2 hidden-xs">
		<img src="<?php echo base_url('uploads/images/logo.png'); ?>" style="width: auto; height: 40px; margin-top: -8px;" class="img-thumbnail img-responsive" />
	</div>
	<div class="col-md-6 col-xs-9 text-center">
		<h1 class="page-header"><?php echo $this->session->userdata('school');  ?></h1>
	</div>
	<div class="col-md-4 col-xs-3">
		<!-- begin breadcrumb -->
		<ol class="breadcrumb pull-right">
			<li ><a href="<?php echo base_url('dashboard/index');?>"><i class="fa fa-laptop"></i> Dashboard</a></li>
			<li><a href="<?php echo base_url('feetype/index');?>">Fee Type</a></li>
			<li class="">Edit Fee Type</li>
		</ol>
		<!-- end breadcrumb -->
	</div>
</div>
<!-- END TOP BAR -->

<!-- begin panel -->
<div class="panel panel-inverse" data-sortable-id="ui-widget-3">
	<div class="panel-heading">
		<h4 class="panel-title"><i class="fa fa-institution"></i> <?=$this->lang->line('panel_title')?></h4>
	</div>
	<div class="panel-body">
		<h3 class="profileBody" style="font-weight: lighter; font-size: 22px; margin-bottom: 30px;">Edit Fee Type Details</h3>
		<form class="form-horizontal" role="form" method="post">
			<?php 
				if(form_error('feetype')) 
					echo "<div class='form-group has-error' >";
				else     
					echo "<div class='form-group' >";
			?>
				<label for="feetype" class="col-sm-2 control-label">
					<?=$this->lang->line("feetype_name")?>
				</label>
				<div class="col-sm-6">
					<input type="text" class="form-control" id="feetype" name="feetype" value="<?=set_value('feetype', $feetype->feetype)?>" >
				</div>
				<span class="col-sm-4 control-label">
					<?php echo form_error('feetype'); ?>
				</span>
			</div>

			<?php 
				if(form_error('note')) 
					echo "<div class='form-group has-error' >";
				else     
					echo "<div class='form-group' >";
			?>
				<label for="note" class="col-sm-2 control-label">
					<?=$this->lang->line("feetype_note")?>
				</label>
				<div class="col-sm-6">
					<textarea class="form-control" style="resize:none;" id="note" name="note"><?=set_value('note', $feetype->note);?></textarea>
				</div>
				<span class="col-sm-4 control-label">
					<?php echo form_error('note'); ?>
				</span>
			</div>

			<div class="form-group">
				<div class="col-sm-offset-2 col-sm-8">
					<input type="submit" class="btn btn-success" value="<?=$this->lang->line("update_feetype")?>" >
				</div>
			</div>

		</form>
	</div>
</div>

<!-- begin scroll to top btn -->
<a href="javascript:;" class="btn btn-icon btn-circle btn-success btn-scroll-to-top fade" data-click="scroll-top"><i class="fa fa-angle-up"></i></a>
<!-- end scroll to top btn -->

</div>
<!-- end page container -->

<?php /*
<div class="box">
    <div class="box-header">
        <h3 class="box-title"><i class="fa icon-feetype"></i> <?=$this->lang->line('panel_title')?></h3>

        <ol class="breadcrumb">
            <li><a href="<?=base_url("dashboard/index")?>"><i class="fa fa-laptop"></i> <?=$this->lang->line('menu_dashboard')?></a></li>
            <li><a href="<?=base_url("feetype/index")?>"><?=$this->lang->line('menu_feetype')?></a></li>
            <li class="active"><?=$this->lang->line('menu_edit')?> <?=$this->lang->line('menu_feetype')?></li>
        </ol>
    </div><!-- /.box-header -->
    <!-- form start -->
    <div class="box-body">
        <div class="row">
            <div class="col-sm-8">
                <form class="form-horizontal" role="form" method="post">

                   <?php 
                        if(form_error('feetype')) 
                            echo "<div class='form-group has-error' >";
                        else     
                            echo "<div class='form-group' >";
                    ?>
                        <label for="feetype" class="col-sm-2 control-label">
                            <?=$this->lang->line("feetype_name")?>
                        </label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" id="feetype" name="feetype" value="<?=set_value('feetype', $feetype->feetype)?>" >
                        </div>
                        <span class="col-sm-4 control-label">
                            <?php echo form_error('feetype'); ?>
                        </span>
                    </div>

                    <?php 
                        if(form_error('note')) 
                            echo "<div class='form-group has-error' >";
                        else     
                            echo "<div class='form-group' >";
                    ?>
                        <label for="note" class="col-sm-2 control-label">
                            <?=$this->lang->line("feetype_note")?>
                        </label>
                        <div class="col-sm-6">
                            <textarea class="form-control" style="resize:none;" id="note" name="note"><?=set_value('note', $feetype->note);?></textarea>
                        </div>
                        <span class="col-sm-4 control-label">
                            <?php echo form_error('note'); ?>
                        </span>
                    </div>

                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-8">
                            <input type="submit" class="btn btn-success" value="<?=$this->lang->line("update_feetype")?>" >
                        </div>
                    </div>

                </form>

            </div>
        </div>
    </div>
</div>
*/ ?>
