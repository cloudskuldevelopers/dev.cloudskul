
<div class="form-box" id="login-box" style="margin-bottom: 20px; background-color: #fff; padding-bottom: 60px; border-bottom: 4px solid transparent; border-color: #00ACAC;">
    <div class="header" style="background-color: #fff; box-shadow: none; border-bottom: 1px solid rgb(29, 175, 237); padding-top: 8px; padding-bottom: 15px;">
		<center>
			<img src="<?php echo base_url('uploads/images/logo.png'); ?>" style="width: 180px; height: auto;" class="img-responsive" />
		</center>
	</div>
	
    <form role="form" method="post">
        <div class="body white-bg">

        	<?php 
	            if($form_validation == "No"){
	            } else {
	                if(count($form_validation)) {
	                    echo "<div class=\"alert alert-danger alert-dismissable\">
                            <i class=\"fa fa-ban\"></i>
                            <button aria-hidden=\"true\" data-dismiss=\"alert\" class=\"close\" type=\"button\">×</button>
                            $form_validation
                        </div>";
	                }
	            }

	            if($this->session->flashdata('reset_send')) {
                    $message = $this->session->flashdata('reset_send');
                    echo "<div class=\"alert alert-success alert-dismissable\">
                        <i class=\"fa fa-ban\"></i>
                        <button aria-hidden=\"true\" data-dismiss=\"alert\" class=\"close\" type=\"button\">×</button>
                        $message
                    </div>";
                } else {
                	if($this->session->flashdata('reset_error')) {
	                    $message = $this->session->flashdata('reset_error');
	                    echo "<div class=\"alert alert-danger alert-dismissable\">
	                        <i class=\"fa fa-ban\"></i>
	                        <button aria-hidden=\"true\" data-dismiss=\"alert\" class=\"close\" type=\"button\">×</button>
	                        $message
	                    </div>";
	                }
                }
	        ?>
			
			
			<div class="input-group" style="margin: 20px 0px;">
				<span class="input-group-addon" style="background-color: rgb(255, 255, 255);"><i class="fa fa-envelope-o"></i></span>
				<input type="email" placeholder="Enter your email" class="form-control" id="email" name="email" value="<?=set_value('email')?>" required>
			</div>

			<div class="form-group">
				<button class="btn btn-lg btn-success btn-block" type="submit">
					<i class="fa fa-check-circle" style="margin-left:5px;"></i>
					Send
				</button>
			</div>
			
			<div class="col-md-6">
				<div class="row">
					<a href="<?=base_url("signin/index")?>" class="btn btn-danger btn-block">
						<i class="fa fa-power-off" style="margin-left:8px;"></i>
						Back to Login
					</a>
				</div>
			</div>
			
			<div class="col-md-6">
				<div class="row">
					<a href="<?=base_url("dashboard/index")?>" class="btn btn-info btn-block">
						<i class="fa fa-home" style="margin-left:8px;"></i>
						Back to Home
					</a>
				</div>
			</div>
        </div>
    </form>
</div>