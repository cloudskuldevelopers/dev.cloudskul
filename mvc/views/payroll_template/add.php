<div id="container">
<!-- TOP BAR -->
<div class="row">
	<div class="col-md-1 hidden-xs">
		<img src="<?php echo base_url('uploads/images/logo.png'); ?>" style="width: auto; height: 40px; margin-top: -8px;" class="img-thumbnail img-responsive" />
	</div>
	<div class="col-md-6 col-xs-9 text-center">
		<h1 class="page-header"><?php echo $this->session->userdata('school');  ?></h1>
	</div>
	<div class="col-md-5 col-xs-3">
		<!-- begin breadcrumb -->
		<ol class="breadcrumb pull-right">
			<li><a href="<?=base_url("dashboard/index")?>"><i class="fa fa-laptop"></i> <?=$this->lang->line('menu_dashboard')?></a></li>
            <li><a href="<?php echo base_url().'payroll_template/index'; ?>">Salary Template</a></li>
            <li class="active">Add Salary Template</li>
		</ol>
		<!-- end breadcrumb -->
	</div>
</div>
<!-- END TOP BAR -->
	
<!-- begin panel -->
<div class="panel panel-inverse" data-sortable-id="ui-widget-3">
	<div class="panel-heading">
		<h4 class="panel-title"><i class="fa fa-list-ol"></i> Salary Template</h3></h4>
	</div>
	<div class="panel-body">
		<div class="col-sm-12">
			<form class="form-horizontal" action="" role="form" method="post">
				<?php 
					if(form_error('template_name')) 
						echo "<div class='form-group has-error' >";
					else     
						echo "<div class='form-group' >";
				?>
					<label for="template_name" class="col-sm-3 control-label">Template Name <span class="required"> *</span></label>
					<div class="col-sm-7">
						<input type="text" name="template_name" class="form-control" placeholder="Enter template name"  value="<?=set_value('template_name')?>"/>
						<p class="control-label col-md-12" style="text-align: left !important; padding-left: 0;"><?php echo form_error('template_name'); ?></p>
					</div>
					<div class="col-sm-2"></div>
				</div>
				
				<?php 
					if(form_error('monthly_rate')) 
						echo "<div class='form-group has-error' >";
					else     
						echo "<div class='form-group' >";
				?>
					<label for="payroll_template" class="col-sm-3 control-label">Monthly Rate <span class="required"> *</span></label>
					<div class="col-sm-7">
						<input type="text" name="monthly_rate" class="form-control" placeholder="Enter monthly rate" value="<?=set_value('monthly_rate')?>" />
						<p class="control-label col-md-12" style="text-align: left !important; padding-left: 0;"><?php echo form_error('monthly_rate'); ?></p>
					</div>
				</div>
				
				<div class='form-group' >
					<label for="overtime_rate" class="col-sm-3 control-label">Overtime Rate</label>
					<div class="col-sm-7">
						<input type="text" name="overtime_rate" class="form-control" placeholder="Enter hourly rate (optional)" value="<?=set_value('overtime_rate')?>" />
						<p class="control-label col-md-12" style="text-align: left !important; padding-left: 0;"><?php echo form_error('overtime_rate'); ?></p>
					</div>
				</div>
					
					
				<div class="form-group margin">
					<div class="col-sm-offset-3 col-sm-7">
						<button type="submit" id="sbtn" class="btn btn-block btn-success"><i class="fa fa-save"></i>&nbsp; Save</button>                            
					</div>
				</div>
			</form>
		</div>
	</div>
</div>
<!-- end panel -->

<!-- begin scroll to top btn -->
<a href="javascript:;" class="btn btn-icon btn-circle btn-success btn-scroll-to-top fade" data-click="scroll-top"><i class="fa fa-angle-up"></i></a>
<!-- end scroll to top btn -->

</div>
<!-- end page container -->


<?php /*
<div class="box box-danger">
    <div class="box-header with-border">
        <h3 class="box-title"><i class="fa fa-list-ol"></i> Salary Template</h3>

       
        <ol class="breadcrumb">
            <li><a href="<?=base_url("dashboard/index")?>"><i class="fa fa-laptop"></i> <?=$this->lang->line('menu_dashboard')?></a></li>
            <li><a href="<?php echo base_url().'payroll_template/index'; ?>">Salary Template</a></li>
            <li class="active">Add Salary Template</li>
        </ol>
    </div><!-- /.box-header -->
    <!-- form start -->
    <div class="box-body">
        <div class="row">
            <div class="col-sm-8">
                <form class="form-horizontal" action="" role="form" method="post">
					<?php 
						if(form_error('template_name')) 
							echo "<div class='form-group has-error' >";
						else     
							echo "<div class='form-group' >";
					?>
						<label for="template_name" class="col-sm-3 control-label">Template Name <span class="required"> *</span></label>
						<div class="col-sm-7">
							<input type="text" name="template_name" class="form-control" placeholder="Enter template name"  value="<?=set_value('template_name')?>"/>
							<p class="control-label col-md-12" style="text-align: left !important; padding-left: 0;"><?php echo form_error('template_name'); ?></p>
						</div>
						<div class="col-sm-2"></div>
					</div>
					
					<?php 
						if(form_error('monthly_rate')) 
							echo "<div class='form-group has-error' >";
						else     
							echo "<div class='form-group' >";
					?>
						<label for="payroll_template" class="col-sm-3 control-label">Monthly Rate <span class="required"> *</span></label>
						<div class="col-sm-7">
							<input type="text" name="monthly_rate" class="form-control" placeholder="Enter monthly rate" value="<?=set_value('monthly_rate')?>" />
							<p class="control-label col-md-12" style="text-align: left !important; padding-left: 0;"><?php echo form_error('monthly_rate'); ?></p>
						</div>
					</div>
					
					<div class='form-group' >
						<label for="overtime_rate" class="col-sm-3 control-label">Overtime Rate</label>
						<div class="col-sm-7">
							<input type="text" name="overtime_rate" class="form-control" placeholder="Enter hourly rate (optional)" value="<?=set_value('overtime_rate')?>" />
							<p class="control-label col-md-12" style="text-align: left !important; padding-left: 0;"><?php echo form_error('overtime_rate'); ?></p>
						</div>
					</div>
						
						
					<div class="form-group margin">
						<div class="col-sm-offset-3 col-sm-7">
							<button type="submit" id="sbtn" class="btn btn-block btn-success"><i class="fa fa-save"></i>&nbsp; Save</button>                            
						</div>
					</div>
				</form>

            </div>
        </div>
    </div>
</div>
*/ ?>