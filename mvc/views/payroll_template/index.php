<div id="container">
<div class="row">
	<div class="col-md-2 hidden-xs">
		<img src="<?php echo base_url('uploads/images/logo.png'); ?>" style="width: auto; height: 40px; margin-top: -8px;" class="img-thumbnail img-responsive" />
	</div>
	<div class="col-md-7 col-xs-9 text-center">
		<h1 class="page-header"><?php echo $this->session->userdata('school');  ?></h1>
	</div>
	<div class="col-md-3 col-xs-3">
		<!-- begin breadcrumb -->
		<ol class="breadcrumb pull-right">
			<li><a href="<?=base_url("dashboard/index")?>"><i class="fa fa-laptop"></i> <?=$this->lang->line('menu_dashboard')?></a></li>
            <li class="active">Payroll Template</li>
		</ol>
		<!-- end breadcrumb -->
	</div>
</div>

<!-- begin panel -->
<div class="panel panel-inverse" data-sortable-id="ui-widget-3">
	<div class="panel-heading">
		<div class="btn-group-vertical pull-right">
			<a href="<?php echo base_url('payroll_template/add'); ?>" type="button" class="btn btn-success btn-xs">
				<i class="fa fa-plus add_margin"></i> Add Salary Template
			</a>
		</div>
		<h3 class="panel-title"><i class="fa fa-tasks"></i> Payroll Template</h3>
	</div>
	<div class="panel-body">
		<table id="data-table" class="table table-striped table-bordered nowrap" width="100%">
			<thead>
				<tr>
					<th class="col-sm-1">#</th>
					<th class="col-sm-2">Template Name</th>
					<th class="col-sm-2">Monthly Rate</th>
					<th class="col-sm-1">Hourly Rate</th>
					<th class="col-sm-1">Action</th>
				</tr>
			</thead>
			<tbody>
				<?php if(count($employee_salary_template)) {$i = 1; foreach($employee_salary_template as $salary_template) { ?>
					<tr>
						<td data-title="<?=$this->lang->line('slno')?>">
							<?php echo $i; ?>
						</td>

						<td>
							<?php echo $salary_template->template_name; ?>
						</td>

						<td>
							<?php echo number_format(floatval($salary_template->monthly_rate),2); ?>
						</td>
						
						<td>
							<?php echo number_format(floatval($salary_template->overtime_rate),2); ?>
						</td>
						
						<td>
							<a href="<?php echo base_url() . "payroll_template/edit_employee_payroll/" . $salary_template->payroll_id; ?>" class="btn btn-warning btn-xs mrg" data-placement="top" data-toggle="tooltip" data-original-title="Edit"><i class="fa fa-edit"></i></a>
							
							<a href="<?php echo base_url() . "payroll_template/delete_employee_payroll/" . $salary_template->payroll_id; ?>" onclick="return confirm('you are about to delete a record. This cannot be undone. are you sure?')" class="btn btn-danger btn-xs mrg" data-placement="top" data-toggle="tooltip" data-original-title="Delete"><i class="fa fa-trash-o"></i></a>
						</td>
					</tr>
				<?php $i++; }} ?>
			</tbody>
		</table>
	</div>
</div>
<!-- end panel -->

<!-- begin scroll to top btn -->
<a href="javascript:;" class="btn btn-icon btn-circle btn-success btn-scroll-to-top fade" data-click="scroll-top"><i class="fa fa-angle-up"></i></a>
<!-- end scroll to top btn -->

</div>
<!-- end page container -->


<?php /*
<div class="box box-danger">
    <div class="box-header with-border">
        <h3 class="box-title"><i class="fa fa-tasks"></i> Payroll Template</h3>

       
        <ol class="breadcrumb">
            <li><a href="<?=base_url("dashboard/index")?>"><i class="fa fa-laptop"></i> <?=$this->lang->line('menu_dashboard')?></a></li>
            <li class="active">Payroll Template</li>
        </ol>
    </div><!-- /.box-header -->
    <!-- form start -->
	
	<div class="box-body">
		<div class="row">
            <div class="col-sm-12 col-md-12">
				<h5 class="page-header">
					<a href="<?php echo base_url('payroll_template/add') ?>" class="btn btn-success btn-lg">
						<i class="fa fa-plus add_margin"></i> Add Salary Template
					</a>
				</h5>
				
				<div id="hide-table">
                    <table id="example1" class="table table-striped table-bordered table-hover dataTable no-footer">
                        <thead>
                            <tr>
                                <th class="col-sm-1">#</th>
                                <th class="col-sm-2">Template Name</th>
                                <th class="col-sm-2">Monthly Rate</th>
								<th class="col-sm-1">Hourly Rate</th>
								<th class="col-sm-1">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php if(count($employee_salary_template)) {$i = 1; foreach($employee_salary_template as $salary_template) { ?>
                                <tr>
                                    <td data-title="<?=$this->lang->line('slno')?>">
                                        <?php echo $i; ?>
                                    </td>

                                    <td>
                                        <?php echo $salary_template->template_name; ?>
                                    </td>

                                    <td>
                                        <?php echo number_format(floatval($salary_template->monthly_rate),2); ?>
                                    </td>
									
									<td>
										<?php echo number_format(floatval($salary_template->overtime_rate),2); ?>
                                    </td>
									
                                    <td>
										<a href="<?php echo base_url() . "payroll_template/edit_employee_payroll/" . $salary_template->payroll_id; ?>" class="btn btn-warning btn-xs mrg" data-placement="top" data-toggle="tooltip" data-original-title="Edit"><i class="fa fa-edit"></i></a>
										
										<a href="<?php echo base_url() . "payroll_template/delete_employee_payroll/" . $salary_template->payroll_id; ?>" onclick="return confirm('you are about to delete a record. This cannot be undone. are you sure?')" class="btn btn-danger btn-xs mrg" data-placement="top" data-toggle="tooltip" data-original-title="Delete"><i class="fa fa-trash-o"></i></a>
									</td>
                                </tr>
                            <?php $i++; }} ?>
                        </tbody>
                    </table>
                </div>
				
			</div>
		</div>
	</div>
</div>
*/ ?>