
<div class="box">
    <div class="box-header">
        <h3 class="box-title"><i class="fa fa-puzzle-piece"></i> <?=$this->lang->line('panel_title')?></h3>

       
        <ol class="breadcrumb">
            <li><a href="<?=base_url("dashboard/index")?>"><i class="fa fa-laptop"></i> <?=$this->lang->line('menu_dashboard')?></a></li>
            <li class="active"><?=$this->lang->line('menu_examschedule')?></li>
        </ol>
    </div><!-- /.box-header -->
    <!-- form start -->
    <div class="box-body">
        <div class="row">
            <div class="col-sm-12">
				<div class="col-md-6 col-sm-offset-3 add_padding">
					<blockquote style="border-right: 5px solid #eee;">
						<form style="" class="form-horizontal" role="form" method="post">
							<div class="input-group">
								<label for="classesID" class="col-sm-2 control-label" style="padding-left: 0px;">
									<?=$this->lang->line("examschedule_student")?>
								</label>
								<span class="input-group-addon offset_addon"><i class="fa fa-sitemap"></i></span>
								<?php
									$array = array("0" => $this->lang->line("examschedule_select_student"));
									if($students) {
										foreach ($students as $student) {
											$array[$student->studentID] = $student->name;
										}
									}
									echo form_dropdown("studentID", $array, set_value("studentID"), "id='studentID' class='form-control'");
								?>
							</div>
						</form>
					</blockquote>
				</div>

            </div> <!-- col-sm-12 -->
            
        </div><!-- row -->
    </div><!-- Body -->
</div><!-- /.box -->
<script type="text/javascript">
    $('#studentID').change(function() {
        var studentID = $(this).val();
        if(studentID == 0) {
            $('#hide-table').hide();
            $('.nav-tabs-custom').hide();
        } else {
            $.ajax({
                type: 'POST',
                url: "<?=base_url('examschedule/student_list')?>",
                data: "id=" + studentID,
                dataType: "html",
                success: function(data) {
                    window.location.href = data;
                }
            });
        }
    });
</script>