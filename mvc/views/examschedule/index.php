<div class="row">
	<div class="col-md-2 hidden-xs">
		<img src="<?php echo base_url('uploads/images/logo.png'); ?>" style="width: auto; height: 40px; margin-top: -8px;" class="img-thumbnail img-responsive" />
	</div>
	<div class="col-md-7 col-xs-9 text-center">
		<h1 class="page-header"><?php echo $this->session->userdata('school');  ?></h1>
	</div>
	<div class="col-md-3 col-xs-3">
		<!-- begin breadcrumb -->
		<ol class="breadcrumb pull-right">
			<li ><a href="<?php echo base_url('dashboard/index');?>"><i class="fa fa-laptop"></i> Dashboard</a></li>
			<li class="">Exam Schedule</li>
		</ol>
		<!-- end breadcrumb -->
	</div>
</div>

<div id="page-container">
<!-- begin panel -->
<div class="panel panel-inverse" data-sortable-id="ui-widget-3">
	<div class="panel-heading">
		<?php 
			$usertype = $this->session->userdata("usertype");
			if($usertype == "Super Admin" || $usertype == "Admin" || $usertype=="Teacher") {
				if($usertype == "Admin" || $usertype == "Super Admin") {
		?>
		<div class="btn-group-vertical pull-right">
			<a href="<?php echo base_url('examschedule/add') ?>" type="button" class="btn btn-success btn-xs"><i class="fa fa-plus"></i> Add an Exam Schedule</a>
		</div>
		<?php } ?>
		<h4 class="panel-title"><i class="fa fa-puzzle-piece"></i> <?=$this->lang->line('panel_title')?></h4>
	</div>
	<div class="panel-body">
		<?php
			$array = array("0" => $this->lang->line("select_class"));
			foreach ($classes as $classa) {
				$array[$classa->classesID] = $classa->classes;
			}
			echo form_dropdown("classesID", $array, set_value("classesID", $set), "id='classesID' class='form-control offset_height'");
			echo  '<br />';
		?>
		<?php if(count($examschedules) > 0 ) { ?>

			<div class="col-sm-12">

				<div class="nav-tabs-custom b1">
					<ul class="nav nav-tabs">
						<li class="active"><a data-toggle="tab" href="#all" aria-expanded="true"><?=$this->lang->line("examschedule_all_examschedule")?></a></li>
						<?php foreach ($sections as $key => $section) {
							echo '<li class=""><a data-toggle="tab" href="#'. $section->schoolID.$section->sectionID .'" aria-expanded="false">'. $this->lang->line("student_section")." ".$section->section. " ( ". $section->category." )".'</a></li>';
						} ?>
					</ul>



					<div class="tab-content">
						<div id="all" class="tab-pane active">

							<div id="hide-table">
								<table id="example1" class="table table-striped table-bordered table-hover dataTable no-footer">
									<thead>
										<tr>
											<th><?=$this->lang->line('slno')?></th>
											<th><?=$this->lang->line('examschedule_name')?></th>
											<th><?=$this->lang->line('examschedule_classes')?></th>
											<th><?=$this->lang->line('examschedule_section')?></th>
											<th><?=$this->lang->line('examschedule_subject')?></th>
											<th><?=$this->lang->line('examschedule_date')?></th>
											<th><?=$this->lang->line('examschedule_time')?></th>
											<th><?=$this->lang->line('examschedule_room')?></th>
											<?php if($usertype == "Admin" || $usertype == "Super Admin") { ?>
											<th><?=$this->lang->line('action')?></th>
											<?php } ?>
										</tr>
									</thead>
									<tbody>
										<?php if(count($examschedules)) {$i = 1; foreach($examschedules as $examschedule) { ?>
											<tr>
												<td data-title="<?=$this->lang->line('slno')?>">
													<?php echo $i; ?>
												</td>
												<td data-title="<?=$this->lang->line('examschedule_name')?>">
													<?php echo $examschedule->exam; ?>
												</td>
												<td data-title="<?=$this->lang->line('examschedule_classes')?>">
													<?php echo $examschedule->classes; ?>
												</td>
												<td data-title="<?=$this->lang->line('examschedule_section')?>">
													<?php echo $examschedule->section; ?>
												</td>
												<td data-title="<?=$this->lang->line('examschedule_subject')?>">
													<?php echo $examschedule->subject; ?>
												</td>

												<td data-title="<?=$this->lang->line('examschedule_time')?>">
													<?php echo date("d M Y", strtotime($examschedule->edate)); ?>
												</td>

												<td data-title="<?=$this->lang->line('examschedule_time')?>">
													<?php echo $examschedule->examfrom, " - ", $examschedule->examto ; ?>
												</td>

												<td data-title="<?=$this->lang->line('examschedule_room')?>">
													<?php echo $examschedule->room; ?>
												</td>

												<?php if($usertype == "Admin" || $usertype == "Super Admin") { ?>
												<td data-title="<?=$this->lang->line('action')?>">
													<?php echo btn_edit('examschedule/edit/'.$examschedule->examscheduleID."/".$set, $this->lang->line('edit')) ?>
													<?php echo btn_delete('examschedule/delete/'.$examschedule->examscheduleID."/".$set, $this->lang->line('delete')) ?>
												</td>
												<?php } ?>
											</tr>
										<?php $i++; }} ?>
									</tbody>
								</table>
							</div>

						</div>

						<?php foreach ($sections as $key => $section) { ?>
								<div id="<?=$section->schoolID.$section->sectionID?>" class="tab-pane">
									
									<div id="hide-table">
										<table id="example1" class="table table-striped table-bordered table-hover dataTable no-footer">
											<thead>
												<tr>
													<th><?=$this->lang->line('slno')?></th>
													<th><?=$this->lang->line('examschedule_name')?></th>
													<th><?=$this->lang->line('examschedule_classes')?></th>
													<th><?=$this->lang->line('examschedule_subject')?></th>
													<th><?=$this->lang->line('examschedule_date')?></th>
													<th><?=$this->lang->line('examschedule_time')?></th>
													<th><?=$this->lang->line('examschedule_room')?></th>
													<?php if($usertype == "Admin" || $usertype == "Super Admin") { ?>
													<th><?=$this->lang->line('action')?></th>
													<?php } ?>
												</tr>
											</thead>
											<tbody>
												<?php if(count($allsection[$section->sectionID])) {$i = 1; foreach($allsection[$section->sectionID] as $examschedule) { ?>
													<tr>
														<td data-title="<?=$this->lang->line('slno')?>">
															<?php echo $i; ?>
														</td>
														<td data-title="<?=$this->lang->line('examschedule_name')?>">
															<?php echo $examschedule->exam; ?>
														</td>
														<td data-title="<?=$this->lang->line('examschedule_classes')?>">
															<?php echo $examschedule->classes; ?>
														</td>
	
														<td data-title="<?=$this->lang->line('examschedule_subject')?>">
															<?php echo $examschedule->subject; ?>
														</td>

														<td data-title="<?=$this->lang->line('examschedule_time')?>">
															<?php echo date("d M Y", strtotime($examschedule->edate)); ?>
														</td>

														<td data-title="<?=$this->lang->line('examschedule_time')?>">
															<?php echo $examschedule->examfrom, " - ", $examschedule->examto ; ?>
														</td>

														<td data-title="<?=$this->lang->line('examschedule_room')?>">
															<?php echo $examschedule->room; ?>
														</td>

														<?php if($usertype == "Admin" || $usertype == "Super Admin") { ?>
														<td data-title="<?=$this->lang->line('action')?>">
															<?php echo btn_edit('examschedule/edit/'.$examschedule->examscheduleID."/".$set, $this->lang->line('edit')) ?>
															<?php echo btn_delete('examschedule/delete/'.$examschedule->examscheduleID."/".$set, $this->lang->line('delete')) ?>
														</td>
														<?php } ?>
													</tr>
												<?php $i++; }} ?>
											</tbody>
										</table>
									</div>

								</div>
						<?php } ?>
					</div>

				</div> <!-- nav-tabs-custom -->
			</div> <!-- col-sm-12 for tab -->

		<?php } else { ?>
			<div class="col-sm-12">

				<div class="nav-tabs-custom">
					<ul class="nav nav-tabs">
						<li class="active"><a data-toggle="tab" href="#all" aria-expanded="true"><?=$this->lang->line("examschedule_all_examschedule")?></a></li>
					</ul>


					<div class="tab-content">
						<div id="all" class="tab-pane active">
							<div id="hide-table">
								<table id="example1" class="table table-striped table-bordered table-hover dataTable no-footer">
									<thead>
										<tr>
											<th><?=$this->lang->line('slno')?></th>
											<th><?=$this->lang->line('examschedule_name')?></th>
											<th><?=$this->lang->line('examschedule_classes')?></th>
											<th><?=$this->lang->line('examschedule_section')?></th>
											<th><?=$this->lang->line('examschedule_subject')?></th>
											<th><?=$this->lang->line('examschedule_date')?></th>
											<th><?=$this->lang->line('examschedule_time')?></th>
											<th><?=$this->lang->line('examschedule_room')?></th>
											<?php if($usertype == "Admin" || $usertype == "Super Admin") { ?>
											<th><?=$this->lang->line('action')?></th>
											<?php } ?>
										</tr>
									</thead>
									<tbody>
										<?php if(count($examschedules)) {$i = 1; foreach($examschedules as $examschedule) { ?>
											<tr>
												<td data-title="<?=$this->lang->line('slno')?>">
													<?php echo $i; ?>
												</td>
												<td data-title="<?=$this->lang->line('examschedule_name')?>">
													<?php echo $examschedule->exam; ?>
												</td>
												<td data-title="<?=$this->lang->line('examschedule_classes')?>">
													<?php echo $examschedule->classes; ?>
												</td>
												<td data-title="<?=$this->lang->line('examschedule_section')?>">
													<?php echo $examschedule->section; ?>
												</td>
												<td data-title="<?=$this->lang->line('examschedule_subject')?>">
													<?php echo $examschedule->subject; ?>
												</td>

												<td data-title="<?=$this->lang->line('examschedule_time')?>">
													<?php echo date("d M Y", strtotime($examschedule->edate)); ?>
												</td>

												<td data-title="<?=$this->lang->line('examschedule_time')?>">
													<?php echo $examschedule->examfrom, " - ", $examschedule->examto ; ?>
												</td>

												<td data-title="<?=$this->lang->line('examschedule_room')?>">
													<?php echo $examschedule->room; ?>
												</td>

												<?php if($usertype == "Admin" || $usertype == "Super Admin") { ?>
												<td data-title="<?=$this->lang->line('action')?>">
													<?php echo btn_edit('examschedule/edit/'.$examschedule->examscheduleID."/".$set, $this->lang->line('edit')) ?>
													<?php echo btn_delete('examschedule/delete/'.$examschedule->examscheduleID."/".$set, $this->lang->line('delete')) ?>
												</td>
												<?php } ?>
											</tr>
										<?php $i++; }} ?>
									</tbody>
								</table>
							</div>    

						</div>
					</div>
				</div> <!-- nav-tabs-custom -->
			</div>
		<?php } ?>


		<?php } elseif($usertype == "Student" || $usertype == "Parent") { ?>
		<div id="hide-table">
			<table id="example1" class="table table-striped table-bordered table-hover dataTable no-footer">
				<thead>
					<tr>
						<th><?=$this->lang->line('slno')?></th>
						<th><?=$this->lang->line('examschedule_name')?></th>
						<th><?=$this->lang->line('examschedule_classes')?></th>
						<th><?=$this->lang->line('examschedule_subject')?></th>
						<th><?=$this->lang->line('examschedule_date')?></th>
						<th><?=$this->lang->line('examschedule_time')?></th>
						<th><?=$this->lang->line('examschedule_room')?></th>
					</tr>
				</thead>
				<tbody>
					<?php if(count($examschedules)) {$i = 1; foreach($examschedules as $examschedule) { ?>
						<tr>
							<td data-title="<?=$this->lang->line('slno')?>">
								<?php echo $i; ?>
							</td>
							<td data-title="<?=$this->lang->line('examschedule_name')?>">
								<?php echo $examschedule->exam; ?>
							</td>
							<td data-title="<?=$this->lang->line('examschedule_classes')?>">
								<?php echo $examschedule->classes; ?>
							</td>
							<td data-title="<?=$this->lang->line('examschedule_subject')?>">
								<?php echo $examschedule->subject; ?>
							</td>

							<td data-title="<?=$this->lang->line('examschedule_time')?>">
								<?php echo date("d M Y", strtotime($examschedule->edate)); ?>
							</td>

							<td data-title="<?=$this->lang->line('examschedule_time')?>">
								<?php echo $examschedule->examfrom, " - ", $examschedule->examto ; ?>
							</td>

							<td data-title="<?=$this->lang->line('examschedule_room')?>">
								<?php echo $examschedule->room; ?>
							</td>
						</tr>
					<?php $i++; }} ?>
				</tbody>
			</table>
		</div>
		<?php } ?>
    </div>
</div>	

<script type="text/javascript">
    $('#classesID').change(function() {
        var classesID = $(this).val();
        if(classesID == 0) {
            $('#hide-table').hide();
            $('.nav-tabs-custom').hide();
        } else {
            $.ajax({
                type: 'POST',
                url: "<?=base_url('examschedule/examschedule_list')?>",
                data: "id=" + classesID,
                dataType: "html",
                success: function(data) {
                    window.location.href = data;
                }
            });
        }
    });
</script>
