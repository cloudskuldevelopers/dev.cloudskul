<div class="row">
	<div class="col-md-1 hidden-xs">
		<img src="<?php echo base_url('uploads/images/logo.png'); ?>" style="width: auto; height: 40px; margin-top: -8px;" class="img-thumbnail img-responsive" />
	</div>
	<div class="col-md-7 col-xs-9 text-center">
		<h1 class="page-header"><?php echo $this->session->userdata('school');  ?></h1>
	</div>
	<div class="col-md-4 col-xs-3">
		<!-- begin breadcrumb -->
		<ol class="breadcrumb pull-right">
			<li><a href="<?=base_url("dashboard/index")?>"><i class="fa fa-laptop"></i> <?=$this->lang->line('menu_dashboard')?></a></li>
            <li><a href="<?=base_url("examschedule/index")?>"><?=$this->lang->line('menu_examschedule')?></a></li>
            <li class="active"><?=$this->lang->line('menu_add')?> <?=$this->lang->line('panel_title')?></li>
		</ol>
		<!-- end breadcrumb -->
	</div>
</div>
<!-- begin panel -->
<div class="panel panel-inverse" data-sortable-id="ui-widget-3">
	<div class="panel-heading">
		<h4 class="panel-title"><i class="fa fa-puzzle-piece"></i> <?=$this->lang->line('panel_title')?></h4>
	</div>
	<div class="panel-body">
		<div class="col-sm-8">
			<form class="form-horizontal" role="form" method="post">
				<?php 
					if(form_error('examID')) 
						echo "<div class='form-group has-error' >";
					else     
						echo "<div class='form-group' >";
				?>
					<label for="examID" class="col-sm-2 control-label">
						<?=$this->lang->line("examschedule_name")?>
					</label>
					<div class="col-sm-6">
						<?php
							$array = array();
							$array[0] = $this->lang->line("examschedule_select_exam");
							foreach ($exams as $exam) {
								$array[$exam->examID] = $exam->exam;
							}
							echo form_dropdown("examID", $array, set_value("examID"), "id='examID' class='form-control'");
						?>
					</div>
					<span class="col-sm-4 control-label">
						<?php echo form_error('examID'); ?>
					</span>
				</div>

				<?php 
					if(form_error('classesID'))
						echo "<div class='form-group has-error' >";
					else     
						echo "<div class='form-group' >";
				?>
					<label for="classesID" class="col-sm-2 control-label">
						<?=$this->lang->line("examschedule_classes")?>
					</label>
					<div class="col-sm-6">
						<?php
							$array = array('0' => $this->lang->line("examschedule_select_classes"));
							foreach ($classes as $classa) {
								$array[$classa->classesID] = $classa->classes;
							}
							echo form_dropdown("classesID", $array, set_value("classesID"), "id='classesID' class='form-control'");
						?>
					</div>
					<span class="col-sm-4 control-label">
						<?php echo form_error('classesID'); ?>
					</span>
				</div>

				<?php 
					if(form_error('sectionID')) 
						echo "<div class='form-group has-error' >";
					else     
						echo "<div class='form-group' >";
				?>
					<label for="sectionID" class="col-sm-2 control-label">
						<?=$this->lang->line("examschedule_section")?>
					</label>
					<div class="col-sm-6">
						<?php
						$array = array('0' => $this->lang->line("examschedule_select_section"));
						if($sections != "empty") {
							foreach ($sections as $section) {
								$array[$section->sectionID] = $section->section;
							}
						}

						$secID = 0;
						if($sectionID == 0) {
							$secID = 0;
						} else {
							$secID = $sectionID;
						}
						
						echo form_dropdown("sectionID", $array, set_value("sectionID", $secID), "id='sectionID' class='form-control'");
						?>
					</div>
					<span class="col-sm-4 control-label">
						<?php echo form_error('sectionID'); ?>
					</span>
				</div>

				<?php 
					if(form_error('subjectID')) 
						echo "<div class='form-group has-error' >";
					else     
						echo "<div class='form-group' >";
				?>
					<label for="subjectID" class="col-sm-2 control-label">
						<?=$this->lang->line("examschedule_subject")?>
					</label>
					<div class="col-sm-6">
						<?php
						$array = array('0' => $this->lang->line("examschedule_select_subject"));
						if($subjects != "empty") {
							foreach ($subjects as $subject) {
								$array[$subject->subjectID] = $subject->subject;
							}
						}

						$sID = 0;
						if($subjectID == 0) {
							$sID = 0;
						} else {
							$sID = $subjectID;
						}
						
						echo form_dropdown("subjectID", $array, set_value("subjectID", $sID), "id='subjectID' class='form-control'");
						?>
					</div>
					<span class="col-sm-4 control-label">
						<?php echo form_error('subjectID'); ?>
					</span>
				</div>

				<?php 
					if(form_error('date')) 
						echo "<div class='form-group has-error' >";
					else     
						echo "<div class='form-group' >";
				?>
					<label for="date" class="col-sm-2 control-label">
						<?=$this->lang->line("examschedule_date")?>
					</label>
					<div class="col-sm-6">
						<input type="text" class="form-control" id="date" name="date" value="<?=set_value('date')?>" >
					</div>
					<span class="col-sm-4 control-label">
						<?php echo form_error('date'); ?>
					</span>
				</div>

				<?php 
					if(form_error('examfrom')) 
						echo "<div class='form-group has-error' >";
					else     
						echo "<div class='form-group' >";
				?>
					<label for="examfrom" class="col-sm-2 control-label">
						<?=$this->lang->line("examschedule_examfrom")?>
					</label>
					<div class="col-sm-6">
						<input type="text" class="form-control" id="examfrom" name="examfrom" value="<?=set_value('examfrom')?>" >
					</div>
					<span class="col-sm-4 control-label">
						<?php echo form_error('examfrom'); ?>
					</span>
				</div>

				<?php 
					if(form_error('examto')) 
						echo "<div class='form-group has-error'>";
					else     
						echo "<div class='form-group'>";
				?>
					<label for="examto" class="col-sm-2 control-label">
						<?=$this->lang->line("examschedule_examto")?>
					</label>
					<div class="col-sm-6">
						<input type="text" class="form-control" id="examto" name="examto" value="<?=set_value('examto')?>" >
					</div>
					<span class="col-sm-4 control-label">
						<?php echo form_error('examto'); ?>
					</span>
				</div>

				<?php 
					if(form_error('room')) 
						echo "<div class='form-group has-error'>";
					else     
						echo "<div class='form-group'>";
				?>
					<label for="room" class="col-sm-2 control-label">
						<?=$this->lang->line("examschedule_room")?>
					</label>
					<div class="col-sm-6">
						<input type="text" class="form-control" id="room" name="room" value="<?=set_value('room')?>" >
					</div>
					<span class="col-sm-4 control-label">
						<?php echo form_error('room'); ?>
					</span>
				</div>

				<div class="form-group">
					<div class="col-sm-offset-2 col-sm-8">
						<input type="submit" class="btn btn-success" value="<?=$this->lang->line("add_examschedule")?>" >
					</div>
				</div>
			</form>
		</div>
	</div>
</div>


<script type="text/javascript">
$('#classesID').change(function(event) {
    var classesID = $(this).val();
    if(classesID === '0') {
        $('#subjectID').val(0);
        $('#sectionID').val(0);
    } else {
        $.ajax({
            type: 'POST',
            url: "<?=base_url('examschedule/subjectcall')?>",
            data: "id=" + classesID,
            dataType: "html",
            success: function(data) {
               $('#subjectID').html(data);
            }
        });

        $.ajax({
            type: 'POST',
            url: "<?=base_url('examschedule/sectioncall')?>",
            data: "id=" + classesID,
            dataType: "html",
            success: function(data) {
               $('#sectionID').html(data);
            }
        });
    }
});

$('#date').datepicker();
$('#examfrom').timepicker();
$('#examto').timepicker();

</script>