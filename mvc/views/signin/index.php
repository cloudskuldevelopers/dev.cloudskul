<style type="text/css">
	.form-control{
		border:1px solid #999;
	}
	.f15{
		font-size:15px;
	}
</style>



<div id="page-loader" class="fade in"><span class="spinner"></span></div>
	<div id="page-container" class="fade">
        <div class="login login-with-news-feed">
            <!-- begin news-feed -->
            <div class="news-feed">
                <div class="news-image">
                    <img src="<?php echo base_url('assets/theme/img/login-bg/bg-7.jpg'); ?>" data-id="login-cover-image" alt="" />
                </div>
                <div class="news-caption">
                    <h4 class="caption-title"><i class="fa fa-laptop text-success"></i> Introducing the CloudSkul app 
						<a href="<?php echo base_url(); ?>" class="btn btn-success btn-lg btn-offset" style="right: 40px; position: absolute; bottom: 40px; line-height: 33px; border-left:5px solid #046363; font-size: 30px; border-radius:0px;">
							<i class="fa fa-home"></i>
						</a>
					</h4> 
                    <p>
                        An application designed to meet the educational demands of the 21st century. Easy to use.
                    </p>
                </div>
            </div>
            <!-- end news-feed -->
            <!-- begin right-content -->
            <div class="right-content">
                <!-- begin login-header -->
                <div class="login-header">
                    <div class="brand">
                        <span style="color:#00ACAC;"><i class="fa fa-lock"></i></span> <span style="color:rgba(36, 42, 48, 0.76);">Sign In </span>
						<small>Welcome | Log in to your account</small>
                    </div>
                    <div class="icon" style="top:146px; z-index:-1">
                        <img src="<?php echo base_url('uploads/images/logo.png'); ?>" />
                    </div>
                </div>
                <!-- end login-header -->
                <!-- begin login-content -->
                <div class="login-content">
                    <form method="POST" class="margin-bottom-0">
						<?php 
							if($form_validation == "No"){
							} else {
								if(count($form_validation)) {
									echo "<div class=\"alert alert-danger alert-dismissable\">
										<i class=\"fa fa-ban\"></i>
										<button aria-hidden=\"true\" data-dismiss=\"alert\" class=\"close\" type=\"button\">×</button>
										$form_validation
									</div>";
								}
							}
							if($this->session->flashdata('reset_success')) {
								$message = $this->session->flashdata('reset_success');
								echo "<div class=\"alert alert-success alert-dismissable\">
									<i class=\"fa fa-ban\"></i>
									<button aria-hidden=\"true\" data-dismiss=\"alert\" class=\"close\" type=\"button\">×</button>
									$message
								</div>";
							}
						?>
                        <div class="form-group m-b-15">
                            <input type="text" class="form-control input-lg" name="username" placeholder="Username" autofocus value="<?=set_value('username')?>" style="background:transparent;" />
                        </div>
                        <div class="form-group m-b-15">
                            <input type="password" class="form-control input-lg" name="password" placeholder="Password" style="background:transparent;" />
                        </div>
                        <div class="checkbox checkbox-success m-b-30 f15">
                            <label>
                                <input  id="remember" type="checkbox" value="Remember Me" name="remember" /> Remember Me
                            </label>
                        </div>
                        <div class="login-buttons">
                            <button type="submit" class="btn btn-success btn-block btn-lg"><i class="fa fa-angle-double-right"></i><i class="fa fa-angle-double-right"></i> &nbsp; SIGN ME IN</button>
                        </div>
                        <div class="m-t-20 m-b-40 p-b-40 f15">
                            Not Registered? Click <a href="<?=base_url('register/index')?>" class="text-success">here</a> to register.
                        </div>
                        <hr />
                        <p class="text-center text-inverse">
                            &copy; Cloudskul All Right Reserved <?=date("Y")?>
                        </p>
                    </form>
                </div>
                <!-- end login-content -->
            </div>
            <!-- end right-container -->
        </div>
        <!-- end login -->
	</div>