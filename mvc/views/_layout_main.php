<?php $this->load->view("components/page_header"); ?>
<?php $this->load->view("components/page_topbar"); ?>
<?php $this->load->view("components/page_menu"); ?>

     <div class="content" id="content">
		<div class="add-min-height">
			<?php $this->load->view($subview); ?>
		</div>
		
		
		<!--footer for pages starts here-->
		<footer class="main-footer">
			<div class="pull-right hidden-xs">
			<p style="float: left; margin-right: 5px;">Version 1.0.0</p>
			  <img src="<?php echo base_url('uploads/images/logo.png'); ?>" style="width: auto; height: 34px; float: right; margin-top: -8px;" class="img-thumbnail img-responsive" />
			</div>
			<strong>Copyright &copy; <?=Date('Y')?> <a href="http://cloudskul.com" style="color: #428bca;">CloudSkul.</a></strong> &nbsp; All rights reserved.
		</footer>
		
		<div class="modal fade verifyphonePopup">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
						<h4 class="modal-title">Debit Card Payment</h4>
					</div>
					<div class="modal-body">
					  <div id="rules">
					  <p>Hi Emmanuel,</p><br>

					  <p>In order to determine that all SMS sent from Cloudskul is delivered, we need you to verify your phone number</p><br>

					  <!--div class="col-sm-6"-->
						  <div class="input-group">
							<input type="text" class="form-control" id="phone" name="phone" placeholder="080xxxxxxxx" value="<?=set_value('phone', $user->phone)?>" >
							<span class="input-group-btn">
							  <button id="verify" onclick="return false;" class="btn btn-success">
								  verify
							  </button>
							  <button id="verify1" style="display:none;" onclick="return false;" class="btn btn-success">
							  Send again
							  </button>
							<button id="verify3" style="display:none;" onclick="return false;" class="btn btn-success">
							  verified
							  </button>
							</span>
						  </div><!-- /.input group ->
					  </div-->
					
					  <br>
					  <p>Thank you.</p>
					  </div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
						<a id="okk" href="#"><button type="button" data-dismiss="modal" class="btn btn-default">OK</button> </a>
					</div>
				</div>
				<!-- /.modal-content -->
			</div>
			<!-- /.modal-dialog -->
		</div>
		<!--footer ends here-->
		
	 </div>
	 
<?php $this->load->view("components/page_footer"); ?>
