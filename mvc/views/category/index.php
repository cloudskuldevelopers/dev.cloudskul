<div class="row">
	<div class="col-md-2 hidden-xs">
		<img src="<?php echo base_url('uploads/images/logo.png'); ?>" style="width: auto; height: 40px; margin-top: -8px;" class="img-thumbnail img-responsive" />
	</div>
	<div class="col-md-7 col-xs-9 text-center">
		<h1 class="page-header"><?php echo $this->session->userdata('school');  ?></h1>
	</div>
	<div class="col-md-3 col-xs-3">
		<!-- begin breadcrumb -->
		<ol class="breadcrumb pull-right">
			<li><a href="<?=base_url("dashboard/index")?>"><i class="fa fa-laptop"></i> <?=$this->lang->line('menu_dashboard')?></a></li>
            <li class="active"><?=$this->lang->line('menu_category')?></li>
		</ol>
		<!-- end breadcrumb -->
	</div>
</div>

<div id="page-container">
<!-- begin panel -->
<div class="panel panel-inverse" data-sortable-id="ui-widget-3">
	<div class="panel-heading">
		<?php
			$usertype = $this->session->userdata("usertype");
			if($usertype == "Admin" || $usertype == "Super Admin") { 
		?>
		<div class="btn-group-vertical pull-right">
			<a href="<?php echo base_url('category/add') ?>" type="button" class="btn btn-success btn-xs"><i class="fa fa-plus"></i> <?=$this->lang->line('add_title')?></a>
		</div>
			<?php } ?>
		<h4 class="panel-title"><i class="fa fa-desktop"></i> <?=$this->lang->line('panel_title')?></h4>
	</div>
	<div class="panel-body">
		<?php
			$usertype = $this->session->userdata("usertype");
			if($usertype == "Admin" || $usertype == "Super Admin") { 
		?>
		<div id="hide-table">
			<table id="example1" class="table table-striped table-bordered table-hover dataTable no-footer">
				<thead>
					<tr>
						<th class="col-sm-2"><?=$this->lang->line('slno')?></th>
						<th class="col-sm-2"><?=$this->lang->line('category_hname')?></th>
						<th class="col-sm-2"><?=$this->lang->line('category_class_type')?></th>
						<th class="col-sm-2"><?=$this->lang->line('category_hbalance')?></th>
						<th class="col-sm-2"><?=$this->lang->line('category_note')?></th>
						<th class="col-sm-2"><?=$this->lang->line('action')?></th>
					</tr>
				</thead>
				<tbody>
					<?php if(count($categorys)) {$i = 1; foreach($categorys as $category) { ?>
						<tr>
							<td data-title="<?=$this->lang->line('slno')?>">
								<?php echo $i; ?>
							</td>
							<td data-title="<?=$this->lang->line('category_hname')?>">
								<?php echo $category->name; ?>
							</td>
							<td data-title="<?=$this->lang->line('category_class_type')?>">
								<?php echo $category->class_type; ?>
							</td>
							<td data-title="<?=$this->lang->line('category_hbalance')?>">
								<?php echo $category->hbalance; ?>
							</td>
							<td data-title="<?=$this->lang->line('category_note')?>">
								<?php echo $category->note; ?>
							</td>

							<td data-title="<?=$this->lang->line('action')?>">
								<?php echo btn_edit('category/edit/'.$category->categoryID, $this->lang->line('edit')) ?>
								<?php echo btn_delete('category/delete/'.$category->categoryID, $this->lang->line('delete')) ?>
							</td>
						</tr>
					<?php $i++; }} ?>
				</tbody>
			</table>
		</div>
		<?php } else { ?>

		<div id="hide-table">
			<table id="example1" class="table table-striped table-bordered table-hover dataTable no-footer">
				<thead>
					<tr>
					   <th><?=$this->lang->line('slno')?></th>
						<th><?=$this->lang->line('category_hname')?></th>
						<th><?=$this->lang->line('category_class_type')?></th>
						<th><?=$this->lang->line('category_hbalance')?></th>
						<th><?=$this->lang->line('category_note')?></th>
					</tr>
				</thead>
				<tbody>
					<?php if(count($categorys)) {$i = 1; foreach($categorys as $category) { ?>
						<tr>
						   <td class="col-lg-1" data-title="<?=$this->lang->line('slno')?>">
								<?php echo $i; ?>
							</td>
							<td class="col-lg-4" data-title="<?=$this->lang->line('category_hname')?>">
								<?php echo $category->name; ?>
							</td>
							<td class="col-lg-2" data-title="<?=$this->lang->line('category_class_type')?>">
								<?php echo $category->class_type; ?>
							</td>
							<td class="col-lg-2" data-title="<?=$this->lang->line('category_hbalance')?>">
								<?php echo $category->hbalance; ?>
							</td>
							<td class="col-lg-3" data-title="<?=$this->lang->line('category_note')?>">
								<?php echo $category->note; ?>
							</td>
						</tr>
					<?php $i++; }} ?>
				</tbody>
			</table>
		</div>
		<?php } ?>
	</div>
</div>
</div>

