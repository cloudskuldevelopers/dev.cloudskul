<?php echo doctype("html5"); ?>
<html class="white-bg-login" lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <title>Sign in ::: CloudSkul</title>

	<!-- ================== BEGIN BASE CSS STYLE ================== -->
	<link href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet">
	<link href="<?php echo base_url('assets/theme/plugins/jquery-ui/themes/base/minified/jquery-ui.min.css'); ?>" rel="stylesheet" />
	<link href="<?php echo base_url('assets/theme/plugins/bootstrap/css/bootstrap.min.css'); ?>" rel="stylesheet" />
	<link href="<?php echo base_url('assets/theme/plugins/font-awesome/css/font-awesome.min.css'); ?>" rel="stylesheet" />
	<link href="<?php echo base_url('assets/theme/css/animate.min.css'); ?>" rel="stylesheet" />
	<link href="<?php echo base_url('assets/theme/css/style.min.css'); ?>" rel="stylesheet" />
	<link href="<?php echo base_url('assets/theme/css/style-responsive.min.css'); ?>" rel="stylesheet" />
	<link href="<?php echo base_url('assets/theme/css/theme/default.css'); ?>" rel="stylesheet" id="theme" />
	<!-- ================== END BASE CSS STYLE ================== -->
	
	<!-- ================== BEGIN PAGE LEVEL CSS STYLE ================== -->
    <link href="<?php echo base_url('assets/theme/plugins/jquery-jvectormap/jquery-jvectormap-1.2.2.css'); ?>" rel="stylesheet" />
    <link href="<?php echo base_url('assets/theme/plugins/bootstrap-calendar/css/bootstrap_calendar.css'); ?>" rel="stylesheet" />
    <link href="<?php echo base_url('assets/theme/plugins/gritter/css/jquery.gritter.css'); ?>" rel="stylesheet" />
    <link href="<?php echo base_url('assets/theme/plugins/morris/morris.css'); ?>" rel="stylesheet" />
	<!-- ================== END PAGE LEVEL CSS STYLE ================== -->
	
	<!-- ================== BEGIN BASE JS ================== -->
	<script src="<?php echo base_url('assets/theme/plugins/pace/pace.min.js'); ?>"></script>
	<!-- ================== END BASE JS ================== -->
	
</head>

<body class="pace-top bg-white">

    <?php $this->load->view($subview); ?>
	
	<!-- ================== BEGIN BASE JS ================== -->
	<script src="<?php echo base_url('assets/theme/plugins/jquery/jquery-1.9.1.min.js'); ?>"></script>
	<script src="<?php echo base_url('assets/theme/plugins/jquery/jquery-migrate-1.1.0.min.js'); ?>"></script>
	<script src="<?php echo base_url('assets/theme/plugins/jquery-ui/ui/minified/jquery-ui.min.js'); ?>"></script>
	<script src="<?php echo base_url('assets/theme/plugins/bootstrap/js/bootstrap.min.js'); ?>"></script>
	<!--[if lt IE 9]>
		<script src="assets/crossbrowserjs/html5shiv.js"></script>
		<script src="assets/crossbrowserjs/respond.min.js"></script>
		<script src="assets/crossbrowserjs/excanvas.min.js"></script>
	<![endif]-->
	<script src="<?php echo base_url('assets/theme/plugins/slimscroll/jquery.slimscroll.min.js'); ?>"></script>
	<script src="<?php echo base_url('assets/theme/plugins/jquery-cookie/jquery.cookie.js'); ?>"></script>
	<!-- ================== END BASE JS ================== -->
	
	<!-- ================== BEGIN PAGE LEVEL JS ================== -->
    <script src="<?php echo base_url('assets/theme/plugins/morris/raphael.min.js'); ?>"></script>
    <script src="<?php echo base_url('assets/theme/plugins/morris/morris.js'); ?>"></script>
    <script src="<?php echo base_url('assets/theme/plugins/jquery-jvectormap/jquery-jvectormap-1.2.2.min.js'); ?>"></script>
    <script src="<?php echo base_url('assets/theme/plugins/jquery-jvectormap/jquery-jvectormap-world-merc-en.js'); ?>"></script>
    <script src="<?php echo base_url('assets/theme/plugins/bootstrap-calendar/js/bootstrap_calendar.min.js'); ?>"></script>
	<script src="<?php echo base_url('assets/theme/plugins/gritter/js/jquery.gritter.js'); ?>"></script>
	<script src="<?php echo base_url('assets/theme/js/dashboard-v2.min.js'); ?>"></script>
	<script src="<?php echo base_url('assets/theme/js/apps.min.js'); ?>"></script>
	<!-- ================== END PAGE LEVEL JS ================== -->
	
	<script>
		$(document).ready(function() {
			App.init();
			DashboardV2.init();
		});
	</script>
</body>
</html>