<div class="row">
	<div class="col-md-2 hidden-xs">
		<img src="<?php echo base_url('uploads/images/logo.png'); ?>" style="width: auto; height: 40px; margin-top: -8px;" class="img-thumbnail img-responsive" />
	</div>
	<div class="col-md-7 col-xs-9 text-center">
		<h1 class="page-header"><?php echo $this->session->userdata('school');  ?></h1>
	</div>
	<div class="col-md-3 col-xs-3">
		<!-- begin breadcrumb -->
		<ol class="breadcrumb pull-right">
			<li ><a href="<?php echo base_url('dashboard/index');?>"><i class="fa fa-laptop"></i><?=$this->lang->line('menu_dashboard')?></a></li>
			<li class="active"><?=$this->lang->line('menu_promotion')?></a></li>
		</ol>
		<!-- end breadcrumb -->
	</div>
</div>
<!-- begin panel -->
<div class="panel panel-inverse" data-sortable-id="ui-widget-3">
	<div class="panel-heading">
		<h4 class="panel-title"><i class="fa fa-arrow-circle-up"></i> <?=$this->lang->line('panel_title')?></h4>
	</div>
	<div class="panel-body">
		<div class="col-sm-12">
			
			<div class="col-md-6 col-sm-offset-3 add_padding m-b-10">
				<form style="" class="form-horizontal" role="form" method="post">
					<div class="input-group">
						<span class="input-group-addon offset_addon"><i class="fa fa-sitemap"></i></span>
						 <?php
								$array = array("0" => $this->lang->line("promotion_select_class"));
								foreach ($classes as $classa) {
									$array[$classa->classesID] = $classa->classes;
								}
								echo form_dropdown("classesID", $array, set_value("classesID",$set), "id='classesID' class='form-control offset_height'");
							?>
					</div>
				</form>
			</div>
			<div class="col-sm-6 col-sm-offset-3 list-group">
				<div class="list-group-item">
					<form class="form-horizontal" role="form" method="post" enctype="multipart/form-data">
						<?php 
							$mark=array();
							$promotionsID = array(); 
							foreach ($promotionsubjects as $key => $promotionsubject) {
								$mark[$promotionsubject->subjectID] = $promotionsubject->subjectMark;
								$promotionsID[$promotionsubject->subjectID] = $promotionsubject->promotionSubjectID;

							}
							
							if(count($promotionsID)) {
								foreach ($subjects as $key => $subject) {    
						?>
							<div class="form-group">
								<label for="<?php if(isset($promotionsID[$subject->subjectID])) echo $promotionsID[$subject->subjectID]; else echo "0";?>" class="col-sm-2 col-sm-offset-2 control-label">
									<?=$subject->subject?>
								</label>
								<div class="col-sm-6">
									<input type="text" class="form-control" id="<?php if(isset($promotionsID[$subject->subjectID])) echo $promotionsID[$subject->subjectID]; else echo "0";?>" name="<?php if(isset($promotionsID[$subject->subjectID])) echo $promotionsID[$subject->subjectID]; else echo "0";?>" value="<?php if(isset($promotionsID[$subject->subjectID])) echo set_value($subject->subjectID, $mark[$subject->subjectID])?>" >
								</div>
								<span class="col-sm-4 col-sm-offset-4 control-label">
									<?php if(isset($promotionsID[$subject->subjectID])) echo form_error($promotionsID[$subject->subjectID]); ?>
								</span>

							</div>
						<?php } } ?>

						<div class="form-group">
							<div class="col-sm-offset-4 col-sm-4">
								<input type="submit" class="btn btn-success" value="<?=$this->lang->line("add_mark_setting")?>" >
							</div>
						</div>

					</form>
				<div>
			</div>
		</div> <!-- col-sm-12 -->
	</div>
</div>

<script type="text/javascript">
    $('#classesID').change(function() {
        var classesID = $(this).val();
        if(classesID == 0) {
            $('#hide-table').hide();
        } else {
            $.ajax({
                type: 'POST',
                url: "<?=base_url('promotion/promotion_list')?>",
                data: "id=" + classesID,
                dataType: "html",
                success: function(data) {
                    window.location.href = data;
                }
            });
        }
    });
</script>