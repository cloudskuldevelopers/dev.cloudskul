<div class="row">
	<div class="col-md-2 hidden-xs">
		<img src="<?php echo base_url('uploads/images/logo.png'); ?>" style="width: auto; height: 40px; margin-top: -8px;" class="img-thumbnail img-responsive" />
	</div>
	<div class="col-md-7 col-xs-9 text-center">
		<h1 class="page-header"><?php echo $this->session->userdata('school');  ?></h1>
	</div>
	<div class="col-md-3 col-xs-3">
		<!-- begin breadcrumb -->
		<ol class="breadcrumb pull-right">
			<li ><a href="<?php echo base_url('dashboard/index');?>"><i class="fa fa-laptop"></i> Dashboard</a></li>
			<li class="active"><?=$this->lang->line('menu_books')?></li>
		</ol>
		<!-- end breadcrumb -->
		
	</div>
</div>
<div class="panel panel-inverse" data-sortable-id="ui-widget-3">
	<div class="panel-heading">
		<?php 
			$usertype = $this->session->userdata("usertype");
			if($usertype == "Super Admin" || $usertype == "Admin") {
		?>
		<div class="btn-group-vertical pull-right">
			<a href="<?php echo base_url('book/add') ?>" type="button" class="btn btn-success btn-xs"><i class="fa fa-plus"></i> <?=$this->lang->line('add_title')?></a>
		</div>
		<?php } ?>
		<h4 class="panel-title"><i class="fa fa-pencil"></i> <?=$this->lang->line('panel_title')?></h4>
	</div>
	<div class="panel-body">
	
		<!--<div class="col-sm-12" style="border-bottom:1px solid #F0F3F5; margin-bottom:10px;"></div>-->

<div class="box box-danger">
        <div class="box-body">
        <div class="row">
            <div class="col-sm-12">

                <div id="hide-table">
                    <table id="example1" class="table table-striped table-bordered table-hover dataTable no-footer">
                        <thead>
                            <tr>
                                <th class="col-sm-1"><?=$this->lang->line('slno')?></th>
                                <th class="col-sm-2"><?=$this->lang->line('book_name')?></th>
                                <th class="col-sm-2"><?=$this->lang->line('book_author')?></th>
                                <th class="col-sm-2"><?=$this->lang->line('book_subject_code')?></th>
                                <?php if($usertype == "Admin" || $usertype == "Super Admin" || $usertype == "Librarian") { ?>
                                <th class="col-sm-1"><?=$this->lang->line('book_price')?></th>
                                <?php } ?>
                                <?php if($usertype == "Admin" || $usertype == "Super Admin" || $usertype == "Librarian") { ?>
                                <th class="col-sm-1"><?=$this->lang->line('book_quantity')?></th>
                                <?php } ?>
                                <th class="col-sm-1"><?=$this->lang->line('book_rack_no')?></th>
                                <th class="col-sm-1"><?=$this->lang->line('book_status')?></th>
                                <?php if($usertype == "Admin" || $usertype == "Super Admin") { ?>
                                <th class="col-sm-1"><?=$this->lang->line('action')?></th>
                                <?php } ?>
                            </tr>
                        </thead>
                        <tbody>
                            <?php if(count($books)) {$i = 1; foreach($books as $book) { ?>
                                <tr>
                                    <td data-title="<?=$this->lang->line('slno')?>">
                                        <?php echo $i; ?>
                                    </td>

                                    <td data-title="<?=$this->lang->line('book_name')?>">
                                        <?php echo $book->book; ?>
                                    </td>

                                    <td data-title="<?=$this->lang->line('book_author')?>">
                                        <?php echo $book->author; ?>
                                    </td>

                                    <td data-title="<?=$this->lang->line('book_subject_code')?>">
                                        <?php echo $book->subject_code; ?>
                                    </td>
                                    
                                    <?php if($usertype == "Admin" || $usertype == "Super Admin" || $usertype == "Librarian") { ?>
                                    <td data-title="<?=$this->lang->line('book_price')?>">
                                        <?php echo $book->price; ?>
                                    </td>
                                    <?php } ?>

                                    <?php if($usertype == "Admin" || $usertype == "Super Admin" || $usertype == "Librarian") { ?>
                                    <td data-title="<?=$this->lang->line('book_quantity')?>">
                                        <?php echo $book->quantity; ?>
                                    </td>
                                    <?php } ?>


                                    <td data-title="<?=$this->lang->line('book_rack_no')?>">
                                        <?php echo $book->rack; ?>
                                    </td>

                                    <td data-title="<?=$this->lang->line('book_status')?>">
                                        <?php 
                                            if($book->quantity == $book->due_quantity) {
                                                echo "<button class='btn btn-danger btn-xs'>" . $this->lang->line('book_unavailable') . "</button>";
                                            } else {
                                                echo "<button class='btn btn-success btn-xs'>" . $this->lang->line('book_available') . "</button>";
                                            }
                                        ?>
                                    </td>

                                    <?php if($usertype == "Admin" || $usertype == "Super Admin") { ?>
                                    <td class="col-md-2" data-title="<?=$this->lang->line('action')?>">
                                        <?php echo btn_edit('book/edit/'.$book->bookID, $this->lang->line('edit')) ?>
                                        <?php echo btn_delete('book/delete/'.$book->bookID, $this->lang->line('delete')) ?>
                                    </td>
                                    <?php } ?>
                                </tr>
                            <?php $i++; }} ?>
                        </tbody>
                    </table>
                </div>

            </div>
        </div>
    </div>
</div>
