<div class="row">
	<div class="col-md-2 hidden-xs">
		<img src="<?php echo base_url('uploads/images/logo.png'); ?>" style="width: auto; height: 40px; margin-top: -8px;" class="img-thumbnail img-responsive" />
	</div>
	<div class="col-md-7 col-xs-9 text-center">
		<h1 class="page-header"><?php echo $this->session->userdata('school');  ?></h1>
	</div>
	<div class="col-md-3 col-xs-3">
		<!-- begin breadcrumb -->
		<ol class="breadcrumb pull-right">
			<li><a href="<?=base_url("dashboard/index")?>"><i class="fa fa-laptop"></i> <?=$this->lang->line('menu_dashboard')?></a></li>
            <li><a href="<?=base_url("book/index")?>"><?=$this->lang->line('menu_books')?></a></li>
            <li class="active"><?=$this->lang->line('menu_edit')?> <?=$this->lang->line('menu_books')?></li>
		</ol>
		<!-- end breadcrumb -->
		
	</div>
</div>
<div class="panel panel-inverse" data-sortable-id="ui-widget-3">
	<div class="panel-heading">
		<?php 
			$usertype = $this->session->userdata("usertype");
			if($usertype == "Super Admin" || $usertype == "Admin") {
		?>
		<div class="btn-group-vertical pull-right">
			<a href="<?php echo base_url('book/add') ?>" type="button" class="btn btn-success btn-xs"><i class="fa fa-plus"></i> <?=$this->lang->line('add_title')?></a>
		</div>
		<?php } ?>
		<h4 class="panel-title"><i class="fa fa-pencil"></i> <?=$this->lang->line('panel_title')?></h4>
	</div>
	<div class="panel-body">
	
		<!--<div class="col-sm-12" style="border-bottom:1px solid #F0F3F5; margin-bottom:10px;"></div>-->


<div class="box">
       <!-- form start -->
    <div class="box-body">
        <div class="row">
            <div class="col-sm-8">

                <form class="form-horizontal" role="form" method="post">
                   <?php 
                        if(form_error('book')) 
                            echo "<div class='form-group has-error' >";
                        else     
                            echo "<div class='form-group' >";
                    ?>
                        <label for="book" class="col-sm-2 control-label">
                            <?=$this->lang->line("book_name")?>
                        </label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" id="book" name="book" value="<?=set_value('book', $book->book)?>" >
                        </div>
                        <span class="col-sm-4 control-label">
                            <?php echo form_error('book'); ?>
                        </span>
                    </div>

                    <?php 
                        if(form_error('author')) 
                            echo "<div class='form-group has-error' >";
                        else     
                            echo "<div class='form-group' >";
                    ?>
                        <label for="author" class="col-sm-2 control-label">
                            <?=$this->lang->line("book_author")?>
                        </label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" id="author" name="author" value="<?=set_value('author', $book->author)?>" >
                        </div>
                        <span class="col-sm-4 control-label">
                            <?php echo form_error('author'); ?>
                        </span>
                    </div>

                    <?php 
                        if(form_error('subject_code')) 
                            echo "<div class='form-group has-error' >";
                        else     
                            echo "<div class='form-group' >";
                    ?>
                        <label for="subject_code" class="col-sm-2 control-label">
                            <?=$this->lang->line("book_subject_code")?>
                        </label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" id="subject_code" name="subject_code" value="<?=set_value('subject_code', $book->subject_code)?>" >
                        </div>
                        <span class="col-sm-4 control-label">
                            <?php echo form_error('subject_code'); ?>
                        </span>
                    </div>

                   

                    <?php 
                        if(form_error('price')) 
                            echo "<div class='form-group has-error' >";
                        else     
                            echo "<div class='form-group' >";
                    ?>
                        <label for="price" class="col-sm-2 control-label">
                            <?=$this->lang->line("book_price")?>
                        </label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" id="price" name="price" value="<?=set_value('price', $book->price)?>" >
                        </div>
                        <span class="col-sm-4 control-label">
                            <?php echo form_error('price'); ?>
                        </span>
                    </div>

                    <?php 
                        if(form_error('quantity')) 
                            echo "<div class='form-group has-error' >";
                        else     
                            echo "<div class='form-group' >";
                    ?>
                        <label for="quantity" class="col-sm-2 control-label">
                            <?=$this->lang->line("book_quantity")?>
                        </label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" id="quantity" name="quantity" value="<?=set_value('quantity', $book->quantity)?>" >
                        </div>
                        <span class="col-sm-4 control-label">
                            <?php echo form_error('quantity'); ?>
                        </span>
                    </div>

                    <?php 
                        if(form_error('rack')) 
                            echo "<div class='form-group has-error' >";
                        else     
                            echo "<div class='form-group' >";
                    ?>
                        <label for="rack" class="col-sm-2 control-label">
                            <?=$this->lang->line("book_rack_no")?>
                        </label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" id="rack" name="rack" value="<?=set_value('rack', $book->rack)?>" >
                        </div>
                        <span class="col-sm-4 control-label">
                            <?php echo form_error('rack'); ?>
                        </span>
                    </div>

                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-8">
                            <input type="submit" class="btn btn-success" value="<?=$this->lang->line("update_book")?>" >
                        </div>
                    </div>
                </form>

            </div>
        </div>
    </div>
</div>
