<!-- TOP BAR -->
<div class="row">
	<div class="col-md-1 hidden-xs">
		<img src="<?php echo base_url('uploads/images/logo.png'); ?>" style="width: auto; height: 40px; margin-top: -8px;" class="img-thumbnail img-responsive" />
	</div>
	<div class="col-md-6 col-xs-9 text-center">
		<h1 class="page-header"><?php echo $this->session->userdata('school');  ?></h1>
	</div>
	<div class="col-md-5 col-xs-3">
		<!-- begin breadcrumb -->
		<ol class="breadcrumb pull-right">
			<li><a href="<?=base_url("dashboard/index")?>"><i class="fa fa-laptop"></i> <?=$this->lang->line('menu_dashboard')?></a></li>
            <li><a href="<?php echo base_url().'employee/index'; ?>">Employee</a></li>
            <li class="active">Add Employee</li>
		</ol>
		<!-- end breadcrumb -->
	</div>
</div>
<!-- END TOP BAR -->
	
<!-- begin panel -->
<div class="panel panel-inverse" data-sortable-id="ui-widget-3">
	<div class="panel-heading">
		<h4 class="panel-title"><i class="fa fa-list-ol"></i> Add New Employee</h4>
	</div>
	<div class="panel-body">
		<div class="col-sm-12">
			<form class="form-horizontal" action="" role="form" method="post">
				<div class="col-md-6">
					<div class="box box-primary">
						<div class="box-header with-border">
							<h4 class="box-title">Personal Details</h4>
						</div>
						
						<div class="box-body">
							<?php 
								if(form_error('first_name')) 
									echo "<div class='form-group has-error' >";
								else     
									echo "<div class='form-group' >";
							?>
								<label for="first_name" class="col-sm-3 control-label">First Name <span class="required"> *</span></label>
								<div class="col-sm-9">
									<input type="text" name="first_name" class="form-control" placeholder="first name" value="<?=set_value('first_name')?>" />
									<p class="control-label col-md-12" style="text-align: left !important; padding-left: 0;"><?php echo form_error('first_name'); ?></p>
								</div>
							</div>
							
							<?php 
								if(form_error('last_name')) 
									echo "<div class='form-group has-error' >";
								else     
									echo "<div class='form-group' >";
							?>
								<label for="last_name" class="col-sm-3 control-label">Last Name <span class="required"> *</span></label>
								<div class="col-sm-9">
									<input type="text" name="last_name" class="form-control" placeholder="last name" value="<?=set_value('last_name')?>" />
									<p class="control-label col-md-12" style="text-align: left !important; padding-left: 0;"><?php echo form_error('last_name'); ?></p>
								</div>
							</div>
							
							<?php 
								if(form_error('date_of_birth')) 
									echo "<div class='form-group has-error' >";
								else     
									echo "<div class='form-group' >";
							?>
								<label for="date_of_birth" class="col-sm-3 control-label">Date of Birth <span class="required"> *</span></label>
								<div class="col-sm-9">
									<input type="date" name="date_of_birth" class="form-control" placeholder="date of birth" />
									<p class="control-label col-md-12" style="text-align: left !important; padding-left: 0;"><?php echo form_error('date_of_birth'); ?></p>
								</div>
							</div>
							
							<?php 
								if(form_error('gender')) 
									echo "<div class='form-group has-error' >";
								else     
									echo "<div class='form-group' >";
							?>
								<label for="gender" class="col-sm-3 control-label">Gender <span class="required"> *</span></label>
								<div class="col-sm-9">
									<select name="gender" class="form-control">
										<option value="none" selected="selected">Gender...</option>
										<option value="Male">Male</option>
										<option value="Female">Female</option>
									</select>
									<p class="control-label col-md-12" style="text-align: left !important; padding-left: 0;"><?php echo form_error('gender'); ?></p>
								</div>
							</div>
							
							<?php 
								if(form_error('marital_status')) 
									echo "<div class='form-group has-error' >";
								else     
									echo "<div class='form-group' >";
							?>
								<label for="marital_status" class="col-sm-3 control-label">Marital status <span class="required"> *</span></label>
								<div class="col-sm-9">
									<select name="marital_status" class="form-control" aria-required="true" aria-invalid="false">
										<option value="none" selected="selected">Maratal Status...</option>
										<option value="Married">Married</option>
										<option value="Un-Married">Un-Married</option>
										<option value="Widowed">Widowed</option>
										<option value="Divorced">Divorced</option>
									</select>
									<p class="control-label col-md-12" style="text-align: left !important; padding-left: 0;"><?php echo form_error('marital_status'); ?></p>
								</div>
							</div>
							
							<?php 
								if(form_error('father_name')) 
									echo "<div class='form-group has-error' >";
								else     
									echo "<div class='form-group' >";
							?>
								<label for="father_name" class="col-sm-3 control-label">Father's Name <span class="required"> *</span></label>
								<div class="col-sm-9">
									<input type="text" name="father_name" class="form-control" placeholder="father's name" value="<?=set_value('father_name')?>" />
									<p class="control-label col-md-12" style="text-align: left !important; padding-left: 0;"><?php echo form_error('father_name'); ?></p>
								</div>
							</div>
							
							<?php 
								if(form_error('nationality')) 
									echo "<div class='form-group has-error' >";
								else     
									echo "<div class='form-group' >";
							?>
								<label for="nationality" class="col-sm-3 control-label">Nationality <span class="required"> *</span></label>
								<div class="col-sm-9">
									<input type="text" name="nationality" class="form-control" placeholder="nationality" value="<?=set_value('nationality')?>" />
									<p class="control-label col-md-12" style="text-align: left !important; padding-left: 0;"><?php echo form_error('nationality'); ?></p>
								</div>
							</div>
							
							<div class='form-group'>
								<label for="passport_no" class="col-sm-3 control-label">Passport No <span class="required"> *</span></label>
								<div class="col-sm-9">
									<input type="text" name="passport_no" class="form-control" placeholder="passport no" value="<?=set_value('passport_no')?>" />
									<p class="control-label col-md-12" style="text-align: left !important; padding-left: 0;"></p>
								</div>
							</div>
							
							<?php 
								if(isset($image)) 
									echo "<div class='form-group has-error' >";
								else     
									echo "<div class='form-group' >";
							?>
								<label for="photo" class="col-sm-2 control-label col-xs-8 col-md-3">
									Photo
								</label>
								<div class="col-sm-4 col-xs-6 col-md-5">
									<input class="form-control"  id="uploadFile" placeholder="Choose File" disabled />  
								</div>

								<div class="col-sm-2 col-xs-6 col-md-4">
									<div class="fileUpload btn btn-success form-control">
										<span class="fa fa-repeat"></span>
										<span><?=$this->lang->line("upload")?></span>
										<input id="uploadBtn" type="file" class="upload" name="image" />
									</div>
								</div>
								 <span class="col-md-12">
								   
									<?php if(isset($image)) echo $image; ?>
								</span>
							</div>
				
				
						</div>
					</div>
				</div>
				<!--End Personal Details -->
				
				<div class="col-md-6">
					<div class="box box-primary">
						<div class="box-header with-border">
							<h4 class="box-title">Contact Details</h4>
						</div>
						
						<div class="box-body">
							<?php 
								if(form_error('present_address')) 
									echo "<div class='form-group has-error' >";
								else     
									echo "<div class='form-group' >";
							?>
								<label for="present_address" class="col-sm-3 control-label">Present Address <span class="required"> *</span></label>
								<div class="col-sm-9">
									<textarea name="present_address" class="form-control" placeholder="present address"></textarea>
									<p class="control-label col-md-12" style="text-align: left !important; padding-left: 0;"><?php echo form_error('present_address'); ?></p>
								</div>
							</div>
							
							<?php 
								if(form_error('city')) 
									echo "<div class='form-group has-error' >";
								else     
									echo "<div class='form-group' >";
							?>
								<label for="city" class="col-sm-3 control-label">City <span class="required"> *</span></label>
								<div class="col-sm-9">
									<input type="text" name="city" class="form-control" placeholder="city" value="<?=set_value('city')?>" />
									<p class="control-label col-md-12" style="text-align: left !important; padding-left: 0;"><?php echo form_error('city'); ?></p>
								</div>
							</div>
							
							<?php 
								if(form_error('phone')) 
									echo "<div class='form-group has-error' >";
								else     
									echo "<div class='form-group' >";
							?>
								<label for="phone" class="col-sm-3 control-label">Phone <span class="required"> *</span></label>
								<div class="col-sm-9">
									<input type="text" name="phone" class="form-control" placeholder="phone" value="<?=set_value('phone')?>" />
									<p class="control-label col-md-12" style="text-align: left !important; padding-left: 0;"><?php echo form_error('phone'); ?></p>
								</div>
							</div>
							
							<?php 
								if(form_error('email')) 
									echo "<div class='form-group has-error' >";
								else     
									echo "<div class='form-group' >";
							?>
								<label for="email" class="col-sm-3 control-label">Email <span class="required"> *</span></label>
								<div class="col-sm-9">
									<input type="text" name="email" class="form-control" placeholder="email" value="<?=set_value('email')?>" />
									<p class="control-label col-md-12" style="text-align: left !important; padding-left: 0;"><?php echo form_error('email'); ?></p>
								</div>
							</div>
							
							
						</div>
					</div>
				</div>
				<!--End Contact Details -->
				
				<div class="col-md-6">
					<div class="box box-primary">
						<div class="box-header with-border">
							<h4 class="box-title">Official Status</h4>
						</div>
						
						<div class="box-body">
							<?php 
								if(form_error('employeeID')) 
									echo "<div class='form-group has-error' >";
								else     
									echo "<div class='form-group' >";
							?>
								<label for="employeeID" class="col-sm-3 control-label">Employee ID <span class="required"> *</span></label>
								<div class="col-sm-9">
									<input type="text" name="employeeID" class="form-control" placeholder="employee id" value="<?=set_value('employeeID')?>" />
									<p class="control-label col-md-12" style="text-align: left !important; padding-left: 0;"><?php echo form_error('employeeID'); ?></p>
								</div>
							</div>
							
							<?php 
								if(form_error('designation_id')) 
									echo "<div class='form-group has-error' >";
								else     
									echo "<div class='form-group' >";
							?>
								<label for="designation_id" class="col-sm-3 control-label">Designation <span class="required"> *</span></label>
								<div class="col-sm-9">
									<select name="designation_id" class="form-control" aria-required="true" aria-invalid="false">
										<option value="none" selected="selected">Select designation...</option>
											<?php if (!empty($all_department_info)): foreach ($all_department_info as $dept_name => $v_department_info) : ?>
													<?php if (!empty($v_department_info)): ?>
														<optgroup label="<?php echo $dept_name; ?>">
															<?php foreach ($v_department_info as $designation) : ?>
																<option value="<?php echo $designation->designation_id; ?>" 
																<?php
																if (!empty($employee_info->designation_id)) {
																	echo $designation->designation_id == $employee_info->designation_id ? 'selected' : '';
																}
																?>><?php echo $designation->designation_name ?></option>                            
																	<?php endforeach; ?>
														</optgroup>
													<?php endif; ?>                            
												<?php endforeach; ?>
											<?php endif; ?>
									</select>
									<p class="control-label col-md-12" style="text-align: left !important; padding-left: 0;"><?php echo form_error('designation_id'); ?></p>
								</div>
							</div>
							
							<?php 
								if(form_error('joining_date')) 
									echo "<div class='form-group has-error' >";
								else     
									echo "<div class='form-group' >";
							?>
								<label for="joining_date" class="col-sm-3 control-label">Joining Date <span class="required"> *</span></label>
								<div class="col-sm-9">
									<input type="date" name="joining_date" class="form-control" placeholder="joining date" />
									<p class="control-label col-md-12" style="text-align: left !important; padding-left: 0;"><?php echo form_error('joining_date'); ?></p>
								</div>
							</div>
							
						</div>
					</div>
				</div>
				<!--End official Details -->
				
				<div class="col-md-12">
					<div class="row">
						<div class="col-md-6">
							<div class="box box-primary">
								<div class="box-header with-border">
									<h4 class="box-title">Bank Information</h4>
								</div>
								
								<div class="box-body">
									<div class='form-group' >
										<label for="bank_name" class="col-sm-3 control-label">Bank name</label>
										<div class="col-sm-9">
											<input type="text" name="bank_name" class="form-control" placeholder="bank name" value="<?=set_value('bank_name')?>" />
											<p class="control-label col-md-12" style="text-align: left !important; padding-left: 0;"></p>
										</div>
									</div>
									
									<div class='form-group' >
										<label for="branch_name" class="col-sm-3 control-label">Branch name </label>
										<div class="col-sm-9">
											<input type="text" name="branch_name" class="form-control" placeholder="branch name" value="<?=set_value('branch_name')?>" />
											<p class="control-label col-md-12" style="text-align: left !important; padding-left: 0;"></p>
										</div>
									</div>
									
									<div class='form-group'>
										<label for="account_name" class="col-sm-3 control-label">Account Name </label>
										<div class="col-sm-9">
											<input type="text" name="account_name" class="form-control" placeholder="account name" value="<?=set_value('account_name')?>" />
											<p class="control-label col-md-12" style="text-align: left !important; padding-left: 0;"></p>
										</div>
									</div>
									
									<div class='form-group' >
										<label for="account_number" class="col-sm-3 control-label">Account Number </label>
										<div class="col-sm-9">
											<input type="text" name="account_number" class="form-control" placeholder="account number" value="<?=set_value('account_number')?>" />
											<p class="control-label col-md-12" style="text-align: left !important; padding-left: 0;"></p>
										</div>
									</div>
									
								</div>
							</div>
						</div>
						<!--End Bank Details -->
						
						<div class="col-md-6">
							<div class="box box-primary">
								<div class="box-header with-border">
									<h4 class="box-title">Employee Document</h4>
								</div>
								
								<div class="box-body">
									<div class='form-group'>
										<label for="resume" class="col-sm-3 control-label">Resume</label>
										<div class="col-sm-5 col-xs-12 col-md-5">
											<input class="form-control"  id="uploadFileResume" placeholder="Choose File" disabled />  
										</div>

										<div class="col-sm-4 col-xs-12 col-md-4">
											<div class="fileUpload btn btn-success form-control">
												<span class="fa fa-repeat"></span>
												<span>Upload</span>
												<input id="uploadResume" type="file" class="upload" name="resume" />
											</div>
										</div>
									</div>
									
									<div class='form-group'>
										<label for="offer_letter" class="col-sm-3 control-label">Offer Letter</label>
										<div class="col-sm-5 col-xs-12 col-md-5">
											<input class="form-control"  id="uploadFileOfferletter" placeholder="Choose File" disabled />  
										</div>

										<div class="col-sm-4 col-xs-12 col-md-4">
											<div class="fileUpload btn btn-success form-control">
												<span class="fa fa-repeat"></span>
												<span>Upload</span>
												<input id="uploadOfferLetter" type="file" class="upload" name="offer_letter" />
											</div>
										</div>
									</div>
									
									<div class='form-group'>
										<label for="joining_letter" class="col-sm-3 control-label">Joining Letter</label>
										<div class="col-sm-5 col-xs-12 col-md-5">
											<input class="form-control"  id="uploadFileJoiningLetter" placeholder="Choose File" disabled />  
										</div>

										<div class="col-sm-4 col-xs-12 col-md-4">
											<div class="fileUpload btn btn-success form-control">
												<span class="fa fa-repeat"></span>
												<span>Upload</span>
												<input id="uploadJoiningLetter" type="file" class="upload" name="joining_letter" />
											</div>
										</div>
									</div>
									
									<div class='form-group'>
										<label for="contract_paper" class="col-sm-3 control-label">Contract Paper</label>
										<div class="col-sm-5 col-xs-12 col-md-5">
											<input class="form-control"  id="uploadFileContractPaper" placeholder="Choose File" disabled />  
										</div>

										<div class="col-sm-4 col-xs-12 col-md-4">
											<div class="fileUpload btn btn-success form-control">
												<span class="fa fa-repeat"></span>
												<span>Upload</span>
												<input id="uploadContractPaper" type="file" class="upload" name="contract_paper" />
											</div>
										</div>
									</div>
									
									<div class='form-group'>
										<label for="id_proff" class="col-sm-3 control-label">ID Proff</label>
										<div class="col-sm-5 col-xs-12 col-md-5">
											<input class="form-control"  id="uploadFileIdProff" placeholder="Choose File" disabled />  
										</div>

										<div class="col-sm-4 col-xs-12 col-md-4">
											<div class="fileUpload btn btn-success form-control">
												<span class="fa fa-repeat"></span>
												<span>Upload</span>
												<input id="UploadIdProff" type="file" class="upload" name="id_proff" />
											</div>
										</div>
									</div>
									
									<div class='form-group'>
										<label for="other_documents" class="col-sm-3 control-label">Other Documents</label>
										<div class="col-sm-5 col-xs-12 col-md-5">
											<input class="form-control"  id="uploadFileOther" placeholder="Choose File" disabled />  
										</div>

										<div class="col-sm-4 col-xs-12 col-md-4">
											<div class="fileUpload btn btn-success form-control">
												<span class="fa fa-repeat"></span>
												<span>Upload</span>
												<input id="uploadOther" type="file" class="upload" name="other_documents" />
											</div>
										</div>
									</div>
									
								</div>
							</div>
						</div>
						<!--End employee document -->
					</div>
				</div>
				
				<div class="col-sm-6 col-sm-offset-3 col-xs-12 col-md-6 col-md-offset-3">
					<div class="form-group margin">
						<button type="submit" id="sbtn" class="btn btn-block btn-success"><i class="fa fa-save"></i>&nbsp; Save</button>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>
<!-- end panel -->

<!-- begin scroll to top btn -->
<a href="javascript:;" class="btn btn-icon btn-circle btn-success btn-scroll-to-top fade" data-click="scroll-top"><i class="fa fa-angle-up"></i></a>
<!-- end scroll to top btn -->




<?php /*
<div class="box box-danger">
    <div class="box-header with-border">
        <h3 class="box-title"><i class="fa fa-list-ol"></i> Add New Employee</h3>

       
        <ol class="breadcrumb">
            <li><a href="<?=base_url("dashboard/index")?>"><i class="fa fa-laptop"></i> <?=$this->lang->line('menu_dashboard')?></a></li>
            <li><a href="<?php echo base_url().'employee/index'; ?>">Employee</a></li>
            <li class="active">Add Employee</li>
        </ol>
    </div><!-- /.box-header -->
    <!-- form start -->
    <div class="box-body">
        <div class="row">
            <div class="col-sm-12">
                <form class="form-horizontal" action="" role="form" method="post">
					<div class="col-md-6">
						<div class="box box-primary">
							<div class="box-header with-border">
								<h4 class="box-title">Personal Details</h4>
							</div>
							
							<div class="box-body">
								<?php 
									if(form_error('first_name')) 
										echo "<div class='form-group has-error' >";
									else     
										echo "<div class='form-group' >";
								?>
									<label for="first_name" class="col-sm-3 control-label">First Name <span class="required"> *</span></label>
									<div class="col-sm-9">
										<input type="text" name="first_name" class="form-control" placeholder="first name" value="<?=set_value('first_name')?>" />
										<p class="control-label col-md-12" style="text-align: left !important; padding-left: 0;"><?php echo form_error('first_name'); ?></p>
									</div>
								</div>
								
								<?php 
									if(form_error('last_name')) 
										echo "<div class='form-group has-error' >";
									else     
										echo "<div class='form-group' >";
								?>
									<label for="last_name" class="col-sm-3 control-label">Last Name <span class="required"> *</span></label>
									<div class="col-sm-9">
										<input type="text" name="last_name" class="form-control" placeholder="last name" value="<?=set_value('last_name')?>" />
										<p class="control-label col-md-12" style="text-align: left !important; padding-left: 0;"><?php echo form_error('last_name'); ?></p>
									</div>
								</div>
								
								<?php 
									if(form_error('date_of_birth')) 
										echo "<div class='form-group has-error' >";
									else     
										echo "<div class='form-group' >";
								?>
									<label for="date_of_birth" class="col-sm-3 control-label">Date of Birth <span class="required"> *</span></label>
									<div class="col-sm-9">
										<input type="date" name="date_of_birth" class="form-control" placeholder="date of birth" />
										<p class="control-label col-md-12" style="text-align: left !important; padding-left: 0;"><?php echo form_error('date_of_birth'); ?></p>
									</div>
								</div>
								
								<?php 
									if(form_error('gender')) 
										echo "<div class='form-group has-error' >";
									else     
										echo "<div class='form-group' >";
								?>
									<label for="gender" class="col-sm-3 control-label">Gender <span class="required"> *</span></label>
									<div class="col-sm-9">
										<select name="gender" class="form-control">
											<option value="none" selected="selected">Gender...</option>
											<option value="Male">Male</option>
											<option value="Female">Female</option>
										</select>
										<p class="control-label col-md-12" style="text-align: left !important; padding-left: 0;"><?php echo form_error('gender'); ?></p>
									</div>
								</div>
								
								<?php 
									if(form_error('marital_status')) 
										echo "<div class='form-group has-error' >";
									else     
										echo "<div class='form-group' >";
								?>
									<label for="marital_status" class="col-sm-3 control-label">Marital status <span class="required"> *</span></label>
									<div class="col-sm-9">
										<select name="marital_status" class="form-control" aria-required="true" aria-invalid="false">
											<option value="none" selected="selected">Maratal Status...</option>
											<option value="Married">Married</option>
											<option value="Un-Married">Un-Married</option>
											<option value="Widowed">Widowed</option>
											<option value="Divorced">Divorced</option>
										</select>
										<p class="control-label col-md-12" style="text-align: left !important; padding-left: 0;"><?php echo form_error('marital_status'); ?></p>
									</div>
								</div>
								
								<?php 
									if(form_error('father_name')) 
										echo "<div class='form-group has-error' >";
									else     
										echo "<div class='form-group' >";
								?>
									<label for="father_name" class="col-sm-3 control-label">Father's Name <span class="required"> *</span></label>
									<div class="col-sm-9">
										<input type="text" name="father_name" class="form-control" placeholder="father's name" value="<?=set_value('father_name')?>" />
										<p class="control-label col-md-12" style="text-align: left !important; padding-left: 0;"><?php echo form_error('father_name'); ?></p>
									</div>
								</div>
								
								<?php 
									if(form_error('nationality')) 
										echo "<div class='form-group has-error' >";
									else     
										echo "<div class='form-group' >";
								?>
									<label for="nationality" class="col-sm-3 control-label">Nationality <span class="required"> *</span></label>
									<div class="col-sm-9">
										<input type="text" name="nationality" class="form-control" placeholder="nationality" value="<?=set_value('nationality')?>" />
										<p class="control-label col-md-12" style="text-align: left !important; padding-left: 0;"><?php echo form_error('nationality'); ?></p>
									</div>
								</div>
								
								<div class='form-group'>
									<label for="passport_no" class="col-sm-3 control-label">Passport No <span class="required"> *</span></label>
									<div class="col-sm-9">
										<input type="text" name="passport_no" class="form-control" placeholder="passport no" value="<?=set_value('passport_no')?>" />
										<p class="control-label col-md-12" style="text-align: left !important; padding-left: 0;"></p>
									</div>
								</div>
								
								<?php 
									if(isset($image)) 
										echo "<div class='form-group has-error' >";
									else     
										echo "<div class='form-group' >";
								?>
									<label for="photo" class="col-sm-2 control-label col-xs-8 col-md-3">
										Photo
									</label>
									<div class="col-sm-4 col-xs-6 col-md-5">
										<input class="form-control"  id="uploadFile" placeholder="Choose File" disabled />  
									</div>

									<div class="col-sm-2 col-xs-6 col-md-4">
										<div class="fileUpload btn btn-success form-control">
											<span class="fa fa-repeat"></span>
											<span><?=$this->lang->line("upload")?></span>
											<input id="uploadBtn" type="file" class="upload" name="image" />
										</div>
									</div>
									 <span class="col-md-12">
									   
										<?php if(isset($image)) echo $image; ?>
									</span>
								</div>
					
					
							</div>
						</div>
					</div>
					<!--End Personal Details -->
					
					<div class="col-md-6">
						<div class="box box-primary">
							<div class="box-header with-border">
								<h4 class="box-title">Contact Details</h4>
							</div>
							
							<div class="box-body">
								<?php 
									if(form_error('present_address')) 
										echo "<div class='form-group has-error' >";
									else     
										echo "<div class='form-group' >";
								?>
									<label for="present_address" class="col-sm-3 control-label">Present Address <span class="required"> *</span></label>
									<div class="col-sm-9">
										<textarea name="present_address" class="form-control" placeholder="present address"></textarea>
										<p class="control-label col-md-12" style="text-align: left !important; padding-left: 0;"><?php echo form_error('present_address'); ?></p>
									</div>
								</div>
								
								<?php 
									if(form_error('city')) 
										echo "<div class='form-group has-error' >";
									else     
										echo "<div class='form-group' >";
								?>
									<label for="city" class="col-sm-3 control-label">City <span class="required"> *</span></label>
									<div class="col-sm-9">
										<input type="text" name="city" class="form-control" placeholder="city" value="<?=set_value('city')?>" />
										<p class="control-label col-md-12" style="text-align: left !important; padding-left: 0;"><?php echo form_error('city'); ?></p>
									</div>
								</div>
								
								<?php 
									if(form_error('phone')) 
										echo "<div class='form-group has-error' >";
									else     
										echo "<div class='form-group' >";
								?>
									<label for="phone" class="col-sm-3 control-label">Phone <span class="required"> *</span></label>
									<div class="col-sm-9">
										<input type="text" name="phone" class="form-control" placeholder="phone" value="<?=set_value('phone')?>" />
										<p class="control-label col-md-12" style="text-align: left !important; padding-left: 0;"><?php echo form_error('phone'); ?></p>
									</div>
								</div>
								
								<?php 
									if(form_error('email')) 
										echo "<div class='form-group has-error' >";
									else     
										echo "<div class='form-group' >";
								?>
									<label for="email" class="col-sm-3 control-label">Email <span class="required"> *</span></label>
									<div class="col-sm-9">
										<input type="text" name="email" class="form-control" placeholder="email" value="<?=set_value('email')?>" />
										<p class="control-label col-md-12" style="text-align: left !important; padding-left: 0;"><?php echo form_error('email'); ?></p>
									</div>
								</div>
								
								
							</div>
						</div>
					</div>
					<!--End Contact Details -->
					
					<div class="col-md-6">
						<div class="box box-primary">
							<div class="box-header with-border">
								<h4 class="box-title">Official Status</h4>
							</div>
							
							<div class="box-body">
								<?php 
									if(form_error('employeeID')) 
										echo "<div class='form-group has-error' >";
									else     
										echo "<div class='form-group' >";
								?>
									<label for="employeeID" class="col-sm-3 control-label">Employee ID <span class="required"> *</span></label>
									<div class="col-sm-9">
										<input type="text" name="employeeID" class="form-control" placeholder="employee id" value="<?=set_value('employeeID')?>" />
										<p class="control-label col-md-12" style="text-align: left !important; padding-left: 0;"><?php echo form_error('employeeID'); ?></p>
									</div>
								</div>
								
								<?php 
									if(form_error('designation_id')) 
										echo "<div class='form-group has-error' >";
									else     
										echo "<div class='form-group' >";
								?>
									<label for="designation_id" class="col-sm-3 control-label">Designation <span class="required"> *</span></label>
									<div class="col-sm-9">
										<select name="designation_id" class="form-control" aria-required="true" aria-invalid="false">
											<option value="none" selected="selected">Select designation...</option>
												<?php if (!empty($all_department_info)): foreach ($all_department_info as $dept_name => $v_department_info) : ?>
														<?php if (!empty($v_department_info)): ?>
															<optgroup label="<?php echo $dept_name; ?>">
																<?php foreach ($v_department_info as $designation) : ?>
																	<option value="<?php echo $designation->designation_id; ?>" 
																	<?php
																	if (!empty($employee_info->designation_id)) {
																		echo $designation->designation_id == $employee_info->designation_id ? 'selected' : '';
																	}
																	?>><?php echo $designation->designation_name ?></option>                            
																		<?php endforeach; ?>
															</optgroup>
														<?php endif; ?>                            
													<?php endforeach; ?>
												<?php endif; ?>
										</select>
										<p class="control-label col-md-12" style="text-align: left !important; padding-left: 0;"><?php echo form_error('designation_id'); ?></p>
									</div>
								</div>
								
								<?php 
									if(form_error('joining_date')) 
										echo "<div class='form-group has-error' >";
									else     
										echo "<div class='form-group' >";
								?>
									<label for="joining_date" class="col-sm-3 control-label">Joining Date <span class="required"> *</span></label>
									<div class="col-sm-9">
										<input type="date" name="joining_date" class="form-control" placeholder="joining date" />
										<p class="control-label col-md-12" style="text-align: left !important; padding-left: 0;"><?php echo form_error('joining_date'); ?></p>
									</div>
								</div>
								
							</div>
						</div>
					</div>
					<!--End official Details -->
					
					<div class="col-md-12">
						<div class="row">
							<div class="col-md-6">
								<div class="box box-primary">
									<div class="box-header with-border">
										<h4 class="box-title">Bank Information</h4>
									</div>
									
									<div class="box-body">
										<div class='form-group' >
											<label for="bank_name" class="col-sm-3 control-label">Bank name</label>
											<div class="col-sm-9">
												<input type="text" name="bank_name" class="form-control" placeholder="bank name" value="<?=set_value('bank_name')?>" />
												<p class="control-label col-md-12" style="text-align: left !important; padding-left: 0;"></p>
											</div>
										</div>
										
										<div class='form-group' >
											<label for="branch_name" class="col-sm-3 control-label">Branch name </label>
											<div class="col-sm-9">
												<input type="text" name="branch_name" class="form-control" placeholder="branch name" value="<?=set_value('branch_name')?>" />
												<p class="control-label col-md-12" style="text-align: left !important; padding-left: 0;"></p>
											</div>
										</div>
										
										<div class='form-group'>
											<label for="account_name" class="col-sm-3 control-label">Account Name </label>
											<div class="col-sm-9">
												<input type="text" name="account_name" class="form-control" placeholder="account name" value="<?=set_value('account_name')?>" />
												<p class="control-label col-md-12" style="text-align: left !important; padding-left: 0;"></p>
											</div>
										</div>
										
										<div class='form-group' >
											<label for="account_number" class="col-sm-3 control-label">Account Number </label>
											<div class="col-sm-9">
												<input type="text" name="account_number" class="form-control" placeholder="account number" value="<?=set_value('account_number')?>" />
												<p class="control-label col-md-12" style="text-align: left !important; padding-left: 0;"></p>
											</div>
										</div>
										
									</div>
								</div>
							</div>
							<!--End Bank Details -->
							
							<div class="col-md-6">
								<div class="box box-primary">
									<div class="box-header with-border">
										<h4 class="box-title">Employee Document</h4>
									</div>
									
									<div class="box-body">
										<div class='form-group'>
											<label for="resume" class="col-sm-3 control-label">Resume</label>
											<div class="col-sm-5 col-xs-12 col-md-5">
												<input class="form-control"  id="uploadFileResume" placeholder="Choose File" disabled />  
											</div>

											<div class="col-sm-4 col-xs-12 col-md-4">
												<div class="fileUpload btn btn-success form-control">
													<span class="fa fa-repeat"></span>
													<span>Upload</span>
													<input id="uploadResume" type="file" class="upload" name="resume" />
												</div>
											</div>
										</div>
										
										<div class='form-group'>
											<label for="offer_letter" class="col-sm-3 control-label">Offer Letter</label>
											<div class="col-sm-5 col-xs-12 col-md-5">
												<input class="form-control"  id="uploadFileOfferletter" placeholder="Choose File" disabled />  
											</div>

											<div class="col-sm-4 col-xs-12 col-md-4">
												<div class="fileUpload btn btn-success form-control">
													<span class="fa fa-repeat"></span>
													<span>Upload</span>
													<input id="uploadOfferLetter" type="file" class="upload" name="offer_letter" />
												</div>
											</div>
										</div>
										
										<div class='form-group'>
											<label for="joining_letter" class="col-sm-3 control-label">Joining Letter</label>
											<div class="col-sm-5 col-xs-12 col-md-5">
												<input class="form-control"  id="uploadFileJoiningLetter" placeholder="Choose File" disabled />  
											</div>

											<div class="col-sm-4 col-xs-12 col-md-4">
												<div class="fileUpload btn btn-success form-control">
													<span class="fa fa-repeat"></span>
													<span>Upload</span>
													<input id="uploadJoiningLetter" type="file" class="upload" name="joining_letter" />
												</div>
											</div>
										</div>
										
										<div class='form-group'>
											<label for="contract_paper" class="col-sm-3 control-label">Contract Paper</label>
											<div class="col-sm-5 col-xs-12 col-md-5">
												<input class="form-control"  id="uploadFileContractPaper" placeholder="Choose File" disabled />  
											</div>

											<div class="col-sm-4 col-xs-12 col-md-4">
												<div class="fileUpload btn btn-success form-control">
													<span class="fa fa-repeat"></span>
													<span>Upload</span>
													<input id="uploadContractPaper" type="file" class="upload" name="contract_paper" />
												</div>
											</div>
										</div>
										
										<div class='form-group'>
											<label for="id_proff" class="col-sm-3 control-label">ID Proff</label>
											<div class="col-sm-5 col-xs-12 col-md-5">
												<input class="form-control"  id="uploadFileIdProff" placeholder="Choose File" disabled />  
											</div>

											<div class="col-sm-4 col-xs-12 col-md-4">
												<div class="fileUpload btn btn-success form-control">
													<span class="fa fa-repeat"></span>
													<span>Upload</span>
													<input id="UploadIdProff" type="file" class="upload" name="id_proff" />
												</div>
											</div>
										</div>
										
										<div class='form-group'>
											<label for="other_documents" class="col-sm-3 control-label">Other Documents</label>
											<div class="col-sm-5 col-xs-12 col-md-5">
												<input class="form-control"  id="uploadFileOther" placeholder="Choose File" disabled />  
											</div>

											<div class="col-sm-4 col-xs-12 col-md-4">
												<div class="fileUpload btn btn-success form-control">
													<span class="fa fa-repeat"></span>
													<span>Upload</span>
													<input id="uploadOther" type="file" class="upload" name="other_documents" />
												</div>
											</div>
										</div>
										
									</div>
								</div>
							</div>
							<!--End employee document -->
						</div>
					</div>
					
					<div class="col-sm-6 col-sm-offset-3 col-xs-12 col-md-6 col-md-offset-3">
						<div class="form-group margin">
							<button type="submit" id="sbtn" class="btn btn-block btn-success"><i class="fa fa-save"></i>&nbsp; Save</button>
						</div>
					</div>
				</form>
            </div>
        </div>
    </div>
</div>
*/ ?>


<script type="text/javascript">
	/*document.getElementById("uploadBtn").onchange = function() {
		document.getElementById("uploadFile").value = this.value;
	};*/
	
	$(document).ready(function (){
		getUploadValue("uploadBtn", "uploadFile");
		getUploadValue("uploadResume", "uploadFileResume");
		getUploadValue("uploadOfferLetter", "uploadFileOfferletter");
		getUploadValue("uploadJoiningLetter", "uploadFileJoiningLetter");
		getUploadValue("uploadContractPaper", "uploadFileContractPaper");
		getUploadValue("UploadIdProff", "uploadFileIdProff");
		getUploadValue("uploadOther", "uploadFileOther");
	});
	
	function getUploadValue($btnName, $txtName){
		document.getElementById($btnName).onchange = function() {
			document.getElementById($txtName).value = this.value;
		};
	}
</script>