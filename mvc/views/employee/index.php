<div class="row">
	<div class="col-md-1 hidden-xs">
		<img src="<?php echo base_url('uploads/images/logo.png'); ?>" style="width: auto; height: 40px; margin-top: -8px;" class="img-thumbnail img-responsive" />
	</div>
	<div class="col-md-7 col-xs-9 text-center">
		<h1 class="page-header"><?php echo $this->session->userdata('school');  ?></h1>
	</div>
	<div class="col-md-4 col-xs-3">
		<!-- begin breadcrumb -->
		<ol class="breadcrumb pull-right">
			<li><a href="<?=base_url("dashboard/index")?>"><i class="fa fa-laptop"></i> <?=$this->lang->line('menu_dashboard')?></a></li>
            <li class="active"><?=$this->lang->line('title') ?></li>
		</ol>
		<!-- end breadcrumb -->
	</div>
</div>

<div id="page-container">
<!-- begin panel -->
<div class="panel panel-inverse" data-sortable-id="ui-widget-3">
	<div class="panel-heading">
		<?php $usertype = $this->session->userdata("usertype"); ?>
		<?php if ($usertype == "Admin" || $usertype == "Super Admin" || $usertype == "Teacher"): ?>
			<div class="btn-group-vertical pull-right">
				<a href="<?php echo base_url('employee/add') ?>" class="btn btn-success btn-xs"><span class="fa fa-plus add_margin"></span> <?=$this->lang->line('add_title')?></a>
			</div>
		<?php endif ?>
		<h4 class="panel-title"><i class="fa fa-desktop"></i> <?=$this->lang->line('panel_title')?></h4>
	</div>
	<div class="panel-body">
		<table id="example1" class="table table-striped table-bordered table-hover dataTable no-footer">
			<thead>
				<tr>
					<th class="col-sm-1"><?=$this->lang->line('slno'); ?></th>
					<th class="col-sm-2"><?=$this->lang->line('employeeID'); ?></th>
					<th class="col-sm-2"><?=$this->lang->line('employee_name'); ?></th>
					<th class="col-sm-2"><?=$this->lang->line('dept_desig'); ?></th>
					<th class="col-sm-2"><?=$this->lang->line('mobile'); ?></th>
					<th class="col-sm-2"><?=$this->lang->line('status'); ?></th>
					<th class="col-sm-2"><?=$this->lang->line('details'); ?></th>
					<th class="col-sm-1"><?=$this->lang->line('action'); ?></th>
				</tr>
			</thead>
			<tbody>
				<?php if(count($all_employee_info)) {$i = 1; foreach($all_employee_info as $all_employee) { ?>
					<tr>
						<td class="col-md-1">
							<?php echo $i; ?>
						</td>

						<td class="col-md-1">
							<?php echo $all_employee->employment_id; ?>
						</td>

						<td>
							<?php echo $all_employee->first_name; ?> <?php echo $all_employee->last_name; ?> 
						</td>
						
						<td>
							<?php echo $all_employee->department_name; ?> > <?php echo $all_employee->designation_name; ?> 
						</td>
						
						<td>
							<?php echo $all_employee->mobile; ?>
						</td>
						
						<td>
							<?php 
								if ($all_employee->status == 1) {
									echo '<span class="label label-success">Active</span>';
								} else {
									echo '<span class="label label-danger">Deactive</span>';
								}
							?>
						</td>
						
						<td>
							<a href="<?php echo base_url() . 'employee/profile/' . $all_employee->employee_id; ?>" class="btn btn-primary btn-xs" data-toggle="tooltip" data-placement="top" title="" data-original-title="View"><span class="fa fa-list-alt"></span></a>
						</td>
						
					   <td class="col-md-1">
							<a href="#" class="btn btn-warning btn-xs mrg" data-placement="top" data-toggle="tooltip" data-original-title="Edit"><i class="fa fa-edit"></i></a>
							
							<a href="<?php echo base_url() . 'employee/delete/' . $all_employee->employee_id; ?>" onclick="return confirm('you are about to delete a record. This cannot be undone. are you sure?')" class="btn btn-danger btn-xs mrg" data-placement="top" data-toggle="tooltip" data-original-title="Delete"><i class="fa fa-trash-o"></i></a>
						</td>
					</tr>
				<?php $i++; }} ?>
			</tbody>
		</table>
	</div>
</div>
</div>





<div class="box box-danger">
    <div class="box-header with-border">
        <h3 class="box-title"><i class="fa fa-list-ul"></i> Manage Employee</h3>

       
        <ol class="breadcrumb">
            <li><a href="<?=base_url("dashboard/index")?>"><i class="fa fa-laptop"></i> <?=$this->lang->line('menu_dashboard')?></a></li>
            <li class="active">Employee</li>
        </ol>
    </div><!-- /.box-header -->
    <!-- form start -->
    
   <div class="box-body">
   		<div class="row">
            <div class="col-sm-12 col-md-12">
				<h5 class="page-header">
					<a href="<?php echo base_url('employee/add') ?>" class="btn btn-success btn-lg">
						<i class="fa fa-plus add_margin"></i> Add Employee
					</a>
				</h5>
				
				
				<div id="hide-table">
                    <table id="example1" class="table table-striped table-bordered table-hover dataTable no-footer">
                        <thead>
                            <tr>
                                <th class="col-sm-1">#</th>
                                <th class="col-sm-2">Employee ID</th>
                                <th class="col-sm-2">Employee Name</th>
								<th class="col-sm-2">Dept > Designation</th>
								<th class="col-sm-2">Mobile</th>
								<th class="col-sm-2">Status</th>
								<th class="col-sm-2">View Details</th>
								<th class="col-sm-1">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php if(count($all_employee_info)) {$i = 1; foreach($all_employee_info as $all_employee) { ?>
                                <tr>
                                    <td class="col-md-1">
                                        <?php echo $i; ?>
                                    </td>

                                    <td class="col-md-1">
                                        <?php echo $all_employee->employment_id; ?>
                                    </td>

                                    <td>
                                        <?php echo $all_employee->first_name; ?> <?php echo $all_employee->last_name; ?> 
                                    </td>
									
									<td>
                                        <?php echo $all_employee->department_name; ?> > <?php echo $all_employee->designation_name; ?> 
                                    </td>
									
									<td>
                                        <?php echo $all_employee->mobile; ?>
                                    </td>
									
									<td>
                                        <?php 
											if ($all_employee->status == 1) {
												echo '<span class="label label-success">Active</span>';
											} else {
												echo '<span class="label label-danger">Deactive</span>';
											}
										?>
                                    </td>
									
									<td>
                                        <a href="<?php echo base_url() . 'employee/profile/' . $all_employee->employee_id; ?>" class="btn btn-primary btn-xs" data-toggle="tooltip" data-placement="top" title="" data-original-title="View"><span class="fa fa-list-alt"></span></a>
                                    </td>
									
                                   <td class="col-md-1">
										<a href="#" class="btn btn-warning btn-xs mrg" data-placement="top" data-toggle="tooltip" data-original-title="Edit"><i class="fa fa-edit"></i></a>
										
										<a href="<?php echo base_url() . 'employee/delete/' . $all_employee->employee_id; ?>" onclick="return confirm('you are about to delete a record. This cannot be undone. are you sure?')" class="btn btn-danger btn-xs mrg" data-placement="top" data-toggle="tooltip" data-original-title="Delete"><i class="fa fa-trash-o"></i></a>
									</td>
                                </tr>
                            <?php $i++; }} ?>
                        </tbody>
                    </table>
                </div>
			</div>
		</div>
	</div>
</div>