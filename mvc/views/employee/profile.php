
<div class="box box-danger">
    <div class="box-header with-border">
        <h3 class="box-title"><i class="fa fa-list-ol"></i> Employee Details</h3>

       
        <ol class="breadcrumb">
            <li><a href="<?=base_url("dashboard/index")?>"><i class="fa fa-laptop"></i> <?=$this->lang->line('menu_dashboard')?></a></li>
            <li><a href="<?php echo base_url().'employee/index'; ?>">Employee</a></li>
            <li class="active">Employee Details</li>
        </ol>
    </div><!-- /.box-header -->
    <!-- form start -->
    <div class="box-body">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
				
				<div class="user-image text-center">
					<img src="<?php echo $employee_info->photo_path; ?>" alt="" title="" class="img-circle">
				</div>
				<div class="user-info-block">
					<div class="user-heading" style="margin-bottom: 20px;">
						<h3><?php echo $employee_info->first_name; ?> <?php echo $employee_info->last_name; ?> </h3>
						|&nbsp;<span class="help-block" style="display: inline; color: #000;"><b>Employee Id:</b> <?php echo $employee_info->employment_id; ?></span>&nbsp;|&nbsp;
						<span class="help-block" style="display: inline; color: #000;"><b>Department:</b> <?php echo $employee_info->department_name; ?></span>&nbsp;|&nbsp;
						<span class="help-block" style="display: inline; color: #000;"><b>Designation:</b> <?php echo $employee_info->designation_name; ?></span>&nbsp;|&nbsp;
					</div>
					<ul class="navigation">
						<li class="active">
							<a data-toggle="tab" href="#personal">
								<span class="fa fa-user"></span> Personal Details
							</a>
						</li>
						<li>
							<a data-toggle="tab" href="#contact">
								<span class="fa icon-bus"></span> Contact Detials
							</a>
						</li>
						<li>
							<a data-toggle="tab" href="#employee">
								<span class="fa fa-credit-card"></span> Bank Information
							</a>
						</li>
						<li>
							<a data-toggle="tab" href="#bank">
								<span class="fa fa-file-text"></span> Employee Document
							</a>
						</li>
					</ul>
					<div class="user-body">
						<div class="tab-content">
							<div id="personal" class="tab-pane active">
								<div class="box-body">
									<div class="col-xs-6 col-md-8 col-md-offset-2">
										<p class="lead">Personal Details</p>
										<div class="table-responsive">
											<table class="table table-striped">
												<tbody>
													<tr>
														<th style="width:50%">Joining Date:</th>
														<td><?php echo $employee_info->joining_date; ?></td>
													</tr>
													<tr>
														<th>Date of Birth:</th>
														<td><?php echo $employee_info->date_of_birth; ?></td>
													</tr>
													<tr>
														<th>Gender:</th>
														<td><?php echo $employee_info->gender; ?></td>
													</tr>
													<tr>
														<th>Marital Status:</th>
														<td><?php echo $employee_info->marital_status; ?></td>
													</tr>
													<tr>
														<th>Father's Name:</th>
														<td><?php echo $employee_info->father_name; ?></td>
													</tr>
													<tr>
														<th>Nationality:</th>
														<td><?php echo $employee_info->nationality; ?></td>
													</tr>
												</tbody>
											</table>
										</div>
									</div>
								</div>
							</div>
							
							<div id="contact" class="tab-pane">
								<div class="box-body">
									<div class="col-xs-6 col-md-8 col-md-offset-2">
										<p class="lead">Contact Details</p>
										<div class="table-responsive">
											<table class="table table-striped">
												<tbody>
													<tr>
														<th style="width:50%">Email:</th>
														<td><?php echo $employee_info->email; ?></td>
													</tr>
													<tr>
														<th>Mobile:</th>
														<td><?php echo $employee_info->mobile; ?></td>
													</tr>
													<tr>
														<th>Present Address:</th>
														<td><?php echo $employee_info->present_address; ?></td>
													</tr>
													<tr>
														<th>City:</th>
														<td><?php echo $employee_info->city; ?></td>
													</tr>
													<tr>
														<th>Nationality:</th>
														<td><?php echo $employee_info->nationality; ?></td>
													</tr>
												</tbody>
											</table>
										</div>
									</div>
								</div>
							</div>
							
							<div id="employee" class="tab-pane">
								<div class="box-body">
									<div class="col-xs-6 col-md-8 col-md-offset-2">
										<p class="lead">Bank Information</p>
										<div class="table-responsive">
											<table class="table table-striped">
												<tbody>
													<?php if (!empty($employee_info->bank_name)): ?>
														<tr>
															<th style="width:50%">Bank Name:</th>
															<td><?php echo $employee_info->bank_name; ?></td>
														</tr>
													<?php endif; ?>													
												
													<?php if (!empty($employee_info->branch_name)): ?>
														<tr>
															<th style="width:50%">Branch Name:</th>
															<td><?php echo $employee_info->branch_name; ?></td>
														</tr>
													<?php endif; ?>
													
													<?php if (!empty($employee_info->account_name)): ?>
														<tr>
															<th style="width:50%">Account Name:</th>
															<td><?php echo $employee_info->account_name; ?></td>
														</tr>
													<?php endif; ?>
													
													<?php if (!empty($employee_info->account_number)): ?>
														<tr>
															<th style="width:50%">Account Number:</th>
															<td><?php echo $employee_info->account_number; ?></td>
														</tr>
													<?php endif; ?>
												</tbody>
											</table>
										</div>
									</div>
								</div>
							</div>
							
							<div id="bank" class="tab-pane">
								<div class="box-body">
									<div class="col-xs-6 col-md-8 col-md-offset-2">
										<p class="lead">Employee Document</p>
										<div class="table-responsive">
											<table class="table table-striped">
												<tbody>
													<?php if (!empty($employee_info->resume)): ?>
														<tr>
															<th style="width:50%">Resume:</th>
															<td>
																<a href="<?php echo base_url() . $employee_info->resume; ?>" target="_blank" style="text-decoration: underline;">View Employee Resume</a>
															</td>
														</tr>
													<?php endif; ?>													
												
													<?php if (!empty($employee_info->offer_letter)): ?>
														<tr>
															<th style="width:50%">Offer Letter:</th>
															<td>
																<a href="<?php echo base_url() . $employee_info->offer_letter; ?>" target="_blank" style="text-decoration: underline;">View Offer Latter</a>
															</td>
														</tr>
													<?php endif; ?>
													
													<?php if (!empty($employee_info->joining_letter)): ?>
														<tr>
															<th style="width:50%">Joining Letter:</th>
															<td>
																<a href="<?php echo base_url() . $employee_info->joining_letter; ?>" target="_blank" style="text-decoration: underline;">View Joining Letter</a>
															</td>
														</tr>
													<?php endif; ?>
													
													<?php if (!empty($employee_info->contract_paper)): ?>
														<tr>
															<th style="width:50%">Contract Paper:</th>
															<td>
																<a href="<?php echo base_url() . $employee_info->contract_paper; ?>" target="_blank" style="text-decoration: underline;">View Contract Paper</a>
															</td>
														</tr>
													<?php endif; ?>
													
													<?php if (!empty($employee_info->id_proff)): ?>
														<tr>
															<th style="width:50%">ID Proff:</th>
															<td>
																<a href="<?php echo base_url() . $employee_info->id_proff; ?>" target="_blank" style="text-decoration: underline;">View ID Proff</a>
															</td>
														</tr>
													<?php endif; ?>
													
													<?php if (!empty($employee_info->other_document)): ?>
														<tr>
															<th style="width:50%">Other Document:</th>
															<td>
																<a href="<?php echo base_url() . $employee_info->other_document; ?>" target="_blank" style="text-decoration: underline;">View Other Document</a>
															</td>
														</tr>
													<?php endif; ?>
												</tbody>
											</table>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				
				
            </div>
        </div>
    </div>
</div>