<style type="text/css">
.fileUpload {
    margin: 0;
    overflow: hidden;
    position: relative;
}

.fileUpload input.upload {
    position: absolute;
    top: 0;
    right: 0;
    margin: 0;
    padding: 0;
    font-size: 20px;
    cursor: pointer;
    opacity: 0;
    filter: alpha(opacity=0);
}
</style>

<div class="row">
	<div class="col-md-2 hidden-xs">
		<img src="<?php echo base_url('uploads/images/logo.png'); ?>" style="width: auto; height: 40px; margin-top: -8px;" class="img-thumbnail img-responsive" />
	</div>
	<div class="col-md-7 col-xs-9 text-center">
		<h1 class="page-header"><?php echo $this->session->userdata('school');  ?></h1>
	</div>
	<div class="col-md-3 col-xs-3">
		<!-- begin breadcrumb -->
		<ol class="breadcrumb pull-right">
			<li><a href="<?=base_url("dashboard/index")?>"><i class="fa fa-laptop"></i> <?=$this->lang->line('menu_dashboard')?></a></li>
            <li><a href="<?=base_url("subject/index")?>"><?=$this->lang->line('menu_subject')?></a></li>
            <li class="active"><?=$this->lang->line('menu_add')?> <?=$this->lang->line('panel_title')?></li>
		</ol>
		<!-- end breadcrumb -->
	</div>
</div>

<div id="page-container">
<!-- begin panel -->
<div class="panel panel-inverse" data-sortable-id="ui-widget-3">
	<div class="panel-heading">
		<h4 class="panel-title"><i class="fa fa-desktop"></i> <?=$this->lang->line('panel_title')?></h4>
	</div>
	<div class="panel-body">
		<div class="col-sm-8">
			<form class="form-horizontal" role="form" method="post">
				<?php 
					if(form_error('classesID')) 
						echo "<div class='form-group has-error' >";
					else     
						echo "<div class='form-group' >";
				?>
					<label for="classesID" class="col-sm-2 control-label">
						<?=$this->lang->line("subject_class_name")?>
					</label>
					<div class="col-sm-6">
					   <?php
							$array = array();
							$array[0] = $this->lang->line("subject_select_classes");
							foreach ($classes as $classa) {
								$array[$classa->classesID] = $classa->classes;
							}
							echo form_dropdown("classesID", $array, set_value("classesID"), "id='classesID' class='form-control'");
						?>
					</div>
					<span class="col-sm-4 control-label">
						<?php echo form_error('classesID'); ?>
					</span>
				</div>

				<?php 
					if(form_error('teacherID')) 
						echo "<div class='form-group has-error' >";
					else     
						echo "<div class='form-group' >";
				?>
					<label for="teacherID" class="col-sm-2 control-label">
						<?=$this->lang->line("subject_teacher_name")?>
					</label>
					<div class="col-sm-6">
						<?php
							$array = array();
							$array[0] = $this->lang->line("subject_select_teacher");
							foreach ($teachers as $teacher) {
								$array[$teacher->teacherID] = $teacher->name;
							}
							echo form_dropdown("teacherID", $array, set_value("teacherID"), "id='teacherID' class='form-control'");
						?>
					</div>
					<span class="col-sm-4 control-label">
						<?php echo form_error('teacherID'); ?>
					</span>
				</div>
				
				<?php 
					if(form_error('subject_category')) 
						echo "<div class='form-group has-error' >";
					else     
						echo "<div class='form-group' >";
				?>
					<label for="subject_code" class="col-sm-2 control-label">
						<?=$this->lang->line("subject_category")?>
					</label>
					<div class="col-sm-6">
						<select class="form-control" id="subject_category" name="subject_category">
							<option value=""><?php echo $this->lang->line("subject_select_category"); ?></option>
							<?php foreach($categories as $val => $category): ?>
							<option value="<?php echo $category['id']; ?>"><?php echo $category['category']; ?></option>
							<?php endforeach; ?>
						</select>
					</div>
					<span class="col-sm-4 control-label">
						<?php echo form_error('subject_category'); ?>
					</span>
				</div>

				<?php 
					if(form_error('subject')) 
						echo "<div class='form-group has-error' >";
					else     
						echo "<div class='form-group' >";
				?>
					<label for="subject" class="col-sm-2 control-label">
						<?=$this->lang->line("subject_name")?>
					</label>
					<div class="col-sm-6">
						<input type="text" class="form-control" id="subject" name="subject" value="<?=set_value('subject')?>" >
					</div>
					<span class="col-sm-4 control-label">
						<?php echo form_error('subject'); ?>
					</span>
				</div>

				<?php 
					if(form_error('subject_author')) 
						echo "<div class='form-group has-error' >";
					else     
						echo "<div class='form-group' >";
				?>
					<label for="subject_author" class="col-sm-2 control-label">
						<?=$this->lang->line("subject_author")?>
					</label>
					<div class="col-sm-6">
						<input type="text" class="form-control" id="subject_author" name="subject_author" value="<?=set_value('subject_author')?>" >
					</div>
					<span class="col-sm-4 control-label">
						<?php echo form_error('subject_author'); ?>
					</span>
				</div>

				<?php 
					if(form_error('subject_code')) 
						echo "<div class='form-group has-error' >";
					else     
						echo "<div class='form-group' >";
				?>
					<label for="subject_code" class="col-sm-2 control-label">
						<?=$this->lang->line("subject_code")?>
					</label>
					<div class="col-sm-6">
						<input type="text" class="form-control" id="subject_code" name="subject_code" value="<?=set_value('subject_code')?>" >
					</div>
					<span class="col-sm-4 control-label">
						<?php echo form_error('subject_code'); ?>
					</span>
				</div>
				
				<?php 
					if(form_error('subject_assessment')) 
						echo "<div class='form-group has-error' >";
					else     
						echo "<div class='form-group' >";
				?>
					<label for="subject_code" class="col-sm-2 control-label">
						<?=$this->lang->line("subject_assessment")?>
					</label>
					<div class="col-sm-6">
						<select class="form-control" id="subject_assessment" name="subject_assessment">
							<option value="">Select An Option</option>
							<option value="1">One</option>
							<option value="2">Two</option>
							<option value="3">Three</option>
							<option value="4">Four</option>
						</select>
					</div>
					<span class="col-sm-4 control-label">
						<?php echo form_error('subject_assessment'); ?>
					</span>
				</div>

				<div class="form-group">
					<div class="col-sm-offset-2 col-sm-8">
						<input type="submit" class="btn btn-success" value="<?=$this->lang->line("add_subject")?>" >
					</div>
				</div>
			</form>
		</div>
	</div>
</div>


