<!-- Header -->

<div class="row">
	<div class="col-md-2 hidden-xs">
		<img src="<?php echo base_url('uploads/images/logo.png'); ?>" style="width: auto; height: 40px; margin-top: -8px;" class="img-thumbnail img-responsive" />
	</div>
	<div class="col-md-7 col-xs-9 text-center">
		<h1 class="page-header"><?php echo $this->session->userdata('school');  ?></h1>
	</div>
	<div class="col-md-3 col-xs-3">
		<!-- begin breadcrumb -->
		<ol class="breadcrumb pull-right">
			<li ><a href="<?php echo base_url('dashboard/index');?>"><i class="fa fa-laptop"></i> Dashboard</a></li>
			<li class="">Subject</li>
		</ol>
		<!-- end breadcrumb -->
		
	</div>
</div>
<div id="page-container">
<!-- begin panel -->
<div class="panel panel-inverse" data-sortable-id="ui-widget-3">
	<div class="panel-heading">
		<?php 
			$usertype = $this->session->userdata("usertype");
			if($usertype == "Super Admin" || $usertype == "Admin") {
		?>
		<div class="btn-group-vertical pull-right">
			<a href="<?php echo base_url('subject/add') ?>" type="button" class="btn btn-success btn-xs"><i class="fa fa-plus"></i> Add a Subject</a>
		</div>
		<?php } ?>
		<h4 class="panel-title"><i class="fa fa-pencil"></i> <?=$this->lang->line('panel_title')?></h4>
	</div>
	
	
	
	<!--End header -->
	

	<div class="panel-body">
    <!-- form start -->
    <div class="box-body">
        <div class="row">
			<div class="col-sm-12">
				<div class="row add_border">
					<div class="col-md-6 add_padding">
						<form style="" class="form-horizontal" role="form" method="post">
							<div class="input-group">
								<span class="input-group-addon offset_addon"><i class="fa fa-sitemap"></i></span>
								<?php
									$array = array("0" => $this->lang->line("subject_select_class"));
									foreach ($classes as $classa) {
										$array[$classa->classesID] = $classa->classes;
									}
									echo form_dropdown("classesID", $array, set_value("classesID"), "id='classesID' class='form-control offset_height'");
								?>
							</div>
						</form>
					</div>
				</div>
			</div><!-- col-sm-12 -->
            
        </div><!-- row -->
    </div><!-- Body -->
</div><!-- /.box -->
<script type="text/javascript">
    $('#classesID').change(function() {
        var classesID = $(this).val();
        if(classesID == 0) {
            $('#hide-table').hide();
        } else {
            $.ajax({
                type: 'POST',
                url: "<?=base_url('subject/subject_list')?>",
                data: "id=" + classesID,
                dataType: "html",
                success: function(data) {
                    window.location.href = data;
                }
            });
        }
    });
</script>