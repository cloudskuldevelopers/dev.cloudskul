<!-- Header -->

<div class="row">
	<div class="col-md-2 hidden-xs">
		<img src="<?php echo base_url('uploads/images/logo.png'); ?>" style="width: auto; height: 40px; margin-top: -8px;" class="img-thumbnail img-responsive" />
	</div>
	<div class="col-md-7 col-xs-9 text-center">
		<h1 class="page-header"><?php echo $this->session->userdata('school');  ?></h1>
	</div>
	<div class="col-md-3 col-xs-3">
		<!-- begin breadcrumb -->
		<ol class="breadcrumb pull-right">
			<li ><a href="<?php echo base_url('dashboard/index');?>"><i class="fa fa-laptop"></i> Dashboard</a></li>
			<li class="">Subject</li>
		</ol>
		<!-- end breadcrumb -->
		
	</div>
</div>
<div id="page-container">
<!-- begin panel -->
<div class="panel panel-inverse" data-sortable-id="ui-widget-3">
	<div class="panel-heading">
		<?php 
			$usertype = $this->session->userdata("usertype");
			if($usertype == "Super Admin" || $usertype == "Admin") {
		?>
		<div class="btn-group-vertical pull-right">
			<a href="<?php echo base_url('subject/add') ?>" type="button" class="btn btn-success btn-xs"><i class="fa fa-plus"></i> Add Subject</a>
		</div>
		<?php } ?>
		<h4 class="panel-title"><i class="fa fa-pencil"></i> <?=$this->lang->line('panel_title')?></h4>
	</div>
	

<div class="panel-body">
    <!-- form start -->
    <div class="box-body">
        <div class="row">
			<div class="col-sm-12" style="border-bottom:1px solid #F0F3F5; margin-bottom:10px;">
				<div class="row add_border">
					<?php 
						$usertype = $this->session->userdata("usertype");
						if($usertype == "Admin" || $usertype == "Super Admin" || $usertype == "Librarian" || $usertype == "Teacher") {
					?>
					<div class="col-md-6 add_padding" style="margin-bottom:10px;">
						<form style="" class="form-horizontal" role="form" method="post">
							<div class="input-group">
								<span class="input-group-addon offset_addon"><i class="fa fa-sitemap"></i></span>
								<?php
									$array = array("0" => $this->lang->line("subject_select_class"));
									foreach ($classes as $classa) {
										$array[$classa->classesID] = $classa->classes;
									}
									echo form_dropdown("classesID", $array, set_value("classesID", $set), "id='classesID' class='form-control offset_height'");
								?>
							</div>
						</form>
					</div>
				</div>
			</div><!-- col-sm-12 -->
			
            <div class="col-sm-12">
                <?php } ?>



                <div id="hide-table">
                    <table id="data-table" class="table table-striped table-bordered nowrap" width="100%">
                        <thead>
                            <tr>
                                <th><?=$this->lang->line('slno')?></th>
                                <th><?=$this->lang->line('subject_name')?></th>
                                <th><?=$this->lang->line('subject_author')?></th>
                                <th><?=$this->lang->line('subject_code')?></th>
                                <th><?=$this->lang->line('subject_category')?></th>
                                <th><?=$this->lang->line('subject_assessment')?></th>
                                <th><?=$this->lang->line('subject_teacher')?></th>
                                <?php  if($usertype == "Admin" || $usertype == "Super Admin") { ?>
                                <th><?=$this->lang->line('action')?></th>
                                <?php } ?>
                            </tr>
                        </thead>

                        <tbody>
                            <?php if(count($subjects)) {$i = 1; foreach($subjects as $subject) { ?>
                                <tr>
                                    <td data-title="<?=$this->lang->line('slno')?>">
                                        <?php echo $i; ?>
                                    </td>
                                    <td data-title="<?=$this->lang->line('subject_name')?>">
                                        <?php echo $subject->subject; ?>
                                    </td>
                                    <td data-title="<?=$this->lang->line('subject_author')?>">
                                        <?php echo $subject->subject_author; ?>
                                    </td>
                                    <td data-title="<?=$this->lang->line('subject_code')?>">
                                        <?php echo $subject->subject_code; ?>
                                    </td>
									<td data-title="<?=$this->lang->line('subject_category')?>">
                                        <?php echo $this->db->get_where('subject_category', array("id" => $subject->subject_categoryID))->row()->category; ?>
                                    </td>
									<td data-title="<?=$this->lang->line('subject_assessment')?>">
                                        <?php echo $subject->no_of_assessment; ?>
                                    </td>
                                    <td data-title="<?=$this->lang->line('subject_teacher')?>">
                                        <?php echo $subject->teacher_name; ?>
                                    </td>
                                    <?php  if($usertype == "Admin" || $usertype == "Super Admin") { ?>
                                    <td data-title="<?=$this->lang->line('action')?>">
                                        <?php echo btn_edit('subject/edit/'.$subject->subjectID."/".$set, $this->lang->line('edit')) ?>
                                        <?php echo btn_delete('subject/delete/'.$subject->subjectID."/".$set, $this->lang->line('delete')) ?>
                                    </td>
                                    <?php } ?>
                                </tr>
                            <?php $i++; }} ?>
                        </tbody>
                    </table>
                </div>

            </div>
        </div>
    </div>
</div>


<script type="text/javascript">
    $('#classesID').change(function() {
        var classesID = $(this).val();
        if(classesID == 0) {
            $('#hide-table').hide();
        } else {
            $.ajax({
                type: 'POST',
                url: "<?=base_url('subject/subject_list')?>",
                data: "id=" + classesID,
                dataType: "html",
                success: function(data) {
                    window.location.href = data;
                }
            });
        }
    });
</script>
