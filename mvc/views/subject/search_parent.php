<!-- Header -->

<div class="row">
	<div class="col-md-2 hidden-xs">
		<img src="<?php echo base_url('uploads/images/logo.png'); ?>" style="width: auto; height: 40px; margin-top: -8px;" class="img-thumbnail img-responsive" />
	</div>
	<div class="col-md-7 col-xs-9 text-center">
		<h1 class="page-header"><?php echo $this->session->userdata('school');  ?></h1>
	</div>
	<div class="col-md-3 col-xs-3">
		<!-- begin breadcrumb -->
		<ol class="breadcrumb pull-right">
			<li ><a href="<?php echo base_url('dashboard/index');?>"><i class="fa fa-laptop"></i> Dashboard</a></li>
			<li class="">Subject</li>
		</ol>
		<!-- end breadcrumb -->
		
	</div>
</div>
<div id="page-container">
<!-- begin panel -->
<div class="panel panel-inverse" data-sortable-id="ui-widget-3">
	<div class="panel-heading">
		<?php 
			$usertype = $this->session->userdata("usertype");
			if($usertype == "Super Admin" || $usertype == "Admin") {
		?>
		<div class="btn-group-vertical pull-right">
			<a href="<?php echo base_url('classes/add') ?>" type="button" class="btn btn-success btn-xs"><i class="fa fa-plus"></i> Add a Subject</a>
		</div>
		<?php } ?>
		<h4 class="panel-title"><i class="fa fa-pencil"></i> <?=$this->lang->line('panel_title')?></h4>
	</div>
	
	
	
	<!--End header -->
	
	
	<div class="panel-body">
    <!-- form start -->
    <div class="box-body">
        <div class="row">
            <div class="col-sm-12">
				<div class="col-md-6 col-sm-offset-3 add_padding">
					<blockquote style="border-right: 5px solid #eee;">
						<form style="" class="form-horizontal" role="form" method="post">
							<div class="input-group">
								<label for="classesID" class="col-sm-2 control-label" style="padding-left: 0px;">
									<?=$this->lang->line("subject_student")?>
								</label>
								<span class="input-group-addon offset_addon"><i class="fa fa-sitemap"></i></span>
								 <?php
									$array = array("0" => $this->lang->line("subject_select_student"));
									if($students) {
										foreach ($students as $student) {
											$array[$student->studentID] = $student->name;
										}
									}
									echo form_dropdown("studentID", $array, set_value("studentID"), "id='studentID' class='form-control offset_height'");
								?>
							</div>
						</form>
					</blockquote>
				</div>

            </div> <!-- col-sm-12 -->
            
        </div><!-- row -->
    </div><!-- Body -->
</div><!-- /.box -->
<script type="text/javascript">
    $('#studentID').change(function() {
        var studentID = $(this).val();
        if(studentID == 0) {
            $('#hide-table').hide();
        } else {
            $.ajax({
                type: 'POST',
                url: "<?=base_url('subject/student_list')?>",
                data: "id=" + studentID,
                dataType: "html",
                success: function(data) {
                    window.location.href = data;
                }
            });
        }
    });
</script>