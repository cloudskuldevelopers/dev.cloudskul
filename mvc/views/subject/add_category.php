<style type="text/css">
.fileUpload {
    margin: 0;
    overflow: hidden;
    position: relative;
}

.fileUpload input.upload {
    position: absolute;
    top: 0;
    right: 0;
    margin: 0;
    padding: 0;
    font-size: 20px;
    cursor: pointer;
    opacity: 0;
    filter: alpha(opacity=0);
}
</style>

<div class="row">
	<div class="col-md-1 hidden-xs">
		<img src="<?php echo base_url('uploads/images/logo.png'); ?>" style="width: auto; height: 40px; margin-top: -8px;" class="img-thumbnail img-responsive" />
	</div>
	<div class="col-md-7 col-xs-9 text-center">
		<h1 class="page-header"><?php echo $this->session->userdata('school');  ?></h1>
	</div>
	<div class="col-md-4 col-xs-3">
		<!-- begin breadcrumb -->
		<ol class="breadcrumb pull-right">
			<li><a href="<?=base_url("dashboard/index")?>"><i class="fa fa-laptop"></i> <?=$this->lang->line('menu_dashboard')?></a></li>
            <li><a href="<?=base_url("subject/category")?>"><?=$this->lang->line('menu_subj_category')?></a></li>
            <li class="active"><?=$this->lang->line('add_category')?></li>
		</ol>
		<!-- end breadcrumb -->
	</div>
</div>

<div id="page-container">
<!-- begin panel -->
<div class="panel panel-inverse" data-sortable-id="ui-widget-3">
	<div class="panel-heading">
		<h4 class="panel-title"><i class="fa fa-desktop"></i> <?=$this->lang->line('panel_title')?></h4>
	</div>
	<div class="panel-body">
		<div class="col-sm-8">
			<form class="form-horizontal" role="form" method="post">
				<?php 
					if(form_error('subject_category')) 
						echo "<div class='form-group has-error' >";
					else     
						echo "<div class='form-group' >";
				?>
					<label for="subject_code" class="col-sm-2 control-label">
						<?=$this->lang->line("subject_category")?>
					</label>
					<div class="col-sm-6">
						<input type="text" class="form-control" id="subject_category" name="subject_category" value="<?=set_value('subject_category')?>" >
					</div>
					<span class="col-sm-4 control-label">
						<?php echo form_error('subject_category'); ?>
					</span>
				</div>
				

				<div class="form-group">
					<div class="col-sm-offset-2 col-sm-8">
						<input type="submit" class="btn btn-success" value="<?=$this->lang->line("add_category")?>" >
					</div>
				</div>
			</form>
		</div>
	</div>
</div>
