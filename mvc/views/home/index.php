<header class="header-area">
<!-- Navigation start -->
<nav class="navbar navbar-custom tb-nav" role="navigation">
	<div class="container">        
	  <div class="navbar-header">
		<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#tb-nav-collapse">
		  <span class="sr-only">Toggle navigation</span>
		  <span class="icon-bar"></span>
		  <span class="icon-bar"></span>
		  <span class="icon-bar"></span>
		</button>
		<a class="navbar-brand logo" href="#">
		<h2><span></i></span><span>&lt;&frasl; Dev&gt;</span>
		</h2>
		</a>
	  </div>
  
	  <div class="collapse navbar-collapse" id="tb-nav-collapse">
  
		<ul class="nav navbar-nav navbar-right">
			<li class="active"><a class="page-scroll" href="#top">Home</a></li>
			<li><a class="page-scroll" href="#about">About</a></li>
			<li><a class="page-scroll" href="#service">What We Do</a></li>
			<li><a class="page-scroll" href="#testimonial">Dev Team</a></li>
			<li><a class="page-scroll" href="#portfolio">Portfolio</a></li>
			
			<ul class="nav navbar-nav navbar-right">
			<li><a class="page-scroll" href="signin/index">Login</a></li>
			</ul>
		
		</ul>
	  </div>
	  
	  
		
	  
	</div>
</nav>
</header>
<!-- Navigation end -->

<!-- hero-section -->
<section class="hero-section static-bg" id="top">
		<div class="slider-caption">
			<div class="container">
				<div class="row">
					<!-- Hero Title -->
					<h1><span style="color:#e5e5e5">welcome to </span><span>Cloudskuldev</span></h1>
					<!-- Hero Subtitle -->
					<h5><span style="color:#fff">we advice + we design + we develop</span></h5>
				</div>
			</div>
		</div>
</section>
<!-- End Of Hero Section -->

<!-- About Us Section -->

<section class="about-us" id="about">
	<div class="container-fluid">
		<div class="row">
			<div class="col-sm-6 col-xs-12 no-padding">
				<!-- About Us Left -->
				<div class="about-us-left">
					<!-- About Us Info -->
					<h2 class="section-title-left">About Cloudskul</h2>
					<p> Our mission is to connect the most remote educational institutions, offer them cheap technology and bring them to appreciate technology in education..</p>
					
				<img src="assets/home/img/bg/_3921aaa.png" width="516" height="400"  alt=""/>
			 </div>
			</div>
			<div class="col-sm-6 col-xs-12 no-padding">
				<!-- About Us Right -->
				<div class="about-us-right">
					<!-- Our Features -->
					<div class="media">
						<div class="media-left dotted">
							<i class="fa fa-lightbulb-o"></i>
						</div>
						<div class="media-body">
							<!-- About Right Title -->
							<h3 class="media-heading">We Advice</h3>
							<p>We monitor financial advisers are complying with their obligations. </p>
						</div>
					</div>
					<!-- Our Features -->
					<div class="media">
						<div class="media-left dotted">
							<i class="fa fa-pencil-square-o"></i>
						</div>
						<div class="media-body">
							<h3 class="media-heading">We Design</h3>
							<p>we are a collection of professionals focused on marketing and design communication with an obsession of creating innovative, intuitive, content driven marketing tools for our clients. </p>
						</div>
					</div>
					<!-- Our Features -->
					<div class="media">
						<div class="media-left">
							<i class="fa fa-code"></i>
						</div>
						<div class="media-body">
							<h3 class="media-heading">We develop</h3>
							<p>When you need an iOS, Android, HTML5 or desktop app to solve a problem, grow your business or engage your employees, partnering with us is a no-brainer. </p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<!-- End Of About Us Section -->


<!-- Our Services -->

<section class="our-services-section" id="service">
	
		<!-- Our service Section Title -->
		<h2 class="section-title text-center">What we do</h2>
		<!-- Our service  Section Subtitle -->
		<p class="sub-title text-center">CloudSkul Provides you a unique platform to manage your school on the cloud and on the GO.</p>
		
		
		<div class="row our-services">
		<div class="service-wrap">
			<div class="container">
			
			<div class="col-md-12">
				<!-- Single Service -->
				<div class="col-md-4 col-lg-4 col-sm-12 col-xs-12 single-service">
					<div class="media">
						<div class="media-left">
							<i class="fa fa-laptop"></i>
						</div>
						<div class="media-body">
							<!-- Single service title -->
							<h3 class="media-heading">UX/UI Design</h3>
						   
						</div>
					</div>
				</div>

				<!-- Single Service -->
				<div class="col-md-4 col-lg-4 col-sm-12 col-xs-12 single-service">
					<div class="media">
						<div class="media-left">
							<i class="fa fa-code"></i>
						</div>
						<div class="media-body">
							<h3 class="media-heading">Server Side Scripting</h3>
						   
						</div>
					</div>
				</div>
				
				 <!-- Single Service -->
				<div class="col-md-4 col-lg-4 col-sm-12 col-xs-12 single-service">
					<div class="media">
						<div class="media-left">
							<i class="fa fa-mixcloud"></i>
						</div>
						<div class="media-body">
							<!-- Single service title -->
							<h3 class="media-heading">Cloud Computing</h3>
						   
						</div>
					</div>
				</div>
			</div>

			<div class="col-md-12">
				<!-- Single Service -->
				<div class="col-md-4 col-lg-4 col-sm-12 col-xs-12 single-service">
					<div class="media">
						<div class="media-left">
							<i class="fa fa-globe"></i>
						</div>
						<div class="media-body">
							<h3 class="media-heading">Web Services</h3>
						   
						</div>
					</div>
				</div>
				
				 <!-- Single Service -->
				<div class="col-md-4 col-lg-4 col-sm-12 col-xs-12 single-service">
					<div class="media">
						<div class="media-left">
							<i class="fa fa-code-fork"></i>
						</div>
						<div class="media-body">
							<!-- Single service title -->
							<h3 class="media-heading">Client Side Scripting</h3>
						   
						</div>
					</div>
				</div>

				<!-- Single Service -->
				<div class="col-md-4 col-lg-4 col-sm-12 col-xs-12 single-service">
					<div class="media">
						<div class="media-left">
							<i class="fa fa-mobile"></i>
						</div>
						<div class="media-body">
							<h3 class="media-heading">Mobile Development</h3>
						   
						</div>
					</div>
				</div>
			</div>
			
			<div class="col-md-12">
				 <!-- Single Service -->
				<div class="col-md-4 col-lg-4 col-sm-12 col-xs-12 single-service">
					<div class="media">
						<div class="media-left">
							<i class="fa fa-mortar-board"></i>
						</div>
						<div class="media-body">
							<!-- Single service title -->
							<h3 class="media-heading">Training</h3>
						   
						</div>
					</div>
				</div>

				<!-- Single Service -->
				<div class="col-md-4 col-lg-4 col-sm-12 col-xs-12 single-service">
					<div class="media">
						<div class="media-left">
							<i class="fa fa-line-chart"></i>
						</div>
						<div class="media-body">
							<h3 class="media-heading">Data Analysis</h3>
						   
						</div>
					</div>
				</div>
						   
						   
							  <!-- Single Service -->
				<div class="col-md-4 col-lg-4 col-sm-12 col-xs-12 single-service">
					<div class="media">
						<div class="media-left">
							<i class="fa fa-wordpress"></i>
						</div>
						<div class="media-body">
							<!-- Single service title -->
							<h3 class="media-heading">Content Management</h3>
						   
						</div>
					</div>
				</div>
			</div>

			<div class="col-md-12">
				<!-- Single Service -->
				<div class="col-md-4 col-lg-4 col-sm-12 col-xs-12 single-service">
					<div class="media">
						<div class="media-left">
							<i class="fa fa-desktop"></i>
						</div>
						<div class="media-body">
							<h3 class="media-heading">Human Computer Interaction Research</h3>
						   
						</div>
					</div>
				</div>
						   
							  <!-- Single Service -->
				<div class="col-md-4 col-lg-4 col-sm-12col-xs-12 single-service">
					<div class="media">
						<div class="media-left">
							<i class="fa fa-angellist"></i>
						</div>
						<div class="media-body">
							<!-- Single service title -->
							<h3 class="media-heading">Testing and Quality Assurance</h3>
						   
						</div>
					</div>
				</div>

				<!-- Single Service -->
				<div class="col-md-4 col-lg-4 col-sm-12 col-xs-12 single-service">
					<div class="media">
						<div class="media-left">
							<i class="fa fa-dashcube"></i>
						</div>
						<div class="media-body">
							<h3 class="media-heading">Web Project Management</h3>
						   
						</div>
					</div>
				</div>
			</div>
							  
			</div>
		</div>
		</div>
	
</section>

<!-- End Of Our Services -->




		 <h2 class="section-title text-center" style="margin:80px 0;">Developers Team</h2> 


<!-- Testimonial Section -->

<section class="testimonial-section section-padding" id="testimonial">
	<div class="container">
		<div class="row">
		
			<!-- Owl-Testimonial Carousel Starts Here -->
			<div id="testimonial-carousel" class="owl-carousel owl-theme">
			   
				<!-- Carousl Item -->
				   <div class="item">
			   
					<!-- Testimonial Body -->
			   
					<div class="col-md-5 col-md-offset-1 col-lg-5 col-lg-offset-1 col-sm-6 col-xs-12">
						<h2 class="section-title-left testimoinal-title">Ugi <br>Augustine</h2>
						<div class="testimonial-body">
							<b>Position</b><p>Ceo � Nugitech </p>
							<p>My bio my bio my bio my bio my bio my bio my bio my bio my bio my bio my bio My bio my bio my bio my bio my bio my bio my bio my bio my bio my bio my bio.</p>
							<div class="client-name">
								<a href=""><b>Skills</b>
		&nbsp;   &raquo;Html/Css  &nbsp;
		   &raquo;C#    	&nbsp;
		   &raquo;PhP		&nbsp;
		   &raquo;Wipe		&nbsp;
								</a>
							</div>
							
							<div class="client-name">
								<a href=""><b>Download Resume</b> &nbsp;&nbsp; <b>Download Cv</b>
	   
								</a>
							</div>
						   
							
						</div>
					</div>
					<!-- Testimonial Client Images -->
					<div class="col-md-4 col-md-offset-1 col-lg-4 col-lg-offset-1 col-sm-6 col-xs-12 testimonial-image">		
						<figure>
							<img class="img-responsive center-block" src="assets/home/img/clients/team1.png" alt="">
						</figure>
					</div>
				</div>
				
				<!-- Carousel Item -->
				<div class="item">
					<!-- Testimonial Body -->
					<div class="col-md-5 col-md-offset-1 col-lg-5 col-lg-offset-1 col-sm-6 col-xs-12">
						  <h2 class="section-title-left testimoinal-title">Sunday <br> Okoi</h2>
						<div class="testimonial-body">
						   <b> Position</b><p>Chief Software Engineer � Nugitech </p>
							<p>My bio my bio my bio my bio my bio my bio my bio my bio my bio my bio my bio My bio my bio my bio my bio my bio my bio my bio my bio my bio my bio my bio.</p>
							<div class="client-name">
								<a href=""><b>Skills</b>
		&nbsp;   &raquo;HTML/CSS  &nbsp;
		   &raquo;C#    	&nbsp;
		   &raquo;PhP		&nbsp;
		   &raquo;Wipe		&nbsp;
								</a>
							</div>
							
							<div class="client-name">
								<a href=""><b>Download Resume</b> &nbsp;&nbsp; <b>Download Cv</b>
	   
								</a>
							</div>
						   
							
						</div>
					</div>
					<!-- Testimonial Client Images -->
					<div class="col-md-4 col-md-offset-1 col-lg-4 col-lg-offset-1 col-sm-6 col-xs-12 testimonial-image">		
						<figure>
							<img class="img-responsive center-block" src="assets/home/img/clients/_3842aa.png" alt="">
						</figure>
					</div>
				</div>
				
				<!-- Carousel Item -->
				<div class="item">
					<!-- Testimonial Body -->
					<div class="col-md-5 col-md-offset-1 col-lg-5 col-lg-offset-1 col-sm-6 col-xs-12">
						 <h2 class="section-title-left testimoinal-title">Patrick <br> Ikoi</h2>
						<div class="testimonial-body">
						   <b> Position</b><p>WebEvangelist � Cloudskul </p>
							<p>My bio my bio my bio my bio my bio my bio my bio my bio my bio my bio my bio My bio my bio my bio my bio my bio my bio my bio my bio my bio my bio my bio.</p>
							<div class="client-name">
								<a href=""><b>Skills</b>
		&nbsp;   &raquo;Html/Css  &nbsp;
		   &raquo;C#    	&nbsp;
		   &raquo;PhP		&nbsp;
		   &raquo;Wipe		&nbsp;
								</a>
							</div>
							
							<div class="client-name">
								<a href=""><b>Download Resume</b> &nbsp;&nbsp; <b>Download Cv</b>
	   
								</a>
							</div>
						   
							
						</div>
					</div>
					<!-- Testimonial Client Images -->
					<div class="col-md-4 col-md-offset-1 col-lg-4 col-lg-offset-1 col-sm-6 col-xs-12 testimonial-image">		
						<figure>
							<img class="img-responsive center-block" src="assets/home/img/clients/profiletestsmall.jpg" alt="">
						</figure>
					</div>
				</div>

				<!-- Carousel Item -->
				<div class="item">
					<!-- Testimonial Body -->
					<div class="col-md-5 col-md-offset-1 col-lg-5 col-lg-offset-1 col-sm-6 col-xs-12">
						  <h2 class="section-title-left testimoinal-title">Ebarim  <br> Sylvester</h2>
						<div class="testimonial-body">
						   <b> Position</b><p>Software Engineer � Cloudskul </p>
							<p>My bio my bio my bio my bio my bio my bio my bio my bio my bio my bio my bio My bio my bio my bio my bio my bio my bio my bio my bio my bio my bio my bio.</p>
							<div class="client-name">
								<a href=""><b>Skills</b>
		&nbsp;   &raquo;Html/Css  &nbsp;
		   &raquo;C#    	&nbsp;
		   &raquo;PhP		&nbsp;
		   &raquo;Wipe		&nbsp;
								</a>
							</div>
							
							<div class="client-name">
								<a href=""><b>Download Resume</b> &nbsp;&nbsp; <b>Download Cv</b>
	   
								</a>
							</div>
						   
							
						</div>
					</div>
					<!-- Testimonial Client Images -->
					<div class="col-md-4 col-md-offset-1 col-lg-4 col-lg-offset-1 col-sm-6 col-xs-12 testimonial-image">		
						<figure>
							<img class="img-responsive center-block" src="assets/home/img/clients/kkj67aa.png" alt="">
						</figure>
					</div>
				</div>
				
				 <!-- Carousl Item -->
				   <div class="item">
			   
					<!-- Testimonial Body -->
			   
					<div class="col-md-5 col-md-offset-1 col-lg-5 col-lg-offset-1 col-sm-6 col-xs-12">
						  <h2 class="section-title-left testimoinal-title">Samuel <br> Moses</h2>
						<div class="testimonial-body">
						  <b>  Position</b><p>Software Engineer � Cloudskul </p>
							<p>My bio my bio my bio my bio my bio my bio my bio my bio my bio my bio my bio My bio my bio my bio my bio my bio my bio my bio my bio my bio my bio my bio.</p>
							<div class="client-name">
								<a href=""><b>Skills</b>
		&nbsp;   &raquo;Html/Css  &nbsp;
		   &raquo;C#    	&nbsp;
		   &raquo;PhP		&nbsp;
		   &raquo;Wipe		&nbsp;
								</a>
							</div>
							
							<div class="client-name">
								<a href=""><b>Download Resume</b> &nbsp;&nbsp; <b>Download Cv</b>
	   
								</a>
							</div>
						   
							
						</div>
					</div>
					<!-- Testimonial Client Images -->
					<div class="col-md-4 col-md-offset-1 col-lg-4 col-lg-offset-1 col-sm-6 col-xs-12 testimonial-image">		
						<figure>
							<img class="img-responsive center-block" src="assets/home/img/clients/jdxbh.png" alt="">
						</figure>
					</div>
				</div>
				
				<!-- Carousel Item -->
				<div class="item">
					<!-- Testimonial Body -->
					<div class="col-md-5 col-md-offset-1 col-lg-5 col-lg-offset-1 col-sm-6 col-xs-12">
						  <h2 class="section-title-left testimoinal-title">Emmanuel<br> Olube</h2>
						<div class="testimonial-body">
						  <b>  Position</b><p>WebDesigner � Cloudskul </p>
							<p>My bio my bio my bio my bio my bio my bio my bio my bio my bio my bio my bio My bio my bio my bio my bio my bio my bio my bio my bio my bio my bio my bio.</p>
							<div class="client-name">
								<a href=""><b>Skills</b>
		&nbsp;   &raquo;Html/Css  &nbsp;
		   &raquo;C#    	&nbsp;
		   &raquo;PhP		&nbsp;
		   &raquo;Wipe		&nbsp;
								</a>
							</div>
							
							<div class="client-name">
								<a href=""><b>Download Resume</b> &nbsp;&nbsp; <b>Download Cv</b>
	   
								</a>
							</div>
						   
							
						</div>
					</div>
					<!-- Testimonial Client Images -->
					<div class="col-md-4 col-md-offset-1 col-lg-4 col-lg-offset-1 col-sm-6 col-xs-12 testimonial-image">		
						<figure>
							<img class="img-responsive center-block" src="assets/home/img/clients/profiletestsmall.jpg" alt="">
						</figure>
					</div>
				</div>

				<!-- Carousel Item -->
				<div class="item">
					<!-- Testimonial Body -->
					<div class="col-md-5 col-md-offset-1 col-lg-5 col-lg-offset-1 col-sm-6 col-xs-12">
						 <h2 class="section-title-left testimoinal-title">Chibuike <br> Mba</h2>
						<div class="testimonial-body">
						   <b> Position</b><p>WebDesigner � Cloudskul </p>
							<p>My bio my bio my bio my bio my bio my bio my bio my bio my bio my bio my bio My bio my bio my bio my bio my bio my bio my bio my bio my bio my bio my bio.</p>
							<div class="client-name">
								<a href=""><b>Skills</b>
		&nbsp;   &raquo;Html/Css  &nbsp;
		   &raquo;C#    	&nbsp;
		   &raquo;PhP		&nbsp;
		   &raquo;Wipe		&nbsp;
								</a>
							</div>
							
							<div class="client-name">
								<a href=""><b>Download Resume</b> &nbsp;&nbsp; <b>Download Cv</b>
	   
								</a>
							</div>
						   
							
						</div>
					</div>
					<!-- Testimonial Client Images -->
					<div class="col-md-4 col-md-offset-1 col-lg-4 col-lg-offset-1 col-sm-6 col-xs-12 testimonial-image">		
						<figure>
							<img class="img-responsive center-block" src="assets/home/img/clients/profiletestsmall.jpg" alt="">
						</figure>
					</div>
				</div>

				<!-- Carousel Item -->
				<div class="item">
					<!-- Testimonial Body -->
					<div class="col-md-5 col-md-offset-1 col-lg-5 col-lg-offset-1 col-sm-6 col-xs-12">
						  <h2 class="section-title-left testimoinal-title">Gabriel <br> Gee</h2>
						<div class="testimonial-body">
						   <b> Position</b><p>WebDesigner � Cloudskul </p>
							<p>My bio my bio my bio my bio my bio my bio my bio my bio my bio my bio my bio My bio my bio my bio my bio my bio my bio my bio my bio my bio my bio my bio.</p>
							<div class="client-name">
								<a href=""><b>Skills</b>
		&nbsp;   &raquo;Html/Css  &nbsp;
		   &raquo;C#    	&nbsp;
		   &raquo;PhP		&nbsp;
		   &raquo;Wipe		&nbsp;
								</a>
							</div>
							
							<div class="client-name">
								<a href=""><b>Download Resume</b> &nbsp;&nbsp; <b>Download Cv</b>
	   
								</a>
							</div>
						   
							
						</div>
					</div>
					<!-- Testimonial Client Images -->
					<div class="col-md-4 col-md-offset-1 col-lg-4 col-lg-offset-1 col-sm-6 col-xs-12 testimonial-image">		
						<figure>
							<img class="img-responsive center-block" src="assets/home/img/clients/profiletestsmall.jpg" alt="">
						</figure>
					</div>
				</div>
				
				

			</div>
		</div>
	</div>
</section>

<!-- End Of Testimonial Section -->

<!-- Portfolio Section Starts Here -->
<section class="protfolio-section section-padding" id="portfolio" style="padding:80px 0;">
	<div class="container-fluid">
		<!-- Portfolio Section Title -->
		<h2 class="section-title text-center">Our Project</h2>
		
		<div class="row">

			<!-- Portfolio Showcase -->
			<div class="portfolio-showcase">
				<!-- Showcase Grid -->
				<div id="grid" class="clearfix">
					<!-- Single Portfolio Item -->
					<div class="thumbnails" data-groups='["all", "webdesign"]'>
						<a href="assets/home/img/portfolio/better-view/aaa.png">
							<img src="assets/home/img/portfolio/aaa.png" alt="">
							<span class="portfolio-overlay">
								<i class="fa fa-plus"></i>
							</span>
						</a>
					</div>
					<!-- Single Portfolio Item -->
					<div class="thumbnails" data-groups='["all", "photography"]'>
						<a href="assets/home/img/portfolio/better-view/bb.png">
							<img src="assets/home/img/portfolio/bb.png" alt="">
							<span class="portfolio-overlay">
								<i class="fa fa-plus"></i>
							</span>
						</a>
					</div>
					<!-- Single Portfolio Item -->
					<div class="thumbnails" data-groups='["all", "people"]'>
						<a href="assets/home/img/portfolio/better-view/ccc.png">
							<img src="assets/home/img/portfolio/cc.png" alt="">
							<span class="portfolio-overlay">
								<i class="fa fa-plus"></i>
							</span>
						</a>
					</div>
					<!-- Single Portfolio Item -->
					<div class="thumbnails" data-groups='["all", "photography"]'>
						<a href="assets/home/img/portfolio/better-view/dd.png">
							<img src="assets/home/img/portfolio/dd.png" alt="">
							<span class="portfolio-overlay">
								<i class="fa fa-plus"></i>
							</span>
						</a>
					</div>
				   
					

				</div>
			</div>


			
		</div>
	</div>
</section>
<!-- End of Portfolio Section Starts Here -->

<!-- Our Partners Section -->

<section class="our-partners-section" style="background:#000">
	<div class="container">
		<div class="row">
			<!-- Our Partners Carousel -->
			<div id="partners-carousel" class="owl-carousel owl-theme">
				
				<div class="item">
					<img src="assets/home/img/partners/11.png" alt="Microsoft" class="img-responsive center-block">
				</div>
				
				<div class="item">
					<img class="img-responsive center-block" src="assets/home/img/partners/azure.png" alt="Azure">
				</div>
				
				<div class="item">
					<img class="img-responsive center-block" src="assets/home/img/partners/datadog.png" alt="Datadog">
				</div>
				
				<div class="item">
					<img class="img-responsive center-block" src="assets/home/img/partners/lenovo.png" alt="Lenovo">
				</div>
				
				<div class="item">
					<img class="img-responsive center-block" src="assets/home/img/partners/ozioma.png" alt="Ozioma">
				</div>
				
				<div class="item">
					<img class="img-responsive center-block" src="assets/home/img/partners/serverpilot.jpg" alt="Server Pilot">
				</div>
				
				<div class="item">
					<img class="img-responsive center-block" src="assets/home/img/partners/startcom.jpg" alt="Startcom">
				</div>
			   
		  </div>
		</div>
	</div>
</section>

<!-- End Of Our Partners Section -->





<!-- Footer Section -->

<footer class="footer-section">
	<div class="container">
		<div class="row mt-30 mb-30">
			<div class="col-md-12 text-center">
				<!-- Footer Copy Right Text -->
				<div class="copyright-info">
					<a><span><i class="fa fa-code"></i></span> with <span><i class="fa fa-heart"></i></span> From The <span>CloudSkul Developer's Team</span></a><br /><span style="color:#fff; letter-spacing:2px;">developersteam@cloudskul.com</span>
				</div>

				<!-- Footer Social Icons -->
				<div class="social-icons mt-30">
					<a href="https://www.facebook.com/cloudskul"><i class="fa fa-facebook"></i></a>
					<a href="https://twitter.com/cloudskul"><i class="fa fa-twitter"></i></a>
					<a href="https://plus.google.com/cloudskul"><i class="fa fa-google-plus"></i></a>
				</div>
			</div>
		</div>
	</div>
</footer>