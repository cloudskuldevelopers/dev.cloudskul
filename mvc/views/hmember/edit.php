<div class="row">
	<div class="col-md-2 hidden-xs">
		<img src="<?php echo base_url('uploads/images/logo.png'); ?>" style="width: auto; height: 40px; margin-top: -8px;" class="img-thumbnail img-responsive" />
	</div>
	<div class="col-md-7 col-xs-9 text-center">
		<h1 class="page-header"><?php echo $this->session->userdata('school');  ?></h1>
	</div>
	<div class="col-md-3 col-xs-3">
		<!-- begin breadcrumb -->
		<ol class="breadcrumb pull-right">
			<li><a href="<?=base_url("dashboard/index")?>"><i class="fa fa-laptop"></i> <?=$this->lang->line('menu_dashboard')?></a></li>
            <li><a href="<?=base_url("hmember/index/$set")?>"><?=$this->lang->line('menu_member')?></a></li>
            <li class="active"><?=$this->lang->line('menu_edit')?> <?=$this->lang->line('menu_member')?></li>
		</ol>
		<!-- end breadcrumb -->
	</div>
</div>

<div id="page-container">
<!-- begin panel -->
<div class="panel panel-inverse" data-sortable-id="ui-widget-3">
	<div class="panel-heading">
		<h4 class="panel-title"><i class="fa fa-desktop"></i> <?=$this->lang->line('panel_title')?></h4>
	</div>
	<div class="panel-body">
		<div class="col-sm-8">
			<form class="form-horizontal" role="form" method="post">

				<?php 
					if(form_error('hostelID')) 
						echo "<div class='form-group has-error' >";
					else     
						echo "<div class='form-group' >";
				?>
					<label for="hostelID" class="col-sm-2 control-label">
						<?=$this->lang->line("hmember_hname")?>
					</label>
					<div class="col-sm-6">
						<?php
							$array = array();
							$array[0] = $this->lang->line("hmember_select_hostel_name");
							foreach ($hostels as $hostel) {
								$array[$hostel->hostelID] = $hostel->name;
							}
							echo form_dropdown("hostelID", $array, set_value("hostelID", $hmember->hostelID), "id='hostelID' class='form-control'");
						?>
					</div>
					<span class="col-sm-4 control-label">
						<?php echo form_error('hostelID'); ?>
					</span>
				</div>

				<?php 
					if(form_error('categoryID')) 
						echo "<div class='form-group has-error' >";
					else     
						echo "<div class='form-group' >";
				?>
					<label for="categoryID" class="col-sm-2 control-label">
						<?=$this->lang->line("hmember_class_type")?>
					</label>
					<div class="col-sm-6">
						<?php
							$array = array(0 => $this->lang->line("hmember_select_class_type"));
							foreach ($categorys as $category) {
								$array[$category->categoryID] = $category->class_type;
							}
							echo form_dropdown("categoryID", $array, set_value("categoryID", $hmember->categoryID), "id='categoryID' class='form-control'");
						?>
					</div>
					<span class="col-sm-4 control-label">
						<?php echo form_error('categoryID'); ?>
					</span>
				</div>

				<div class="form-group">
					<div class="col-sm-offset-2 col-sm-8">
						<input type="submit" class="btn btn-success" value="<?=$this->lang->line("update_hmember")?>" >
					</div>
				</div>
			</form>
		</div>
	</div>
</div>
</div>



<script type="text/javascript">

$('#hostelID').change(function(event) {
    var hostelID = $(this).val();
    if(hostelID == 0 || hostelID == "" || hostelID == null) {
        $('#categoryID').val(0);
    } else {
        $.ajax({
            type: 'POST',
            url: "<?=base_url('hmember/categorycall')?>",
            data: "id=" + hostelID,
            dataType: "html",
            success: function(data) {
               $('#categoryID').html(data)
            }
        });
    }
});
</script>