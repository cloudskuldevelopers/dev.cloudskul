<div class="row">
	<div class="col-md-2 hidden-xs">
		<img src="<?php echo base_url('uploads/images/logo.png'); ?>" style="width: auto; height: 40px; margin-top: -8px;" class="img-thumbnail img-responsive" />
	</div>
	<div class="col-md-7 col-xs-9 text-center">
		<h1 class="page-header"><?php echo $this->session->userdata('school');  ?></h1>
	</div>
	<div class="col-md-3 col-xs-3">
		<!-- begin breadcrumb -->
		<ol class="breadcrumb pull-right">
			<li><a href="<?=base_url("dashboard/index")?>"><i class="fa fa-laptop"></i> <?=$this->lang->line('menu_dashboard')?></a></li>
			<li><a href="<?=base_url("hmember/index/$set")?>"><?=$this->lang->line('menu_member')?></a></li>
			<li class="active"><?=$this->lang->line('view')?></li>
		</ol>
		<!-- end breadcrumb -->
	</div>
</div>

<?php 
    if(count($student)) {
        $usertype = $this->session->userdata("usertype");
                if($usertype == "Admin" || $usertype == "Super Admin" || $usertype == "Accountant" || $usertype == "Student") {
?>
        <?php if($usertype == "Admin" || $usertype == "Super Admin" || $usertype == "Accountant") { ?>
        <div class="well">
            <div class="row">
                <div class="col-sm-10">
                    <button class="btn btn-success m-r-5 m-b-5" onclick="javascript:printDiv('printablediv')"><span class="fa fa-print"></span> <?=$this->lang->line('print')?> </button>
                    <?php
                     echo btn_add_pdf('hmember/print_preview/'.$hmember->studentID."/".$set, $this->lang->line('pdf_preview')) 
                    ?>
                    <?php echo btn_sm_edit('hmember/edit/'.$hmember->studentID."/".$set, $this->lang->line('edit')) 
                    ?>
                    <button class="btn btn-success m-r-5 m-b-5" data-toggle="modal" data-target="#mail"><span class="fa fa-envelope-o"></span> <?=$this->lang->line('mail')?></button>
                </div>
            </div>
        </div>
        <?php } if($student && $hmember) { ?>

        <div id="printablediv">
			<div class="profile-container" style="padding:0px;">
				<!-- begin profile-section -->
				<div class="profile-section">
					<div class="col-md-12 profile-head-bg">
						<div class="profile-left">
							<!-- begin profile-image -->
							<div class="profile-image" style="border:none;">
								<img src="<?php echo base_url('uploads/images/'.$student->photo);?>" width="170px" height="170px" />
								<i class="fa fa-user hide"></i>
							</div>
							<div class="profile-highlight">
								<h4><i class="fa fa-user"></i> <?=$student->name?> </h4>
								<div class="checkbox m-b-0">
									<?=$this->lang->line("hmember_classes")." ".$class->classes?>
								</div>
							</div>
						</div>
					</div>
					<!-- end profile-left -->
					<!-- begin profile-right -->
					<div class="profile-right col-md-12">
						<!-- begin profile-info -->
						<div class="profile-info">
							<!-- begin table -->
							<div class="table-responsive">
								<table class="table table-profile">
									<tbody>
										<tr class="highlight">
											<td class="field" colspan="4" style="text-align:left;"><h4><?=$this->lang->line("personal_information")?> <small>about student</small></h4></td>
										</tr>
										<tr class="divider">
											<td colspan="4"></td>
										</tr>
										<tr>
											<td class="field"><?=$this->lang->line("hmember_roll")?></td>
											<td><?=$student->roll?></td>
											<td class="field"><?=$this->lang->line("menu_section")?></td>
											<td><?php if(count($section)) { echo $section->section;} else { echo $student->section;}?></td>
										</tr>
										<tr>
											<td class="field"><?=$this->lang->line("hmember_hname")?></td>
											<td><?=$hostel->name?></td>
											<td class="field"><?=$this->lang->line("hmember_htype")?> </td>
											<td><?=$hostel->htype?></td>
										</tr>
										<tr>
											<td class="field"><?=$this->lang->line("hmember_class_type")?></td>
											<td><?=$category->class_type?></td>
											<td class="field"><?=$this->lang->line("hmember_tfee")?></td>
											<td><?=$hmember->hbalance?></td>
										</tr>
										<tr>
											<td class="field"><?=$this->lang->line("hmember_hostel_address")?></td>
											<td><?=$hostel->address?></td>
											<td class="field"><?=$this->lang->line("hmember_joindate")?></td>
											<td><?=date("d M Y", strtotime($hmember->hjoindate))?></td>
										</tr>
										<tr>
											<td class="field"><?=$this->lang->line("hmember_dob")?></td>
											<td><?=date("d M Y", strtotime($student->dob))?></td>
											<td class="field"><?=$this->lang->line("hmember_sex")?></td>
											<td><?=$student->sex?></td>
										</tr>
										<tr>
											<td class="field"><?=$this->lang->line("hmember_religion")?></td>
											<td><?=$student->religion?></td>
											<td class="field"><?=$this->lang->line("hmember_email")?></td>
											<td><?=$student->email?></td>
										</tr>
										<tr>
											<td class="field"><?=$this->lang->line("hmember_phone")?></td>
											<td><i class="fa fa-mobile fa-lg m-r-5"></i> <?=$student->phone?></td>
											<td class="field"><?=$this->lang->line("hmember_address")?></td>
											<td><?=$student->address?></td>
										</tr>
										
									</tbody>
								</table>
							</div>
							<!-- end table -->
						</div>
						<!-- end profile-info -->
					</div>
					<!-- end profile-right -->
				</div>
			</div>
        </div>
        <?php } else { ?>
            <div class="col-lg-12">
            <?php echo "<h2>". $this->lang->line("hmember_message") . "</h2>"; ?>
            </div>
        <?php } ?>


    <?php } ?>
<?php } ?>
<!-- email modal starts here -->
<form class="form-horizontal" role="form" action="<?=base_url('hmember/send_mail');?>" method="post">
    <div class="modal fade" id="mail">
      <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title"><?=$this->lang->line('mail')?></h4>
            </div>
            <div class="modal-body">
            
                <?php 
                    if(form_error('to')) 
                        echo "<div class='form-group has-error' >";
                    else     
                        echo "<div class='form-group' >";
                ?>
                    <label for="to" class="col-sm-2 control-label">
                        <?=$this->lang->line("to")?>
                    </label>
                    <div class="col-sm-6">
                        <input type="email" class="form-control" id="to" name="to" value="<?=set_value('to')?>" >
                    </div>
                    <span class="col-sm-4 control-label" id="to_error">
                    </span>
                </div>

                <?php 
                    if(form_error('subject')) 
                        echo "<div class='form-group has-error' >";
                    else     
                        echo "<div class='form-group' >";
                ?>
                    <label for="subject" class="col-sm-2 control-label">
                        <?=$this->lang->line("subject")?>
                    </label>
                    <div class="col-sm-6">
                        <input type="text" class="form-control" id="subject" name="subject" value="<?=set_value('subject')?>" >
                    </div>
                    <span class="col-sm-4 control-label" id="subject_error">
                    </span>

                </div>

                <?php 
                    if(form_error('message')) 
                        echo "<div class='form-group has-error' >";
                    else     
                        echo "<div class='form-group' >";
                ?>
                    <label for="message" class="col-sm-2 control-label">
                        <?=$this->lang->line("message")?>
                    </label>
                    <div class="col-sm-6">
                        <textarea class="form-control" id="message" style="resize: vertical;" name="message" value="<?=set_value('message')?>" ></textarea>
                    </div>
                </div>

            
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" style="margin-bottom:0px;" data-dismiss="modal"><?=$this->lang->line('close')?></button>
                <input type="button" id="send_pdf" class="btn btn-success" value="<?=$this->lang->line("send")?>" />
            </div>
        </div>
      </div>
    </div>
</form>
<!-- email end here -->  

<script language="javascript" type="text/javascript">
    function printDiv(divID) {
        //Get the HTML of div
        var divElements = document.getElementById(divID).innerHTML;
        //Get the HTML of whole page
        var oldPage = document.body.innerHTML;

        //Reset the page's HTML with div's HTML only
        document.body.innerHTML = 
          "<html><head><title></title></head><body>" + 
          divElements + "</body>";

        //Print Page
        window.print();

        //Restore orignal HTML
        document.body.innerHTML = oldPage;
    }
    function closeWindow() {
        location.reload(); 
    }

    function check_email(email) {
        var status = false;     
        var emailRegEx = /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i;
        if (email.search(emailRegEx) == -1) {
            $("#to_error").html('');
            $("#to_error").html("<?=$this->lang->line('mail_valid')?>").css("text-align", "left").css("color", 'red');
        } else {
            status = true;
        }
        return status;
    }


    $("#send_pdf").click(function(){
        var to = $('#to').val();
        var subject = $('#subject').val();
        var message = $('#message').val();
        var id = "<?=$student->studentID;?>";
        var set = "<?=$set;?>";
        var error = 0;

        if(to == "" || to == null) {
            error++;
            $("#to_error").html("");
            $("#to_error").html("<?=$this->lang->line('mail_to')?>").css("text-align", "left").css("color", 'red');
        } else {
            if(check_email(to) == false) {
                error++
            }
        } 

        if(subject == "" || subject == null) {
            error++;
            $("#subject_error").html("");
            $("#subject_error").html("<?=$this->lang->line('mail_subject')?>").css("text-align", "left").css("color", 'red');
        } else {
            $("#subject_error").html("");
        }

        if(error == 0) {
            $.ajax({
                type: 'POST',
                url: "<?=base_url('hmember/send_mail')?>",
                data: 'to='+ to + '&subject=' + subject + "&id=" + id+ "&message=" + message+ "&set=" + set,
                dataType: "html",
                success: function(data) {
                    location.reload();
                }
            });
        }
    });
</script>

<?php
    $usertype = $this->session->userdata('usertype');
    if($usertype == "Student") {
?>
    <script language="javascript" type="text/javascript">
        var url = window.location.href;
        $("a[href$='"+url+"']").parent().parent().parent().addClass('active');
        $("a[href$='"+url+"']").parent().addClass('active');
        $("a[href$='"+url+"']").parent().parent().css('display', 'block');
    </script>

<?php } ?>