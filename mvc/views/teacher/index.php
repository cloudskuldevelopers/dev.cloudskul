<div class="row">
	<div class="col-md-2 hidden-xs">
		<img src="<?php echo base_url('uploads/images/logo.png'); ?>" style="width: auto; height: 40px; margin-top: -8px;" class="img-thumbnail img-responsive" />
	</div>
	<div class="col-md-7 col-xs-9 text-center">
		<h1 class="page-header"><?php echo $this->session->userdata('school');  ?></h1>
	</div>
	<div class="col-md-3 col-xs-3">
		<!-- begin breadcrumb -->
		<ol class="breadcrumb pull-right">
			<li ><a href="<?php echo base_url('dashboard/index');?>"><i class="fa fa-laptop"></i> Dashboard</a></li>
			<li class="active"><?=$this->lang->line('menu_teacher')?></li>
		</ol>
		<!-- end breadcrumb -->
	</div>
</div>

<div id="page-container">
<!-- begin panel -->
<div class="panel panel-inverse" data-sortable-id="ui-widget-3">
	<div class="panel-heading">
		 <?php 
			$usertype = $this->session->userdata("usertype");
			if($usertype == "Admin" || $usertype == "Super Admin") {
		?>
			<div class="btn-group-vertical pull-right">
				<a href="<?php echo base_url('teacher/add') ?>" type="button" class="btn btn-success btn-xs"><i class="fa fa-plus"></i> Add Teacher</a>
			</div>
		<?php } ?>
		<h4 class="panel-title"><i class="fa fa-desktop"></i> <?=$this->lang->line('panel_title')?></h4>
	</div>
	<div class="panel-body">
		<table id="data-table" class="table table-striped table-bordered nowrap" width="100%">
			<thead>
				<tr>
					<th class="col-sm-2"><?=$this->lang->line('slno')?></th>
					<th class="col-sm-2"><?=$this->lang->line('teacher_photo')?></th>
					<th class="col-sm-2"><?=$this->lang->line('teacher_name')?></th>
					<th class="col-sm-2"><?=$this->lang->line('teacher_email')?></th>
					<?php if($usertype == "Admin" || $usertype == "Super Admin"){ ?>
					<th class="col-sm-2"><?=$this->lang->line('teacher_status')?></th>
					<?php } ?>
					<th class="col-sm-2"><?=$this->lang->line('action')?></th>
				</tr>
			</thead>
			<tbody>
				<?php if(count($teachers)) {$i = 1; foreach($teachers as $teacher) { ?>
					<tr>
						<td data-title="<?=$this->lang->line('slno')?>">
							<?php echo $i; ?>
						</td>
						<td data-title="<?=$this->lang->line('teacher_photo')?>">
							<?php $array = array(
									"src" => base_url('uploads/images/'.$teacher->photo),
									'width' => '35px',
									'height' => '35px',
									'class' => 'img-rounded'

								);
								echo img($array); 
							?>
						</td>
						<td data-title="<?=$this->lang->line('teacher_name')?>">
							<?php echo $teacher->name; ?>
						</td>
						<td data-title="<?=$this->lang->line('teacher_email')?>">
							<?php echo $teacher->email; ?>
						</td>
						<?php if($usertype == "Admin" || $usertype == "Super Admin"){ ?>
						<td data-title="<?=$this->lang->line('teacher_status')?>">
						  <div class="onoffswitch-small" id="<?=$teacher->teacherID?>">
							  <input type="checkbox" id="myonoffswitch<?=$teacher->teacherID?>" class="onoffswitch-small-checkbox" name="paypal_demo" <?php if($teacher->teacheractive === '1') echo "checked='checked'"; ?>>
							  <label for="myonoffswitch<?=$teacher->teacherID?>" class="onoffswitch-small-label">
								  <span class="onoffswitch-small-inner"></span>
								  <span class="onoffswitch-small-switch"></span>
							  </label>
						  </div>           
						</td>
						<?php } ?>
						<td data-title="<?=$this->lang->line('action')?>">
							<?php 
								echo btn_view('teacher/view/'.$teacher->teacherID, $this->lang->line('view')); 
								if($usertype == "Admin" || $usertype == "Super Admin") { 
									echo btn_edit('teacher/edit/'.$teacher->teacherID, $this->lang->line('edit')); 
									echo btn_delete('teacher/delete/'.$teacher->teacherID, $this->lang->line('delete')); 
								} 
							?>
						</td>
					</tr>
				<?php $i++; }} ?>
			</tbody>
		</table>
	</div>
</div>

<script>
  var status = '';
  var id = 0;
  $('.onoffswitch-small-checkbox').click(function() {
      if($(this).prop('checked')) {
          status = 'chacked';
          id = $(this).parent().attr("id");
      } else {
          status = 'unchacked';
          id = $(this).parent().attr("id");
      }

      if((status != '' || status != null) && (id !='')) {
          $.ajax({
              type: 'POST',
              url: "<?=base_url('teacher/active')?>",
              data: "id=" + id + "&status=" + status,
              dataType: "html",
              success: function(data) {
                  if(data == 'Success') {
                      toastr["success"]("Success")
                      toastr.options = {
                        "closeButton": true,
                        "debug": false,
                        "newestOnTop": false,
                        "progressBar": false,
                        "positionClass": "toast-top-right",
                        "preventDuplicates": false,
                        "onclick": null,
                        "showDuration": "500",
                        "hideDuration": "500",
                        "timeOut": "5000",
                        "extendedTimeOut": "1000",
                        "showEasing": "swing",
                        "hideEasing": "linear",
                        "showMethod": "fadeIn",
                        "hideMethod": "fadeOut"
                      }
                  } else {
                      toastr["error"]("Error")
                      toastr.options = {
                        "closeButton": true,
                        "debug": false,
                        "newestOnTop": false,
                        "progressBar": false,
                        "positionClass": "toast-top-right",
                        "preventDuplicates": false,
                        "onclick": null,
                        "showDuration": "500",
                        "hideDuration": "500",
                        "timeOut": "5000",
                        "extendedTimeOut": "1000",
                        "showEasing": "swing",
                        "hideEasing": "linear",
                        "showMethod": "fadeIn",
                        "hideMethod": "fadeOut"
                      }
                  }
              }
          });
      }
  }); 
</script>
