<div class="row">
	<div class="col-md-2 hidden-xs">
		<img src="<?php echo base_url('uploads/images/logo.png'); ?>" style="width: auto; height: 40px; margin-top: -8px;" class="img-thumbnail img-responsive" />
	</div>
	<div class="col-md-7 col-xs-9 text-center">
		<h1 class="page-header"><?php echo $this->session->userdata('school');  ?></h1>
	</div>
	<div class="col-md-3 col-xs-3">
		<!-- begin breadcrumb -->
		<ol class="breadcrumb pull-right">
			<li><a href="<?=base_url("dashboard/index")?>"><i class="fa fa-laptop"></i> <?=$this->lang->line('menu_dashboard')?></a></li>
			<li><a href="<?=base_url("teacher/index")?>"><?=$this->lang->line('menu_teacher')?></a></li>
			<li class="active"><?=$this->lang->line('view')?></li>
		</ol>
		<!-- end breadcrumb -->
	</div>
</div>

<?php 
    if(count($teacher)) {
        $usertype = $this->session->userdata("usertype");
        if($usertype == "Admin" || $usertype == "Super Admin") {
?>
    <div class="well">
        <div class="row">
            <div class="col-sm-10">
                <button class="btn btn-success m-r-5 m-b-5" onclick="javascript:printDiv('printablediv')"><span class="fa fa-print"></span> <?=$this->lang->line('print')?> </button>
                <?php
                 echo btn_add_pdf('teacher/print_preview/'.$teacher->teacherID."/", $this->lang->line('pdf_preview')) 
                ?>

                <button class="btn btn-success m-r-5 m-b-5" data-toggle="modal" data-target="#idCard"><span class="fa fa-floppy-o"></span> <?=$this->lang->line('idcard')?> </button>

                <?php echo btn_sm_edit('teacher/edit/'.$teacher->teacherID."/", $this->lang->line('edit')) 
                ?>
                <button class="btn btn-success m-r-5 m-b-5" data-toggle="modal" data-target="#mail"><span class="fa fa-envelope-o"></span> <?=$this->lang->line('mail')?></button>
            </div>
        </div>   
    </div>
    <?php } ?>
	
	<div id="printablediv">
		<div class="profile-container" style="padding:0px;">
			<!-- begin profile-section -->
			<div class="profile-section">
				<div class="col-md-12 profile-head-bg">
					<div class="profile-left">
						<!-- begin profile-image -->
						<div class="profile-image" style="border:none;">
							<img src="<?php echo base_url('uploads/images/'.$teacher->photo);?>" width="170px" height="170px" />
							<i class="fa fa-user hide"></i>
						</div>
						<div class="profile-highlight">
							<h4><i class="fa fa-user"></i> <?=$teacher->name?> </h4>
							<div class="checkbox m-b-5 m-t-0">
								<?=$teacher->designation?>
							</div>
						</div>
					</div>
				</div>
				
				<div class="profile-right col-md-12">
					<!-- begin profile-info -->
					<div class="profile-info">
						<!-- begin table -->
						<div class="table-responsive">
							<table class="table table-profile">
								<tbody>
									<tr class="highlight">
										<td class="field" colspan="4" style="text-align:left;"><h4><?=$this->lang->line("personal_information")?> <small>about teacher</small></h4></td>
									</tr>
									<tr class="divider">
										<td colspan="4"></td>
									</tr>
									<tr>
										<td class="field"><?=$this->lang->line("teacher_dob")?></td>
										<td><?=date("d M Y", strtotime($teacher->dob))?> </td>
										<td class="field"><?=$this->lang->line("teacher_jod")?> </td>
										<td><?=date("d M Y", strtotime($teacher->jod))?> </td>
									</tr>
									<tr>
										<td class="field"><?=$this->lang->line("teacher_sex")?> </td>
										<td><?=$teacher->sex?></td>
										<td class="field"><?=$this->lang->line("teacher_religion")?> </td>
										<td><?=$teacher->religion?></td>
									</tr>
									<tr>
										<td class="field"><?=$this->lang->line("teacher_email")?> </td>
										<td><?=$teacher->email?></td>
										<td class="field"><?=$this->lang->line("teacher_phone")?> </td>
										<td><i class="fa fa-mobile fa-lg m-r-5"></i> <?=$teacher->phone?></td>
									</tr>
									<tr>
										<td class="field"><?=$this->lang->line("teacher_address")?> </td>
										<td><?=$teacher->address?></td>
										<?php if($usertype == "Admin" || $usertype == "Super Admin") { ?>
										<td class="field"><?=$this->lang->line("teacher_username")?></td>
										<td><?=$teacher->username?></td>
										<?php } ?>
									</tr>
								</tbody>
							</table>
						</div>
						<!-- end table -->
					</div>
					<!-- end profile-info -->
				</div>
			</div>
		</div>
	</div>

    <?php if($usertype == "Admin" || $usertype == "Super Admin") { ?>

    <!-- Modal content start here -->
    <div class="modal fade" id="idCard">
      <div class="modal-dialog">
        <div class="modal-content">
            <div id="idCardPrint">
              <div class="modal-header">
                <?=$this->lang->line('idcard')?>
              </div>
              <div class="modal-body" > 
                    <table>
                        <tr>
                            <td>
                                <h4 style="margin:0;">
                                <?php 
                                    if($siteinfos->photo) {
                                        $array = array(
                                            "src" => base_url('uploads/images/'.$this->session->userdata('schoollogo')),
                                            'width' => '25px',
                                            'height' => '25px',
                                            "style" => "margin-bottom:10px;"
                                        );
                                        echo img($array);
                                    }
                                ?>
                                </h4>
                            </td>
                            <td style="padding-left:5px;">
                                <h4><?=$this->session->userdata('school');?></h4>
                            </td>
                        </tr>
                    </table>

                <table class="idcard-Table" style="background-color:#173640">
                    <tr>
                        <td>
                            <h4>
                        <?php
                            $image_properties = array(
                                'src' => base_url('uploads/images/'.$teacher->photo),
                                'style' => 'border: 8px solid #2F6C7F',
                            );
                            echo img($image_properties);
                        ?>
                            </h4> 
                        </td>
                        <td class="row-style">
                            <h3><?php  echo $teacher->name; ?></h3>
                            <h5><?php  echo $this->lang->line("teacher_designation")." : ".$teacher->designation; ?>
                            </h5>
                            <h5><?php  echo $this->lang->line("teacher_email")." : ".$teacher->email; ?>
                            </h5>
                            <h5>
                                <?php  echo $this->lang->line("teacher_phone")." : ".$teacher->phone; ?>
                            </h5>
                        </td>
                    </tr>
                </table>    
              </div>
            </div>
          <div class="modal-footer">
            <button type="button" style="margin-bottom:0px;" class="btn btn-default" onclick="javascript:closeWindow()"  data-dismiss="modal"><?=$this->lang->line('close')?></button>
            <button type="button" class="btn btn-success" onclick="javascript:printDiv('idCardPrint')"><?=$this->lang->line('print')?></button>
          </div>
        </div>
      </div>
    </div>
    <!-- Modal content End here -->

<!-- email modal starts here -->
<form class="form-horizontal" role="form" action="<?=base_url('teacher/send_mail');?>" method="post">
    <div class="modal fade" id="mail">
      <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title"><?=$this->lang->line('mail')?></h4>
            </div>
            <div class="modal-body">
            
                <?php 
                    if(form_error('to')) 
                        echo "<div class='form-group has-error' >";
                    else     
                        echo "<div class='form-group' >";
                ?>
                    <label for="to" class="col-sm-2 control-label">
                        <?=$this->lang->line("to")?>
                    </label>
                    <div class="col-sm-6">
                        <input type="email" class="form-control" id="to" name="to" value="<?=set_value('to')?>" >
                    </div>
                    <span class="col-sm-4 control-label" id="to_error">
                    </span>
                </div>

                <?php 
                    if(form_error('subject')) 
                        echo "<div class='form-group has-error' >";
                    else     
                        echo "<div class='form-group' >";
                ?>
                    <label for="subject" class="col-sm-2 control-label">
                        <?=$this->lang->line("subject")?>
                    </label>
                    <div class="col-sm-6">
                        <input type="text" class="form-control" id="subject" name="subject" value="<?=set_value('subject')?>" >
                    </div>
                    <span class="col-sm-4 control-label" id="subject_error">
                    </span>

                </div>

                <?php 
                    if(form_error('message')) 
                        echo "<div class='form-group has-error' >";
                    else     
                        echo "<div class='form-group' >";
                ?>
                    <label for="message" class="col-sm-2 control-label">
                        <?=$this->lang->line("message")?>
                    </label>
                    <div class="col-sm-6">
                        <textarea class="form-control" id="message" style="resize: vertical;" name="message" value="<?=set_value('message')?>" ></textarea>
                    </div>
                </div>

            
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" style="margin-bottom:0px;" data-dismiss="modal"><?=$this->lang->line('close')?></button>
    
                <input type="button" id="send_pdf" class="btn btn-success" value="<?=$this->lang->line("send")?>" />
            </div>
        </div>
      </div>
    </div>
</form>
<!-- email end here -->

    <script language="javascript" type="text/javascript">
        function printDiv(divID) {
            //Get the HTML of div
            var divElements = document.getElementById(divID).innerHTML;
            //Get the HTML of whole page
            var oldPage = document.body.innerHTML;

            //Reset the page's HTML with div's HTML only
            document.body.innerHTML = 
              "<html><head><title></title></head><body>" + 
              divElements + "</body>";

            //Print Page
            window.print();

            //Restore orignal HTML
            document.body.innerHTML = oldPage;
        }
        function closeWindow() {
            location.reload(); 
        }
        
        function check_email(email) {
            var status = false;     
            var emailRegEx = /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i;
            if (email.search(emailRegEx) == -1) {
                $("#to_error").html('');
                $("#to_error").html("<?=$this->lang->line('mail_valid')?>").css("text-align", "left").css("color", 'red');
            } else {
                status = true;
            }
            return status;
        }


        $("#send_pdf").click(function(){
            var to = $('#to').val();
            var subject = $('#subject').val();
            var message = $('#message').val();
            var id = "<?=$teacher->teacherID?>";
            var error = 0;

            if(to == "" || to == null) {
                error++;
                $("#to_error").html("");
                $("#to_error").html("<?=$this->lang->line('mail_to')?>").css("text-align", "left").css("color", 'red');
            } else {
                if(check_email(to) == false) {
                    error++
                }
            } 

            if(subject == "" || subject == null) {
                error++;
                $("#subject_error").html("");
                $("#subject_error").html("<?=$this->lang->line('mail_subject')?>").css("text-align", "left").css("color", 'red');
            } else {
                $("#subject_error").html("");
            }

            if(error == 0) {
                $.ajax({
                    type: 'POST',
                    url: "<?=base_url('teacher/send_mail')?>",
                    data: 'to='+ to + '&subject=' + subject + "&id=" + id+ "&message=" + message,
                    dataType: "html",
                    success: function(data) {
                        location.reload();
                    }
                });
            }
        });
    </script>

    <?php } ?>

<?php } ?>