<?php 

    $usertype = $this->session->userdata("usertype");
    if($usertype == "Super Admin") {
?>

    <section class="panel">
        <div class="panel-body profile-view-dis">
            <p>
                <?=img(base_url('uploads/images/'.$reg_school->photo))?>
            </p>
            
            <h1><?=$this->lang->line("school_information")?></h1>
            <div class="row">
                <div class="profile-view-tab">
                    <p><span><?=$this->lang->line("school_school")?> </span>: <?=$reg_school->school?></p>
                </div>

                <div class="profile-view-tab">
                    <p><span><?=$this->lang->line("school_email")?> </span>: <?=$reg_school->email?></p>
                </div>
                <div class="profile-view-tab">
                    <p><span><?=$this->lang->line("school_phone")?> </span>: <?=$reg_school->phone?></p>
                </div>
                <div class="profile-view-tab">
                    <p><span><?=$this->lang->line("school_address")?> </span>: <?=$reg_school->address?></p>
                </div>
                <div class="profile-view-tab">
                    <p><span><?=$this->lang->line("create_date")?> </span>: <?=$reg_school->create_date?></p>
                </div>
                <div class="profile-view-tab">
                    <p><span><?=$this->lang->line("applicant_name")?> </span>: <?=$reg_school->name?></p>
                </div>
                <div class="profile-view-tab">
                    <p><span><?=$this->lang->line("applicant_dob")?> </span>: <?=$reg_school->dob?></p>
                </div>
                <div class="profile-view-tab">
                    <p><span><?=$this->lang->line("applicant_sex")?> </span>: <?=$reg_school->sex?></p>
                </div>
                <div class="profile-view-tab">
                    <p><span><?=$this->lang->line("applicant_username")?> </span>: <?=$reg_school->username?></p>
                </div>
                
            </div>
       
        </div>
    </section>


<?php } ?>

