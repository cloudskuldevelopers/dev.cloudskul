<div class="row">
	<div class="col-md-2 hidden-xs">
		<img src="<?php echo base_url('uploads/images/logo.png'); ?>" style="width: auto; height: 40px; margin-top: -8px;" class="img-thumbnail img-responsive" />
	</div>
	<div class="col-md-7 col-xs-9 text-center">
		<h1 class="page-header"><?php echo $this->session->userdata('school');  ?></h1>
	</div>
	<div class="col-md-3 col-xs-3">
		<!-- begin breadcrumb -->
		<ol class="breadcrumb pull-right">
			<li><a href="<?=base_url("dashboard/index")?>"><i class="fa fa-laptop"></i> <?=$this->lang->line('menu_dashboard')?></a></li>
            <li class="active"><?=$this->lang->line('menu_applied_school')?></li>
		</ol>
		<!-- end breadcrumb -->
		
	</div>
</div>
<div class="panel panel-inverse" data-sortable-id="ui-widget-3">
	<div class="panel-heading">
		<h4 class="panel-title"><i class="fa fa-pencil"></i> <?=$this->lang->line('panel_title')?></h4>
	</div>
	<div class="panel-body">
		<div class="col-sm-12">
			<?php 
				$usertype = $this->session->userdata("usertype");
				if($usertype == "Super Admin") {
			?>                
			<div id="hide-table">
				<table id="example1" class="table table-striped table-bordered table-hover dataTable no-footer">
					<thead>
						<tr>
							<th class="col-lg-1"><?=$this->lang->line('slno')?></th>
							<th class="col-lg-2"><?=$this->lang->line('school_school')?></th>
							<th class="col-lg-2"><?=$this->lang->line('create_date')?></th>
							<th class="col-lg-2"><?=$this->lang->line('school_email')?></th>
							<th class="col-lg-2"><?=$this->lang->line('school_phone')?></th>
							<th class="col-lg-2"><?=$this->lang->line('school_enroll')?></th>
							<th class="col-lg-1"><?=$this->lang->line('action')?></th>
						</tr>
					</thead>
					<tbody>
						<?php if(count($applied_school)) {$i = 1; foreach($applied_school as $school) { ?>
							<tr>
								<td data-title="<?=$this->lang->line('slno')?>">
									<?php echo $i; ?>
								</td>
								<td data-title="<?=$this->lang->line('school_school')?>">
									<?=$school->school?>
								</td>
								<td data-title="<?=$this->lang->line('create_date')?>">
									<?php echo $school->create_date; ?>
								</td>
								<td data-title="<?=$this->lang->line('school_email')?>">
									<?=$school->email; ?>
								</td>
								<td data-title="<?=$this->lang->line('school_phone')?>">
									<?=$school->phone; ?>
								</td>

								<td data-title="<?=$this->lang->line('school_enroll')?>">
									<div class="onoffswitch-small" id="<?=$school->tmp_schoolID?>">
										<input type="checkbox" id="myonoffswitch<?=$school->tmp_schoolID?>" class="onoffswitch-small-checkbox" name="paypal_demo" >
										<label for="myonoffswitch<?=$school->tmp_schoolID?>" class="onoffswitch-small-label">
											<span class="onoffswitch-small-inner"></span>
											<span class="onoffswitch-small-switch"></span>
										</label>
									</div>
								</td>

								<td data-title="<?=$this->lang->line('action')?>">
									<?php echo btn_view('applied_school/view/'.$school->tmp_schoolID, $this->lang->line('view')) ?>
									<?php echo btn_delete('applied_school/delete/'.$school->tmp_schoolID, $this->lang->line('delete')) ?>
								</td>
							</tr>
						<?php $i++; }} ?>
					</tbody>
				</table>
			</div>
			<?php } ?>

		</div>
	</div>
</div>
</div>

<script>
  var status = '';
  var id = 0;
  $('.onoffswitch-small-checkbox').click(function() {
      var id = $(this).parent().attr("id");
      if((id !='')) {
          $.ajax({
              type: 'POST',
              url: "<?=base_url('applied_school/active')?>",
              data: "id=" + id,
              dataType: "html",
              success: function(data) {
                window.location.href = data;
              }
          });
      }
  }); 
</script>

