<div class="row">
	<div class="col-md-2 hidden-xs">
		<img src="<?php echo base_url('uploads/images/logo.png'); ?>" style="width: auto; height: 40px; margin-top: -8px;" class="img-thumbnail img-responsive" />
	</div>
	<div class="col-md-7 col-xs-9 text-center">
		<h1 class="page-header"><?php echo $this->session->userdata('school');  ?></h1>
	</div>
	<div class="col-md-3 col-xs-3">
		<!-- begin breadcrumb -->
		<ol class="breadcrumb pull-right">
			<li ><a href="<?php echo base_url('dashboard/index');?>"><i class="fa fa-laptop"></i><?=$this->lang->line('menu_dashboard')?></a></li>
			<li class="active"><?=$this->lang->line('menu_sattendance')?></a></li>
		</ol>
		<!-- end breadcrumb -->
	</div>
</div>
<!-- begin panel -->
<div class="panel panel-inverse" data-sortable-id="ui-widget-3">
	<div class="panel-heading">
		<?php 
			$usertype = $this->session->userdata("usertype");
			if($usertype == "Super Admin" || $usertype == "Admin" || $usertype == 'Teacher') {
		?>
		<div class="btn-group-vertical pull-right">
			<a href="<?php echo base_url('sattendance/add') ?>" type="button" class="btn btn-success btn-xs"><i class="fa fa-plus"></i><?=$this->lang->line('add_title')?></a>
		</div>
		<?php } ?>
		<h4 class="panel-title"><i class="fa fa-book"></i> <?=$this->lang->line('panel_title')?></h4>
	</div>
	<div class="panel-body">
		<div class="col-md-6 add_padding">
			<form style="" class="form-horizontal" role="form" method="post">
				<div class="input-group m-b-10">
					<span class="input-group-addon offset_addon"><i class="fa fa-sitemap"></i></span>
					<?php
						$array = array("0" => $this->lang->line("attendance_select_classes"));
						foreach ($classes as $classa) {
							$array[$classa->classesID] = $classa->classes;
						}
						echo form_dropdown("classesID", $array, set_value("classesID",$set), "id='classesID' class='form-control offset_height'");
					?>
				</div>
			</form>
		</div>
		<?php 
			$usertype = $this->session->userdata("usertype");
			if($usertype == "Admin" || $usertype == "Super Admin" || $usertype == 'Teacher') {
		?>

		<?php if(count($students) > 0 ) { ?>

			<div class="col-sm-12">

				<div class="nav-tabs-custom b1">
					<ul class="nav nav-tabs">
						<li class="active"><a data-toggle="tab" href="#all" aria-expanded="true"><?=$this->lang->line("attendance_all_students")?></a></li>
						<?php foreach ($sections as $key => $section) {
							echo '<li class=""><a data-toggle="tab" href="#'.$section->schoolID.$section->sectionID.'" aria-expanded="false">'. $this->lang->line("student_section")." ".$section->section. " ( ". $section->category." )".'</a></li>';
						} ?>
					</ul>



					<div class="tab-content">
						<div id="all" class="tab-pane active">
							<div id="hide-table">
								<table id="example1" class="table table-striped table-bordered table-hover dataTable no-footer">
									<thead>
										<tr>
											<th class="col-sm-2"><?=$this->lang->line('slno')?></th>
											<th class="col-sm-2"><?=$this->lang->line('attendance_photo')?></th>
											<th class="col-sm-2"><?=$this->lang->line('attendance_name')?></th>
											<th class="col-sm-2"><?=$this->lang->line('attendance_roll')?></th>
											<th class="col-sm-2"><?=$this->lang->line('attendance_phone')?></th>
											<th class="col-sm-2"><?=$this->lang->line('action')?></th>
										</tr>
									</thead>
									<tbody>
										<?php if(count($students)) {$i = 1; foreach($students as $student) { ?>
											<tr>
												<td data-title="<?=$this->lang->line('slno')?>">
													<?php echo $i; ?>
												</td>

												<td data-title="<?=$this->lang->line('attendance_photo')?>">
													<?php $array = array(
															"src" => base_url('uploads/images/'.$student->photo),
															'width' => '35px',
															'height' => '35px',
															'class' => 'img-rounded'

														);
														echo img($array); 
													?>
												</td>
												<td data-title="<?=$this->lang->line('attendance_name')?>">
													<?php echo $student->name; ?>
												</td>
												<td data-title="<?=$this->lang->line('attendance_roll')?>">
													<?php echo $student->roll; ?>
												</td>
												<td data-title="<?=$this->lang->line('attendance_phone')?>">
													<?php echo $student->phone; ?>
												</td>
												<td data-title="<?=$this->lang->line('action')?>">
													<?php echo btn_view('sattendance/view/'.$student->studentID."/".$set, $this->lang->line('view')); ?>
												</td>
										   </tr>
										<?php $i++; }} ?>
									</tbody>
								</table>
							</div>

						</div>

						<?php foreach ($sections as $key => $section) { ?>
								<div id="<?=$section->schoolID.$section->sectionID?>" class="tab-pane">
									<div id="hide-table">
										<table class="table table-striped table-bordered table-hover dataTable no-footer">
											<thead>
												<tr>
													<th class="col-sm-2"><?=$this->lang->line('slno')?></th>
													<th class="col-sm-2"><?=$this->lang->line('attendance_photo')?></th>
													<th class="col-sm-2"><?=$this->lang->line('attendance_name')?></th>
													<th class="col-sm-2"><?=$this->lang->line('attendance_roll')?></th>
													<th class="col-sm-2"><?=$this->lang->line('attendance_phone')?></th>
													<th class="col-sm-2"><?=$this->lang->line('action')?></th>
												</tr>
											</thead>
											<tbody>
												<?php if(count($allsection[$section->sectionID])) { $i = 1; foreach($allsection[$section->sectionID] as $student) { ?>
													<tr>
														<td data-title="<?=$this->lang->line('slno')?>">
															<?php echo $i; ?>
														</td>

														<td data-title="<?=$this->lang->line('attendance_photo')?>">
															<?php $array = array(
																	"src" => base_url('uploads/images/'.$student->photo),
																	'width' => '35px',
																	'height' => '35px',
																	'class' => 'img-rounded'

																);
																echo img($array); 
															?>
														</td>
														<td data-title="<?=$this->lang->line('attendance_name')?>">
															<?php echo $student->name; ?>
														</td>
														<td data-title="<?=$this->lang->line('attendance_roll')?>">
															<?php echo $student->roll; ?>
														</td>
														<td data-title="<?=$this->lang->line('attendance_phone')?>">
															<?php echo $student->phone; ?>
														</td>
														<td data-title="<?=$this->lang->line('action')?>">
															<?php echo btn_view('sattendance/view/'.$student->studentID."/".$set, $this->lang->line('view')); ?>
														</td>
												   </tr>
												<?php $i++; }} ?>
											</tbody>
										</table>
									</div>
								</div>
						<?php } ?>
					</div>

				</div> <!-- nav-tabs-custom -->
			</div> <!-- col-sm-12 for tab -->

		<?php } else { ?>
			<div class="col-sm-12">

				<div class="nav-tabs-custom">
					<ul class="nav nav-tabs">
						<li class="active"><a data-toggle="tab" href="#all" aria-expanded="true"><?=$this->lang->line("attendance_all_students")?></a></li>
					</ul>


					<div class="tab-content">
						<div id="all" class="tab-pane active">
							<div id="hide-table">
								<table id="example1" class="table table-striped table-bordered table-hover dataTable no-footer">
									<thead>
										<tr>
											<th class="col-sm-2"><?=$this->lang->line('slno')?></th>
											<th class="col-sm-2"><?=$this->lang->line('attendance_photo')?></th>
											<th class="col-sm-2"><?=$this->lang->line('attendance_name')?></th>
											<th class="col-sm-2"><?=$this->lang->line('attendance_roll')?></th>
											<th class="col-sm-2"><?=$this->lang->line('attendance_phone')?></th>
											<th class="col-sm-2"><?=$this->lang->line('action')?></th>
										</tr>
									</thead>
									<tbody>
										<?php if(count($students)) {$i = 1; foreach($students as $student) { ?>
											<tr>
												<td data-title="<?=$this->lang->line('slno')?>">
													<?php echo $i; ?>
												</td>

												<td data-title="<?=$this->lang->line('attendance_photo')?>">
													<?php $array = array(
															"src" => base_url('uploads/images/'.$student->photo),
															'width' => '35px',
															'height' => '35px',
															'class' => 'img-rounded'

														);
														echo img($array); 
													?>
												</td>
												<td data-title="<?=$this->lang->line('attendance_name')?>">
													<?php echo $student->name; ?>
												</td>
												<td data-title="<?=$this->lang->line('attendance_roll')?>">
													<?php echo $student->roll; ?>
												</td>
												<td data-title="<?=$this->lang->line('attendance_phone')?>">
													<?php echo $student->phone; ?>
												</td>
												<td data-title="<?=$this->lang->line('action')?>">
													<?php echo btn_view('sattendance/view/'.$student->studentID."/".$set, $this->lang->line('view')); ?>
												</td>
										   </tr>
										<?php $i++; }} ?>
									</tbody>
								</table>
							</div>

						</div>
					</div>
				</div> <!-- nav-tabs-custom -->
			</div>
		<?php } ?>


		<?php } ?>
	</div>
</div>	




<script type="text/javascript">
    $('#classesID').change(function() {
        var classesID = $(this).val();
        if(classesID == 0) {
            $('#hide-table').hide();
        } else {
            $.ajax({
                type: 'POST',
                url: "<?=base_url('sattendance/student_list')?>",
                data: "id=" + classesID,
                dataType: "html",
                success: function(data) {
                    window.location.href = data;
                }
            });
        }
    });
</script>