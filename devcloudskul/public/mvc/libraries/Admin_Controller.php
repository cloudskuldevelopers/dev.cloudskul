<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class MY_Controller extends MY_Controller {
/*
| -----------------------------------------------------
| PRODUCT NAME: 	CloudSkul
| -----------------------------------------------------
| AUTHOR:			CLOUDSKUL TEAM
| -----------------------------------------------------
| EMAIL:			info@cloudskul.com
| -----------------------------------------------------
| COPYRIGHT:		RESERVED BY Cloudskul
| -----------------------------------------------------
| WEBSITE:			http://cloudskul.com
| -----------------------------------------------------
*/

	function __construct () {
		
		parent::__construct();
		$this->load->model("signin_m");
		$this->load->model("systemadmin_m");
		$this->load->model("message_m");
		$this->load->model("site_m");
		$this->load->model("school_m");
		$this->load->model("applied_school_m");
		$this->data["siteinfos"] = $this->site_m->get_site(1, "TRUE");
		$this->data["allschools"] = $this->school_m->get_school();
		$this->data['applied_school'] = $this->applied_school_m->get_tmp_school();
		$this->load->library("session");
		$this->load->helper('language');
		$this->load->helper('date');
		$this->load->helper('form');
		$this->load->library('form_validation');
		$this->data["setschool"] = $this->school_m->get_school($this->session->userdata('schoolID'));

		/* Alert System Start.........*/
		$this->load->model("notice_m");
		$this->load->model("alert_m");
		$this->data['all'] = array();
		$this->data['alert'] = array();
		$notices = $this->notice_m->get_order_by_notice(array('schoolID' => $this->session->userdata('schoolID')));
		$i = 0;
		if(count($notices) >0) {
			foreach ($notices as $notice) {
				$this->data['all'][] = $this->alert_m->get_order_by_alert(array('schoolID' => $this->session->userdata('schoolID'), "noticeID" => $notice->noticeID, "username" => $this->session->userdata("username")));
				if(count($this->data['all'][$i]) == 0) {
					$this->data['alert'][] = $notice;
				}
				$i++;
			}
		}
		$this->data['alert'];
		/* Alert System End.........*/
		/*message counter*/
		$email = $this->session->userdata('email');
		$usertype = $this->session->userdata('usertype');

		/*
		* access level
		*/
		if($usertype == 'Super Admin'){
			$id = $this->session->userdata('loginuserID');
			$access = $this->systemadmin_m->get_systemadmin($id);
			$this->data['current_access'] = explode(',', $access->priviledges);
		}
		// $userID = $this->userID();
		$this->data['unread'] = $this->message_m->get_order_by_message(array('email' => $email, 'receiverType' => $usertype, 'to_status' => 0, 'read_status' => 0));
		/*message counter end*/

		$language = $this->session->userdata('lang');
		$this->lang->load('topbar_menu', $language);

		$exception_uris = array(
			"signin/index",
			"signin/signout",
			"donation/index",
			"donation/payment",
			"donation/payment_success",
			"donation/success/",
			"donation/payment_fail",
			"donation/make_payment/",
			"invoice/batched_response",
			"applied_school/email_template2",
			"applied_school/emailer"
		);
		$new_uri = preg_replace('/[0-9]+/', '', uri_string());
		//dump($new_uri);
		//if(in_array(uri_string(), $exception_uris) == FALSE) {
		if(in_array($new_uri, $exception_uris) == FALSE) {
			if($this->signin_m->loggedin() == FALSE) {
				redirect(base_url("signin/index"));
				//echo base_url(); die("testing");
			}
		}
	}
}

/* End of file MY_Controller.php */
/* Location: .//D/xampp/htdocs/school/mvc/libraries/MY_Controller.php */
